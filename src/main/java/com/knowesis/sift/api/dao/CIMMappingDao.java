package com.knowesis.sift.api.dao;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.protocol.views.Query;
import com.couchbase.client.protocol.views.Stale;
import com.couchbase.client.protocol.views.View;
import com.couchbase.client.protocol.views.ViewResponse;
import com.couchbase.client.protocol.views.ViewRow;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.PandaCacheConfig;
import com.knowesis.sift.api.model.CIM;

public class CIMMappingDao {

	public static LinkedHashMap< String, JsonObject > cimExpressions = new LinkedHashMap< String, JsonObject >();
	public static HashMap< String, JsonObject > cimMappingDef = new HashMap< String, JsonObject >();
	
	public void createFromFile( String fileName ) throws IOException {
	
		HashMap< String, JsonObject > cimList = new CIMDao().read();
		
		JsonParser parser = new JsonParser();
		PandaCacheConfig cache = new PandaCacheConfig( );

		BufferedReader br = new BufferedReader( new FileReader( fileName ) );
		String line;
		while ( ( line = br.readLine() ) != null ) {
			String[ ] tokens = line.split( "~" );
			System.out.println( tokens[ 0 ] + " =====> " + tokens[ 1 ] );
			JsonObject jObj = ( JsonObject ) parser.parse( tokens[ 1 ] );
			
			//iterate over all stored CIM Mappings
			JsonObject storedCIMMapping = jObj.get( "storedCIMMapping" ).getAsJsonObject();
			Iterator< Entry< String, JsonElement >> iter = storedCIMMapping.entrySet().iterator();
			while( iter.hasNext() ) {
				Entry< String, JsonElement > element = iter.next();
				JsonObject aMapping = element.getValue().getAsJsonObject();
    			if( aMapping.get( "expression" ) != null && aMapping.get( "expression" ).getAsString().trim().length() > 0 ) {
    				String id = tokens[ 0 ] + "_" + element.getKey();
    				JsonObject thisCIM = cimList.get( element.getKey() );
    				aMapping.addProperty( "expressionType", thisCIM.get( "type" ).getAsString() );
    				aMapping.addProperty( "id", id );
    				cimExpressions.put( id, aMapping );
    			}
			}
			cache.getClient().set( tokens[ 0 ], 0, jObj.toString() );
		}	
		System.out.println( cimExpressions.toString() );
	}
	
	public  HashMap< String, JsonObject > read( boolean refresh, String source) {
		
		if( cimMappingDef.size() > 0 && ! refresh )
			return cimMappingDef;
		
		
		HashMap< String, JsonObject > cimList = new CIMDao().read();
		System.out.println( "Reading from **** Persist **** " );
		PandaCacheConfig cache = new PandaCacheConfig();
		View view = cache.getView( "view_AllCIMMappingDefinitions" );
		CouchbaseClient client = cache.getClient();
		Query query = new Query();
		query.setIncludeDocs( false );
		query.setStale( Stale.FALSE );
		ViewResponse viewresponse = client.query( view, query );
		Iterator< ViewRow > itr = viewresponse.iterator();
		JsonParser parser = new JsonParser();
		while( itr.hasNext() ) {
			ViewRow record = itr.next();
			String doc = ( String ) cache.get( record.getKey() );
			JsonObject jobj = ( JsonObject ) parser.parse( doc );
			String id = jobj.get( "id" ).getAsString();
			if(! id.equals( source )) 
				continue;
			cimMappingDef.put( id, jobj );
			System.out.println("CIMMapping for "+id);
			JsonObject storedCIMMapping = jobj.get( "storedCIMMapping" ).getAsJsonObject();
			Iterator< Entry< String, JsonElement >> iter = storedCIMMapping.entrySet().iterator();
			while( iter.hasNext() ) {
				Entry< String, JsonElement > element = iter.next();
				JsonObject aMapping = element.getValue().getAsJsonObject();
    			if( aMapping.get( "expression" ) != null && aMapping.get( "expression" ).getAsString().trim().length() > 0 ) {
    				String mappingId = id + "_" + element.getKey();
    				JsonObject thisCIM = cimList.get( element.getKey() );
    				aMapping.addProperty( "expressionType", thisCIM.get( "type" ).getAsString() );
    				aMapping.addProperty( "id", mappingId );
    				cimExpressions.put( mappingId, aMapping );
    			}
			}
			
			JsonObject storedFilters = jobj.get( "storedFilters" ).getAsJsonObject();
			Iterator< Entry< String, JsonElement >> filterIter = storedFilters.entrySet().iterator();
			while( filterIter.hasNext() ) {
				Entry< String, JsonElement > element = filterIter.next();
				JsonObject aMapping = element.getValue().getAsJsonObject();
    			if( aMapping.get( "expression" ) != null && aMapping.get( "expression" ).getAsString().trim().length() > 0 ) {
    				String mappingId = id + "_" + element.getKey();
    				JsonObject thisCIM = cimList.get( element.getKey() );
    				aMapping.addProperty( "expressionType", thisCIM.get( "type" ).getAsString() );
    				aMapping.addProperty( "id", mappingId );
    				cimExpressions.put( mappingId, aMapping );
    			}
			}
		}
		return cimMappingDef;
	}

	/**
	 * @param args
	 * @throws URISyntaxException 
	 * @throws IOException 
	 */
	public static void main( String[ ] arg ) throws URISyntaxException, IOException {
		if( arg.length < 3 ) {
			System.out.println( "Usage : siftInit PersistAddressList action(export/import) filename" );
			return;
		}

		String strPersistAddressList = arg[ 0 ];
		LinkedList< URI > uris = new LinkedList< URI >();
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );
		for( int i = 0; i < arrPersistAddress.length; i++ )
			uris.add( new URI( arrPersistAddress[ i ] ) );
		CIMMappingDao dao = new CIMMappingDao();
		dao.createFromFile( arg[2] );
	}

}
