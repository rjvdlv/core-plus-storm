package com.knowesis.sift.api.dao;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import com.knowesis.sift.expression.SiftMath;

import au.com.bytecode.opencsv.CSVReader;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.protocol.views.Query;
import com.couchbase.client.protocol.views.Stale;
import com.couchbase.client.protocol.views.View;
import com.couchbase.client.protocol.views.ViewResponse;
import com.couchbase.client.protocol.views.ViewRow;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.PandaCacheConfig;
import com.knowesis.sift.api.model.function;

public class FunctionDao {

	public LinkedHashMap< String, JsonObject > readAll() {
		PandaCacheConfig cache = new PandaCacheConfig();
		View view = cache.getView( "view_allFunctions" );
		CouchbaseClient client = cache.getClient();
		Query query = new Query();
		query.setIncludeDocs( false );
		query.setStale( Stale.FALSE );
		ViewResponse viewresponse = client.query( view, query );
		Iterator< ViewRow > itr = viewresponse.iterator();

		LinkedHashMap< String, JsonObject > allFunctions = new LinkedHashMap< String, JsonObject >();

		JsonParser parser = new JsonParser();
		while( itr.hasNext() ) {
			ViewRow record = itr.next();
			String doc = ( String ) cache.get( record.getKey() );
			JsonObject jobj = ( JsonObject ) parser.parse( doc );
			String id = record.getId();
			allFunctions.put( id, jobj );
		}
		return allFunctions;
	}
	
	public LinkedHashMap< String, JsonObject > readUserDefined() {
		PandaCacheConfig cache = new PandaCacheConfig();
		View view = cache.getView( "view_allFunctions" );
		CouchbaseClient client = cache.getClient();
		Query query = new Query();
		query.setIncludeDocs( false );
		query.setStale( Stale.FALSE );
		ViewResponse viewresponse = client.query( view, query );
		Iterator< ViewRow > itr = viewresponse.iterator();

		LinkedHashMap< String, JsonObject > allFunctions = new LinkedHashMap< String, JsonObject >();

		JsonParser parser = new JsonParser();
		while( itr.hasNext() ) {
			ViewRow record = itr.next();
			String doc = ( String ) cache.get( record.getKey() );
			JsonObject jobj = ( JsonObject ) parser.parse( doc );
			String id = record.getId();
			allFunctions.put( id, jobj );
		}
		for( int i = 0; i < SiftMath.functions.length; i++ ) {
			String thisFunc = SiftMath.functions[ i ];
			if( allFunctions.containsKey(  thisFunc ) )
				allFunctions.remove( thisFunc );
		}
		return allFunctions;
	}
	
	public java.util.HashMap<String,  function > createFromFile( String fileName ) throws IOException {
		CSVReader reader = new CSVReader( new FileReader( fileName ) );
		String[ ] line = null;
		java.util.HashMap<String,  function > funcList = new java.util.HashMap< String, function >();
		while( ( line = reader.readNext() ) != null ) {
			System.out.println( line[ 0 ] );
			funcList.put( line[ 0 ], new function( line[ 0 ], line[ 1 ], line[ 2 ], line[ 3 ] ) );
		}
		PandaCacheConfig cache = new PandaCacheConfig();
		cache.getClient().set( "FUNCTIONS", 0, funcList );
		return funcList;
	}

	public void createDocumentsFromFile( String functionsFile ) throws IOException {
		CSVReader reader = new CSVReader( new FileReader( functionsFile ) );
		String[ ] line = null; 
		PandaCacheConfig cache = new PandaCacheConfig();
		while( ( line = reader.readNext() ) != null ) {
			JsonObject jfunc = new JsonObject();
			jfunc.addProperty( "docType", "Function" );
			jfunc.addProperty( "_class", "com.knowesis.sift.api.model.Function" );
			jfunc.addProperty( "name", line[0] );
			jfunc.addProperty( "signature", line[1] );
			jfunc.addProperty( "returnType", line[2] ); 
			jfunc.addProperty( "description", line[3] );
			cache.getClient().set(  line[0], 0, jfunc.toString() );
		}
	}
	
	public java.util.HashMap<String,  function > read() {
		PandaCacheConfig cache = new PandaCacheConfig();
		java.util.HashMap<String,  function > funcList = ( java.util.HashMap<String,  function > ) cache.getClient().get( "FUNCTIONS" );
		return funcList;
	}
}
