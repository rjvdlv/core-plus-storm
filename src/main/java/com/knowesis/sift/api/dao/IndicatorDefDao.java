package com.knowesis.sift.api.dao;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import au.com.bytecode.opencsv.CSVReader;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.protocol.views.Query;
import com.couchbase.client.protocol.views.Stale;
import com.couchbase.client.protocol.views.View;
import com.couchbase.client.protocol.views.ViewResponse;
import com.couchbase.client.protocol.views.ViewRow;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.PandaCacheConfig;
import com.knowesis.sift.expression.PExpressionEvaluator;
import com.knowesis.sift.expression.PExpressionException;

/**
 * @author Raja SP
 * 
 */
public class IndicatorDefDao {
	
	private static java.util.LinkedHashMap< String, JsonObject > allInd;

	// Creates All from file
	public void createFromFile( String fileName ) throws IOException {
		CSVReader reader = new CSVReader( new FileReader( fileName ) );
		String[ ] line = null;

		int nRecs = -1;
		while( ( line = reader.readNext() ) != null ) {
			nRecs++;
			if( nRecs == 0 )
				continue;

			if( line[ 0 ] == null || line[ 0 ].trim().length() == 0 )
				continue;

			String docType = "IndicatorDefinition";
			JsonObject jObj = new JsonObject();
			jObj.addProperty( "docType", docType );
			jObj.addProperty( "id", line[ 0 ] );
			jObj.addProperty( "internalId", "i" + nRecs );
			jObj.addProperty( "description", line[ 0 ] );
			jObj.addProperty( "serviceType", line[ 1 ] );
			jObj.addProperty( "serviceSubType", line[ 2 ] );
			jObj.addProperty( "measurementType", line[ 3 ] );
			jObj.addProperty( "frequency", line[ 4 ] );
			jObj.addProperty( "owner", line[ 5 ] );
			jObj.addProperty( "expressionType", line[ 6 ] );
			jObj.addProperty( "expression", line[ 7 ] );
			jObj.addProperty( "status", line[ 8 ] );
			jObj.addProperty( "scriptOrExpression", line[ 9 ] );
			jObj.addProperty( "alternateNames", line[ 10 ] );
			
			jObj.addProperty( "creationDate", new Long( new java.util.Date().getTime() ) );
			jObj.addProperty( "lastUpateDate", new Long( new java.util.Date().getTime() ) );

			new PandaCacheConfig().getClient().set( line[ 0 ], 0, jObj.toString() );

//			if( line[ 0 ].contains( "VOICE_ALL_Count" ) )
				System.out.println( line[ 0 ] );

		}
		System.out.println( "Total Records : " + nRecs );
	}

	// Reads all from file
	public java.util.LinkedHashMap< String, JsonObject > read() {
		
		if( allInd != null )
			return allInd;
		
		System.out.println( "Reading from **** Persist **** " );
		PandaCacheConfig cache = new PandaCacheConfig();
		View view = cache.getView( "view_allIndicatorDefinitions" );
		CouchbaseClient client = cache.getClient();
		Query query = new Query();
		query.setIncludeDocs( false );
		query.setStale( Stale.FALSE );
		ViewResponse viewresponse = client.query( view, query );
		Iterator< ViewRow > itr = viewresponse.iterator();

		allInd = new java.util.LinkedHashMap< String, JsonObject >();
		
		JsonParser parser = new JsonParser();
		while( itr.hasNext() ) {
			ViewRow record = itr.next();
			String doc = ( String ) cache.get( record.getKey() );
			JsonObject jobj = ( JsonObject ) parser.parse( doc );
			String id = record.getId();
			if( ! jobj.get( "frequency" ).getAsString().equals( "Summary" ) )     //SUMMARY INDICATOR
				allInd.put( id, jobj );
		}
		return allInd;
	}

	
	// Reads all from file
	public java.util.LinkedHashMap< String, JsonObject > readPartial( String limits ) {
		System.out.println( "********** READING WITH LIMITS : " + limits );
		if( allInd == null )
			allInd = this.read();
		
		LinkedHashMap< String, JsonObject > partialInd = new java.util.LinkedHashMap< String, JsonObject >();
		if( allInd != null ) {
			Iterator< Entry< String, JsonObject >> itr = allInd.entrySet().iterator();
			int i = 0;
			while( itr.hasNext() && i < Integer.parseInt( limits ) ) {
				Entry< String, JsonObject > entry = itr.next();
			    partialInd.put( entry.getKey(), entry.getValue() );
			    i++;
			}
		}
		return partialInd;

/*
		PandaCache cache = new PandaCache();
		View view = cache.getView( "view_allIndicatorDefinitions" );
		CouchbaseClient client = cache.getClient();
		Query query = new Query();
		query.setIncludeDocs( true );
		query.setStale( Stale.FALSE );
		query.setLimit( Integer.parseInt( limits ) ); 
		ViewResponse viewresponse = client.query( view, query );
		Iterator< ViewRow > itr = viewresponse.iterator();

		java.util.LinkedHashMap< String, JsonObject > indMap = new java.util.LinkedHashMap< String, JsonObject >();
		
		JsonParser parser = new JsonParser();
		while( itr.hasNext() ) {
			ViewRow record = itr.next();
			JsonObject jobj = ( JsonObject ) parser.parse( record.getDocument().toString() );
			String id = record.getId();
			indMap.put( id, jobj );
		}
		JsonObject jObjTotal = new JsonObject();
		jObjTotal.addProperty( "TotalRows", viewresponse.getTotalRows());
	    indMap.put( "TotalRows", jObjTotal );
		return indMap;
*/		
	}

	// Reads all from file
	public java.util.LinkedHashMap< String, JsonObject > readActive( ) {
		PandaCacheConfig cache = new PandaCacheConfig();
		View view = cache.getView( "view_AllActiveIndicatorDefinitions" );
		CouchbaseClient client = cache.getClient();
		Query query = new Query();
		query.setIncludeDocs( false );
		query.setStale( Stale.FALSE );
		ViewResponse viewresponse = client.query( view, query );
		Iterator< ViewRow > itr = viewresponse.iterator();

		java.util.LinkedHashMap< String, JsonObject > indMap = new java.util.LinkedHashMap< String, JsonObject >();
		
		JsonParser parser = new JsonParser();
		while( itr.hasNext() ) {
			ViewRow record = itr.next();
			String doc = ( String ) cache.get( record.getKey() );
			JsonObject jobj = ( JsonObject ) parser.parse( doc );
			String id = record.getId();
			if( ! jobj.get( "frequency" ).getAsString().equals( "Summary" ) )      //SUMMARY INDICATOR
			    indMap.put( id, jobj );
		}
		return indMap;
	}

	
    //SUMMARY INDICATOR
	public java.util.LinkedHashMap< String, JsonObject > readActiveSummaryIndicators( ) {
		PandaCacheConfig cache = new PandaCacheConfig();
		View view = cache.getView( "view_AllActiveIndicatorDefinitions" );
		CouchbaseClient client = cache.getClient();
		Query query = new Query();
		query.setIncludeDocs( false );
		query.setStale( Stale.FALSE );
		ViewResponse viewresponse = client.query( view, query );
		Iterator< ViewRow > itr = viewresponse.iterator();

		java.util.LinkedHashMap< String, JsonObject > indMap = new java.util.LinkedHashMap< String, JsonObject >();
		
		JsonParser parser = new JsonParser();
		while( itr.hasNext() ) {
			ViewRow record = itr.next();
			String doc = ( String ) cache.get( record.getKey() );
			JsonObject jobj = ( JsonObject ) parser.parse( doc );
			String id = record.getId();
			if( jobj.get( "frequency" ).getAsString().equals( "Summary" ) )
			    indMap.put( id, jobj );
		}
		return indMap;
	}

	
	/*
	// creates the specified accumulator definition
	public void create( String name, String id, String description, String serviceType, String serviceSubType,
			String measurementType, String frequency, String expression, String expressionType, String status,
			String scriptOrExpression, String alternateNames )
			throws PExpressionException, IOException {

		// create the ExpressionEvaluator object

		String docType = "AccumulatorDefinition";
		JsonObject jObj = new JsonObject();
		jObj.addProperty( "docType", docType );
		jObj.addProperty( "id", id );
		jObj.addProperty( "description", description );
		jObj.addProperty( "serviceType", serviceType );
		jObj.addProperty( "serviceSubType", serviceSubType );
		jObj.addProperty( "measurementType", measurementType );
		jObj.addProperty( "frequency", frequency );
		jObj.addProperty( "expressionType", expressionType );
		jObj.addProperty( "expression", expression );
		jObj.addProperty( "status", status );
		jObj.addProperty( "scriptOrExpression", scriptOrExpression );
		jObj.addProperty( "creationDate", new Long( new java.util.Date().getTime() ) );
		jObj.addProperty( "lastUpateDate", new Long( new java.util.Date().getTime() ) );
		jObj.addProperty( "alternateNames", alternateNames );

		ExpressionDao expDao = new ExpressionDao();
		PExpressionEvaluator expEval = expDao.createByDefinition( jObj );
		PandaCacheConfig cache = new PandaCacheConfig();
		java.util.HashMap< String, PExpressionEvaluator > expressionsMap = ( HashMap< String, PExpressionEvaluator > ) cache
				.getClient().get( "expressionsMap" );
		if( expressionsMap == null )
			expressionsMap = new java.util.HashMap< String, PExpressionEvaluator >();
		expressionsMap.put( id, expEval );
		cache.getClient().set( "expressionsMap", 0, expressionsMap );

		System.out.println( jObj.toString() );

		cache.getClient().set( id, 0, jObj.toString() );
	}
 
    */
 	
	// reads the specified accumulator definition
	public String read( String id ) {
		
		if( allInd == null )
			allInd = this.read();
		
		JsonObject ind = allInd.get( id );
		if( ind != null )
		    return allInd.get( id ).toString();
		else
			return new JsonObject().toString();
		/*
		PandaCache cache = new PandaCache();
		return ( String ) cache.getClient().get( id );
		*/
	}

	/*
	// updates the specified accumulator definition
	public void update( String id, String jsonString ) throws PExpressionException, IOException {
		JsonParser parser = new JsonParser();
		JsonObject jObj = ( JsonObject ) parser.parse( jsonString );
		ExpressionDao expDao = new ExpressionDao();
		System.out.println( jObj.toString() );
		PExpressionEvaluator expEval = expDao.createByDefinition( jObj );
		PandaCacheConfig cache = new PandaCacheConfig();
//		java.util.HashMap< String, PExpressionEvaluator > expressionsMap = ( HashMap< String, PExpressionEvaluator > ) cache
//				.getClient().get( "expressionsMap" );
//		if( expressionsMap == null )
//			expressionsMap = new java.util.HashMap< String, PExpressionEvaluator >();
//		expressionsMap.put( id, expEval );
//		cache.getClient().set( "expressionsMap", 0, expressionsMap );
		System.out.println( jObj.toString() );
		cache.getClient().set( id, 0, jObj.toString() );
		
		String status = jObj.get( "status" ).getAsString();
		if( ! status.equals( "DELETED" ) )
			allInd.put( id, jObj );
		else
			allInd.remove( id );
	}
	*/

	public static void main( String[ ] arg ) throws IOException, PExpressionException {
		IndicatorDefDao dao = new IndicatorDefDao();
		dao.createFromFile( "/home/stream/PandaData/accumulatorAIS.csv" );
		// System.out.println( dao.read().toString() );
	}

}
