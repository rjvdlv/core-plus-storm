package com.knowesis.sift.api.dao;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.protocol.views.Query;
import com.couchbase.client.protocol.views.Stale;
import com.couchbase.client.protocol.views.View;
import com.couchbase.client.protocol.views.ViewResponse;
import com.couchbase.client.protocol.views.ViewRow;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.PandaCacheConfig;

public class SupportingDataDao {

	public HashMap< String, JsonObject > getSupportingData() {
		PandaCacheConfig cache = new PandaCacheConfig();
		View view = cache.getView( "view_allSupportingData" );
		CouchbaseClient client = cache.getClient();
		Query query = new Query();
		query.setIncludeDocs( false );
		query.setStale( Stale.FALSE );
		ViewResponse viewresponse = client.query( view, query );
		Iterator< ViewRow > itr = viewresponse.iterator();
		java.util.HashMap< String, JsonObject > supportingDataMap = new java.util.HashMap< String, JsonObject >();
		JsonParser parser = new JsonParser();
		while( itr.hasNext() ) {
			ViewRow record = itr.next();
			String supportingDataDocString = ( String ) cache.get( record.getKey() );
			JsonObject jobj = ( JsonObject ) parser.parse( supportingDataDocString );
			jobj.remove( "docType" );
			supportingDataMap.put( record.getKey(), jobj );
		}	
		return supportingDataMap;
	}
	
	/**
	 * @param args
	 * @throws URISyntaxException 
	 */
	public static void main( String[ ] args ) throws URISyntaxException {
		URI uri = new URI( "http://127.0.0.1:8091/pools" );
		LinkedList< URI > uriList = new LinkedList< URI >();
		uriList.add( uri );
		PandaCacheConfig cache = new PandaCacheConfig( uriList );
		
		SupportingDataDao dao = new SupportingDataDao();
		System.out.println( dao.getSupportingData().toString() );
	}

}
