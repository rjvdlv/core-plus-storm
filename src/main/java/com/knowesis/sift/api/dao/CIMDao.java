package com.knowesis.sift.api.dao;

import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

import au.com.bytecode.opencsv.CSVReader;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.protocol.views.Query;
import com.couchbase.client.protocol.views.Stale;
import com.couchbase.client.protocol.views.View;
import com.couchbase.client.protocol.views.ViewResponse;
import com.couchbase.client.protocol.views.ViewRow;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.PandaCacheConfig;
import com.knowesis.sift.api.model.CIM;

public class CIMDao {
	
	public static LinkedHashMap<String,  JsonObject > cimList = new LinkedHashMap<String,  JsonObject > ();
	

	public CIMDao(){

	}

	public LinkedHashMap<String,  JsonObject > read() {
		if( cimList.size() > 0 ) //initalised
			return cimList;
		PandaCacheConfig cache = new PandaCacheConfig();
		
		View view = cache.getView( "view_allCIMs" );
		CouchbaseClient client = cache.getClient();
		Query query = new Query();
		query.setIncludeDocs( false );
		query.setStale( Stale.FALSE );
		ViewResponse viewresponse = client.query( view, query );
		Iterator< ViewRow > itr = viewresponse.iterator();
		cimList = new java.util.LinkedHashMap< String, JsonObject >();
		JsonParser parser = new JsonParser();
		while( itr.hasNext() ) {
			ViewRow record = itr.next();
			String doc = ( String ) cache.get( record.getKey() );
			JsonObject jobj = ( JsonObject ) parser.parse( doc );
			cimList.put( record.getId(), jobj );
		}	
		return cimList;
		
	}
	
	public java.util.HashMap<String,  JsonObject > read( boolean refresh) {
		if( cimList.size() > 0 && ! refresh ) //initalised
			return cimList;
		PandaCacheConfig cache = new PandaCacheConfig();
		View view = cache.getView( "view_allCIMs" );
		CouchbaseClient client = cache.getClient();
		Query query = new Query();
		query.setIncludeDocs( false );
		query.setStale( Stale.FALSE );
		ViewResponse viewresponse = client.query( view, query );
		Iterator< ViewRow > itr = viewresponse.iterator();
		cimList = new java.util.LinkedHashMap< String, JsonObject >();
		JsonParser parser = new JsonParser();
		while( itr.hasNext() ) {
			ViewRow record = itr.next();
			String doc = ( String ) cache.get( record.getKey() );
			JsonObject jobj = ( JsonObject ) parser.parse( doc );
			cimList.put( record.getId(), jobj );
		}	
		return cimList;
	}

	/*
	public java.util.HashMap<String,  CIM > createFromFile( String fileName ) throws IOException {
		//CSVReader reader = new CSVReader( new FileReader( fileName ) );
		CSVReader reader = new CSVReader( new FileReader( fileName ), '~' );
		String[ ] line = null;
		while( ( line = reader.readNext() ) != null ) {
			System.out.println( line[ 0 ] );
			cimList.put( line[ 0 ], new CIM( line[ 0 ], line[ 1 ], line[ 2 ], line[ 3 ], line[ 4 ] ) );
		}
		PandaCacheConfig cache = new PandaCacheConfig();
		cache.getClient().set( "CIM", 0, cimList );
		return cimList;
	}

	public void createDocumentsFromFile( String cimConFile ) throws IOException {
		CSVReader reader = new CSVReader( new FileReader( cimConFile ), '~' );
		String[ ] line = null;
		PandaCacheConfig cache = new PandaCacheConfig();
		while( ( line = reader.readNext() ) != null ) {
			JsonObject jCIM = new JsonObject();
			jCIM.addProperty( "docType", "CIM" );
			jCIM.addProperty( "_class", "com.knowesis.sift.api.model.CIM" );
			jCIM.addProperty( "name", line[ 0 ] );
			jCIM.addProperty( "type", line[ 1 ] );
			jCIM.addProperty( "alias", line[ 2 ] );
			jCIM.addProperty( "category", line[ 3 ] );
			jCIM.addProperty( "description", line[ 0 ] );
			cache.getClient().set( line[0] , 0, jCIM.toString() );
		}
	}
	*/
	
	public static void main( String[] args ) {
		PandaCacheConfig cacheConfig = new PandaCacheConfig();
		CIMDao dao = new CIMDao();
		LinkedHashMap<String, JsonObject> allCIMs = dao.read();
		Set<String> ks = allCIMs.keySet();
		Iterator<String> itr = ks.iterator();
		while( itr.hasNext() ) {
			System.out.println( allCIMs.get( itr.next() ).getAsJsonObject().toString() );
		}
		
		
	}
	
}
