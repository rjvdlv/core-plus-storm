package com.knowesis.sift.api.dao;

import java.util.Iterator;
import java.util.LinkedHashMap;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.protocol.views.Query;
import com.couchbase.client.protocol.views.Stale;
import com.couchbase.client.protocol.views.View;
import com.couchbase.client.protocol.views.ViewResponse;
import com.couchbase.client.protocol.views.ViewRow;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.PandaCacheConfig;

/**
 * @author Raja SP
 * 
 */

public class ProgramsDAO {

	private static java.util.LinkedHashMap< String, JsonObject > allPrograms = null;
	
	/**
	 * @return
	 */
	public java.util.LinkedHashMap< String, JsonObject > read() {
		
		/*if (allPrograms != null) {
			return allPrograms;
		}*/
		
		PandaCacheConfig cache = new PandaCacheConfig();
		View view = cache.getView( "view_allPrograms" );
		CouchbaseClient client = cache.getClient();
		Query query = new Query();
		query.setIncludeDocs( false );
		query.setStale( Stale.FALSE );
		ViewResponse viewresponse = client.query( view, query );
		Iterator< ViewRow > itr = viewresponse.iterator();
		allPrograms = new java.util.LinkedHashMap< String, JsonObject >(1);
		JsonParser parser = new JsonParser();
		while( itr.hasNext() ) {
			ViewRow record = itr.next();
			String doc = ( String ) cache.get( record.getKey() );
			JsonObject jobj = ( JsonObject ) parser.parse( doc );
			allPrograms.put( record.getId(), jobj );
		}
		return allPrograms;
	}
	
	/**
	 * @return
	 */
	public java.util.LinkedHashMap< String, JsonObject > readActive() {
		
		//if( allPrograms == null )
		allPrograms = this.read();
		
		LinkedHashMap< String, JsonObject > activePrograms = 
			new java.util.LinkedHashMap< String, JsonObject >();
		
		if( allPrograms != null ) {
			Iterator< String> itr = allPrograms.keySet().iterator();
			String key;
			JsonObject value;
			while( itr.hasNext()) {
				key = itr.next();
				value = allPrograms.get(key);
				
				if (value.has("status")) {
					if ((value.get("status").getAsString().equals("Active")) || (value.get("status").getAsString().equalsIgnoreCase("simulation"))) {
						activePrograms.put( key, value );
					}
				}
			}
		}
		
		return activePrograms;
	}

	public LinkedHashMap< String, JsonObject > readActive( boolean refresh ) {
		LinkedHashMap< String, JsonObject > activePrograms = 
				new java.util.LinkedHashMap< String, JsonObject >();
			
			allPrograms = this.read();
			if( allPrograms != null ) {
				Iterator< String> itr = allPrograms.keySet().iterator();
				String key;
				JsonObject value;
				while( itr.hasNext()) {
					key = itr.next();
					value = allPrograms.get(key);
					
					if (value.has("status")) {
						if ((value.get("status").getAsString().equals("Active"))) {
							activePrograms.put( key, value );
						}
					}
				}
			}
			
			return activePrograms;
	}

}
