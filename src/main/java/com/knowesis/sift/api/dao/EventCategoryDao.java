package com.knowesis.sift.api.dao;

import java.util.Iterator;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.protocol.views.Query;
import com.couchbase.client.protocol.views.Stale;
import com.couchbase.client.protocol.views.View;
import com.couchbase.client.protocol.views.ViewResponse;
import com.couchbase.client.protocol.views.ViewRow;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.knowesis.sift.Common.PandaCacheConfig;

public class EventCategoryDao {

	public String read() {
		PandaCacheConfig cache = new PandaCacheConfig();
		View view = cache.getView( "view_allEventCategoryDefinitions" );
		CouchbaseClient client = cache.getClient();
		Query query = new Query();
		query.setIncludeDocs( true );
		query.setStale( Stale.FALSE );
		ViewResponse viewresponse = client.query( view, query );
		Iterator< ViewRow > itr = viewresponse.iterator();
		JsonArray jArry = new JsonArray();
		JsonParser parser = new JsonParser();
		while( itr.hasNext() ) {
			ViewRow record = itr.next();
			JsonObject jobj = ( JsonObject ) parser.parse( record.getDocument().toString() );
			jArry.add( new JsonPrimitive( jobj.get( "id" ).getAsString() ) );
		}
		return jArry.toString();

	}

	/**
	 * @param args
	 */
	public static void main( String[ ] args ) {
		// TODO Auto-generated method stub

	}

}
