package com.knowesis.sift.api.dao;

import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import au.com.bytecode.opencsv.CSVReader;

import com.knowesis.sift.Common.PandaCache;
import com.knowesis.sift.api.model.TagListItem;

/**
 * @author Raja SP
 * 
 */

public class TagListDao {

	private static String IND_TAGLIST_PREFIX = "indicator_taglist_";
	private static String EVENT_TAGLIST_PREFIX = "EventTagList-";
	private static String INDICATOR_FOR_ALL_TAGLIST = "indicator_for_all_taglist";
	private static String EVENT_FOR_ALL_TAGLIST = "event_for_all_taglist";
	private static ArrayList< TagListItem > indTaggedForAll = null;
	private static ArrayList< TagListItem > eventsTaggedForAll = null;

	public ArrayList< TagListItem > getIndicatorTagList( String msisdn ) {
		PandaCache cache = new PandaCache();
		// ArrayList< TagListItem > indTaggedForAll =
		// this.getIndicatorsTaggedForAll();
		if( indTaggedForAll == null )
			indTaggedForAll = this.getIndicatorsTaggedForAll();

		if( indTaggedForAll == null )
			indTaggedForAll = new ArrayList< TagListItem >();
		// ArrayList< TagListItem > result = ( ArrayList< TagListItem > )
		// cache.getClient().get( IND_TAGLIST_PREFIX + msisdn );
		// if( result != null )
		// indTaggedForAll.addAll( result );
		return indTaggedForAll;
	}

	public void setIndicatorTagList( String msisdn, ArrayList< TagListItem > list ) {
		PandaCache cache = new PandaCache();
		cache.getClient().set( IND_TAGLIST_PREFIX + msisdn, 0, list );
	}

	public ArrayList< TagListItem > getEventTagList( String msisdn ) {
		PandaCache cache = new PandaCache();
		// ArrayList<TagListItem> eventsTaggedForAll =
		// this.getEventsTaggedForAll();
		if( eventsTaggedForAll == null )
			eventsTaggedForAll = this.getEventsTaggedForAll();

		if( eventsTaggedForAll == null )
			eventsTaggedForAll = new ArrayList< TagListItem >();
		// ArrayList< TagListItem > result = ( ArrayList< TagListItem > )
		// cache.getClient().get( EVENT_TAGLIST_PREFIX + msisdn );
		// if( result != null )
		// eventsTaggedForAll.addAll( result );
		return eventsTaggedForAll;
	}

	public void setEventTagList( String msisdn, ArrayList< TagListItem > list ) {
		PandaCache cache = new PandaCache();
		cache.getClient().set( EVENT_TAGLIST_PREFIX + msisdn, 0, list );
	}

	public ArrayList< TagListItem > getIndicatorsTaggedForAll() {
		PandaCache cache = new PandaCache();
		ArrayList< TagListItem > result = ( ArrayList< TagListItem > ) cache.getClient()
				.get( INDICATOR_FOR_ALL_TAGLIST );
		return result;
	}

	public void setIndicatorsForAll( ArrayList< TagListItem > list ) {
		PandaCache cache = new PandaCache();
		cache.getClient().set( INDICATOR_FOR_ALL_TAGLIST, 0, list );
	}

	public ArrayList< TagListItem > getEventsTaggedForAll() {
		PandaCache cache = new PandaCache();
		ArrayList< TagListItem > result = ( ArrayList< TagListItem > ) cache.getClient().get( EVENT_FOR_ALL_TAGLIST );
		return result;
	}

	public void setEventsForAll( ArrayList< TagListItem > list ) {
		PandaCache cache = new PandaCache();
		cache.getClient().set( EVENT_FOR_ALL_TAGLIST, 0, list );
	}

	public void createFromFile( String indTagFileName, String eventTagFileName ) throws IOException, ParseException {
		// MSISDN, Indicator Id, Tag Start Date, Tag End Date, Tagging Duration,
		// External Id
		CSVReader reader = new CSVReader( new FileReader( indTagFileName ) );
		String[ ] line = null;
		PandaCache cache = new PandaCache();
		int nRec = -1;
		while( ( line = reader.readNext() ) != null ) {
			nRec++;
			if( nRec == 0 )
				continue;
			if( line[ 0 ] == null || line[ 0 ].trim().length() == 0 )
				continue;
			// String type, String msisdn, String id, Date startDate, Date
			// endDate, int duration, String extId;
			long lstart = 0;
			long lend = 0;

			SimpleDateFormat dtf = new SimpleDateFormat( "ddMMyyyy HH:mm:ss" );
			if( line[ 2 ] != null ) {
				Date dt = dtf.parse( line[ 2 ] );
				lstart = dt.getTime();
			}
			if( line[ 3 ] == null ) {
				Date endDt = dtf.parse( line[ 3 ] );
				lend = endDt.getTime();
			}

			int duration = 0;
			if( line[ 4 ] != null && line[ 4 ].trim().length() > 0 )
				duration = Integer.parseInt( line[ 4 ] );

			TagListItem item = new TagListItem( "Indicator", line[ 0 ], line[ 1 ], lstart, lend, duration, line[ 5 ] );
			String msisdn = line[ 0 ];
			if( msisdn.equals( "ALL" ) ) {
				ArrayList< TagListItem > indtaglist = this.getIndicatorsTaggedForAll();
				if( indtaglist == null ) {
					indtaglist = new ArrayList< TagListItem >();
				}
				indtaglist.add( item );
				this.setIndicatorsForAll( indtaglist );
			}else {
				ArrayList< TagListItem > indtaglist = this.getIndicatorTagList( msisdn );
				if( indtaglist == null ) {
					indtaglist = new ArrayList< TagListItem >();
				}
				indtaglist.add( item );
				this.setIndicatorTagList( msisdn, indtaglist );
			}
		}

		// Read the events taglist file
		reader = new CSVReader( new FileReader( eventTagFileName ) );
		nRec = -1;
		while( ( line = reader.readNext() ) != null ) {
			nRec++;
			if( nRec == 0 )
				continue;
			if( line[ 0 ] == null || line[ 0 ].trim().length() == 0 )
				continue;
			// String type, String msisdn, String id, Date startDate, Date
			// endDate, int duration, String extId;
			long lstart = 0;
			long lend = 0;

			SimpleDateFormat dtf = new SimpleDateFormat( "ddMMyyyy HH:mm:ss" );
			if( line[ 2 ] != null ) {
				Date dt = dtf.parse( line[ 2 ] );
				lstart = dt.getTime();
			}
			if( line[ 3 ] == null ) {
				Date endDt = dtf.parse( line[ 3 ] );
				lend = endDt.getTime();
			}

			int duration = 0;
			if( line[ 4 ] != null && line[ 4 ].trim().length() > 0 )
				duration = Integer.parseInt( line[ 4 ] );

			TagListItem item = new TagListItem( "Event", line[ 0 ], line[ 1 ], lstart, lend, duration, line[ 5 ] );
			String msisdn = line[ 0 ];
			if( msisdn.equals( "ALL" ) ) {
				ArrayList< TagListItem > indtaglist = this.getEventsTaggedForAll();
				if( indtaglist == null ) {
					indtaglist = new ArrayList< TagListItem >();
				}
				indtaglist.add( item );
				this.setEventsForAll( indtaglist );
			}else {
				ArrayList< TagListItem > indtaglist = this.getEventTagList( msisdn );
				if( indtaglist == null ) {
					indtaglist = new ArrayList< TagListItem >();
				}
				indtaglist.add( item );
				this.setEventTagList( msisdn, indtaglist );
			}
		}
	}
}
