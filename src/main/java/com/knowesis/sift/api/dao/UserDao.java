package com.knowesis.sift.api.dao;

import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;

import au.com.bytecode.opencsv.CSVReader;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.protocol.views.Query;
import com.couchbase.client.protocol.views.Stale;
import com.couchbase.client.protocol.views.View;
import com.couchbase.client.protocol.views.ViewResponse;
import com.couchbase.client.protocol.views.ViewRow;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.PandaCache;

/**
 * @author Raja SP
 * 
 */
public class UserDao {

	private static LinkedHashMap< String, JsonObject > allUser;

	/*
	{
		"docType": "siftusers",
			"id": "sift-user-raja",
			"username": "Raja",
			"password": "password",
			"group": "ADMIN",
			"timestamp": 1378537502684
	}
*/
	// Creates All from file
	public void createFromFile( String fileName ) throws IOException {
		CSVReader reader = new CSVReader( new FileReader( fileName ) );
		String[ ] line = null;

		int nRecs = -1;
		while( ( line = reader.readNext() ) != null ) {
			nRecs++;
			if( nRecs == 0 )
				continue;

			if( line[ 0 ] == null || line[ 0 ].trim().length() == 0 )
				continue;

			String docType = "siftusers";
			JsonObject jObj = new JsonObject();
			jObj.addProperty( "docType", docType );
			jObj.addProperty( "id", line[ 0 ] );
			jObj.addProperty( "username", line[ 1 ]);
			jObj.addProperty( "password", line[ 2 ] );
			jObj.addProperty( "group", line[ 3 ] );
			jObj.addProperty( "timestamp", new Long( new java.util.Date().getTime() ) );


			new PandaCache().getClient().set( line[ 0 ], 0, jObj.toString() );

			//if( line[ 0 ].contains( "VOICE_ALL_Count" ) )
				System.out.println( line[ 0 ] );

		}
		System.out.println( "Total Records : " + nRecs );
	}


	// Reads all from file
	public LinkedHashMap< String, JsonObject > read() {

		System.out.println( "Reading Users from **** Persist **** " );
		PandaCache cache = new PandaCache();
		View view = cache.getView( "view_allUsers" );
		CouchbaseClient client = cache.getClient();
		Query query = new Query();
		query.setIncludeDocs( false );
		query.setStale( Stale.FALSE );
		ViewResponse viewresponse = client.query( view, query );
		Iterator< ViewRow > itr = viewresponse.iterator();

		allUser = new LinkedHashMap< String, JsonObject >();

		JsonParser parser = new JsonParser();
		while( itr.hasNext() ) {
			ViewRow record = itr.next();
			String doc = ( String ) cache.get( record.getKey() );
			JsonObject jobj = ( JsonObject ) parser.parse( doc );
			String id = jobj.get( "id" ).getAsString();
			allUser.put( id, jobj );
		}
		return allUser;
	}



	// creates the specified user definition
	public void create( String username, String id, String password, String group)
			throws  IOException {



		String docType = "siftusers";
		JsonObject jObj = new JsonObject();

		jObj.addProperty( "docType", docType );
		jObj.addProperty( "id", id );
		jObj.addProperty( "username", username);
		jObj.addProperty( "password", password );
		jObj.addProperty( "group", group );
		jObj.addProperty( "timestamp", new Long( new java.util.Date().getTime() ) );


		PandaCache cache = new PandaCache();

		System.out.println( jObj.toString() );

		cache.getClient().set( id, 0, jObj.toString() );
	}

	
	// reads the specified user definition
	public String read( String id ) {
		

		PandaCache cache = new PandaCache();

		JsonParser parser = new JsonParser();
		String docStr = (String)cache.getClient().get(id);
		JsonObject userObj =  ( JsonObject ) parser.parse(docStr);
		if( userObj != null )
		    return userObj.toString();
		else
			return new JsonObject().toString();
		/*
		PandaCache cache = new PandaCache();
		return ( String ) cache.getClient().get( id );
		*/
	}

	// updates the specified User definition
	public void update( String id, String jsonString ) throws IOException {
		JsonParser parser = new JsonParser();
		JsonObject jObj = ( JsonObject ) parser.parse( jsonString );

		PandaCache cache = new PandaCache();

		System.out.println( jObj.toString() );
		cache.getClient().set( id, 0, jObj.toString() );
		

			//allUser.put( id, jObj );

	}

	// Remove the specified User definition
	public void delete( String id) throws IOException {

		PandaCache cache = new PandaCache();


		cache.getClient().delete(id);

		//allUser.remove( id );
	}


	public static void main( String[ ] arg ) throws IOException {
		UserDao dao = new UserDao();
		//dao.createFromFile( "/home/stream/PandaData/accumulatorconf5.0.csv" );
		String filepath = "/home/stream/PandaData/accumulatorconf5.0.csv";

		if(arg.length >0){
			filepath = arg[0];
		}else {
			System.out.println("Default file path : " + filepath);
			System.out.println("Override file path Usage : java com.knowesis.sift.api.dao.UserDao <UserFile> ");

		}
		dao.createFromFile(filepath);
		// System.out.println( dao.read().toString() );
	}

}
