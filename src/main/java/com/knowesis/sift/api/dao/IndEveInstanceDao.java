package com.knowesis.sift.api.dao;

import com.knowesis.sift.Common.PandaCacheConfig;


/**
 * @author Raja SP
 * 
 */

public class IndEveInstanceDao {

	// Read all the Accumulators belonging to the subscriber.
	public String read( String msisdn, String instanceType ) {
		String key = msisdn;
		if( instanceType.equals( "Indicator" ) )
			key += "-Indicators";
		else
			key += "-Events";

		PandaCacheConfig cache = new PandaCacheConfig();
		Object result = cache.getClient().get( key );
		if( result != null )
			return result.toString();
		return null;
	}

	// Set the updated accumulators back into Persist
	public void update( String msisdn, String accum, String instanceType ) {
		String key = msisdn;
		if( instanceType.equals( "Indicator" ) )
			key += "-Indicators";
		else
			key += "-Events";

		PandaCacheConfig cache = new PandaCacheConfig();
		cache.getClient().set( key, 0, accum );
	}
}
