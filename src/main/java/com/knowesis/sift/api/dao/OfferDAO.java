package com.knowesis.sift.api.dao;

import java.util.Iterator;
import java.util.LinkedHashMap;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.protocol.views.Query;
import com.couchbase.client.protocol.views.Stale;
import com.couchbase.client.protocol.views.View;
import com.couchbase.client.protocol.views.ViewResponse;
import com.couchbase.client.protocol.views.ViewRow;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.PandaCacheConfig;

/**
 * @author Raja SP
 * 
 */

public class OfferDAO {

	private static java.util.LinkedHashMap< String, JsonObject > allOffers = null;
	
	/**
	 * @return
	 */
	public java.util.LinkedHashMap< String, JsonObject > read() {
		
		/*if (allOffers != null) {
			return allOffers;
		}*/
		
		PandaCacheConfig cache = new PandaCacheConfig();
		View view = cache.getView( "view_allOffers" );
		CouchbaseClient client = cache.getClient();
		Query query = new Query();
		query.setIncludeDocs( false );
		query.setStale( Stale.FALSE );
		ViewResponse viewresponse = client.query( view, query );
		Iterator< ViewRow > itr = viewresponse.iterator();
		allOffers = new java.util.LinkedHashMap< String, JsonObject >(1);
		JsonParser parser = new JsonParser();
		while( itr.hasNext() ) {
			ViewRow record = itr.next();
			String doc = ( String ) cache.get( record.getKey() );
			JsonObject jobj = ( JsonObject ) parser.parse( doc );
			allOffers.put( record.getId(), jobj );
		}
		return allOffers;
	}

	public LinkedHashMap< String, JsonObject > read( boolean refresh ) {
		PandaCacheConfig cache = new PandaCacheConfig();
		View view = cache.getView( "view_allOffers" );
		CouchbaseClient client = cache.getClient();
		Query query = new Query();
		query.setIncludeDocs( false );
		query.setStale( Stale.FALSE );
		ViewResponse viewresponse = client.query( view, query );
		Iterator< ViewRow > itr = viewresponse.iterator();
		allOffers = new java.util.LinkedHashMap< String, JsonObject >(1);
		JsonParser parser = new JsonParser();
		while( itr.hasNext() ) {
			ViewRow record = itr.next();
			String doc = ( String ) cache.get( record.getKey() );
			JsonObject jobj = ( JsonObject ) parser.parse( doc );
			allOffers.put( record.getId(), jobj );
		}
		return allOffers;
	}
}
