package com.knowesis.sift.api.dao;

import java.util.Iterator;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.protocol.views.Query;
import com.couchbase.client.protocol.views.Stale;
import com.couchbase.client.protocol.views.View;
import com.couchbase.client.protocol.views.ViewResponse;
import com.couchbase.client.protocol.views.ViewRow;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.PandaCacheConfig;

/**
 * @author Raja SP
 * 
 */

public class PandagramDefDao {


	// Reads all from file
	public java.util.LinkedHashMap< String, JsonObject > read() {
		PandaCacheConfig cache = new PandaCacheConfig();
		View view = cache.getView( "view_AllPandagramDefinitions" );
		CouchbaseClient client = cache.getClient();
		Query query = new Query();
		query.setIncludeDocs( false );
		query.setStale( Stale.FALSE );
		ViewResponse viewresponse = client.query( view, query );
		Iterator< ViewRow > itr = viewresponse.iterator();
		java.util.LinkedHashMap< String, JsonObject > pandagramMap = new java.util.LinkedHashMap< String, JsonObject >();
		JsonParser parser = new JsonParser();
		while( itr.hasNext() ) {
			ViewRow record = itr.next();
			String doc = ( String ) cache.get( record.getKey() );
			JsonObject jobj = ( JsonObject ) parser.parse( doc );
			pandagramMap.put( jobj.get( "pandagramId" ).getAsString(), jobj );
		}
		return pandagramMap;
	}

}
