package com.knowesis.sift.api.dao;

import java.io.FileReader;
import java.io.IOException;

import au.com.bytecode.opencsv.CSVReader;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.PandaCacheConfig;
import com.knowesis.sift.api.model.function;

public class UserDefinedFunctionDao {


	public UserDefinedFunctionDao(){

	}

	public JsonObject read() {
		PandaCacheConfig cache = new PandaCacheConfig();
		JsonParser parser = new JsonParser();
		String funcs = ( String ) cache.getClient().get( "USER_DEFINED_FUNCTIONS" );
		if( funcs != null )
		     return parser.parse( funcs ).getAsJsonObject();
		return new JsonObject();
	}
}
