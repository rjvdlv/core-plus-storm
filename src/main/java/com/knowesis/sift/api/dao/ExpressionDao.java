package com.knowesis.sift.api.dao;
 
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCacheConfig;
import com.knowesis.sift.expression.ExpressionProcessor;
import com.knowesis.sift.expression.PExpressionEvaluator;
import com.knowesis.sift.expression.PExpressionException;

/**
 * @author Raja SP
 * 
 */

public class ExpressionDao {

	public static HashMap< String, PExpressionEvaluator > allExpressions = null;

	@SuppressWarnings("unchecked")
	public ExpressionDao() throws PExpressionException, IOException, ParseException{
		//		if( allExpressions == null )
		//			allExpressions = ( HashMap< String, PExpressionEvaluator > ) prepareAllExpressions()[ 0 ];
		allExpressions = new HashMap< String, PExpressionEvaluator >();
	}

	@SuppressWarnings("unchecked")
	public HashMap< String, PExpressionEvaluator > read() throws PExpressionException, IOException, ParseException {
		// PandaCache cache = new PandaCache();
		// HashMap< String, PExpressionEvaluator > allExpressions = ( HashMap<
		// String, PExpressionEvaluator > ) cache
		// .getClient().get( "expressionsMap" );
		if( allExpressions == null )
			allExpressions = ( HashMap< String, PExpressionEvaluator > ) prepareAllExpressions()[0];


		return allExpressions;
	}



	public PExpressionEvaluator prepareExpressions( JsonObject thisObj ) throws PExpressionException, ParseException {
		IndicatorDefDao dao = new IndicatorDefDao();
		LinkedHashMap< String, JsonObject > allIndicators = ( LinkedHashMap< String, JsonObject > ) dao.readActive();
		if( allIndicators == null || allIndicators.size() == 0 )
			throw new PExpressionException( "Indicator Definitions not Available" );

		EventDefDao daoEvents = new EventDefDao();
		LinkedHashMap< String, JsonObject > allEvents = daoEvents.readActive();

		CIMDao cimDao = new CIMDao();
		LinkedHashMap< String, JsonObject > allCIM = cimDao.read();

		FunctionDao funcDao = new FunctionDao();
		LinkedHashMap< String, JsonObject > allFunctions = funcDao.readUserDefined();

		Gson gson = new Gson();
		ExpressionProcessor expProc = new ExpressionProcessor( allIndicators, allEvents, allFunctions, allCIM );
		PExpressionEvaluator thisExp = expProc.createByDefinition( thisObj );
		String params = gson.toJson( thisExp.getParameterNames() );
		thisObj.addProperty( "parameters", params );
		PandaCacheConfig cache = new PandaCacheConfig();
		String id = thisObj.get( "id" ).getAsString();
		cache.getClient().set( id, 0, thisObj.toString() );
		System.out.println( "Setting Parameters to : " + id + " - " + params );
		if( thisExp != null ) 
			allExpressions.put( thisObj.get( "id" ).getAsString(), thisExp );
		return thisExp;
	}

	public Object[] prepareAllExpressions() throws PExpressionException, IOException, ParseException {
		Object[] resObj = new Object[2];
		HashMap< String, String > fixedOfferParams = null;

		allExpressions = new HashMap< String, PExpressionEvaluator >();

		IndicatorDefDao dao = new IndicatorDefDao();
		LinkedHashMap< String, JsonObject > allIndicators = ( LinkedHashMap< String, JsonObject > ) dao.readActive();
		if( allIndicators == null || allIndicators.size() == 0 )
			throw new PExpressionException( "Indicator Definitions not Available" );

		LinkedHashMap< String, JsonObject > allSummaryIndicators = ( LinkedHashMap< String, JsonObject > ) dao.readActiveSummaryIndicators(); 
		EventDefDao daoEvents = new EventDefDao();
		LinkedHashMap< String, JsonObject > allEvents = daoEvents.readActive();

		CIMDao cimDao = new CIMDao();
		LinkedHashMap< String, JsonObject > allCIM = cimDao.read();

		FunctionDao funcDao = new FunctionDao();
		LinkedHashMap< String, JsonObject > allFunctions = funcDao.readUserDefined();

		ExpressionProcessor expProc = new ExpressionProcessor( allIndicators, allEvents, allFunctions, allCIM );

		//FUNC-EXTER
		//compile all functions first
		Gson gson = new Gson();
		Iterator< Entry< String, JsonObject >> funcItr = allFunctions.entrySet().iterator();
		while( funcItr.hasNext() ) {
			Entry< String, JsonObject > entry = funcItr.next();
			JsonObject funcDef = entry.getValue().getAsJsonObject();
			System.out.println( "Compiling Function : " + funcDef.get( "name" ) + " -- > " + funcDef.get( "expression" ) );
			funcDef.addProperty( "expressionType", funcDef.get( "returnType" ).getAsString() );
			funcDef.addProperty( "userDefinedFunction", true );
			PExpressionEvaluator thisExp = expProc.createByDefinition( funcDef );
			String params = gson.toJson( thisExp.getParameterNames() );
			funcDef.addProperty( "parameters", params );
			PandaCacheConfig cache = new PandaCacheConfig();
			cache.getClient().set( entry.getKey(), 0, funcDef.toString() );
			System.out.println( "Setting Parameters to : " + entry.getKey() + " - " + params );
			if( thisExp != null ) 
				allExpressions.put( funcDef.get( "id" ).getAsString(), thisExp );
		}

		//Compile All Indicators
		Iterator< JsonObject > itr = allIndicators.values().iterator();
		while( itr.hasNext() ) {
			JsonObject thisObj = itr.next();
			String id = thisObj.get( "id" ).getAsString();
			System.out.println( "Compiling : " + thisObj.get( "id" ) + " -- > " + thisObj.get( "expression" ) );
			PExpressionEvaluator thisExp = expProc.createByDefinition( thisObj );
			String params = gson.toJson( thisExp.getParameterNames() );
			thisObj.addProperty( "parameters", params );
			PandaCacheConfig cache = new PandaCacheConfig();
			cache.getClient().set( id, 0, thisObj.toString() );
			System.out.println( "Setting Parameters to : " + id + " - " + params );
			if( thisExp != null ) 
				allExpressions.put( id, thisExp );
		}
		
		//Compile All Summary Indicators
		Iterator< JsonObject > summaryItr = allSummaryIndicators.values().iterator();
		while( summaryItr.hasNext() ) {
			JsonObject thisObj = summaryItr.next();
			String id = thisObj.get( "id" ).getAsString();
			System.out.println( "Compiling : " + thisObj.get( "id" ) + " -- > " + thisObj.get( "expression" ) );
			PExpressionEvaluator thisExp = expProc.createByDefinition( thisObj );
			String params = gson.toJson( thisExp.getParameterNames() );
			thisObj.addProperty( "parameters", params );
			PandaCacheConfig cache = new PandaCacheConfig();
			cache.getClient().set( id, 0, thisObj.toString() );
			System.out.println( "Setting Parameters to : " + id + " - " + params );
			if( thisExp != null ) 
				allExpressions.put( id, thisExp );
		}

		//Compile All Triggers
		itr = allEvents.values().iterator();
		while( itr.hasNext() ) {
			JsonObject thisObj = itr.next();
			String id = thisObj.get( "id" ).getAsString();

			String eventType = "";
			if( thisObj.has( "type" ) && !thisObj.get( "type" ).isJsonNull() ) {
				eventType = thisObj.get( "type" ).getAsString();
			}

			if( eventType.equals( "" ) || eventType.equals( "Behavioural" ) || eventType.equals( "NonBehavioural" ) || 
					eventType.equals( "Summary" ) || eventType.equals( "ScheduledTrigger" ) ) {
				System.out.println( "Compiling : " + id + " -- > " + thisObj.get( "expression" ) );
				PExpressionEvaluator thisExp = expProc.createByDefinition( thisObj );
				String params = gson.toJson( thisExp.getParameterNames() );
				thisObj.addProperty( "parameters", params );
				PandaCacheConfig cache = new PandaCacheConfig();
				cache.getClient().set( id, 0, thisObj.toString() );
				System.out.println( "Setting Parameters to : " + id + " - " + params );
				if( thisExp != null ) 
					allExpressions.put( thisObj.get( "id" ).getAsString(), thisExp );
			}
		}

		// Programs Start
		try {
			fixedOfferParams = cookAllOfferExpressions( allIndicators, allEvents, allCIM, allFunctions );
			cookExpressionsFromProgram( allIndicators, allEvents, allCIM, allFunctions );
		}catch( Exception e ) {
			e.printStackTrace();
			throw new PExpressionException( e.getMessage() );
		}
		// Programs End

		// Global Contact Policy Parameters Start
		try {
			PandaCacheConfig cache = new PandaCacheConfig();
			String contactPolicyString = ( String ) cache.get( "ContactPolicy" );
			if( contactPolicyString != null ) {
				JsonParser parser = new JsonParser();
				JsonObject contactPolicyObject = ( JsonObject ) parser.parse( contactPolicyString );

				JsonObject maxContactPerSubscriberObj = contactPolicyObject.get( "maxContactPerSubscriber" ).getAsJsonObject();
				JsonElement maxContactPerSubsEle = maxContactPerSubscriberObj.get( "expression" );
				if( maxContactPerSubsEle != null && ! maxContactPerSubsEle.isJsonNull() && maxContactPerSubsEle.getAsString().trim().length() > 0 ) {
					String id = "GLOBAL_MAX_CONTACT_PER_SUBSCRIBER";
					maxContactPerSubscriberObj.add( "id", new JsonPrimitive( id ) );
					maxContactPerSubscriberObj.add( "expressionType", new JsonPrimitive( maxContactPerSubscriberObj.get( "type" ).getAsString() ) );
					System.out.println( "Compiling : " + id + " -- > " + maxContactPerSubscriberObj.get( "expression" ) );
					PExpressionEvaluator thisExp = expProc.createByDefinition( maxContactPerSubscriberObj );
					if( thisExp != null )
						allExpressions.put( id, thisExp );
				}

				JsonObject maxContactAcrossSubscribersObj = contactPolicyObject.get( "maxContactAcrossSubscribers" ).getAsJsonObject();
				JsonElement maxContactAcrossSubsEle = maxContactAcrossSubscribersObj.get( "expression" ); 
				if( maxContactAcrossSubsEle != null && ! maxContactAcrossSubsEle.isJsonNull() && maxContactAcrossSubsEle.getAsString().trim().length() > 0 ) {
					String id = "GLOBAL_MAX_CONTACT_ACROSS_SUBSCRIBERS";
					maxContactAcrossSubscribersObj.add( "id", new JsonPrimitive( id ) );
					maxContactAcrossSubscribersObj.add( "expressionType", new JsonPrimitive( maxContactAcrossSubscribersObj.get( "type" ).getAsString() ) );
					System.out.println( "Compiling : " + id + " -- > " + maxContactAcrossSubscribersObj.get( "expression" ) );
					PExpressionEvaluator thisExp = expProc.createByDefinition( maxContactAcrossSubscribersObj );
					if( thisExp != null )
						allExpressions.put( id, thisExp );
				}

				JsonObject cooldownObj = contactPolicyObject.get( "cooldownPeriodForOfferPerSubscriber" ).getAsJsonObject();
				JsonElement coolDownEle = cooldownObj.get( "expression" ); 
				if( coolDownEle != null && ! coolDownEle.isJsonNull() && coolDownEle.getAsString().trim().length() > 0 ) {
					String id = "GLOBAL_COOLDOWN_RULE";
					cooldownObj.add( "id", new JsonPrimitive( id ) );
					cooldownObj.add( "expressionType", new JsonPrimitive( cooldownObj.get( "type" ).getAsString() ) );
					System.out.println( "Compiling : " + id + " -- > " + cooldownObj.get( "expression" ) );
					PExpressionEvaluator thisExp = expProc.createByDefinition( cooldownObj );
					if( thisExp != null )
						allExpressions.put( id, thisExp );
				}	
				
				if( contactPolicyObject.has( "advancedContactRules" ) ) {
					JsonObject advancedContactRulesObject = contactPolicyObject.get( "advancedContactRules" ).getAsJsonObject();
					if( advancedContactRulesObject.has( "cooldownPeriodForOfferPerSubscriber" ) ) {
						JsonObject coolDown = advancedContactRulesObject.get( "cooldownPeriodForOfferPerSubscriber" ).getAsJsonObject();
						Iterator< Entry< String, JsonElement > > programIterator = coolDown.entrySet().iterator();
						while( programIterator.hasNext() ) {
							Entry< String, JsonElement > entry = programIterator.next();
							String thisProgramId = entry.getKey();
							JsonObject thisProgramCoolDownValue = entry.getValue().getAsJsonObject();
							Iterator< Entry< String, JsonElement > > compareProgIterator = thisProgramCoolDownValue.entrySet().iterator();
							while( compareProgIterator.hasNext() ) {
								Entry< String, JsonElement > compareEntry = compareProgIterator.next();
								String compareProgId = compareEntry.getKey();
								JsonObject thisProgObject = compareEntry.getValue().getAsJsonObject();
								JsonElement thisExpressionEle = thisProgObject.get( "expression" );
								if( thisExpressionEle == null || thisExpressionEle.isJsonNull() )
									continue;
								
								String thisExpression = thisExpressionEle.getAsString().trim();
								if( thisExpression.length() == 0 )
									continue;
								
								String expId = thisProgramId + "_" + compareProgId + "_COOLDOWNEXP";
								thisProgObject.add( "id", new JsonPrimitive( expId ) );
								thisProgObject.add( "expressionType", new JsonPrimitive( thisProgObject.get( "type" ).getAsString() ) );
								System.out.println( "Compiling : " + expId + " -- > " + thisExpression );
								PExpressionEvaluator thisExp = expProc.createByDefinition( thisProgObject );
								if( thisExp != null )
									allExpressions.put( expId, thisExp );
							}
						}
					}
				}
			}
			
			
			System.out.println( "------------ Looking for evaluateRScript and to compile the expressions-------------"  );
			HashMap< String, PExpressionEvaluator > rScriptExp = new HashMap< String, PExpressionEvaluator >();
			Set< String > expkeys = allExpressions.keySet();
			Iterator< String > expitr = expkeys.iterator();
			while( expitr.hasNext() ) {
				String thiskey = expitr.next();
				PExpressionEvaluator thisExp = allExpressions.get( thiskey );
				String expression = thisExp.getExpression();
				
				System.out.println( thiskey + " ---- " + expression );
				
				String pattern = "\\s*\\S*\\s*(SIFTMATH.evaluateRScript\\s*\\()\\s*(\\S*)(\\s*\\))";

				//String pattern = "\\s*\\S*\\s*(knowesis.SiftMath.evaluateRScript\\s*\\()\\s*(\\S*)(\\s*\\))";
				Pattern p = Pattern.compile( pattern );
				Matcher matcher = p.matcher( expression );
				boolean matches = matcher.find();
				if( matches ) {
					String fileName = matcher.group( 2 );
					try {
						String strScriptDef = readFile( fileName );
						JsonObject scriptDef = ( JsonObject ) new JsonParser().parse( strScriptDef );
						JsonArray arrParamNames = scriptDef.get( "siftParamMap" ).getAsJsonArray();
						JsonArray arrParamTypes = scriptDef.get( "paramTypes" ).getAsJsonArray();
						for( int k = 0; k < arrParamNames.size(); k++ ) {
							String thisParam = arrParamNames.get( k ).getAsString();
							//if this is not a CIM or Indicator, this is an expression
							if( ! allIndicators.containsKey( thisParam ) && ! allCIM.containsKey( thisParam ) ) {
								JsonObject expressionToCook = new JsonObject();
								String id = fileName.replace( "\"", "").replace( "/", "_" ).replace( ".", "" ) + "_" + k;
								expressionToCook.addProperty( "id", id );
								expressionToCook.addProperty( "expression", thisParam );
								expressionToCook.addProperty( "expressionType", arrParamTypes.get( k  ).getAsString() );
								PExpressionEvaluator compiledExp = expProc.getCompiledExpression( expressionToCook );
								System.out.println( "------------id : " + id + "-------------Expression : " + expression  );
								rScriptExp.put( id, compiledExp );
							}
						}
					} catch( IOException e ) {
						throw new PExpressionException( e.toString() );
					}
				}
				
				
				pattern = "\\s*\\S*\\s*(SIFTMATH.executeRModel\\s*\\()\\s*(\\S*)(\\s*\\))";
				p = Pattern.compile( pattern );
				matcher = p.matcher( expression );
				matches = matcher.find();
				if( matches ) {
					String modelId = matcher.group( 2 );
					System.out.println("model Id : " + modelId );
					try {
						String strScriptDef = Commons.pandaCacheConfig.get( modelId.replace( "\"", "") ).toString();
						JsonObject scriptDef = ( JsonObject ) new JsonParser().parse( strScriptDef );
						
						JsonArray arrParamTypes = new JsonArray();
						JsonArray arrParamNames = new JsonArray();
						JsonArray inputParams = scriptDef.get("modelParams").getAsJsonArray();
						for(int i=0; i< inputParams.size();i++){
							JsonObject thisParam = inputParams.get(i).getAsJsonObject();
							arrParamTypes.add(thisParam.get("paramDataType"));
							arrParamNames.add(thisParam.get("paramMapping"));
						}
						
						for( int k = 0; k < arrParamNames.size(); k++ ) {
							String thisParam = arrParamNames.get( k ).getAsString();
							//if this is not a CIM or Indicator, this is an expression
							if( ! allIndicators.containsKey( thisParam ) && ! allCIM.containsKey( thisParam ) ) {
								JsonObject expressionToCook = new JsonObject();
								String id = modelId.replace( "\"", "").replace( "/", "_" ).replace( ".", "" ) + "_" + k;
								expressionToCook.addProperty( "id", id );
								expressionToCook.addProperty( "expression", thisParam );
								expressionToCook.addProperty( "expressionType", arrParamTypes.get( k  ).getAsString() );
								PExpressionEvaluator compiledExp = expProc.getCompiledExpression( expressionToCook );
								System.out.println( "------------id : " + id + "-------------Expression : " + expression  );
								rScriptExp.put( id, compiledExp );
							}
						}
					} catch( Exception e ) {
						e.printStackTrace();
						throw new PExpressionException( e.toString() );
					}
				}
			}

			expkeys = rScriptExp.keySet();
			expitr = expkeys.iterator();
			while( expitr.hasNext() ) {
				String thiskey = expitr.next();
				PExpressionEvaluator thisExp = rScriptExp.get( thiskey );
				allExpressions.put( thiskey, thisExp );
			}	
			
		}
		catch( Exception e ){
			e.printStackTrace();
			throw new PExpressionException( e.getMessage() );
		}
		// Global Contact Policy Parameters End

		System.out.println( "----------------------------------- Compiled all the Expressions -----------------------------------" );
		resObj[0] = allExpressions;
		resObj[1] = fixedOfferParams;
		return resObj;
	}


	static String readFile( String fileName ) throws IOException {
		fileName = fileName.replace( "\"", "" );
		BufferedReader br = new BufferedReader( new FileReader( fileName ) );
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while( line != null ) {
				sb.append( line );
				sb.append( "\n" );
				line = br.readLine();
			}
			return sb.toString();
		}finally {
			br.close();
		}
	}

	// preparing expression for cimMap config update
	public HashMap< String, PExpressionEvaluator > prepareAllExpressions( LinkedHashMap< String, JsonObject > expressionsList ) throws PExpressionException,
	IOException, ParseException {
		IndicatorDefDao dao = new IndicatorDefDao();
		LinkedHashMap< String, JsonObject > allAccums = ( LinkedHashMap< String, JsonObject > ) dao.read();

		EventDefDao daoEvents = new EventDefDao();
		LinkedHashMap< String, JsonObject > allEvents = daoEvents.read();
		CIMDao cimDao = new CIMDao();
		LinkedHashMap< String, JsonObject > allCIM = cimDao.read();

		FunctionDao funcDao = new FunctionDao();
		LinkedHashMap< String, JsonObject > allFunctions = funcDao.readUserDefined();

		HashMap< String, PExpressionEvaluator > currentExpressions = new HashMap< String, PExpressionEvaluator >();

		ExpressionProcessor expProc = new ExpressionProcessor( allAccums, allEvents, allFunctions, allCIM );

		Iterator< Entry< String, JsonObject >> funcItr = allFunctions.entrySet().iterator();
		while( funcItr.hasNext() ) {
			Entry< String, JsonObject > entry = funcItr.next();
			JsonObject funcDef = entry.getValue().getAsJsonObject();
			System.out.println( "Compiling Function : " + funcDef.get( "id" ) + " -- > " + funcDef.get( "expression" ) );
			funcDef.addProperty( "expressionType", funcDef.get( "returnType" ).getAsString() );
			funcDef.addProperty( "userDefinedFunction", true );
			PExpressionEvaluator thisExp = expProc.createByDefinition( funcDef );
			currentExpressions.put( funcDef.get( "id" ).getAsString(), thisExp );
		}

		Iterator< JsonObject > itr = expressionsList.values().iterator();
		while( itr.hasNext() ) {
			JsonObject thisObj = itr.next();
			System.out.println( "Compiling : " + thisObj.get( "id" ) + " -- > " + thisObj.get( "expression" ) );
			PExpressionEvaluator thisExp = expProc.createByDefinition( thisObj );
			currentExpressions.put( thisObj.get( "id" ).getAsString(), thisExp );
		}
		System.out.println( "----------------------------------- Compiled all the Expressions -----------------------------------" );
		// PandaCache cache = new PandaCache();
		// cache.getClient().set( "expressionsMap", 0, allExpressions );
		return currentExpressions;
	}


	/**expProc
	 * Cook All offer related expressions.
	 * 
	 * @param allAccums
	 * @param allEvents
	 * @param cimList
	 * @throws Exception
	 */
	public HashMap< String, String > cookAllOfferExpressions( LinkedHashMap< String, JsonObject > allAccums, LinkedHashMap< String, JsonObject > allEvents,
			LinkedHashMap< String, JsonObject > cimList, LinkedHashMap< String, JsonObject > allFunctions ) throws Exception {

		ExpressionProcessor expProc = new ExpressionProcessor( allAccums, allEvents, allFunctions, cimList );
		expProc.setExistingExpressions( this.allExpressions );

		OfferDAO offerDAO = new OfferDAO();
		java.util.LinkedHashMap< String, JsonObject > offerMap = offerDAO.read();
		Iterator< String > offerMapIter = offerMap.keySet().iterator();

		String offerId = null;
		JsonObject offerObj = null;
		JsonArray offerThresholdParametersArray = null;
		String offerCriteria = null;
		JsonArray offerMessages = null;
		JsonObject fullfillmentObj = null;
		JsonArray fulfillmentMessages = null;
		JsonArray fulfillmentConfirmMessages = null;
		JsonArray fulfillmentProducts = null;
		HashMap< String, String > fixedOfferParams = new HashMap< String, String >();
		JsonObject reportParameterObj = null;

		ProgramsDAO programsDAO = new ProgramsDAO();
		java.util.LinkedHashMap< String, JsonObject > programsMap = programsDAO.readActive();

		while( offerMapIter.hasNext() ) {
			offerId = offerMapIter.next();
			offerObj = offerMap.get( offerId );
			System.out.println( "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& cookAllOfferExpressions : " +  offerId + "&&&&&&&&&&&&&&&&&&&&&&&&&&&" );

			if( offerObj != null ) {
				JsonElement programsEle = offerObj.get( "programs" );
				JsonArray programsArray = null;
				if( programsEle != null && ! programsEle.isJsonNull() )
					programsArray = programsEle.getAsJsonArray();

				if( programsArray == null )
					continue;

				for( int progs = 0; progs < programsArray.size(); progs ++ ) {
					String programId = programsArray.get( progs ).getAsString();
					if( ! programsMap.containsKey( programId ) )
						continue;

					if( offerObj.has( "offerThresholdParameters" ) ) {
						offerThresholdParametersArray = offerObj.get( "offerThresholdParameters" ).getAsJsonArray();
						cookOfferThresholdParameters( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, allFunctions, expProc, programId );
					}

					if( offerObj.has( "offerCriteria" ) ) {
						offerCriteria = offerObj.get( "offerCriteria" ).getAsString();
						cookOfferCriteriaExpressions( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, offerCriteria, allFunctions, expProc, programId );
					}

					if( offerObj.has( "offerChannels" ) ) {
						offerMessages = offerObj.get( "offerChannels" ).getAsJsonArray();
						cookOfferMessages( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, offerMessages, fixedOfferParams, allFunctions, expProc, programId );
					}

					if( offerObj.has( "fulfillmentList" ) ) {
						JsonArray fulfillmentList = ( JsonArray ) offerObj.get( "fulfillmentList" ).getAsJsonArray();

						for (int i = 0; i < fulfillmentList.size(); i ++) {
							fullfillmentObj = fulfillmentList.get(i).getAsJsonObject();

							if( fullfillmentObj.has(  "optinKeyword" ) && ! fullfillmentObj.get( "optinKeyword" ).isJsonNull() && 
									fullfillmentObj.get( "optinKeyword" ).getAsString().trim().length() > 0 ) {
								String optinKeyword = fullfillmentObj.get( "optinKeyword" ).getAsString();
								cookOptInKeywordExpressions( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, fulfillmentProducts, optinKeyword, expProc, programId );
							}

							if( fullfillmentObj.has( "fulfillmentProducts" ) ) {
								fulfillmentProducts = fullfillmentObj.get( "fulfillmentProducts" ).getAsJsonArray();
								cookFulfillmentProductExpressions( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, fulfillmentProducts, allFunctions, expProc, programId );
							}

							if( fullfillmentObj.has( "fulfillmentMessageChannels" ) ) {
								fulfillmentMessages = fullfillmentObj.get( "fulfillmentMessageChannels" ).getAsJsonArray();
								cookFulfillmentMessages( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, fulfillmentMessages, allFunctions, expProc, programId );
							}

							if( fullfillmentObj.has( "fulfillmentConfirmationMessageChannels" ) ) {
								fulfillmentConfirmMessages = fullfillmentObj.get( "fulfillmentConfirmationMessageChannels" ).getAsJsonArray();
								cookFulfillmenConfirmationtMessages( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, fulfillmentConfirmMessages, allFunctions, expProc, programId );
							}
						}
					}

					if( offerObj.has( "reportParameter" ) ) {
						reportParameterObj = ( JsonObject ) offerObj.get( "reportParameter" );
						cookReportParameterExpressions( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, reportParameterObj, allFunctions, expProc, programId );
					}
					if( offerObj.has( "offerReportParameters" ) ) {
						JsonArray reportParameterArray = offerObj.get( "offerReportParameters" ).getAsJsonArray();
						cookDynamicReportParameterExpressions( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, reportParameterArray, allFunctions, expProc, programId );
					}
					if( offerObj.has( "participationType" ) ) {
						JsonElement eParticipationType = offerObj.get( "participationType" );
						if( eParticipationType != null && ! eParticipationType.isJsonNull() & ! eParticipationType.getAsString().equals( "" ) )
							cookParticipationTypeExpressions( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, eParticipationType.getAsString(), allFunctions, expProc, programId );
					}
					
					if( offerObj.has( "offerScore" ) ) {
						JsonElement eOfferScore = offerObj.get( "offerScore" );
						if( eOfferScore != null && ! eOfferScore.isJsonNull() & ! eOfferScore.getAsString().trim().equals( "" ) )
							cookOfferScoreExpressions( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, eOfferScore.getAsString(), allFunctions, expProc, programId );
					}
					
					if( offerObj.has( "maxFulfillmentsPerSubscriber" ) && ! offerObj.get( "maxFulfillmentsPerSubscriber" ).isJsonNull() ) {
						String id = "MAX_FULFILLMENTS_" + offerId;
						JsonObject thisExpObj = offerObj.get( "maxFulfillmentsPerSubscriber" ).getAsJsonObject();
						cookContactPolicyExpressions( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, id, 
								thisExpObj, allFunctions, expProc, programId );
					}
				}
			}
		}
		return fixedOfferParams;
	}


	private void cookOptInKeywordExpressions( LinkedHashMap< String, JsonObject > allAccums, LinkedHashMap< String, JsonObject > allEvents,
			HashMap< String, JsonObject > cimList, String offerId, JsonArray offerThresholdParametersArray, JsonArray fulfillmentProducts, String optinKeyword, ExpressionProcessor expProc, String programId  ) throws Exception {
		System.out.println( "cookOptInKeywordExpressions() : Start : offerId=" + offerId + " optInExpKeyword = "  + optinKeyword );
		JsonObject criteriaObjToCook = new JsonObject();
		String expressionId = "OFFER_OPTINKEYWORD_" + programId + "_" + offerId + "_" + optinKeyword.replace( " ", "_" ).replace( "\"", "" );		
		optinKeyword = replaceThresholdParameter(offerId, optinKeyword, offerThresholdParametersArray, programId);
		criteriaObjToCook.addProperty( "id", expressionId );
		criteriaObjToCook.addProperty( "expression", optinKeyword );
		criteriaObjToCook.addProperty( "expressionType", "java.lang.String" );
		PExpressionEvaluator exp = expProc.createByDefinition( criteriaObjToCook );
		if( exp != null ) 
			allExpressions.put( criteriaObjToCook.get( "id").getAsString(), exp );
		System.out.println( "cookOptInKeywordExpressions() : End" );		
	}


	/**
	 * @param allAccums
	 * @param allEvents
	 * @param cimList
	 * @param refresh
	 * @return
	 * @throws Exception
	 */
	public HashMap< String, String > cookAllOfferExpressions( LinkedHashMap< String, JsonObject > allAccums, LinkedHashMap< String, JsonObject > allEvents,
			java.util.LinkedHashMap< String, JsonObject > cimList, boolean refresh, 
			LinkedHashMap< String, JsonObject > allFunctions, ExpressionProcessor expProc ) throws Exception {

		OfferDAO offerDAO = new OfferDAO();
		java.util.LinkedHashMap< String, JsonObject > offerMap = offerDAO.read( refresh );
		Iterator< String > offerMapIter = offerMap.keySet().iterator();

		String offerId = null;
		JsonObject offerObj = null;
		JsonArray offerThresholdParametersArray = null;
		String offerCriteria = null;
		JsonArray offerMessages = null;
		HashMap< String, String > fixedOfferParams = new HashMap< String, String >();
		JsonObject reportParameterObj = null;

		while( offerMapIter.hasNext() ) {
			offerId = offerMapIter.next();
			offerObj = offerMap.get( offerId );
			System.out.println( "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& cookAllOfferExpressions : " +  offerId + "&&&&&&&&&&&&&&&&&&&&&&&&&&&" );

			if( offerObj != null ) {
				JsonElement programsEle = offerObj.get( "programs" );
				JsonArray programsArray = null;
				if( programsEle != null && ! programsEle.isJsonNull() )
					programsArray = programsEle.getAsJsonArray();

				if( programsArray == null )
					continue;

				for( int progs = 0; progs < programsArray.size(); progs ++ ) {
					String programId = programsArray.get( progs ).getAsString();

					if( offerObj.has( "offerThresholdParameters" ) ) {
						offerThresholdParametersArray = offerObj.get( "offerThresholdParameters" ).getAsJsonArray();
						cookOfferThresholdParameters( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, allFunctions, expProc, programId );
					}

					if( offerObj.has( "offerCriteria" ) ) {
						offerCriteria = offerObj.get( "offerCriteria" ).getAsString();
						cookOfferCriteriaExpressions( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, offerCriteria, allFunctions, expProc, programId );
					}

					if( offerObj.has( "offerChannels" ) ) {
						offerMessages = offerObj.get( "offerChannels" ).getAsJsonArray();
						cookOfferMessages( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, offerMessages, fixedOfferParams, allFunctions, expProc, programId );
					}

					if( offerObj.has( "reportParameter" ) ) {
						reportParameterObj = ( JsonObject ) offerObj.get( "reportParameter" );
						cookReportParameterExpressions( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, reportParameterObj, allFunctions, expProc, programId );
					}
				}
			}
		}
		return fixedOfferParams;
	}
	/**
	 * Cook Offer Threshold Parameters.
	 * 
	 * @param allAccums
	 * @param allEvents
	 * @param cimList
	 * @param offerId
	 * @param offerThresholdParametersArray
	 * @param allFunctions 
	 * @throws Exception
	 */
	private void cookOfferThresholdParameters( LinkedHashMap< String, JsonObject > allAccums, LinkedHashMap< String, JsonObject > allEvents,
			java.util.LinkedHashMap< String, JsonObject > cimList, String offerId, JsonArray offerThresholdParametersArray, 
			LinkedHashMap< String, JsonObject > allFunctions, ExpressionProcessor expProc, String programId  ) throws Exception {

		System.out.println( "cookOfferThresholdParameters() : Start" );

		JsonObject offerThresholdParameterObj = null;
		JsonObject offerThresholdParameterObjToCook = null;

		String key = null;
		String type = null;
		String value = null;
		String modifiedValue = null;
		String id = null;
		List<JsonObject> offerThresholdParametersToLater = new ArrayList<JsonObject>(1);

		for( int i = 0; i < offerThresholdParametersArray.size(); i++ ) {
			offerThresholdParameterObj = offerThresholdParametersArray.get( i ).getAsJsonObject();

			if( offerThresholdParameterObj != null ) {
				if( offerThresholdParameterObj.has( "key" ) ) {
					key = offerThresholdParameterObj.get( "key" ).getAsString();
					System.out.println( "Processing Offer Threshold Param : " + key );
				}
				if( offerThresholdParameterObj.has( "type" ) ) {
					type = offerThresholdParameterObj.get( "type" ).getAsString();
				}
				if( offerThresholdParameterObj.has( "value" ) ) {
					value = offerThresholdParameterObj.get( "value" ).getAsString();
					modifiedValue = replaceThresholdParameter(offerId, value, offerThresholdParametersArray, programId);
				}

				if( key != null && type != null && modifiedValue != null ) {
					id = "OFFER_THRESHOLD_PARAM_" + programId + "_" + offerId + '_' + key;
					System.out.println( "Processing Offer Threshold Param : " + key + " - " + id );
					offerThresholdParameterObjToCook = new JsonObject();
					offerThresholdParameterObjToCook.addProperty( "id", id );
					offerThresholdParameterObjToCook.addProperty( "expression", modifiedValue );
					offerThresholdParameterObjToCook.addProperty( "expressionType", type );

					System.out.println( "Modified Value : " + modifiedValue + " Value = " + value );

					if (modifiedValue.equals(value)) {
						PExpressionEvaluator thisExp = expProc.createByDefinition( offerThresholdParameterObjToCook);
						if( thisExp != null ) 
							allExpressions.put( id, thisExp );
					} else {
						offerThresholdParametersToLater.add(offerThresholdParameterObjToCook);
					}
				}
			}
		}

		for( int i = 0; i < offerThresholdParametersToLater.size(); i++ ) {
			JsonObject thisItem = offerThresholdParametersToLater.get( i );
			expProc.setExistingExpressions( allExpressions );
			PExpressionEvaluator thisExp = expProc.createByDefinition( thisItem );
			if( thisExp != null ) 
				allExpressions.put( thisItem.get( "id").getAsString(), thisExp );
		}

		System.out.println( "cookOfferThresholdParameters() : End" );
	}

	/**
	 * Cook Offer Criteria Expression.
	 * 
	 * @param allAccums
	 * @param allEvents
	 * @param cimList
	 * @param offerId
	 * @param offerThresholdParametersArray
	 * @param criteria
	 * @param type
	 * @throws Exception
	 */
	private void cookOfferCriteriaExpressions( LinkedHashMap< String, JsonObject > allAccums, LinkedHashMap< String, JsonObject > allEvents,
			java.util.LinkedHashMap< String, JsonObject > cimList, String offerId, JsonArray thresholdParametersArray, String criteria, 
			LinkedHashMap< String, JsonObject > allFunctions, ExpressionProcessor expProc, String programId  ) throws Exception {

		System.out.println( "cookCriteriaExpressions() : Start : offerId=" + offerId );

		JsonObject criteriaObjToCook = new JsonObject();

		String expressionId = "OFFER_SEGMENTATION_" + offerId;

		criteria = replaceThresholdParameter(offerId, criteria, thresholdParametersArray, programId);

		criteriaObjToCook.addProperty( "id", expressionId );
		criteriaObjToCook.addProperty( "expression", criteria );
		criteriaObjToCook.addProperty( "expressionType", "boolean" );

		PExpressionEvaluator thisExp = expProc.createByDefinition( criteriaObjToCook );
		if( thisExp != null ) 
			allExpressions.put( expressionId, thisExp );

		System.out.println( "cookCriteriaExpressions() : End" );
	}
	
	
	
	private void cookOfferCriteriaExpressions( LinkedHashMap< String, JsonObject > allAccums, LinkedHashMap< String, JsonObject > allEvents,
			java.util.LinkedHashMap< String, JsonObject > cimList, String offerId, JsonArray thresholdParametersArray, JsonArray criteriaArray, 
			LinkedHashMap< String, JsonObject > allFunctions, ExpressionProcessor expProc, String programId  ) throws Exception {

		System.out.println( "cookCriteriaArrayxpressions() : Start : offerId=" + offerId );
		for( int i=0; i<criteriaArray.size(); i++ ) {
    		JsonObject criteriaObjToCook = new JsonObject();
    		String expressionId = "OFFER_SEGMENTATION_" + offerId + "_" + i;
    		String criteria = criteriaArray.get( i ).getAsString();
    		criteria = replaceThresholdParameter(offerId, criteria, thresholdParametersArray, programId);
    
    		criteriaObjToCook.addProperty( "id", expressionId );
    		criteriaObjToCook.addProperty( "expression", criteria );
    		criteriaObjToCook.addProperty( "expressionType", "boolean" );
    
    		PExpressionEvaluator thisExp = expProc.createByDefinition( criteriaObjToCook );
    		if( thisExp != null ) 
    			allExpressions.put( expressionId, thisExp );
		}
		System.out.println( "cookCriteriaExpressions() : End" );
	}
	

	/**
	 * Cook Report Parameters.
	 * 
	 * @param allAccums
	 * @param allEvents
	 * @param cimList
	 * @param offerId
	 * @param offerThresholdParametersArray
	 * @param allFunctions 
	 * @param fulfillmentProducts
	 * @throws Exception
	 */
	private void cookReportParameterExpressions( LinkedHashMap< String, JsonObject > allAccums, LinkedHashMap< String, JsonObject > allEvents,
			java.util.LinkedHashMap< String, JsonObject > cimList, String offerId, JsonArray offerThresholdParametersArray, JsonObject reportParameterObj, 
			LinkedHashMap< String, JsonObject > allFunctions, ExpressionProcessor expProc, String programId  ) throws Exception {

		System.out.println( "cookReportParameterExpressions() : Start : offerId=" + offerId );

		String id = null;

		JsonObject monitoredValueObj = null;
		if( reportParameterObj.has( "monitoredValue" ) ) {
			monitoredValueObj = ( JsonObject ) reportParameterObj.get( "monitoredValue" ).getAsJsonObject();		
			id = "REPORT_PARAM_" + offerId + "_MONITORED_VALUE";
			cookDynamicParameterObj(allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, id, monitoredValueObj, allFunctions, expProc, programId );
		}

		JsonObject fulfillmentValueObj = null;
		if( reportParameterObj.has( "fulfillmentValue" ) ) {
			fulfillmentValueObj = ( JsonObject ) reportParameterObj.get( "fulfillmentValue" ).getAsJsonObject();
			id = "REPORT_PARAM_" + offerId + "_FULFILLMENT_VALUE";
			cookDynamicParameterObj(allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, id, fulfillmentValueObj, allFunctions, expProc, programId );
		}

		JsonObject rewardValueInDollarUnitsObj = null;
		if( reportParameterObj.has( "rewardValueInDollarUnits" ) ) {
			rewardValueInDollarUnitsObj = ( JsonObject ) reportParameterObj.get( "rewardValueInDollarUnits" ).getAsJsonObject();
			id = "REPORT_PARAM_" + offerId + "_REWARD_VALUE_CURRENCY";
			cookDynamicParameterObj(allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, id, rewardValueInDollarUnitsObj, allFunctions, expProc, programId  );
		}

		JsonObject rewardValueInActualUnitsObj = null;
		if( reportParameterObj.has( "rewardValueInActualUnits" ) ) {
			rewardValueInActualUnitsObj = ( JsonObject ) reportParameterObj.get( "rewardValueInActualUnits" ).getAsJsonObject();
			id = "REPORT_PARAM_" + offerId + "_REWARD_VALUE_ACTUAL";
			cookDynamicParameterObj(allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, id, rewardValueInActualUnitsObj, allFunctions, expProc, programId );
		}

		JsonObject activityChannelObj = null;
		if( reportParameterObj.has( "activityChannel" ) ) {
			activityChannelObj = ( JsonObject ) reportParameterObj.get( "activityChannel" ).getAsJsonObject();
			id = "REPORT_PARAM_" + offerId + "_ACTIVITY_CHANNEL";
			cookDynamicParameterObj(allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, id, activityChannelObj, allFunctions, expProc, programId );
		}

		System.out.println( "cookReportParameterExpressions() : End" );
	}

	private void cookParticipationTypeExpressions( LinkedHashMap< String, JsonObject > allAccums, LinkedHashMap< String, JsonObject > allEvents,
			java.util.LinkedHashMap< String, JsonObject > cimList, String offerId, JsonArray thresholdParametersArray, String participationTypeExp, 
			LinkedHashMap< String, JsonObject > allFunctions, ExpressionProcessor expProc, String programId  ) throws Exception {

		System.out.println( "cookParticipationTypeExpressions() : Start : offerId=" + offerId );
		JsonObject ParticipationTypeObjToCook = new JsonObject();
		String expressionId = "PARTICIPATION_TYPE_" + offerId;

		participationTypeExp = replaceThresholdParameter(offerId, participationTypeExp, thresholdParametersArray, programId);

		ParticipationTypeObjToCook.addProperty( "id", expressionId );
		ParticipationTypeObjToCook.addProperty( "expression", participationTypeExp );
		ParticipationTypeObjToCook.addProperty( "expressionType", "java.lang.String" );

		PExpressionEvaluator thisExp = expProc.createByDefinition( ParticipationTypeObjToCook );
		if( thisExp != null ) 
			allExpressions.put( expressionId, thisExp );

		System.out.println( "cookParticipationTypeExpressions() : End" );
	}
	
	private void cookOfferScoreExpressions( LinkedHashMap< String, JsonObject > allAccums, LinkedHashMap< String, JsonObject > allEvents,
			java.util.LinkedHashMap< String, JsonObject > cimList, String offerId, JsonArray thresholdParametersArray, String offerScoreExp, 
			LinkedHashMap< String, JsonObject > allFunctions, ExpressionProcessor expProc, String programId  ) throws Exception {

		System.out.println( "cookOfferScoreExpressions() : Start : offerId=" + offerId );
		JsonObject offerScoreObjectToCook = new JsonObject();
		String expressionId = "OFFER_SCORE_" + offerId;

		offerScoreExp = replaceThresholdParameter(offerId, offerScoreExp, thresholdParametersArray, programId);

		offerScoreObjectToCook.addProperty( "id", expressionId );
		offerScoreObjectToCook.addProperty( "expression", offerScoreExp );
		offerScoreObjectToCook.addProperty( "expressionType", "double" );

		PExpressionEvaluator thisExp = expProc.createByDefinition( offerScoreObjectToCook );
		if( thisExp != null ) 
			allExpressions.put( expressionId, thisExp );

		System.out.println( "cookOfferScoreExpressions() : End" );
	}

	/**
	 * Cook Report Parameters.
	 * 
	 * @param allAccums
	 * @param allEvents
	 * @param cimList
	 * @param offerId
	 * @param offerThresholdParametersArray
	 * @param allFunctions 
	 * @param fulfillmentProducts
	 * @throws Exception
	 */
	private void cookDynamicReportParameterExpressions( LinkedHashMap< String, JsonObject > allAccums, LinkedHashMap< String, JsonObject > allEvents,
			java.util.LinkedHashMap< String, JsonObject > cimList, String offerId, JsonArray offerThresholdParametersArray, JsonArray reportParameterArray, 
			LinkedHashMap< String, JsonObject > allFunctions, ExpressionProcessor expProc, String programId  ) throws Exception {

		System.out.println( "cookDynamicReportParameterExpressions() : Start : offerId=" + offerId );

		String id = null;

		for( int i=0; i<reportParameterArray.size(); i++ ) {
			JsonObject reportParamObj = reportParameterArray.get( i  ).getAsJsonObject();
			id = "REPORT_PARAM_" + offerId + "_" + reportParamObj.get( "key" ).getAsString();
			System.out.println( "Cooking Report Expression : " + id );
			cookDynamicParameterObj(allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, id, reportParamObj, allFunctions, expProc, programId );
		}
		System.out.println( "cookDynamicReportParameterExpressions() : End" );
	}

	/**
	 * @param allAccums
	 * @param allEvents
	 * @param cimList
	 * @param offerId
	 * @param offerThresholdParametersArray
	 * @param dynamicParameterObj
	 * @throws Exception
	 */
	private void cookDynamicParameterObj(LinkedHashMap< String, JsonObject > allAccums, LinkedHashMap< String, JsonObject > allEvents,
			java.util.LinkedHashMap< String, JsonObject > cimList, String offerId, JsonArray offerThresholdParametersArray, String id, JsonObject dynamicParameterObj, 
			LinkedHashMap< String, JsonObject > allFunctions, ExpressionProcessor expProc, String programId  ) throws Exception {

		String value = null;
		String type = null;
		JsonObject reportParameterObjToCook = new JsonObject();

		if (dynamicParameterObj.has("value")) {
			value = dynamicParameterObj.get("value").getAsString();
			type = dynamicParameterObj.get("type").getAsString();

			if (value != null && !"".equals(value)) {
				value = replaceThresholdParameter(offerId, value, offerThresholdParametersArray, programId);

				reportParameterObjToCook.addProperty( "id", id );
				reportParameterObjToCook.addProperty( "expression", value );
				reportParameterObjToCook.addProperty( "expressionType", type );

				PExpressionEvaluator exp = expProc.createByDefinition( reportParameterObjToCook );
				if( exp != null ) 
					allExpressions.put(  id, exp );
			}
		}
	}


	/**
	 * Cook Offer Message Expressions.
	 * 
	 * @param allAccums
	 * @param allEvents
	 * @param cimList
	 * @param offerId
	 * @param offerThresholdParametersArray
	 * @param fixedOfferParams 
	 * @param allFunctions 
	 * @param offerCriteria
	 * @throws Exception
	 */
	private void cookOfferMessages( LinkedHashMap< String, JsonObject > allAccums, LinkedHashMap< String, JsonObject > allEvents,
			java.util.LinkedHashMap< String, JsonObject > cimList, String offerId, JsonArray offerThresholdParametersArray, JsonArray offerMessages, 
			HashMap< String, String > fixedOfferParams, LinkedHashMap< String, JsonObject > allFunctions, ExpressionProcessor expProc, String programId  ) throws Exception {

		System.out.println( "cookOfferMessages() : Start : offerId=" + offerId );

		JsonObject offerChannelObj = null;
		JsonObject offerChannelObjToCook = null;
		String channelName = null;
		String lang = null;
		String text = null;
		JsonArray messagesArray = null;
		JsonObject messageObj = null;
		String id = null;
		JsonObject offerThresholdParameterObj = null;
		String key = null;
		String replacementKey = null;

		for( int i = 0; i < offerMessages.size(); i++ ) {
			offerChannelObj = offerMessages.get( i ).getAsJsonObject();

			if( offerChannelObj != null ) {

				if( offerChannelObj.has( "channelName" ) ) {
					channelName = offerChannelObj.get( "channelName" ).getAsString();
				}

				if( offerChannelObj.has( "messages" ) ) {
					messagesArray = offerChannelObj.get( "messages" ).getAsJsonArray();

					for( int j = 0; j < messagesArray.size(); j++ ) {
						messageObj = messagesArray.get( j ).getAsJsonObject();

						lang = messageObj.get( "lang" ).getAsString();
						text = messageObj.get( "text" ).getAsString();

						id = "OFFER_MESSAGE_" + offerId + "_" + channelName + "_" + lang;

						for( int k = 0; k < offerThresholdParametersArray.size(); k++ ) {
							offerThresholdParameterObj = offerThresholdParametersArray.get( k ).getAsJsonObject();

							if( offerThresholdParameterObj != null ) {
								if( offerThresholdParameterObj.has( "key" ) ) {
									key = offerThresholdParameterObj.get( "key" ).getAsString();

									if( text.indexOf( key ) != -1 ) {
										replacementKey = "OFFER_THRESHOLD_PARAM_" + programId + "_" + offerId + '_' + key;
										text = text.replace( key, replacementKey );
										fixedOfferParams.put( replacementKey, replacementKey );
									}
								}
							}
						}
						offerChannelObjToCook = new JsonObject();
						offerChannelObjToCook.addProperty( "id", id );
						offerChannelObjToCook.addProperty( "expression", text );
						offerChannelObjToCook.addProperty( "expressionType", "java.lang.String" );

						PExpressionEvaluator exp = expProc.createByDefinition( offerChannelObjToCook );
						if( exp != null )
							allExpressions.put(  id, exp );

					}
				}
			}
		}
	}

	/**
	 * Cook Fulfillment Message Expressions.
	 * 
	 * @param allAccums
	 * @param allEvents
	 * @param cimList
	 * @param offerId
	 * @param triggerId
	 * @param offerThresholdParametersArray
	 * @param fulfillmentMessages
	 * @throws Exception
	 */
	private void cookFulfillmentMessages( LinkedHashMap< String, JsonObject > allAccums, 
			LinkedHashMap< String, JsonObject > allEvents,
			java.util.LinkedHashMap< String, JsonObject > cimList, 
			String offerId, 
			JsonArray offerThresholdParametersArray, 
			JsonArray fulfillmentMessages,
			LinkedHashMap< String, JsonObject > allFunctions, ExpressionProcessor expProc,
			String programId ) throws Exception {

		System.out.println( "cookFulfillmentMessages() : Start : offerId=" + offerId );

		JsonObject channelObj = null;
		JsonObject channelObjToCook = null;
		String channelName = null;
		String lang = null;
		String text = null;
		JsonArray messagesArray = null;
		JsonObject messageObj = null;
		String id = null;
		JsonObject offerThresholdParameterObj = null;
		String key = null;
		String replacementKey = null;

		for( int i = 0; i < fulfillmentMessages.size(); i++ ) {
			channelObj = fulfillmentMessages.get( i ).getAsJsonObject();

			if( channelObj != null ) {

				if( channelObj.has( "channelName" ) ) {
					channelName = channelObj.get( "channelName" ).getAsString();
				}

				if( channelObj.has( "messages" ) ) {
					messagesArray = channelObj.get( "messages" ).getAsJsonArray();

					for( int j = 0; j < messagesArray.size(); j++ ) {
						messageObj = messagesArray.get( j ).getAsJsonObject();

						lang = messageObj.get( "lang" ).getAsString();
						text = messageObj.get( "text" ).getAsString();

						id = "FULFILLMENT_MESSAGE_" + offerId + "_" + channelName + "_" + lang;

						for( int k = 0; k < offerThresholdParametersArray.size(); k++ ) {
							offerThresholdParameterObj = offerThresholdParametersArray.get( k ).getAsJsonObject();

							if( offerThresholdParameterObj != null ) {
								if( offerThresholdParameterObj.has( "key" ) ) {
									key = offerThresholdParameterObj.get( "key" ).getAsString();
									if( text.indexOf( key ) != -1 ) {
										replacementKey = "OFFER_THRESHOLD_PARAM_" + programId + "_" + offerId + '_' + key;
										text = text.replace( key, replacementKey );
									}
								}
							}
						}

						channelObjToCook = new JsonObject();
						channelObjToCook.addProperty( "id", id );
						channelObjToCook.addProperty( "expression", text );
						channelObjToCook.addProperty( "expressionType", "java.lang.String" );

						PExpressionEvaluator exp = expProc.createByDefinition( channelObjToCook );
						if( exp != null )
							allExpressions.put(  id, exp );
					}
				}
			}
		}
	}


	/**
	 * Cook Fulfillment Confirmation Message Expressions.
	 * 
	 * @param allAccums
	 * @param allEvents
	 * @param cimList
	 * @param offerId
	 * @param offerThresholdParametersArray
	 * @param fulfillmentMessages
	 * @throws Exception
	 */
	private void cookFulfillmenConfirmationtMessages( LinkedHashMap< String, JsonObject > allAccums, 
			LinkedHashMap< String, JsonObject > allEvents,
			java.util.LinkedHashMap< String, JsonObject > cimList, 
			String offerId, 
			JsonArray offerThresholdParametersArray, 
			JsonArray fulfillmentConfMessages,
			LinkedHashMap< String, JsonObject > allFunctions, ExpressionProcessor expProc, String programId  ) throws Exception {

		System.out.println( "cookFulfillmenConfirmationtMessages() : Start : offerId=" + offerId );

		JsonObject channelObj = null;
		JsonObject channelObjToCook = null;
		String channelName = null;
		String lang = null;
		String text = null;
		JsonArray messagesArray = null;
		JsonObject messageObj = null;
		String id = null;
		JsonObject offerThresholdParameterObj = null;
		String key = null;
		String replacementKey = null;

		for( int i = 0; i < fulfillmentConfMessages.size(); i++ ) {
			channelObj = fulfillmentConfMessages.get( i ).getAsJsonObject();

			if( channelObj != null ) {

				if( channelObj.has( "channelName" ) ) {
					channelName = channelObj.get( "channelName" ).getAsString();
				}

				if( channelObj.has( "messages" ) ) {
					messagesArray = channelObj.get( "messages" ).getAsJsonArray();

					for( int j = 0; j < messagesArray.size(); j++ ) {
						messageObj = messagesArray.get( j ).getAsJsonObject();

						lang = messageObj.get( "lang" ).getAsString();
						text = messageObj.get( "text" ).getAsString();

						id = "FULFILLMENT_CONFIRMATION_MESSAGE_" + offerId + "_" + channelName + "_" + lang;

						for( int k = 0; k < offerThresholdParametersArray.size(); k++ ) {
							offerThresholdParameterObj = offerThresholdParametersArray.get( k ).getAsJsonObject();

							if( offerThresholdParameterObj != null ) {
								if( offerThresholdParameterObj.has( "key" ) ) {
									key = offerThresholdParameterObj.get( "key" ).getAsString();
									if( text.indexOf( key ) != -1 ) {
										replacementKey = "OFFER_THRESHOLD_PARAM_" + programId + "_" + offerId + '_' + key;
										text = text.replace( key, replacementKey );
									}
								}
							}
						}

						channelObjToCook = new JsonObject();
						channelObjToCook.addProperty( "id", id );
						channelObjToCook.addProperty( "expression", text );
						channelObjToCook.addProperty( "expressionType", "java.lang.String" );

						PExpressionEvaluator exp = expProc.createByDefinition( channelObjToCook );
						if( exp != null )
							allExpressions.put(  id, exp );
					}
				}
			}
		}
	}


	/**
	 * Cook Fulfillment Products.
	 * 
	 * @param allAccums
	 * @param allEvents
	 * @param cimList
	 * @param offerId
	 * @param offerThresholdParametersArray
	 * @param fulfillmentProducts
	 * @throws Exception
	 */
	private void cookFulfillmentProductExpressions( LinkedHashMap< String, JsonObject > allAccums, 
			LinkedHashMap< String, JsonObject > allEvents,
			java.util.LinkedHashMap< String, JsonObject > cimList, 
			String offerId, 
			JsonArray offerThresholdParametersArray, 
			JsonArray fulfillmentProducts,
			LinkedHashMap< String, JsonObject > allFunctions, ExpressionProcessor expProc,
			String programId ) throws Exception {

		System.out.println( "cookFulfillmentProductExpressions() : Start : offerId=" + offerId );

		JsonObject fulfillmentProductObjToCook = new JsonObject();

		String id = null;
		JsonObject fulfillmentProductObj = null;
		JsonArray dynamicParametersArray = null;
		JsonObject dynamicParameterObj = null;
		String key = null;
		String value = null;
		String type = null;
		String productId = null;
		String productCriteria = null;

		for (int i = 0; i < fulfillmentProducts.size(); i ++) {
			fulfillmentProductObj = fulfillmentProducts.get(i).getAsJsonObject();

			productId = fulfillmentProductObj.get("productId").getAsString();

			if (fulfillmentProductObj.has("productCriteria") && ! fulfillmentProductObj.get("productCriteria").isJsonNull()) {
				productCriteria = fulfillmentProductObj.get("productCriteria").getAsString();

				id = "FULFILLMENT_PRODUCT_CRITERIA_" + offerId + "_" + productId + "_" + i;

				String modifiedCriteria = replaceThresholdParameter(offerId, productCriteria, offerThresholdParametersArray, programId );

				fulfillmentProductObjToCook.addProperty( "id", id );
				fulfillmentProductObjToCook.addProperty( "expression", modifiedCriteria );
				fulfillmentProductObjToCook.addProperty( "expressionType", "boolean" );

				PExpressionEvaluator exp = expProc.createByDefinition( fulfillmentProductObjToCook );
				if( exp != null )
					allExpressions.put(  id, exp );

			}

			if (fulfillmentProductObj.has("dynamicParameters")) {
				dynamicParametersArray = fulfillmentProductObj.get("dynamicParameters").getAsJsonArray();

				for (int j = 0; j < dynamicParametersArray.size(); j ++) {
					dynamicParameterObj = dynamicParametersArray.get(j).getAsJsonObject();

					if (dynamicParameterObj.has("key")) {
						key = dynamicParameterObj.get("key").getAsString();

						if (dynamicParameterObj.has("value")) {
							value = dynamicParameterObj.get("value").getAsString();
							type = dynamicParameterObj.get("type").getAsString();

							if (value != null && !"".equals(value)) {
								value = replaceThresholdParameter(offerId, value, offerThresholdParametersArray, programId);

								id = "FULFILLMENT_PRODUCT_" + offerId + "_" + productId + "_" + i + "_" + key;

								fulfillmentProductObjToCook.addProperty( "id", id );
								fulfillmentProductObjToCook.addProperty( "expression", value );
								fulfillmentProductObjToCook.addProperty( "expressionType", type );

								PExpressionEvaluator exp = expProc.createByDefinition( fulfillmentProductObjToCook );
								if( exp != null )
									allExpressions.put(  id, exp );
							}
						}
					}
				}
			}
		}

		System.out.println( "cookFulfillmentProductExpressions() : End" );
	}

	/**
	 * Cook All reminder messages.
	 * 
	 * @param allAccums
	 * @param allEvents
	 * @param cimList
	 * @throws Exception
	 */
	public void cookExpressionsFromProgram( LinkedHashMap< String, JsonObject > allAccums, LinkedHashMap< String, JsonObject > allEvents,
			LinkedHashMap< String, JsonObject > cimList, LinkedHashMap< String, JsonObject > allFunctions ) throws Exception {

		ExpressionProcessor expProc = new ExpressionProcessor( allAccums, allEvents, allFunctions, cimList );
		expProc.setExistingExpressions( this.allExpressions );

		System.out.println("cookExpressionsFromProgram() : Start.");

		ProgramsDAO programsDAO = new ProgramsDAO();

		java.util.LinkedHashMap< String, JsonObject > programsMap = programsDAO.readActive();

		JsonArray triggerActionsArray = null;
		Iterator< String > programsMapIter = programsMap.keySet().iterator();
		String programId = null;
		JsonObject programObj = null;
		JsonObject programActionsObj = null;
		Iterator< Entry< String, JsonElement >> programActionsObjIter = null;
		String triggerId = null;

		while( programsMapIter.hasNext() ) {
			programId = programsMapIter.next();
			programObj = programsMap.get( programId ).getAsJsonObject();
			
			if( programObj != null ) {
				if( programObj.has( "programActions" ) ) {
					programActionsObj = programObj.get( "programActions" ).getAsJsonObject();

					programActionsObjIter = programActionsObj.entrySet().iterator();

					while( programActionsObjIter.hasNext() ) {
						Entry< String, JsonElement > thisEntry = programActionsObjIter.next();
						triggerId = thisEntry.getKey();
						triggerActionsArray = thisEntry.getValue().getAsJsonArray();
						System.out.println( "Cooking Trigger Action : " + triggerId + " - " + triggerActionsArray.toString() );

						for( int i = 0; i < triggerActionsArray.size(); i++ ) {
							JsonObject triggerActionObj = triggerActionsArray.get( i ).getAsJsonObject();
							if( triggerActionObj.has("actionType" ) )
                                cookTriggerAction( triggerActionObj, allAccums, allEvents, cimList, allFunctions, expProc, programId, triggerId );
							else {
								JsonObject jNextBestOfferCandidates = triggerActionObj;
								Iterator< Entry< String, JsonElement >> itrNBA = jNextBestOfferCandidates.entrySet().iterator();
								while( itrNBA.hasNext() ) {
									Entry< String, JsonElement > entry = itrNBA.next();
									JsonObject jNBAAction = entry.getValue().getAsJsonObject();
									cookTriggerAction( jNBAAction, allAccums, allEvents, cimList, allFunctions, expProc, programId, triggerId );
									System.out.println( "^^^^^^^^^^^^^^^^Cooking  NBA Action ^^^^^^^^^^^ " + entry.getKey() );
								}
							}
						}
					}
				}
				
				System.out.println( "Cooking Program Contact Rules.." );
				if( programObj.has( "programContactRule" ) && ! programObj.get( "programContactRule" ).isJsonNull() ) {
					JsonObject thisProgramContactRule = programObj.get( "programContactRule" ).getAsJsonObject();
					if( thisProgramContactRule.has( "offerExpressionMap" ) && ! thisProgramContactRule.get( "offerExpressionMap" ).isJsonNull() ) {
						JsonObject offerExpMapObj = thisProgramContactRule.get( "offerExpressionMap" ).getAsJsonObject();
						Iterator< Entry< String, JsonElement > > offersIter = offerExpMapObj.entrySet().iterator();
						while( offersIter.hasNext() ) {
							Entry< String, JsonElement > entry = offersIter.next();
							String offerId = entry.getKey();
							JsonObject thisOfferObj = entry.getValue().getAsJsonObject();
							if( thisOfferObj.has( "Controlgroup") ) {
								JsonObject controlGroupObj = thisOfferObj.get( "Controlgroup" ).getAsJsonObject();
								JsonElement thisControlGrpExp = controlGroupObj.get( "expression" );
								if( thisControlGrpExp != null && ! thisControlGrpExp.isJsonNull() && thisControlGrpExp.getAsString().trim().length() != 0 ) {
									String id = programId + "_" + offerId + "_CONTROL_GROUP_EXP";
									controlGroupObj.add( "id", new JsonPrimitive( id ) );
									controlGroupObj.add( "expressionType", new JsonPrimitive( controlGroupObj.get( "type" ).getAsString() ) );
									System.out.println( "Compiling : " + id + " -- > " + controlGroupObj.get( "expression" ) );
									PExpressionEvaluator thisExp = expProc.createByDefinition( controlGroupObj );
									if( thisExp != null )
										allExpressions.put( id, thisExp );
								}
							}
							if( thisOfferObj.has( "Score") ) {
								JsonObject scoreObj = thisOfferObj.get( "Score" ).getAsJsonObject();
								JsonElement thisScoreExp = scoreObj.get( "expression" );
								if( thisScoreExp != null && ! thisScoreExp.isJsonNull() && thisScoreExp.getAsString().trim().length() != 0 ) {
									String id = programId + "_" + offerId + "_OFFER_PRIORITY_SCORE_EXP";
									scoreObj.add( "id", new JsonPrimitive( id ) );
									scoreObj.add( "expressionType", new JsonPrimitive( "double" ) );
									System.out.println( "Compiling : " + id + " -- > " + scoreObj.get( "expression" ) );
									PExpressionEvaluator thisExp = expProc.createByDefinition( scoreObj );
									if( thisExp != null )
										allExpressions.put( id, thisExp );
								}
							}
						}
					}
				}
			}
		}
	}
	
	
	
	private void cookTriggerAction( JsonObject triggerActionObj, LinkedHashMap< String, JsonObject > allAccums, LinkedHashMap< String, JsonObject > allEvents,
			LinkedHashMap< String, JsonObject > cimList, LinkedHashMap< String, JsonObject > allFunctions, ExpressionProcessor expProc, 
			String programId, String triggerId ) throws Exception {
		
		String criteriaExpression = null;
		String offerId = null;
		JsonArray offerThresholdParametersArray = null;
		JsonArray reminderMessagesArray = null;
		String actionType = null;
		String id = null;
		JsonObject contactPolicyExpressionObj = null;
		JsonObject fulfillmentTrigger = null;

		actionType = triggerActionObj.get("actionType").getAsString();
		
		// Compile the contactCriteria expression in Monitoring triggers for next wave offers.
		if( triggerActionObj.has( "nextwave" ) && ! triggerActionObj.get( "nextwave" ).isJsonNull() ) {
			JsonArray nextWaveArray = triggerActionObj.get( "nextwave" ).getAsJsonArray();
			for( int i = 0; i < nextWaveArray.size(); i ++ ) {
				JsonObject thisWaveObject = nextWaveArray.get( i ).getAsJsonObject();
				if( ! thisWaveObject.has( "contactCriteria" ) || thisWaveObject.get( "contactCriteria" ).isJsonNull() )
					continue;
				
				JsonObject thisContactCriteria = thisWaveObject.get( "contactCriteria" ).getAsJsonObject();
				if( ! thisContactCriteria.has( "expression" ) || thisContactCriteria.get( "expression" ).isJsonNull() )
					continue;
				
				String thisContactCriteriaExpression = thisContactCriteria.get( "expression" ).getAsString();
				if( thisContactCriteriaExpression.trim().length() == 0 )
					continue;
				
				id = "NEXTWAVE_CONTACT_TIME_EXP_" + programId + "_" + thisWaveObject.get( "triggerId" ).getAsString();
				monitoringContactCriteriaExpressions( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, id, thisContactCriteriaExpression, allFunctions, expProc, programId );
			}
		}
		
		if( actionType.equals( "nextBestDynamicAction" ) ) {
			if( triggerActionObj.get( "nextBestActionDecisionCriteria" ).isJsonArray() ) {
				JsonArray allOfferDecisionCriterias = triggerActionObj.get( "nextBestActionDecisionCriteria" ).getAsJsonArray();
				for( int i=0; i<allOfferDecisionCriterias.size(); i++ ) {
					JsonArray thisCriteriaArray = allOfferDecisionCriterias.get(i).getAsJsonArray();
					for( int j=0; j<thisCriteriaArray.size(); j++ ) {
						JsonObject aCriteria = thisCriteriaArray.get(j).getAsJsonObject();
						cookOfferDecisionCriteria(expProc, aCriteria, 
							triggerActionObj.get("nextBestActionTriggerId").getAsString() + "_" + i + "_" + j + "_0"   );
					}
				}
			} 
			return;
		} else if( actionType.equals( "nextBesAction" ) ) {
			String nextBestActionId = triggerActionObj.get( "nextBestActionId" ).getAsString();
			String expressionId = programId + "_" + nextBestActionId;
			String expression = triggerActionObj.get( "nextBestActionCriteria" ).getAsString();
			cookNextBestActionCriteria( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, expressionId, 
						expression, allFunctions, expProc, programId );
			return;
		} else if( actionType.equals( "offerDecisionAction" ) ) {
			JsonArray allOfferDecisionCriterias = triggerActionObj.get("offerDecisionCriteria").getAsJsonArray();
			for( int i=0; i<allOfferDecisionCriterias.size(); i++ ) {
				JsonArray thisCriteriaArray = allOfferDecisionCriterias.get(i).getAsJsonArray();
				for( int j=0; j<thisCriteriaArray.size(); j++ ) {
					JsonObject aCriteria = thisCriteriaArray.get(j).getAsJsonObject();
					cookOfferDecisionCriteria(expProc, aCriteria, 
						triggerActionObj.get("offerDecisionTriggerId").getAsString() + "_" + i + "_" + j + "_0"   );
				}
			}
			return;
		}
		
		
		if( actionType.equals( "scheduledTriggerAction" ) )
			return;
		
		offerId = triggerActionObj.get( "offerId" ).getAsString();

		if( triggerActionObj.has( "offerThresholdParameters" ) ) {
			if( triggerActionObj.get( "offerThresholdParameters" ) == null || triggerActionObj.get( "offerThresholdParameters" ).isJsonNull() )
				offerThresholdParametersArray = new JsonArray();
			else
			    offerThresholdParametersArray = triggerActionObj.get( "offerThresholdParameters" ).getAsJsonArray();
		}
		
		if( triggerActionObj.has( "isCountAsContact" ) && triggerActionObj.get( "isCountAsContact" ) != null && 
				! triggerActionObj.get( "isCountAsContact" ).isJsonNull() ) {
				String expId = programId + "_" + offerId + "_" + "COUNT_AS_CONTACT";
				if( triggerActionObj.get( "isCountAsContact" ).getAsString().equals("Y") )
					System.out.println( "Gotcha");
				cookCountAsContactExpressions( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, expId,
						triggerActionObj.get( "isCountAsContact" ).getAsString(),
						allFunctions, expProc, programId );
		}
		
		

		if( triggerActionObj.has( "offerCriteriaArray" ) &&  ! triggerActionObj.get( "offerCriteriaArray" ).isJsonNull() ) {
			JsonArray offerCriteriaArray = triggerActionObj.get( "offerCriteriaArray" ).getAsJsonArray();
			cookOfferCriteriaExpressions( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, offerCriteriaArray,
					allFunctions, expProc, programId );
		}
		
		if( triggerActionObj.has( "controlGroupCriteria" ) && ! triggerActionObj.get( "controlGroupCriteria" ).isJsonNull() ) {
			id = "CONTROL_GROUP_CRITERIA_" + offerId;

			if ( ! allExpressions.containsKey( id ) ) {
				JsonElement jeCG = triggerActionObj.get( "controlGroupCriteria" );
				String expression = "";
				if( jeCG != null && ! jeCG.isJsonNull() ) {
					if( jeCG.isJsonObject() )
						expression = jeCG.getAsJsonObject().get( "expression" ).getAsString();
					else
						expression = triggerActionObj.get( "controlGroupCriteria" ).getAsString();
					cookControlGroupExpressions( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, id, 
							expression, allFunctions, expProc, programId );
				}	
			}
		}

		if( triggerActionObj.has( "maxContactAcrossSubscribers" ) && ! triggerActionObj.get( "maxContactAcrossSubscribers" ).isJsonNull() ) {
			id = "MAX_CONTACT_ACROSS_SUBSCRIBERS_" + offerId;

			//								if ( !allExpressions.containsKey( id ) ) {
			contactPolicyExpressionObj = triggerActionObj.get( "maxContactAcrossSubscribers" ).getAsJsonObject();
			cookContactPolicyExpressions( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, id, 
					contactPolicyExpressionObj, allFunctions, expProc, programId );
			//								}
		}

		if( triggerActionObj.has( "maxContactPerSubscriber" ) && ! triggerActionObj.get( "maxContactPerSubscriber" ).isJsonNull() ) {
			id = "MAX_CONTACT_PER_SUBSCRIBER_" + offerId;

			//								if ( !allExpressions.containsKey( id ) ) {
			contactPolicyExpressionObj = triggerActionObj.get( "maxContactPerSubscriber" ).getAsJsonObject();
			cookContactPolicyExpressions( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, 
					id, contactPolicyExpressionObj, allFunctions, expProc, programId );
			//								}
		}

		if( triggerActionObj.has( "cooldownPeriodForOfferPerSubscriber" ) && ! triggerActionObj.get( "cooldownPeriodForOfferPerSubscriber" ).isJsonNull() ) {
			id = "COOLDOWN_PERIOD_FOR_OFFER_PER_SUBSCRIBER_" + offerId;

			//								if ( !allExpressions.containsKey( id ) ) {
			contactPolicyExpressionObj = triggerActionObj.get( "cooldownPeriodForOfferPerSubscriber" ).getAsJsonObject();
			cookContactPolicyExpressions( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, 
					id, contactPolicyExpressionObj, allFunctions, expProc, programId );
			//								}
		}
		
		if ( "monitoringAction".equals( actionType ) ) {
			if( triggerActionObj.has( "fulfillmentTrigger" ) ) {
				fulfillmentTrigger = triggerActionObj.get("fulfillmentTrigger").getAsJsonObject();

				cookMonitoringDurationExpression( allAccums, 
						allEvents,
						cimList, 
						offerId, 
						offerThresholdParametersArray, 
						fulfillmentTrigger,
						allFunctions, expProc, programId );
			}
		} else if ( "fulfillmentAction".equals( actionType ) ) {
			if( triggerActionObj.has( "fulfillmentCriteria" )  ) {
				JsonElement eFulfillmentCriteria = triggerActionObj.get("fulfillmentCriteria");
				if( eFulfillmentCriteria != null && ! eFulfillmentCriteria.isJsonNull() ){
					criteriaExpression = eFulfillmentCriteria.getAsString();
					cookProgramCriteriaExpressions(allAccums, allEvents,
							cimList, offerId, triggerId, criteriaExpression, offerThresholdParametersArray, allFunctions, expProc, programId );
				}
			}
		} else if( "reminderAction".equals( actionType ) ) {
			if( triggerActionObj.has( "reminderCriteria" ) ) {
				criteriaExpression = triggerActionObj.get("reminderCriteria").getAsString();
				cookProgramCriteriaExpressions(allAccums, allEvents,
						cimList, offerId, triggerId, criteriaExpression, offerThresholdParametersArray, allFunctions, expProc, programId );
			}

			if( triggerActionObj.has( "reminderMessages" ) ) {
				reminderMessagesArray = triggerActionObj.get( "reminderMessages" ).getAsJsonArray();
				cookReminderMessages( allAccums, allEvents, cimList, offerId, triggerId, offerThresholdParametersArray,
						reminderMessagesArray, allFunctions, expProc, programId );
			}
		}
	}
	

	private void cookNextBestActionCriteria(LinkedHashMap<String, JsonObject> allAccums,
			LinkedHashMap<String, JsonObject> allEvents, LinkedHashMap<String, JsonObject> cimList, String offerId,
			JsonArray offerThresholdParametersArray, String expressionId, String expression,
			LinkedHashMap<String, JsonObject> allFunctions, ExpressionProcessor expProc, String programId) throws PExpressionException, ParseException {
		System.out.println( "cookNextBestActionCriteria() : Start : criteria=" + expression );
		if( expression.trim().length() > 0 ) {
			String type = "JsonArray";
			JsonObject objToCook = new JsonObject();
			objToCook.addProperty( "id", expressionId );
			objToCook.addProperty( "expression", expression );
			objToCook.addProperty( "expressionType", type );
			PExpressionEvaluator exp = expProc.createByDefinition( objToCook );
			if( exp != null )
				allExpressions.put(  expressionId, exp );
		}
		System.out.println( "cookNextBestActionCriteria() : End" );
	}

	/**
	 * @param allAccums
	 * @param allEvents
	 * @param cimList
	 * @param refresh
	 * @throws Exception
	 */
	public void cookExpressionsFromProgram( LinkedHashMap< String, JsonObject > allAccums, LinkedHashMap< String, JsonObject > allEvents,
			java.util.LinkedHashMap< String, JsonObject > cimList, boolean refresh, LinkedHashMap< String, JsonObject > allFunctions, ExpressionProcessor expProc  ) throws Exception {

		System.out.println("cookExpressionsFromProgram() : Start.");

		ProgramsDAO programsDAO = new ProgramsDAO();

		java.util.LinkedHashMap< String, JsonObject > programsMap = programsDAO.readActive( refresh );

		Iterator< String > programsMapIter = programsMap.keySet().iterator();
		String programId = null;
		JsonObject programObj = null;
		JsonObject programActionsObj = null;
		Iterator< Entry< String, JsonElement >> programActionsObjIter = null;
		String triggerId = null;
		JsonArray triggerActionsArray = null;
		String criteriaExpression = null;
		String offerId = null;
		JsonArray offerThresholdParametersArray = null;
		JsonArray reminderMessagesArray = null;
		String actionType = null;
		String id = null;
		JsonObject contactPolicyExpressionObj = null;
		JsonObject fulfillmentTrigger = null;

		while( programsMapIter.hasNext() ) {
			programId = programsMapIter.next();
			programObj = programsMap.get( programId ).getAsJsonObject();

			System.out.println("cookExpressionsFromProgram() : programId=" + programId + ", programObj=" + programObj);

			if( programObj != null ) {
				if( programObj.has( "programActions" ) ) {
					programActionsObj = programObj.get( "programActions" ).getAsJsonObject();

					System.out.println("cookExpressionsFromProgram() : programActionsObj=" + programActionsObj);

					programActionsObjIter = programActionsObj.entrySet().iterator();

					while( programActionsObjIter.hasNext() ) {
						Entry< String, JsonElement > thisEntry = programActionsObjIter.next();
						triggerId = thisEntry.getKey();
						triggerActionsArray = thisEntry.getValue().getAsJsonArray();

						System.out.println("cookExpressionsFromProgram() : triggerId=" + triggerId);

						for( int i = 0; i < triggerActionsArray.size(); i++ ) {
							JsonObject triggerActionObj = triggerActionsArray.get( i ).getAsJsonObject();

							actionType = triggerActionObj.get("actionType").getAsString();
							
							if( actionType.equals( "nextBestAction" ) ) {
								String nextBestActionId = triggerActionObj.get( "nextBestActionId" ).getAsString();
								String expressionId = programId + "_" + nextBestActionId;
								String expression = triggerActionObj.get( "nextBestActionCriteria" ).getAsString();
								cookNextBestActionCriteria( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, expressionId, 
										expression, allFunctions, expProc, programId );
								continue;
							}
							
							offerId = triggerActionObj.get( "offerId" ).getAsString();

							System.out.println("cookExpressionsFromProgram() : actionType=" + actionType + ", offerId=" + offerId);

							if( triggerActionObj.has( "offerThresholdParameters" ) ) {
								offerThresholdParametersArray = triggerActionObj.get( "offerThresholdParameters" ).getAsJsonArray();
							}

							if( triggerActionObj.has( "maxContactAcrossSubscribers" ) && ! triggerActionObj.get( "maxContactAcrossSubscribers" ).isJsonNull() ) {
								id = "MAX_CONTACT_ACROSS_SUBSCRIBERS_" + offerId;

								//								if ( !allExpressions.containsKey( id ) ) {
								contactPolicyExpressionObj = triggerActionObj.get( "maxContactAcrossSubscribers" ).getAsJsonObject();
								cookContactPolicyExpressions( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, id, 
										contactPolicyExpressionObj, allFunctions, expProc, programId );
								//								}
							}

							if( triggerActionObj.has( "maxContactPerSubscriber" ) && ! triggerActionObj.get( "maxContactPerSubscriber" ).isJsonNull() ) {
								id = "MAX_CONTACT_PER_SUBSCRIBER_" + offerId;

								//								if ( !allExpressions.containsKey( id ) ) {
								contactPolicyExpressionObj = triggerActionObj.get( "maxContactPerSubscriber" ).getAsJsonObject();
								cookContactPolicyExpressions( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, id, 
										contactPolicyExpressionObj, allFunctions, expProc, programId );
								//								}
							}

							if( triggerActionObj.has( "cooldownPeriodForOfferPerSubscriber" ) && ! triggerActionObj.get( "cooldownPeriodForOfferPerSubscriber" ).isJsonNull() ) {
								id = "COOLDOWN_PERIOD_FOR_OFFER_PER_SUBSCRIBER_" + offerId;

								//								if ( !allExpressions.containsKey( id ) ) {
								contactPolicyExpressionObj = triggerActionObj.get( "cooldownPeriodForOfferPerSubscriber" ).getAsJsonObject();
								cookContactPolicyExpressions( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, id, 
										contactPolicyExpressionObj, allFunctions, expProc, programId );
								//								}
							}
							
							if( triggerActionObj.has( "maxFulfillmentsPerSubscriber" ) && ! triggerActionObj.get( "maxFulfillmentsPerSubscriber" ).isJsonNull() ) {
								id = "MAX_FULFILLMENTS_" + offerId;
								contactPolicyExpressionObj = triggerActionObj.get( "maxFulfillmentsPerSubscriber" ).getAsJsonObject();
								cookContactPolicyExpressions( allAccums, allEvents, cimList, offerId, offerThresholdParametersArray, id, 
										contactPolicyExpressionObj, allFunctions, expProc, programId );
							}

							if( "monitoringAction".equals( actionType ) ) {
								if( triggerActionObj.has( "fulfillmentTrigger" ) ) {
									fulfillmentTrigger = triggerActionObj.get("fulfillmentTrigger").getAsJsonObject();

									cookMonitoringDurationExpression( allAccums, 
											allEvents,
											cimList, 
											offerId, 
											offerThresholdParametersArray, 
											fulfillmentTrigger,
											allFunctions, expProc, programId );
								}
							} else if( "fulfillmentAction".equals( actionType ) ) {
								if( triggerActionObj.has( "fulfillmentCriteria" ) && ! triggerActionObj.get( "fulfillmentCriteria" ).isJsonNull() ) {
									criteriaExpression = triggerActionObj.get("fulfillmentCriteria").getAsString();
									cookProgramCriteriaExpressions(allAccums, allEvents,
											cimList, offerId, triggerId, criteriaExpression, offerThresholdParametersArray, allFunctions, expProc, programId );
								}

							} else if( "reminderAction".equals( actionType ) ) {
								if( triggerActionObj.has( "reminderCriteria" ) ) {
									criteriaExpression = triggerActionObj.get("reminderCriteria").getAsString();
									cookProgramCriteriaExpressions(allAccums, allEvents,
											cimList, offerId, triggerId, criteriaExpression, offerThresholdParametersArray, allFunctions, expProc, programId );
								}

								if( triggerActionObj.has( "reminderMessages" ) ) {
									reminderMessagesArray = triggerActionObj.get( "reminderMessages" ).getAsJsonArray();
									cookReminderMessages( allAccums, allEvents, cimList, offerId, triggerId, offerThresholdParametersArray,
											reminderMessagesArray, allFunctions, expProc, programId );
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Cook ReminderCriteria Expressions.
	 * 
	 * @param allAccums
	 * @param allEvents
	 * @param cimList
	 * @param offerId
	 * @param triggerId
	 * @param reminderCriteria
	 * @param offerThresholdParametersArray
	 * @throws Exception
	 */
	private void cookProgramCriteriaExpressions(LinkedHashMap< String, JsonObject > allAccums, LinkedHashMap< String, JsonObject > allEvents,
			java.util.LinkedHashMap< String, JsonObject > cimList, String offerId, String triggerId, String criteria, JsonArray offerThresholdParametersArray,
			LinkedHashMap< String, JsonObject > allFunctions, ExpressionProcessor expProc, String programId  ) throws Exception {

		System.out.println("cookProgramCriteriaExpressions() : Start : offerId=" + offerId 
				+ ", triggerId=" + triggerId 
				+ ", criteria=" + criteria 
				+ ", offerThresholdParametersArray=" + offerThresholdParametersArray);

		criteria = replaceThresholdParameter(offerId, criteria, offerThresholdParametersArray, programId);

		System.out.println("cookProgramCriteriaExpressions() : criteria=" + criteria);

		JsonObject reminderCriteriaObjToCook = allEvents.get( triggerId );		
		reminderCriteriaObjToCook.addProperty( "expression", criteria );

		PExpressionEvaluator exp = expProc.createByDefinition( reminderCriteriaObjToCook );
		if( exp != null )
			allExpressions.put(  reminderCriteriaObjToCook.get( "id").getAsString(), exp );

	}

	/**
	 * Cook Fulfillment Message Expressions.
	 * 
	 * @param allAccums
	 * @param allEvents
	 * @param cimList
	 * @param offerId
	 * @param offerThresholdParametersArray
	 * @param fulfillmentMessages
	 * @throws Exception
	 */
	private void cookReminderMessages( LinkedHashMap< String, JsonObject > allAccums, LinkedHashMap< String, JsonObject > allEvents,
			java.util.LinkedHashMap< String, JsonObject > cimList, String offerId, String reminderTriggerId, JsonArray offerThresholdParametersArray,
			JsonArray reminderMessages, LinkedHashMap< String, JsonObject > allFunctions, ExpressionProcessor expProc, String programId  ) throws Exception {

		System.out.println( "cookReminderMessages() : Start : offerId=" + offerId + ", reminderTriggerId=" + reminderTriggerId );

		JsonObject channelObj = null;
		JsonObject channelObjToCook = null;
		String channelName = null;
		String lang = null;
		String text = null;
		JsonArray messagesArray = null;
		JsonObject messageObj = null;
		String id = null;

		for( int i = 0; i < reminderMessages.size(); i++ ) {
			channelObj = reminderMessages.get( i ).getAsJsonObject();

			if( channelObj != null ) {

				if( channelObj.has( "channelName" ) ) {
					channelName = channelObj.get( "channelName" ).getAsString();
				}

				if( channelObj.has( "messages" ) ) {
					messagesArray = channelObj.get( "messages" ).getAsJsonArray();

					for( int j = 0; j < messagesArray.size(); j++ ) {
						messageObj = messagesArray.get( j ).getAsJsonObject();

						lang = messageObj.get( "lang" ).getAsString();
						text = messageObj.get( "text" ).getAsString();

						text = replaceThresholdParameter(offerId, text, offerThresholdParametersArray, programId);

						id = "REMINDER_MESSAGE_" + reminderTriggerId + "_" + channelName + "_" + lang;

						channelObjToCook = new JsonObject();
						channelObjToCook.addProperty( "id", id );
						channelObjToCook.addProperty( "expression", text );
						channelObjToCook.addProperty( "expressionType", "java.lang.String" );

						PExpressionEvaluator exp = expProc.createByDefinition( channelObjToCook );
						if( exp != null )
							allExpressions.put(  id, exp );
					}
				}
			}
		}
	}

	/**
	 * Cook Report Parameters.
	 * 
	 * @param allAccums
	 * @param allEvents
	 * @param cimList
	 * @param offerId
	 * @param offerThresholdParametersArray
	 * @param fulfillmentProducts
	 * @throws Exception
	 */
	private void cookContactPolicyExpressions( LinkedHashMap< String, JsonObject > allAccums, 
			LinkedHashMap< String, JsonObject > allEvents,
			LinkedHashMap< String, JsonObject > cimList, 
			String offerId, 
			JsonArray offerThresholdParametersArray, 
			String id,
			JsonObject contactPolicyExpressionObj,
			LinkedHashMap< String, JsonObject > allFunctions, ExpressionProcessor expProc,
			String programId ) throws Exception {

		System.out.println( "cookContactPolicyExpressions() : Start : offerId=" + offerId );

		if( contactPolicyExpressionObj.has( "expression" ) ) {

			String expression = contactPolicyExpressionObj.get( "expression" ).getAsString();
			String type = contactPolicyExpressionObj.get( "type" ).getAsString();

			expression = replaceThresholdParameter(offerId, expression, offerThresholdParametersArray, programId);

			JsonObject objToCook = new JsonObject();

			objToCook.addProperty( "id", id );
			objToCook.addProperty( "expression", expression );
			objToCook.addProperty( "expressionType", type );

			PExpressionEvaluator exp = expProc.createByDefinition( objToCook );
			if( exp != null )
				allExpressions.put(  id, exp );
		}

		System.out.println( "cookContactPolicyExpressions() : End" );
	}


	private void cookControlGroupExpressions( LinkedHashMap< String, JsonObject > allAccums, 
			LinkedHashMap< String, JsonObject > allEvents,
			LinkedHashMap< String, JsonObject > cimList, 
			String offerId, 
			JsonArray offerThresholdParametersArray, 
			String id,
			String expression,
			LinkedHashMap< String, JsonObject > allFunctions, ExpressionProcessor expProc,
			String programId ) throws Exception {

		System.out.println( "cookControlGroupCriteria() : Start : offerId=" + offerId );

		if( expression.trim().length() > 0 ) {

			String type = "boolean";

			expression = replaceThresholdParameter(offerId, expression, offerThresholdParametersArray, programId);

			JsonObject objToCook = new JsonObject();

			objToCook.addProperty( "id", id );
			objToCook.addProperty( "expression", expression );
			objToCook.addProperty( "expressionType", type );

			PExpressionEvaluator exp = expProc.createByDefinition( objToCook );
			if( exp != null )
				allExpressions.put(  id, exp );
		}

		System.out.println( "cookControlGroupCriteria() : End" );
	}
	
	
	private void cookCountAsContactExpressions( LinkedHashMap< String, JsonObject > allAccums, 
			LinkedHashMap< String, JsonObject > allEvents,
			LinkedHashMap< String, JsonObject > cimList, 
			String offerId, 
			JsonArray offerThresholdParametersArray, 
			String id,
			String expression,
			LinkedHashMap< String, JsonObject > allFunctions, ExpressionProcessor expProc,
			String programId ) throws Exception {

		System.out.println( "cookCountAsContactExpressions() : Start : offerId=" + offerId );

		if( expression.trim().length() > 0 ) {

			String type = "java.lang.String";

			expression = replaceThresholdParameter(offerId, expression, offerThresholdParametersArray, programId);

			JsonObject objToCook = new JsonObject();

			objToCook.addProperty( "id", id );
			objToCook.addProperty( "expression", expression );
			objToCook.addProperty( "expressionType", type );

			PExpressionEvaluator exp = expProc.createByDefinition( objToCook );
			if( exp != null )
				allExpressions.put(  id, exp );
		}

		System.out.println( "cookCountAsContactExpressions() : End" );
	}
	
	private void monitoringContactCriteriaExpressions( LinkedHashMap< String, JsonObject > allAccums, 
			LinkedHashMap< String, JsonObject > allEvents,
			LinkedHashMap< String, JsonObject > cimList, 
			String offerId, 
			JsonArray offerThresholdParametersArray, 
			String id,
			String expression,
			LinkedHashMap< String, JsonObject > allFunctions, 
			ExpressionProcessor expProc,
			String programId ) throws Exception {

		System.out.println( "contactCriteria() : Start : Id = " + id );

		String type = "long";
		JsonObject objToCook = new JsonObject();

		objToCook.addProperty( "id", id );
		objToCook.addProperty( "expression", expression );
		objToCook.addProperty( "expressionType", type );

		PExpressionEvaluator exp = expProc.createByDefinition( objToCook );
		if( exp != null )
			allExpressions.put(  id, exp );

		System.out.println( "contactCriteria() : End" );
	}


	/**
	 * @param allAccums
	 * @param allEvents
	 * @param cimList
	 * @param offerId
	 * @param offerThresholdParametersArray
	 * @param allFunctions 
	 * @param monitoringDurationExpressionObj
	 */
	private void cookMonitoringDurationExpression( LinkedHashMap< String, JsonObject > allAccums, 
			LinkedHashMap< String, JsonObject > allEvents,
			LinkedHashMap< String, JsonObject > cimList, 
			String offerId, 
			JsonArray offerThresholdParametersArray, 
			JsonObject fulfillmentTrigger, LinkedHashMap< String, JsonObject > allFunctions, ExpressionProcessor expProc,
			String programId ) throws Exception {

		JsonObject monitoringDurationObj = null;

		if ( fulfillmentTrigger.has( "monitoringDuration1" ) ) {
			monitoringDurationObj = fulfillmentTrigger.get( "monitoringDuration1" ).getAsJsonObject();

			if ( monitoringDurationObj.has( "expression" ) ) {
				String expression = monitoringDurationObj.get( "expression" ).getAsString();
				String type = monitoringDurationObj.get( "type" ).getAsString();

				if ( !"".equals(expression ) && !"".equals( type ) ) {

					if ( fulfillmentTrigger.has( "triggerId" ) ) {
						String fulfillmentTriggerId = fulfillmentTrigger.get( "triggerId" ).getAsString();

						String id = "MONITORING_DURATION_" + fulfillmentTriggerId;
						System.out.println( "cookMonitoringDurationExpression() : Start : " + "MONITORING_DURATION_" + fulfillmentTriggerId + " Exp: " + expression );

						expression = replaceThresholdParameter(offerId, expression, offerThresholdParametersArray, programId);

						JsonObject objToCook = new JsonObject();

						objToCook.addProperty( "id", id );
						objToCook.addProperty( "expression", expression );
						objToCook.addProperty( "expressionType", type );

						PExpressionEvaluator exp = expProc.createByDefinition( objToCook );
						if( exp != null )
							allExpressions.put(  id, exp );
					}
				}
			}
		}
	}

	/**
	 * @param offerId
	 * @param text
	 * @param offerThresholdParametersArray
	 * @return
	 * @throws Exception
	 */
	private String replaceThresholdParameter(String offerId, String text, JsonArray offerThresholdParametersArray, String programId) throws Exception {

		JsonObject offerThresholdParameterObj = null;
		String key = null;
		String replacementKey = null;
		String returnText = text;

		for( int i = 0; i < offerThresholdParametersArray.size(); i++ ) {
			offerThresholdParameterObj = offerThresholdParametersArray.get( i ).getAsJsonObject();

			if( offerThresholdParameterObj != null ) {
				if( offerThresholdParameterObj.has( "key" ) ) {
					key = offerThresholdParameterObj.get( "key" ).getAsString();

					if( text.indexOf( key ) != -1 ) {
						replacementKey = "OFFER_THRESHOLD_PARAM_" + programId + "_" + offerId + '_' + key;
						returnText = returnText.replace( key, replacementKey );
					}
				}
			}
		}

		return returnText;
	}

	
	private void cookOfferDecisionCriteria( ExpressionProcessor expProc, JsonObject aNode, String id ) throws Exception {
		JsonObject objToCook = new JsonObject();	
		objToCook.addProperty( "id", id );
		objToCook.addProperty( "expression", aNode.get( "expression").getAsString() );
		objToCook.addProperty( "expressionType", "boolean" );

		PExpressionEvaluator exp = expProc.createByDefinition( objToCook );
		if( exp != null )
			allExpressions.put( id, exp );
		
		if( aNode.get( "nextLevel" ) == null || aNode.get( "nextLevel" ).isJsonNull() )
			return;
		JsonArray jNextLevel = aNode.get( "nextLevel" ).getAsJsonArray();
		for( int i=0; i<jNextLevel.size(); i++ ) {
			cookOfferDecisionCriteria( expProc, jNextLevel.get(i).getAsJsonObject(), id + "_" + i );
		}	
    }
	
	
	public static void main( String[ ] arg ) throws PExpressionException, IOException, URISyntaxException, ParseException {
		ExpressionDao expDao = new ExpressionDao();
		LinkedList< URI > uris = new LinkedList< URI >();
		URI uri = new URI( "http://127.0.0.1:8091/pools" );
		uris.add( uri );
		Commons.pandaCacheConfig=new PandaCacheConfig(uris);
		expDao.prepareAllExpressions();

		//		JsonObject dynamicParams = new JsonObject();
		//		JsonObject attribs = new JsonObject();
		//		attribs.addProperty( "type", "double" );
		//		attribs.addProperty( "value", "10" );
		//		dynamicParams.add( "target_topup", attribs );
		//		
		//		
		//		IndicatorDefDao dao = new IndicatorDefDao();
		//		LinkedHashMap< String, JsonObject > allIndicators = ( LinkedHashMap< String, JsonObject > ) dao.read();
		//		if( allIndicators == null || allIndicators.size() == 0 )
		//			throw new PExpressionException( "Indicator Definitions not Available" );
		//
		//
		//		EventDefDao daoEvents = new EventDefDao();
		//		LinkedHashMap< String, JsonObject > allEvents = daoEvents.read();
		//
		//		CIMDao cimDao = new CIMDao();
		//		LinkedHashMap< String, JsonObject > allCIM = cimDao.read();
		//
		//		FunctionDao funcDao = new FunctionDao();
		//		LinkedHashMap< String, JsonObject > allFunctions = funcDao.readUserDefined();
		//		
		//		ExpressionProcessor expProc = new ExpressionProcessor( allIndicators, allEvents, allFunctions, allCIM );
		//
		//		String expression = "CHARGED_AMOUNT > SiftAdd1( 1, 2 )";
		//		expProc.validateExpression( "id", expression, "boolean", "expression", dynamicParams );
	}
}