package com.knowesis.sift.api.dao;

import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Set;

import au.com.bytecode.opencsv.CSVReader;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.protocol.views.Query;
import com.couchbase.client.protocol.views.Stale;
import com.couchbase.client.protocol.views.View;
import com.couchbase.client.protocol.views.ViewResponse;
import com.couchbase.client.protocol.views.ViewRow;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.knowesis.sift.Common.PandaCacheConfig;
import com.knowesis.sift.api.resource.EventDefResource;
import com.knowesis.sift.expression.PExpressionEvaluator;
import com.knowesis.sift.expression.PExpressionException;

/**
 * @author Raja SP
 * 
 */

public class EventDefDao {

	private static java.util.LinkedHashMap< String, JsonObject > allEvents = null;
	
	// Creates All from file
	public void createFromFile( String fileName ) throws IOException, ParseException {
		CSVReader reader = new CSVReader( new FileReader( fileName ) );
		String[ ] line = null;

		int nRecs = -1;
		while( ( line = reader.readNext() ) != null ) {
			nRecs++;
			if( nRecs == 0 )
				continue;

			if( line[ 0 ] == null || line[ 0 ].trim().length() == 0 )
				continue;

			String docType = "EventDefinition";
			JsonObject jObj = new JsonObject();
			jObj.addProperty( "docType", docType );
			jObj.addProperty( "id", line[ 0 ] );
			
			System.out.println( "-------------------Reading from File : ------------------ " + line[0] );
			
			jObj.addProperty( "description", line[ 1 ] );
			String category = line[ 2 ];
			jObj.addProperty( "category", category );
			if( category != null ) {
				EventDefResource res = new EventDefResource();
				res.manageCategory( category );
			}

			String maxTrigger = line[ 3 ];
			if( maxTrigger != null && maxTrigger.trim().length() > 0 ) {
			    jObj.addProperty( "maxTriggerLimit", Integer.parseInt( maxTrigger ) );
			}
			
			String startTime = line[ 4 ];
			SimpleDateFormat dtf = new SimpleDateFormat( "ddMMyyyy HH:mm:ss" );
			Date dt = dtf.parse( startTime );

			Date endDt = dtf.parse( line[ 5 ] );

			jObj.addProperty( "eventStartTime", new Long( dt.getTime() ) );
			jObj.addProperty( "eventEndTime", new Long( endDt.getTime() ) );

			jObj.addProperty( "eventMonitoringDuration", line[ 6 ] );
			jObj.addProperty( "targetSystemID", line[ 7 ] );
			jObj.addProperty( "author", line[ 8 ] );
			jObj.addProperty( "requireNoEvent", line[ 9 ] );
			jObj.addProperty( "enableRecuringEvent", line[ 10 ] );
			
			String recCount = line[ 11 ];
			if( recCount != null && recCount.trim().length() > 0 )
			    jObj.addProperty( "recurringCount", Integer.parseInt( recCount ) );
			
			jObj.addProperty( "expressionType", line[ 12 ] );
			jObj.addProperty( "expression", line[ 13 ] );
			jObj.addProperty( "status", line[ 14 ] );
			jObj.addProperty( "executionMode", line[ 15 ] );
			jObj.addProperty( "variables", line[ 16 ] );
			jObj.addProperty( "values", line[ 17 ] );

			String btrigTime = line[ 18 ];
			if( btrigTime != null && btrigTime.trim().length() > 0 ) {
				dt = dtf.parse( btrigTime );
				jObj.addProperty( "batchTriggerTimestamp", new Long( dt.getTime() ) );
			}
			String scriptOrExpression = line[19];
			jObj.addProperty( "scriptOrExpression", scriptOrExpression );
			
			//PAYLOAD
			String payload = line[20];
			if( payload != null )
			    jObj.addProperty( "eventPayload", payload );
			
			String taggedForAll = line[21];
			boolean bTaggedForAll = false;
			if( taggedForAll != null ) {
			    jObj.addProperty( "taggedForAll", taggedForAll );
			    if( taggedForAll.equals( "true" ) )
			        bTaggedForAll = true;
			}    
			
			jObj.add( "scheduledReminderCondition", convertToJsonArray(  line[22] ) );
			jObj.add( "partialEventTriggerCondition", convertToJsonArray(  line[23] ) );
			jObj.add( "noEventTriggerCondition", convertToJsonArray(  line[24] ) );
			jObj.add( "activityTriggeredReminderCondition", convertToJsonArray(  line[25] ) );
			JsonArray mainEvent = convertToJsonArray(  line[26] );
			jObj.add( "mainEvent", mainEvent );
			if( mainEvent.size() > 0 )
				jObj.addProperty( "dependantEvent", true  );
			else
				jObj.addProperty( "dependantEvent", false  );

			String programs = line[27];
			if( programs != null ) {
				String[] strProg = programs.split( "," );
				JsonArray jarr = new JsonArray();
				for( int i=0; i<strProg.length; i++ ) {
					if( strProg[i].trim().length() > 0 )
					    jarr.add( new JsonPrimitive( strProg[i]) );
				}
				jObj.add( "programs", jarr );
			}
			
			String campaignCodes = line[ 28 ];
			jObj.addProperty( "campaigns", campaignCodes  );

			JsonParser parser = new JsonParser();
			PandaCacheConfig cache = new PandaCacheConfig();
			JsonObject jTagListForAll = null;
			if( bTaggedForAll &&  line[ 14 ].equals( "Active" )) {
				String taglistForAll = ( String )cache.getClient().get( "EventTagList-ALL" );
				if( taglistForAll != null ) {
					jTagListForAll = ( JsonObject ) parser.parse( taglistForAll );
				} else {
					jTagListForAll = new JsonObject();
					jTagListForAll.addProperty( "docType", "TagList" );
				}	
				JsonObject thisTag = new JsonObject();
				thisTag.addProperty( "startTimestamp", new java.util.Date().getTime() );
				thisTag.addProperty( "endTimestamp", new java.util.Date().getTime() );
				jTagListForAll.add( line[ 0 ], thisTag );
				cache.getClient().set( "EventTagList-ALL", 0, jTagListForAll.toString() );
			}
			
			jObj.addProperty( "creationDate", new Long( new java.util.Date().getTime() ) );
			jObj.addProperty( "lastUpateDate", new Long( new java.util.Date().getTime() ) );

			new PandaCacheConfig().getClient().set( line[ 0 ], 0, jObj.toString() );
		}
	}

	private JsonArray convertToJsonArray( String scheduledReminderCondition ) {
		JsonArray result = new JsonArray();
		if( scheduledReminderCondition != null ) {
			String[] entries = scheduledReminderCondition.split( "," );
			for( int i=0; i<entries.length; i++ ) {
				if( entries[ i ].trim().length() > 0 )
				    result.add( new JsonPrimitive( entries[i] ) );
			}	
		}
		return result;
	}

	// Reads all from file
	public java.util.LinkedHashMap< String, JsonObject > read() {
		if( allEvents != null )
			return allEvents;
		
		System.out.println( "Reading from **** Persist **** " );

		PandaCacheConfig cache = new PandaCacheConfig();
		View view = cache.getView( "view_allEventDefinitions" );
		CouchbaseClient client = cache.getClient();
		Query query = new Query();
		query.setIncludeDocs( false );
		query.setStale( Stale.FALSE );
		ViewResponse viewresponse = client.query( view, query );
		Iterator< ViewRow > itr = viewresponse.iterator();
		allEvents = new java.util.LinkedHashMap< String, JsonObject >();
		JsonParser parser = new JsonParser();
		while( itr.hasNext() ) {
			ViewRow record = itr.next();
			String doc = ( String ) cache.get( record.getKey() );
			JsonObject jobj = ( JsonObject ) parser.parse( doc );
			allEvents.put( record.getId(), jobj );
		}
		return allEvents;
	}

	public java.util.LinkedHashMap< String, JsonObject > readActive() {
		PandaCacheConfig cache = new PandaCacheConfig();
		View view = cache.getView( "view_AllActiveEventDefinitions" );
		CouchbaseClient client = cache.getClient();
		Query query = new Query();
		query.setIncludeDocs( false );
		query.setStale( Stale.FALSE );
		ViewResponse viewresponse = client.query( view, query );
		Iterator< ViewRow > itr = viewresponse.iterator();
		java.util.LinkedHashMap< String, JsonObject > eventMap = new java.util.LinkedHashMap< String, JsonObject >();
		JsonParser parser = new JsonParser();
		while( itr.hasNext() ) {
			ViewRow record = itr.next();
			String doc = ( String ) cache.get( record.getKey() );
			JsonObject jobj = ( JsonObject ) parser.parse( doc );
			eventMap.put( record.getId(), jobj );
		}
		return eventMap;
	}
	
	
	// reads the specified accumulator definition
	public String read( String id ) {
		
		if( allEvents == null )
			allEvents = this.read();
		
		JsonObject eve = allEvents.get( id );
		if( eve != null )
		    return allEvents.get( id ).toString();
		else 
			return new JsonObject().toString();
		/*
		PandaCache cache = new PandaCache();
		return ( String ) cache.getClient().get( id );
		*/
	}

	/*
	// updates the specified accumulator definition
	public void update( String id, String jsonString ) throws PExpressionException, IOException {
		JsonParser parser = new JsonParser();
		JsonObject jObj = ( JsonObject ) parser.parse( jsonString );
		ExpressionDao expDao = new ExpressionDao();
		PExpressionEvaluator expEval = expDao.createByDefinition( jObj );
		PandaCacheConfig cache = new PandaCacheConfig();
//		java.util.HashMap< String, PExpressionEvaluator > expressionsMap = ( HashMap< String, PExpressionEvaluator > ) cache
//				.getClient().get( "expressionsMap" );
//		if( expressionsMap == null )
//			expressionsMap = new java.util.HashMap< String, PExpressionEvaluator >();
//		expressionsMap.put( id, expEval );
//		cache.getClient().set( "expressionsMap", 0, expressionsMap );

		System.out.println( jObj.toString() );

		//Check the taglist
		boolean taggedForAll = true;
		JsonElement jtagFlag = jObj.get( "taggedForAll" );
		if( jtagFlag == null )
			taggedForAll = false;
		else {
			String strTaggedForAll = jtagFlag.getAsString();
			if( strTaggedForAll.equals( "true" ) )
			    taggedForAll = true;
			else
			    taggedForAll = false;
		}
		JsonObject jTagListForAll = null;
		String taglistForAll = ( String ) cache.getClient().get( "EventTagList-ALL" );
		if( taglistForAll != null ) 
			jTagListForAll = ( JsonObject ) parser.parse( taglistForAll );
		else
			jTagListForAll = new JsonObject();
		
		String status = jObj.get( "status" ).getAsString();
		if( status.equals( "DELETED" ) ) {
			jTagListForAll.remove( id );
			cache.getClient().set( "EventTagList-ALL", 0, jTagListForAll.toString() );
		} else { 
    		if( taggedForAll ) {
    			if( jTagListForAll != null ) {
    				JsonObject thisTag = new JsonObject();
    				thisTag.addProperty( "startTimestamp", new java.util.Date().getTime() );
    				thisTag.addProperty( "endTimestamp", new java.util.Date().getTime() );
    				jTagListForAll.add( id, thisTag );
    				cache.getClient().set( "EventTagList-ALL", 0, jTagListForAll.toString() );
    			}
    		} else {
    			if( jTagListForAll != null ) {
    				jTagListForAll.remove( id );
    				cache.getClient().set( "EventTagList-ALL", 0, jTagListForAll.toString() );
    			}
    			//remove the List here if it exists.
    		}
		}

		cache.getClient().set( id, 0, jObj.toString() );
		//if the event is deleted, remove it from the cache
		if( ! status.equals( "DELETED" ) )
		    allEvents.put( id, jObj );
		else {
			allEvents.remove( id );
			
		}	
	}
	*/

	public static void main( String[ ] arg ) throws IOException, PExpressionException, URISyntaxException, ParseException {
		LinkedList< URI > uris = new LinkedList< URI >();
		uris.add( new URI( "http://localhost:8091/pools" ) );
		PandaCacheConfig cache = new PandaCacheConfig( uris );
		EventDefDao dao = new EventDefDao();
		//dao.createFromFile( "/home/stream/PandaData/eventconf.csv" );
//		LinkedHashMap< String, JsonObject > allEve = dao.read();
//		
//		IndicatorDefDao indDao = new IndicatorDefDao();
//		LinkedHashMap< String, JsonObject > allIndDef = ( LinkedHashMap< String, JsonObject > ) indDao.read()[0];
//		
//		ExpressionDao expDao  = new ExpressionDao();
//		HashMap< String, PExpressionEvaluator > expMap = expDao.prepareAllExpressions();
//
//		Set< String > activeInd = dao.getActiveIndicators( allEve, expMap, allIndDef );
		Set< String > activeInd = dao.getIndicatorsInEvent( "AirportLocationCampaign" );
		Iterator< String > itr = activeInd.iterator();
		while( itr.hasNext() )
			System.out.println( itr.next() );

		System.out.println( "-------------------------------------------------" );
//		activeInd = dao .getActiveIndicators( "MoUHighOutgoingCallsSMSDaily", expMap, allIndDef );
//		itr = activeInd.iterator();
//		while( itr.hasNext() )
//			System.out.println( itr.next() );
	}
	
	
	public Set< String > getIndicatorsInEvent( String eventId ) throws PExpressionException, IOException, ParseException {
		System.out.println( "Getting Indicators in Event : " + eventId );
		ExpressionDao expDao = new ExpressionDao();
		HashMap< String, PExpressionEvaluator > allExpMap = expDao.read();
		System.out.println( "Got Expressions : " + allExpMap.size() );

		LinkedList< String > unVisitedIndList = new LinkedList< String >();
		IndicatorDefDao indDao = new IndicatorDefDao();
		java.util.LinkedHashMap< String, JsonObject >  allIndDef = ( LinkedHashMap< String, JsonObject > ) indDao.read();

		System.out.println( "Got Indicators : " + allIndDef.size() );
		
		Set< String > indInEvent = this.getIndicatorsInExpression( eventId, allExpMap, allIndDef );
		System.out.println( "Got Indicators in Event Expression : " + indInEvent.toString() );
		
		unVisitedIndList.addAll( indInEvent );
		Set< String > finalList = new LinkedHashSet< String >();

		//iterate over the unVisitedInd list and expand them until all are expanded.
		String aUnVistedInd = null;
		while( ! unVisitedIndList.isEmpty() && ( aUnVistedInd = unVisitedIndList.remove() ) != null ){
			finalList.add( aUnVistedInd );
			Set< String > allIndInExp = getIndicatorsInExpression( aUnVistedInd, allExpMap, allIndDef );
			Iterator< String > itrIndInExp = allIndInExp.iterator();
			while( itrIndInExp.hasNext() ) {
				String thisKey = itrIndInExp.next();
				if( ! finalList.contains( thisKey ) )
					unVisitedIndList.add( thisKey );
			}
		}
		System.out.println( "DONE Getting Indicators in Event Expression : " + eventId );
		return finalList;
	}
	
	
	private Set< String >  getActiveIndicators( LinkedHashMap< String, JsonObject > allEveDef, 
			HashMap< String, PExpressionEvaluator > expMap, LinkedHashMap< String, JsonObject > allIndDef ) {
		//Iterate through all the active events and get all used indicators.
		Set< String > keys = allEveDef.keySet();
		Iterator< String > itr = keys.iterator();
		LinkedList< String > unVisitedIndList = new LinkedList< String >();
		Set< String > finalList = new LinkedHashSet< String >();
		
		while( itr.hasNext() ) {
			String key = itr.next();
			Set< String > indInEvent = this.getIndicatorsInExpression( key, expMap, allIndDef );
			unVisitedIndList.addAll( indInEvent );
			
			//iterate over the unVisitedInd list and expand them until all are expanded.
			String aUnVinistedInd = null;
			while( ! unVisitedIndList.isEmpty() && ( aUnVinistedInd = unVisitedIndList.remove() ) != null ){
				finalList.add( aUnVinistedInd );
				Set< String > allIndInExp = getIndicatorsInExpression( aUnVinistedInd, expMap, allIndDef );
				Iterator< String > itrIndInExp = allIndInExp.iterator();
				while( itrIndInExp.hasNext() ) {
					String thisKey = itrIndInExp.next();
					if( ! finalList.contains( thisKey ) )
						unVisitedIndList.add( thisKey );
				}
			}
		}
		return finalList;
	}

	private Set< String > getIndicatorsInExpression( String key, HashMap< String, PExpressionEvaluator > expMap, 
			LinkedHashMap< String, JsonObject > allIndDef ) {
		PExpressionEvaluator exp = expMap.get( key );
		String[ ] params = exp.getParameterNames();
		Set< String > indInEvent = new LinkedHashSet< String >();
		for( int i=0; i<params.length; i++ ) {
			String thisParam = params[i];
			if( allIndDef.get( thisParam ) != null ) //this is an Indicator
				indInEvent.add( thisParam );
		}
        return indInEvent;
	}

	public Set< String > getActiveIndicators( String event, HashMap< String, PExpressionEvaluator > expMap, 
			LinkedHashMap< String, JsonObject > allIndDef ) {
		LinkedList< String > unVisitedIndList = new LinkedList< String >();
		Set< String > finalList = new LinkedHashSet< String >();

		Set< String > indInEvent = this.getIndicatorsInExpression( event, expMap, allIndDef );
		unVisitedIndList.addAll( indInEvent );
		
		//iterate over the unVisitedInd list and expand them until all are expanded.
		String aUnVinistedInd = null;
		while( ! unVisitedIndList.isEmpty() && ( aUnVinistedInd = unVisitedIndList.remove() ) != null ){
			finalList.add( aUnVinistedInd );
			Set< String > allIndInExp = getIndicatorsInExpression( aUnVinistedInd, expMap, allIndDef );
			//Add these new entries only if they are not in the final list
			Iterator< String > itrIndInExp = allIndInExp.iterator();
			while( itrIndInExp.hasNext() ) {
				String thisKey = itrIndInExp.next();
				if( ! finalList.contains( thisKey ) )
					unVisitedIndList.add( thisKey );
			}
		}
		return finalList;
	}
}
