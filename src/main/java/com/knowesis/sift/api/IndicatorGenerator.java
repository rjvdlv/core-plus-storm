package com.knowesis.sift.api;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import au.com.bytecode.opencsv.CSVWriter;

public class IndicatorGenerator {

	public void generateIndicators( String[ ] dimensions, String serviceType, CSVWriter writer ) {
		String[ ] frequency = { "Daily", "Weekly", "Monthly", "Yearly", "LifeTime" };
		String[ ] measure = { "Count", "Spend", "MoU", "Max", "Mean" };
		if( serviceType.equals( "SMS" ) ) {
			measure = new String[] { "Count", "Spend", "Max", "Mean" };
		}else if( serviceType.equals( "DATA" ) ) {
			measure = new String[] { "Spend", "Volume", "Max", "Mean" };
		}else if( serviceType.equals( "CREDIT" ) ) {
			measure = new String[] { "Count", "Value", "Max", "Mean" };
		}

		for( int i = 0; i < frequency.length; i++ ) {
			for( int j = 0; j < measure.length; j++ ) {
				for( int k = 0; k < dimensions.length; k++ ) {
					String indName = serviceType + "_" + dimensions[ k ] + "_" + measure[ j ] + "_" + frequency[ i ];
					System.out.println( indName );

					String[ ] record = new String[ 8 ];
					record[ 0 ] = indName;
					record[ 1 ] = serviceType;
					record[ 2 ] = serviceType + "-All";
					record[ 3 ] = measure[ j ];
					record[ 4 ] = frequency[ i ];
					record[ 5 ] = "Nathan";
					record[ 6 ] = "double";
					record[ 7 ] = "";
					writer.writeNext( record );
				}
			}
		}
	}

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main( String[ ] args ) throws IOException {

		CSVWriter writer = new CSVWriter( new FileWriter( "/home/stream/PandaData/accumulatorconf5.0.csv" ) );
		IndicatorGenerator indGen = new IndicatorGenerator();
		// TODO Auto-generated method stub
		// Dimensions = All, CallType (IDD, LOCAL), SMS(IDD, LOCAL),
		// Direction(Incoming,Outgoing),
		String[ ] serviceTypeArr = { "VOICE", "SMS", "DATA", "CREDIT" };
		for( int i = 0; i < serviceTypeArr.length; i++ ) {
			String serviceType = serviceTypeArr[ i ];
			String[ ] dimensions = { "ALL" };
			indGen.generateIndicators( dimensions, serviceType, writer );

			String[ ] dimensionsCallType = new String[] { "IDD", "LOCAL" };
			indGen.generateIndicators( dimensionsCallType, serviceType, writer );

			if( !serviceType.equals( "DATA" ) && !serviceType.equals( "CREDIT" ) ) {
				String[ ] dimensionsDirection = new String[] { "Incoming", "Outgoing" };
				indGen.generateIndicators( dimensionsDirection, serviceType, writer );

				// Two Dimensional
				ArrayList< String > multiDimensions = new ArrayList< String >();
				for( int j = 0; j < dimensionsCallType.length; j++ ) {
					for( int k = 0; k < dimensionsDirection.length; k++ ) {
						String multiDimension = dimensionsCallType[ j ] + "_" + dimensionsDirection[ k ];
						multiDimensions.add( multiDimension );
					}
					indGen.generateIndicators( multiDimensions.toArray( new String[ multiDimensions.size() ] ),
							serviceType, writer );
				}
			}
			if( serviceType.equals( "CREDIT" ) ) {
				dimensions = new String[] { "EVC-CASH", "USSD", };
				indGen.generateIndicators( dimensions, serviceType, writer );
			}
		}
		writer.close();
	}
}