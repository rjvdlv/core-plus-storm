package com.knowesis.sift.api;

import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.routing.Router;

import com.knowesis.sift.api.resource.CIMResource;
import com.knowesis.sift.api.resource.EventCategoryResource;
import com.knowesis.sift.api.resource.EventDefResource;
import com.knowesis.sift.api.resource.EventInstanceResource;
import com.knowesis.sift.api.resource.EventStatsResource;
import com.knowesis.sift.api.resource.ExpressionResource;
import com.knowesis.sift.api.resource.FunctionResource;
import com.knowesis.sift.api.resource.GeoFenceResource;
import com.knowesis.sift.api.resource.IndicatorDefResource;
import com.knowesis.sift.api.resource.IndicatorInstanceResource;
import com.knowesis.sift.api.resource.PandagramDefResource;
import com.knowesis.sift.api.resource.SiftOperationResource;
import com.knowesis.sift.api.resource.TagListResource;
import com.knowesis.sift.api.resource.TelcoIndicatorResource;
import com.knowesis.sift.api.resource.UserDefinedFunctionResource;
import com.knowesis.sift.api.resource.UserResource;

public class APIApplication extends Application {

	@Override
	public Restlet createInboundRoot() {
		// Create a router Restlet that defines routes.
		Router router = new Router( getContext() );

		// Defines a route for the resource "list of items"
		router.attach( "/indicatorTypes", IndicatorDefResource.class );
		// Defines a route for the resource "item"
		router.attach( "/indicatorTypes/{indicatorTypeId}", IndicatorDefResource.class );
		router.attach( "/indicatorTypes/partial/{limits}", IndicatorDefResource.class );

		router.attach( "/eventTypes", EventDefResource.class );
		router.attach( "/eventTypes/{eventTypeId}", EventDefResource.class );

		router.attach( "/cim", CIMResource.class );
		router.attach( "/cim/{fieldName}", CIMResource.class );

		router.attach( "/functions", FunctionResource.class );
		router.attach( "/functions/{functionName}", FunctionResource.class );
		
		router.attach( "/userDefinedFunctions", UserDefinedFunctionResource.class );
		router.attach( "/userDefinedFunctions/{functionName}", UserDefinedFunctionResource.class );
		
		router.attach( "/indicatorInstance/{msisdn}", IndicatorInstanceResource.class );
		router.attach( "/indicatorInstance/{msisdn}/indicator/{indicatorName}", IndicatorInstanceResource.class );
		router.attach( "/eventInstance/{msisdn}", EventInstanceResource.class );
		router.attach( "/eventInstance/{msisdn}/event/{eventId}", EventInstanceResource.class );

		router.attach( "/tagList/{msisdn}", TagListResource.class );

		router.attach( "/validateExpression", ExpressionResource.class );

		router.attach( "/eventCategory", EventCategoryResource.class );
		router.attach( "/eventCategory/{eventCategoryId}", EventCategoryResource.class );

		router.attach( "/geoFenceGroup", GeoFenceResource.class );
		router.attach( "/geoFenceGroup/{geoFenceGroupName}", GeoFenceResource.class );
		router.attach( "/geoFenceGroup/{geoFenceGroupName}/geoFenceZone", GeoFenceResource.class );
		router.attach( "/geoFenceGroup/{geoFenceGroupName}/geoFenceZone/{geoFenceZoneName}", GeoFenceResource.class );
		
		router.attach( "/telcoIndicator", TelcoIndicatorResource.class );
		router.attach( "/telcoIndicator/{instanceId}", TelcoIndicatorResource.class );
		router.attach( "/telcoIndicator/{serviceType}/{serviceSubType}/{measurementType}/{frequency}/{range}/{operation}", TelcoIndicatorResource.class );
		router.attach( "/telcoIndicator/{serviceType}/{serviceSubType}/{measurementType}/{frequency}/{range}", TelcoIndicatorResource.class );

		router.attach( "/siftOperation/{parameterType}", SiftOperationResource.class );

		router.attach( "/eventStat/{type}/event/{eventId}/period/{date}", EventStatsResource.class );
		router.attach( "/eventStat/{type}/event/{eventId}/period/{date}/frequency/{frequency}", EventStatsResource.class );

		router.attach( "/pandagram", PandagramDefResource.class );
		router.attach( "/pandagram/{pandagramId}", PandagramDefResource.class );

		router.attach( "/users", UserResource.class );
		router.attach( "/users/{userId}", UserResource.class );
		
		return router;
	}
}
