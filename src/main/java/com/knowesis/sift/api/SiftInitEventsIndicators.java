package com.knowesis.sift.api;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Set;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.PandaCache;
import com.knowesis.sift.api.dao.EventDefDao;
import com.knowesis.sift.api.dao.IndicatorDefDao;
import com.knowesis.sift.expression.PExpressionException;


public class SiftInitEventsIndicators {

	public static void main( String[ ] arg ) throws IOException, ParseException, PExpressionException, URISyntaxException {
		if( arg.length < 4 ) {
			System.out
			.println( "Usage : siftInit PersistAddressList action(export/import) type filename" );
			return;
		}

		String strPersistAddressList = arg[ 0 ];
		LinkedList< URI > uris = new LinkedList< URI >();
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );
		for( int i = 0; i < arrPersistAddress.length; i++ )
			uris.add( new URI( arrPersistAddress[ i ] ) );

		JsonParser parser = new JsonParser();

		PandaCache cache = new PandaCache( uris );
		String action = arg[ 1 ];
		String type = arg[ 2 ];
		String fileName = arg[ 3 ];

		EventDefDao eveDao = new EventDefDao();
		IndicatorDefDao indDao = new IndicatorDefDao();

		LinkedHashMap< String, JsonObject > allEveDef = eveDao.read();
		LinkedHashMap< String, JsonObject > allIndDef = indDao.read();

		if( action.equals( "export" ) ) {
			FileWriter writer = new FileWriter( fileName );
			Set< Entry< String, JsonObject >> es = null;
			if( type.equals( "indicator" ) )
				es = allIndDef.entrySet();
			else
				es = allEveDef.entrySet();
			Iterator< Entry< String, JsonObject >> itr = es.iterator();
			while( itr.hasNext() ) {
				String[] line = new String[2];
				Entry< String, JsonObject > elem = itr.next();
				line[ 0 ] = elem.getKey();
				line[ 1 ] = elem.getValue().toString();
				String aLine = new String( line[ 0 ] + "~" + line[1] + "\n" );
				writer.append( aLine );
			}
			writer.close();
			System.out.println( "****************** Export completed******************" );
		} else if( action.equals( "import" ) ) {
			//			CSVReader reader = new CSVReader( new FileReader( fileName ), '~' );
			BufferedReader br = new BufferedReader(new FileReader(fileName ) );
			String line;
			while ( ( line = br.readLine() ) != null ) {
				String[ ] tokens = line.split( "~" );
				System.out.println( tokens[ 0 ] + " =====> " + tokens[ 1 ] );
				JsonObject jObj = ( JsonObject ) parser.parse( tokens[ 1 ] );
				cache.getClient().set( tokens[ 0 ], 0, jObj.toString() );

				if( type.equals( "indicator" ) )
					continue;
				
				JsonObject jTagListForAll = null;
				boolean bTaggedForAll = jObj.get( "taggedForAll" ).getAsBoolean();
				String status = jObj.get( "status" ).getAsString();
				if( bTaggedForAll &&  status.equals( "Active" )) {
					String taglistForAll = ( String )cache.getClient().get( "EventTagList-ALL" );
					if( taglistForAll != null ) {
						jTagListForAll = ( JsonObject ) parser.parse( taglistForAll );
					} else {
						jTagListForAll = new JsonObject();
						jTagListForAll.addProperty( "docType", "TagList" );
					}	
					JsonObject thisTag = new JsonObject();
					thisTag.addProperty( "startTimestamp", new java.util.Date().getTime() );
					thisTag.addProperty( "endTimestamp", new java.util.Date().getTime() );
					jTagListForAll.add( tokens[ 0 ], thisTag );
					cache.getClient().set( "EventTagList-ALL", 0, jTagListForAll.toString() );
				}			
			}
			System.out.println( "****************** Import completed******************" );
		}
	}
}
