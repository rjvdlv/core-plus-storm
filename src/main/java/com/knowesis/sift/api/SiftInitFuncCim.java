package com.knowesis.sift.api;

import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Set;

import au.com.bytecode.opencsv.CSVReader;

import com.couchbase.client.CouchbaseClient;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.PandaCache;
import com.knowesis.sift.api.dao.CIMDao;
import com.knowesis.sift.api.dao.EventDefDao;
import com.knowesis.sift.api.dao.ExpressionDao;
import com.knowesis.sift.api.dao.FunctionDao;
import com.knowesis.sift.api.dao.IndicatorDefDao;
import com.knowesis.sift.api.model.CIM;
import com.knowesis.sift.expression.PExpressionEvaluator;
import com.knowesis.sift.expression.PExpressionException;


public class SiftInitFuncCim {

	String indConfFile;
	String eveConfFile;
	String cimConFile;
	String indTagFile;
	String eveTagFile;
	String datainitpersistFile;
	String eventsTag;

	protected HashMap< String, PExpressionEvaluator > expMap = null;
	protected LinkedHashMap< String, JsonObject > allIndDef = null;
	protected LinkedHashMap< String, JsonObject > allEveDef = null;

	public HashMap< String, PExpressionEvaluator > expMapActive = null;
	public LinkedHashMap< String, JsonObject > allIndDefActive = null;
	public LinkedHashMap< String, JsonObject > allEveDefActive = null;
	public java.util.HashMap<String,  CIM > cimList = null;
	
	public void initialise() throws IOException, ParseException, PExpressionException {
		// load Indicator Definition
		// load Event Definition
		// load CIM lists
		// Prepare Expressions
		// load TagLists

//		System.out.println( "Reading Indicator Definitions...." );
//		IndicatorDefDao indDefDao = new IndicatorDefDao();
//		indDefDao.createFromFile( this.indConfFile );
//		allIndDef = ( LinkedHashMap< String, JsonObject > ) indDefDao.read();
//		System.out.println( "Loaded Indicator Definitions : " + allIndDef.size() );
//		
//		System.out.println( "Reading Event Definitions...." );
//		EventDefDao eventDefDao = new EventDefDao();
//		eventDefDao.createFromFile( this.eveConfFile );
//		allEveDef =  eventDefDao.read();
//		System.out.println( "Loaded Event Definitions : " + allEveDef.size() );
		
		System.out.println( "Reading CIM Definitions...." );
		CIMDao cimDao = new CIMDao();
//		cimDao.createFromFile( this.cimConFile );
		System.out.println( "Loaded CIM Fields : " + cimDao.read().size() );

		/*
		System.out.println( "Reading Function Definitions...." );
		FunctionDao funcDao = new FunctionDao();
		String functionsFile = cimConFile.replace( "cimconf", "functions" );
		funcDao.createFromFile( functionsFile );
		System.out.println( "Loaded Functions List : " + funcDao.read().size() );
		*/
		
//		System.out.println( "Compiling Expressions...." );
//		ExpressionDao expDao = new ExpressionDao();
//		expMap = expDao.prepareAllExpressions();
//		System.out.println( "Compiled Expressions : " + expDao.read().size() );
//
//		System.out.println( "Loading Tag Lists...." );
//		TagListDao taglistDao = new TagListDao();
//		taglistDao.createFromFile( this.indTagFile, this.eveTagFile );

		// System.out.println( "TagLists  : Events( " +
		// taglistDao.getEventsTaggedForAll().size() + ") Indicators ( " +
		// taglistDao.getIndicatorsTaggedForAll().size() + " ) " );
	}

	
	//All Indictors are InActive in the beginning. This method reads from the events list and turns on all 
	//the necessary indicators.
	public LinkedHashSet< String > prepareActiveIndicatorsEventsExpressions() throws PExpressionException, IOException, ParseException {
		if( allEveDef == null ) {
			EventDefDao eventDefDao = new EventDefDao();
			allEveDef = eventDefDao.read();
		}	
		if( allIndDef == null ) {
			IndicatorDefDao indDefDao = new IndicatorDefDao();
			allIndDef = ( LinkedHashMap< String, JsonObject > ) indDefDao.read();
		}
		if( expMap == null ) {
			ExpressionDao expDao = new ExpressionDao();
			expMap = expDao.read();
		}
		if( cimList == null ) {
			CIMDao cimDao = new CIMDao();
//			cimList = cimDao.read();
		}
		
        Set< Entry< String, JsonObject >> es = allEveDef.entrySet();
        Iterator< Entry< String, JsonObject >> itr = es.iterator();
		LinkedList< String > allIndInEvent = new LinkedList< String >();
		LinkedHashSet< String > finalList = new LinkedHashSet< String >();

        while( itr.hasNext() ) {
        	Entry< String, JsonObject > entry = itr.next();
        	JsonObject thisEvent = entry.getValue();
        	String id = entry.getKey();
        	Set< String > allIndsUsed = getIndicatorsInExpression( id );
        	if( allIndsUsed != null )
    		    allIndInEvent.addAll( allIndsUsed );
    		
			//iterate over the unVisitedInd list and expand them until all are expanded.
			String anIndInEvent = null;
			LinkedList< String > visited = new LinkedList< String >();
			while( ! allIndInEvent.isEmpty() && ( anIndInEvent = allIndInEvent.remove() ) != null ){
				Set< String > allDependents = new LinkedHashSet< String >();
				gatherLeaves( anIndInEvent, allDependents, visited );
				finalList.addAll( allDependents );
			}
        }
        initialiseActive( finalList );
        return finalList;
	}
	
	
	private void initialiseActive( LinkedHashSet< String > finalList ) throws PExpressionException {
		expMapActive = new HashMap< String, PExpressionEvaluator >();
		allEveDefActive = new LinkedHashMap< String, JsonObject >();
		allIndDefActive = new LinkedHashMap< String, JsonObject >();
		PandaCache cache = new PandaCache();
		Iterator< String > itr = finalList.iterator();
		while( itr.hasNext() ) {
			String elem = itr.next();
			JsonObject activeInd =  allIndDef.get( elem );
			activeInd.addProperty( "status", "Active" );
			allIndDefActive.put( elem, activeInd );
			//Make it active in Persist
			cache.getClient().set( elem, 0, activeInd.toString() );
			PExpressionEvaluator expEval = expMap.get( elem );
//			String strExpression = expEval.getExpression();
//			Object[ ] paramNamesTypes = expDao.inferParamNamesTypes( strExpression, allIndDef, allEveDef, cimList );
//			expEval.setOptionalExpressionType( ExpressionUtil.stringToType( expEval.getOptionalExpressionType().getName() ) );
//			expEval.setParameters( ( String[ ] ) paramNamesTypes[ 0 ], ( Class[ ] ) paramNamesTypes[ 1 ] );
//			expEval.validateExpression();
			expMapActive.put( elem, expEval );
		}
		//iterate through the active events
		Set< String > keys = allEveDef.keySet();
		Iterator< String > itrKeys = keys.iterator();
		while( itrKeys.hasNext() ) {
			String aKey = itrKeys.next();
			JsonObject anEventDef = allEveDef.get( aKey );
			if( anEventDef.get( "status" ).getAsString().equals( "Active" ) ) {
				allEveDefActive.put( aKey, anEventDef );
				PExpressionEvaluator expEval = expMap.get( aKey );
//				String strExpression = expEval.getExpression();
//				Object[ ] paramNamesTypes = expDao.inferParamNamesTypes( strExpression, allIndDef, allEveDef, cimList );
//				expEval.setOptionalExpressionType( ExpressionUtil.stringToType( expEval.getOptionalExpressionType().getName() ) );
//				expEval.setParameters( ( String[ ] ) paramNamesTypes[ 0 ], ( Class[ ] ) paramNamesTypes[ 1 ] );
//				expEval.validateExpression();
				expMapActive.put( aKey, expEval );
			}
		}
	}

	private void gatherLeaves( String key, Set< String > allDependents, LinkedList< String > visited  ) {
		if( visited.contains( key ) )
			return;
		visited.add( key );
		Set< String > thisDep = getIndicatorsInExpression( key );
		if( thisDep == null ) {
			allDependents.add( key );
			return;
		}	
		//for all dependents, gather leaves.
		Iterator< String > itr = thisDep.iterator();
		while( itr.hasNext() ) {
			String aChild = itr.next();
			gatherLeaves( aChild, allDependents, visited );
		}
		allDependents.add( key );
	}
	
	
	private Set< String > getIndicatorsInExpression( String key ) {
		//System.out.println( "Getting All Indicators for : " + key );
		PExpressionEvaluator exp = expMap.get( key );
		if( exp == null )
			return null;
		String[ ] params = exp.getParameterNames();
		Set< String > indInEvent = new LinkedHashSet< String >();
		for( int i=0; i<params.length; i++ ) {
			String thisParam = params[i];
			if( thisParam.equals( key ) )
				continue;
			if( allIndDef.get( thisParam ) != null ) //this is an Indicator
				indInEvent.add( thisParam );
		}
        return indInEvent;
	}

	
	public static void main( String[ ] arg ) throws IOException, ParseException, PExpressionException, URISyntaxException {
		if( arg.length < 6 ) {
			System.out
					.println( "Usage : siftInit PersistAddressList indConfFile eveConfFile cimConfFile indTagFile eveTagFile" );
			return;
		}

		String strPersistAddressList = arg[ 0 ];
		LinkedList< URI > uris = new LinkedList< URI >();
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );
		for( int i = 0; i < arrPersistAddress.length; i++ )
			uris.add( new URI( arrPersistAddress[ i ] ) );

		SiftInitFuncCim handler = new SiftInitFuncCim();
		PandaCache cache = new PandaCache( uris );

		handler.indConfFile = arg[ 1 ];
		handler.eveConfFile = arg[ 2 ];
		handler.cimConFile = arg[ 3 ];
		handler.indTagFile = arg[ 4 ];
		handler.eveTagFile = arg[ 5 ];
		handler.datainitpersistFile = arg[ 6 ];
		handler.eventsTag = arg[ 7 ];

		handler.initialise();

//		System.out.println( "--------------------- Setting up Initial Data in Persist --------------------------" );
//		handler.initData();
//		System.out.println( "---------------------  Sift Init Completed  --------------------------" );
////		handler.initEventTagList();
//		System.out.println( "---------------------  Sift Init tag Lists Completed  --------------------------" );
//		
//		System.out.println( "---------------------  Getting Indicator Sequence  --------------------------" );
//		LinkedHashSet< String > list = handler.prepareActiveIndicatorsEventsExpressions();
//		System.out.println( "---------------------  Active Indicators  --------------------------" );
//		Iterator< String > itr = list.iterator();
//		while( itr.hasNext() ) {
//			System.out.println( itr.next() );
//		}
//		
//		System.out.println( "---------------------  Active Indicator Definitions  --------------------------" );
//		Set< String > keys = handler.allIndDefActive.keySet();
//		Iterator< String > itrKeys = keys.iterator();
//		int i=0;
//		while( itrKeys.hasNext() ) {
//			System.out.println( ++i + ". " +  itrKeys.next() );
//		}
//
//		System.out.println( "---------------------  Active Event Definitions  --------------------------" );
//		keys = handler.allEveDefActive.keySet();
//		itrKeys = keys.iterator();
//		i =0;
//		while( itrKeys.hasNext() ) {
//			System.out.println( ++i + ". " +  itrKeys.next() );
//		}
//
//		System.out.println( "---------------------  Active Expressions  --------------------------" );
//		keys = handler.expMapActive.keySet();
//		itrKeys = keys.iterator();
//		i = 0;
//		while( itrKeys.hasNext() ) {
//			System.out.println( ++i + ". " +  itrKeys.next() );
//		}
	}

	private void initData() throws IOException {
		CSVReader reader = new CSVReader( new FileReader( this.datainitpersistFile ) );
		String[ ] line = null;
		while( ( line = reader.readNext() ) != null ) {
			System.out.println( "Working on : " + line[ 0 ] );
			if( line[0].contains( "telco-" ) )
				System.out.println( "content : " + line[ 1 ] );
			new PandaCache().getClient().set( line[ 0 ], 0, line[ 1 ] );
		}
	}

	private void initEventTagList() throws IOException {
		CSVReader reader = new CSVReader( new FileReader( this.eventsTag ) );
		String[ ] line = null;
		while( ( line = reader.readNext() ) != null ) {
			JsonObject aTag = new JsonObject();
			JsonParser parser = new JsonParser();
			String msisdn = line[ 0 ];
			String eventId = line[ 1 ];
			String strStartDate = line[ 2 ];
			String strEndDate = line[ 3 ];
			String duration = line[ 4 ];
			String parameterValue = line[ 5 ];
			String productId = line[ 6 ];

			Long lStartDate = null, lEndDate = null, lDuration = null;
			SimpleDateFormat df = new SimpleDateFormat( "yyyyMMddHHmmss" );
			if( strStartDate != null ) {
				try {
					lStartDate = df.parse( strStartDate ).getTime();
				}catch( ParseException e ) {
					e.printStackTrace();
				}
			}
			if( strEndDate != null ) {
				try {
					lEndDate = df.parse( strEndDate ).getTime();
				}catch( ParseException e ) {
					e.printStackTrace();
				}
			}
			if( duration != null && duration.trim().length() > 0 )
				lDuration = Long.parseLong( duration );

			aTag.addProperty( "startTimestamp", lStartDate );
			aTag.addProperty( "endTimestamp", lEndDate );
			if( lDuration != null )
				aTag.addProperty( "duration", lDuration );
			if( parameterValue != null && parameterValue.trim().length() > 0 )
				aTag.addProperty( "parameterValue", parameterValue );

			if( productId != null && productId.trim().length() > 0 )
				aTag.addProperty( "productId", productId );

			CouchbaseClient client = new PandaCache().getClient();
			String hisTagList = ( String ) client.get( "EventTagList-" + msisdn );
			JsonObject jTagList = null;
			if( hisTagList == null ) {
				jTagList = new JsonObject();
				jTagList.addProperty( "docType", "TagList" );
			}else {
				jTagList = ( JsonObject ) parser.parse( hisTagList );
			}

			jTagList.add( eventId, aTag );

			new PandaCache().getClient().set( "EventTagList-" + msisdn, 0, jTagList.toString() );
		}
	}
}
