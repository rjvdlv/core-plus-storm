package com.knowesis.sift.api;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Set;

import com.google.gson.JsonObject;
import com.knowesis.sift.Common.PandaCache;
import com.knowesis.sift.api.dao.EventDefDao;
import com.knowesis.sift.expression.PExpressionException;

public class SiftInitTagAll {

	public static void main( String[ ] arg ) throws IOException, ParseException, PExpressionException, URISyntaxException {
		if( arg.length < 2 ) {
			System.out
					.println( "Usage : siftInit PersistAddressList save" );
			return;
		}

		String strPersistAddressList = arg[ 0 ];
		LinkedList< URI > uris = new LinkedList< URI >();
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );
		for( int i = 0; i < arrPersistAddress.length; i++ )
			uris.add( new URI( arrPersistAddress[ i ] ) );

		PandaCache cache = new PandaCache( uris );
		String save = arg[ 1 ];

		EventDefDao eveDao = new EventDefDao();
		LinkedHashMap< String, JsonObject > allEveDef = eveDao.readActive();
		
		Set< String > keys = allEveDef.keySet();
		Iterator< String > itrKeys = keys.iterator();
		JsonObject jTagListForAll = new JsonObject();
		jTagListForAll.addProperty( "docType", "TagList" );
		while( itrKeys.hasNext() ) {
			String aKey = itrKeys.next();
			JsonObject anEventDef = allEveDef.get( aKey );
			boolean taggedForAll = anEventDef.get( "taggedForAll" ).getAsBoolean();
			if( taggedForAll ) {
//				{
//					  "PR_MBB_STR_10": {
//					    "startTimestamp": 1439715346807,
//					    "endTimestamp": 1439715346807
//					  },
//					  "PP_HH_STR_21_A": {
//					    "startTimestamp": 1439715513688,
//					    "endTimestamp": 1439715513688
//					  }
//				}
				JsonObject thisTag = new JsonObject();
				thisTag.addProperty( "startTimestamp", anEventDef.get( "eventStartTime" ).getAsLong() );
				thisTag.addProperty( "endTimestamp", anEventDef.get( "eventEndTime" ).getAsLong() );
				jTagListForAll.add( anEventDef.get( "id" ).getAsString(), thisTag );
			}
		}
		if( save.equals( "yes" ) ) {
			cache.getClient().set( "EventTagList-ALL", 0, jTagListForAll.toString() );
			System.out.println( "SAVED EventTagList-ALL" );
		}
		System.out.println( jTagListForAll.toString() );
	}


}
