package com.knowesis.sift.api.resource;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.knowesis.sift.Common.PandaCache;
/**
 * @author Raja SP
 * 
 */
public class TelcoIndicatorResource extends ServerResource {

	String serviceType;
	String serviceSubType;
	String measurementType;
	String frequency;
    String range;
    String operation;
    String instanceId;
	
	@Override
	protected void doInit() throws ResourceException {
		serviceType = ( String ) getRequest().getAttributes().get( "serviceType" );
		serviceSubType = ( String ) getRequest().getAttributes().get( "serviceSubType" );
		measurementType = ( String ) getRequest().getAttributes().get( "measurementType" );
		frequency = ( String ) getRequest().getAttributes().get( "frequency" );
		range = ( String ) getRequest().getAttributes().get( "range" );
		operation = ( String ) getRequest().getAttributes().get( "operation" );
		instanceId = ( String ) getRequest().getAttributes().get( "instanceId" );
	}
	

	@Override
	protected Representation get() throws ResourceException {
		
		System.out.println( "In Telco Indicator Get Method - " + serviceType + " - " + serviceSubType + " - " + measurementType + 
				" - " + frequency + " - " + operation );
		//create Indicator key
		
		Calendar cal = Calendar.getInstance();
		cal.setTime( new java.util.Date() );
		JsonParser parser = new JsonParser();
		long thisYear = cal.get( Calendar.YEAR ) - 2000;
		
		//Get All Telco Indicators
		PandaCache cache = new PandaCache();
		String strNumInstances = ( String ) cache.getClient().get( "SiftCoreInstances" );
		int nNumInstances = Integer.parseInt( ( ( JsonObject )parser.parse( strNumInstances ) ).get( "instances" ).getAsString() );
		ArrayList< JsonObject > telcoIndicatorObjects = new ArrayList< JsonObject >();
		for( int i = 1; i <= nNumInstances; i++ ) {
			String thisInd = ( String ) cache.getClient().get( "telco-" + i + "-Indicators" );
			telcoIndicatorObjects.add(  ( JsonObject )parser.parse( thisInd ) );
		}
		
		if( serviceType == null ) {
			
			if( instanceId != null ) {
				int id = Integer.parseInt( instanceId );
				return new JsonRepresentation( telcoIndicatorObjects.get( id - 1 ).toString() );
			}
			
			JsonObject consolidatedObj = new JsonObject();

			JsonObject reference = telcoIndicatorObjects.get( 0 );
			Set< Entry< String, JsonElement >> es = reference.entrySet();
			Iterator< Entry< String, JsonElement >> itr = es.iterator();
			while( itr.hasNext() ) {
				Entry< String, JsonElement > entry = itr.next();
				String key = entry.getKey();
				if( key.startsWith( "VOICE" ) || key.startsWith( "SMS" ) || key.startsWith( "DATA" ) || 
						key.startsWith( "CREDIT" ) || key.startsWith( "TOTAL" ) ) {
					double sum = 0;
					for( int i = 0; i <= nNumInstances - 1; i++ ) {
						JsonElement thisElem = telcoIndicatorObjects.get( i ).get( key );
						if( thisElem != null )
						    sum += thisElem.getAsDouble();
					}
					consolidatedObj.addProperty( key, sum );
				}
			}
			//System.out.println( consolidatedObj.toString() );
			return new JsonRepresentation( consolidatedObj.toString() );
		}
		
		String[] rangeTokens = this.range.split( "-" );
		int start = Integer.parseInt( rangeTokens[0] );
		int limit = Integer.parseInt( rangeTokens[1] );

		JsonArray result = new JsonArray();
		String prefix = serviceType + "_" + serviceSubType + "_" + measurementType + "_" + frequency + "-";
		String indicatorName = null;
		for( int i = start; i < limit; i++ ) {
			if( frequency.equalsIgnoreCase( "Hourly" ) ||  frequency.equalsIgnoreCase( "LifeTimeHourly" ) ) {
				indicatorName = prefix + i + "H";
			} else if( frequency.equalsIgnoreCase( "Daily" ) ) {
				long numParam = new Long( cal.get( Calendar.DAY_OF_YEAR ) );
				indicatorName = prefix + ( numParam - i ) + "D" + thisYear;
			} 
			
			double sum = 0;
			for( int j=0; j<telcoIndicatorObjects.size(); j++ ) {
				JsonObject thisIndInstance = telcoIndicatorObjects.get( j );
				System.out.println( "Getting Indicator : " + indicatorName );
				JsonElement thisElem = thisIndInstance.get( indicatorName );
				if( thisElem != null )
				    sum += thisElem.getAsDouble();
			}
			result.add( new JsonPrimitive( sum ) );
		}
		
		JsonRepresentation output = null;
		if( operation == null )
			output = new JsonRepresentation( result.toString() );
		else if( operation.equals( "max" ) ) {
			double max = result.get( 0  ).getAsDouble();
			for( int i=0; i<result.size(); i++ ) {
				if( max < result.get( i ).getAsDouble() )
					max = result.get( i ).getAsDouble();
			}
			JsonArray maxResult = new JsonArray();
			maxResult.add( new JsonPrimitive( max ) );
			output = new JsonRepresentation( maxResult.toString() );
		} else if(  operation.equals( "min" ) ) { 
			double min = result.get( 0  ).getAsDouble();
			for( int i=0; i<result.size(); i++ ) {
				if( min > result.get( i ).getAsDouble() )
					min = result.get( i ).getAsDouble();
			}
			JsonArray minResult = new JsonArray();
			minResult.add( new JsonPrimitive( min ) );
			output = new JsonRepresentation( minResult.toString() );
		} else if( operation.equals( "mean" ) ) {
			double sum = 0;
			int totalEntries = 0;
			for( int i=0; i<result.size(); i++ ) {
				sum += result.get( i ).getAsDouble();
				if( result.get( i ).getAsDouble() > 0 )
					totalEntries ++;
			}
			if( totalEntries == 0 )
				totalEntries = 1;
			JsonArray meanArr = new JsonArray();
			meanArr.add( new JsonPrimitive( sum / totalEntries ) );
			output = new JsonRepresentation( meanArr.toString() );
		}
		return output;
	}

	/**
	 * @param args
	 */
	public static void main( String[ ] args ) {
		// TODO Auto-generated method stub

	}

}
