package com.knowesis.sift.api.resource;

import java.io.IOException;
import java.net.URLDecoder;

import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

import com.knowesis.sift.api.dao.ExpressionDao;
import com.knowesis.sift.expression.PExpressionException;
/**
 * @author Raja SP
 * 
 */
public class ExpressionResource extends ServerResource {

//	private String expression;
//	private String type;

	@Override
	protected void doInit() throws ResourceException {
//		expression = ( String ) getRequest().getAttributes().get( "expression" );
//		type = ( String ) getRequest().getAttributes().get( "type" );
	}

	/*
	@Override
	protected Representation post( Representation entity ) throws ResourceException {
		// TODO Auto-generated method stub
		Form expressionForm = new Form( entity );
		//EXPRESSION REFERING TO SELF- VALIDATION
		String id = expressionForm.getFirstValue( "id" );
		String type = expressionForm.getFirstValue( "type" );
		String expression =  expressionForm.getFirstValue( "expression" );
		
		System.out.println( "Vaidate Expression Called : " + expression + " --Type-- " + type );
		StringRepresentation output = null;
		ExpressionDao expDao = null;
		try {
			expDao = new ExpressionDao();
		}catch( PExpressionException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch( IOException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			expression = URLDecoder.decode( expression, "UTF-8" );
			System.out.println( "Incoming Expression : " + expression );
			System.out.println( "Incoming Expression Type : " + type );
			System.out.println( "Incoming Expression Id : " + id );
			
			//EXPRESSION REFERING TO SELF- VALIDATION
			expDao.validateExpression( id, expression, type, null );
		}catch( Exception exception ) {
			output = new StringRepresentation( exception.getMessage() );
			return output;
		}
		output = new StringRepresentation( "SUCCESS" );
		return output;
	}
	
	*/

//	@Override
//	protected Representation get() throws ResourceException {
//		System.out.println( "GET Called" );
//		StringRepresentation output = null;
//		ExpressionDao expDao = null;
//		try {
//			expDao = new ExpressionDao();
//		}catch( PExpressionException e ) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}catch( IOException e ) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		try {
//			expression = URLDecoder.decode( expression, "UTF-8" );
//			System.out.println( "Incoming Expression : " + expression );
//			System.out.println( "Incoming Expression Type : " + type );
//			
//			expDao.validateExpression( expression, type, null );
//		}catch( Exception exception ) {
//			output = new StringRepresentation( exception.getMessage() );
//			return output;
//		}
//		output = new StringRepresentation( "SUCCESS" );
//		return output;
//	}
	
	
}
