package com.knowesis.sift.api.resource;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.restlet.data.Form;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

import com.couchbase.client.CouchbaseClient;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.PandaCache;
/**
 * @author Raja SP
 * 
 */
public class TagListResource extends ServerResource {

	private String msisdn;

	@Override
	protected void doInit() throws ResourceException {
		msisdn = ( String ) getRequest().getAttributes().get( "msisdn" );
	}

	@Override
	protected Representation get() throws ResourceException {
		System.out.println( "GET Called" );
		String result = ( String ) new PandaCache().getClient().get( "EventTagList-" + msisdn );
		if( result == null )
			result = "ERROR : TagList Not Found";
		JsonRepresentation joutput = new JsonRepresentation( result );
		return joutput;
	}

	@Override
	protected Representation post( Representation entity ) throws ResourceException {
		System.out.println( "POST Called" );
		Form tagListForm = new Form( entity );

		JsonObject aTag = new JsonObject();
		JsonParser parser = new JsonParser();
		String eventId = tagListForm.getFirstValue( "eventId" );
		String strStartDate = tagListForm.getFirstValue( "startTimestamp" );
		String strEndDate = tagListForm.getFirstValue( "endTimestamp" );
		String duration = tagListForm.getFirstValue( "duration" );
		String parameterValue = tagListForm.getFirstValue( "parameterValue" );
		String productId = tagListForm.getFirstValue( "productId" );

		Long lStartDate = null, lEndDate = null, lDuration = null;
		SimpleDateFormat df = new SimpleDateFormat( "yyyyMMddHHmmss" );
		if( strStartDate != null ) {
			try {
				lStartDate = df.parse( strStartDate ).getTime();
			}catch( ParseException e ) {
				e.printStackTrace();
			}
		}
		if( strEndDate != null ) {
			try {
				lEndDate = df.parse( strEndDate ).getTime();
			}catch( ParseException e ) {
				e.printStackTrace();
			}
		}
		if( duration != null )
			lDuration = Long.parseLong( duration );

		aTag.addProperty( "startTimestamp", lStartDate );
		aTag.addProperty( "endTimestamp", lEndDate );
		if( lDuration != null )
			aTag.addProperty( "duration", lDuration );
		if( parameterValue != null )
			aTag.addProperty( "parameterValue", parameterValue );
		if( productId != null )
			aTag.addProperty( "producctId", productId );

		CouchbaseClient client = new PandaCache().getClient();
		String hisTagList = ( String ) client.get( "EventTagList-" + msisdn );
		JsonObject jTagList = null;
		if( hisTagList == null ) {
			jTagList = new JsonObject();
			jTagList.addProperty( "docType", "TagList" );
		}else {
			jTagList = ( JsonObject ) parser.parse( hisTagList );
		}

		jTagList.add( eventId, aTag );

		// new PandaCache().getClient().set( "EventTagList-" + msisdn, 0,
		// jTagList.toString() );

		StringRepresentation output = new StringRepresentation( "SUCCESS_ACCEPTED" );
		return output;
	}

	@Override
	protected Representation put( Representation entity ) throws ResourceException {
		System.out.println( "PUT Called" );
		return super.put( entity );
	}

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main( String[ ] args ) throws IOException {
		URL url = new URL( "http://127.0.0.1:8111/api/v1/tagList/rajanumber" );
		HttpURLConnection con = ( HttpURLConnection ) url.openConnection();
		con.setRequestMethod( "POST" );

		String urlParams = "msisdn=rajanumber" + "&" + "eventId=Topup500Get100MinsFree"
				+ "&startTimestamp=20131028000000" + "&endTimestamp=20131028235900" + "&parameterValue=500";
		con.setDoOutput( true );
		DataOutputStream wr = new DataOutputStream( con.getOutputStream() );
		wr.writeBytes( urlParams );
		wr.flush();
		wr.close();
		int responseCode = con.getResponseCode();
		System.out.println( responseCode );
		BufferedReader in = new BufferedReader( new InputStreamReader( con.getInputStream() ) );
		String inputLine;
		StringBuffer response = new StringBuffer();

		while( ( inputLine = in.readLine() ) != null ) {
			response.append( inputLine );
		}
		System.out.println( response.toString() );
		in.close();
	}
}
