package com.knowesis.sift.api.resource;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import org.restlet.data.Form;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

import au.com.bytecode.opencsv.CSVWriter;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.knowesis.sift.Common.PandaCache;
import com.knowesis.sift.api.APIServer;
import com.knowesis.sift.api.dao.EventDefDao;
import com.knowesis.sift.api.dao.PandagramDefDao;

public class PandagramDefResource extends ServerResource {

	private String pandagramId;

	@Override
	protected void doInit() throws ResourceException {
		pandagramId = ( String ) getRequest().getAttributes().get( "pandagramId" );
	}

	@Override
	protected Representation get() throws ResourceException {
		System.out.println( "GET Called : " + pandagramId );
		StringRepresentation output = null;
		if( pandagramId == null ) {
			LinkedHashMap< String, JsonObject > pandagramMap = new PandagramDefDao().read();
			Collection< JsonObject > allPandagramDefs = pandagramMap.values();
			output = new StringRepresentation( allPandagramDefs.toString() );
		}else {
			try {
				pandagramId =  URLDecoder.decode( pandagramId, "UTF-8" );
			}catch( UnsupportedEncodingException e ) {
				e.printStackTrace();
			}
			String pandagramDef = ( String ) new PandaCache().getClient().get( pandagramId );
			output = new StringRepresentation( pandagramDef );
		}
		return output;
	}

	@Override
	protected Representation post( Representation entity ) throws ResourceException {
        try {
			//return handlePost( pandagramDefForm );
        	Form programDefForm = new Form( entity );
        	String program = programDefForm.getFirstValue( "program" );
        	System.out.println( "Program Content : " + program );
			return handlePostAfrica2( program );
		}catch( IOException e ) {
			// TODO Auto-generated catch block
			throw new ResourceException(0);
		}
	}
	
	public Representation handlePostAfrica2( String jData )  throws IOException {
		JsonObject jobj = ( JsonObject ) new JsonParser().parse(  jData );
		jobj.addProperty( "docType", "Pandagram" );
		
		String pandagramId = null;
		String status = "Active";
		JsonArray nodeDataArray = jobj.get( "nodeDataArray" ).getAsJsonArray();
		if( nodeDataArray != null ) {
			for( int i=0; i<nodeDataArray.size(); i++ ) {
				JsonObject node = ( JsonObject ) nodeDataArray.get( i );
				String category = node.get( "category" ).getAsString();
				if( category.equals( "Program" ) ) {
				    pandagramId = node.get( "programId" ).getAsString();
				    status = node.get( "programStatus" ).getAsString();
				    break;
				}    
			}
		}
		
		if( pandagramId == null ) {
			StringRepresentation output = new StringRepresentation( "MISSING PANDAGRAM ID" );
			return output;		
		}
		jobj.addProperty( "pandagramId", pandagramId );
		jobj.addProperty( "pandagramName", pandagramId );
		jobj.addProperty( "status", status );

		System.out.println( "Persisting Pandagram : " + jobj.toString() );
		new PandaCache().getClient().set( pandagramId, 0, jobj.toString() );
		StringRepresentation output = new StringRepresentation( "SUCCESS" );
		return output;		
	}

	public Representation handlePost( Form form ) throws IOException {

		JsonObject jFull = new JsonObject();
		String pandagramId = form.getFirstValue( "txtProgramId" );
		System.out.println( pandagramId );

		JsonObject jobj = new JsonObject();

		jFull.addProperty( "docType", "Pandagram" );
		jFull.addProperty( "pandagramId", pandagramId );
		jFull.addProperty( "pandagramName", form.getFirstValue( "txtProgramName" ) );
		jFull.addProperty( "status", form.getFirstValue( "selActive" ) );
		
		String fromDate = form.getFirstValue( "fromDate" );
		String fromTime = form.getFirstValue( "fromTime" );
		if( fromTime == null || fromTime.trim().length() == 0  )
		    fromTime = "00:00:00";
		if( fromDate != null && fromDate.trim().length() > 0 ) {
			fromDate += "-" + fromTime;
			SimpleDateFormat df = new SimpleDateFormat( "dd-MM-yyyy-HH:mm:ss" );
			try {
				jFull.addProperty( "startDate", df.parse( fromDate ).getTime() );
			}catch( ParseException e ) {
				e.printStackTrace();
			}
		}
		
		String toDate = form.getFirstValue( "toDate" );
		String toTime = form.getFirstValue( "toTime" );
		if( toTime == null || toTime.trim().length() == 0  )
			toTime = "00:00:00";
		if( toDate != null && toDate.trim().length() > 0 ) {
			toDate += "-" + toDate;
			SimpleDateFormat df = new SimpleDateFormat( "dd-MM-yyyy-HH:mm:ss" );
			try {
				jFull.addProperty( "endDate", df.parse( toDate ).getTime() );
			}catch( ParseException e ) {
				e.printStackTrace();
			}
		}

		
		jFull.add( "presentationElement", jobj );

		JsonObject nodes = new JsonObject();
		jFull.add( "nodes", nodes );

		EventDefResource eventRes = new EventDefResource();

		for( int i = 0; i < 5; i++ ) {
			
//			[[txtProgramId=NewProgram], [txtProgramName=NewProgram], [txtProgramStartDate=], [txtProgramEndDate=], 
			
//			 [eventsListMain0=CELL_BASED_STIMULATION],
//			 [actionType0=provisionOffer], 
				//[eventsListNext1=], [eventNotificationMessage0=], 
			//[eventNotificationChannel0=SMS], [productSelection0=CreditBonus], [couponSelection0=N.A.], [quantity0=10], [provisionNotificationMessage0=10 Credited], [provisionNotificationChannel0=SMS], 
			
			//[eventsListMain1=], [actionType1=monitorNextEvent], [eventsListNext2=], [eventNotificationMessage1=], [eventNotificationChannel1=SMS], [productSelection1=N.A.], 
//			 [couponSelection1=N.A.], [quantity1=], [provisionNotificationMessage1=], [provisionNotificationChannel1=SMS], [eventsListMain2=], 
//			 [actionType2=monitorNextEvent], [eventsListNext3=], [eventNotificationMessage2=], [eventNotificationChannel2=SMS], [productSelection2=N.A.], 
//			 [couponSelection2=N.A.], [quantity2=], [provisionNotificationMessage2=], 
//			 [provisionNotificationChannel2=SMS], [txtCreated=19-10-2014 14:55:52], [txtLastUpd=19-10-2014 14:55:52], [selActive=Active], [txtAuthor=Raja]]
			
			String eventId = form.getFirstValue( "eventsListMain" + i );
			if( eventId != null && eventId.trim().length() > 0 ) {
				//copy the event details from persist
				JsonObject mainEvent = ( JsonObject ) nodes.get( eventId );
				
				if( mainEvent == null ) {
					mainEvent = includePandagramInEvent( pandagramId, eventId, i );
					//eventRes.manageEvent( eventId, mainEvent );
					
					mainEvent = new JsonObject();
					mainEvent.addProperty( "pandagramCategory", "Event" );
					nodes.add( eventId, mainEvent );
				}  

				String actionType = form.getFirstValue( "actionType" + i );
				String notificationMsg = "";
				String notificationChannel = "";
				if( actionType.equals( "provisionOffer" ) ) {
    				JsonObject anOffer = new JsonObject();
    				///[productSelection0=CreditBonus], [couponSelection0=N.A.], [quantity0=10], [provisionNotificationMessage0=10 Credited]
    				String productId = form.getFirstValue( "productSelection" + i );
    				String productType = "Core";
    				if( productId.equals( "N.A." ) ) {
    					productId = form.getFirstValue( "couponSelection" + i );
    					productType = "Coupon";
    				}    

    				anOffer.addProperty( "productId", productId );
    				String offerId = pandagramId + "-Offer-" + productId;
    				anOffer.addProperty( "productType", productType );
    				anOffer.addProperty( "productParameter",form.getFirstValue( "quantity" + i ));
    				//anOffer.addProperty( "validDuration", aNode.get( "validDuration" ).getAsString() );
    				//anOffer.addProperty( "keyInDiagram", aNode.get( "key" ).getAsInt() );
    				anOffer.addProperty( "pandagramCategory", "Offer" );
    				nodes.add( offerId, anOffer );
    				
    				notificationMsg = form.getFirstValue( "provisionNotificationMessage" + i );
    				notificationChannel = form.getFirstValue( "provisionNotificationChannel" + i );
    				
    				JsonObject link = new JsonObject();
    				link.addProperty( "to", offerId );
    				link.addProperty( "message", notificationMsg );
    				link.addProperty( "channel", notificationChannel );
    				mainEvent.add( "SUCCESS_TRIGGER", link );
    				
				} else {
					String followEvent = form.getFirstValue( "eventsListNext" + (i+1) );
					if( nodes.get( followEvent ) == null ) {
						System.out.println( "Working on Followup Event : " + followEvent + " for - " + eventId );
						JsonObject jfollowEvent = includePandagramInEvent( pandagramId, followEvent, (i+1) );
						//eventRes.manageEvent( followEvent, jfollowEvent );

						JsonObject eventobj = new JsonObject();
						eventobj.addProperty( "pandagramCategory", "Event" );
						String sDur = form.getFirstValue( "duration" + (i+1) );
						if( sDur != null && sDur.trim().length() > 0 )
							eventobj.addProperty( "duration", Integer.parseInt( sDur ) );
						nodes.add( followEvent, eventobj );
					}
    				JsonObject link = new JsonObject();
    				link.addProperty( "to", followEvent );
    				link.addProperty( "message", form.getFirstValue( "eventNotificationMessage" + i ) );
    				link.addProperty( "channel", form.getFirstValue( "eventNotificationChannel" + i ) );
    				mainEvent.add( "SUCCESS_TRIGGER", link );
				}
			}
		}
		
		new PandaCache().getClient().set( pandagramId, 0, jFull.toString() );
		
		// Post the entry to Worker Nodes - id, type, operation
		for( int i = 0; i < APIServer.streamsConfigPath.size(); i++ ) {
    		String thisPath = APIServer.streamsConfigPath.get( i );
    		String fileName = thisPath + "/" + "Pandagram-" + new java.util.Date().getTime() + ".csv";
    		try {
    			CSVWriter writer = new CSVWriter( new FileWriter( fileName ), '|' );
    			String[ ] line = new String[ 4 ];
    			line[ 0 ] = pandagramId;
    			line[ 1 ] = "PandagramDefinition";
    			line[ 2 ] = "C";
    			line[ 3 ] = null;
    			writer.writeNext( line );
    			writer.close();
    		}catch( Exception e ) {
    			throw new ResourceException( Status.CLIENT_ERROR_NOT_ACCEPTABLE, e.toString() );
    		}
		}
		StringRepresentation output = new StringRepresentation( "SUCCESS" );
		return output;		
	}
	
	
	private JsonObject includePandagramInEvent( String pandagramId, String eventId, int i ) {
		EventDefDao eventDao = new EventDefDao();
		JsonParser parser = new JsonParser();

		String thisEvent = eventDao.read( eventId );
		JsonObject jEvent = ( JsonObject ) parser.parse( thisEvent );
		
		if( i == 0 )
			jEvent.addProperty( "taggedForAll", "true" );
		else
			jEvent.addProperty( "taggedForAll", "false" );
		
		JsonElement elem = jEvent.get( "programs" );
		JsonArray programs = null;
		if( elem != null )
			programs = elem.getAsJsonArray();
		else
			programs = new JsonArray();
		
		programs.add( new JsonPrimitive( pandagramId ) );
		jEvent.add( "programs", programs );
		return jEvent;
	}
	

	public Representation handlePostAfrica( Form pandagramDefForm ) {
		JsonParser parser = new JsonParser();
		String jstr = pandagramDefForm.getFirstValue( "pandagramJson" );
		System.out.println( jstr.toString() );
		if( jstr == null )
			return new StringRepresentation( "ERROR : Missing Diagram Descriptor Json" );

		JsonObject jFull = new JsonObject();
		JsonObject jobj = ( JsonObject ) parser.parse( jstr );
		String pandagramId = null;
		JsonElement jPandagramId = jobj.get( "pandagramId" );
		if( jPandagramId == null ) {
			pandagramId = "SiftAfrica-" + new Random().nextInt(100) + 1;
		} else 
		    pandagramId = jPandagramId.getAsString();
		System.out.println( pandagramId );

		jFull.addProperty( "docType", "Pandagram" );
		jFull.addProperty( "pandagramId", pandagramId );
		jFull.add( "presentationElement", jobj );

		JsonArray nodeArray = jobj.get( "nodeDataArray" ).getAsJsonArray();
		if( nodeArray == null )
			return new StringRepresentation( "ERROR : Empty Diagram" );

		JsonObject nodes = new JsonObject();
		jFull.add( "nodes", nodes );

		EventDefResource eventRes = new EventDefResource();

		for( int i = 0; i < nodeArray.size(); i++ ) {
			JsonObject aNode = ( JsonObject ) nodeArray.get( i );
			String category = aNode.get( "category" ).getAsString();
			if( category.equals( "Event" ) ) {
				String eventId = aNode.get( "id" ).getAsString();
				eventId = pandagramId + "::" + eventId;
				String name = aNode.get( "text" ).getAsString();
				String expression = aNode.get( "expression" ).getAsString();
				String status = aNode.get( "status" ).getAsString();
				String executionMode = aNode.get( "executionMode" ).getAsString();
				boolean taggedForAll = aNode.get( "taggedForAll" ).getAsBoolean();
				JsonObject anEvent = new JsonObject();
				anEvent.addProperty( "docType", "EventDefinition" );
				anEvent.addProperty( "id", eventId );
				anEvent.addProperty( "name", name );
				anEvent.addProperty( "expression", expression );
				anEvent.addProperty( "expressionType", "boolean" );	
				anEvent.addProperty( "taggedForAll", taggedForAll );
				anEvent.addProperty( "executionMode", executionMode );
				anEvent.addProperty( "status", status );
				anEvent.addProperty( "keyInDiagram", aNode.get( "key" ).getAsInt() );
				anEvent.addProperty( "category", category );
				
				nodes.add( eventId, anEvent );

				//eventRes.manageEvent( eventId, anEvent );

			}else if( category.equals( "Offer" ) ) {
				JsonObject anOffer = new JsonObject();
				String offerId = aNode.get( "id" ).getAsString();
				anOffer.addProperty( "id", offerId );
				anOffer.addProperty( "name", aNode.get( "text" ).getAsString() );
				anOffer.addProperty( "productId", aNode.get( "productId" ).getAsString() );
				anOffer.addProperty( "productType", aNode.get( "productType" ).getAsString() );
				anOffer.addProperty( "productParameter", aNode.get( "productParameter" ).getAsString() );
				anOffer.addProperty( "validDuration", aNode.get( "validDuration" ).getAsString() );
				anOffer.addProperty( "keyInDiagram", aNode.get( "key" ).getAsInt() );
				anOffer.addProperty( "category", category );

				nodes.add( offerId, anOffer );
			}
		}
		
		JsonArray linkArray = jobj.get( "linkDataArray" ).getAsJsonArray();
		if( linkArray == null )
			return new StringRepresentation( "ERROR : Empty Links in Diagram" );
		
		for( int i=0; i<linkArray.size(); i++ ) {
			JsonObject aLink = ( JsonObject ) linkArray.get( i );
			int nFrom = aLink.get( "from" ).getAsInt();
			int nTo = aLink.get( "to" ).getAsInt();
			
			//find the from and to nodes and add the connection
			Set< Entry< String, JsonElement >> es = nodes.entrySet();
			Iterator< Entry< String, JsonElement >> itr = es.iterator();
			String fromNode = null;
			String toNode = null;
			while( itr.hasNext() ) {
				Entry< String, JsonElement > entry = itr.next();
				JsonObject aNode = ( JsonObject ) entry.getValue();
				if( aNode.get( "keyInDiagram" ).getAsInt() == nFrom ) {
					fromNode = entry.getKey();
				} else if( aNode.get( "keyInDiagram" ).getAsInt() == nTo ) {
					toNode = entry.getKey();
				}
			}
			JsonObject jfromNode = ( JsonObject ) nodes.get( fromNode );
			
			String startPort = aLink.get( "fromPort" ).getAsString();
			JsonObject linkObject = new JsonObject();
			linkObject.addProperty( "to", toNode );
			linkObject.addProperty( "message", aLink.get( "text" ).getAsString() );
			linkObject.addProperty( "channel", aLink.get( "channel" ).getAsString()  );
			String linkKey = "SUCCESS_TRIGGER";
			if( startPort.equals( "T" ) ) {
				linkKey = "FAIL_TRIGGER";
			} else if( startPort.equals( "B" ) ) {
				linkKey = "PARTIAL_TRIGGER";
			}
			jfromNode.add( linkKey, linkObject );
		}
		
		new PandaCache().getClient().set( pandagramId, 0, jFull.toString() );
		
		// Post the entry to Worker Nodes - id, type, operation
		for( int i = 0; i < APIServer.streamsConfigPath.size(); i++ ) {
    		String thisPath = APIServer.streamsConfigPath.get( i );
    		String fileName = thisPath + "/" + "Pandagram-" + new java.util.Date().getTime() + ".csv";
    		try {
    			CSVWriter writer = new CSVWriter( new FileWriter( fileName ), '|' );
    			String[ ] line = new String[ 4 ];
    			line[ 0 ] = pandagramId;
    			line[ 1 ] = "PandagramDefinition";
    			line[ 2 ] = "C";
    			line[ 3 ] = null;
    			writer.writeNext( line );
    			writer.close();
    		}catch( Exception e ) {
    			throw new ResourceException( Status.CLIENT_ERROR_NOT_ACCEPTABLE, e.toString() );
    		}
		}

		
		StringRepresentation output = new StringRepresentation( "SUCCESS_ACCEPTED" );
		return output;		
	}
	
	public void manageCategory( String category ) {
		if( category.trim().length() == 0 )
			return;
		String[ ] cat = category.split( "," );
		for( int i = 0; i < cat.length; i++ ) {
			String thisCat = cat[ i ];
			JsonObject jobj = new JsonObject();
			jobj.addProperty( "docType", "EventCategory" );
			jobj.addProperty( "id", thisCat );
			new PandaCache().getClient().set( "EventCategory-" + thisCat.replace( " ", "" ), 0, jobj.toString() );
		}

	}

	@Override
	protected Representation put( Representation entity ) throws ResourceException {
		System.out.println( "PUT Called" );
		return super.put( entity );
	}

	public Representation postEvent( Form pandagramDefForm ) throws IOException {
		System.out.println( "POST Called" );
        return handlePost( pandagramDefForm );
	}

	public static void main( String arg[] ) throws IOException {

		File file = new File( "/home/stream/PandaData/pandagramconf.csv" );
		StringBuilder fileContents = new StringBuilder( ( int ) file.length() );
		Scanner scanner = new Scanner( file );
		String lineSeparator = System.getProperty( "line.separator" );

		try {
			while( scanner.hasNextLine() ) {
				fileContents.append( scanner.nextLine() + lineSeparator );
			}
		}finally {
			scanner.close();
		}

		String data = fileContents.toString();
		
		String url = "http://localhost:8111/api/v1/pandagram";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
		//add reuqest header
		con.setRequestMethod("POST");
		String urlParameters =  "pandagramJson=" + URLEncoder.encode( data, "UTF-8" );
 
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
 
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);
 
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
 
		//print result
		System.out.println(response.toString());
		
		
		
//		
//		
//		Form form = new Form();
//		form.add( "pandagramJson", data );
//
//		System.out.println( data );
//		PandagramDefResource eveDef = new PandagramDefResource();
//		Representation output = eveDef.postEvent( form );
//		System.out.println( output.toString() );
	}

}
