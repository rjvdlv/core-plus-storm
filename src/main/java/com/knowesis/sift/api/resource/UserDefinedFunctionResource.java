package com.knowesis.sift.api.resource;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.knowesis.sift.api.dao.FunctionDao;
import com.knowesis.sift.api.dao.UserDefinedFunctionDao;
import com.knowesis.sift.api.model.function;

/**
 * @author Raja SP
 * 
 */
public class UserDefinedFunctionResource extends ServerResource {

	private String functionName;

	@Override
	protected void doInit() throws ResourceException {
		functionName = ( String ) getRequest().getAttributes().get( "functionName" );
		if( functionName != null )
			try {
				functionName = URLDecoder.decode( functionName, "UTF-8" );
			}catch( UnsupportedEncodingException e ) {
				e.printStackTrace();
			}
	}

	@Override
	protected Representation get() throws ResourceException {
		System.out.println( "GET Called : " + this.functionName );
		JsonRepresentation output = null;
		UserDefinedFunctionDao dao = new UserDefinedFunctionDao();
		JsonObject functions = dao.read();
		if( functionName == null ) {
			output = new JsonRepresentation( functions.toString() );
		}else {
			if( functions.get( functionName ) != null )
			    output = new JsonRepresentation( functions.get( functionName ).toString() );
			else
				output = new JsonRepresentation( "{ \"status\" : \"Not Found\" }" );
		}
		return output;
	}

	/**
	 * @param args
	 */
	public static void main( String[ ] args ) {
		// TODO Auto-generated method stub

	}

}
