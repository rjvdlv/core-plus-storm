package com.knowesis.sift.api.resource;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Iterator;
import java.util.Set;

import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.knowesis.sift.api.dao.CIMDao;
import com.knowesis.sift.api.model.CIM;
/**
 * @author Raja SP
 * 
 */
public class CIMResource extends ServerResource {

	private String fieldName;

	@Override
	protected void doInit() throws ResourceException {
		fieldName = ( String ) getRequest().getAttributes().get( "fieldName" );
		if( fieldName != null )
			try {
				fieldName = URLDecoder.decode( fieldName, "UTF-8" );
			}catch( UnsupportedEncodingException e ) {
				e.printStackTrace();
			}
	}

	@Override
	protected Representation get() throws ResourceException {
		System.out.println( "GET Called : " + this.fieldName );
		JsonRepresentation output = null;
		CIMDao dao = new CIMDao();
		java.util.HashMap<String,  JsonObject > cim = null;
		JsonArray res = new JsonArray();
		cim = dao.read();
		if( cim != null ) {
			Set< String > keys = cim.keySet();
			Iterator< String > iterator = keys.iterator();
			while( iterator.hasNext() ) {
				String key = iterator.next();
				JsonObject thisElem = cim.get( key );
				res.add( thisElem );
			}
		}
		if( fieldName == null ) {
			output = new JsonRepresentation( res.toString() );
		}else {
			output = new JsonRepresentation( cim.get( fieldName ) );
		}
		return output;
	}

	/**
	 * @param args
	 */
	public static void main( String[ ] args ) {
		// TODO Auto-generated method stub

	}

}
