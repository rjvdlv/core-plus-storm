package com.knowesis.sift.api.resource;

import java.io.FileWriter;
import java.io.IOException;

import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

import au.com.bytecode.opencsv.CSVWriter;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.api.APIServer;
import com.knowesis.sift.api.dao.IndEveInstanceDao;

public class IndicatorInstanceResource extends ServerResource {

	private String msisdn;
	private String indicatorName;

	@Override
	protected void doInit() throws ResourceException {
		msisdn = ( String ) getRequest().getAttributes().get( "msisdn" );
		indicatorName = ( String )getRequest().getAttributes().get( "indicatorName" );
	}

	@Override
	protected Representation get() throws ResourceException {
		IndEveInstanceDao dao = new IndEveInstanceDao();
		String result = dao.read( msisdn, "Indicator" );
		if( result == null )
			result = "ERROR : Indicator Not Found";
		else {
			JsonParser parser = new JsonParser();
			if( indicatorName != null ) {
				JsonObject jInd = ( JsonObject ) parser.parse( result );
				JsonElement val = jInd.get( indicatorName );
				if( val == null ) {
					result = "ERROR : Indicator Not Found";
				}
				else if( val.isJsonArray() ) {
					result = val.getAsJsonArray().toString();
				}	
				else {
					result = val.getAsString();
				}  
			}
		}
		StringRepresentation output = new StringRepresentation( result );
		return output;
	}
	
	@Override
	protected Representation post( Representation entity ) throws ResourceException {
		System.out.println( "POST Called" );
		String ind;
		try {
			ind = entity.getText();
		}catch( IOException e ) {
			e.printStackTrace();
			StringRepresentation output = new StringRepresentation( "FAILED : " + e.getMessage() );
			return output;
		}
		System.out.println( "MSISDN : " + msisdn + " -  Indicator Submitted : " + ind );
		IndEveInstanceDao dao = new IndEveInstanceDao();
		dao.update( msisdn, ind, "Indicator" );
		// Post the entry to Worker Nodes - id, type, operation
		for( int i = 0; i < APIServer.streamsConfigPath.size(); i++ ) {
			String thisPath = APIServer.streamsConfigPath.get( i );
			String fileName = thisPath + "/" + "IndicatorInstance-" + new java.util.Date().getTime() + ".csv";
			try {
				CSVWriter writer = new CSVWriter( new FileWriter( fileName ), '|' );
				String[ ] line = new String[ 4 ];
				line[ 0 ] = msisdn;
				line[ 1 ] = "IndicatorInstance";
				line[ 2 ] = "U";
				line[ 3 ] = null;
				writer.writeNext( line );
				writer.close();
			}catch( Exception e ) {
				e.printStackTrace();
				StringRepresentation output = new StringRepresentation( "FAILED : " + e.getMessage() );
				return output;
			}
		}

		StringRepresentation output = new StringRepresentation( "SUCCESS_ACCEPTED" );
		return output;
	}	


}
