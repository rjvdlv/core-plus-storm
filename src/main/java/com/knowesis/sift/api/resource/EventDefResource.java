package com.knowesis.sift.api.resource;

import java.io.FileWriter;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

import org.restlet.data.Form;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

import au.com.bytecode.opencsv.CSVWriter;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.PandaCache;
import com.knowesis.sift.Common.PandaCacheConfig;
import com.knowesis.sift.api.APIServer;
import com.knowesis.sift.api.dao.EventDefDao;
import com.knowesis.sift.api.dao.IndicatorDefDao;
/**
 * @author Raja SP
 * 
 */
public class EventDefResource extends ServerResource {

	private String eventTypeId;

	@Override
	protected void doInit() throws ResourceException {
		eventTypeId = ( String ) getRequest().getAttributes().get( "eventTypeId" );
	}

	@Override
	protected Representation get() throws ResourceException {
		System.out.println( "GET Called" );
		StringRepresentation output = null;
		EventDefDao dao = new EventDefDao();
		if( eventTypeId == null ) {
			LinkedHashMap< String, JsonObject > eventDefs = dao.read();
			Collection< JsonObject > allEventDefs = eventDefs.values();
			output = new StringRepresentation( allEventDefs.toString() );
		}else {
			String eventDef = dao.read( eventTypeId );
			output = new StringRepresentation( eventDef );
		}
		return output;
	}
	
	

	@Override
	protected Representation post( Representation entity ) throws ResourceException {
		System.out.println( "POST Called" );
		// read all the values from the form

		// "id": "HeavyIDDUser",
		// "description": "Heavy IDD User",
		// "category": "",
		// "maxTriggerLimit": "1",
		// "eventStartTime": 1375340400000,
		// "eventEndTime": 1377846000000,
		// "eventMonitoringDuration": "",
		// "targetSystemID": "",
		// "author": "Raja",
		// "requireNoEvent": "FALSE",
		// "enableRecuringEvent": "FALSE",
		// "monitoringThreshold": "-1",
		// "expressionType": "boolean",
		// "expression": "Voice_IDD_Count_Daily > 3",
		// "status": "Active",
		// "creationDate": 1378173590594,
		// "lastUpateDate": 1378173590594

		// Create a Json object
		// Call ExpressionDao.update( id, jsonStr ) method.
		// Write a File into the File System where Streams is monitoring

		/*
		 * event id -c event name -c event description (new) - c event category
		 * (new) - c Status Max Events in a Day - c
		 * 
		 * Max Notifications in a day - not needed
		 * 
		 * Start Timestamp - c End Timestamp -c
		 * 
		 * Recurring Yes/No - c Number of Recurrence - c
		 * 
		 * NoEvent required Yes/No - c
		 * 
		 * PartialEvent Required Yes/No - c Partial Event Count -c
		 * 
		 * Provisioning Sources list Output Destinations List
		 * 
		 * Indicators List to be sent
		 * 
		 * Expression
		 * 
		 * Created DateTime Last Updated DateTime Last updated user
		 */

		Form eventDefForm = new Form( entity );
		
		System.out.println( eventDefForm.toString() );
		
		JsonObject jEventObj = new JsonObject();
		jEventObj.addProperty( "docType", "EventDefinition" );

		String id = eventDefForm.getFirstValue( "id" );
		jEventObj.addProperty( "id", id );
		jEventObj.addProperty( "name", eventDefForm.getFirstValue( "name" ) );
		jEventObj.addProperty( "description", eventDefForm.getFirstValue( "description" ) );

		String category = eventDefForm.getFirstValue( "category" );
		if( category != null )
			jEventObj.addProperty( "category", category );

		String maxTrigger = eventDefForm.getFirstValue( "maxTriggerLimit" );
		if( maxTrigger != null && maxTrigger.trim().length() > 0 )
		    jEventObj.addProperty( "maxTriggerLimit", Integer.parseInt( maxTrigger )  );

		String strDate = eventDefForm.getFirstValue( "eventStartTime" );
//		SimpleDateFormat dtFormat = new SimpleDateFormat( "yyyyMMddHHmmss" );
//		java.util.Date startDateTime;
//		try {
//			startDateTime = dtFormat.parse( strDate );
//		}catch( ParseException e1 ) {
//			throw new ResourceException( Status.CLIENT_ERROR_NOT_ACCEPTABLE, "Error Parsing Start Date" );
//		}
		long lstartDate = Long.parseLong( eventDefForm.getFirstValue( "eventStartTime" ) );
		jEventObj.addProperty( "eventStartTime", lstartDate );

		long lendDate = Long.parseLong( eventDefForm.getFirstValue( "eventEndTime" ) );
		jEventObj.addProperty( "eventEndTime", lendDate );

		jEventObj.addProperty( "priovisioningSourceID", eventDefForm.getFirstValue( "priovisioningSourceID" ) );
		jEventObj.addProperty( "targetSystemID", eventDefForm.getFirstValue( "targetSystemID" ) );

		jEventObj.addProperty( "requireNoEvent", eventDefForm.getFirstValue( "requireNoEvent" ) );
		
		jEventObj.addProperty( "taggedForAll",  eventDefForm.getFirstValue( "taggedForAll" ) );

		jEventObj.addProperty( "eventPayload",  eventDefForm.getFirstValue( "eventPayload" ) );
		
		
		jEventObj.addProperty( "enableRecuringEvent", eventDefForm.getFirstValue( "enableRecuringEvent" ) );
		
		String recCount = eventDefForm.getFirstValue( "recurringCount" );
		if( recCount != null && recCount.trim().length() > 0 )
		    jEventObj.addProperty( "recurringCount", Integer.parseInt( recCount ) );

		jEventObj.addProperty( "enableIntermediateEvent", eventDefForm.getFirstValue( "enableIntermediateEvent" ) );

		JsonObject intermediateEventsDef = null;
		for( int i = 0; i < 5; i++ ) {
			String intermediateEventId = eventDefForm.getFirstValue( "intermediateEventId" + i );
			String intermediateEventExpression = eventDefForm.getFirstValue( "intermediateEventExpression" + i );
			String intermediateEventScheduleCount = eventDefForm.getFirstValue( "intermediateEventScheduleCount" + i );
			String intermediateEventScheduleUnit = eventDefForm.getFirstValue( "intermediateEventScheduleUnit" + i );
			String intermediateEventRemarks = eventDefForm.getFirstValue( "intermediateEventRemarks" + i );

			if( intermediateEventId != null ) {
				JsonObject jIntermediateEvent = new JsonObject();
				jIntermediateEvent.addProperty( "intermediateEventId", intermediateEventId );
				jIntermediateEvent.addProperty( "intermediateEventExpression", intermediateEventExpression );
				jIntermediateEvent.addProperty( "intermediateEventScheduleCount", intermediateEventScheduleCount );
				jIntermediateEvent.addProperty( "intermediateEventScheduleUnit", intermediateEventScheduleUnit );
				jIntermediateEvent.addProperty( "intermediateEventRemarks", intermediateEventRemarks );

				if( i == 0 ) {
					intermediateEventsDef = new JsonObject();
					jEventObj.add( "intermediateEventsDef", intermediateEventsDef );
				}
				intermediateEventsDef.add( intermediateEventId, jIntermediateEvent );
			}
		}

		//jEventObj.addProperty( "monitoringThreshold", eventDefForm.getFirstValue( "monitoringThreshold" ) );

		jEventObj.addProperty( "expressionType", eventDefForm.getFirstValue( "expressionType" ) );
		String expression =  eventDefForm.getFirstValue( "expression" );
		jEventObj.addProperty( "expression", expression );
		
		String elemScriptOrExpression = eventDefForm.getFirstValue( "scriptOrExpression" );
		if( elemScriptOrExpression == null ) {
			elemScriptOrExpression = "expression";
			String[] lines = elemScriptOrExpression.split( ";" );
			if( lines.length > 1 )
				elemScriptOrExpression = "script";
		}
		jEventObj.addProperty( "scriptOrExpression", elemScriptOrExpression );
		
		jEventObj.addProperty( "status", eventDefForm.getFirstValue( "status" ) );
		String execMode = eventDefForm.getFirstValue( "executionMode" );
		if( execMode == null || execMode.length() == 0 )
			execMode = "RealTime";
		jEventObj.addProperty( "executionMode", execMode );
		jEventObj.addProperty( "variables", eventDefForm.getFirstValue( "variables" ) );
		jEventObj.addProperty( "values", eventDefForm.getFirstValue( "values" ) );

		jEventObj.addProperty( "outputIndicatorsList", eventDefForm.getFirstValue( "outputIndicatorsList" ) );
		
		jEventObj.addProperty( "dependantEvent", false );

		long ldate = Long.parseLong( eventDefForm.getFirstValue( "creationDate" ) );
		jEventObj.addProperty( "creationDate", ldate );

		ldate = Long.parseLong( eventDefForm.getFirstValue( "lastUpateDate" ) );
		jEventObj.addProperty( "lastUpateDate", ldate );
		
		//PAYLOAD
		if( eventDefForm.getFirstValue( "selEventIntInd" ) != null )
		    jEventObj.addProperty( "eventPayload", eventDefForm.getFirstValue( "selEventIntInd" ) );
		
		String campaigns =  eventDefForm.getFirstValue( "campaigns" );
		jEventObj.addProperty( "campaigns", campaigns );
		
		String interactionPoint =  eventDefForm.getFirstValue( "interactionPoint" );
		jEventObj.addProperty( "interactionPoint", interactionPoint );
		
		
		System.out.println( "Submitted Event Data : " + jEventObj.toString() );

		/*
		EventDefDao dao = new EventDefDao();
		IndicatorDefDao indDao = new IndicatorDefDao();
		JsonParser parser = new JsonParser();
		try {
			dao.update( id, jEventObj.toString() );
			System.out.println( "---------------Updated Event -------------- : " + id );
			Set< String > allIndInEvent = dao.getIndicatorsInEvent( id );
			System.out.println( "-------------- Number of Indicators in Event : ------------- " + allIndInEvent.size() );
			Iterator< String > itr = allIndInEvent.iterator();
			while( itr.hasNext() ) {
				String ind = itr.next();
				System.out.println( "---------------Activating Indicator : --------------- " + ind );
				String strInd = indDao.read( ind );
				JsonObject jInd = ( JsonObject ) parser.parse( strInd );
				String status = jInd.get( "status" ).getAsString();
				if( ! status.equals( "Active" ) ) {
				    jInd.addProperty( "status", "Active" );
				    indDao.update( ind, jInd.toString() );
				}
			}
		}catch( Exception e ) {
			System.out.println( "ERROR Creating Event : " + e.toString() );
			throw new ResourceException( Status.CLIENT_ERROR_NOT_ACCEPTABLE, e.toString() );
		}
		*/
		
		String status = jEventObj.get( "status" ).getAsString();
		String operation = "C";
		if( status.equals( "DELETED" ) ||  status.equals( "InActive" ) )
			operation = "D";

		// Post the entry to Worker Nodes - id, type, operation
		for( int i = 0; i < APIServer.streamsConfigPath.size(); i++ ) {
			String thisPath = APIServer.streamsConfigPath.get( i );
			String fileName = thisPath + "/" + "Event-" + new java.util.Date().getTime() + ".csv";
			try {
				CSVWriter writer = new CSVWriter( new FileWriter( fileName ), '|' );
				String[ ] line = new String[ 4 ];
				line[ 0 ] = id;
				line[ 1 ] = "EventDefinition";
				line[ 2 ] = operation;
				line[ 3 ] = null;
				writer.writeNext( line );
				writer.close();
			}catch( Exception e ) {
				throw new ResourceException( Status.CLIENT_ERROR_NOT_ACCEPTABLE, e.toString() );
			}
		}
		if( category != null )
			manageCategory( category );
		StringRepresentation output = new StringRepresentation( "SUCCESS_ACCEPTED" );
		return output;
	
	}


	public void manageCategory( String category ) {
		if( category.trim().length() == 0 )
			return;
		String[ ] cat = category.split( "," );
		for( int i = 0; i < cat.length; i++ ) {
			String thisCat = cat[ i ];
			JsonObject jobj = new JsonObject();
			jobj.addProperty( "docType", "EventCategory" );
			jobj.addProperty( "id", thisCat );
			new PandaCacheConfig().getClient().set( "EventCategory-" + thisCat.replace( " ", "" ), 0, jobj.toString() );
		}

	}

	@Override
	protected Representation put( Representation entity ) throws ResourceException {
		System.out.println( "PUT Called" );
		return super.put( entity );
	}
	

	public Representation postEvent( Form eventDefForm ) {
		JsonObject jEventObj = new JsonObject();
		jEventObj.addProperty( "docType", "EventDefinition" );

		String id = eventDefForm.getFirstValue( "id" );
		jEventObj.addProperty( "id", id );
		jEventObj.addProperty( "name", eventDefForm.getFirstValue( "name" ) );
		jEventObj.addProperty( "description", eventDefForm.getFirstValue( "description" ) );

		jEventObj.addProperty( "category", eventDefForm.getFirstValue( "category" ) );

		jEventObj.addProperty( "maxTriggerLimit", eventDefForm.getFirstValue( "maxTriggerLimit" ) );

		long lstartDate = Long.parseLong( eventDefForm.getFirstValue( "eventStartTime" ) );
		jEventObj.addProperty( "eventStartTime", lstartDate );

		long lendDate = Long.parseLong( eventDefForm.getFirstValue( "eventEndTime" ) );
		jEventObj.addProperty( "eventEndTime", lendDate );


		jEventObj.addProperty( "priovisioningSourceID", eventDefForm.getFirstValue( "priovisioningSourceID" ) );
		jEventObj.addProperty( "targetSystemID", eventDefForm.getFirstValue( "targetSystemID" ) );

		jEventObj.addProperty( "requireNoEvent", eventDefForm.getFirstValue( "requireNoEvent" ) );

		jEventObj.addProperty( "taggedForAll",  eventDefForm.getFirstValue( "taggedForAll" ) );
		
		jEventObj.addProperty( "enableRecuringEvent", eventDefForm.getFirstValue( "enableRecuringEvent" ) );
		jEventObj.addProperty( "recurringCount", eventDefForm.getFirstValue( "recurringCount" ) );

		jEventObj.addProperty( "enableIntermediateEvent", eventDefForm.getFirstValue( "enablePartialEvent" ) );

		JsonObject intermediateEventsDef = null;
		for( int i = 0; i < 5; i++ ) {
			String intermediateEventId = eventDefForm.getFirstValue( "intermediateEventId" + i );
			String intermediateEventExpression = eventDefForm.getFirstValue( "intermediateEventExpression" + i );
			String intermediateEventScheduleCount = eventDefForm.getFirstValue( "intermediateEventScheduleCount" + i );
			String intermediateEventScheduleUnit = eventDefForm.getFirstValue( "intermediateEventScheduleUnit" + i );
			String intermediateEventRemarks = eventDefForm.getFirstValue( "intermediateEventRemarks" + i );

			if( intermediateEventId != null ) {
				JsonObject jIntermediateEvent = new JsonObject();
				jIntermediateEvent.addProperty( "intermediateEventId", intermediateEventId );
				jIntermediateEvent.addProperty( "intermediateEventExpression", intermediateEventExpression );
				jIntermediateEvent.addProperty( "intermediateEventExpressionType", "boolean" );
				jIntermediateEvent.addProperty( "intermediateEventScheduleCount", intermediateEventScheduleCount );
				jIntermediateEvent.addProperty( "intermediateEventScheduleUnit", intermediateEventScheduleUnit );
				jIntermediateEvent.addProperty( "intermediateEventRemarks", intermediateEventRemarks );

				if( i == 0 ) {
					intermediateEventsDef = new JsonObject();
					jEventObj.add( "intermediateEventsDef", intermediateEventsDef );
				}
				intermediateEventsDef.add( intermediateEventId, jIntermediateEvent );
			}
		}

		jEventObj.addProperty( "monitoringThreshold", eventDefForm.getFirstValue( "monitoringThreshold" ) );

		jEventObj.addProperty( "expressionType", eventDefForm.getFirstValue( "expressionType" ) );
		jEventObj.addProperty( "expression", eventDefForm.getFirstValue( "expression" ) );
		jEventObj.addProperty( "status", eventDefForm.getFirstValue( "status" ) );
		String execMode = eventDefForm.getFirstValue( "executionMode" );
		if( execMode == null || execMode.length() == 0 )
			execMode = "RealTime";
		jEventObj.addProperty( "executionMode", execMode );
		jEventObj.addProperty( "variables", eventDefForm.getFirstValue( "variables" ) );
		jEventObj.addProperty( "values", eventDefForm.getFirstValue( "values" ) );

		jEventObj.addProperty( "outputIndicatorsList", eventDefForm.getFirstValue( "outputIndicatorsList" ) );

		long ldate = Long.parseLong( eventDefForm.getFirstValue( "creationDate" ) );
		jEventObj.addProperty( "creationDate", ldate );

		ldate = Long.parseLong( eventDefForm.getFirstValue( "lastUpateDate" ) );
		jEventObj.addProperty( "lastUpateDate", ldate );


		String category = eventDefForm.getFirstValue( "category" );
		if( category != null )
			jEventObj.addProperty( "category", category );

		/*
		EventDefDao dao = new EventDefDao();
		IndicatorDefDao indDao = new IndicatorDefDao();
		JsonParser parser = new JsonParser();
		try {
			dao.update( id, jEventObj.toString() );
			System.out.println( "---------------Updated Event -------------- : " + id );
			Set< String > allIndInEvent = dao.getIndicatorsInEvent( id );
			System.out.println( "-------------- Number of Indicators in Event : ------------- " + allIndInEvent.size() );
			Iterator< String > itr = allIndInEvent.iterator();
			while( itr.hasNext() ) {
				String ind = itr.next();
				System.out.println( "---------------Activating Indicator : --------------- " + ind );
				String strInd = indDao.read( ind );
				JsonObject jInd = ( JsonObject ) parser.parse( strInd );
				String status = jInd.get( "status" ).getAsString();
				if( ! status.equals( "Active" ) ) {
				    jInd.addProperty( "status", "Active" );
				    indDao.update( ind, jInd.toString() );
				}
			}
		}catch( Exception e ) {
			System.out.println( "ERROR Creating Event : " + e.toString() );
			throw new ResourceException( Status.CLIENT_ERROR_NOT_ACCEPTABLE, e.toString() );
		}
		*/

		String fileName1 = "/home/stream/pworkspace/SiFT_Source_data/data/update_handler/input_files/" + "Event-" + new java.util.Date().getTime() + ".csv";
		try {
			CSVWriter writer = new CSVWriter( new FileWriter( fileName1 ), '|' );
			String[ ] line = new String[ 4 ];
			line[ 0 ] = id;
			line[ 1 ] = "EventDefinition";
			line[ 2 ] = "C";
			line[ 3 ] = null;
			writer.writeNext( line );
			writer.close();
		}catch( Exception e ) {
			throw new ResourceException( Status.CLIENT_ERROR_NOT_ACCEPTABLE, e.toString() );
		}
		System.out.println( "------Created the Configuration Update File for Sift Core : " + fileName1 );

		
		
		// Post the entry to Worker Nodes - id, type, operation
		for( int i = 0; i < APIServer.streamsConfigPath.size(); i++ ) {
			String thisPath = APIServer.streamsConfigPath.get( i );
			String fileName = thisPath + "/" + "Event-" + new java.util.Date().getTime() + ".csv";
			try {
				CSVWriter writer = new CSVWriter( new FileWriter( fileName ), '|' );
				String[ ] line = new String[ 4 ];
				line[ 0 ] = id;
				line[ 1 ] = "EventDefinition";
				line[ 2 ] = "C";
				line[ 3 ] = null;
				writer.writeNext( line );
				writer.close();
			}catch( Exception e ) {
				throw new ResourceException( Status.CLIENT_ERROR_NOT_ACCEPTABLE, e.toString() );
			}
			System.out.println( "------Created the Configuration Update File for Sift Core : " + fileName );
		}
		if( category != null )
			manageCategory( category );
		StringRepresentation output = new StringRepresentation( "SUCCESS_ACCEPTED" );
		return output;	
	}

	/*
	public void manageEvent( String id, JsonObject jEventObj ) {
		EventDefDao dao = new EventDefDao();
		IndicatorDefDao indDao = new IndicatorDefDao();
		JsonParser parser = new JsonParser();
		try {
			dao.update( id, jEventObj.toString() );
			System.out.println( "---------------Updated Event -------------- : " + id );
			Set< String > allIndInEvent = dao.getIndicatorsInEvent( id );
			System.out.println( "-------------- Number of Indicators in Event : ------------- " + allIndInEvent.size() );
			Iterator< String > itr = allIndInEvent.iterator();
			while( itr.hasNext() ) {
				String ind = itr.next();
				System.out.println( "---------------Activating Indicator : --------------- " + ind );
				String strInd = indDao.read( ind );
				JsonObject jInd = ( JsonObject ) parser.parse( strInd );
				String status = jInd.get( "status" ).getAsString();
				if( ! status.equals( "Active" ) ) {
				    jInd.addProperty( "status", "Active" );
				    indDao.update( ind, jInd.toString() );
				}
			}
		}catch( Exception e ) {
			System.out.println( "ERROR Creating Event : " + e.toString() );
			throw new ResourceException( Status.CLIENT_ERROR_NOT_ACCEPTABLE, e.toString() );
		}
		

		// Post the entry to Worker Nodes - id, type, operation
		for( int i = 0; i < APIServer.streamsConfigPath.size(); i++ ) {
			String thisPath = APIServer.streamsConfigPath.get( i );
			String fileName = thisPath + "/" + "Event-" + new java.util.Date().getTime() + ".csv";
			try {
				CSVWriter writer = new CSVWriter( new FileWriter( fileName ), '|' );
				String[ ] line = new String[ 4 ];
				line[ 0 ] = id;
				line[ 1 ] = "EventDefinition";
				line[ 2 ] = "C";
				line[ 3 ] = null;
				writer.writeNext( line );
				writer.close();
			}catch( Exception e ) {
				throw new ResourceException( Status.CLIENT_ERROR_NOT_ACCEPTABLE, e.toString() );
			}
		}
	}
    */
	public static void main( String arg[] ) {
		Form form = new Form();
		form.add( "id", "HeavyIDDUserNewVersion" );
		form.add( "name", "Heavy IDD User Trigger" );
		form.add( "description", "If the IDD usage in a day is more than 3, this event will be triggered" );
		form.add( "category", "Daily, IDD, StimulateVoice" );
		form.add( "maxTriggerLimit", "1" );
		form.add( "eventStartTime", "20130101000000" );
		form.add( "eventEndTime", "20130801000000" );
		form.add( "targetSystemID", "Unica Interact" );
		form.add( "priovisioningSourceID", "Unica Interact" );
		form.add( "author", "Raja" );
		form.add( "requireNoEvent", "FALSE" );
		form.add( "enableRecuringEvent", "TRUE" );
		form.add( "recurringCount", "3" );
		form.add( "enablePartialEvent", "TRUE" );
		form.add( "partialEventCount", "2" );
		form.add( "outputIndicatorsList", "VOICE_IDD_Count_Daily, VOICE_IDD_Count_Weekly" );
		form.add( "executionMode", "RealTime" );
		form.add( "expressionType", "boolean" );
		form.add( "expression", "VOICE_IDD_Count_Daily > 3" );
		form.add( "status", "Active" );
		form.add( "creationDate", "20130101000000" );
		form.add( "lastUpateDate", "20130101000000" );

		form.add( "intermediateEventId0", "HeavyIDDUserNewVersion-Intermediate-001" );
		form.add( "intermediateEventExpression0", "VOICE_IDD_Count_Daily > 1" );
		form.add( "intermediateEventScheduleCount0", "2" );
		form.add( "intermediateEventScheduleUnit0", "day" );
		form.add( "intermediateEventRemarks0", "" );

		form.add( "intermediateEventId1", "HeavyIDDUserNewVersion-Intermediate-002" );
		form.add( "intermediateEventExpression1", "VOICE_IDD_Count_Daily > 2" );
		form.add( "intermediateEventScheduleCount1", "3" );
		form.add( "intermediateEventScheduleUnit1", "day" );
		form.add( "intermediateEventRemarks1", "" );

		form.add( "intermediateEventId2", "HeavyIDDUserNewVersion-Intermediate-003" );
		form.add( "intermediateEventExpression2", "VOICE_IDD_Count_Daily > 3" );
		form.add( "intermediateEventScheduleCount2", "3" );
		form.add( "intermediateEventScheduleUnit2", "day" );
		form.add( "intermediateEventRemarks2", "" );

		EventDefResource eveDef = new EventDefResource();
		Representation output = eveDef.postEvent( form );
		System.out.println( output.toString() );
	}

}
