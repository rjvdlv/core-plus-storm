package com.knowesis.sift.api.resource;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.TimeZone;

import com.knowesis.sift.expression.SiftMath;

import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.PandaCache;
/**
 * @author Raja SP
 * 
 */
public class EventStatsResource extends ServerResource {

	private String type;
	private String eventId;
	private String date;
	private String frequency;

	@Override
	protected void doInit() throws ResourceException {
		type = ( String ) getRequest().getAttributes().get( "type" );
		eventId = ( String ) getRequest().getAttributes().get( "eventId" );
		date = ( String ) getRequest().getAttributes().get( "date" );
		frequency = ( String ) getRequest().getAttributes().get( "frequency" );
	}

	@Override
	protected Representation get() throws ResourceException {
		String countType = "TriggerCounts";
		if( ! type.equals( "trigger" ) )
			countType = "MonitoringCounts";

		System.out.println( "Getting Trigge Counts : " + eventId + " - " + date + " - type : "  + type);

		JsonParser parser = new JsonParser();
		PandaCache cache = new PandaCache();
		String strNumInstances = ( String ) cache.getClient().get( "SiftCoreInstances" );
		int nNumInstances = Integer.parseInt( ( ( JsonObject )parser.parse( strNumInstances ) ).get( "instances" ).getAsString() );
		if( date.equals( "uptoNow" ) ) {
			long count = 0;
			for( int i = 1; i <= nNumInstances; i++ ) {
				String thisInd = ( String ) cache.getClient().get( "TELCO_" + eventId + "-" + i + "-" + countType );
				if( thisInd != null ) {
					JsonObject triggerCount =  ( JsonObject )parser.parse( thisInd );
					count += triggerCount.get( "count" ).getAsLong();
				}
			}
			return new StringRepresentation( new Long( count ).toString() );
		} else if( frequency.equals( "hourly" ) ) {
			SimpleDateFormat df = new SimpleDateFormat( "yyyyMMddHHmmss" );
			df.setTimeZone( TimeZone.getTimeZone( "GMT" ) );
			long timestamp;
			try {
				timestamp = df.parse( date + "000000" ).getTime();
			}catch( ParseException e ) {
				e.printStackTrace();
				return new StringRepresentation( "ERROR : Invalid Date" );
			}
			long[] result = new long[24];
			String key = "HourlyCount-" + timestamp;
			for( int i = 1; i <= nNumInstances; i++ ) {
				String[ ] eventsList = eventId.split( "\\*" );
				for( int k=0; k<eventsList.length; k++ ) {
					String thisInd = null;
					String thisEvent = eventsList[k];
					if( ! type.equals( "telco" ) )
						thisInd = ( String ) cache.getClient().get( "TELCO_" + thisEvent + "-" + i + "-" + countType );
					else {
						String telcoInd = ( String ) cache.getClient().get( "telco-" + i + "-Indicators" );
						System.out.println( telcoInd );
						if( telcoInd == null )
							continue;
						JsonObject jtel = ( JsonObject ) parser.parse( telcoInd );
						JsonElement eInd = jtel.get( thisEvent + "_ALL_Count");
						if( eInd != null )
						    thisInd = eInd.toString();
						else
							continue;
						System.out.println( thisInd );
					}
					if( thisInd != null ) {
						JsonObject triggerCount =  ( JsonObject )parser.parse( thisInd );
						JsonElement eHourly = triggerCount.get( key );
						if( eHourly == null ) {
							System.out.println( "Unable to find Hourly values : " + key );
							continue;
						}	
						JsonArray jHourly = eHourly.getAsJsonArray();
						for( int j=0; j<jHourly.size(); j++ ) {
							int hour = jHourly.get( j ).getAsJsonArray().get( 0 ).getAsInt();
							result[ hour ] += jHourly.get( j ).getAsJsonArray().get( 1 ).getAsLong();
						}
					} else
						System.out.println( "Unable to find indicator values : TELCO_" + eventId );
				}
			}
			return new StringRepresentation( Arrays.toString( result ) );
		} else if( frequency.equals( "daily" ) ) {
			String key = "DailyCount";
			String[] dates = date.split( "-" );
			SimpleDateFormat df = new SimpleDateFormat( "yyyyMMdd" );
			df.setTimeZone( TimeZone.getTimeZone( "GMT" ) );
			long start, end;
			try {
				start = df.parse( dates[0] ).getTime();
				end = df.parse( dates[1] ).getTime();
			}catch( ParseException e ) {
				e.printStackTrace();
				return new StringRepresentation( "ERROR : Invalid Date" );
			}
			long[] result = new long[ ( int ) ( ( end - start ) / SiftMath.msecPerDay + 1) ];
			for( int i = 1; i <= nNumInstances; i++ ) {
				String[ ] eventsList = eventId.split( "\\*" );
				for( int k=0; k<eventsList.length; k++ ) {
					String thisInd = null;
					String thisEvent = eventsList[k];
					if( ! type.equals( "telco" ) )
						thisInd = ( String ) cache.getClient().get( "TELCO_" + thisEvent + "-" + i + "-" + countType );
					else {
						String telcoInd = ( String ) cache.getClient().get( "telco-" + i + "-Indicators" );
						if( telcoInd == null )
							continue;
						JsonObject jtel = ( JsonObject ) parser.parse( telcoInd );
						JsonElement eInd = jtel.get( thisEvent + "_ALL_Count" );
						if( eInd != null )
							thisInd = eInd.toString();
						else
							continue;
						System.out.println( thisInd );
					}
					if( thisInd != null ) {
						JsonObject triggerCount =  ( JsonObject )parser.parse( thisInd );
						JsonElement eDaily = triggerCount.get( key );
						if( eDaily == null ) {
							System.out.println( "Unable to find Daily values : " + key );
							continue;
						}	
						JsonArray jDaily = eDaily.getAsJsonArray();
						for( int j=0; j<jDaily.size(); j++ ) {
							JsonArray thisElement = jDaily.get( j ).getAsJsonArray();
							long thisDate = thisElement.get( 0 ).getAsLong();
							if( thisDate >= start && thisDate <= end  )
								result[ ( int ) ( ( thisDate - start ) / SiftMath.msecPerDay ) ] += thisElement.get( 1 ).getAsLong();
						}
					}	
				}
			}	
			return new StringRepresentation( Arrays.toString( result ) );
		}
		return new StringRepresentation( new String() );
	}
}
