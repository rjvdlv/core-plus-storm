package com.knowesis.sift.api.resource;

import java.io.FileWriter;
import java.util.Collection;
import java.util.LinkedHashMap;
import org.restlet.data.Form;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
import au.com.bytecode.opencsv.CSVWriter;
import com.google.gson.JsonObject;
import com.knowesis.sift.api.APIServer;
import com.knowesis.sift.api.dao.IndicatorDefDao;

public class IndicatorDefResource extends ServerResource {

	private String indicatorTypeId;
	private String limits;

	@Override
	protected void doInit() throws ResourceException {
		indicatorTypeId = ( String ) getRequest().getAttributes().get( "indicatorTypeId" );
        limits = ( String ) getRequest().getAttributes().get( "limits" );  
	}

	@Override
	protected Representation get() throws ResourceException {
		System.out.println( "GET Called" );
		StringRepresentation output = null;
		IndicatorDefDao dao = new IndicatorDefDao();
		 LinkedHashMap< String, JsonObject > indDefs = null;
		if( indicatorTypeId == null ) {
			if( limits == null || limits.equals( "ALL" ) )
				indDefs = ( LinkedHashMap< String, JsonObject > ) dao.read();
			else
				indDefs = ( LinkedHashMap< String, JsonObject > ) dao.readPartial( limits );
			Collection< JsonObject > allIndDefs = indDefs.values();
			String result = allIndDefs.toString();
			output = new StringRepresentation( result );
		}else {
			String indDef = dao.read( indicatorTypeId );
			output = new StringRepresentation( indDef );
		}
		return output;
	}

	/*
	@Override
	protected Representation post( Representation entity ) throws ResourceException {
		System.out.println( "POST Called" );
		Form eventDefForm = new Form( entity );
		JsonObject jIndObj = new JsonObject();
		jIndObj.addProperty( "docType", "IndicatorDefinition" );

		String id = eventDefForm.getFirstValue( "id" );
		jIndObj.addProperty( "id", id );
		jIndObj.addProperty( "description", eventDefForm.getFirstValue( "description" ) );
		jIndObj.addProperty( "internalId", eventDefForm.getFirstValue( "internalId" ) );
		jIndObj.addProperty( "description", eventDefForm.getFirstValue( "description" ) );
		jIndObj.addProperty( "serviceType", eventDefForm.getFirstValue( "serviceType" ) );
		jIndObj.addProperty( "serviceSubType", eventDefForm.getFirstValue( "serviceSubType" ) );
		jIndObj.addProperty( "measurementType", eventDefForm.getFirstValue( "measurementType" ) );
		jIndObj.addProperty( "frequency", eventDefForm.getFirstValue( "frequency" ) );
		jIndObj.addProperty( "owner", eventDefForm.getFirstValue( "owner" ) );
		jIndObj.addProperty( "expressionType", eventDefForm.getFirstValue( "expressionType" ) );
		jIndObj.addProperty( "expression", eventDefForm.getFirstValue( "expression" ) );
		jIndObj.addProperty( "status", eventDefForm.getFirstValue( "status" ) );
		jIndObj.addProperty( "scriptOrExpression", eventDefForm.getFirstValue( "scriptOrExpression" ) );
		jIndObj.addProperty( "alternateNames", eventDefForm.getFirstValue( "alternateNames" ) );
		jIndObj.addProperty( "creationDate", eventDefForm.getFirstValue( "creationDate" ) );
		jIndObj.addProperty( "lastUpateDate", eventDefForm.getFirstValue( "lastUpateDate" ) );
		

		IndicatorDefDao dao = new IndicatorDefDao();
		try {
			dao.update( id, jIndObj.toString() );
		}catch( Exception e ) {
			throw new ResourceException( Status.CLIENT_ERROR_NOT_ACCEPTABLE, e.toString() );
		}
		// Post the entry to Worker Nodes - id, type, operation
		
		String status = jIndObj.get( "status" ).getAsString();
		String operation = "C";
		if( status.equals( "DELETED" ) ||  status.equals( "InActive" ) )
			operation = "D";
		
		for( int i = 0; i < APIServer.streamsConfigPath.size(); i++ ) {
			String thisPath = APIServer.streamsConfigPath.get( i );
			String fileName = thisPath + "/" + "Indicator-" + new java.util.Date().getTime() + ".csv";
			try {
				CSVWriter writer = new CSVWriter( new FileWriter( fileName ), '|' );
				String[ ] line = new String[ 4 ];
				line[ 0 ] = id;
				line[ 1 ] = "IndicatorDefinition";
				line[ 2 ] = operation;
				line[ 3 ] = null;
				writer.writeNext( line );
				writer.close();
			}catch( Exception e ) {
				System.out.println( e.toString() );
				throw new ResourceException( Status.CLIENT_ERROR_NOT_ACCEPTABLE, e.toString() );
			}
		}
		StringRepresentation output = new StringRepresentation( "SUCCESS_ACCEPTED" );
		return output;
	}
	*/

	@Override
	protected Representation put( Representation entity ) throws ResourceException {
		System.out.println( "PUT Called" );
		return super.put( entity );
	}

}
