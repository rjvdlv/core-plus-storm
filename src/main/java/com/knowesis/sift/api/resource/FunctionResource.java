package com.knowesis.sift.api.resource;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.knowesis.sift.api.dao.FunctionDao;
import com.knowesis.sift.api.model.function;

/**
 * @author Raja SP
 * 
 */
public class FunctionResource extends ServerResource {

	private String functionName;

	@Override
	protected void doInit() throws ResourceException {
		functionName = ( String ) getRequest().getAttributes().get( "functionName" );
		if( functionName != null )
			try {
				functionName = URLDecoder.decode( functionName, "UTF-8" );
			}catch( UnsupportedEncodingException e ) {
				e.printStackTrace();
			}
	}

	@Override
	protected Representation get() throws ResourceException {
		System.out.println( "GET Called : " + this.functionName );
		JsonRepresentation output = null;
		FunctionDao dao = new FunctionDao();
		HashMap< String, JsonObject > functions = dao.readAll();
		JsonArray res = new JsonArray();
		if( functions != null ) {
			Set< String > keys = functions.keySet();
			Iterator< String > iterator = keys.iterator();
			while( iterator.hasNext() ) {
				String key = iterator.next();
				JsonObject thisElem = functions.get( key );
				JsonObject functionCIM = new JsonObject();
				functionCIM.addProperty( "name", thisElem.get( "name" ).getAsString() );
				functionCIM.addProperty( "signature", thisElem.get( "signature" ).getAsString() );
				functionCIM.addProperty( "returnType", thisElem.get( "returnType" ).getAsString() );
				functionCIM.addProperty( "description", thisElem.get( "description" ).getAsString() );
				res.add( functionCIM );
			}
		}
		if( functionName == null ) {
			output = new JsonRepresentation( res.toString() );
		}else {
			JsonObject result = new JsonObject();
			JsonObject thisElem = functions.get( functionName );
			if( thisElem != null ) {
				result.addProperty( "name", thisElem.get( "name" ).getAsString() );
				result.addProperty( "signature", thisElem.get( "signature" ).getAsString() );
				result.addProperty( "returnType",thisElem.get( "returnType" ).getAsString() );
				result.addProperty( "description", thisElem.get( "description" ).getAsString() );
			}
			output = new JsonRepresentation( result.toString() );
		}
		return output;
	}

	/**
	 * @param args
	 */
	public static void main( String[ ] args ) {
		// TODO Auto-generated method stub

	}

}
