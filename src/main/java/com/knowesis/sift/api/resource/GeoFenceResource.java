package com.knowesis.sift.api.resource;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;

import org.restlet.data.Form;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.knowesis.sift.Common.PandaCache;
/**
 * @author Raja SP
 * 
 */
public class GeoFenceResource extends ServerResource {

	// router.attach( "/geoFenceGroup", GeoFenceZoneResource.class );
	// router.attach( "/geoFenceGroup/{geoFenceGroupName}",
	// GeoFenceZoneResource.class );
	// router.attach( "/geoFenceGroup/{geoFenceGroupName}/geoFenceZone",
	// GeoFenceZoneResource.class );
	// router.attach(
	// "/geoFenceGroup/{geoFenceGroupName}/geoFenceZone/GeoFenceZoneName",
	// GeoFenceZoneResource.class );

	@Override
	protected Representation delete() throws ResourceException {
		PandaCache cache = new PandaCache();
		JsonParser parser = new JsonParser();

		if( this.geoFenceGroupName == null || this.geoFenceZoneName == null ) {
			return new StringRepresentation( "ERROR : Missing GroupName or Zone Name" );
		}

		String strGeoFences = ( String ) cache.getClient().get( "GeoFences" );
		JsonObject jGeo = ( JsonObject ) parser.parse( strGeoFences );
		if( jGeo != null ) {
			JsonObject jGroup = ( JsonObject ) jGeo.get( geoFenceGroupName );
			if( jGroup == null )
				return new StringRepresentation( "ERROR : Group Name Does not Exist" );
			JsonElement jZone = jGroup.get( geoFenceZoneName );
			if( jZone == null )
				return new StringRepresentation( "ERROR : Zone Name Does not Exist" );
			// jGroup.remove( geoFenceZoneName );
			jGeo.remove( geoFenceGroupName );
			cache.getClient().set( "GeoFences", 0, jGeo.toString() );
			StringRepresentation output = new StringRepresentation( "SUCCESS_ACCEPTED" );
			return output;
		}else
			return new StringRepresentation( "ERROR : Missing GeoFences" );
	}

	private String geoFenceGroupName;
	private String geoFenceZoneName;

	@Override
	protected void doInit() throws ResourceException {
		geoFenceGroupName = ( String ) getRequest().getAttributes().get( "geoFenceGroupName" );
		if( geoFenceGroupName != null )
			try {
				geoFenceGroupName = URLDecoder.decode( geoFenceGroupName, "UTF-8" );
			}catch( UnsupportedEncodingException e ) {
				e.printStackTrace();
			}
		geoFenceZoneName = ( String ) getRequest().getAttributes().get( "geoFenceZoneName" );
		if( geoFenceZoneName != null )
			try {
				geoFenceZoneName = URLDecoder.decode( geoFenceZoneName, "UTF-8" );
			}catch( UnsupportedEncodingException e ) {
				e.printStackTrace();
			}
	}

	@Override
	protected Representation get() throws ResourceException {
		PandaCache cache = new PandaCache();
		JsonParser parser = new JsonParser();
		if( geoFenceGroupName == null ) {
			JsonArray result = new JsonArray();
			String thisFences = ( String ) cache.getClient().get( "GeoFences" );
			result.add( parser.parse( thisFences ) );
			return new JsonRepresentation( result.toString() );
		}else if( geoFenceZoneName == null ) {
			// only return all the zones in that geoFenceGroup
			String strGeoFences = ( String ) cache.getClient().get( "GeoFences" );
			JsonObject jGeo = ( JsonObject ) parser.parse( strGeoFences );
			JsonElement jGroup = jGeo.get( geoFenceGroupName );
			if( jGroup != null ) {
				JsonArray result = new JsonArray();
				result.add( jGroup );
				return new JsonRepresentation( result.toString() );
			}
		}else {
			String strGeoFences = ( String ) cache.getClient().get( "GeoFences" );
			JsonObject jGeo = ( JsonObject ) parser.parse( strGeoFences );
			if( jGeo != null ) {
				JsonObject jGroup = ( JsonObject ) jGeo.get( geoFenceGroupName );
				if( jGroup != null ) {
					JsonElement jZone = jGroup.get( geoFenceZoneName );
					if( jZone != null ) {
						JsonArray result = new JsonArray();
						result.add( jZone );
						return new JsonRepresentation( result.toString() );
					}
				}
			}
		}
		return new StringRepresentation( "Error : Group Name or Zone Name not found" );
	}

	@Override
	protected Representation post( Representation entity ) throws ResourceException {
		System.out.println( "POST Called" );
		if( geoFenceGroupName == null && geoFenceZoneName == null )
			return new StringRepresentation( "ERROR : Missing GroupName and Zone Name" );
		// read all the values from the form
		Form geoFenceDefForm = new Form( entity );
		PandaCache cache = new PandaCache();
		JsonParser parser = new JsonParser();
		String geoFenceObj = ( String ) cache.getClient().get( "GeoFences" );
		JsonObject jFences = null;
		if( geoFenceObj == null ) {
			jFences = new JsonObject();
		}else
			jFences = ( JsonObject ) parser.parse( geoFenceObj );

		JsonObject jGroup = ( JsonObject ) jFences.get( geoFenceGroupName );
		if( jGroup == null ) {
			jGroup = new JsonObject();
			jFences.add( geoFenceGroupName, jGroup );
		}

		String latlongPairs = geoFenceDefForm.getFirstValue( "coordinates" );
		System.out.println( latlongPairs );
		ArrayList< JsonArray > latlongs = this.splitLatLongs( latlongPairs );
		JsonObject thisZone = new JsonObject();
		thisZone.add( "lat", latlongs.get( 0 ) );
		thisZone.add( "long", latlongs.get( 1 ) );
		jGroup.add( geoFenceZoneName, thisZone );

		cache.getClient().set( "GeoFences", 0, jFences.toString() );
		StringRepresentation output = new StringRepresentation( "SUCCESS_ACCEPTED" );
		return output;
	}

	protected Representation post( Form geoFenceDefForm ) throws ResourceException {
		System.out.println( "POST Called" );
		if( geoFenceGroupName == null && geoFenceZoneName == null )
			return new StringRepresentation( "ERROR : Missing GroupName and Zone Name" );
		// read all the values from the form
		PandaCache cache = new PandaCache();
		JsonParser parser = new JsonParser();
		String geoFenceObj = ( String ) cache.getClient().get( "GeoFences" );
		JsonObject jFences = null;
		if( geoFenceObj == null ) {
			jFences = new JsonObject();
		}else
			jFences = ( JsonObject ) parser.parse( geoFenceObj );

		JsonObject jGroup = ( JsonObject ) jFences.get( geoFenceGroupName );
		if( jGroup == null ) {
			jGroup = new JsonObject();
			jFences.add( geoFenceGroupName, jGroup );
		}

		String latlongPairs = geoFenceDefForm.getFirstValue( "coordinates" );
		ArrayList< JsonArray > latlongs = this.splitLatLongs( latlongPairs );
		JsonObject thisZone = new JsonObject();
		thisZone.add( "lat", latlongs.get( 0 ) );
		thisZone.add( "long", latlongs.get( 1 ) );
		jGroup.add( geoFenceZoneName, thisZone );

		cache.getClient().set( "GeoFences", 0, jFences.toString() );
		StringRepresentation output = new StringRepresentation( "SUCCESS_ACCEPTED" );
		return output;
	}

	private ArrayList< JsonArray > splitLatLongs( String latlongPairs ) {
		// inputs will be of the form (lat1,long1)(lat2,long2)
		String[ ] pairs = latlongPairs.split( "\\)" );
		JsonArray latList = new JsonArray();
		JsonArray longList = new JsonArray();

		for( int i = 0; i < pairs.length; i++ ) {
			String aPair = pairs[ i ];
			if( aPair.trim().length() <= 0 )
				continue;
			aPair = aPair.replaceAll( "\\(\\s*", "" );
			String[ ] entries = aPair.split( "," );
			latList.add( new JsonPrimitive( Double.parseDouble( entries[ 0 ].trim() ) ) );
			longList.add( new JsonPrimitive( Double.parseDouble( entries[ 1 ].trim() ) ) );
		}
		ArrayList< JsonArray > res = new ArrayList< JsonArray >();
		res.add( latList );
		res.add( longList );
		return res;
	}

	/**
	 * @param args
	 */
	public static void main( String[ ] args ) {
		GeoFenceResource fence = new GeoFenceResource();
		ArrayList< JsonArray > res = fence.splitLatLongs( "    (1.2 , 1.3   )   ( 2.1,    2.2)   " );
		System.out.println( res.get( 0 ).toString() );
		System.out.println( res.get( 1 ).toString() );

		fence.geoFenceGroupName = "Group2";
		fence.geoFenceZoneName = "Zone2";
		Form form = new Form();
		form.add( "coordinates", "    (1.2 , 1.3   )   ( 2.1,    2.2)   " );
		fence.post( form );
	}

}
