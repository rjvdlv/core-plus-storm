package com.knowesis.sift.api.resource;

import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.api.dao.IndEveInstanceDao;
/**
 * @author Raja SP
 * 
 */
public class EventInstanceResource extends ServerResource {

	private String msisdn;
	private String eventId;
	
	@Override
	protected void doInit() throws ResourceException {
		msisdn = ( String ) getRequest().getAttributes().get( "msisdn" );
		eventId = ( String ) getRequest().getAttributes().get( "eventId" );
	}

	@Override
	protected Representation get() throws ResourceException {
        String result = null;
		if( msisdn == null ) {
			result = "ERROR : Missing MSISDN";
		} else {
    		IndEveInstanceDao dao = new IndEveInstanceDao();
    		result = dao.read( msisdn, "Event" );
    		if( result == null )
    			result = "ERROR : Event Not Found";
    		else {
    			JsonParser parser = new JsonParser();
    			if( eventId != null ) {
    				JsonObject jEvent = ( JsonObject ) parser.parse( result );
    				JsonElement val = jEvent.get( eventId );
    				if( val == null ) {
    					result = "ERROR : Event Not Found";
    				}
    				else if( val.isJsonArray() ) {
    					result = val.getAsJsonArray().toString();
    				}	
    				else {
    					result = val.getAsString();
    				}  
    			}
    		}
		}
		StringRepresentation output = new StringRepresentation( result );
		return output;
	}
}
