package com.knowesis.sift.api.resource;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.knowesis.sift.api.dao.EventCategoryDao;
import com.knowesis.sift.api.dao.EventDefDao;
/**
 * @author Raja SP
 * 
 */
public class EventCategoryResource extends ServerResource {

	private String eventCategoryId;

	@Override
	protected void doInit() throws ResourceException {
		eventCategoryId = ( String ) getRequest().getAttributes().get( "eventCategoryId" );
		if( eventCategoryId != null )
			try {
				eventCategoryId = URLDecoder.decode( eventCategoryId, "UTF-8" );
			}catch( UnsupportedEncodingException e ) {
				e.printStackTrace();
			}
	}

	@Override
	protected Representation get() throws ResourceException {
		System.out.println( "GET Called : " + this.eventCategoryId );
		JsonRepresentation output = null;
		EventCategoryDao dao = new EventCategoryDao();
		if( eventCategoryId == null ) {
			output = new JsonRepresentation( dao.read() );
		}else {
			// get all the events
			// filter the events with this category
			// put them into an array and send out
			EventDefDao edao = new EventDefDao();
			LinkedHashMap< String, JsonObject > eventDefs = edao.read();
			Collection< JsonObject > allEventDefs = eventDefs.values();
			Iterator< JsonObject > itr = allEventDefs.iterator();
			JsonArray finalList = new JsonArray();
			while( itr.hasNext() ) {
				JsonObject thisEventDef = itr.next();
				JsonElement eCat = thisEventDef.get( "category" );
				if( eCat != null && eCat.getAsString().contains( this.eventCategoryId ) ) {
					finalList.add( thisEventDef );
				}
			}
			output = new JsonRepresentation( finalList.toString() );
		}
		return output;
	}

	/**
	 * @param args
	 */
	public static void main( String[ ] args ) {
		// TODO Auto-generated method stub

	}

}
