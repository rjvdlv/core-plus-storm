package com.knowesis.sift.api.resource;

import java.util.Collection;
import java.util.LinkedHashMap;

import org.restlet.data.Form;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

import com.google.gson.JsonObject;
import com.knowesis.sift.api.dao.UserDao;

public class UserResource extends ServerResource {

	private String userId;
	//private String limits;

	@Override
	protected void doInit() throws ResourceException {
		userId = ( String ) getRequest().getAttributes().get( "userId" );
        //limits = ( String ) getRequest().getAttributes().get( "limits" );
	}

	@Override
	protected Representation get() throws ResourceException {
		System.out.println( "GET Called" );
		StringRepresentation output = null;
		UserDao dao = new UserDao();
		 LinkedHashMap< String, JsonObject > userData = null;
		if( userId == null ) {

			userData = ( LinkedHashMap< String, JsonObject > ) dao.read();

			Collection< JsonObject > allUserData = userData.values();
			String result = allUserData.toString();
			output = new StringRepresentation( result );
		}else {
			String userInfo = dao.read( userId );
			output = new StringRepresentation( userInfo );
		}
		return output;
	}

	@Override
	protected Representation post( Representation entity ) throws ResourceException {
		System.out.println( "User POST Called" );
		Form userForm = new Form( entity );
		JsonObject jUserObj = new JsonObject();
		jUserObj.addProperty( "docType", "siftusers" );

		String id = userForm.getFirstValue( "id" );
		jUserObj.addProperty( "id", id );
		jUserObj.addProperty( "username", userForm.getFirstValue( "username" ) );
		jUserObj.addProperty( "password", userForm.getFirstValue( "password" ) );
		jUserObj.addProperty( "group", userForm.getFirstValue( "group" ) );
		jUserObj.addProperty( "timestamp", userForm.getFirstValue( "timestamp" ) );


		UserDao dao = new UserDao();
		try {
			dao.update( id, jUserObj.toString() );
		}catch( Exception e ) {
			throw new ResourceException( Status.CLIENT_ERROR_NOT_ACCEPTABLE, e.toString() );
		}


		StringRepresentation output = new StringRepresentation( "SUCCESS_ACCEPTED" );
		return output;
	}

	@Override
	protected Representation put( Representation entity ) throws ResourceException {
		System.out.println( "User PUT Called" );
		return super.put( entity );
	}

}
