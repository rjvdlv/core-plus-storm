package com.knowesis.sift.api.resource;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.PandaCache;
/**
 * @author Raja SP
 * 
 */
public class SiftOperationResource extends ServerResource {
	
	private String parameterType;

	@Override
	protected void doInit() throws ResourceException {
		parameterType = ( String ) getRequest().getAttributes().get( "parameterType" );
	}

	@Override
	protected Representation get() throws ResourceException {
		// siftOperation/numberOfDays
		if( parameterType != null && parameterType.equals( "daysInOperation" ) ) {
			System.out.println( "Days in Operation Called" );
    		PandaCache cache = new PandaCache();
    		String thisInd = ( String ) cache.getClient().get( "telco-1-Indicators" );
    		JsonObject jObj = ( JsonObject ) new JsonParser().parse( thisInd );
    		int nDays = 0;
    		Iterator< Entry< String, JsonElement >> itr = jObj.entrySet().iterator();
    		while( itr.hasNext() ) {
    			String aKey = itr.next().getKey();
    			if( aKey.contains( "TOTAL_Record_Count" ) )
    				nDays++;
    		}
    		return new StringRepresentation( new Integer( nDays ).toString() );
		} else if( parameterType != null && parameterType.equals( "recordsProcessed" ) ) {
			System.out.println( "Records Processed Operation Called" );
			double consolidatedCount = 0;
			PandaCache cache = new PandaCache();
			JsonParser parser = new JsonParser();
			String strNumInstances = ( String ) cache.getClient().get( "SiftCoreInstances" );
			int nNumInstances = Integer.parseInt( ( ( JsonObject )parser.parse( strNumInstances ) ).get( "instances" ).getAsString() );
			ArrayList< JsonObject > telcoIndicatorObjects = new ArrayList< JsonObject >();
			for( int i = 1; i <= nNumInstances; i++ ) {
				String thisInd = ( String ) cache.getClient().get( "telco-" + i + "-Indicators" );
				telcoIndicatorObjects.add(  ( JsonObject )parser.parse( thisInd ) );
			}
			
			//get the first reference object and find the latest day in it.
			//Sum it up from all indicators and give it.
			JsonObject reference = telcoIndicatorObjects.get( 0 );
			Set< Entry< String, JsonElement >> es = reference.entrySet();
			Iterator< Entry< String, JsonElement >> itr = es.iterator();
			int maxDay = 0;
			int maxYear = 0;
			while( itr.hasNext() ) {
				Entry< String, JsonElement > entry = itr.next();
				String key = entry.getKey();
				if( key.startsWith( "TOTAL_Record_Count_Daily" ) ) {
					key = key.replace( "TOTAL_Record_Count_Daily-", "" );
				    String strDay = key.split( "D" )[0];
				    String strYear = key.split( "D" )[1];
				    Integer nDay = Integer.parseInt( strDay );
				    Integer year = Integer.parseInt( strYear );
				    if( nDay > maxDay ) {
				    	if( year >= maxYear ) {
					    	maxDay = nDay;
					    	maxYear = year;
				    	}
				    }	
				}
			}	
			String key = "TOTAL_Record_Count_Daily-" + maxDay + "D" + maxYear;
			System.out.println( "Latest Daily Count key : " + key );
			for( int i = 0; i < telcoIndicatorObjects.size(); i++ ) {
				JsonObject aInd = telcoIndicatorObjects.get( i );
				JsonElement elem =  aInd.get( key );
				if( elem != null ) {
					consolidatedCount += elem.getAsDouble();
				}	
			}		
			JsonObject result = new JsonObject();
			result.addProperty( "recordsProcessed", new Double( consolidatedCount  ) );
			return new JsonRepresentation( result.toString() );
		} else
		    return  new StringRepresentation( "OPERATION INDEFINED" );
	}

}
