package com.knowesis.sift.api.model;

public class function implements java.io.Serializable {

	
	public String name;
	public String signature;
	public String returnType;
	public String description;

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature( String signature ) {
		this.signature = signature;
	}

	public String getReturnType() {
		return returnType;
	}

	public void setReturnType( String returnType ) {
		this.returnType = returnType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription( String description ) {
		this.description = description;
	}

	public function( String name, String signature, String returnType, String description ){
		super();
		this.name = name;
		this.signature = signature;
		this.returnType = returnType;
		this.description = description;
	}

	
	/**
	 * @param args
	 */
	public static void main( String[ ] args ) {
		

	}

}
