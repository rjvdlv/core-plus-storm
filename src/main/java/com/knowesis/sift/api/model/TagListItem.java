package com.knowesis.sift.api.model;

/**
 * @author Raja SP
 * 
 */
public class TagListItem implements java.io.Serializable {
	@Override
	public String toString() {
		return "TagListItem [type=" + type + ", msisdn=" + msisdn + ", id=" + id + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", duration=" + duration + ", extId=" + extId + "]";
	}

	private String type; // Indicator or Event
	private String msisdn;
	private String id;
	private long startDate;
	private long endDate;
	private int duration;
	private String extId;

	public TagListItem( String type, String msisdn, String id, long startDate, long endDate, int duration, String extId ){
		super();
		this.type = type;
		this.msisdn = msisdn;
		this.id = id;
		this.startDate = startDate;
		this.endDate = endDate;
		this.duration = duration;
		this.extId = extId;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn( String msisdn ) {
		this.msisdn = msisdn;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration( int duration ) {
		this.duration = duration;
	}

	public String getExtId() {
		return extId;
	}

	public void setExtId( String extId ) {
		this.extId = extId;
	}

	public String getType() {
		return type;
	}

	public void setType( String type ) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId( String id ) {
		this.id = id;
	}

	public long getStartDate() {
		return startDate;
	}

	public void setStartDate( long startDate ) {
		this.startDate = startDate;
	}

	public long getEndDate() {
		return endDate;
	}

	public void setEndDate( long endDate ) {
		this.endDate = endDate;
	}
}
