package com.knowesis.sift.api.model;

/**
 * @author Raja SP
 * 
 */
public class CIM implements java.io.Serializable {
	private String field;
	private String type;
	private String alias;
	private String category;
	public String getCategory() {
		return category;
	}

	public void setCategory( String category ) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription( String description ) {
		this.description = description;
	}

	private String description;

	public CIM( String cimField, String className, String al, String category, String description ){
		this.field = cimField;
		type = className;
		alias = al;
		this.category = category;
		this.description = description;
	}

	public String getField() {
		return field;
	}

	public void setField( String field ) {
		this.field = field;
	}

	public String getType() {
		return type;
	}

	public void setType( String type ) {
		this.type = type;
	}
	
	public void setAlias( String al ) {
		alias = al;
	}
	
	public String getAlias( ) {
		return alias;
	}

}
