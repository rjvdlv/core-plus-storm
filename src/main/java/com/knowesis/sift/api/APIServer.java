package com.knowesis.sift.api;

import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedList;

import org.restlet.Component;
import org.restlet.data.Protocol;

import com.knowesis.sift.Common.PandaCache;
import com.knowesis.sift.Common.PandaCacheConfig;

public class APIServer {

	public static ArrayList< String > streamsConfigPath = new ArrayList< String >();

	public static void main( String[ ] args ) throws Exception {
		// Create a new Component.
		Component component = new Component();

		if( args.length < 3 ) {
			System.out
					.println( "Missing Parameters. Usage java -jar SiftAPIServer.jar <PortNumber> <SiftPersist Addresses seperated by ;> <Streams Config InputPaths seperated by ;>" );
			System.exit( 0 );
		}

		String strSreamsPath = args[ 2 ];
		String[ ] arrStreamsPath = strSreamsPath.split( ";" );

		for( int i = 0; i < arrStreamsPath.length; i++ ) {
			streamsConfigPath.add( arrStreamsPath[ i ] );
		}

		String strSiftIP = args[ 1 ];
		String[ ] arrSiftIP = strSiftIP.split( ";" );

		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < arrSiftIP.length; i++ ) {
			uris.add( new URI( arrSiftIP[ i ] ) );
		}
		PandaCacheConfig cache = new PandaCacheConfig( uris );

		// Add a new HTTP server listening on port 8111.
		int port = 8111;
		String sPort = args[ 0 ];
		port = Integer.parseInt( sPort );

		component.getServers().add( Protocol.HTTP, port );

		component.getDefaultHost().attach( "/api/v1", new APIApplication() );

		// Start the component.
		component.start();
	}
}
