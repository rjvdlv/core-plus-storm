package com.knowesis.sift.helper;

import java.io.FileWriter;
import java.io.IOException;

import au.com.bytecode.opencsv.CSVWriter;

public class CampaignCommand {
	
	protected String msisdn;
	protected String extId;
	
    public void execute() throws IOException {};
    
    public void notifySubscriber( String channel, String message ) throws IOException {
    	if( channel == null || channel.trim().equals( "" ) )
    		channel = "SMS";
    	if( channel.equals( "SMS" ) )
    		this.sendSMS( message );
    	else if( channel.equals( "IVR" ) )
    		this.sendIVR( message );
    }
    
    
    public void sendSMS( String message ) throws IOException {
		System.out.println( "Sending SMS for : " + msisdn + " - "  + message );
    	CSVWriter writer = new CSVWriter( new FileWriter( "/home/stream/notifications/sms.csv", true ) );
    	String[] line = new String[ 3 ];
    	line[ 0 ] = msisdn;
    	line[ 1 ] = message;
    	line[ 2 ] = new java.util.Date().toString();
    	writer.writeNext( line );
    	writer.close();
    }
    
    
    public void sendIVR( String message ) throws IOException {
		System.out.println( "Sending IVR for : " + msisdn + " - "  + message );
    	CSVWriter writer = new CSVWriter( new FileWriter( "/home/stream/notifications/ivr.csv", true ) );
    	String[] line = new String[ 3 ];
    	line[ 0 ] = msisdn;
    	line[ 1 ] = message;
    	line[ 2 ] = new java.util.Date().toString();
    	writer.writeNext( line );
    	writer.close();
    }
    
    
    public void provision( String productId ) throws IOException {
		System.out.println( "Provisioning Product for : " + msisdn + " - "  + productId );
		CSVWriter writer = new CSVWriter( new FileWriter( "/home/stream/notifications/provision.csv", true ) );
    	String[] line = new String[ 3 ];
    	line[ 0 ] = msisdn;
    	line[ 1 ] = productId;
    	line[ 2 ] = new java.util.Date().toString();
    	writer.writeNext( line );
    	writer.close();
    }
}
