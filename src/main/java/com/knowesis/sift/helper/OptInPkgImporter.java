package com.knowesis.sift.helper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.TimeZone;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCacheConfig;

import au.com.bytecode.opencsv.CSVReader;

public class OptInPkgImporter {

	private String readFile( String fileName ) throws IOException {
		BufferedReader br = new BufferedReader( new FileReader( fileName ) );
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while( line != null ) {
				sb.append( line );
				sb.append( "\n" );
				line = br.readLine();
			}
			return sb.toString();
		}finally {
			br.close();
		}
	}

	/**
	 * @param args
	 * @throws IOException
	 * @throws URISyntaxException
	 * @throws ParseException
	 */
	public static void main( String[ ] args ) throws IOException, URISyntaxException, ParseException {

		if( args.length < 3 ) {
			System.err.println( "Usage : java -jar FixBonus.jar http://<persistAddress>:8091 templatePath csvFilePath" );
			System.exit( 0 );
		}

		OptInPkgImporter refillPkg = new OptInPkgImporter();

		CSVReader reader = new CSVReader( new FileReader( args[ 2 ] ) );
		String[ ] line = null;

		String strPersistAddressList = args[ 0 ];
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );
		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddress.length; i++ ) {
			Commons.logger.info( arrPersistAddress[ i ] );
			uris.add( new URI( arrPersistAddress[ i ] ) );
		}
		PandaCacheConfig pandaCacheConfig = new PandaCacheConfig( uris );

		int nCount = 0;
		while( ( line = reader.readNext() ) != null ) {
			String template = refillPkg.readFile( args[ 1 ] );
			JsonParser parser = new JsonParser();
			JsonObject jTemplate = ( JsonObject ) parser.parse( template );

			String offerId = line[ 0 ];
			offerId = offerId.replaceAll( "  ", " " ).replaceAll( ": ", ":" ).replaceAll( " ", "_" ).replaceAll( "/", "_" ).replaceAll( ":", "_" ).replaceAll( "\\.", "_" ).replaceAll( "-", "_" ).replaceAll( "\\+", "_" )
					.replaceAll( "\\(", "_" ).replaceAll( "\\)", "_" ).replaceAll( "__", "_" );
			jTemplate.addProperty( "name", line[ 1 ] );
			jTemplate.addProperty( "description", line[ 1 ] );

			String applicableLocations = line[ 4 ];
			jTemplate.addProperty( "applicableLocations", applicableLocations );
			jTemplate.addProperty( "category", "ALL_Package," + line[ 5 ] );
			jTemplate.addProperty( "businessCode", line[ 6 ] );

			if( line[ 7 ] != null && line[ 7 ].trim().length() > 0 )
				jTemplate.addProperty( "offerPrice", Double.parseDouble( line[ 7 ] ) );
			jTemplate.addProperty( "offerRevenue", Double.parseDouble( line[ 8 ] ) );
			jTemplate.addProperty( "deliveryReportRequired", line[ 9 ].equals( "Y" ) ? true : false );
			jTemplate.addProperty( "offerStrategy", line[ 11 ] );

			// effective date - expiry date
			SimpleDateFormat format = new SimpleDateFormat( "dd/MM/yy" );
			jTemplate.addProperty( "effectiveDate", format.parse( line[ 2 ] ).getTime() + TimeZone.getDefault().getRawOffset() );
			jTemplate.addProperty( "expiryDate", format.parse( line[ 3 ] ).getTime() + TimeZone.getDefault().getRawOffset() );

			jTemplate.addProperty( "packageGroup", line[ 27 ] );
			jTemplate.addProperty( "packageSubGroup", line[ 28 ] );
			jTemplate.addProperty( "packageFee", Double.parseDouble( line[ 29 ] ) );
			jTemplate.addProperty( "packageValidityDays", Double.parseDouble( line[ 30 ] ) );

			if( line[ 31 ].trim().length() > 0 )
				jTemplate.addProperty( "packageVolumeMB", Double.parseDouble( line[ 31 ] ) );
			else
				jTemplate.addProperty( "packageVolumeMB", 0 );

			if( line[ 32 ].trim().length() > 0 )
				jTemplate.addProperty( "packageVolumeMinute", Double.parseDouble( line[ 32 ] ) );
			else
				jTemplate.addProperty( "packageVolumeMinute", 0 );

			if( line[ 33 ].trim().length() > 0 )
				jTemplate.addProperty( "packageSpeedKbps", Double.parseDouble( line[ 33 ] ) );
			else
				jTemplate.addProperty( "packageSpeedKbps", 0 );

			JsonObject maxFullLimet = new JsonObject();
			maxFullLimet.addProperty( "expression", "getCurrentFulfilmentCount() < 1" );
			maxFullLimet.addProperty( "type", "boolean" );
			jTemplate.add( "maxFulfillmentsPerSubscriber", maxFullLimet );

			JsonArray programsArray = new JsonArray();
			programsArray.add( new JsonPrimitive( "DTAC_NBO_PACKAGE" ) );
			jTemplate.add( "programs", programsArray );

			JsonArray arrOfferChannels = jTemplate.get( "offerChannels" ).getAsJsonArray();
			String[ ] notificationChannels = line[ 47 ].split( "," );
			for( int i = 0; i < notificationChannels.length; i++ ) {
				JsonObject aLocationMessage = new JsonObject();
				arrOfferChannels.add( aLocationMessage );

				String thisLocation = notificationChannels[ i ];

				aLocationMessage.addProperty( "channelName", thisLocation );
				aLocationMessage.addProperty( "shortCode", "" );

				JsonArray langMessages = new JsonArray();
				aLocationMessage.add( "messages", langMessages );

				JsonObject enMsg = new JsonObject();
				enMsg.addProperty( "contentId", line[ 15 ] );
				enMsg.addProperty( "text", "\"\"" );
				enMsg.addProperty( "lang", "NA" );
				langMessages.add( enMsg );
			}

			// optinShort Code and OptinKeyword in fulfillment list -
			// optinShortCode, optinKeyword
			JsonObject fulfillmentObj = jTemplate.get( "fulfillmentList" ).getAsJsonArray().get( 0 ).getAsJsonObject();
			fulfillmentObj.addProperty( "optinShortCode", line[ 35 ] );
			fulfillmentObj.addProperty( "optinKeyword", "\"" + line[ 34 ] + "\"" );

			String productId = line[ 36 ];
			if( productId != null && productId.trim().length() > 0 ) {
				int rewardValidityDays = 7;
				if( line[ 41 ].trim().length() != 0 )
					rewardValidityDays = Integer.parseInt( line[ 41 ] );
				JsonArray fulfillmentProducts = fulfillmentObj.get( "fulfillmentProducts" ).getAsJsonArray();
				JsonObject aProduct = fulfillmentProducts.get( 0 ).getAsJsonObject();
				aProduct.addProperty( "productId", line[ 36 ] );

				aProduct.addProperty( "targetSystem", line[ 40 ] );

				// packCode
				JsonObject dynamicProd = aProduct.get( "dynamicParameters" ).getAsJsonArray().get( 0 ).getAsJsonObject();
				dynamicProd.addProperty( "value", "\"" + line[ 37 ] + "\"" );

				// package_group_code
				dynamicProd = aProduct.get( "dynamicParameters" ).getAsJsonArray().get( 2 ).getAsJsonObject();
				dynamicProd.addProperty( "value", "\"" + line[ 38 ] + "\"" );

				// cbs_offer_id
				dynamicProd = aProduct.get( "dynamicParameters" ).getAsJsonArray().get( 3 ).getAsJsonObject();
				dynamicProd.addProperty( "value", "\"" + line[ 39 ] + "\"" );

				// PDGroupFlag
				String pdgroupFlag = ( line[ 38 ].trim().length() == 0 ) ? "N" : "Y";
				dynamicProd = aProduct.get( "dynamicParameters" ).getAsJsonArray().get( 5 ).getAsJsonObject();
				dynamicProd.addProperty( "value", "\"" + pdgroupFlag + "\"" );

				// PDProdType
				dynamicProd = aProduct.get( "dynamicParameters" ).getAsJsonArray().get( 6 ).getAsJsonObject();
				dynamicProd.addProperty( "value", "\"" + line[ 43 ] + "\"" );
				// NextCycl
				dynamicProd = aProduct.get( "dynamicParameters" ).getAsJsonArray().get( 7 ).getAsJsonObject();
				dynamicProd.addProperty( "value", "\"" + line[ 44 ] + "\"" );
			}

			JsonArray offerReportParameters = jTemplate.get( "offerReportParameters" ).getAsJsonArray();
			offerReportParameters.get( 2 ).getAsJsonObject().addProperty( "value", "\"" + line[ 37 ] + "\"" );
			offerReportParameters.get( 3 ).getAsJsonObject().addProperty( "value", "\"" + offerId + "\"" );
			offerReportParameters.get( 4 ).getAsJsonObject().addProperty( "value", "\"" + line[ 39 ] + "\"" );
			
			String recentBuyFlag = "\"" +  line[ 37 ] + "\".equals( getStringOutputFromNBOModel( \"recent_pack_cd\" )  ) ? \"Y\" : \"N\"";
			JsonObject jRecentBuy = new JsonObject();
			jRecentBuy.addProperty( "key", "RECENT_BUY_FLAG" );
			jRecentBuy.addProperty( "value", recentBuyFlag );
			jRecentBuy.addProperty( "type", "String" );
			offerReportParameters.add(  jRecentBuy );

			JsonArray arrFulfillChannels = fulfillmentObj.get( "fulfillmentMessageChannels" ).getAsJsonArray();
			String[ ] fulfillmentNotificationChannels = line[ 48 ].split( "," );
			for( int i = 0; i < fulfillmentNotificationChannels.length; i++ ) {
				JsonObject aLocationMessage = new JsonObject();
				arrFulfillChannels.add( aLocationMessage );
				aLocationMessage.addProperty( "channelName", fulfillmentNotificationChannels[ i ] );
				aLocationMessage.addProperty( "shortCode", "" );

				JsonArray langMessages = new JsonArray();
				aLocationMessage.add( "messages", langMessages );

				JsonObject enMsg = new JsonObject();
				enMsg.addProperty( "contentId", "" );
				enMsg.addProperty( "text", "\"\"" );
				enMsg.addProperty( "lang", "NA" );
				langMessages.add( enMsg );
			}

			String fulcriteria = "false";
			jTemplate.get( "fulfillmentList" ).getAsJsonArray().get( 0 ).getAsJsonObject().addProperty( "fulfillmentCriteria", fulcriteria );

			String monDuration = "0";
			jTemplate.get( "fulfillmentList" ).getAsJsonArray().get( 0 ).getAsJsonObject().get( "monitoringDuration" ).getAsJsonObject().addProperty( "expression", monDuration );

			// JsonObject participationType = new JsonObject();
			// participationType.addProperty( "type", "java.lang.String" );
			// participationType.addProperty( "expression", "if(
			// REQUESTER_LOCATION.equals( \"SMS\" ) ) return \"AUTO\"; else
			// return \"MANUAL\";" );
			jTemplate.addProperty( "participationType", "if( RECORD_TYPE.equals( \"GET_OFFER\" ) ) return \"MANUAL\"; else return \"AUTO\";" );
			jTemplate.addProperty( "isCountAsContact", "\"N\"" );

			// JsonObject fulfillmentsType = new JsonObject();
			// fulfillmentsType.addProperty( "type", "java.lang.String" );
			// fulfillmentsType.addProperty( "expression", "\"AUTO\"" );
			jTemplate.addProperty( "fulfillmentsType", "\"AUTO\"" );

			jTemplate.addProperty( "offerMeasurementGroup", line[ 49 ] );
			jTemplate.addProperty( "OfferReportingCategory", line[ 50 ] );
			jTemplate.addProperty( "offerAccountType", line[ 51 ] );
			jTemplate.addProperty( "offerPackageCode", line[ 37 ] );

			
			String btlScore = ( line[ 52 ].trim().length() == 0 ) ? "0" : line[ 52 ];
			String btlStdScore = ( line[ 53 ].trim().length() == 0 ) ? "0" : line[ 53 ];
			String btlSpecialScore = ( line[ 54 ].trim().length() == 0 ) ? "0" : line[ 54 ];
			String btlSpecialStdScore = ( line[ 55 ].trim().length() == 0 ) ? "0" : line[ 55 ];
			String atlScore = ( line[ 56 ].trim().length() == 0 ) ? "0" : line[ 56 ];
			String atlStdScore = ( line[ 57 ].trim().length() == 0 ) ? "0" : line[ 57 ];
			String pkgSubGroup =  line[ 28 ] ;

			double touristScore = ( line[ 58 ].trim().length() == 0 ) ? 0 : Double.parseDouble(  line[ 58 ] );
			double ucgScore = ( line[ 59 ].trim().length() == 0 ) ? 0 : Double.parseDouble(  line[ 59 ] );
			jTemplate.addProperty( "touristScore", touristScore );
			jTemplate.addProperty( "ucgScore", ucgScore );

			String[] pkgSubGroupArr = pkgSubGroup.split( "," );
			String[] btlScoreArr = btlScore.split( "," );
			String[] btlStdScoreArr = btlStdScore.split( "," );
			String[] btlSpecialScoreArr = btlSpecialScore.split( "," );
			String[] btlSpecialStdScoreArr = btlSpecialStdScore.split( "," );
			String[] atlScoreArr = atlScore.split( "," );
			String[] atlStdScoreArr = atlStdScore.split( "," );
			for( int i = 0; i<pkgSubGroupArr.length; i++ ) {
				if( btlScoreArr.length > i )
				    jTemplate.addProperty( pkgSubGroupArr[ i ] + "-btlScore", Double.parseDouble( btlScoreArr[ i ] ) );
				else
					jTemplate.addProperty( pkgSubGroupArr[ i ] + "-btlScore", 0 );
				if( btlStdScoreArr.length > i )
					jTemplate.addProperty( pkgSubGroupArr[ i ] + "-btlStdScore", Double.parseDouble( btlStdScoreArr[ i ] ) );
				else
					jTemplate.addProperty( pkgSubGroupArr[ i ] + "-btlStdScore", 0 );
				if( btlSpecialScoreArr.length > i )
					jTemplate.addProperty( pkgSubGroupArr[ i ] + "-btlSpecialScore", Double.parseDouble( btlSpecialScoreArr[ i ] ) );
				else
					jTemplate.addProperty( pkgSubGroupArr[ i ] + "-btlSpecialScore", 0 );
				if( btlSpecialStdScoreArr.length > i )
					jTemplate.addProperty( pkgSubGroupArr[ i ] + "-btlSpecialStdScore", Double.parseDouble( btlSpecialStdScoreArr[ i ] ) );
				else
					jTemplate.addProperty( pkgSubGroupArr[ i ] + "-btlSpecialStdScore", 0 );
				if( atlScoreArr.length > i )
					jTemplate.addProperty( pkgSubGroupArr[ i ] + "-atlScore", Double.parseDouble( atlScoreArr[ i ] ) );
				else
					jTemplate.addProperty( pkgSubGroupArr[ i ] + "-atlScore", 0 );
				if( atlStdScoreArr.length > i )
					jTemplate.addProperty( pkgSubGroupArr[ i ] + "-atlStdScore", Double.parseDouble( atlStdScoreArr[ i ] ) );
				else
					jTemplate.addProperty( pkgSubGroupArr[ i ] + "-atlStdScore", 0 );
			}

/*
			//OfferScores
			double btlScore = ( line[ 52 ].trim().length() == 0 ) ? 0 : Double.parseDouble(  line[ 52 ] );
			double btlStdScore = ( line[ 53 ].trim().length() == 0 ) ? 0 : Double.parseDouble(  line[ 53 ] );
			double btlSpecialScore = ( line[ 54 ].trim().length() == 0 ) ? 0 : Double.parseDouble(  line[ 54 ] );
			double btlSpecialStdScore = ( line[ 55 ].trim().length() == 0 ) ? 0 : Double.parseDouble(  line[ 55 ] );
			double atlScore = ( line[ 56 ].trim().length() == 0 ) ? 0 : Double.parseDouble(  line[ 56 ] );
			double atlStdScore = ( line[ 57 ].trim().length() == 0 ) ? 0 : Double.parseDouble(  line[ 57 ] );
			double touristScore = ( line[ 58 ].trim().length() == 0 ) ? 0 : Double.parseDouble(  line[ 58 ] );
			double ucgScore = ( line[ 59 ].trim().length() == 0 ) ? 0 : Double.parseDouble(  line[ 59 ] );
			
			jTemplate.addProperty( "btlScore", btlScore );
			jTemplate.addProperty( "btlStdScore", btlStdScore );
			jTemplate.addProperty( "btlSpecialScore", btlSpecialScore );
			jTemplate.addProperty( "btlSpecialStdScore", btlSpecialStdScore );
			jTemplate.addProperty( "atlScore", atlScore );
			jTemplate.addProperty( "atlStdScore", atlStdScore );
			jTemplate.addProperty( "touristScore", touristScore );
			jTemplate.addProperty( "ucgScore", ucgScore );
*/			
			//Add Offer Score Expression
//			String offerScoreExp = "\t\tif( TOURIST ) return  getNumericOfferAttribute( \"touristScore\" );\n\t\tif( getStringOfferAttribute( \"offerPackageCode\" ).equals( getStringOutputFromNBOModel( \"recent_pack_cd\" ) ) ) return 1000;\n\t\tif( SELECTED_OFFER_CATEGORY.equals( \"NBO_Package\" ) ) {\n\t\t\tif( getStringOfferAttribute( \"packageGroup\" ).equals( \"COMBO\" ) ) {\n\t\t\t\tif( ( SELECTED_OFFER_LOCATION.equals( \"Recommend\" ) && getStringOutputFromNBOModel( \"recommend_shelf\" ).equals( \"COMBO|BOOSTER\" ) ) ||\n\t\t\t\t    ( SELECTED_OFFER_LOCATION.equals( \"Bubble_Package\" ) && getStringOutputFromNBOModel( \"banner_shelf\" ).equals( \"COMBO|BOOSTER\" ) ) ) \n\t\t\t\t        return  getNumericOfferAttribute( \"btlScore\" ) * 100;\n\t\t\t}\t\n\t\t\tif( getStringOfferAttribute( \"packageGroup\" ).equals( \"BOOSTER\" ) ) {\n                                       return getNumericOfferAttribute( \"btlScore\" );\n                        }\n\t\t\tif( BTL_GROUP ) return getNumericOfferAttribute( \"btlScore\" );\n\t\t\tif( BTL_SPECIAL_GROUP ) return getNumericOfferAttribute( \"btlSpecialScore\" );\n\t\t\tif( ATL_GROUP ) return getNumericOfferAttribute( \"atlScore\" );\n\t\t} else {\n\t\t\t if( getStringOfferAttribute( \"packageGroup\" ).equals( \"COMBO\" ) ) {\n\t\t\t\tif( ( SELECTED_OFFER_LOCATION.equals( \"Recommend\" ) && getStringOutputFromNBOModel( \"recommend_shelf\" ).equals( \"COMBO|BOOSTER\" ) ) ||\n\t\t\t\t    ( SELECTED_OFFER_LOCATION.equals( \"Bubble_Package\" ) && getStringOutputFromNBOModel( \"banner_shelf\" ).equals( \"COMBO|BOOSTER\" ) ) ) \n\t\t\t\t        return  getNumericOfferAttribute( \"btlStdScore\" ) * 0;\n\t\t\t}\t\n\t\t\tif( getStringOfferAttribute( \"packageGroup\" ).equals( \"BOOSTER\" ) ) \n\t\t\t\treturn getNumericOfferAttribute( \"btlStdScore\" );\n\t\t\tif( BTL_GROUP ) return getNumericOfferAttribute( \"btlStdScore\" );\n\t\t\tif( BTL_SPECIAL_GROUP ) return getNumericOfferAttribute( \"btlSpecialStdScore\" );\n\t\t\tif( ATL_GROUP ) return getNumericOfferAttribute( \"atlStdScore\" );\n\t\t}\n\t\treturn 0;\n";
			String offerScoreExp =  " \t\tif( TOURIST ) return  getNumericOfferAttribute( \"touristScore\" );\n\t\tif( getStringOfferAttribute( \"offerPackageCode\" ).equals( getStringOutputFromNBOModel( \"recent_pack_cd\" ) ) ) return 10000;\n\t\tif( SELECTED_OFFER_LOCATION.equals( \"MostBuy\" ) ) return 1000;\n\n\t\tString selectedPkgSubGroup = ( String ) ( getStringOfferAttribute( \"packageGroup\" ).equals( \"DATA\" )  ? getStringOutputFromNBOModel(  \"data_pack_typ\" ) :\n                                    \t\t\tgetStringOfferAttribute( \"packageGroup\" ).equals( \"VOICE\" )  ? getStringOutputFromNBOModel(  \"voice_pack_typ\" ) : \n                                    \t\t\tgetStringOfferAttribute( \"packageGroup\" ).equals( \"COMBO\" )  ? getStringOutputFromNBOModel(  \"combo_pack_typ\" ) : \n                                    \t\t\tgetStringOfferAttribute( \"packageGroup\" ).equals( \"BOOSTER\" )  ? getStringOutputFromNBOModel(  \"booster_pack_typ\" ) : \n                                                        getStringOfferAttribute( \"packageGroup\" ).equals( \"CONTENT\" )  ? getStringOutputFromNBOModel(  \"content_pack_typ\" ) : \n                                    \t\t\tgetStringOutputFromNBOModel(  \"combo_pack_typ\" ) );\n\n\t\tif( SELECTED_OFFER_CATEGORY.equals( \"NBO_Package\" ) ) {\n\t\t\tif( getStringOfferAttribute( \"packageGroup\" ).equals( \"COMBO\" ) ) {\n\t\t\t\tif( ( SELECTED_OFFER_LOCATION.equals( \"Recommend\" ) && getStringOutputFromNBOModel( \"recommend_shelf\" ).equals( \"COMBO|BOOSTER\" ) ) ||\n\t\t\t\t\t\t( SELECTED_OFFER_LOCATION.equals( \"Bubble_Package\" ) && getStringOutputFromNBOModel( \"banner_shelf\" ).equals( \"COMBO|BOOSTER\" ) ) ) \n\t\t\t\t\treturn  getNumericOfferAttribute( selectedPkgSubGroup + \"-btlScore\" ) * 100;\n\t\t\t}\t\n\t\t\tif( getStringOfferAttribute( \"packageGroup\" ).equals( \"BOOSTER\" ) ) {\n\t\t\t\treturn getNumericOfferAttribute( selectedPkgSubGroup + \"-btlScore\" );\n\t\t\t}\n\t\t\tif( BTL_GROUP ) return getNumericOfferAttribute( selectedPkgSubGroup + \"-btlScore\" );\n\t\t\tif( BTL_SPECIAL_GROUP ) return getNumericOfferAttribute( selectedPkgSubGroup + \"-btlSpecialScore\" );\n\t\t\tif( ATL_GROUP ) return getNumericOfferAttribute( selectedPkgSubGroup + \"-atlScore\" );\n\t\t} else {\n\t\t\tif( getStringOfferAttribute( \"packageGroup\" ).equals( \"COMBO\" ) ) {\n\t\t\t\tif( ( SELECTED_OFFER_LOCATION.equals( \"Recommend\" ) && getStringOutputFromNBOModel( \"recommend_shelf\" ).equals( \"COMBO|BOOSTER\" ) ) ||\n\t\t\t\t\t\t( SELECTED_OFFER_LOCATION.equals( \"Bubble_Package\" ) && getStringOutputFromNBOModel( \"banner_shelf\" ).equals( \"COMBO|BOOSTER\" ) ) ) \n\t\t\t\t\treturn  getNumericOfferAttribute( selectedPkgSubGroup + \"-btlStdScore\" ) * 0;\n\t\t\t}\t\n\t\t\tif( getStringOfferAttribute( \"packageGroup\" ).equals( \"BOOSTER\" ) ) \n\t\t\t\treturn getNumericOfferAttribute( selectedPkgSubGroup + \"-btlStdScore\" );\n\t\t\tif( BTL_GROUP ) return getNumericOfferAttribute( selectedPkgSubGroup + \"-btlStdScore\" );\n\t\t\tif( BTL_SPECIAL_GROUP ) return getNumericOfferAttribute( selectedPkgSubGroup + \"-btlSpecialStdScore\" );\n\t\t\tif( ATL_GROUP ) return getNumericOfferAttribute( selectedPkgSubGroup + \"-atlStdScore\" );\n\t\t}\n\t\treturn 0;";

			jTemplate.addProperty( "offerScore", offerScoreExp );		
			
			jTemplate.addProperty( "most_buy_flag", line[ 60 ] );		
			jTemplate.addProperty( "recent_buy_flag", line[ 61 ] );		
			
			JsonArray repParams = jTemplate.get( "offerReportParameters" ).getAsJsonArray();
			JsonObject segmentName = new JsonObject();
			segmentName.addProperty( "type", "String" );
			segmentName.addProperty( "key", "SEGMENT_NAME" );
			segmentName.addProperty( "value", "if( PROGRAM_ID.equals( \"DTAC_WINBACK_SILENT\" ) ) return DTAC_QOS_REPORT_SEGMENT_NAME_LifeTime; if( PROGRAM_ID.equals( \"DTAC_REFILL\" ) ) return DTAC_ROCKET_REFILL_REPORT_SEGMENT_LifeTime;if( PROGRAM_ID.equals( \"DTAC_NBO_PACKAGE\" ) ) return getStringOutputFromNBOModel( \"segment_name\" ); return \"\";" );
			repParams.add( segmentName );

			pandaCacheConfig.set( offerId, 0, jTemplate.toString() );
			nCount++;
			System.out.println( nCount + " = " + jTemplate.toString() );
		}
		System.out.println( nCount + " Offers Imported" );
		System.exit( 0 );
	}
}
