package com.knowesis.sift.helper;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Set;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCacheConfig;
import com.knowesis.sift.api.dao.IndicatorDefDao;

import au.com.bytecode.opencsv.CSVReader;

public class DependencyChecker {

	public static void main(String[] args) throws URISyntaxException, IOException {
		
		if( args.length < 2 ) {
			System.err.println( "Usage : java -jar DependencyChecker.jar http://<persistAddress>:8091 indList" );
			System.exit( 0 );
		}
		
		String strPersistAddressList = args[ 0 ];
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );
		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddress.length; i++ ) {
			Commons.logger.info( arrPersistAddress[ i ] );
			uris.add( new URI( arrPersistAddress[ i ] ) );
		}

		PandaCacheConfig pandaCacheConfig = new PandaCacheConfig( uris );
		DependencyChecker checker = new DependencyChecker();
		
		IndicatorDefDao indDao = new IndicatorDefDao();
		LinkedHashMap<String, JsonObject> allInd = indDao.read();
		
		CSVReader reader = new CSVReader( new FileReader( args[ 1 ]) );
		String[] line = null;
		JsonParser parser = new JsonParser();
		HashSet< String > results = new HashSet< String >();
		while( ( line = reader.readNext() ) != null ) {
			checker.findDependents( pandaCacheConfig, line[ 0 ], parser, results, allInd );
		}
		ArrayList< String > types = new ArrayList< String > ();
		Iterator<String> itr = results.iterator();
		while( itr.hasNext() ) {
			String entry = itr.next();
			String thisInd = (String) pandaCacheConfig.get( entry );
			JsonObject indDef = (JsonObject) parser.parse( thisInd );
			String expressionType = indDef.get( "expressionType" ).getAsString();
			types.add( expressionType );
		}
		System.out.println( results.toString() );
		System.out.println( types.toString() );
	}
	
	
	void findDependents( PandaCacheConfig pandaCacheConfig, String id, JsonParser parser, HashSet< String > results, LinkedHashMap<String, JsonObject> allInd ) {
		if( ! allInd.containsKey( id ) || results.contains( id ) )
			return;
		results.add( id );
//		System.out.println( "finding dependents for : " + id );
//		String thisInd = (String) pandaCacheConfig.get( id );
		JsonObject indDef = allInd.get( id );
		String[] directDependents = indDef.get( "parameters" ).getAsString().replace( "[", "" ).replaceAll( "]",  "" ).split( "," );
		if(indDef.get( "parameters" ).getAsString().contains( "DTAC_PKG_USAGE_FEE_Sum_LifeTime" ) )
			System.out.println( "***** " + id + "contains PKG_HIST" );
		if( directDependents == null || directDependents.length == 0 )
			return;
		for( int i=0; i<directDependents.length; i++ )
			findDependents( pandaCacheConfig, directDependents[ i ].replaceAll( "\"", "" ), parser, results, allInd );
	}
}
