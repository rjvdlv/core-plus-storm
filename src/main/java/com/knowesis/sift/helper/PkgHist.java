package com.knowesis.sift.helper;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.knowesis.sift.expression.SiftMath;

public class PkgHist {

	public JsonObject method( String RECORD_TYPE, String USAGE_SERVICE_TYPE, JsonObject DTAC_PKG_HIST_SERIES_LifeTime, 
			String PRODUCT_PACKAGE_ID, double TRANSACTION_VALUE, int CHARGEDAMOUNT_BONUS_FEENONEZERO, 
			int SUBSCRIPTION_ENDDATE, int SUBSCRIPTION_STARTDATE, String RECORD_DAYSTAMP, int CURRENTAMOUNT_MAIN,
			int SOS_BALANCE, int CHARGEDAMOUNT_MAIN_FEENONEZERO, int CHARGEDAMOUNT_MAIN_FEEZERO, int CHARGEDAMOUNT_BONUS_FEEZERO, 
			int ELAPSE_CYCLE_COUNT, Boolean TRANSITION_ID, String OFFER_ORDER_INTEGRATION_ID ) {

		
        //check for cancellation
		boolean entryFound = true;
		if( ( RECORD_TYPE.equals( "MGR" ) && new String( "3913" ).contains( USAGE_SERVICE_TYPE ) ) ||
			( RECORD_TYPE.equals( "MON" ) && new String( "2004" ).contains( USAGE_SERVICE_TYPE ) ) ) {
			java.util.Set<java.util.Map.Entry<String, JsonElement>> entrySet = DTAC_PKG_HIST_SERIES_LifeTime.entrySet();
			for(java.util.Map.Entry<String,JsonElement> entry : entrySet){
				JsonArray packsInDay = DTAC_PKG_HIST_SERIES_LifeTime.get(entry.getKey().toString()).getAsJsonArray();
				if( entryFound ) 
					break;
				for( int i=0; i<packsInDay.size(); i++ ) {
					JsonArray aPack = packsInDay.get( i ).getAsJsonArray();
					if( aPack.size() == 15 && aPack.get( 13 ).getAsString().equals( OFFER_ORDER_INTEGRATION_ID) ) {
						entryFound = true;
						aPack.set( 14, new JsonPrimitive( true ) );
						break;
					}
				}	
			}	
			return DTAC_PKG_HIST_SERIES_LifeTime;
		}
		
		boolean inserted = false;
		if( RECORD_TYPE.equals( "MGR" ) && new String( "3912" ).contains(USAGE_SERVICE_TYPE) ) {
			String vPKGCat = getSupportingData( "PKG_PROD_CAT", PRODUCT_PACKAGE_ID );
			if(! (new String( "PARTNER,CONTENT,OTHERS,IDD" ).contains(vPKGCat)) || vPKGCat.equals("")){
				double VADDFEE_PRICE = TRANSACTION_VALUE;
				double BALANCE_BEFORE_TOPPING = ( CURRENTAMOUNT_MAIN - CHARGEDAMOUNT_MAIN_FEENONEZERO + CHARGEDAMOUNT_MAIN_FEEZERO ) +
						( SOS_BALANCE - CHARGEDAMOUNT_BONUS_FEENONEZERO + CHARGEDAMOUNT_BONUS_FEEZERO);
				JsonArray array = new JsonArray();
				array.add( new JsonPrimitive( 0 ) ); // 0. ADDPKG_STRTDTTM
				array.add( new JsonPrimitive( 0 ) ); // 1. ADDPKG_ENDDTTM
				array.add( new JsonPrimitive( PRODUCT_PACKAGE_ID ) ); // 2. ADDPKG_CODE
				array.add( new JsonPrimitive( getSupportingData( "PKG_PROD_CAT", PRODUCT_PACKAGE_ID ) ) );  //4
				array.add( new JsonPrimitive( getSupportingData( "PKG_PROD_BENEFIT_TYPE", PRODUCT_PACKAGE_ID ) ) ); //5
				array.add( new JsonPrimitive( VADDFEE_PRICE ) ); // 6. ADDFEE_PRICE 
				array.add( new JsonPrimitive( 0 ) ); // 7. ADDPKG_VALIDITY_DAYS
				array.add( new JsonPrimitive( 0 ) ); // 8. AVG_Fee = ADDFEE_PRICE/ADDPKG_VALIDITY_DAYS
				array.add( new JsonPrimitive( BALANCE_BEFORE_TOPPING  ) ); // 9. Balance Before Topping
				array.add( new JsonPrimitive( getSupportingData( "PKG_CONTINUITY", PRODUCT_PACKAGE_ID ) ) ); // 10. Package Continuity

				array.add( new JsonPrimitive( getNumericSupportingData( "PKG_SPEED", PRODUCT_PACKAGE_ID ) ) ); // 11. Package Speed
				array.add( new JsonPrimitive( TRANSITION_ID ) ); // 12. Transition Id
				array.add( new JsonPrimitive( OFFER_ORDER_INTEGRATION_ID ) ); // 13. Transition Id
				array.add( new JsonPrimitive( false ) ); // 14. Cancel Flag
				

				String sDayStamp = new Long( RECORD_DAYSTAMP ).toString();
				JsonElement eSeries = DTAC_PKG_HIST_SERIES_LifeTime.get( sDayStamp );
				if( eSeries == null ) {
					JsonArray newArray = new JsonArray();
					newArray.add( array );
					DTAC_PKG_HIST_SERIES_LifeTime.add( sDayStamp, newArray );
				} else {
					JsonArray listPackage = eSeries.getAsJsonArray();
					int listPackageSize = listPackage.size();

					for(int i=0;i<listPackageSize;i++) {
						JsonArray elementPackage = listPackage.get(i).getAsJsonArray();
						if( ( elementPackage.size() == 15 && elementPackage.get(12).getAsString().equals( TRANSITION_ID ) ) || 
								elementPackage.get(2).getAsString().equals(PRODUCT_PACKAGE_ID)){
							if(VADDFEE_PRICE > 0.0){
								elementPackage.set(5,new JsonPrimitive(VADDFEE_PRICE));
								elementPackage.set(8,new JsonPrimitive(BALANCE_BEFORE_TOPPING));
								if(elementPackage.get(6).getAsInt() > 0){
									elementPackage.set(7,new JsonPrimitive(VADDFEE_PRICE/elementPackage.get(6).getAsInt()));
								}
								listPackage.set(i,elementPackage);
							}
							inserted = true;
						}
					}
					if (!inserted){
						eSeries.getAsJsonArray().add(array);
					}
				}
			}
		}else if( RECORD_TYPE.equals( "MON" ) && new String( "2001,2002" ).contains(USAGE_SERVICE_TYPE) ){
			String vPKGCat = getSupportingData( "PKG_PROD_CAT", PRODUCT_PACKAGE_ID );
			if(! (new String( "PARTNER,CONTENT,OTHERS,IDD" ).contains(vPKGCat)) || vPKGCat.equals("")){
				double VADDFEE_PRICE = TRANSACTION_VALUE;
				double BALANCE_BEFORE_TOPPING = ( CURRENTAMOUNT_MAIN - CHARGEDAMOUNT_MAIN_FEENONEZERO + CHARGEDAMOUNT_MAIN_FEEZERO ) +
						( SOS_BALANCE - CHARGEDAMOUNT_BONUS_FEENONEZERO + CHARGEDAMOUNT_BONUS_FEEZERO);
				// long ADDPKG_VALIDITY_DAYS = ((SUBSCRIPTION_ENDDATE - (SUBSCRIPTION_ENDDATE % 86400000)) - (SUBSCRIPTION_STARTDATE - (SUBSCRIPTION_STARTDATE % 86400000))) / 86400000L;
				double vDiffTime = SUBSCRIPTION_ENDDATE - SUBSCRIPTION_STARTDATE;
				double vDiffDate = (vDiffTime - 60000)/86400000;
				int ADDPKG_VALIDITY_DAYS = (int) java.lang.Math.ceil(vDiffDate);

				if(ADDPKG_VALIDITY_DAYS == 0){
					ADDPKG_VALIDITY_DAYS = 1;
				}
				double AVG_Fee = TRANSACTION_VALUE/ADDPKG_VALIDITY_DAYS;

				JsonArray array = new JsonArray();
				array.add( new JsonPrimitive( SUBSCRIPTION_STARTDATE ) ); // ADDPKG_STRTDTTM
				array.add( new JsonPrimitive( SUBSCRIPTION_ENDDATE ) ); // ADDPKG_ENDDTTM
				array.add( new JsonPrimitive( PRODUCT_PACKAGE_ID ) ); // ADDPKG_CODE
				array.add( new JsonPrimitive( getSupportingData( "PKG_PROD_CAT", PRODUCT_PACKAGE_ID ) ) );
				array.add( new JsonPrimitive( getSupportingData( "PKG_PROD_BENEFIT_TYPE", PRODUCT_PACKAGE_ID ) ) );
				array.add( new JsonPrimitive( VADDFEE_PRICE ) ); // ADDFEE_PRICE
				array.add( new JsonPrimitive( ADDPKG_VALIDITY_DAYS ) ); // ADDPKG_VALIDITY_DAYS
				array.add( new JsonPrimitive( AVG_Fee ) ); // AVG_Fee = ADDFEE_PRICE/ADDPKG_VALIDITY_DAYS
				array.add( new JsonPrimitive( BALANCE_BEFORE_TOPPING  ) ); // Balance Before Topping
				array.add( new JsonPrimitive( getSupportingData( "PKG_CONTINUITY", PRODUCT_PACKAGE_ID ) ) ); // Package Continuity

				array.add( new JsonPrimitive( getNumericSupportingData( "PKG_SPEED", PRODUCT_PACKAGE_ID ) ) ); // 11. Package Speed
				array.add( new JsonPrimitive( TRANSITION_ID ) ); // 12. Transition Id
				array.add( new JsonPrimitive( OFFER_ORDER_INTEGRATION_ID ) ); // 13. Transition Id
				array.add( new JsonPrimitive( false ) ); // 14. Cancel Flag


				String sDayStamp = new Long( RECORD_DAYSTAMP ).toString();
				JsonElement eSeries = DTAC_PKG_HIST_SERIES_LifeTime.get( sDayStamp );
				if( eSeries == null ) {
					JsonArray newArray = new JsonArray();
					newArray.add( array );
					DTAC_PKG_HIST_SERIES_LifeTime.add( sDayStamp, newArray );
				}else{
					JsonArray listPackage = eSeries.getAsJsonArray();
					int listPackageSize = listPackage.size();

					for(int i=0;i<listPackageSize;i++) {
						JsonArray elementPackage = listPackage.get(i).getAsJsonArray();
						if( ( elementPackage.size() == 15 && elementPackage.get(12).getAsString().equals( TRANSITION_ID ) ) || 
								elementPackage.get(2).getAsString().equals(PRODUCT_PACKAGE_ID)){
							if(ELAPSE_CYCLE_COUNT > 1){
								elementPackage.set(5,new JsonPrimitive(VADDFEE_PRICE));
								elementPackage.set(7,new JsonPrimitive(AVG_Fee));
								elementPackage.set(8,new JsonPrimitive(BALANCE_BEFORE_TOPPING));
							}else{
								double eAddFee = elementPackage.get(5).getAsDouble();
								elementPackage.set(7,new JsonPrimitive(eAddFee/ADDPKG_VALIDITY_DAYS));
							}
							elementPackage.set(0,new JsonPrimitive(SUBSCRIPTION_STARTDATE));
							elementPackage.set(1,new JsonPrimitive(SUBSCRIPTION_ENDDATE));
							elementPackage.set(6,new JsonPrimitive(ADDPKG_VALIDITY_DAYS));
							listPackage.set(i,elementPackage);

							inserted = true;
						}
					}

					if (!inserted){
						eSeries.getAsJsonArray().add(array);
					}
				}
			}
		}
		return DTAC_PKG_HIST_SERIES_LifeTime;
	}

	private double getNumericSupportingData(String string, String pRODUCT_PACKAGE_ID) {
		// TODO Auto-generated method stub
		return 10.0;
	}

	private String getSupportingData(String string, Object pRODUCT_PACKAGE_ID) {
		// TODO Auto-generated method stub
		return null;
	}


}
