package com.knowesis.sift.helper;

import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;

import org.antlr.v4.codegen.model.chunk.ThisRulePropertyRef_stop;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSyntaxException;
import com.knowesis.sift.Common.PandaCache;
import com.knowesis.sift.expression.SiftMath;

import au.com.bytecode.opencsv.CSVReader;

public class PkgDerivatives {

/*
	public JsonObject computeDerivatesFromKNet( JsonObject DTAC_PKG_HIST_SERIES_LifeTime, String RECORD_TYPE, String IDD_FLAG, JsonObject DTAC_PKG_SUMMARY_LifeTime ) {
		if( RECORD_TYPE.equals( "CREDIT" ) 
				|| RECORD_TYPE.equals( "BALANCE-CHECK" ) || RECORD_TYPE.equals( "GET_OFFER" ) 
				|| ( RECORD_TYPE.equals( "VOICE" ) && ! (IDD_FLAG.equals( "IDD" ) ) ) ) {

			java.util.Set<java.util.Map.Entry<String, JsonElement>> entrySet = DTAC_PKG_HIST_SERIES_LifeTime.entrySet();
			if( entrySet.size() == 0 )
				return DTAC_PKG_SUMMARY_LifeTime;

			double activeDataPackCount = 0;
			double activeVoicePackCount = 0;
			double dataPackCount = 0;
			double activeComboPackCount = 0;
			double activeBoosterPackCount = 0;
			double dataPackFeeSum = 0;
			double dataPackFeeSum1to30 = 0;

			double comboPackFeeSum = 0;
			double dataPackFeeSum0T15D = 0;
			double voicePackFeeSum0T15D = 0;
			double comboPackFeeSum0T15D = 0;

			double dataPackFeeSum7d = 0;
			double dataPackValiditySum = 0;
			double packValiditySum = 0;
			double zeroValidityPackCount = 0;
			double lmDataPackCount = 0;
			double lmVoicePackCount = 0;
			double umDataPackCount = 0;
			double umDataFlatPackCount = 0;
			double umVoicePackCount = 0;
			double packCount = 0;
			double voicePackCount = 0;
			double voicePackFeeSum = 0;
			double voicePackValiditySum = 0;
			double packUsageFeeSum = 0;
			double voiceComboPackFeeSum7d = 0;
			double voiceComboPackFeeSum30d = 0;
			long maxEndDate = 0;
			long minsLeft = 0;
			String activePackBenType = "OTHERS";
			String actRecurringPack = "NA";
			long actRecurringEndDate = 0;
			String actPackSpeed1d = "N";
			String actPackSpeedGT1d = "N";
			String excBoosterOtherFlg = "N";

			java.util.ArrayList< Double > data30dayFeeSeries = new java.util.ArrayList< Double >();
			java.util.ArrayList< Double > voice30dayFeeSeries = new java.util.ArrayList< Double >();
			java.util.ArrayList< Integer > data30dayValiditySeries = new java.util.ArrayList< Integer >();
			JsonArray validitySeries = new JsonArray();
			JsonArray feeSeries = new JsonArray();
			JsonArray packCatSeries = new JsonArray();
			JsonArray priceSenSeries = new JsonArray();
			JsonArray speedSeries = new JsonArray();
			java.util.ArrayList< Integer > voice30dayValiditySeries = new java.util.ArrayList< Integer >();
			java.util.ArrayList< String > pkgCodeSeries = new java.util.ArrayList< String >();

			long currentTime = SiftMath.getCurrentTime();

			long start = currentTime;
			start -= start % 86400000L;
			long dayStart1 = start - 1 * 86400000L;
			long dayEnd1to30 = start - 30 * 86400000L;
			long dayEnd1to7 = start - 7 * 86400000L;
			long dayEnd30 = start - 29 * 86400000L;
			long dayEnd60 = start - 59 * 86400000L;
			long dayEnd15 = start - 15 * 86400000L;

			for(java.util.Map.Entry<String,JsonElement> entry : entrySet){
				String thiskey = entry.getKey().toString();
				long lkey = Long.parseLong( thiskey );

				if( lkey <= start && lkey >= dayEnd60 ) {  //all the 0-60 days indicators
					JsonArray packsInDay = DTAC_PKG_HIST_SERIES_LifeTime.get(entry.getKey().toString()).getAsJsonArray();
					for( int i=0; i<packsInDay.size(); i++ ) {
						JsonArray aPack = packsInDay.get( i ).getAsJsonArray();

						long thisPackEndDate = aPack.get( 1 ).getAsLong();
						boolean cancelled = false;	
						//skip the cancelled packs - only if its active
						if( aPack.size() == 14 && aPack.get( 13 ).getAsBoolean() == true && thisPackEndDate > currentTime )
							cancelled = true;

						String thisPackCode = aPack.get( 2 ).getAsString();
						String thisPackCat = aPack.get( 3 ).getAsString();
						String thisPackBenefit = aPack.get( 4 ).getAsString();
						double thisPackPrice = aPack.get( 5 ).getAsDouble();
						double thisPackFee = aPack.get( 7 ).getAsDouble();
						int thisPackValidity = aPack.get( 6 ).getAsInt();
						double balBefTopping = aPack.get( 8 ).getAsDouble();
						String continuity = aPack.get( 9 ).getAsString();

						if( thisPackPrice > 0 ) {
							if( thisPackCat.trim().length() > 0 )
								packCount++;	
							validitySeries.add( new JsonPrimitive( thisPackValidity ) );
							feeSeries.add( new JsonPrimitive( thisPackPrice ) );
							packCatSeries.add( new JsonPrimitive( thisPackCat ) );
							if( aPack.size() == 14 )
								speedSeries.add( new JsonPrimitive( aPack.get( 10 ).getAsDouble() ) );

							if( aPack.size() < 9 ) 
								priceSenSeries.add( new JsonPrimitive( 0.99) );
							else
								priceSenSeries.add( new JsonPrimitive( thisPackPrice / balBefTopping ) ); 
						}	

						// active package
						if( thisPackEndDate > currentTime ) {
							if (! cancelled) {
								activePackBenType = thisPackBenefit;
								if( thisPackCat.contains( "DATA" ) && thisPackPrice > 0 ) {
									if( thisPackBenefit.contains("UNLIMITED") ) {
										activeDataPackCount++;
										if( thisPackEndDate > maxEndDate ) {
											maxEndDate = thisPackEndDate;
											minsLeft = ( maxEndDate - currentTime ) / 60000;
											if( minsLeft > 86400 )
												minsLeft = 86400;
											if( minsLeft < -86400 )
												minsLeft = -86400;
										}
									}
									else if( thisPackBenefit.contains("SUPERBOOSTER") )
										activeDataPackCount++;
									else if ( thisPackBenefit.contains("BOOSTER") )
										activeBoosterPackCount++;
									else if ( thisPackBenefit.contains("LIMITED") )
										activeDataPackCount++;
								}
								if (thisPackCat.contains( "COMBO" ) && thisPackPrice > 0 ) {
									if( thisPackBenefit.contains( "UNLIMITED" ) ) {
										activeComboPackCount++;
										if ( thisPackBenefit.contains( "FLAT" ) ) 
											if( thisPackValidity == 1 )
												actPackSpeed1d = "Y";
											else
												actPackSpeedGT1d = "Y";

										if( thisPackEndDate > maxEndDate ) {
											maxEndDate = thisPackEndDate;
											minsLeft = ( maxEndDate - currentTime ) / 60000;
											if( minsLeft > 86400 )
												minsLeft = 86400;
											if( minsLeft < -86400 )
												minsLeft = -86400;
										}
									}
									else if (thisPackBenefit.contains( "LIMITED" ) )
										activeComboPackCount++;
								}
								if( thisPackCat.contains( "VOICE" ) && thisPackPrice > 0 )
									activeVoicePackCount++;
							}
						}	

						if( thisPackBenefit.contains("SUPERBOOSTER") )
							excBoosterOtherFlg = "N";
						else if( thisPackBenefit.contains("BOOSTER") )
							excBoosterOtherFlg = "Y";
						else if( thisPackBenefit.contains("UNLIMITED") )
							excBoosterOtherFlg = "N";
						else if( thisPackBenefit.contains("LIMITED") )
							excBoosterOtherFlg = "N";
						else
							excBoosterOtherFlg = "Y";

						if( lkey >= dayEnd1to30 && lkey <= dayStart1 && thisPackPrice > 0 && excBoosterOtherFlg.equals("N") ) {
							packUsageFeeSum += thisPackPrice;
							if( thisPackCat.contains( "VOICE" ) || thisPackCat.contains( "COMBO" ) )
								voiceComboPackFeeSum30d += thisPackPrice;
							if( thisPackCat.contains( "DATA" ) || thisPackCat.contains( "COMBO" ) )
								dataPackFeeSum1to30 += thisPackPrice;
						}

						if( lkey < dayEnd30 )  //all the 0-30 days indicators
							continue;


						if( thisPackPrice > 0 && excBoosterOtherFlg.equalsIgnoreCase( "N") ) {
							packValiditySum += thisPackValidity;

							if( thisPackCat.trim().length() > 0 )
								pkgCodeSeries.add( thisPackCode );

							if( thisPackValidity == 0 )
								zeroValidityPackCount++;

							if( thisPackCat.contains( "VOICE" )  ) {
								voicePackCount++;
								voicePackFeeSum += thisPackPrice;
								voicePackValiditySum += thisPackValidity;
								voice30dayValiditySeries.add( thisPackValidity );

								if(  lkey > dayEnd15 )
									voicePackFeeSum0T15D += thisPackPrice;

								if( thisPackBenefit.startsWith( "LIMITED" ) )
									lmVoicePackCount++;
								else if( thisPackBenefit.startsWith( "UNLIMITED" ) )
									umVoicePackCount++;

								voice30dayFeeSeries.add( thisPackFee );
							}	   

							if( thisPackCat.contains( "COMBO" ) ) {
								comboPackFeeSum += thisPackPrice;
								if( lkey > dayEnd15 )
									comboPackFeeSum0T15D += thisPackPrice;

							}

							if( thisPackCat.contains( "VOICE" ) || thisPackCat.contains( "COMBO" ) ) {
								if( lkey >= dayEnd1to7 && lkey <= dayStart1 )
									voiceComboPackFeeSum7d += thisPackPrice;
							}		

							if( thisPackCat.contains( "DATA" ) || thisPackCat.contains( "COMBO" ) ) {
								dataPackCount++;
								dataPackFeeSum += thisPackPrice;

								if( lkey > dayEnd15 && thisPackCat.contains( "DATA" ) )
									dataPackFeeSum0T15D += 	thisPackPrice;

								dataPackValiditySum += thisPackValidity;
								data30dayFeeSeries.add( thisPackFee );
								data30dayValiditySeries.add( thisPackValidity );

								if( thisPackBenefit.startsWith( "LIMITED" ) )
									lmDataPackCount++;
								else if( thisPackBenefit.startsWith( "UNLIMITED FLAT" ) )
									umDataFlatPackCount++;
								else if( thisPackBenefit.startsWith( "UNLIMITED" ) )
									umDataPackCount++;

								if( lkey >= dayEnd1to7 && lkey <= dayStart1 )
									dataPackFeeSum7d += thisPackPrice;
							}	   
						}
					}
				}
			}

			JsonObject result = new JsonObject();
			result.addProperty( "activeDataPackCount", activeDataPackCount );
			result.addProperty( "activeVoicePackCount", activeVoicePackCount );
			result.addProperty( "dataPackCount", dataPackCount );
			result.addProperty( "activeComboPackCount", activeComboPackCount );
			result.addProperty( "activeBoosterPackCount", activeBoosterPackCount );
			result.addProperty( "dataPackFeeSum", dataPackFeeSum );
			result.addProperty( "dataPackValiditySum", dataPackValiditySum );
			result.addProperty( "packValiditySum", packValiditySum );
			result.addProperty( "zeroValidityPackCount", zeroValidityPackCount );
			result.addProperty( "lmDataPackCount", lmDataPackCount );
			result.addProperty( "lmVoicePackCount", lmVoicePackCount );
			result.addProperty( "umDataPackCount", umDataPackCount );
			result.addProperty( "umDataFlatPackCount", umDataFlatPackCount );
			result.addProperty( "umVoicePackCount", umVoicePackCount );
			result.addProperty( "packCount", packCount );

			result.addProperty( "voicePackCount", voicePackCount );
			result.addProperty( "voicePackFeeSum", voicePackFeeSum );
			result.addProperty( "voicePackValiditySum", voicePackValiditySum );
			result.addProperty( "packUsageFeeSum", packUsageFeeSum );

			result.addProperty( "activeDataPackMins", minsLeft );

			activePackBenType = activePackBenType.trim();
			if( activePackBenType.contains( "SUPERBOOSTER" ) )
				activePackBenType = "SUPER_BOOSTER";
			else if( activePackBenType.contains( "BOOSTER" ) )
				activePackBenType = "BOOSTER";
			else if( activePackBenType.contains( "UNLIMITED" ) && activePackBenType.contains( "FLAT") )
				activePackBenType = "UNLIMITED_FLAT";
			else if( activePackBenType.contains( "UNLIMITED" ) && activePackBenType.contains( "ROLLOVER") )
				activePackBenType = "UNLIMITED_ROLLOVER";
			else if( activePackBenType.contains( "UNLIMITED" ) )
				activePackBenType = "UNLIMITED";
			else if( activePackBenType.contains( "LIMITED" ) )
				activePackBenType = "LIMITED";
			else
				activePackBenType = "OTHERS";

			result.addProperty( "activePackBenType", activePackBenType );

			result.addProperty( "avgDataFee30d", dataPackFeeSum1to30 / 30  );
			result.addProperty( "avgDataFee7d", dataPackFeeSum7d / 7  );
			result.addProperty( "avgVoiceComboFee30d", voiceComboPackFeeSum30d / 30  );
			result.addProperty( "avgVoiceComboFee7d", voiceComboPackFeeSum7d / 7  );

			result.addProperty( "actRecurringPack", actRecurringPack );
			result.addProperty( "actRecurringEndDate", actRecurringEndDate );

			result.addProperty( "dataPackFeeSum0T15D", dataPackFeeSum0T15D );
			result.addProperty( "comboPackFeeSum0T15D", comboPackFeeSum0T15D );
			result.addProperty( "voicePackFeeSum0T15D", voicePackFeeSum0T15D );

			result.addProperty( "dataPackFeeSum16T30D", dataPackFeeSum - dataPackFeeSum0T15D );
			result.addProperty( "comboPackFeeSum16T30D", comboPackFeeSum - comboPackFeeSum0T15D );
			result.addProperty( "voicePackFeeSum16T30D", voicePackFeeSum - voicePackFeeSum0T15D );

			result.addProperty( "actPackSpeed1d", actPackSpeed1d );
			result.addProperty( "actPackSpeedGT1d", actPackSpeedGT1d );

			//sort data30dayFee, voice30dayFee. For median.
			java.util.Collections.sort( data30dayFeeSeries );
			java.util.Collections.sort( voice30dayFeeSeries );

			Double dataFeeMedian = 0.0;
			int middle = data30dayFeeSeries.size() / 2;
			if( data30dayFeeSeries.size() > 0 ) {
				if ( data30dayFeeSeries.size() % 2 == 1) 
					dataFeeMedian = ( Double )data30dayFeeSeries.get( middle );
				else 
					dataFeeMedian = new Double( ( ( Double )data30dayFeeSeries.get( middle - 1  ) + ( Double )data30dayFeeSeries.get( middle ) ) / 2.0 );
			}
			result.addProperty( "dataFeeMedian", dataFeeMedian.doubleValue()  );

			Double voiceFeeMedian = 0.0;
			if( voice30dayFeeSeries.size() > 0 ) {
				middle = voice30dayFeeSeries.size() / 2;
				if ( voice30dayFeeSeries.size() % 2 == 1) 
					voiceFeeMedian = ( Double )voice30dayFeeSeries.get( middle );
				else 
					voiceFeeMedian = new Double( ( ( Double )voice30dayFeeSeries.get( middle - 1  ) + ( Double )voice30dayFeeSeries.get( middle ) ) / 2.0 );
			}
			result.addProperty( "voiceFeeMedian", voiceFeeMedian.doubleValue()  );


			double maxValue = 0;
			double maxCount = 0;
			for (int i = 0; i < data30dayValiditySeries.size(); ++i) {
				double count = 0;
				for (int j = 0; j < data30dayValiditySeries.size(); ++j) {
					if ( ( Integer )data30dayValiditySeries.get( j ) == ( Integer )data30dayValiditySeries.get( i ) ) ++count;
				}
				if (count > maxCount) {
					maxCount = count;
					maxValue = ( Integer )data30dayValiditySeries.get( i );
				}
			}
			result.addProperty( "modDataValidity", maxValue );

			maxValue = 0;
			maxCount = 0;
			for (int i = 0; i < validitySeries.size(); ++i) {
				double count = 0;
				for (int j = 0; j < validitySeries.size(); ++j) {
					if( validitySeries.get( j ).getAsDouble() == validitySeries.get( i ).getAsDouble() ) ++count;
				}
				if (count > maxCount) {
					maxCount = count;
					maxValue = validitySeries.get( i ).getAsDouble();
				}
			}
			result.addProperty( "modValidity", maxValue );

			maxValue = 0;
			maxCount = 0;
			for (int i = 0; i < voice30dayValiditySeries.size(); ++i) {
				double count = 0;
				for (int j = 0; j < voice30dayValiditySeries.size(); ++j) {
					if ( ( Integer )voice30dayValiditySeries.get( j ) == ( Integer )voice30dayValiditySeries.get( i ) ) ++count;
				}
				if (count > maxCount) {
					maxCount = count;
					maxValue = ( Integer )voice30dayValiditySeries.get( i );
				}
			}
			result.addProperty( "modVoiceValidity", maxValue );


			String strmaxValue = "0";
			maxCount = 0;
			for (int i = 0; i < pkgCodeSeries.size(); ++i) {
				double count = 0;
				for (int j = 0; j < pkgCodeSeries.size(); ++j) {
					if ( ( ( String )pkgCodeSeries.get( j ) ).equals( ( String )pkgCodeSeries.get( i ) ) ) ++count;
				}
				if ( count > maxCount) {
					maxCount = count;
					strmaxValue = ( String )pkgCodeSeries.get( i );
				}
			}
			result.addProperty( "mostBoughtPackCode", strmaxValue );
			result.add( "validitySeries", validitySeries );
			result.add( "feeSeries", feeSeries );
			result.add( "packCatSeries", packCatSeries );lmDataPackCountlmDataPackCount
			result.add( "priceSenSeries", priceSenSeries );
			result.add( "speedSeries", speedSeries );

			return result;
		}
		return DTAC_PKG_SUMMARY_LifeTime;
	}
*/

	public JsonObject computeDerivativesReconciled( JsonObject DTAC_PKG_HIST_SERIES_LifeTime, String RECORD_TYPE, String IDD_FLAG, JsonObject DTAC_PKG_SUMMARY_LifeTime ) {
		if( RECORD_TYPE.equals( "CREDIT" ) || RECORD_TYPE.equals( "SUBSCRIBER_PROFILE" ) 
				|| RECORD_TYPE.equals( "BALANCE-CHECK" ) || RECORD_TYPE.equals( "GET_OFFER" ) 
				|| ( RECORD_TYPE.equals( "VOICE" ) && ! (IDD_FLAG.equals( "IDD" ) ) ) ) {

			java.util.Set<java.util.Map.Entry<String, JsonElement>> entrySet = DTAC_PKG_HIST_SERIES_LifeTime.entrySet();
			if( entrySet.size() == 0 )
				return DTAC_PKG_SUMMARY_LifeTime;

			double activeDataPackCount = 0;
			double activeVoicePackCount = 0;
			double dataPackCount = 0;
			double activeComboPackCount = 0;
			double activeBoosterPackCount = 0;
			double dataPackFeeSum = 0;
			double onlyDataPackFeeSum = 0;
			double dataPackFeeSum1to30 = 0;

			double comboPackFeeSum = 0;
			double dataPackFeeSum0T15D = 0;
			double voicePackFeeSum0T15D = 0;
			double comboPackFeeSum0T15D = 0;

			double dataPackFeeSum7d = 0;
			double dataPackValiditySum = 0;
			double packValiditySum = 0;
			double zeroValidityPackCount = 0;
			double lmDataPackCount = 0;
			double lmVoicePackCount = 0;
			double umDataPackCount = 0;
			double umDataFlatPackCount = 0;
			double umVoicePackCount = 0;
			double packCount = 0;
			double voicePackCount = 0;
			double voicePackFeeSum = 0;
			double voicePackValiditySum = 0;
			double packUsageFeeSum = 0;
			double voiceComboPackFeeSum7d = 0;
			double voiceComboPackFeeSum30d = 0;
			long maxEndDate = 0;
			long minsLeft = 0;
			String activePackBenType = "OTHERS";
			String actRecurringPack = "NA";
			long actRecurringEndDate = 0;
			String actPackSpeed1d = "N";
			String actPackSpeedGT1d = "N";
			String allActivePackId = "|";
			String excBoosterOtherFlg = "N";

			java.util.ArrayList< Double > data30dayFeeSeries = new java.util.ArrayList< Double >();
			java.util.ArrayList< Double > voice30dayFeeSeries = new java.util.ArrayList< Double >();
			java.util.ArrayList< Integer > data30dayValiditySeries = new java.util.ArrayList< Integer >();
			JsonArray validitySeries = new JsonArray();
			JsonArray feeSeries = new JsonArray();
			JsonArray packCatSeries = new JsonArray();
			JsonArray priceSenSeries = new JsonArray();
			JsonArray speedSeries = new JsonArray();
			java.util.ArrayList< Integer > voice30dayValiditySeries = new java.util.ArrayList< Integer >();
			java.util.ArrayList< String > pkgCodeSeries = new java.util.ArrayList< String >();
			java.util.ArrayList< String > pkgBenSeries = new java.util.ArrayList< String >();

			long currentTime = SiftMath.getCurrentTime();

			long start = currentTime;
			start -= start % 86400000L;
			long dayStart1 = start - 1 * 86400000L;
			long dayEnd1to30 = start - 30 * 86400000L;
			long dayEnd1to7 = start - 7 * 86400000L;
			long dayEnd30 = start - 29 * 86400000L;
			long dayEnd60 = start - 59 * 86400000L;
			long dayEnd15 = start - 15 * 86400000L;

			for(java.util.Map.Entry<String,JsonElement> entry : entrySet){
				String thiskey = entry.getKey().toString();
				long lkey = Long.parseLong( thiskey );

				if( lkey <= start && lkey >= dayEnd60 ) {  //all the 0-60 days indicators
					JsonArray packsInDay = DTAC_PKG_HIST_SERIES_LifeTime.get(entry.getKey().toString()).getAsJsonArray();
					for( int i=0; i<packsInDay.size(); i++ ) {
						JsonArray aPack = packsInDay.get( i ).getAsJsonArray();

						long thisPackEndDate = aPack.get( 1 ).getAsLong();
						boolean cancelled = false;	
						//skip the cancelled packs - only if its active
						if( aPack.size() == 14 && aPack.get( 13 ).getAsBoolean() == true && thisPackEndDate > currentTime )
							cancelled = true;

						String thisPackCode = aPack.get( 2 ).getAsString();
						String thisPackCat = aPack.get( 3 ).getAsString();
						String thisPackBenefit = aPack.get( 4 ).getAsString();
						double thisPackPrice = aPack.get( 5 ).getAsDouble();
						double thisPackFee = aPack.get( 7 ).getAsDouble();
						int thisPackValidity = aPack.get( 6 ).getAsInt();
						double balBefTopping = aPack.get( 8 ).getAsDouble();
						String continuity = aPack.get( 9 ).getAsString();

						if( thisPackPrice > 0 ) {
							if( thisPackCat.trim().length() > 0 )
								packCount++;	
							validitySeries.add( new JsonPrimitive( thisPackValidity ) );
							feeSeries.add( new JsonPrimitive( thisPackPrice ) );
							packCatSeries.add( new JsonPrimitive( thisPackCat ) );
							if( aPack.size() == 14 )
								speedSeries.add( new JsonPrimitive( aPack.get( 10 ).getAsDouble() ) );

							if( aPack.size() < 9 || balBefTopping <= 0 ) 
								priceSenSeries.add( new JsonPrimitive( 0.99) );
							else
								priceSenSeries.add( new JsonPrimitive( thisPackPrice / balBefTopping ) ); 
						}	
						// active package
						if( thisPackEndDate > currentTime ) {
							if (! cancelled) {
								activePackBenType += "|" + thisPackBenefit;
								allActivePackId += "|" + thisPackCode;
								if( thisPackCat.contains( "DATA" ) && thisPackPrice > 0 ) {
									if( thisPackBenefit.contains("UNLIMITED") ) {
										activeDataPackCount++;
										if( thisPackEndDate > maxEndDate ) {
											maxEndDate = thisPackEndDate;
											minsLeft = ( maxEndDate - currentTime ) / 60000;
											if( minsLeft > 86400 )
												minsLeft = 86400;
											if( minsLeft < -86400 )
												minsLeft = -86400;
										}
									}
									else if( thisPackBenefit.contains("SUPERBOOSTER") )
										activeDataPackCount++;
									else if ( thisPackBenefit.contains("BOOSTER") )
										activeBoosterPackCount++;
									else if ( thisPackBenefit.contains("LIMITED") )
										activeDataPackCount++;
								}
								if (thisPackCat.contains( "COMBO" ) && thisPackPrice > 0 ) {
									if( thisPackBenefit.contains( "UNLIMITED" ) ) {
										activeComboPackCount++;
										if ( thisPackBenefit.contains( "FLAT" ) ) 
											if( thisPackValidity == 1 )
												actPackSpeed1d = "Y";
											else
												actPackSpeedGT1d = "Y";

										if( thisPackEndDate > maxEndDate ) {
											maxEndDate = thisPackEndDate;
											minsLeft = ( maxEndDate - currentTime ) / 60000;
											if( minsLeft > 86400 )
												minsLeft = 86400;
											if( minsLeft < -86400 )
												minsLeft = -86400;
										}
									}
									else if (thisPackBenefit.contains( "LIMITED" ) )
										activeComboPackCount++;
								}
								if( thisPackCat.contains( "VOICE" ) && thisPackPrice > 0 )
									activeVoicePackCount++;
							}
						}	

						if( thisPackBenefit.contains("SUPERBOOSTER") )
							excBoosterOtherFlg = "N";
						else if( thisPackBenefit.contains("BOOSTER") )
							excBoosterOtherFlg = "Y";
						else if( thisPackBenefit.contains("UNLIMITED") )
							excBoosterOtherFlg = "N";
						else if( thisPackBenefit.contains("LIMITED") )
							excBoosterOtherFlg = "N";
						else
							excBoosterOtherFlg = "Y";

						if( lkey >= dayEnd1to30 && lkey <= dayStart1 && thisPackPrice > 0 && excBoosterOtherFlg.equals("N") ) {
							packUsageFeeSum += thisPackPrice;
							if( thisPackCat.contains( "VOICE" ) || thisPackCat.contains( "COMBO" ) )
								voiceComboPackFeeSum30d += thisPackPrice;
							if( thisPackCat.contains( "DATA" ) || thisPackCat.contains( "COMBO" ) )
								dataPackFeeSum1to30 += thisPackPrice;
						}

						if( lkey < dayEnd30 )  //all the 0-30 days indicators
							continue;

						//to make logic same as DTAC_LAST_RECURRING_OBJECT_LifeTime
						if( ! cancelled && continuity.equals( "RECURRING" ) && thisPackCode.startsWith(  "2" ) && thisPackEndDate <= SiftMath.getCurrentDay() ) {
							if( thisPackEndDate > actRecurringEndDate ) {
    							actRecurringPack = thisPackCode;
    							actRecurringEndDate = thisPackEndDate;
							}
							else if( thisPackEndDate == actRecurringEndDate && actRecurringEndDate != 0  ) {
    							actRecurringPack +=  "|" + thisPackCode;
    							actRecurringEndDate = thisPackEndDate;
    						}
						}

						if( thisPackPrice > 0 && excBoosterOtherFlg.equalsIgnoreCase( "N") ) {
							packValiditySum += thisPackValidity;

							if( thisPackCat.trim().length() > 0 ) {
								pkgCodeSeries.add( thisPackCode );
								pkgBenSeries.add( thisPackBenefit );
							}	


							if( thisPackValidity == 0 )
								zeroValidityPackCount++;

							if( thisPackCat.contains( "VOICE" )  ) {
								voicePackCount++;
								voicePackFeeSum += thisPackPrice;
								voicePackValiditySum += thisPackValidity;
								voice30dayValiditySeries.add( thisPackValidity );

								if(  lkey > dayEnd15 )
									voicePackFeeSum0T15D += thisPackPrice;

								if( thisPackBenefit.startsWith( "LIMITED" ) )
									lmVoicePackCount++;
								else if( thisPackBenefit.startsWith( "UNLIMITED" ) )
									umVoicePackCount++;
								voice30dayFeeSeries.add( thisPackFee );
							}	   

							if( thisPackCat.contains( "COMBO" ) ) {
								comboPackFeeSum += thisPackPrice;
								if( lkey > dayEnd15 )
									comboPackFeeSum0T15D += thisPackPrice;

							}

							if( thisPackCat.contains( "VOICE" ) || thisPackCat.contains( "COMBO" ) ) {
								if( lkey >= dayEnd1to7 && lkey <= dayStart1 )
									voiceComboPackFeeSum7d += thisPackPrice;
							}		

							if( thisPackCat.contains( "DATA" ) || thisPackCat.contains( "COMBO" ) ) {
								dataPackCount++;
								dataPackFeeSum += thisPackPrice;

								if( thisPackCat.contains( "DATA" ) ) {
									onlyDataPackFeeSum += thisPackPrice;
    								if( lkey > dayEnd15  )
    									dataPackFeeSum0T15D += 	thisPackPrice;
								}

								dataPackValiditySum += thisPackValidity;
								data30dayFeeSeries.add( thisPackFee );
								data30dayValiditySeries.add( thisPackValidity );

								if( thisPackBenefit.startsWith( "LIMITED" ) )
									lmDataPackCount++;
								else if( thisPackBenefit.startsWith( "UNLIMITED" ) )
									umDataPackCount++;
								else if( thisPackBenefit.startsWith( "UNLIMITED FLAT" ) )
									umDataFlatPackCount++;

								if( lkey >= dayEnd1to7 && lkey <= dayStart1 )
									dataPackFeeSum7d += thisPackPrice;
							}	   
						}
					}
				}
			}

			JsonObject result = new JsonObject();
			result.addProperty( "activeDataPackCount", activeDataPackCount );
			result.addProperty( "activeVoicePackCount", activeVoicePackCount );
			result.addProperty( "dataPackCount", dataPackCount );
			result.addProperty( "activeComboPackCount", activeComboPackCount );
			result.addProperty( "activeBoosterPackCount", activeBoosterPackCount );
			result.addProperty( "dataPackFeeSum", dataPackFeeSum );
			result.addProperty( "dataPackValiditySum", dataPackValiditySum );
			result.addProperty( "packValiditySum", packValiditySum );
			result.addProperty( "zeroValidityPackCount", zeroValidityPackCount );
			result.addProperty( "lmDataPackCount", lmDataPackCount );
			result.addProperty( "lmVoicePackCount", lmVoicePackCount );
			result.addProperty( "umDataPackCount", umDataPackCount );
			result.addProperty( "umDataFlatPackCount", umDataFlatPackCount );
			result.addProperty( "umVoicePackCount", umVoicePackCount );
			result.addProperty( "packCount", packCount );

			result.addProperty( "voicePackCount", voicePackCount );
			result.addProperty( "voicePackFeeSum", voicePackFeeSum );
			result.addProperty( "voicePackValiditySum", voicePackValiditySum );
			result.addProperty( "packUsageFeeSum", packUsageFeeSum );

			result.addProperty( "activeDataPackMins", minsLeft );
			result.addProperty( "allActivePackId", allActivePackId );

			activePackBenType = activePackBenType.trim();
			if( activePackBenType.contains( "SUPERBOOSTER" ) )
				activePackBenType = "SUPER_BOOSTER";
			else if( activePackBenType.contains( "BOOSTER" ) )
				activePackBenType = "BOOSTER";
			else if( activePackBenType.contains( "UNLIMITED" ) && activePackBenType.contains( "FLAT") )
				activePackBenType = "UNLIMITED_FLAT";
			else if( activePackBenType.contains( "UNLIMITED" ) && activePackBenType.contains( "ROLLOVER") )
				activePackBenType = "UNLIMITED_ROLLOVER";
			else if( activePackBenType.contains( "UNLIMITED" ) )
				activePackBenType = "UNLIMITED";
			else if( activePackBenType.contains( "LIMITED" ) )
				activePackBenType = "LIMITED";
			else
				activePackBenType = "OTHERS";

			result.addProperty( "activePackBenType", activePackBenType );

			result.addProperty( "avgDataFee30d", dataPackFeeSum1to30 / 30  );
			result.addProperty( "avgDataFee7d", dataPackFeeSum7d / 7  );
			result.addProperty( "avgVoiceComboFee30d", voiceComboPackFeeSum30d / 30  );
			result.addProperty( "avgVoiceComboFee7d", voiceComboPackFeeSum7d / 7  );

			result.addProperty( "actRecurringPack", actRecurringPack );
			result.addProperty( "actRecurringEndDate", actRecurringEndDate );

			result.addProperty( "dataPackFeeSum0T15D", dataPackFeeSum0T15D );
			result.addProperty( "comboPackFeeSum0T15D", comboPackFeeSum0T15D );
			result.addProperty( "voicePackFeeSum0T15D", voicePackFeeSum0T15D );

			result.addProperty( "dataPackFeeSum16T30D", onlyDataPackFeeSum - dataPackFeeSum0T15D );
			result.addProperty( "comboPackFeeSum16T30D", comboPackFeeSum - comboPackFeeSum0T15D );
			result.addProperty( "voicePackFeeSum16T30D", voicePackFeeSum - voicePackFeeSum0T15D );

			result.addProperty( "actPackSpeed1d", actPackSpeed1d );
			result.addProperty( "actPackSpeedGT1d", actPackSpeedGT1d );

			//sort data30dayFee, voice30dayFee. For median.
			java.util.Collections.sort( data30dayFeeSeries );
			java.util.Collections.sort( voice30dayFeeSeries );

			Double dataFeeMedian = 0.0;
			int middle = data30dayFeeSeries.size() / 2;
			if( data30dayFeeSeries.size() > 0 ) {
				if ( data30dayFeeSeries.size() % 2 == 1) 
					dataFeeMedian = ( Double )data30dayFeeSeries.get( middle );
				else 
					dataFeeMedian = new Double( ( ( Double )data30dayFeeSeries.get( middle - 1  ) + ( Double )data30dayFeeSeries.get( middle ) ) / 2.0 );
			}
			result.addProperty( "dataFeeMedian", dataFeeMedian.doubleValue()  );

			Double voiceFeeMedian = 0.0;
			if( voice30dayFeeSeries.size() > 0 ) {
				middle = voice30dayFeeSeries.size() / 2;
				if ( voice30dayFeeSeries.size() % 2 == 1) 
					voiceFeeMedian = ( Double )voice30dayFeeSeries.get( middle );
				else 
					voiceFeeMedian = new Double( ( ( Double )voice30dayFeeSeries.get( middle - 1  ) + ( Double )voice30dayFeeSeries.get( middle ) ) / 2.0 );
			}
			result.addProperty( "voiceFeeMedian", voiceFeeMedian.doubleValue()  );


			double maxValue = 0;
			double maxCount = 0;
			for (int i = 0; i < data30dayValiditySeries.size(); ++i) {
				double count = 0;
				for (int j = 0; j < data30dayValiditySeries.size(); ++j) {
					if ( ( Integer )data30dayValiditySeries.get( j ) == ( Integer )data30dayValiditySeries.get( i ) ) ++count;
				}
				if (count > maxCount) {
					maxCount = count;
					maxValue = ( Integer )data30dayValiditySeries.get( i );
				}
			}
			result.addProperty( "modDataValidity", maxValue );

			maxValue = 0;
			maxCount = 0;
			for (int i = 0; i < validitySeries.size(); ++i) {
				double count = 0;
				for (int j = 0; j < validitySeries.size(); ++j) {
					if( validitySeries.get( j ).getAsDouble() == validitySeries.get( i ).getAsDouble() ) ++count;
				}
				if (count > maxCount) {
					maxCount = count;
					maxValue = validitySeries.get( i ).getAsDouble();
				}
			}
			result.addProperty( "modValidity", maxValue );

			maxValue = 0;
			maxCount = 0;
			for (int i = 0; i < voice30dayValiditySeries.size(); ++i) {
				double count = 0;
				for (int j = 0; j < voice30dayValiditySeries.size(); ++j) {
					if ( ( Integer )voice30dayValiditySeries.get( j ) == ( Integer )voice30dayValiditySeries.get( i ) ) ++count;
				}
				if (count > maxCount) {
					maxCount = count;
					maxValue = ( Integer )voice30dayValiditySeries.get( i );
				}
			}
			result.addProperty( "modVoiceValidity", maxValue );


			String strmaxValue = "0";
			maxCount = 0;
			for (int i = 0; i < pkgCodeSeries.size(); ++i) {
				if( ! ( ( String )pkgCodeSeries.get( i ) ).startsWith( "2" ) || ( ( String )pkgBenSeries.get( i ) ).contains("SUPERBOOSTER") )
					continue;
				double count = 0;
				for (int j = 0; j < pkgCodeSeries.size(); ++j) {
					if ( ( ( String )pkgCodeSeries.get( j ) ).equals( ( String )pkgCodeSeries.get( i ) ) ) ++count;
				}
				if ( count > maxCount) {
					maxCount = count;
					strmaxValue = ( String )pkgCodeSeries.get( i );
				}
			}
			result.addProperty( "mostBoughtPackCode", strmaxValue );
			result.add( "validitySeries", validitySeries );
			result.add( "feeSeries", feeSeries );
			result.add( "packCatSeries", packCatSeries );
			result.add( "priceSenSeries", priceSenSeries );
			result.add( "speedSeries", speedSeries );

			return result;
		}
		return DTAC_PKG_SUMMARY_LifeTime;
	}

/*
	public JsonObject computeDerivatives( JsonObject DTAC_PKG_HIST_SERIES_LifeTime, String RECORD_TYPE, String IDD_FLAG, JsonObject DTAC_PKG_SUMMARY_LifeTime ) {

		if( RECORD_TYPE.equals( "CREDIT" ) 
				|| RECORD_TYPE.equals( "BALANCE-CHECK" ) || RECORD_TYPE.equals( "GET_OFFER" ) 
				|| ( RECORD_TYPE.equals( "VOICE" ) && ! (IDD_FLAG.equals( "IDD" ) ) ) ) {

			java.util.Set<java.util.Map.Entry<String, JsonElement>> entrySet = DTAC_PKG_HIST_SERIES_LifeTime.entrySet();
			if( entrySet.size() == 0 )
				return DTAC_PKG_SUMMARY_LifeTime;

			double activeDataPackCount = 0;
			double activeVoicePackCount = 0;
			double dataPackCount = 0;
			double activeComboPackCount = 0;
			double activeBoosterPackCount = 0;
			double dataPackFeeSum = 0;
			double dataPackFeeSum1to30 = 0;

			double comboPackFeeSum = 0;
			double dataPackFeeSum0T15D = 0;
			double voicePackFeeSum0T15D = 0;
			double comboPackFeeSum0T15D = 0;

			double dataPackFeeSum7d = 0;
			double dataPackValiditySum = 0;
			double packValiditySum = 0;
			double zeroValidityPackCount = 0;
			double lmDataPackCount = 0;
			double lmVoicePackCount = 0;
			double umDataPackCount = 0;
			double umDataFlatPackCount = 0;
			double umVoicePackCount = 0;
			double packCount = 0;
			double voicePackCount = 0;
			double voicePackFeeSum = 0;
			double voicePackValiditySum = 0;
			double packUsageFeeSum = 0;
			double voiceComboPackFeeSum7d = 0;
			double voiceComboPackFeeSum30d = 0;
			long maxEndDate = 0;
			long minsLeft = 0;
			String activePackBenType = "OTHERS";
			String actRecurringPack = "NA";
			long actRecurringEndDate = 0;
			String actPackSpeed1d = "N";
			String actPackSpeedGT1d = "N";
			String allActivePackId = "|";

			java.util.ArrayList< Double > data30dayFeeSeries = new java.util.ArrayList< Double >();
			java.util.ArrayList< Double > voice30dayFeeSeries = new java.util.ArrayList< Double >();
			java.util.ArrayList< Integer > data30dayValiditySeries = new java.util.ArrayList< Integer >();
			JsonArray validitySeries = new JsonArray();
			JsonArray feeSeries = new JsonArray();
			JsonArray packCatSeries = new JsonArray();
			JsonArray priceSenSeries = new JsonArray();
			JsonArray speedSeries = new JsonArray();
			java.util.ArrayList< Integer > voice30dayValiditySeries = new java.util.ArrayList< Integer >();
			java.util.ArrayList< String > pkgCodeSeries = new java.util.ArrayList< String >();

			long currentTime = SiftMath.getCurrentTime();

			long start = currentTime;
			start -= start % 86400000L;
			long dayStart1 = start - 1 * 86400000L;
			long dayEnd1to30 = start - 30 * 86400000L;
			long dayEnd1to7 = start - 7 * 86400000L;
			long dayEnd30 = start - 29 * 86400000L;
			long dayEnd60 = start - 59 * 86400000L;
			long dayEnd15 = start - 15 * 86400000L;

			for(java.util.Map.Entry<String,JsonElement> entry : entrySet){
				String thiskey = entry.getKey().toString();
				long lkey = Long.parseLong( thiskey );

				if( lkey <= start && lkey >= dayEnd60 ) {  //all the 0-60 days indicators
					JsonArray packsInDay = DTAC_PKG_HIST_SERIES_LifeTime.get(entry.getKey().toString()).getAsJsonArray();
					for( int i=0; i<packsInDay.size(); i++ ) {
						JsonArray aPack = packsInDay.get( i ).getAsJsonArray();

						long thisPackEndDate = aPack.get( 1 ).getAsLong();
						boolean cancelled = false;	
						//skip the cancelled packs - only if its active
						if( aPack.size() == 14 && aPack.get( 13 ).getAsBoolean() == true && thisPackEndDate > currentTime )
							cancelled = true;

						String thisPackCode = aPack.get( 2 ).getAsString();
						String thisPackCat = aPack.get( 3 ).getAsString();
						String thisPackBenefit = aPack.get( 4 ).getAsString();
						double thisPackPrice = aPack.get( 5 ).getAsDouble();
						double thisPackFee = aPack.get( 7 ).getAsDouble();
						int thisPackValidity = aPack.get( 6 ).getAsInt();
						double balBefTopping = aPack.get( 8 ).getAsDouble();
						String continuity = aPack.get( 9 ).getAsString();

						if( thisPackPrice > 0 ) {
							if( thisPackCat.trim().length() > 0 )
								packCount++;	
							validitySeries.add( new JsonPrimitive( thisPackValidity ) );
							feeSeries.add( new JsonPrimitive( thisPackPrice ) );
							packCatSeries.add( new JsonPrimitive( thisPackCat ) );
							if( aPack.size() == 14 )
								speedSeries.add( new JsonPrimitive( aPack.get( 10 ).getAsDouble() ) );

							if( aPack.size() < 9 ) 
								priceSenSeries.add( new JsonPrimitive( 0.99) );
							else
								priceSenSeries.add( new JsonPrimitive( thisPackPrice / balBefTopping ) ); 
						}	
						if( thisPackEndDate > currentTime ) {
							if( ! cancelled )
								allActivePackId += "|" + thisPackCode;
							if( ( thisPackCat.contains( "DATA" ) || thisPackCat.contains( "COMBO" ) ) && thisPackPrice > 0 ) {
								if( ! cancelled )
									activeDataPackCount++;
								if( thisPackBenefit.contains( "UNLIMITED" ) && thisPackEndDate > maxEndDate && ! cancelled ) {
									maxEndDate = thisPackEndDate;
									minsLeft = ( maxEndDate - currentTime ) / 60000;
									if( minsLeft > 86400 )
										minsLeft = 86400;
									if( minsLeft < -86400 )
										minsLeft = -86400;
								}
							}
						}	

						if( lkey >= dayEnd1to30 && lkey <= dayStart1 && thisPackPrice > 0 ) {
							packUsageFeeSum += thisPackPrice;
							if( thisPackCat.contains( "VOICE" ) || thisPackCat.contains( "COMBO" ) )
								voiceComboPackFeeSum30d += thisPackPrice;
							if( thisPackCat.contains( "DATA" ) || thisPackCat.contains( "COMBO" ) )
								dataPackFeeSum1to30 += thisPackPrice;
						}

						if( lkey < dayEnd30 )  //all the 0-30 days indicators
							continue;


						if( thisPackPrice > 0 ) {
							packValiditySum += thisPackValidity;

							if( thisPackCat.trim().length() > 0 )
								pkgCodeSeries.add( thisPackCode );

							if( thisPackEndDate > currentTime && ! cancelled ) {
								if( thisPackCat.contains( "COMBO" ) ) {
									activeComboPackCount++;
									if( thisPackBenefit.contains( "UNLIMITED") && thisPackBenefit.contains( "FLAT") )
										if( thisPackValidity == 1 )
											actPackSpeed1d = "Y";	
										else
											actPackSpeedGT1d = "Y";	
								}	
								if( thisPackBenefit.contains( "BOOSTER" ) ) 
									activeBoosterPackCount++;
								if( continuity.equals( "RECURRING" ) ) {
									actRecurringPack = thisPackCode;
									actRecurringEndDate = thisPackEndDate;
								}
								activePackBenType = thisPackBenefit;
							} 

							//to make logic same as DTAC_LAST_RECURRING_OBJECT_LifeTime
							if( thisPackEndDate == actRecurringEndDate && actRecurringEndDate != 0 && ! cancelled && continuity.equals( "RECURRING" ) ) {
								actRecurringPack +=  "|" + thisPackCode;
								actRecurringEndDate = thisPackEndDate;
							}

							if( thisPackValidity == 0 )
								zeroValidityPackCount++;

							if( thisPackCat.contains( "VOICE" )  ) {
								voicePackCount++;
								voicePackFeeSum += thisPackPrice;
								voicePackValiditySum += thisPackValidity;
								voice30dayValiditySeries.add( thisPackValidity );

								if(  lkey > dayEnd15 )
									voicePackFeeSum0T15D += thisPackPrice;

								if( thisPackBenefit.startsWith( "LIMITED" ) )
									lmVoicePackCount++;
								else if( thisPackBenefit.startsWith( "UNLIMITED" ) )
									umVoicePackCount++;
								if( thisPackEndDate > currentTime && ! cancelled )
									activeVoicePackCount++;
								voice30dayFeeSeries.add( thisPackFee );
							}	   

							if( thisPackCat.contains( "COMBO" ) ) {
								comboPackFeeSum += thisPackPrice;
								if( lkey > dayEnd15 )
									comboPackFeeSum0T15D += thisPackPrice;

							}

							if( thisPackCat.contains( "VOICE" ) || thisPackCat.contains( "COMBO" ) ) {
								if( lkey >= dayEnd1to7 && lkey <= dayStart1 )
									voiceComboPackFeeSum7d += thisPackPrice;
							}		

							if( thisPackCat.contains( "DATA" ) || thisPackCat.contains( "COMBO" ) ) {
								dataPackCount++;
								dataPackFeeSum += thisPackPrice;

								if( lkey > dayEnd15 && thisPackCat.contains( "DATA" ) )
									dataPackFeeSum0T15D += 	thisPackPrice;

								dataPackValiditySum += thisPackValidity;
								data30dayFeeSeries.add( thisPackFee );
								data30dayValiditySeries.add( thisPackValidity );

								if( thisPackBenefit.startsWith( "LIMITED" ) )
									lmDataPackCount++;
								else if( thisPackBenefit.startsWith( "UNLIMITED" ) )
									umDataPackCount++;
								else if( thisPackBenefit.startsWith( "UNLIMITED FLAT" ) )
									umDataFlatPackCount++;

								if( lkey >= dayEnd1to7 && lkey <= dayStart1 )
									dataPackFeeSum7d += thisPackPrice;
							}	   
						}
					}
				}
			}

			JsonObject result = new JsonObject();
			result.addProperty( "activeDataPackCount", activeDataPackCount );
			result.addProperty( "activeVoicePackCount", activeVoicePackCount );
			result.addProperty( "dataPackCount", dataPackCount );
			result.addProperty( "activeComboPackCount", activeComboPackCount );
			result.addProperty( "activeBoosterPackCount", activeBoosterPackCount );
			result.addProperty( "dataPackFeeSum", dataPackFeeSum );
			result.addProperty( "dataPackValiditySum", dataPackValiditySum );
			result.addProperty( "packValiditySum", packValiditySum );
			result.addProperty( "zeroValidityPackCount", zeroValidityPackCount );
			result.addProperty( "lmDataPackCount", lmDataPackCount );
			result.addProperty( "lmVoicePackCount", lmVoicePackCount );
			result.addProperty( "umDataPackCount", umDataPackCount );
			result.addProperty( "umDataFlatPackCount", umDataFlatPackCount );
			result.addProperty( "umVoicePackCount", umVoicePackCount );
			result.addProperty( "packCount", packCount );

			result.addProperty( "voicePackCount", voicePackCount );
			result.addProperty( "voicePackFeeSum", voicePackFeeSum );
			result.addProperty( "voicePackValiditySum", voicePackValiditySum );
			result.addProperty( "packUsageFeeSum", packUsageFeeSum );

			result.addProperty( "activeDataPackMins", minsLeft );
			result.addProperty( "allActivePackId", allActivePackId );

			activePackBenType = activePackBenType.trim();
			if( activePackBenType.contains( "SUPERBOOSTER" ) )
				activePackBenType = "SUPER_BOOSTER";
			else if( activePackBenType.contains( "BOOSTER" ) )
				activePackBenType = "BOOSTER";
			else if( activePackBenType.contains( "UNLIMITED" ) && activePackBenType.contains( "FLAT") )
				activePackBenType = "UNLIMITED_FLAT";
			else if( activePackBenType.contains( "UNLIMITED" ) && activePackBenType.contains( "ROLLOVER") )
				activePackBenType = "UNLIMITED_ROLLOVER";
			else if( activePackBenType.contains( "UNLIMITED" ) )
				activePackBenType = "UNLIMITED";
			else if( activePackBenType.contains( "LIMITED" ) )
				activePackBenType = "LIMITED";
			else
				activePackBenType = "OTHERS";

			result.addProperty( "activePackBenType", activePackBenType );

			result.addProperty( "avgDataFee30d", dataPackFeeSum1to30 / 30  );
			result.addProperty( "avgDataFee7d", dataPackFeeSum7d / 7  );
			result.addProperty( "avgVoiceComboFee30d", voiceComboPackFeeSum30d / 30  );
			result.addProperty( "avgVoiceComboFee7d", voiceComboPackFeeSum7d / 7  );

			result.addProperty( "actRecurringPack", actRecurringPack );
			result.addProperty( "actRecurringEndDate", actRecurringEndDate );

			result.addProperty( "dataPackFeeSum0T15D", dataPackFeeSum0T15D );
			result.addProperty( "comboPackFeeSum0T15D", comboPackFeeSum0T15D );
			result.addProperty( "voicePackFeeSum0T15D", voicePackFeeSum0T15D );

			result.addProperty( "dataPackFeeSum16T30D", dataPackFeeSum - dataPackFeeSum0T15D );
			result.addProperty( "comboPackFeeSum16T30D", comboPackFeeSum - comboPackFeeSum0T15D );
			result.addProperty( "voicePackFeeSum16T30D", voicePackFeeSum - voicePackFeeSum0T15D );

			result.addProperty( "actPackSpeed1d", actPackSpeed1d );
			result.addProperty( "actPackSpeedGT1d", actPackSpeedGT1d );

			//sort data30dayFee, voice30dayFee. For median.
			java.util.Collections.sort( data30dayFeeSeries );
			java.util.Collections.sort( voice30dayFeeSeries );

			Double dataFeeMedian = 0.0;
			int middle = data30dayFeeSeries.size() / 2;
			if( data30dayFeeSeries.size() > 0 ) {
				if ( data30dayFeeSeries.size() % 2 == 1) 
					dataFeeMedian = ( Double )data30dayFeeSeries.get( middle );
				else 
					dataFeeMedian = new Double( ( ( Double )data30dayFeeSeries.get( middle - 1  ) + ( Double )data30dayFeeSeries.get( middle ) ) / 2.0 );
			}
			result.addProperty( "dataFeeMedian", dataFeeMedian.doubleValue()  );

			Double voiceFeeMedian = 0.0;
			if( voice30dayFeeSeries.size() > 0 ) {
				middle = voice30dayFeeSeries.size() / 2;
				if ( voice30dayFeeSeries.size() % 2 == 1) 
					voiceFeeMedian = ( Double )voice30dayFeeSeries.get( middle );
				else 
					voiceFeeMedian = new Double( ( ( Double )voice30dayFeeSeries.get( middle - 1  ) + ( Double )voice30dayFeeSeries.get( middle ) ) / 2.0 );
			}
			result.addProperty( "voiceFeeMedian", voiceFeeMedian.doubleValue()  );


			double maxValue = 0;
			double maxCount = 0;
			for (int i = 0; i < data30dayValiditySeries.size(); ++i) {
				double count = 0;
				for (int j = 0; j < data30dayValiditySeries.size(); ++j) {
					if ( ( Integer )data30dayValiditySeries.get( j ) == ( Integer )data30dayValiditySeries.get( i ) ) ++count;
				}
				if (count > maxCount) {
					maxCount = count;
					maxValue = ( Integer )data30dayValiditySeries.get( i );
				}
			}
			result.addProperty( "modDataValidity", maxValue );

			maxValue = 0;
			maxCount = 0;
			for (int i = 0; i < validitySeries.size(); ++i) {
				double count = 0;
				for (int j = 0; j < validitySeries.size(); ++j) {
					if( validitySeries.get( j ).getAsDouble() == validitySeries.get( i ).getAsDouble() ) ++count;
				}
				if (count > maxCount) {
					maxCount = count;
					maxValue = validitySeries.get( i ).getAsDouble();
				}
			}
			result.addProperty( "modValidity", maxValue );

			maxValue = 0;
			maxCount = 0;
			for (int i = 0; i < voice30dayValiditySeries.size(); ++i) {
				double count = 0;
				for (int j = 0; j < voice30dayValiditySeries.size(); ++j) {
					if ( ( Integer )voice30dayValiditySeries.get( j ) == ( Integer )voice30dayValiditySeries.get( i ) ) ++count;
				}
				if (count > maxCount) {
					maxCount = count;
					maxValue = ( Integer )voice30dayValiditySeries.get( i );
				}
			}
			result.addProperty( "modVoiceValidity", maxValue );


			String strmaxValue = "0";
			maxCount = 0;
			for (int i = 0; i < pkgCodeSeries.size(); ++i) {
				double count = 0;
				for (int j = 0; j < pkgCodeSeries.size(); ++j) {
					if ( ( ( String )pkgCodeSeries.get( j ) ).equals( ( String )pkgCodeSeries.get( i ) ) ) ++count;
				}
				if ( count > maxCount) {
					maxCount = count;
					strmaxValue = ( String )pkgCodeSeries.get( i );
				}
			}
			result.addProperty( "mostBoughtPackCode", strmaxValue );
			result.add( "validitySeries", validitySeries );
			result.add( "feeSeries", feeSeries );
			result.add( "packCatSeries", packCatSeries );
			result.add( "priceSenSeries", priceSenSeries );
			result.add( "speedSeries", speedSeries );

			return result;
		}
		return DTAC_PKG_SUMMARY_LifeTime;
	}

*/
	public JsonArray selectNBOOffers( JsonObject DTAC_NBO_MODEL_LifeTime) {
		String recommededOffer = DTAC_NBO_MODEL_LifeTime.get( "recommend_offer" ).getAsString();
		String toppingStatus = DTAC_NBO_MODEL_LifeTime.get( "topping_status_flg" ).getAsString();
		String packType = "";
		double packVld = 0;
		double packSpeed = 0;
		if( recommededOffer.equals( "DATA" ) ) {
			packType =  DTAC_NBO_MODEL_LifeTime.get( "data_pack_typ" ).getAsString();
			packVld = DTAC_NBO_MODEL_LifeTime.get( "data_pack_vldp" ).getAsDouble();
			packSpeed = DTAC_NBO_MODEL_LifeTime.get( "data_pack_spd" ).getAsDouble();
		} else if( recommededOffer.equals( "VOICE" ) ) {
			packType =  DTAC_NBO_MODEL_LifeTime.get( "vc_pack_typ" ).getAsString();
			packVld = DTAC_NBO_MODEL_LifeTime.get( "vc_pack_vldp" ).getAsDouble();
		} else if( recommededOffer.equals( "COMBO" ) ) {
			packType =  DTAC_NBO_MODEL_LifeTime.get( "combo_pack_typ" ).getAsString();
			packVld = DTAC_NBO_MODEL_LifeTime.get( "combo_pack_vldp" ).getAsDouble();
			packSpeed = DTAC_NBO_MODEL_LifeTime.get( "combo_pack_spd" ).getAsDouble();
		} else if( recommededOffer.equals( "CONTENT" ) ) {
			packType =  DTAC_NBO_MODEL_LifeTime.get( "content_pack_typ" ).getAsString();
			packVld = DTAC_NBO_MODEL_LifeTime.get( "content_pack_vldp" ).getAsDouble();
		}
		//Rule 1
		JsonArray rule1Offers = getOffersByAttribute( "packType,toppingFlag", "=,=", packType + "," + toppingStatus, null );

		//Rule2
		JsonArray finalList = new JsonArray();
		for( int i=0; i<rule1Offers.size(); i++ ) {
			//Apply Rule 2 Here

			//Apply Rule 3 here
		}

		//Rule 4
		if( finalList.size() == 0 ) {
			//Attach the Standard Offer Id here
		}
		return finalList;
	}

	private JsonArray getOffersByAttribute( String lhs, String operands, String rhs, JsonArray listOfOffers  ) {
		// TODO Auto-generated method stub
		return new JsonArray();
	}


	public static void batchCompare( String csvFile, PandaCache pandaCache ) throws JsonSyntaxException, IOException {
		CSVReader reader = new CSVReader( new FileReader( csvFile ) );
		String line[] = null;
		while(( line = reader.readNext() ) != null ) {
			String thisDoc = ( String )pandaCache.get( line[ 0 ] + "-Indicators" );
			JsonParser parser = new JsonParser();
			JsonObject jobj = ( JsonObject ) parser.parse( thisDoc );
			PkgDerivatives der = new PkgDerivatives();
//			JsonObject result1 = der.computeDerivatives( jobj.get( "DTAC_PKG_HIST_SERIES_LifeTime-L" ).getAsJsonObject(), "BALANCE-CHECK", "NA", new JsonObject() );
			JsonObject result2 = der.computeDerivativesReconciled( jobj.get( "DTAC_PKG_HIST_SERIES_LifeTime-L" ).getAsJsonObject(), "BALANCE-CHECK", "NA", new JsonObject() );
			System.out.println( "----------------------- Working on MSISDN : " + line[ 0 ] + "-----------------------------------" );
			System.out.println( "PackageSeries : " + jobj.get( "DTAC_PKG_HIST_SERIES_LifeTime-L" ).getAsJsonObject() );
//			System.out.println( "PackageSeries - oldVersion : " + result1.toString() );
			System.out.println( "PackageSeries - NewVersion : " + result2.toString() );
//			Iterator< Entry< String, JsonElement > > itr = result1.entrySet().iterator();
//			while( itr.hasNext() ) {
//				Entry< String, JsonElement > entry = itr.next();
//				String key = entry.getKey();
//				if( ! result1.get( key ).toString().equals( result2.get( key ).toString() ) )
//					System.out.println(  "Not Matching for : " + key + ". Old/New = " +  result1.get( key ).toString() + " / " +  result2.get( key ).toString() );
//			}
		}
	}

	/**
	 * @param args
	 * @throws URISyntaxException 
	 * @throws IOException 
	 * @throws JsonSyntaxException 
	 */
	public static void main( String[ ] args ) throws URISyntaxException, JsonSyntaxException, IOException {
		LinkedList< URI > uris = new LinkedList< URI >();
		uris.add( new URI( "http://10.89.104.49:8091/pools" ) );

		PandaCache pandaCache = new PandaCache( uris );
		String thisDoc = ( String )pandaCache.get( "66920231901-Indicators" );
		System.out.println( thisDoc   );
		JsonParser parser = new JsonParser();
		JsonObject jobj = ( JsonObject ) parser.parse( thisDoc );
		System.out.println( jobj.toString()   );

		PkgDerivatives der = new PkgDerivatives();
		System.out.println( der.computeDerivativesReconciled( jobj.get( "DTAC_PKG_HIST_SERIES_LifeTime-L" ).getAsJsonObject(), "BALANCE-CHECK", "NA", new JsonObject() ) );
		//		PkgDerivatives der = new PkgDerivatives();
		//		der.batchCompare( "/home/streamsadmin/Documents/DTAC/PKGTEST1/ind.csv", pandaCache );
	}

	private double score(String SELECTED_OFFER_CATEGORY, boolean BTL_GROUP, boolean ATL_GROUP, boolean BTL_SPECIAL_GROUP, String SELECTED_OFFER_LOCATION, boolean TOURIST) {
		if( TOURIST ) return  getNumericOfferAttribute( "touristScore" );
		if( getStringOfferAttribute( "offerPackageCode" ).equals( getStringOutputFromNBOModel( "recent_pack_cd" ) ) ) return 10000;
		if( SELECTED_OFFER_LOCATION.equals( "MostBuy" ) ) return 0;

		String selectedPkgSubGroup = ( String ) ( getStringOfferAttribute( "packageGroup" ).equals( "DATA" )  ? getStringOutputFromNBOModel(  "data_pack_typ" ) :
			getStringOfferAttribute( "packageGroup" ).equals( "VOICE" )  ? getStringOutputFromNBOModel(  "voice_pack_typ" ) : 
				getStringOfferAttribute( "packageGroup" ).equals( "COMBO" )  ? getStringOutputFromNBOModel(  "combo_pack_typ" ) : 
					getStringOfferAttribute( "packageGroup" ).equals( "BOOSTER" )  ? getStringOutputFromNBOModel(  "booster_pack_typ" ) : 
						getStringOutputFromNBOModel(  "combo_pack_typ" ) );

		if( SELECTED_OFFER_CATEGORY.equals( "NBO_Package" ) ) {
			if( getStringOfferAttribute( "packageGroup" ).equals( "COMBO" ) ) {
				if( ( SELECTED_OFFER_LOCATION.equals( "Recommend" ) && getStringOutputFromNBOModel( "recommend_shelf" ).equals( "COMBO|BOOSTER" ) ) ||
						( SELECTED_OFFER_LOCATION.equals( "Bubble_Package" ) && getStringOutputFromNBOModel( "banner_shelf" ).equals( "COMBO|BOOSTER" ) ) ) 
					return  getNumericOfferAttribute( selectedPkgSubGroup + "btlScore" ) * 100;
			}	
			if( getStringOfferAttribute( "packageGroup" ).equals( "BOOSTER" ) ) {
				return getNumericOfferAttribute( selectedPkgSubGroup + "btlScore" );
			}
			if( BTL_GROUP ) return getNumericOfferAttribute( selectedPkgSubGroup + "btlScore" );
			if( BTL_SPECIAL_GROUP ) return getNumericOfferAttribute( selectedPkgSubGroup + "btlSpecialScore" );
			if( ATL_GROUP ) return getNumericOfferAttribute( selectedPkgSubGroup + "atlScore" );
		} else {
			if( getStringOfferAttribute( "packageGroup" ).equals( "COMBO" ) ) {
				if( ( SELECTED_OFFER_LOCATION.equals( "Recommend" ) && getStringOutputFromNBOModel( "recommend_shelf" ).equals( "COMBO|BOOSTER" ) ) ||
						( SELECTED_OFFER_LOCATION.equals( "Bubble_Package" ) && getStringOutputFromNBOModel( "banner_shelf" ).equals( "COMBO|BOOSTER" ) ) ) 
					return  getNumericOfferAttribute( selectedPkgSubGroup + "btlStdScore" ) * 0;
			}	
			if( getStringOfferAttribute( "packageGroup" ).equals( "BOOSTER" ) ) 
				return getNumericOfferAttribute( selectedPkgSubGroup + "btlStdScore" );
			if( BTL_GROUP ) return getNumericOfferAttribute( selectedPkgSubGroup + "btlStdScore" );
			if( BTL_SPECIAL_GROUP ) return getNumericOfferAttribute( selectedPkgSubGroup + "btlSpecialStdScore" );
			if( ATL_GROUP ) return getNumericOfferAttribute( selectedPkgSubGroup + "atlStdScore" );
		}
		return 0;

	}
	
	public boolean isOfferOpen( JsonObject OFFER_HISTORY_LifeTime, String offerCategory ) {
		if( OFFER_HISTORY_LifeTime != null && OFFER_HISTORY_LifeTime.entrySet().size() > 0 ) {
			long currentTime = SiftMath.getCurrentTime();
			java.util.Set<java.util.Map.Entry<String, JsonElement>> entrySet = OFFER_HISTORY_LifeTime.entrySet();
			for(java.util.Map.Entry<String,JsonElement> entry : entrySet){
				String thiskey = entry.getKey().toString();
				JsonObject thisOffer = OFFER_HISTORY_LifeTime.get( thiskey ).getAsJsonObject();
				String offerCat = thisOffer.get( "CATEGORY" ).getAsString();
				if( ! offerCat.equals( offerCategory ) )
					continue;
				long offerEndDate = thisOffer.get( "OFFER_END_DATE" ).getAsLong();
				if( offerEndDate > currentTime ) {
					//check if the max fulfillment limits reached or not
//					String expKey = "MAX_FULFILLMENTS_" + thisOffer.get( "OFFER_ID" ).getAsString();
//					Object fulfilResult = evaluateExpression( expKey );
//					if( fulfilResult.toString().equals( "false" ) ) 
                        return true;
				}
			}	
		}
		return false;
	}
	

	private double getNumericOfferAttribute( String string ) {
		// TODO Auto-generated method stub
		return 0;
	}


	private Object getStringOfferAttribute( String string ) {
		// TODO Auto-generated method stub
		return null;
	}


	private Object getStringOutputFromNBOModel( String string ) {
		// TODO Auto-generated method stub
		return null;
	}

}
