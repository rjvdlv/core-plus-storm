package com.knowesis.sift.helper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.CouchbaseConnectionFactoryBuilder;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCache;
import com.knowesis.sift.Common.PandaCacheConfig;

public class LoadConfigDocs {

	/**
	 * @param args
	 * @throws URISyntaxException 
	 * @throws IOException 
	 */
	public static void main( String[ ] args ) throws URISyntaxException, IOException {
		if( args.length < 3 ) {
			System.err.println( "Usage : java -jar LoadConfigDocs.jar http://<SourcePersistAddress>:8091 http://<DestPersistAddress>:8091 csvFilePath" );
			System.exit( 0 );
		}
		java.util.HashMap< String, String > srcDocs = new java.util.HashMap< String, String >();
		
		String strDSourcePersistAddressList = args[ 0 ];
		String[ ] arrSourcePersistAddress = strDSourcePersistAddressList.split( "," );
		LinkedList< URI > urisSource = new LinkedList< URI >();
		for( int i = 0; i < arrSourcePersistAddress.length; i++ ) {
			Commons.logger.info( arrSourcePersistAddress[ i ] );
			urisSource.add( new URI( arrSourcePersistAddress[ i ] ) );
		}
		PandaCacheConfig sourcePandaCacheConfig = new PandaCacheConfig( urisSource );

		int importCount = 0;
		CSVReader reader = new CSVReader( new FileReader( args[ 2 ] ) );
        String[] readLine = null;
        while ( ( readLine = reader.readNext() ) != null ) {
        	String docName = readLine[ 0 ];
        	String content = ( String ) sourcePandaCacheConfig.get( docName );
        	srcDocs.put( docName, content );
        }
		reader.close();
		
		
		
		String strDestPersistAddressList = args[ 1 ];
		String[ ] arrDestPersistAddress = strDestPersistAddressList.split( "," );
		LinkedList< URI > urisDest = new LinkedList< URI >();
		for( int i = 0; i < arrDestPersistAddress.length; i++ ) {
			Commons.logger.info( arrDestPersistAddress[ i ] );
			urisDest.add( new URI( arrDestPersistAddress[ i ] ) );
		}
		
		CouchbaseConnectionFactoryBuilder cfb = new CouchbaseConnectionFactoryBuilder();
		cfb.setOpTimeout( 10000 );
		CouchbaseClient couchClientDest = new CouchbaseClient( cfb.buildCouchbaseConnection( urisDest, "config", "" ) );
		
		Set< String > keySet = srcDocs.keySet();
		Iterator< String > itr = keySet.iterator();
		while( itr.hasNext() ) {
			String key = itr.next();
    		System.out.println(  "Migrating Document  : " + key + " --------- " + srcDocs.get( key ) );
    		couchClientDest.set( key, 0, srcDocs.get( key ) )	;
        	importCount++;
		}
		System.out.println( "Imported Document Count : " + importCount );
//		System.exit( 0 );
	}	
}
