package com.knowesis.sift.helper;

import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Set;

import au.com.bytecode.opencsv.CSVWriter;
import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.protocol.views.Query;
import com.couchbase.client.protocol.views.View;
import com.couchbase.client.protocol.views.ViewResponse;
import com.couchbase.client.protocol.views.ViewRow;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.PandaCache;

public class IndicatorDumper {

	/**
	 * @param args
	 */

	private ArrayList< String > columnNames = null;
	private HashMap< String, Integer > columnNumbersMap = null;

	public static void main( String[ ] args ) throws IOException {

		IndicatorDumper dumper = new IndicatorDumper();
		CSVWriter writer = null;
		LinkedList< URI > uris = new LinkedList< URI >();
		uris.add( URI.create( args[ 0 ] ) );

		int nTotalRecords = Integer.parseInt( args[ 1 ] );
		int nbatchSize = Integer.parseInt( args[ 2 ] );
		int nFileSize =  Integer.parseInt( args[ 3 ] );
		int nInitialSkip = Integer.parseInt( args[ 4 ] );
		int nRecCount = 0;
		int nFileIndex = 0;

		PandaCache cache = new PandaCache( uris );
		View view = cache.getView( "view_AllIndicatorInstances" );
		CouchbaseClient client = cache.getClient();
		Query query = new Query();
		query.setLimit( nbatchSize );

		nTotalRecords = nTotalRecords - nInitialSkip;
		int iterationCount = nTotalRecords / nbatchSize;
		System.out.println( "Number of Iterations Plannded : " + iterationCount );
		for( int j = 0; j < iterationCount; j++ ) {
			
			if( ( nRecCount % nFileSize == 0 ) || nRecCount == 0 ) {
				if( writer != null )
					writer.close();
			    writer = new CSVWriter( new FileWriter ( "/tmp/persistdump" + nFileIndex++ + ".csv" ) );
			}
			
			query.setIncludeDocs( true );
			System.out.println( "Fetching Start from : " + ( nInitialSkip + ( j * nbatchSize ) ) );
			query.setSkip( nInitialSkip + ( j * nbatchSize ) );
			
			ViewResponse viewresponse = client.query( view, query );
			Iterator< ViewRow > itr = viewresponse.iterator();
			JsonParser parser = new JsonParser();
			while( itr.hasNext() ) {	
				ViewRow record = itr.next();
				String msisdn = record.getKey();
				msisdn = msisdn.split( "-" )[0];
				JsonObject jInd = ( JsonObject ) parser.parse( record.getDocument().toString() );
				if( nRecCount == 0 ) {
					System.out.println( "Fetching Column Names from Indciator : " + record.getKey() );
					dumper.initialiseTableHeader( jInd );
					String[] cols = new String[ dumper.columnNames.size() ];
					for( int i=0; i<dumper.columnNames.size(); i++ ) {
						cols[ i ] = dumper.columnNames.get( i );
					}
					writer.writeNext( cols );
				}

				nRecCount++;
				String[] aLine = new String[ dumper.columnNames.size() ];
				Set< Entry< String, JsonElement >> es = jInd.entrySet();
				Iterator< Entry< String, JsonElement >> itrObj = es.iterator();
				while( itrObj.hasNext() ) {
					Entry< String, JsonElement > entry = itrObj.next();
					String key = entry.getKey();
					JsonElement value = entry.getValue();
					aLine[ 0 ] = msisdn;
					if( value.isJsonPrimitive() && ! key.equals( "docType" ) ) {
						Integer index = dumper.columnNumbersMap.get( key );
						if( index != null )
							aLine[ index ] = value.toString();
					}
				}	
				writer.writeNext( aLine );
				if( nRecCount % 100000 == 0 )
					System.out.println( "Dump Count : " + nRecCount );
			}	
		}
		System.out.println( "Finished Dumping all Indicator Instances : " + nRecCount );
		writer.close();
	}

	private void initialiseTableHeader( JsonObject jInd ) {
		Set< Entry< String, JsonElement >> es = jInd.entrySet();
		Iterator< Entry< String, JsonElement >> itr = es.iterator();
		columnNames = new ArrayList< String >();
		columnNumbersMap = new HashMap< String, Integer >();
		columnNames.add( "MSISDN" );
		columnNumbersMap.put( "MSISDN", new Integer( 0 ) );
		while( itr.hasNext() ) {
			Entry< String, JsonElement > entry = itr.next();
			String key = entry.getKey();
			if( key.equals( "docType" ) )
				continue;
			JsonElement value = entry.getValue();
			if( value.isJsonPrimitive() ) {
				columnNames.add( key );
				columnNumbersMap.put( key, columnNames.size() - 1 );
			}
		}
		System.out.println( "Column Names Size : " + columnNames.size() );
		System.out.println( "Column Numbers Map Size : " + columnNumbersMap.size() );
	}
}
