package com.knowesis.sift.helper;

import java.io.IOException;

import com.unicacorp.interact.api.NameValuePair;

public class ProvisionCommand extends CampaignCommand {
	
	private String productId;
	private String notificationMessage;
	private String channel;
	
	
	public ProvisionCommand( String msisdn, NameValuePair[ ] params, String extId ) throws IOException{
		this.msisdn = msisdn;
		this.extId = extId;
		for( int i=0; i<params.length; i++ ) {
			String name = params[i].getName();
			if( name.equals( "IN_product_mapping" ) )
				productId = params[i].getValueAsString();
			else if( name.equals( "outbound_channel" ) )
				channel = params[i].getValueAsString();
			else if( name.equals( "NotificationMessage" ) )
				notificationMessage = params[i].getValueAsString();
		}	
	}
	
	@Override
    public void execute() throws IOException {
		this.provision( productId );
		this.notifySubscriber( channel, notificationMessage );
    }
}
