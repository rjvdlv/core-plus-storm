package com.knowesis.sift.helper;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.TimeZone;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCacheConfig;

import au.com.bytecode.opencsv.CSVReader;

public class RefillBonusImporter {

	private String readFile( String fileName ) throws IOException {
		BufferedReader br = new BufferedReader( new FileReader( fileName ) );
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while( line != null ) {
				sb.append( line );
				sb.append( "\n" );
				line = br.readLine();
			}
			return sb.toString();
		}finally {
			br.close();
		}
	}

	/**
	 * @param args
	 * @throws IOException
	 * @throws URISyntaxException
	 * @throws ParseException
	 */
	public static void main( String[ ] args ) throws IOException, URISyntaxException, ParseException {

		if( args.length < 3 ) {
			System.err.println( "Usage : java -jar FixBonus.jar http://<persistAddress>:8091 templatePath csvFilePath" );
			System.exit( 0 );
		}

		RefillBonusImporter fixBonus = new RefillBonusImporter();

		CSVReader reader = new CSVReader( new FileReader( args[ 2 ] ) );
		String[ ] line = null;

		String strPersistAddressList = args[ 0 ];
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );
		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddress.length; i++ ) {
			Commons.logger.info( arrPersistAddress[ i ] );
			uris.add( new URI( arrPersistAddress[ i ] ) );
		}
		PandaCacheConfig pandaCacheConfig = new PandaCacheConfig( uris );

		int nCount = 0;
		while( ( line = reader.readNext() ) != null ) {
			String template = fixBonus.readFile( args[ 1 ] );
			JsonParser parser = new JsonParser();
			JsonObject jTemplate = ( JsonObject ) parser.parse( template );

			String offerId = line[ 0 ];
			offerId = offerId.replaceAll( "  ", " " ).replaceAll( ": ", ":" ).replaceAll( " ", "_" ).replaceAll( "/", "_" ).replaceAll( ":", "_" ).replaceAll( "\\.", "_" ).replaceAll( "-", "_" ).replaceAll( "\\+", "_" )
					.replaceAll( "\\(", "_" ).replaceAll( "\\)", "_" ).replaceAll( "__", "_" );
			jTemplate.addProperty( "name", line[ 1 ] );
			jTemplate.addProperty( "description", offerId );

			String applicableChannels = line[ 4 ];
			jTemplate.addProperty( "applicableLocations", applicableChannels );
			jTemplate.addProperty( "category", line[ 5 ] );
			jTemplate.addProperty( "businessCode", line[ 6 ] );
			jTemplate.addProperty( "offerPrice", Double.parseDouble( line[ 7 ] ) );
			jTemplate.addProperty( "offerRevenue", Double.parseDouble( line[ 8 ] ) );
			jTemplate.addProperty( "deliveryReportRequired", line[ 9 ].equals( "Y" ) ? true : false );
			jTemplate.addProperty( "offerStrategy", line[ 11 ] );

			// effective date - expiry date
			SimpleDateFormat format = new SimpleDateFormat( "dd/MM/yy" );
			jTemplate.addProperty( "effectiveDate", format.parse( line[ 2 ] ).getTime() + TimeZone.getDefault().getRawOffset() );
			jTemplate.addProperty( "expiryDate", format.parse( line[ 3 ] ).getTime() + TimeZone.getDefault().getRawOffset() );

			boolean percentBonus = false;

			if( line[ 13 ] != null && !line[ 13 ].trim().equals( "NA" ) ) {
				percentBonus = true;
				double percent = Double.parseDouble( line[ 13 ] ) / 100;
				String maxCap = line[ 14 ];
				jTemplate.addProperty( "bonusPercentage", percent * 100 );
				JsonObject bonusValue = new JsonObject();
				bonusValue.addProperty( "type", "double" );
				bonusValue.addProperty( "value", "double bonus = TRANSACTION_VALUE * " + percent + ";\nif( bonus >= " + maxCap + " )\n    return " + maxCap + " ;\nreturn bonus;" );
				bonusValue.addProperty( "key", "BONUS_VALUE" );
				jTemplate.get( "offerThresholdParameters" ).getAsJsonArray().add( bonusValue );
			}
			if( line[ 23 ] != null && line[ 23 ].trim().length() > 0 && !percentBonus )
				jTemplate.addProperty( "bonusValue", Double.parseDouble( line[ 23 ] ) );

			jTemplate.addProperty( "bonusType", line[ 12 ] );
			jTemplate.addProperty( "bonusValidityDays", Integer.parseInt( line[ 24 ] ) );

			JsonArray arrOfferChannels = jTemplate.get( "offerChannels" ).getAsJsonArray();
			String[ ] notificationChannels = line[ 28 ].split( "," );
			for( int i = 0; i < notificationChannels.length; i++ ) {
				JsonObject aLocationMessage = new JsonObject();
				arrOfferChannels.add( aLocationMessage );
				aLocationMessage.addProperty( "channelName", notificationChannels[ i ] );
				aLocationMessage.addProperty( "shortCode", "" );

				JsonArray langMessages = new JsonArray();
				aLocationMessage.add( "messages", langMessages );

				JsonObject enMsg = new JsonObject();
				enMsg.addProperty( "contentId", line[ 15 ] );
				enMsg.addProperty( "text", "\"" + line[ 17 ] + "\"" );
				enMsg.addProperty( "lang", "en_US" );
				langMessages.add( enMsg );

				JsonObject thMsg = new JsonObject();
				thMsg.addProperty( "contentId", line[ 15 ] );
				thMsg.addProperty( "text", "\"" + line[ 16 ] + "\"" );
				thMsg.addProperty( "lang", "th_TH" );
				langMessages.add( thMsg );

				JsonObject burMsg = new JsonObject();
				burMsg.addProperty( "contentId", line[ 15 ] );
				burMsg.addProperty( "text", "\"" + line[ 18 ] + "\"" );
				burMsg.addProperty( "lang", "br_BR" );
				langMessages.add( burMsg );
			}

			JsonArray fulfillmentProducts = jTemplate.get( "fulfillmentList" ).getAsJsonArray().get( 0 ).getAsJsonObject().get( "fulfillmentProducts" ).getAsJsonArray();
			JsonObject aProduct = fulfillmentProducts.get( 0 ).getAsJsonObject();
			aProduct.addProperty( "productId", line[ 22 ] );

			String targetSystem = line[ 25 ];
			String provisionType = line[ 27 ];

			if( targetSystem.equals( "CBS" ) && ( provisionType.equals( "OneTimeBonus" ) || provisionType.equals( "OneTimePackage" ) ) )
				targetSystem = "CBSDCC";
			else
				targetSystem = "CBSWS";
			aProduct.addProperty( "targetSystem", targetSystem );

			// Account_Balance_Change
			JsonObject dynamicProd = aProduct.get( "dynamicParameters" ).getAsJsonArray().get( 0 ).getAsJsonObject();
			dynamicProd.addProperty( "rewardValidityDays", Integer.parseInt( line[ 24 ] ) );
			dynamicProd.addProperty( "value", line[ 23 ] );

			// validity
			dynamicProd = aProduct.get( "dynamicParameters" ).getAsJsonArray().get( 1 ).getAsJsonObject();
			dynamicProd.addProperty( "value", "\"" + line[ 24 ] + "\"" );
			// bonusTye
			dynamicProd = aProduct.get( "dynamicParameters" ).getAsJsonArray().get( 2 ).getAsJsonObject();
			dynamicProd.addProperty( "value", "\"" + line[ 12 ] + "\"" );
			// serviceReason
			dynamicProd = aProduct.get( "dynamicParameters" ).getAsJsonArray().get( 3 ).getAsJsonObject();
			dynamicProd.addProperty( "value", "\"" + line[ 6 ] + "\"" );
			// APIName
			dynamicProd = aProduct.get( "dynamicParameters" ).getAsJsonArray().get( 4 ).getAsJsonObject();
			String apiName = "1";
			if( provisionType.equals( "OneTimeBonus" ) )
				apiName = "0";
			dynamicProd.addProperty( "value", "\"" + apiName + "\"" );

			JsonArray arrFulfillChannels = jTemplate.get( "fulfillmentList" ).getAsJsonArray().get( 0 ).getAsJsonObject().get( "fulfillmentMessageChannels" ).getAsJsonArray();
			String[ ] fulfillmentNotificationChannels = line[ 29 ].split( "," );
			for( int i = 0; i < fulfillmentNotificationChannels.length; i++ ) {
				if( fulfillmentNotificationChannels[ i ].equals( "SMS" ) ) {
					JsonObject aLocationMessage = new JsonObject();
					arrFulfillChannels.add( aLocationMessage );
					aLocationMessage.addProperty( "channelName", fulfillmentNotificationChannels[ i ] );
					aLocationMessage.addProperty( "shortCode", "" );

					JsonArray langMessages = new JsonArray();
					aLocationMessage.add( "messages", langMessages );

					JsonObject enMsg = new JsonObject();
					enMsg.addProperty( "contentId", line[ 15 ] );
					if( percentBonus )
						enMsg.addProperty( "text", "\"" + line[ 19 ].replace( "xxx", "\" + (int)BONUS_VALUE + \"" ) + "\"" );
					else
						enMsg.addProperty( "text", "\"" + line[ 19 ] + "\"" );
					enMsg.addProperty( "lang", "en_US" );
					langMessages.add( enMsg );

					JsonObject thMsg = new JsonObject();
					thMsg.addProperty( "contentId", line[ 15 ] );
					if( percentBonus )
						thMsg.addProperty( "text", "\"" + line[ 20 ].replace( "xxx", "\" + (int)BONUS_VALUE + \"" ) + "\"" );
					else
						thMsg.addProperty( "text", "\"" + line[ 20 ] + "\"" );
					thMsg.addProperty( "lang", "th_TH" );
					langMessages.add( thMsg );

					JsonObject burMsg = new JsonObject();
					burMsg.addProperty( "contentId", line[ 15 ] );
					if( percentBonus )
						burMsg.addProperty( "text", "\"" + line[ 21 ].replace( "xxx", "\" + (int)BONUS_VALUE + \"" ) + "\"" );
					else
						burMsg.addProperty( "text", "\"" + line[ 21 ] + "\"" );
					burMsg.addProperty( "lang", "br_BR" );
					langMessages.add( burMsg );
				}
			}

			String fulcriteria = "RECORD_TYPE.equals( \"CREDIT\" ) && TRANSACTION_VALUE >= " + line[ 26 ];
			jTemplate.get( "fulfillmentList" ).getAsJsonArray().get( 0 ).getAsJsonObject().addProperty( "fulfillmentCriteria", fulcriteria );

			String monDuration = "";
			String[ ] monDurArr = line[ 10 ].split( "," );
			for( int i = 0; i < monDurArr.length; i++ ) {
				String[ ] thisLocCond = monDurArr[ i ].split( ":" );
				if( thisLocCond[ 0 ].equals( "Else" ) || thisLocCond[ 0 ].equals( "ALL" )  )
					monDuration += "return " + thisLocCond[ 1 ] + ";";
				else
					// monDuration += "if( REQUESTER_LOCATION.contains( \"" +
					// thisLocCond[ 0 ] + "\" ) ) return " + thisLocCond[ 1 ] +
					// ";";
					monDuration += "if( RECORD_TYPE.equals( \"" + thisLocCond[ 0 ] + "\" ) ) return " + thisLocCond[ 1 ] + ";";
			}

			jTemplate.get( "fulfillmentList" ).getAsJsonArray().get( 0 ).getAsJsonObject().get( "monitoringDuration" ).getAsJsonObject().addProperty( "expression", monDuration );

			jTemplate.addProperty( "participationType", "if( RECORD_TYPE.equals( \"GET_OFFER\" ) ) return \"MANUAL\"; else return \"AUTO\";" );
			// jTemplate.addProperty( "isCountAsContact" ,"if(
			// REQUESTER_LOCATION.equals( \"SMS\" ) ) return \"Y\"; else return
			// \"N\";" );
			jTemplate.addProperty( "isCountAsContact", "if( RECORD_TYPE.equals( \"GET_OFFER\" ) ) return \"N\"; else return \"Y\";" );

			jTemplate.addProperty( "fulfillmentsType", "\"AUTO\"" );

			jTemplate.addProperty( "RESPONSE_CHANNEL", line[ 29 ] );
			jTemplate.addProperty( "RESPONSE_APPLICATION", line[ 30 ] );
			jTemplate.addProperty( "RESPONSE_ZONE", line[ 31 ] );

			jTemplate.addProperty( "offerMeasurementGroup", line[ 32 ] );
			jTemplate.addProperty( "OfferReportingCategory", line[ 33 ] );
			jTemplate.addProperty( "offerAccountType", line[ 34 ] );
			
			
			JsonArray programsArray = new JsonArray();
			programsArray.add( new JsonPrimitive( "DTAC_WINBACK_SILENT" ) );
			programsArray.add( new JsonPrimitive( "DTAC_REFILL" ) );
			jTemplate.add( "programs", programsArray );
			
			JsonArray repParams = jTemplate.get( "offerReportParameters" ).getAsJsonArray();
			JsonObject segmentName = new JsonObject();
			segmentName.addProperty( "type", "String" );
			segmentName.addProperty( "key", "SEGMENT_NAME" );
			segmentName.addProperty( "value", "if( PROGRAM_ID.equals( \"DTAC_WINBACK_SILENT\" ) ) return DTAC_QOS_REPORT_SEGMENT_NAME_LifeTime; if( PROGRAM_ID.equals( \"DTAC_REFILL\" ) ) return DTAC_ROCKET_REFILL_REPORT_SEGMENT_LifeTime;if( PROGRAM_ID.equals( \"DTAC_NBO_PACKAGE\" ) ) return getStringOutputFromNBOModel( \"segment_name\" ); return \"\";" );
			repParams.add( segmentName );

			JsonObject bonusValue = new JsonObject();
			bonusValue.addProperty( "type", "double" );
			bonusValue.addProperty( "key", "BONUS_VALUE" );
			if( percentBonus )
				bonusValue.addProperty( "value", "BONUS_VALUE" );
			else
				bonusValue.addProperty( "value", Double.parseDouble( line[ 23 ] ) );
			repParams.add( bonusValue );
			
			JsonObject maxFullLimet = new JsonObject();
			if( ! percentBonus )
				maxFullLimet.addProperty( "expression", "getCurrentFulfilmentCount() < 3" );
			else
				maxFullLimet.addProperty( "expression", "getCurrentFulfilmentCount() < 1" );
			maxFullLimet.addProperty( "type", "boolean" );
			jTemplate.add( "maxFulfillmentsPerSubscriber", maxFullLimet );


			pandaCacheConfig.set( offerId, 0, jTemplate.toString() );
			nCount++;
			System.out.println( nCount + " = " + jTemplate.toString() );
		}
		System.out.println( nCount + " Offers Imported" );
		System.exit( 0 );
	}
}
