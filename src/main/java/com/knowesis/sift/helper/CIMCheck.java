package com.knowesis.sift.helper;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;

import au.com.bytecode.opencsv.CSVReader;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCacheConfig;

public class CIMCheck {

	/**
	 * @param args
	 * @throws URISyntaxException 
	 * @throws IOException 
	 */
	public static void main( String[ ] args ) throws URISyntaxException, IOException {
		if( args.length < 2 ) {
			System.out.println( "Usage : java -jar CIMCheck.jar persist-address fileName" );
			return;
		}

		CIMCheck dep = new CIMCheck();

		String fileName = args[ 1 ];
		String strPersistAddressList = args[ 0 ];
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );

		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddress.length; i++ ) {
			Commons.logger.info( arrPersistAddress[ i ] );
			uris.add( new URI( arrPersistAddress[ i ] ) );
		}
		PandaCacheConfig pandaCacheConfig = new PandaCacheConfig( uris );
		
		JsonParser parser = new JsonParser();
		
		CSVReader reader = new CSVReader( new FileReader( fileName ) );
		String[] fields = null;
		while( ( fields = reader.readNext() ) != null ) {
		    String cimId = fields[ 0 ];
		    String currentAlias = fields[ 2 ];
		    
		    String thisCIMDef = ( String ) pandaCacheConfig.get( cimId );
		    JsonObject objCIM = ( JsonObject ) parser.parse( thisCIMDef );
//	    	System.out.println( "WORKING ON : " + cimId );
		    if( currentAlias.trim().length() > 0 && ! ( objCIM.has( "alias" ) && objCIM.get( "alias").getAsString().equals( currentAlias ) ) )
		    	System.out.println( "Mismatch in Alias found : " + cimId + "," + currentAlias  );
		}
		System.out.println( "Checked All");
		System.exit( 0 );
	}
}
