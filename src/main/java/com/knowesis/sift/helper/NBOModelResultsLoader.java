package com.knowesis.sift.helper;

import java.io.FileReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCache;

import au.com.bytecode.opencsv.CSVReader;

public class NBOModelResultsLoader {
	
	static PandaCache pandaCache = null;
	static JsonParser parser = new JsonParser();

	public static void main(String[] args) throws Exception {
		if( args.length < 2 ) {
			System.err.println( "USAGE: java -jar LoadRocketNBO.jar persistAddress inputFile" );
			System.exit( 0 );
		}

		String inputFile = args[ 1 ];
		String strPersistAddressList = args[ 0 ];
		
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );
		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddress.length; i++ ) {
			Commons.logger.info( arrPersistAddress[ i ] );
			uris.add( new URI( arrPersistAddress[ i ] ) );
		}
		pandaCache = new PandaCache( uris );
		
		NBOModelResultsLoader loadData = new NBOModelResultsLoader();
		loadData.updateIndicator( inputFile );
		
		System.out.println( "Processed all records. Press control-C to exit");
	}

	public void updateIndicator( String inputFile ) throws Exception {
		@SuppressWarnings("resource")
		CSVReader reader = new CSVReader( new FileReader( inputFile ) );
		String[] thisRecord = null;
		List< String > indicatorFieldNames = new ArrayList< String >();
		List< String >  fieldTypes = new ArrayList< String >();
		int recordNumber = 0;
		String msisdn = null;
		
		while( ( thisRecord = reader.readNext() ) != null ) {
			if( recordNumber == 0 ) {
				recordNumber ++;
				continue;
			}
			else if( recordNumber == 1 ) {
				recordNumber ++;
				for( int i = 0; i < thisRecord.length; i ++ )
					indicatorFieldNames.add( thisRecord[ i ] );
				
				continue;
			}
			else if( recordNumber == 2 ) {
				recordNumber ++;
				for( int i = 0; i < thisRecord.length; i ++ )
					fieldTypes.add( thisRecord[ i ] );
				
				continue;
			}
			
			msisdn = thisRecord[ 4 ];
			System.out.println( "Processing msisdn: " + msisdn );
			
			pandaCache.delete( msisdn + "-Indicators" );
			pandaCache.delete( "EventTagList-" + msisdn );
			
			Object indObject = pandaCache.get( msisdn + "-Indicators" );
			JsonObject thisIndicator = null;
			if( indObject != null )
				thisIndicator = ( JsonObject ) parser.parse( indObject.toString() );
			else {
				thisIndicator = new JsonObject();
				thisIndicator.addProperty( "docType", "subscriberIndicators");
			}
			
			JsonObject nboInd = new JsonObject();
			for( int i = 4; i < indicatorFieldNames.size(); i ++ ) {
				String indFieldName = indicatorFieldNames.get( i ).trim();
				if( indFieldName.length() == 0 )
					continue;
				
				if( fieldTypes.get( i ).equalsIgnoreCase( "double" ) ) {
					String value = thisRecord[ i ].trim();
					if( value.equals( "" ) )
						nboInd.addProperty( indFieldName, 0 );
					else
						nboInd.addProperty( indFieldName, Double.parseDouble( thisRecord[ i ] ) );
				}
				else
					nboInd.addProperty( indFieldName, thisRecord[ i ] );
			}
			JsonObject bal = new JsonObject();
			bal.addProperty( "timestamp", 1500467400000l );
			bal.addProperty( "balance", 100 );
			thisIndicator.add( "LATEST_BALANCE_OBJECT_LifeTime-L", bal );
			
			thisIndicator.add( "DTAC_NBO_MODEL_LifeTime-L", nboInd );
			pandaCache.set( msisdn + "-Indicators", 0, thisIndicator.toString() );
		}
	}
}
