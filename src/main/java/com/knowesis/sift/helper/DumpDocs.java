package com.knowesis.sift.helper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.google.gson.JsonParser;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCache;
import com.knowesis.sift.Common.PandaCacheConfig;

public class DumpDocs {

	/**
	 * @param args
	 * @throws URISyntaxException 
	 * @throws IOException 
	 */
	public static void main( String[ ] args ) throws URISyntaxException, IOException {
		if( args.length < 3 ) {
			System.err.println( "Usage : java -jar DumpDocs.jar http://<persistAddress>:8091 inFilePath outFilePath" );
			System.exit( 0 );
		}
		
		CSVReader reader = new CSVReader( new FileReader( args[ 1 ] ) );
		String outFile = args[ 2 ];
		String[] inputline = null;

		String strPersistAddressList = args[ 0 ];
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );
		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddress.length; i++ ) {
			Commons.logger.info( arrPersistAddress[ i ] );
			uris.add( new URI( arrPersistAddress[ i ] ) );
		}
		PandaCacheConfig pandaCacheConfig = new PandaCacheConfig( uris );

		FileOutputStream fos = new FileOutputStream( new File( outFile ) );
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));		
		
		int importCount = 0;
		int missingCount = 0;
		while( ( inputline = reader.readNext() ) != null ) {	
			Object objdoc = pandaCacheConfig.get( inputline[ 0 ].replaceAll( "-L", "" )  );
			if( objdoc == null ) {
				System.out.println( "Missing : " + inputline[ 0 ].replaceAll( "-L", "" ) );
				missingCount++;
				continue;
			}
			String outputLine = inputline[ 0 ] + "~siftdelimiter~" + ( String )objdoc;
			bw.write( outputLine );
			bw.newLine();
			importCount++;
		}
		bw.close();
		reader.close();
		System.out.println( "Imported Document Count : " + importCount );
		System.out.println( "Missing Document Count : " + missingCount );
		System.exit( 0 );
	}	
}
