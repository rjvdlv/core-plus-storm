package com.knowesis.sift.helper;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import au.com.bytecode.opencsv.CSVReader;

public class PayLoadExtract {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main( String[ ] args ) throws IOException {
		
		CSVReader reader = new CSVReader( new FileReader( "/home/streamsadmin/Downloads/eventconf.csv" ) );
		String[] fields = null;
		while( ( fields = reader.readNext() ) != null ) {
			String eveId = fields[ 0 ];
			String payload = fields[ 20 ];
			System.out.println( eveId + "|" + payload );
		}
		
	}

}
