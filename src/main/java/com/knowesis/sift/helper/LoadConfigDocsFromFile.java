package com.knowesis.sift.helper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.google.gson.JsonParser;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCache;
import com.knowesis.sift.Common.PandaCacheConfig;

public class LoadConfigDocsFromFile {

	/**
	 * @param args
	 * @throws URISyntaxException 
	 * @throws IOException 
	 */
	public static void main( String[ ] args ) throws URISyntaxException, IOException {
		if( args.length < 2 ) {
			System.err.println( "Usage : java -jar LoadConfigDocs.jar http://<persistAddress>:8091 csvFilePath" );
			System.exit( 0 );
		}
		String strPersistAddressList = args[ 0 ];
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );
		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddress.length; i++ ) {
			Commons.logger.info( arrPersistAddress[ i ] );
			uris.add( new URI( arrPersistAddress[ i ] ) );
		}
		PandaCache pandaCacheConfig = new PandaCache( uris );

		int importCount = 0;
        File f = new File( args[ 1 ] );

        BufferedReader b = new BufferedReader(new FileReader( f ) );
        String readLine = "";
        while ( ( readLine = b.readLine() ) != null ) {
        	String[] content = readLine.split( "~siftdelimiter~" );
        	System.out.println( content[ 0 ].replaceAll("-L", "") );
        	System.out.println( content[ 1 ] );
        	pandaCacheConfig.delete( content[ 0 ].replaceAll("-L", "") );
        	pandaCacheConfig.set( content[ 0 ].replaceAll("-L", ""), 0, content[ 1 ] );
        	importCount++;
        }
		b.close();
		
		System.out.println( "Imported Document Count : " + importCount );
		System.exit( 0 );
	}	
}
