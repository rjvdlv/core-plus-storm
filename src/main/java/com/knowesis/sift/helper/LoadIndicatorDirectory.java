package com.knowesis.sift.helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;

import com.google.gson.JsonParser;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCache;

public class LoadIndicatorDirectory {

	private String readFile( String fileName ) throws IOException {
		BufferedReader br = new BufferedReader( new FileReader( fileName ) );
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while( line != null ) {
				sb.append( line );
				sb.append( "\n" );
				line = br.readLine();
			}
			return sb.toString();
		}finally {
			br.close();
		}
	}
	
	public static void main( String[ ] args ) throws URISyntaxException, IOException {
		LoadIndicatorDirectory loader = new LoadIndicatorDirectory();
		if( args.length < 2 ) {
			System.err.println( "Usage : java -jar LoadFromFileNames.jar couchIP inputs" );
			System.exit( 0 );
		}
		
		JsonParser parser = new JsonParser();
		String strPersistAddressList = args[ 0 ];
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );

		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddress.length; i++ ) {
			Commons.logger.info( arrPersistAddress[ i ] );
			uris.add( new URI( arrPersistAddress[ i ] ) );
		}
		PandaCache pandaCache = new PandaCache( uris );

        File folder = new File( args[ 1 ] );
		String[] files = folder.list();
		for (String file : files) 
		{
			System.out.println(file);
			String content = loader.readFile( folder + "/" + file  );
			System.out.println(  content  );
			pandaCache.set( file + "-Indicators", 0, content );
			pandaCache.delete( "EventTagList-" + file );
		}
	}
}
