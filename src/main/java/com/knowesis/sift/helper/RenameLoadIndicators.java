package com.knowesis.sift.helper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCache;
import com.knowesis.sift.Common.PandaCacheConfig;

import au.com.bytecode.opencsv.CSVReader;

public class RenameLoadIndicators {

	private String readFile( String fileName ) throws IOException {
		BufferedReader br = new BufferedReader( new FileReader( fileName ) );
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while( line != null ) {
				sb.append( line );
				sb.append( "\n" );
				line = br.readLine();
			}
			return sb.toString();
		}finally {
			br.close();
		}
	}

	public static void main(String[] args) throws IOException, URISyntaxException {
		RenameLoadIndicators comparator = new RenameLoadIndicators();
		if( args.length < 3 ) {
			System.err.println( "Usage : java -jar RenameLoadIndicators.jar couchIPSource couchIPDest indListFile" );
			System.exit( 0 );
		}

		String strPersistAddressListSource = args[ 0 ];
		String[ ] arrPersistAddressSource = strPersistAddressListSource.split( "," );

		LinkedList< URI > urisSource = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddressSource.length; i++ ) {
			Commons.logger.info( arrPersistAddressSource[ i ] );
			urisSource.add( new URI( arrPersistAddressSource[ i ] ) );
		}

		String strPersistAddressListDest = args[ 1 ];
		String[ ] arrPersistAddressDest = strPersistAddressListDest.split( "," );

		LinkedList< URI > urisDest = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddressSource.length; i++ ) {
			Commons.logger.info( arrPersistAddressDest[ i ] );
			urisDest.add( new URI( arrPersistAddressDest[ i ] ) );
		}

		PandaCacheConfig pandaCacheSource = new PandaCacheConfig( urisSource );
		PandaCacheConfig pandaCacheDest = new PandaCacheConfig( urisDest );

		CSVReader reader = new CSVReader( new FileReader( args[ 2 ] ) );
		
		int count = 0;
		String[] indLine = null;
		while( ( indLine = reader.readNext() ) != null ) {
			String srcInd = indLine[ 0 ].replaceAll("-L", "" );
			String scInd = (String) pandaCacheSource.get( srcInd );
			if( scInd == null )
				continue;
			String newId = "PROD_" + srcInd;
			System.out.println( "Creating " + newId + " : " + scInd );
			pandaCacheDest.set(newId, 0, scInd);
		}
		System.exit(0);
	}
	
	private String convertToString( JsonElement elem ) {
		if( elem.isJsonObject() || elem.isJsonArray() )
			return elem.toString();
		return elem.getAsString();
	}
}
