package com.knowesis.sift.helper;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;

import au.com.bytecode.opencsv.CSVReader;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCacheConfig;

public class UpdIndStatus {

	/**
	 * @param args
	 * @throws URISyntaxException 
	 * @throws IOException 
	 * @throws JsonSyntaxException 
	 */
	public static void main( String[ ] args ) throws URISyntaxException, JsonSyntaxException, IOException {
		if( args.length < 2 ) {
			System.err.println( "Usage : java -jar UpdIndStatus.jar http://<persistAddress>:8091 csvFilePath" );
			System.exit( 0 );
		}
		
		CSVReader reader = new CSVReader( new FileReader( args[ 1 ] ) );
		String[] line = null;
		
		String strPersistAddressList = args[ 0 ];
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );
		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddress.length; i++ ) {
			Commons.logger.info( arrPersistAddress[ i ] );
			uris.add( new URI( arrPersistAddress[ i ] ) );
		}
		PandaCacheConfig pandaCacheConfig = new PandaCacheConfig( uris );
		JsonParser parser = new JsonParser();

		int nCount = 0;
		while( ( line = reader.readNext() ) != null ) {
			String indDef = ( String ) pandaCacheConfig.get( line[ 0 ] );
			JsonObject jInd = ( JsonObject ) parser.parse( indDef );
			jInd.addProperty( "status", "InActive" );
			pandaCacheConfig.set( line[ 0 ], 0, jInd.toString() );
			nCount++;
		}
		System.out.println( "Updated Status Count  : " + nCount );
	}

}
