package com.knowesis.sift.helper;

import java.util.Iterator;
import java.util.LinkedHashMap;

import com.google.gson.JsonObject;
import com.knowesis.sift.Common.PandaCacheConfig;
import com.knowesis.sift.api.dao.OfferDAO;

public class DeleteOffers {

	public static void main(String[] args) {
		PandaCacheConfig config = new PandaCacheConfig();
		
		OfferDAO dao = new OfferDAO();
		LinkedHashMap<String, JsonObject> allOffers = dao.read();
		Iterator<String> itr = allOffers.keySet().iterator();
		int i = 0;
		while( itr.hasNext() ) {
			String key = itr.next();
			System.out.println( i++ + " : Deleting : " + key );
			config.delete( key );
		}
		System.exit( 0 );
	}
}
