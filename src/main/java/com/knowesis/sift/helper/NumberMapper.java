package com.knowesis.sift.helper;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import au.com.bytecode.opencsv.CSVReader;

public class NumberMapper {

	private String readFile( String fileName ) throws IOException {
		BufferedReader br = new BufferedReader( new FileReader( fileName ) );
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while( line != null ) {
				sb.append( line );
				sb.append( "\n" );
				line = br.readLine();
			}
			return sb.toString();
		}finally {
			br.close();
		}
	}

	public static void main( String[ ] args ) throws IOException {
		NumberMapper mapper = new NumberMapper();
		java.util.HashMap< String, String > numberMap = new java.util.HashMap< String, String >();
		CSVReader reader = new CSVReader( new FileReader( "/home/streamsadmin/Documents/DTAC/DTAC/ReplacementMap.csv") );
		String[] line = null;
		while( ( line = reader.readNext() ) != null ) {
			numberMap.put( line[ 0 ], line[ 2 ] );
			numberMap.put( line[ 1 ], line[ 3 ] );
		}
		String content = mapper.readFile( "/home/streamsadmin/Documents/DTAC/DTAC/SIFT_GENERIC_REPORTING_DATA_20170823.txt" );
		System.out.println( "Original Content : " );
		System.out.println( content );
		Set< String > keySet = numberMap.keySet();
		Iterator< String > itr = keySet.iterator();
		while( itr.hasNext() ) {
			String key = itr.next();
			content = content.replaceAll( key, numberMap.get( key ) );
		}
		
		System.out.println( "Modified Content : " );
		System.out.println( content );
	}
}
