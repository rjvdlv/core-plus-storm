package com.knowesis.sift.helper;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.protocol.views.Query;
import com.couchbase.client.protocol.views.Stale;
import com.couchbase.client.protocol.views.View;
import com.couchbase.client.protocol.views.ViewResponse;
import com.couchbase.client.protocol.views.ViewRow;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.PandaCacheConfig;
import com.knowesis.sift.api.dao.EventDefDao;

public class CleanupEvents {

	public static void main(String[] args) {
		CleanupEvents thisObj = new CleanupEvents();
		PandaCacheConfig pandaConfig = new PandaCacheConfig();
		LinkedHashMap<String, JsonObject> allEvents = thisObj.read();
		Iterator<Entry<String, JsonObject>> itr = allEvents.entrySet().iterator();
		while( itr.hasNext() ) {
			Entry<String, JsonObject> entry = itr.next();
			if( entry.getValue().get("status").getAsString().equals( "InActive") ) {
				System.out.println( "Removing : " + entry.getKey() );
				pandaConfig.delete( entry.getKey() );
				continue;
			}	
			String key = entry.getKey();
			if( key.equals( "DTAC_INBOUND_REQUEST" ) || key.equals( "DTAC_LBAL_DETECTED" ) ||
					key.equals( "DTAC_ROCKET_OUTBOUND_TRIGGER" ) || key.equals( "DTAC_SLNT_NEEX_MASS_CNCT") ) {
				continue;
			}	else {
				System.out.println( "Removing : " + entry.getKey() );
				pandaConfig.delete( entry.getKey() );
			}
		}
	}
	
	public java.util.LinkedHashMap< String, JsonObject > read() {
		System.out.println( "Reading from **** Persist **** " );

		PandaCacheConfig cache = new PandaCacheConfig();
		View view = cache.getView( "view_allEvents" );
		CouchbaseClient client = cache.getClient();
		Query query = new Query();
		query.setIncludeDocs( false );
		query.setStale( Stale.FALSE );
		ViewResponse viewresponse = client.query( view, query );
		Iterator< ViewRow > itr = viewresponse.iterator();
		LinkedHashMap<String, JsonObject> allEvents = new java.util.LinkedHashMap< String, JsonObject >();
		JsonParser parser = new JsonParser();
		while( itr.hasNext() ) {
			ViewRow record = itr.next();
			String doc = ( String ) cache.get( record.getKey() );
			JsonObject jobj = ( JsonObject ) parser.parse( doc );
			allEvents.put( record.getId(), jobj );
		}
		return allEvents;
	}


}
