package com.knowesis.sift.helper;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.knowesis.sift.expression.SiftMath;
import au.com.bytecode.opencsv.CSVWriter;

import com.unicacorp.interact.api.NameValuePair;

public class RequestCustomerActionCommand extends CampaignCommand{

	private String offerMessage;
	private String channel;
	private String siftCriteria;
	private double offerDurationDays;
	private String siftEvent;
	private double offerDurationHours;
	private double offerReminderDays;
	private double offerReminderHours;
	
	
	public RequestCustomerActionCommand( String msisdn, NameValuePair[ ] params, String extId ) throws IOException{
		this.msisdn = msisdn;
		this.extId = extId;
		//read and initialise the data members.
		for( int i=0; i<params.length; i++ ) {
			String name = params[i].getName();
			if( name.equals( "offer_message" ) )
				offerMessage = params[i].getValueAsString();
			else if( name.equals( "outbound_channel" ) )
				channel = params[i].getValueAsString();
			else if( name.equals( "sift_criteria" ) )
				siftCriteria = params[i].getValueAsString();
			else if( name.equals( "offer_duration_day" ) )
				offerDurationDays = params[i].getValueAsNumeric();
			else if( name.equals( "offer_duration_hour" ) )
				offerDurationHours = params[i].getValueAsNumeric();
			else if( name.equals( "sift_event" ) )
				siftEvent = params[i].getValueAsString();
			else if( name.equals( "reminder_day" ) )
				offerReminderDays = params[i].getValueAsNumeric();
			else if( name.equals( "reminder_hour" ) )
				offerReminderHours = params[i].getValueAsNumeric();
		}
	}
	
	
	private void provisionEventToSift() throws IOException {
		System.out.println( "Creating Tag List for Sift : " + msisdn + " - "  + this.siftEvent );
//		rstring msisdn, rstring eventId, rstring startTime, rstring endTime, uint64 durationHours,uint64 durationDays, 
//		rstring reminderTime, uint64 reminderDurationHours,uint64 reminderDurationDays, rstring extId, rstring parameterValue
		
		//YYYYMMDDhhmmss
		SimpleDateFormat df = new SimpleDateFormat( "yyyyMMddHHmmss" );
		Date today = new java.util.Date();
		String fileName = "/home/stream/pworkspace/SiFT_Source_data/data/taglist_input/Tag-" + msisdn + "-" + siftEvent + ".csv";
		CSVWriter writer = new CSVWriter( new FileWriter( fileName, false ) );
		String[] line = new String[ 11 ];
		line[0] = msisdn;
		
		line[1] = siftEvent;
		
		//Hardcoded For Resting
		line[1] = siftEvent;
		
		line[2] = df.format( today );
		long lend = ( long ) ( today.getTime() + ( this.offerDurationDays * SiftMath.msecPerDay ) + ( this.offerDurationHours * 3600000 ) );
		line[3] = df.format( new Date( lend ) );
		
		line[4] = null;
		line[5] = null;
		
		lend = ( long ) ( today.getTime() + ( this.offerReminderDays * SiftMath.msecPerDay ) + ( this.offerReminderHours * 3600000 ) );
		line[6] = df.format( new Date( lend ) );
		//hard coded for testing.
//		lend = ( long ) ( today.getTime() + ( 3 * 60000 ) );
//		line[6] = df.format( new Date( lend ) );
		
		line[7] = null;
		line[8] = null;
		
		//Need to fill with the actual correlation id.
		line[9] = this.extId;
		line[10] = siftCriteria;
		
		writer.writeNext( line );
		writer.close();
	}


	@Override
	public void execute() throws IOException {
		provisionEventToSift();
		notifySubscriber( channel, offerMessage );
	}

}
