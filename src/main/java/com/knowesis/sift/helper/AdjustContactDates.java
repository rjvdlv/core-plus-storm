package com.knowesis.sift.helper;

import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSyntaxException;
import com.knowesis.sift.Common.PandaCache;

import au.com.bytecode.opencsv.CSVReader;

public class AdjustContactDates {

	public static final long msecPerDay = 86400000L;
	private static PandaCache cache = null;
	static JsonParser parser = new JsonParser();

	public void initialize( String[ ] strPersistAddress ) throws Exception {
		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < strPersistAddress.length; i++ ) {
			uris.add( new URI( strPersistAddress[ i ] ) );
		}
		cache = new PandaCache( uris );
	}

	public static void main(String[] args) throws Exception {
		if( args.length < 3 ) {
			System.out.println("USAGE: java -jar AdjustContactDates.jar <PersistAddress> <MsisdnFile> <NumDaysToReset>");
			System.exit( 0 );
		}
			
		AdjustContactDates adjustInst = new AdjustContactDates();
		String[ ] addrs = args[ 0 ].split( "," );
		adjustInst.initialize( addrs );
		adjustInst.changeDates( args[ 1 ], Integer.parseInt( args[ 2 ] ) );
	}

	public void changeDates( String inputFile, int numDays ) throws JsonSyntaxException, IOException {
		@SuppressWarnings("resource")
		CSVReader reader = new CSVReader( new FileReader( inputFile ) );
		String[ ] line = null;

		int nRecs = 0;
		while( ( line = reader.readNext() ) != null ) {
			String msisdn = line[ 0 ];
			System.out.println( "Working on Indicator : " + msisdn );
			Object indicatorDocument = cache.get( msisdn + "-Indicators" );
			JsonObject jIndicator = null;
			if( indicatorDocument == null || indicatorDocument.toString().length() <= 2 ) {
				System.out.println("Doc does not exist for: " + msisdn );
			}else {
				jIndicator = ( JsonObject ) parser.parse( indicatorDocument.toString() );
				jIndicator = adjustProgramHistory( jIndicator, numDays );
				jIndicator = adjustProgramCountIndicator( jIndicator, numDays );
				jIndicator = adjustOfferHistory( jIndicator, numDays);
				cache.set( msisdn + "-Indicators", 0, jIndicator.toString() );
			}
			
			Object eventDocumet = cache.get( "EventTagList-" + msisdn );
			if( eventDocumet != null && eventDocumet.toString().length() > 2 ) {
				JsonObject jEventTagList = ( JsonObject ) parser.parse( eventDocumet.toString() );
				jEventTagList = adjustEventTagList( jEventTagList, numDays);
				cache.set( "EventTagList-" + msisdn, 0, jEventTagList.toString() );
			}
			nRecs ++;
		}
		System.out.println( "Processed Number Of Subscribers: " + nRecs );
	}
	
	public JsonObject adjustEventTagList( JsonObject eventTagList, int numDays ) {
		Iterator< Entry< String, JsonElement > > iterator = eventTagList.entrySet().iterator();
		while( iterator.hasNext() ) {
			Entry< String, JsonElement > entry = iterator.next();
			String key = entry.getKey();
			if( key.equals( "docType" ) )
				continue;
			
			JsonObject thisTag = entry.getValue().getAsJsonObject();
			long monStart = thisTag.get( "MONITORING_START_TIME" ).getAsLong();
			long monEnd = thisTag.get( "MONITORING_END_TIME" ).getAsLong();
			
			thisTag.addProperty( "MONITORING_START_TIME", monStart - ( msecPerDay * numDays ) );
			thisTag.addProperty( "MONITORING_END_TIME", monEnd - ( msecPerDay * numDays ) );
		}
		return eventTagList;
	}
	
	public JsonObject adjustOfferHistory( JsonObject subscriberObject, int numDays ) {
		
		JsonElement eOfferHist = subscriberObject.get( "OFFER_HISTORY_LifeTime-L" );
		if( eOfferHist == null || eOfferHist.isJsonNull() )
			return subscriberObject;
		
		JsonObject offerHistory = eOfferHist.getAsJsonObject();
		Iterator< Entry< String, JsonElement > > iterator = offerHistory.entrySet().iterator();
		while( iterator.hasNext() ) {
			JsonObject thisHistory = iterator.next().getValue().getAsJsonObject();
			long offerStart = thisHistory.get( "OFFER_START_DATE" ).getAsLong();
			long offerEnd = thisHistory.get( "OFFER_END_DATE" ).getAsLong();
			thisHistory.addProperty( "OFFER_START_DATE", offerStart - ( msecPerDay * numDays ) );
			thisHistory.addProperty( "OFFER_END_DATE", offerEnd - ( msecPerDay * numDays ) );
		}
		
		return subscriberObject;
	}
	
	public JsonObject adjustProgramCountIndicator( JsonObject subscriberObject, int numDays ) {
		
		JsonElement eProgramsCount = subscriberObject.get( "PROGRAMS_COUNT_LifeTime-L" );
		if( eProgramsCount == null || eProgramsCount.isJsonNull() ) 
			return subscriberObject;

		JsonObject progCountLifeTime = eProgramsCount.getAsJsonObject();
		JsonArray keys = progCountLifeTime.get( "keys" ).getAsJsonArray();
		for( int i = 0; i < keys.size(); i ++ ) {
			long thisKey = keys.get( i ).getAsLong();
			String thisKeyString = Long.toString( thisKey );
			JsonObject thisKeyObject = progCountLifeTime.get( thisKeyString ).getAsJsonObject();
			long newKey = thisKey - ( msecPerDay * numDays );
			String newKeyString = Long.toString( newKey );
			progCountLifeTime.add( newKeyString, thisKeyObject );
			
			progCountLifeTime.remove( thisKeyString );
			keys.set( i, new JsonPrimitive( newKey ) );
		}
		
		JsonObject latestDatesObj = progCountLifeTime.get( "latestProgramDates" ).getAsJsonObject();
		Iterator< Entry< String, JsonElement > > iterator = latestDatesObj.entrySet().iterator();
		while( iterator.hasNext() ) {
			Entry< String, JsonElement > entry = iterator.next();
			if( entry.getKey().endsWith( "type" ) )
				continue;
			JsonElement ele  = entry.getValue();
			if( ele.isJsonArray() ) {
				JsonArray thisArray = entry.getValue().getAsJsonArray();
				for( int i = 0; i < thisArray.size(); i ++ ) {
					long thisDate = thisArray.get( i ).getAsLong();
					if( thisDate == 0 )
						continue;
					
					thisArray.set( i, new JsonPrimitive( thisDate - ( msecPerDay * numDays ) ) );
				}	
			} else if( ele.isJsonPrimitive() ) {
				long thisdate = entry.getValue().getAsLong();
				latestDatesObj.addProperty( entry.getKey(), ( thisdate - ( msecPerDay * numDays ) ) );
			}
		}
		
		return subscriberObject;
	}
	
	public JsonObject adjustProgramHistory( JsonObject subscriberObject, int numDays ) {

		JsonElement eProgramsHist = subscriberObject.get( "PROGRAMS_LifeTime-L" );
		if( eProgramsHist == null || eProgramsHist.isJsonNull() ) 
			return subscriberObject;

		JsonObject progHist = eProgramsHist.getAsJsonObject();
		Iterator< Entry< String, JsonElement > > programsIterator = progHist.entrySet().iterator();
		while( programsIterator.hasNext() ) {
			JsonArray thisProgram = programsIterator.next().getValue().getAsJsonArray();
			JsonObject thisProgramObj = thisProgram.get( thisProgram.size() - 1 ).getAsJsonObject();
			Iterator< Entry< String, JsonElement > > offersIterator = thisProgramObj.entrySet().iterator();
			while( offersIterator.hasNext() ) {
				JsonArray thisOfferArray = offersIterator.next().getValue().getAsJsonArray();
				for( int i = 0; i < thisOfferArray.size(); i ++ ) {
					JsonObject thisOfferObject = thisOfferArray.get( i ).getAsJsonObject();
					if( thisOfferObject.has( "monitoringAction" ) ) {
						long montoringAction = thisOfferObject.get( "monitoringAction" ).getAsLong();
						montoringAction -= ( msecPerDay * numDays );
						thisOfferObject.addProperty( "monitoringAction", montoringAction );
					}
				}
			}

		}
		return subscriberObject;
	}
}
