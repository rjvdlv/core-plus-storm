package com.knowesis.sift.helper;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.TimeZone;

import com.google.gson.JsonObject;
import com.knowesis.sift.expression.SiftMath;

public class Rand {

	public static void main(String[] args ) {
//		for (int i = 0; i < 100; i++) {
//			System.out.println( Math.round(Math.random() * 100000));
//		}    
//		long gmttime = SiftMath.getCurrentTime();
//		long localtime = gmttime - java.util.TimeZone.getDefault().getOffset( gmttime );
//
////		SimpleDateFormat formatter = new SimpleDateFormat( "YYYY-MM-dd HH:mm:ss:SSS" );
//		System.out.println( gmttime );
//		System.out.println( localtime );
//		
		
		long curTimeSiftMath = SiftMath.getCurrentTime();
//		long curTime = new java.util.Date().getTime();
				
		System.out.println(  curTimeSiftMath  );
//		System.out.println(  curTime  );
		SimpleDateFormat dtFormat = new SimpleDateFormat( "YYYYMMddHHmmss" );
		dtFormat.setTimeZone( TimeZone.getTimeZone("GMT") );
		System.out.println( dtFormat.format( curTimeSiftMath ) );
		
		
	}

	public JsonObject swon( JsonObject DTAC_AGE_ON_NETWORK_OBJECT_LifeTime, long RECORD_DAYSTAMP, String DTAC_SWON_DATE_LifeTime ) {
		if( DTAC_SWON_DATE_LifeTime.trim().length() == 0 )
			return new JsonObject();
		if( DTAC_AGE_ON_NETWORK_OBJECT_LifeTime != null && DTAC_AGE_ON_NETWORK_OBJECT_LifeTime.has( "timestamp" ) ) {
			long lastupdateTimestamp = DTAC_AGE_ON_NETWORK_OBJECT_LifeTime.get( "timestamp" ).getAsLong();
			if( lastupdateTimestamp < RECORD_DAYSTAMP ) {
				long AON = 0;
				try{
					long currentTime= System.currentTimeMillis();
					java.text.SimpleDateFormat myFormat = new  java.text.SimpleDateFormat("yyyy-MM-dd");
					long swonDate=myFormat.parse(DTAC_SWON_DATE_LifeTime.substring(0,10)).getTime();
					AON =  java.util.concurrent.TimeUnit.DAYS.convert((currentTime-swonDate), java.util.concurrent.TimeUnit.MILLISECONDS)+1;
				}
				catch(java.text.ParseException e){
					e.printStackTrace();
				}
				DTAC_AGE_ON_NETWORK_OBJECT_LifeTime.addProperty( "timestamp", RECORD_DAYSTAMP );
				DTAC_AGE_ON_NETWORK_OBJECT_LifeTime.addProperty( "AON", AON );
			}
		} else {
			JsonObject accTypeObj = new JsonObject();
			long AON = 0;
			try {
				long currentTime= System.currentTimeMillis();
				java.text.SimpleDateFormat myFormat = new  java.text.SimpleDateFormat("yyyy-MM-dd");
				long swonDate=myFormat.parse(DTAC_SWON_DATE_LifeTime.substring(0,10)).getTime();
				AON =  java.util.concurrent.TimeUnit.DAYS.convert((currentTime-swonDate), java.util.concurrent.TimeUnit.MILLISECONDS)+1;
			}
			catch(java.text.ParseException e){
				e.printStackTrace();
			}
			accTypeObj.addProperty( "timestamp", RECORD_DAYSTAMP );
			accTypeObj.addProperty( "AON", AON );

			return accTypeObj;
		}
		return DTAC_AGE_ON_NETWORK_OBJECT_LifeTime;	
	}

	public JsonObject age( JsonObject DTAC_AGE_ON_NETWORK_OBJECT_LifeTime, long RECORD_DAYSTAMP, String DTAC_SWON_DATE_LifeTime ) {
		if( DTAC_AGE_ON_NETWORK_OBJECT_LifeTime != null && DTAC_AGE_ON_NETWORK_OBJECT_LifeTime.has( "timestamp" ) ) {
			long lastupdateTimestamp = DTAC_AGE_ON_NETWORK_OBJECT_LifeTime.get( "timestamp" ).getAsLong();
			if( lastupdateTimestamp < RECORD_DAYSTAMP ) {
				long AON = 0;
				try{
					long currentTime= System.currentTimeMillis();
					java.text.SimpleDateFormat myFormat = new  java.text.SimpleDateFormat("yyyy-MM-dd");
					long swonDate=myFormat.parse(DTAC_SWON_DATE_LifeTime.substring(0,10)).getTime();
					AON =  java.util.concurrent.TimeUnit.DAYS.convert((currentTime-swonDate), java.util.concurrent.TimeUnit.MILLISECONDS)+1;
				}
				catch(java.text.ParseException e){
					e.printStackTrace();
				}
				DTAC_AGE_ON_NETWORK_OBJECT_LifeTime.addProperty( "timestamp", RECORD_DAYSTAMP );
				DTAC_AGE_ON_NETWORK_OBJECT_LifeTime.addProperty( "AON", AON );
			}
		} else {
			JsonObject accTypeObj = new JsonObject();
			long AON = 0;
			try {
				long currentTime= System.currentTimeMillis();
				java.text.SimpleDateFormat myFormat = new  java.text.SimpleDateFormat("yyyy-MM-dd");
				if( DTAC_SWON_DATE_LifeTime.trim().length() == 0 )
					AON = 0;
				else {
					long swonDate=myFormat.parse(DTAC_SWON_DATE_LifeTime.substring(0,10)).getTime();
					AON =  java.util.concurrent.TimeUnit.DAYS.convert((currentTime-swonDate), java.util.concurrent.TimeUnit.MILLISECONDS)+1;
				}
			}
			catch(java.text.ParseException e){
				e.printStackTrace();
			}
			accTypeObj.addProperty( "timestamp", RECORD_DAYSTAMP );
			accTypeObj.addProperty( "AON", AON );

			return accTypeObj;
		}
		return DTAC_AGE_ON_NETWORK_OBJECT_LifeTime;
	}
	
	
}
