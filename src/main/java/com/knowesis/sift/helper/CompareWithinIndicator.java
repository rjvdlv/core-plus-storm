package com.knowesis.sift.helper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCache;

import au.com.bytecode.opencsv.CSVReader;

public class CompareWithinIndicator {

	private String readFile( String fileName ) throws IOException {
		BufferedReader br = new BufferedReader( new FileReader( fileName ) );
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while( line != null ) {
				sb.append( line );
				sb.append( "\n" );
				line = br.readLine();
			}
			return sb.toString();
		}finally {
			br.close();
		}
	}

	public static void main(String[] args) throws IOException, URISyntaxException {
		CompareWithinIndicator comparator = new CompareWithinIndicator();
		if( args.length < 3 ) {
			System.err.println( "Usage : java -jar CompareIndicators.jar couchIP msisdn indMatchFile" );
			System.exit( 0 );
		}
		JsonParser parser = new JsonParser();
		String strPersistAddressList = args[ 0 ];
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );

		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddress.length; i++ ) {
			Commons.logger.info( arrPersistAddress[ i ] );
			uris.add( new URI( arrPersistAddress[ i ] ) );
		}
		PandaCache pandaCache = new PandaCache( uris );

		String[] allSubs = args[ 1 ].split( "," );
		for( int i=0; i<allSubs.length; i++ ) {
			JsonObject subInd = (JsonObject) parser.parse( ( String ) pandaCache.get( allSubs[ i ] ) );
			System.out.println( "-----------------------------Analysing Results for MSISDN : " + allSubs[ i ] + "-----------------------------" );
			CSVReader reader = new CSVReader( new FileReader( args[ 2 ] ) );
			int count = 0;
			String[] indLine = null;
			while( ( indLine = reader.readNext() ) != null ) {
				String ind1Name = indLine[ 0 ].trim();
				String ind2Name = indLine[ 1 ].trim();
				String indVal1 = comparator.convertToString( subInd.get( ind1Name ) );
				String indVal2 = null;
				if( ind2Name.contains( "~") ) {
					String[] parts = ind2Name.split( "~" );
	//				System.out.println( parts[ 0 ] + " - " + parts[ 1 ] );
					indVal2 = comparator.convertToString( subInd.get( parts[ 0 ] ).getAsJsonObject().get( parts[ 1 ]  ) );
				} else
				    indVal2 = comparator.convertToString( subInd.get( indLine[ 1 ].trim() ) );
	//			System.out.println( "COMPARING : " + ind1Name + " : " + ind2Name + " : " + indVal1 + " - " + indVal2 );
				if( ! indVal1.equals( indVal2 ) )
					System.out.println( "******" + count ++ +"  " + ind1Name + " MISMATCH FOUND : " + indVal1 + " != " + indVal2 );
				else
					System.out.println( count ++ +"  " + ind1Name + " MATCHED : " + indVal1 + " == " + indVal2 );
			}
			System.out.println( "-----------------------------------------------------------------------------------------------------------\n\n" );
		}
		System.exit(0);
	}
	
	private String convertToString( JsonElement elem ) {
		if( elem == null )
			return "";
		if( elem.isJsonObject() || elem.isJsonArray() )
			return elem.toString();
		return elem.getAsString();
	}
}
