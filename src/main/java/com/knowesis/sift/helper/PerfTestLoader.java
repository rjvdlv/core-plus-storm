package com.knowesis.sift.helper;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCache;

import au.com.bytecode.opencsv.CSVReader;

public class PerfTestLoader {

	private String readFile( String fileName ) throws IOException {
		BufferedReader br = new BufferedReader( new FileReader( fileName ) );
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while( line != null ) {
				sb.append( line );
				sb.append( "\n" );
				line = br.readLine();
			}
			return sb.toString();
		}finally {
			br.close();
		}
	}

	public static void main( String[ ] args ) throws URISyntaxException, IOException {
		PerfTestLoader loader = new PerfTestLoader();
		if( args.length < 3 ) {
			System.err.println( "Usage : java -jar PerfTestLoader.jar couchIP templates actualFiles" );
			System.exit( 0 );
		}

		JsonParser parser = new JsonParser();
		String strPersistAddressList = args[ 0 ];
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );

		java.util.HashMap< Integer, JsonObject > templateMap = new java.util.HashMap< Integer, JsonObject >();
		
		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddress.length; i++ ) {
			Commons.logger.info( arrPersistAddress[ i ] );
			uris.add( new URI( arrPersistAddress[ i ] ) );
		}
		PandaCache pandaCache = new PandaCache( uris );
		String[] templateFiles = args[ 1 ].split( "," );
		for( int i=0; i<templateFiles.length; i++ ) {
			System.out.println( "------------------------------------Reading Template : " + templateFiles[ i ] + "-----------------------------" );
			String template = loader.readFile( templateFiles[ i ] );
			JsonObject jTemplate = ( JsonObject ) parser.parse( template  );
			templateMap.put( new Integer( i ), jTemplate );
		}
		
		String[] msisdnFiles = args[ 2 ].split( "," );
		for( int i=0; i<msisdnFiles.length; i++ ) {
			String msisdnFile = msisdnFiles[ i ];
			System.out.println( "------------------------------------Working on File : " + msisdnFile + "-----------------------------" );
			CSVReader reader = new CSVReader( new FileReader( msisdnFile ) );
			String[] record = null;
			int recCount = 0;
			while( ( record = reader.readNext() ) != null ) {
				if( recCount == 0 ) {
					recCount++;
					continue;
				}
				String msisdn = record[ 0 ];
				
				int index = ( int ) ( Long.parseLong( msisdn ) % 10 );
				JsonObject doc = templateMap.get( index );
				
				JsonObject custNum = new JsonObject();
				custNum.addProperty( "timestamp", new java.util.Date().getTime() );
				custNum.addProperty( "customerNumber", record[ 1 ] );
				doc.add( "DTAC_CUSTOMER_NUMBER_OBJECT_LifeTime-L", custNum );
				pandaCache.set( msisdn + "-Indicators", 0, doc.toString() );
				recCount++;
			}
			System.out.println( "------------------------------------Finsihed File : " + msisdnFile + ". Record Count : " + recCount + " -----------------------------" );
		}
	}
}
