package com.knowesis.sift.helper;

import java.net.URI;
import java.util.LinkedList;

import com.couchbase.client.CouchbaseClient;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.PandaCache;

public class IndUpd {

	/**
	 * @param args
	 */
	public static void main( String[ ] args ) {
		// TODO Auto-generated method stub
		JsonParser parser = new JsonParser();
		LinkedList< URI > uris = new LinkedList< URI >();
		uris.add( URI.create( args[ 0 ] ) );

		PandaCache cache = new PandaCache( uris );
		CouchbaseClient client = cache.getClient();
		
		String key = "telco-1-Indicators";
		String propKey = "TOTAL_Record_Count_Daily-229D14";
		String ind = ( String ) client.get( key );
		JsonObject jInd = ( JsonObject ) parser.parse( ind );
		jInd.addProperty( propKey, 31592275 );
		client.set( key, 0, jInd.toString() );
		
		key = "telco-2-Indicators";
		ind = ( String ) client.get( key );
		jInd = ( JsonObject ) parser.parse( ind );
		jInd.addProperty( propKey, 31462249 );
		client.set( key, 0, jInd.toString() );
		
		key = "telco-3-Indicators";
		ind = ( String ) client.get( key );
		jInd = ( JsonObject ) parser.parse( ind );
		jInd.addProperty( propKey, 31822297 );
		client.set( key, 0, jInd.toString() );
		
		key = "telco-4-Indicators";
		ind = ( String ) client.get( key );
		jInd = ( JsonObject ) parser.parse( ind );
		jInd.addProperty( propKey, 31392188 );
		client.set( key, 0, jInd.toString() );

		key = "telco-5-Indicators";
		ind = ( String ) client.get( key );
		jInd = ( JsonObject ) parser.parse( ind );
		jInd.addProperty( propKey, 31792325 );
		client.set( key, 0, jInd.toString() );

		key = "telco-6-Indicators";
		ind = ( String ) client.get( key );
		jInd = ( JsonObject ) parser.parse( ind );
		jInd.addProperty( propKey, 31492306 );
		client.set( key, 0, jInd.toString() );
	}
}
