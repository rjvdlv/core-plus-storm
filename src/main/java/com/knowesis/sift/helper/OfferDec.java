package com.knowesis.sift.helper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class OfferDec {
	
	private void cookOfferDecisionCriteria( //ExpressionProcessor expProc,
			JsonObject aNode, 
			HashMap< String, String > allExp, 
			String id ) throws Exception {
		System.out.println( id + " : " + aNode.get( "expression" ).getAsString() );
		allExp.put( id, aNode.get( "expression").getAsString() );
		if( aNode.get( "nextLevel" ) == null || aNode.get( "nextLevel" ).isJsonNull() )
			return;
		JsonArray jNextLevel = aNode.get( "nextLevel" ).getAsJsonArray();
		for( int i=0; i<jNextLevel.size(); i++ ) {
			cookOfferDecisionCriteria( jNextLevel.get(i).getAsJsonObject(), allExp, id + "_" + i );
		}	
		
//			JsonObject objToCook = new JsonObject();	
//			objToCook.addProperty( "id", id );
//			objToCook.addProperty( "expression", expression );
//			objToCook.addProperty( "expressionType", type );
//	
//			PExpressionEvaluator exp = expProc.createByDefinition( objToCook );
//			if( exp != null )
//				allExpressions.put(  id, exp );
	}
	
    String readFile( String fileName ) throws IOException {
		fileName = fileName.replace( "\"", "" );
		BufferedReader br = new BufferedReader( new FileReader( fileName ) );
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while( line != null ) {
				sb.append( line );
				sb.append( "\n" );
				line = br.readLine();
			}
			return sb.toString();
		}finally {
			br.close();
		}
	}

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		OfferDec thisDao = new OfferDec();
		String content = thisDao.readFile("/home/streamsadmin/Downloads/offerDec.json");
		JsonParser parser = new JsonParser();
		JsonObject jobj = ( JsonObject )parser.parse( content );
		HashMap< String, String > outMap = new HashMap< String, String >();
		thisDao.cookOfferDecisionCriteria( jobj, outMap, "P_0" );
		Set<String> keyset = outMap.keySet();
		Iterator<String> itr = keyset.iterator();
		
		System.out.println( "Printing vaues here ---------------- : " + outMap.size() );
		
		while( itr.hasNext() ) {
			String key = itr.next();
			System.out.println(  key + " : " + outMap.get(key) );
		}
	}

}
