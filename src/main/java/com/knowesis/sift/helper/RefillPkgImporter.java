package com.knowesis.sift.helper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.TimeZone;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCacheConfig;

import au.com.bytecode.opencsv.CSVReader;

public class RefillPkgImporter {

	private String readFile( String fileName ) throws IOException {
		BufferedReader br = new BufferedReader( new FileReader( fileName ) );
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while( line != null ) {
				sb.append( line );
				sb.append( "\n" );
				line = br.readLine();
			}
			return sb.toString();
		}finally {
			br.close();
		}
	}

	/**
	 * @param args
	 * @throws IOException
	 * @throws URISyntaxException
	 * @throws ParseException
	 */
	public static void main( String[ ] args ) throws IOException, URISyntaxException, ParseException {

		if( args.length < 3 ) {
			System.err.println( "Usage : java -jar FixBonus.jar http://<persistAddress>:8091 templatePath csvFilePath" );
			System.exit( 0 );
		}

		RefillPkgImporter pkgBonus = new RefillPkgImporter();

		CSVReader reader = new CSVReader( new FileReader( args[ 2 ] ) );
		String[ ] line = null;

		String strPersistAddressList = args[ 0 ];
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );
		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddress.length; i++ ) {
			Commons.logger.info( arrPersistAddress[ i ] );
			uris.add( new URI( arrPersistAddress[ i ] ) );
		}
		PandaCacheConfig pandaCacheConfig = new PandaCacheConfig( uris );

		int nCount = 0;
		while( ( line = reader.readNext() ) != null ) {
			String template = pkgBonus.readFile( args[ 1 ] );
			JsonParser parser = new JsonParser();
			JsonObject jTemplate = ( JsonObject ) parser.parse( template );

			String offerId = line[ 0 ];
			offerId = offerId.replaceAll( "  ", " " ).replaceAll( ": ", ":" ).replaceAll( " ", "_" ).replaceAll( "/", "_" ).replaceAll( ":", "_" ).replaceAll( "\\.", "_" ).replaceAll( "-", "_" ).replaceAll( "\\+", "_" )
					.replaceAll( "\\(", "_" ).replaceAll( "\\)", "_" ).replaceAll( "__", "_" );
			jTemplate.addProperty( "name", line[ 1 ] );
			jTemplate.addProperty( "description", offerId );

			String applicableChannels = line[ 4 ];
			jTemplate.addProperty( "applicableLocations", applicableChannels );
			jTemplate.addProperty( "category", line[ 5 ] );
			jTemplate.addProperty( "businessCode", line[ 6 ] );
			jTemplate.addProperty( "offerPrice", Double.parseDouble( line[ 7 ] ) );
			jTemplate.addProperty( "offerRevenue", Double.parseDouble( line[ 8 ] ) );
			jTemplate.addProperty( "deliveryReportRequired", line[ 9 ].equals( "Y" ) ? true : false );
			jTemplate.addProperty( "offerStrategy", line[ 11 ] );

			JsonObject maxFullLimet = new JsonObject();
			maxFullLimet.addProperty( "expression", "getCurrentFulfilmentCount() < 1" );
			maxFullLimet.addProperty( "type", "boolean" );
			jTemplate.add( "maxFulfillmentsPerSubscriber", maxFullLimet );

			// effective date - expiry date
			SimpleDateFormat format = new SimpleDateFormat( "dd/MM/yy" );
			jTemplate.addProperty( "effectiveDate", format.parse( line[ 2 ] ).getTime() + TimeZone.getDefault().getRawOffset() );
			jTemplate.addProperty( "expiryDate", format.parse( line[ 3 ] ).getTime() + TimeZone.getDefault().getRawOffset() );

			jTemplate.addProperty( "packageGroup", line[ 27 ] );
			jTemplate.addProperty( "packageSubGroup", line[ 28 ] );
			jTemplate.addProperty( "packageFee", line[ 29 ] );
			jTemplate.addProperty( "packageValidityDays", line[ 30 ] );
			jTemplate.addProperty( "packageVolumeMB", line[ 31 ] );
			jTemplate.addProperty( "packageVolumeMinute", line[ 32 ] );
			jTemplate.addProperty( "PackageSpeedKbps", line[ 33 ] );

			JsonArray arrOfferChannels = jTemplate.get( "offerChannels" ).getAsJsonArray();
			String[ ] notificationChannels = line[ 45 ].split( "," );
			for( int i = 0; i < notificationChannels.length; i++ ) {
				if( notificationChannels[ i ].equals( "SMS" ) ) {
					JsonObject aLocationMessage = new JsonObject();
					arrOfferChannels.add( aLocationMessage );
					aLocationMessage.addProperty( "channelName", notificationChannels[ i ] );
					aLocationMessage.addProperty( "shortCode", "" );

					JsonArray langMessages = new JsonArray();
					aLocationMessage.add( "messages", langMessages );

					JsonObject enMsg = new JsonObject();
					enMsg.addProperty( "contentId", line[ 15 ] );
					enMsg.addProperty( "text", "\"" + line[ 17 ] + "\"" );
					enMsg.addProperty( "lang", "en_US" );
					langMessages.add( enMsg );

					JsonObject thMsg = new JsonObject();
					thMsg.addProperty( "contentId", line[ 15 ] );
					thMsg.addProperty( "text", "\"" + line[ 16 ] + "\"" );
					thMsg.addProperty( "lang", "th_TH" );
					langMessages.add( thMsg );

					JsonObject burMsg = new JsonObject();
					burMsg.addProperty( "contentId", line[ 15 ] );
					burMsg.addProperty( "text", "\"" + line[ 18 ] + "\"" );
					burMsg.addProperty( "lang", "br_BR" );
					langMessages.add( burMsg );
				}else {
					JsonObject aLocationMessage = new JsonObject();
					arrOfferChannels.add( aLocationMessage );
					aLocationMessage.addProperty( "channelName", notificationChannels[ i ] );
					aLocationMessage.addProperty( "shortCode", "" );

					JsonArray langMessages = new JsonArray();
					aLocationMessage.add( "messages", langMessages );

					JsonObject enMsg = new JsonObject();
					enMsg.addProperty( "contentId", line[ 15 ] );
					enMsg.addProperty( "text", "\"" + line[ 17 ] + "\"" );
					enMsg.addProperty( "lang", "en_US" );
					langMessages.add( enMsg );
					
					JsonObject thMsg = new JsonObject();
					thMsg.addProperty( "contentId", line[ 15 ] );
					thMsg.addProperty( "text", "\"" + line[ 16 ] + "\"" );
					thMsg.addProperty( "lang", "th_TH" );
					langMessages.add( thMsg );
				}
			}

			// optinShort Code and OptinKeyword in fulfillment list -
			// optinShortCode, optinKeyword
			JsonObject fulfillmentObj = jTemplate.get( "fulfillmentList" ).getAsJsonArray().get( 0 ).getAsJsonObject();
			fulfillmentObj.addProperty( "optinShortCode", line[ 35 ] );
			fulfillmentObj.addProperty( "optinKeyword", "\"" + line[ 34 ] + "\"" );

			String productId = line[ 36 ];
			if( productId != null && productId.trim().length() > 0 ) {
				int rewardValidityDays = 7;
				if( line[ 41 ].trim().length() != 0 )
					rewardValidityDays = Integer.parseInt( line[ 41 ] );
				JsonArray fulfillmentProducts = fulfillmentObj.get( "fulfillmentProducts" ).getAsJsonArray();
				JsonObject aProduct = fulfillmentProducts.get( 0 ).getAsJsonObject();
				aProduct.addProperty( "productId", line[ 36 ] );

				aProduct.addProperty( "targetSystem", line[ 40 ] );

				// packCode
				JsonObject dynamicProd = aProduct.get( "dynamicParameters" ).getAsJsonArray().get( 0 ).getAsJsonObject();
				dynamicProd.addProperty( "value", "\"" + line[ 37 ] + "\"" );

				// package_group_code
				dynamicProd = aProduct.get( "dynamicParameters" ).getAsJsonArray().get( 2 ).getAsJsonObject();
				dynamicProd.addProperty( "value", "\"" + line[ 38 ] + "\"" );

				// cbs_offer_id
				dynamicProd = aProduct.get( "dynamicParameters" ).getAsJsonArray().get( 3 ).getAsJsonObject();
				dynamicProd.addProperty( "value", "\"" + line[ 39 ] + "\"" );

				// PDGroupFlag
				String pdgroupFlag = ( line[ 38 ].trim().length() == 0 ) ? "N" : "Y";
				dynamicProd = aProduct.get( "dynamicParameters" ).getAsJsonArray().get( 5 ).getAsJsonObject();
				dynamicProd.addProperty( "value", "\"" + pdgroupFlag + "\"" );

				// PDProdType
				dynamicProd = aProduct.get( "dynamicParameters" ).getAsJsonArray().get( 6 ).getAsJsonObject();
				dynamicProd.addProperty( "value", "\"" + line[ 43 ] + "\"" );
				// NextCycl
				dynamicProd = aProduct.get( "dynamicParameters" ).getAsJsonArray().get( 7 ).getAsJsonObject();
				dynamicProd.addProperty( "value", "\"" + line[ 44 ] + "\"" );
			}

			JsonArray offerReportParameters = jTemplate.get( "offerReportParameters" ).getAsJsonArray();
			offerReportParameters.get( 2 ).getAsJsonObject().addProperty( "value", "\"" + line[ 37 ] + "\"" );
			offerReportParameters.get( 3 ).getAsJsonObject().addProperty( "value", "\"" + line[ 38 ] + "\"" );
			offerReportParameters.get( 4 ).getAsJsonObject().addProperty( "value", "\"" + line[ 39 ] + "\"" );

			JsonArray arrFulfillChannels = jTemplate.get( "fulfillmentList" ).getAsJsonArray().get( 0 ).getAsJsonObject().get( "fulfillmentMessageChannels" ).getAsJsonArray();
			String[ ] fulfillmentNotificationChannels = line[ 46 ].split( "," );
			for( int i = 0; i < fulfillmentNotificationChannels.length; i++ ) {
				if( fulfillmentNotificationChannels[ i ].equals( "SMS" ) ) {
					JsonObject aLocationMessage = new JsonObject();
					arrFulfillChannels.add( aLocationMessage );
					aLocationMessage.addProperty( "channelName", fulfillmentNotificationChannels[ i ] );
					aLocationMessage.addProperty( "shortCode", "" );

					JsonArray langMessages = new JsonArray();
					aLocationMessage.add( "messages", langMessages );

					JsonObject enMsg = new JsonObject();
					enMsg.addProperty( "contentId", "" );
					enMsg.addProperty( "text", "\"" + line[ 22 ] + "\"" );
					enMsg.addProperty( "lang", "en_US" );
					langMessages.add( enMsg );

					JsonObject thMsg = new JsonObject();
					thMsg.addProperty( "contentId", "" );
					thMsg.addProperty( "text", "\"" + line[ 23 ] + "\"" );
					thMsg.addProperty( "lang", "th_TH" );
					langMessages.add( thMsg );

					JsonObject burMsg = new JsonObject();
					burMsg.addProperty( "contentId", "" );
					burMsg.addProperty( "text", "\"" + line[ 24 ] + "\"" );
					burMsg.addProperty( "lang", "br_BR" );
					langMessages.add( burMsg );
				}
			}

			String fulcriteria = "";
			if( line[ 42 ].trim().equals( "0" ) )
				fulcriteria = "RECORD_TYPE.equals( \"CREDIT\" ) && TRANSACTION_VALUE > " + line[ 42 ];
			else
				fulcriteria = "RECORD_TYPE.equals( \"CREDIT\" ) && TRANSACTION_VALUE >= " + line[ 42 ];
			jTemplate.get( "fulfillmentList" ).getAsJsonArray().get( 0 ).getAsJsonObject().addProperty( "fulfillmentCriteria", fulcriteria );

			String monDuration = "";
			String[ ] monDurArr = line[ 10 ].split( "," );
			for( int i = 0; i < monDurArr.length; i++ ) {
				String[ ] thisLocCond = monDurArr[ i ].split( ":" );
				if( thisLocCond[ 0 ].equals( "Else" ) || thisLocCond[ 0 ].equals( "ALL" )  )
					monDuration += "return " + thisLocCond[ 1 ] + ";";
				else
					// monDuration += "if( REQUESTER_LOCATION.contains( \"" +
					// thisLocCond[ 0 ] + "\" ) ) return " + thisLocCond[ 1 ] +
					// ";";
					monDuration += "if( RECORD_TYPE.equals( \"" + thisLocCond[ 0 ] + "\" ) ) return " + thisLocCond[ 1 ] + ";";
			}

			jTemplate.get( "fulfillmentList" ).getAsJsonArray().get( 0 ).getAsJsonObject().get( "monitoringDuration" ).getAsJsonObject().addProperty( "expression", monDuration );

			jTemplate.addProperty( "participationType", "if( RECORD_TYPE.equals( \"GET_OFFER\" ) ) return \"MANUAL\"; else return \"AUTO\";" );
			// jTemplate.addProperty( "isCountAsContact" ,"if(
			// REQUESTER_LOCATION.equals( \"SMS\" ) ) return \"Y\"; else return
			// \"N\";" );
			jTemplate.addProperty( "isCountAsContact", "if( RECORD_TYPE.equals( \"GET_OFFER\" ) ) return \"N\"; else return \"Y\";" );

			jTemplate.addProperty( "fulfillmentsType", "\"AUTO\"" );

			jTemplate.addProperty( "RESPONSE_CHANNEL", line[ 46 ] );
			jTemplate.addProperty( "RESPONSE_APPLICATION", line[ 47 ] );
			jTemplate.addProperty( "RESPONSE_ZONE", line[ 48 ] );
			
			jTemplate.addProperty( "offerMeasurementGroup", line[ 49 ] );
			jTemplate.addProperty( "OfferReportingCategory", line[ 50 ] );
			jTemplate.addProperty( "offerAccountType", line[ 51 ] );

			JsonArray programsArray = new JsonArray();
			programsArray.add( new JsonPrimitive( "DTAC_WINBACK_SILENT" ) );
			programsArray.add( new JsonPrimitive( "DTAC_REFILL" ) );
			jTemplate.add( "programs", programsArray );

			JsonArray repParams = jTemplate.get( "offerReportParameters" ).getAsJsonArray();
			JsonObject segmentName = new JsonObject();
			segmentName.addProperty( "type", "java.lang.String" );
			segmentName.addProperty( "key", "SEGMENT_NAME" );
			segmentName.addProperty( "value", "if( PROGRAM_ID.equals( \"DTAC_WINBACK_SILENT\" ) ) return DTAC_QOS_REPORT_SEGMENT_NAME_LifeTime; if( PROGRAM_ID.equals( \"DTAC_REFILL\" ) ) return DTAC_ROCKET_REFILL_REPORT_SEGMENT_LifeTime;if( PROGRAM_ID.equals( \"DTAC_NBO_PACKAGE\" ) ) return getStringOutputFromNBOModel( \"segment_name\" ); return \"\";" );
			repParams.add( segmentName );

			pandaCacheConfig.delete( offerId );
			pandaCacheConfig.set( offerId, 0, jTemplate.toString() );
			nCount++;
			System.out.println( nCount + " = " + jTemplate.toString() );
		}
		System.out.println( nCount + " Offers Imported" );
//		System.exit( 0 );
	}
}
