package com.knowesis.sift.helper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.LinkedList;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.protocol.views.Query;
import com.couchbase.client.protocol.views.Stale;
import com.couchbase.client.protocol.views.View;
import com.couchbase.client.protocol.views.ViewResponse;
import com.couchbase.client.protocol.views.ViewRow;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCache;
import com.knowesis.sift.Common.PandaCacheConfig;

public class DumpDocsByView {

	/**
	 * @param args
	 * @throws URISyntaxException 
	 * @throws IOException 
	 */
	public static void main( String[ ] args ) throws URISyntaxException, IOException {
		if( args.length < 2 ) {
			System.err.println( "Usage : java -jar DumpDocs.jar http://<persistAddress>:8091 viewName csvOutFilePath" );
			System.exit( 0 );
		}
		
		String outFile = args[ 2 ];

		String strPersistAddressList = args[ 0 ];
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );
		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddress.length; i++ ) {
			Commons.logger.info( arrPersistAddress[ i ] );
			uris.add( new URI( arrPersistAddress[ i ] ) );
		}
		PandaCacheConfig pandaCacheConfig = new PandaCacheConfig( uris );

		FileOutputStream fos = new FileOutputStream( new File( outFile ) );
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));		
		
		View view = pandaCacheConfig.getView( args[ 1 ] );
		CouchbaseClient client = pandaCacheConfig.getClient();
		Query query = new Query();
		query.setIncludeDocs( false );
		query.setStale( Stale.FALSE );
		ViewResponse viewresponse = client.query( view, query );
		Iterator< ViewRow > itr = viewresponse.iterator();

		
		int importCount = 0;
		int missingCount = 0;
		while( itr.hasNext() ) {
			ViewRow record = itr.next();
			String doc = ( String ) pandaCacheConfig.get( record.getKey() );
			String outputLine = record.getKey() + "~siftdelimiter~" + doc;
			bw.write( outputLine );
			bw.newLine();
			importCount++;
		}
		bw.close();
		System.out.println( "Imported Document Count : " + importCount );
		System.out.println( "Missing Document Count : " + missingCount );
		System.exit( 0 );
	}	
}
