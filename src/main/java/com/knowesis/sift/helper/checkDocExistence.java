package com.knowesis.sift.helper;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;

import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCacheConfig;

import au.com.bytecode.opencsv.CSVReader;

public class checkDocExistence {

	public static void main( String[ ] args ) throws URISyntaxException, IOException {
		if( args.length < 2 ) {
			System.err.println( "Usage : java -jar LoadConfigDocs.jar http://<SourcePersistAddress>:8091 csvFilePath" );
			System.exit( 0 );
		}
		java.util.HashMap< String, String > srcDocs = new java.util.HashMap< String, String >();
		
		String strDSourcePersistAddressList = args[ 0 ];
		String[ ] arrSourcePersistAddress = strDSourcePersistAddressList.split( "," );
		LinkedList< URI > urisSource = new LinkedList< URI >();
		for( int i = 0; i < arrSourcePersistAddress.length; i++ ) {
			Commons.logger.info( arrSourcePersistAddress[ i ] );
			urisSource.add( new URI( arrSourcePersistAddress[ i ] ) );
		}
		PandaCacheConfig sourcePandaCacheConfig = new PandaCacheConfig( urisSource );

		int existing = 0;
		CSVReader reader = new CSVReader( new FileReader( args[ 1 ] ) );
        String[] readLine = null;
        while ( ( readLine = reader.readNext() ) != null ) {
        	String docName = readLine[ 0 ];
        	Object content = sourcePandaCacheConfig.get( docName );
        	if( content != null ) {
        		System.out.println(  "Existing Document : " + docName );
        		existing++;
        	}	
        }
		reader.close();
		System.out.println( "Total Existing Documents : " + existing );
		System.exit( 0 );
	}

}
