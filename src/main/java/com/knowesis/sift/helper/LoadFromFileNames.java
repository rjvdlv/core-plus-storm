package com.knowesis.sift.helper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCache;

import au.com.bytecode.opencsv.CSVReader;

public class LoadFromFileNames {

	private String readFile( String fileName ) throws IOException {
		BufferedReader br = new BufferedReader( new FileReader( fileName ) );
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while( line != null ) {
				sb.append( line );
				sb.append( "\n" );
				line = br.readLine();
			}
			return sb.toString();
		}finally {
			br.close();
		}
	}

	
	public static void main(String[] args) throws URISyntaxException, IOException {
		LoadFromFileNames loader = new LoadFromFileNames();
		if( args.length < 2 ) {
			System.err.println( "Usage : java -jar LoadFromFileNames.jar couchIP inputs" );
			System.exit( 0 );
		}

		JsonParser parser = new JsonParser();
		String strPersistAddressList = args[ 0 ];
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );

		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddress.length; i++ ) {
			Commons.logger.info( arrPersistAddress[ i ] );
			uris.add( new URI( arrPersistAddress[ i ] ) );
		}
		PandaCache pandaCache = new PandaCache( uris );
		
		CSVReader reader = new CSVReader( new FileReader( args[ 1 ] ) );
		String[] fileName = null;
		while( ( fileName = reader.readNext() ) != null ) {
			String content = loader.readFile( fileName[ 0 ] );
			JsonObject jcontent = ( JsonObject )parser.parse( content );
			
			JsonObject act = new JsonObject();
			act.addProperty( "timestamp", 1499746122000l );
			act.addProperty( "accountType", "PREPAID");
			jcontent.add( "ACCOUNT_TYPE_OBJECT_LifeTime-L", act );
			
			JsonObject age = new JsonObject();
			age.addProperty( "timestamp", 1499746122000l );
			age.addProperty( "AON", 100 );
			jcontent.add( "DTAC_AGE_ON_NETWORK_OBJECT_LifeTime-L", age );
			
			if( jcontent.has( "DTAC_TOPUP_STRETCH_LifeTime-L") ) {
				JsonObject stretch = jcontent.get("DTAC_TOPUP_STRETCH_LifeTime-L").getAsJsonObject();
				stretch.addProperty( "TranscationDate", 20170710 );
			}
			
			String ind = fileName[ 0 ].replace( "/home/streamsadmin/Documents/DTAC/SIT/PKGSUM/DATA/", "" );
			ind = ind.replace( ".txt", "" );
			pandaCache.set(ind, 0, jcontent.toString() );
		}
		System.exit(0);
		
	}

}
