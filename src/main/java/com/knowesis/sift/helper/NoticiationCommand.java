package com.knowesis.sift.helper;

import java.io.IOException;

import com.unicacorp.interact.api.NameValuePair;

public class NoticiationCommand extends CampaignCommand{

	private String notificationMessage;
	private String channel;
	
	public NoticiationCommand( String msisdn, NameValuePair[ ] params, String extId ) throws IOException{
		this.msisdn = msisdn;
		this.extId = extId;
		//read and initialise the data members.
		for( int i=0; i<params.length; i++ ) {
			String name = params[i].getName();
			if( name.equals( "NotificationMessage" ) )
				notificationMessage = params[i].getValueAsString();
			else if( name.equals( "outbound_channel" ) )
				channel = params[i].getValueAsString();
		}
	}
	

	@Override
	public void execute() throws IOException {
		notifySubscriber( channel, notificationMessage );
	}

}
