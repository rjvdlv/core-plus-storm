package com.knowesis.sift.helper;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Set;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCache;
import com.knowesis.sift.Common.PandaCacheConfig;

public class ProgramScoreMaker {

	public static void main( String[ ] args ) throws URISyntaxException {
//		if( args.length < 2 ) {
//			System.err.println( "Usage : java -jar LoadConfigDocs.jar http://<persistAddress>:8091 csvFilePath" );
//			System.exit( 0 );
//		}
		String strPersistAddressList = "http://localhost:8091/pools";
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );
		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddress.length; i++ ) {
			Commons.logger.info( arrPersistAddress[ i ] );
			uris.add( new URI( arrPersistAddress[ i ] ) );
		}
		PandaCacheConfig pandaCacheConfig = new PandaCacheConfig( uris );
		JsonParser parser = new JsonParser();
		JsonObject jProgram = ( JsonObject ) parser.parse(  ( String )pandaCacheConfig.get( "DTAC_NBO_PACKAGE" ) );
		JsonObject scoresMap = jProgram.get( "programContactRule" ).getAsJsonObject().get( "offerExpressionMap" ).getAsJsonObject();
		Set< Entry< String, JsonElement > > eset = scoresMap.entrySet();
		Iterator< Entry< String, JsonElement > > itr = eset.iterator();
		while( itr.hasNext() ) {
			Entry< String, JsonElement > entry = itr.next();
			String key = entry.getKey();
			JsonObject score = entry.getValue().getAsJsonObject().get( "Score"  ).getAsJsonObject();
			String expression = score.get( "expression"  ).getAsString();
			expression = "getStringOfferAttribute( \"offerPackageCode\" ).equals( getStringOutputFromNBOModel( \"recent_pack_cd\" ) ) ? 1000 : " + expression;
			score.addProperty( "expression", expression );
		}
		pandaCacheConfig.set( "DTAC_NBO_PACKAGE", 0, jProgram.toString() );
	}

}
