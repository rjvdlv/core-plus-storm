package com.knowesis.sift.helper;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;

import au.com.bytecode.opencsv.CSVReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSyntaxException;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCache;
import com.knowesis.sift.Common.PandaCacheConfig;

public class TestDataCreator {

	/**
	 * @param args
	 * @throws URISyntaxException 
	 * @throws IOException 
	 * @throws JsonSyntaxException 
	 */
	public static void main( String[ ] args ) throws URISyntaxException, JsonSyntaxException, IOException {
		if( args.length < 3 ) {
			System.err.println( "Usage : java -jar TestDataCreator.jar http://<persistAddress>:8091 csvFilePath seperator(single char)" );
			System.exit( 0 );
		}

		CSVReader reader = new CSVReader( new FileReader( args[ 1 ] ), args[ 2 ].charAt( 0 ) );
		String[] line = null;

		String strPersistAddressList = args[ 0 ];
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );
		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddress.length; i++ ) {
			Commons.logger.info( arrPersistAddress[ i ] );
			uris.add( new URI( arrPersistAddress[ i ] ) );
		}
		PandaCache pandaCache = new PandaCache( uris );
		JsonParser parser = new JsonParser();

		int nCount = 0;
		JsonArray indNames = new JsonArray();
		JsonArray indTypes = new JsonArray();

		while( ( line = reader.readNext() ) != null ) {
			if ( nCount == 0 ){
				for ( int i=0;i<line.length;i++)
					indNames.add( new JsonPrimitive(line[i]) );
			}
			else if ( nCount == 1 ){
				for ( int j=0;j<line.length;j++)
					indTypes.add( new JsonPrimitive(line[j]) );
			}
			else {
				String msisdn = line[0].split( ":",-1 )[0].trim();
				//System.out.println(msisdn);
				JsonObject subsIndObj;

				pandaCache.delete(  msisdn + "-Indicators" );
				pandaCache.delete(  "EventTagList-" + msisdn );
				
				Object subsInd = pandaCache.get( msisdn + "-Indicators");
//				if ( subsInd == null ) {
					subsIndObj = new JsonObject();
					subsIndObj.addProperty( "docType", "subscriberIndicators" );
//				}
//				else
//					subsIndObj = ( JsonObject ) parser.parse( subsInd.toString() );

				for ( int k=1;k<indNames.size();k++) {
					String name = indNames.get( k ).getAsString().trim();
					String type = indTypes.get( k ).getAsString().trim();
					String val = line[k].trim();
					if ( type.equals( "double" ))
						subsIndObj.addProperty( name, Double.parseDouble( val ) );
					if ( type.equals( "String" ))
						subsIndObj.addProperty( name,  val  );
					if ( type.equals( "int" ))
						subsIndObj.addProperty( name, Integer.parseInt( val ) );
					if ( type.equals( "long" ))
						subsIndObj.addProperty( name, Long.parseLong( val ) );
					if ( type.equals( "boolean" ))
						subsIndObj.addProperty( name, Boolean.parseBoolean( val ) );
					if ( type.equals( "JsonObject" ))
						subsIndObj.add( name, (JsonObject)parser.parse( val ) );
					if ( type.equals( "JsonArray" ))
						subsIndObj.add( name, (JsonArray)parser.parse( val ) );
				}

				pandaCache.set( msisdn + "-Indicators", 0, subsIndObj.toString() );
			}
			nCount++;
		}
		reader.close();
		System.out.println( " Count  : " + nCount );
		System.exit( 0 );
	}
}
