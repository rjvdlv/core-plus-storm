package com.knowesis.sift.helper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCache;

import au.com.bytecode.opencsv.CSVReader;

public class CompareIndicators {

	private String readFile( String fileName ) throws IOException {
		BufferedReader br = new BufferedReader( new FileReader( fileName ) );
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while( line != null ) {
				sb.append( line );
				sb.append( "\n" );
				line = br.readLine();
			}
			return sb.toString();
		}finally {
			br.close();
		}
	}

	public static void main(String[] args) throws IOException, URISyntaxException {
		CompareIndicators comparator = new CompareIndicators();
		if( args.length < 4 ) {
			System.err.println( "Usage : java -jar CompareIndicators.jar couchIP jsonFile1 msisdn1 msisdn2" );
			System.exit( 0 );
		}

//		String str1 = comparator.readFile( args[ 0 ] );
//		String str2 = comparator.readFile( args[ 1 ] );
//		
		JsonParser parser = new JsonParser();
//		
//		JsonObject ind1 = ( JsonObject )parser.parse( str1 );
//		JsonObject ind2 = ( JsonObject )parser.parse( str2 );

		String strPersistAddressList = args[ 0 ];
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );

		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddress.length; i++ ) {
			Commons.logger.info( arrPersistAddress[ i ] );
			uris.add( new URI( arrPersistAddress[ i ] ) );
		}
		PandaCache pandaCache = new PandaCache( uris );
		JsonObject ind1 = (JsonObject) parser.parse( ( String ) pandaCache.get( args[ 1 ] ) );
		JsonObject ind2 = (JsonObject) parser.parse( ( String ) pandaCache.get( args[ 2 ] ) );

		CSVReader reader = new CSVReader( new FileReader( args[ 3 ] ) );
		
		int count = 0;
		String[] indLine = null;
		while( ( indLine = reader.readNext() ) != null ) {
			if( ! ind1.has(indLine[ 0 ] ) )
				continue;
			String indVal1 = comparator.convertToString( ind1.get( indLine[ 0 ] ) );
			String indVal2 = comparator.convertToString( ind2.get( indLine[ 0 ] ) );
//			System.out.println( "COMPARING : " + indLine[ 0 ] + " : " + indVal1 + " - " + indVal2 );
			if( ! indVal1.equals( indVal2 ) )
				System.out.println( count ++ + " MISMATCH FOUND : " + indLine[ 0 ] + " : " + indVal1 + " >< " + indVal2 );
			else
				System.out.println( count ++ + " MATCHED : " + indLine[ 0 ] + " : " + indVal1 + " >< " + indVal2 );
		}
	}
	
	private String convertToString( JsonElement elem ) {
		if( elem.isJsonObject() || elem.isJsonArray() )
			return elem.toString();
		return elem.getAsString();
	}
}
