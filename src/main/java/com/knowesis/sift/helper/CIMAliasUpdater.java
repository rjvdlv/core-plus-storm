package com.knowesis.sift.helper;

import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;

import au.com.bytecode.opencsv.CSVReader;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCacheConfig;

public class CIMAliasUpdater {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws URISyntaxException 
	 */
	public static void main( String[ ] args ) throws IOException, URISyntaxException {
		if( args.length < 2 ) {
			System.out.println( "Usage : java -jar CIMAliasUpdater.jar persist-address fileName" );
			return;
		}

		CIMAliasUpdater dep = new CIMAliasUpdater();

		String fileName = args[ 1 ];
		String strPersistAddressList = args[ 0 ];
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );

		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddress.length; i++ ) {
			Commons.logger.info( arrPersistAddress[ i ] );
			uris.add( new URI( arrPersistAddress[ i ] ) );
		}
		PandaCacheConfig pandaCacheConfig = new PandaCacheConfig( uris );
		
		JsonParser parser = new JsonParser();
		
		CSVReader reader = new CSVReader( new FileReader( fileName ) );
		String[] fields = null;
		int nCount = 0;
		while( ( fields = reader.readNext() ) != null ) {
			String cimId = fields[ 0 ];
			String alias = fields[ 1 ];
			String strCIM = ( String ) pandaCacheConfig.get(  cimId  );
			JsonObject jCIM = ( JsonObject ) parser.parse( strCIM );
			jCIM.addProperty( "alias", alias );
			System.out.println( "Updating : " + jCIM.toString() );
			pandaCacheConfig.set( cimId, 0, jCIM.toString() );
			nCount++;
		}
		System.out.println( "Updated Count : " + nCount );
		System.exit( 0 );
		

	}

}
