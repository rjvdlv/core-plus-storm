/**
 * @author Raja SP
 * 
 */
package com.knowesis.sift.helper;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;

import com.unicacorp.interact.api.AdvisoryMessage;
import com.unicacorp.interact.api.AdvisoryMessageCodes;
import com.unicacorp.interact.api.NameValuePair;
import com.unicacorp.interact.api.NameValuePairImpl;
import com.unicacorp.interact.api.Offer;
import com.unicacorp.interact.api.OfferList;
import com.unicacorp.interact.api.Response;
import com.unicacorp.interact.api.jsoverhttp.InteractAPI;

public class CampaignHelper {

	private static final String UNICA_INTERACT_SESSIONID = "1";
	private static final String AUDIENCE_LEVEL = "Customer";
	private static final String INTERECATIVE_CHANNEL = "PCM";

	public void handleEventTrigger( String campaignManagerURL, String msisdn, String eventId, String extId ) throws IOException {
		try {
			InteractAPI api = InteractAPI.getInstance( campaignManagerURL );
			String sessionId = UNICA_INTERACT_SESSIONID;
			String audienceLevel = AUDIENCE_LEVEL;

			NameValuePairImpl custId = new NameValuePairImpl();
			custId.setName( "CustomerID" );
			custId.setValueAsNumeric( Double.parseDouble( msisdn ) );
			custId.setValueDataType( NameValuePair.DATA_TYPE_NUMERIC );
			NameValuePairImpl[ ] initialAudienceId = { custId };

			NameValuePairImpl thisEvent = new NameValuePairImpl();
			thisEvent.setName("This_Event");
			thisEvent.setValueAsString(eventId);
			NameValuePairImpl[] startSessionParam = {thisEvent};

			// startSession( sessionId, relyOnExistingSession, initialDebugFlag,
			// interactiveChannel,initialAudienceId, audienceLevel,
			// initialParameters )
			Response response = api.startSession( sessionId, false, true, INTERECATIVE_CHANNEL, initialAudienceId, audienceLevel, startSessionParam );
			// sessionId, interactionPoint, numberRequested )
			response = api.getOffers( sessionId, INTERECATIVE_CHANNEL, 1 );
			
			//post Event
			Offer[] offers = response.getOfferList().getRecommendedOffers();
			if(offers != null && offers.length > 0){
				// print the response to verify
				printResponse( response );
				String treatmentCode = offers[0].getTreatmentCode();
				processResponse( msisdn, response, treatmentCode );

    			NameValuePairImpl parmB1 = new NameValuePairImpl();
    			parmB1.setName("UACIOfferTrackingCode");
    			parmB1.setValueAsString( treatmentCode );
    			parmB1.setValueDataType(NameValuePair.DATA_TYPE_STRING);
    			NameValuePairImpl[] eventParms = { parmB1};
    			api.postEvent(sessionId, "contact", eventParms);
			} else {
				System.out.println( "No Offers" );
			}
			api.endSession( sessionId );
		}catch( MalformedURLException e ) {
			e.printStackTrace();
		}catch( RemoteException e ) {
			e.printStackTrace();
		}
	}

	private void processResponse( String msisdn, Response response, String extId ) throws IOException {
		OfferList[ ] allOfferList = response.getAllOfferLists();
		if( response == null || allOfferList == null || allOfferList.length == 0 ) {
			System.out.println( "NO OFFERS FOUND!!!!" );
			return;
		}
		Offer[ ] offers = allOfferList[ 0 ].getRecommendedOffers();
		Offer thisOffer = offers[ 0 ];
		NameValuePair[ ] params = thisOffer.getAdditionalAttributes();
		ArrayList< CampaignCommand > commands = new ArrayList< CampaignCommand >();
		for( int i = 0; i < params.length; i++ ) {
			NameValuePair aParam = params[ i ];
			if( aParam.getName().equals( "ActionType" ) ) {
				String value = aParam.getValueAsString();
				if( value.equals( "OfferMonitor" ) ) {
					CampaignCommand command = new RequestCustomerActionCommand( msisdn, params, extId );
					commands.add( command );
				}else if( value.equals( "ProvisionOffer" ) ) {
					CampaignCommand command = new ProvisionCommand( msisdn, params, extId );
					commands.add( command );
				}else if( value.equals( "ProvisionOfferMonitor" ) ) {
					CampaignCommand command = new ProvisionCommand( msisdn, params, extId );
					commands.add( command );
					command = new NoticiationCommand( msisdn, params, extId );
					commands.add( command );
				}else if( value.equals( "Offer" ) ) {
					// this is just a notification - Interact calls it an Offer
					CampaignCommand command = new NoticiationCommand( msisdn, params, extId );
					commands.add( command );
				}
			}
		}
		for( int i = 0; i < commands.size(); i++ )
			commands.get( i ).execute();
	}

	public static void printResponse( Response resp ) {

		System.out.println( "****************************** Response from Get Offer ******************************" );

		if( resp.getStatusCode() == Response.STATUS_SUCCESS ) {
			System.out.println( "getOffers call processed with no warnings or errors" );

			System.out.println( "StatusCode:" + resp.getStatusCode() + "\n" );
			System.out.println( "ApiVersion:" + resp.getApiVersion() + "\n" );
			System.out.println( "SessionID:" + resp.getSessionID() + "\n" );
			AdvisoryMessage[ ] msg = resp.getAdvisoryMessages();
			if( msg != null ) {
				for( int x = 0; x < msg.length; x++ ) {
					AdvisoryMessage am = msg[ x ];
					if( am != null ) {
						System.out.println( "AdvisoryMessage:" + x + "\n" );
						System.out.println( "MessageCode:" + am.getMessageCode() + "\n" );
						System.out.println( "Message:" + am.getMessage() + "\n" );
						System.out.println( "DetailMessage:" + am.getDetailMessage() + "\n" );
						System.out.println( "StatusLevel:" + am.getStatusLevel() + "\n" );
					}
				}
			}else {
				System.out.println( "No advisory msgs\n" );
			}

			NameValuePair[ ] profile = resp.getProfileRecord();
			if( ( profile != null ) && ( profile.length > 0 ) ) {
				printNameValuePairsNew( profile );
			}else {
				System.out.println( "No profileRecord\n" );
			}

			OfferList[ ] allOfferList = resp.getAllOfferLists();
			if( allOfferList != null ) {
				for( OfferList offerList : allOfferList ) {
					System.out.println( "Offers for Interacton Point - " + offerList.getInteractionPointName() + "\n" );
					System.out.println( "DefaultText:" + offerList.getDefaultString() + "\n" );
					Offer[ ] offers = offerList.getRecommendedOffers();
					if( offers != null ) {
						for( int x = 0; x < offers.length; x++ ) {
							if( offers[ x ] != null ) {
								System.out.println( "	**Recommended Offer:" + ( x + 1 ) + "\n" );
								System.out.println( "		Offer Score:" + offers[ x ].getScore() + "\n" );
								System.out.println( "		Offer Name:" + offers[ x ].getOfferName() + "\n" );
								System.out.println( "		Offer Desc:" + offers[ x ].getDescription() + "\n" );
								System.out.println( "		TreatmentCode:" + offers[ x ].getTreatmentCode() + "\n" );
								String[ ] offerCode = offers[ x ].getOfferCode();
								System.out.println( "		OfferCode:" );
								if( offerCode != null ) {
									for( String part : offerCode ) {
										System.out.println( "		" + part );
									}
								}
								NameValuePair[ ] ops = offers[ x ].getAdditionalAttributes();
								System.out.println( "OfferParms:\n" );
								printNameValuePairsNew( ops );
							}
						}
					}else {
						System.out.println( "No recommended offers\n" );
					}
				}
			}else
				System.out.println( "No offerList\n" );
		}else if( resp.getStatusCode() == Response.STATUS_WARNING ) {
			System.out.println( "getOffers call processed with a warning" );
		}else {
			System.out.println( "getOffers call processed with an error" );
		}

		// For any non-successes, there should be advisory messages explaining
		// why
		if( resp.getStatusCode() != Response.STATUS_SUCCESS )
			printDetailMessageOfWarningOrError( "getOffers", resp.getAdvisoryMessages() );

		System.out.println( "****************************** End of Response from Get Offer ******************************" );

	}

	public static void printDetailMessageOfWarningOrError( String command, AdvisoryMessage[ ] messages ) {
		System.out.println( "Calling " + command );
		for( AdvisoryMessage msg : messages ) {
			System.out.println( msg.getMessage() );
			// Some advisory messages may have additional detail:
			System.out.println( msg.getDetailMessage() );

			// All advisory messages have a code that will allow the client to
			// implement different
			// behavior based on the type of warning/error
			if( msg.getMessageCode() == AdvisoryMessageCodes.INVALID_INTERACTIVE_CHANNEL ) {
				System.out.println( "IC passed in is not valid!!" );
			}else if( msg.getMessageCode() == AdvisoryMessageCodes.INVALID_INTERACTION_POINT ) {
				System.out.println( "IP name passed in is not valid!!" );
			} // and so on...
			else // a catch all
			{
				System.out.println( "Method call failed!" );
			}
		}
	}

	private static void printNameValuePairsNew( NameValuePair[ ] nvps ) {

		if( nvps != null ) {
			// Set opSet = new TreeSet(nvpComparator);
			// List opList = Arrays.asList(nvps);
			// opSet.addAll(opList);
			for( NameValuePair op : nvps ) {
				String varName = op.getName();

				// System.out.println("<td>" + op.getName() + "</td>\n");
				String type = op.getValueDataType();
				// System.out.println("<td>" + type + "</td>\n");

				if( "datetime".equalsIgnoreCase( type ) ) {
					Date value = op.getValueAsDate();
					System.out.println( varName + " ::  " + ( value != null ? value : "NULL" ) + "</td>\n" );
				}else if( "string".equalsIgnoreCase( type ) ) {
					String value = op.getValueAsString();
					System.out.println( varName + " :: " + ( value == null ? "NULL" : value ) );
				}else if( "numeric".equalsIgnoreCase( type ) ) {
					Double value = op.getValueAsNumeric();
					System.out.println( varName + " :: " + ( value == null ? "NULL" : value.toString() ) );
				}
			}
		}
	}

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main( String[ ] args ) throws IOException {
		CampaignHelper helper = new CampaignHelper();
		helper.handleEventTrigger( "http://54.251.246.9:9080/interact/servlet/InteractJSService", "66717658003", "COMPETITOR_URL_ACCESS_DETECTION", "1" );
//		helper.handleEventTrigger( "http://54.251.246.9:9080/interact/servlet/InteractJSService", "66807370041", "LOCATION_BASED_VOICE_STIMULATION_SUBSCRIBED_30", "1" );
//		helper.handleEventTrigger( "http://54.251.246.9:9080/interact/servlet/InteractJSService", "66807370031", "NO_EVENT_HIGH_CHURN_DATA_PACKAGE_SUBSCRIBED", "1" );
	}

}
