package com.knowesis.sift.helper;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TimeZone;

import org.springframework.aop.framework.adapter.ThrowsAdviceInterceptor;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.protocol.views.Query;
import com.couchbase.client.protocol.views.Stale;
import com.couchbase.client.protocol.views.View;
import com.couchbase.client.protocol.views.ViewResponse;
import com.couchbase.client.protocol.views.ViewRow;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class DTACExtractMasterConfigurations {

	protected CouchbaseClient cache = null;
	protected JsonParser parser = new JsonParser();
	protected Date date;
	protected DateFormat dateFormat = new SimpleDateFormat( "dd-MM-yyyy HH:mm:ss" );
	//protected DateFormat dateFormat = new SimpleDateFormat( "YYYY-MM-DD HH24:MI:SS" );

	public void initializeCache( String pAddr, String bucketName ) throws Exception {
		String[] strPersistAddress = pAddr.split( "," );
		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < strPersistAddress.length; i++ ) {
			uris.add( new URI( strPersistAddress[ i ] ) );
		}
		cache = new CouchbaseClient( uris, bucketName, "" );
		dateFormat.setTimeZone( TimeZone.getTimeZone( "GMT" ) );
	}

	public void extractProgramConfig( String fileName ) throws IOException{
		@SuppressWarnings( "resource" )
		PrintWriter writer = new PrintWriter( fileName );
		String viewName = "view_allPrograms";
		View view = cache.getView( "pandalytics", viewName  );
		Query query = new Query();
		query.setIncludeDocs( false );
		query.setStale( Stale.OK );
		ViewResponse viewresponse = cache.query( view, query );
		Iterator< ViewRow > itr = viewresponse.iterator();

		JsonElement elementCheck;
		while( itr.hasNext() ) {
			ViewRow viewRow = itr.next();
			String thisRecord;
			String id = viewRow.getId();

			String strDoc = ( String ) cache.get( viewRow.getId() );
			JsonObject jDoc = ( JsonObject ) parser.parse( strDoc );

			// PROGRAM_ID 
			thisRecord = id + "|";						

			// PROGRAM_NAME 
			thisRecord += id + "|";		

			// PROGRAM_CLM_ID 
			elementCheck = jDoc.get( "programClmId" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else{
				String desc = elementCheck.getAsString();
				thisRecord += desc + "|";
			}

			//PROGRAM_DESCRIPTION 
			elementCheck = jDoc.get( "description" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else{
				String desc = elementCheck.getAsString().replaceAll( "\n", " " ).replaceAll( "\r", " " );
				thisRecord += desc + "|";
			}

			// PROGRAM_CATEGORY 
			elementCheck = jDoc.get( "category" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else
				thisRecord += elementCheck.getAsString() + "|";
			
			// PROGRAM_OBJECTIVE 
			elementCheck = jDoc.get( "objective" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else
				thisRecord += elementCheck.getAsString() + "|";
			
			// PROGRAM_UNIT 
			elementCheck = jDoc.get( "programUnit" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else
				thisRecord += elementCheck.getAsString() + "|";
			
			// PROGRAM_ACCOUNT_TYPE 
			elementCheck = jDoc.get( "programAccountType" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else
				thisRecord += elementCheck.getAsString() + "|";
			
			// PROGRAM_START_TIME 
			date = new Date( jDoc.get( "startDateTime" ).getAsLong() );
			String formattedDate = dateFormat.format( date );
			thisRecord += formattedDate + "|";

			// PROGRAM_END_TIME 
			date = new Date( jDoc.get( "endDateTime" ).getAsLong() );
			formattedDate = dateFormat.format( date );
			thisRecord += formattedDate + "|";
			
			// PROGRAM_STATUS 
			thisRecord += jDoc.get( "status" ).getAsString() + "|";
			
			// OWNING_DEPARTMENT 
			elementCheck = jDoc.get( "campaignDepartment" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else
				thisRecord += elementCheck.getAsString() + "|";
			
			// OWNER 
			elementCheck = jDoc.get( "campaignOwner" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else
				thisRecord += elementCheck.getAsString() + "|";
			
			JsonElement updateHistEle = jDoc.get( "updateHistory" );
			if( updateHistEle != null && ! updateHistEle.isJsonNull() ) {
				JsonArray updateHistArray = updateHistEle.getAsJsonArray();
				if( updateHistArray.size() > 0 ) {
					// CREATED_BY
					JsonObject createdObject = updateHistArray.get( 0 ).getAsJsonObject();
					thisRecord += createdObject.get( "updatedBy" ).getAsString() + "|";

					// CREATED_TIME 
					date = new Date( createdObject.get( "updatedTimestamp" ).getAsLong() );
					formattedDate = dateFormat.format( date );
					thisRecord += formattedDate + "|";

					// LAST_UPDATE_TIME
					JsonObject lastUpdateObj = updateHistArray.get( updateHistArray.size() - 1 ).getAsJsonObject();
					date = new Date( lastUpdateObj.get( "updatedTimestamp" ).getAsLong() );
					formattedDate = dateFormat.format( date );
					thisRecord += formattedDate + "|";

					// UPDATE_DESCRIPTION 
					thisRecord += lastUpdateObj.get( "remarks" ).getAsString();
				}
				else {
					thisRecord += "|||";
				}
			}
			else {
				thisRecord += "|||";
			}
			thisRecord += "\n";
			writer.write( thisRecord );
		}
		writer.close();
		cache.shutdown();
		System.out.println( "ExtractConfigurations : Program extraction completed" );
	}

	public void extractTriggerConfig( String fileName ) throws FileNotFoundException{
		PrintWriter writer = new PrintWriter( fileName );
		String viewName = "view_allEvents";
		View view = cache.getView( "pandalytics", viewName  );
		Query query = new Query();
		query.setIncludeDocs( false );
		query.setStale( Stale.OK );
		ViewResponse viewresponse = cache.query( view, query );
		Iterator< ViewRow > itr = viewresponse.iterator();
		JsonElement elementCheck;
		while( itr.hasNext() ){
			String thisRecord;
			ViewRow row = itr.next();
			String strDoc = ( String ) cache.get( row.getId() );
			JsonObject jDoc = ( JsonObject ) parser.parse( strDoc );

			// TRIGGER_ID 
			thisRecord = row.getId() + "|";
			
			// TRIGGER_NAME
			elementCheck = jDoc.get( "name" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += row.getId() + "|";
			else
				thisRecord += elementCheck.getAsString() + "|";
			
			// TRIGGER_DESCRIPTION 
			elementCheck = jDoc.get( "description" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else{
				String desc = elementCheck.getAsString().replaceAll( "\n", " " ).replaceAll( "\r", " " );
				thisRecord += desc + "|";
			}

			// TRIGGER_CATEGORY 
			elementCheck = jDoc.get( "category" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else
				thisRecord += elementCheck.getAsString() + "|";
			
			// STATUS 
			thisRecord += jDoc.get( "status" ).getAsString() + "|";
			
			// TRIGGER_IMEDIATE_FLAG 
			thisRecord += "true" + "|";
			
			// TRIGGER_DATASOURCE 
			thisRecord += "defaultDataSource" + "|";

			JsonElement updateHistEle = jDoc.get( "updateHistory" );
			if( updateHistEle != null && ! updateHistEle.isJsonNull() ) {
				JsonArray updateHistArray = updateHistEle.getAsJsonArray();
				if( updateHistArray.size() > 0 ) {
					// CREATED_BY
					JsonObject createdObject = updateHistArray.get( 0 ).getAsJsonObject();
					thisRecord += createdObject.get( "updatedBy" ).getAsString() + "|";
					
					// CREATED_TIME 
					date = new Date( createdObject.get( "updatedTimestamp" ).getAsLong() );
					String formattedDate = dateFormat.format( date );
					thisRecord += formattedDate + "|";

					// LAST_UPDATE_TIME
					JsonObject lastUpdateObj = updateHistArray.get( updateHistArray.size() - 1 ).getAsJsonObject();
					date = new Date( lastUpdateObj.get( "updatedTimestamp" ).getAsLong() );
					formattedDate = dateFormat.format( date );
					thisRecord += formattedDate + "|";

					// UPDATE_DESCRIPTION 
					thisRecord += lastUpdateObj.get( "remarks" ).getAsString();
				}
				else {
					thisRecord += "|||";
				}
			}
			else {
				thisRecord += "|||";
			}
			thisRecord += "\n";
			writer.write( thisRecord );
		}
		writer.close();
		cache.shutdown();
		System.out.println( "ExtractConfigurations : Trigger extraction completed" );
	}


	public void extractProductConfig( String fileName ) throws FileNotFoundException{
		PrintWriter writer = new PrintWriter( fileName );
		String viewName = "view_allProducts";
		View view = cache.getView( "pandalytics", viewName  );
		Query query = new Query();
		query.setIncludeDocs( false );
		query.setStale( Stale.OK );
		ViewResponse viewresponse = cache.query( view, query );
		Iterator< ViewRow > itr = viewresponse.iterator();
		JsonElement elementCheck;
		while( itr.hasNext() ){
			String commonfields;
			ViewRow row = itr.next();
			String strDoc = ( String ) cache.get( row.getId() );
			JsonObject jDoc = ( JsonObject ) parser.parse( strDoc );
			
			// PRODUCT_ID 
			commonfields = row.getId() + "|";
			
			// PRODUCT_NAME 
			commonfields += jDoc.get( "name" ).getAsString() + "|";

			// PRODUCT_DESCRIPTION 
			elementCheck = jDoc.get( "description" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				commonfields += "|";
			else{
				String desc = elementCheck.getAsString().replaceAll( "\n", " " ).replaceAll( "\r", " " );
				commonfields += desc + "|";
			}

			// PRODUCT_CATEGORY 
			elementCheck = jDoc.get( "category" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				commonfields += "|";
			else{
				String desc = elementCheck.getAsString().replaceAll( "\n", " " ).replaceAll( "\r", " " );
				commonfields += desc + "|";
			}

			// PRODUCT_TARGET_SYSTEM 
			elementCheck = jDoc.get( "targetSystem" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				commonfields += "|";
			else
				commonfields += elementCheck.getAsString() + "|";

			// PACKAGE_CODE 
			
			
			// BONUS_POCKET_CODE 
			
			
			// BONUS_UNIT 
			
			
			// BONUS_PERCENTAGE 
			
			
			// REASON_CODE 
			
			
			
			// BONUS_AMOUNT 
			
			
			// BONUS_VALIDITY 
			
			
			// CBS_OFFER_ID 
			
			
			// WAIVE_FEE 
			
			
			// PACKAGE_PRICE 
			
			
			// FULFILMENT_SYSTEM 
			
			
			// OPT_IN_SHORT_CODE 
			
			
			// OPT_IN_KEYWORD 
			
			
			// 
			
			
			
			
			
			
			
			
			
			
			

			JsonElement dynamicParamsEle = jDoc.get( "dynamicParameters" );
			if( dynamicParamsEle == null || elementCheck.isJsonNull() )
				continue;

			JsonArray dynamicParamsArray = dynamicParamsEle.getAsJsonArray();
			for( int i = 0; i < dynamicParamsArray.size(); i ++ ){
				String record = commonfields;
				JsonObject dynParamObj = dynamicParamsArray.get( i ).getAsJsonObject();
				record += dynParamObj.get( "key" ).getAsString() + "|";
				record += dynParamObj.get( "value" ).getAsString() + "|";
				record += dynParamObj.get( "type" ).getAsString() + "|";
				record += dynParamObj.get( "rewardValidityDays" ).getAsString() + "|";

				JsonElement updateHistEle = jDoc.get( "updateHistory" );
				JsonArray updateHistArray = updateHistEle.getAsJsonArray();

				if( updateHistArray.size() > 1 ) {
					date = new Date( updateHistArray.get( 0 ).getAsJsonObject().get( "updatedTimestamp" ).getAsLong() );
					String formattedDate = dateFormat.format( date );
					record += formattedDate + "|";

					date = new Date( updateHistArray.get( updateHistArray.size() - 1 ).getAsJsonObject().get( "updatedTimestamp" ).getAsLong() );
					formattedDate = dateFormat.format( date );
					record += formattedDate;
				}
				else {
					record += "|";
				}
				record += "\n";
				writer.write( record );
			}
		}
		writer.close();
		cache.shutdown();
		System.out.println( "ExtractConfigurations : Product extraction completed" );
	}


	public void extractOfferConfig( String fileName ) throws FileNotFoundException{
		PrintWriter writer = new PrintWriter( fileName );
		String viewName = "view_allOffers";
		View view = cache.getView( "pandalytics", viewName  );
		Query query = new Query();
		query.setIncludeDocs( false );
		query.setStale( Stale.OK );
		ViewResponse viewresponse = cache.query( view, query );
		Iterator< ViewRow > itr = viewresponse.iterator();
		while( itr.hasNext() ){
			String thisRecord;
			ViewRow row = itr.next();
			String strDoc = ( String ) cache.get( row.getId() );

			JsonObject jDoc = ( JsonObject ) parser.parse( strDoc );
			JsonElement elementCheck;

			// OFFER_ID 
			thisRecord = row.getId() + "|";

			// OFFER_NAME 
			thisRecord += jDoc.get( "name" ).getAsString() + "|";

			// OFFER_DESCRIPTION 
			elementCheck = jDoc.get( "description" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else{
				String desc = elementCheck.getAsString().replaceAll( "\n", " " ).replaceAll( "\r", " " );
				thisRecord += desc + "|";
			}

			// OFFER_CATEGORY 
			elementCheck = jDoc.get( "category" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else
				thisRecord += elementCheck.getAsString() + "|";

			// OFFER_TYPE 
			thisRecord += jDoc.get( "offerType" ).getAsString() + "|";

			// OFFER_PRICE 
			elementCheck = jDoc.get( "offerPrice" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else
				thisRecord += elementCheck.getAsString() + "|";

			// OFFER_ACCOUNT_TYPE 
			elementCheck = jDoc.get( "offerAccountType" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else
				thisRecord += elementCheck.getAsString() + "|";

			// OFFER_MESAUREMENT_GROUP 
			elementCheck = jDoc.get( "offerMeasurementGroup" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else
				thisRecord += elementCheck.getAsString() + "|";

			// BU_CODE 
			elementCheck = jDoc.get( "businessCode" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else
				thisRecord += elementCheck.getAsString() + "|";

			// OFFER_START_TIME 
			elementCheck = jDoc.get( "effectiveDate" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else {
				date = new Date( elementCheck.getAsLong() );
				String formattedDate = dateFormat.format( date );
				thisRecord += formattedDate + "|";
			}

			// OFFER_END_TIME 
			elementCheck = jDoc.get( "expiryDate" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else {
				date = new Date( elementCheck.getAsLong() );
				String formattedDate = dateFormat.format( date );
				thisRecord += formattedDate + "|";
			}

			// OFFER_REVENUE 
			elementCheck = jDoc.get( "offerRevenue" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else
				thisRecord += elementCheck.getAsString() + "|";

			// DELIVERY_REPORT_FLAG 
			elementCheck = jDoc.get( "deliveryReportRequired" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else
				thisRecord += elementCheck.getAsString() + "|";

			JsonElement updateHistEle = jDoc.get( "updateHistory" );
			if( updateHistEle != null && ! updateHistEle.isJsonNull() ) {
				JsonArray updateHistArray = updateHistEle.getAsJsonArray();
				if( updateHistArray.size() > 0 ) {
					// CREATED_BY
					JsonObject createdObject = updateHistArray.get( 0 ).getAsJsonObject();
					thisRecord += createdObject.get( "updatedBy" ).getAsString() + "|";

					// CREATED_TIME 
					date = new Date( createdObject.get( "updatedTimestamp" ).getAsLong() );
					String formattedDate = dateFormat.format( date );
					thisRecord += formattedDate + "|";

					// LAST_UPDATE_TIME
					JsonObject lastUpdateObj = updateHistArray.get( updateHistArray.size() - 1 ).getAsJsonObject();
					date = new Date( lastUpdateObj.get( "updatedTimestamp" ).getAsLong() );
					formattedDate = dateFormat.format( date );
					thisRecord += formattedDate + "|";

					// UPDATE_DESCRIPTION 
					thisRecord += lastUpdateObj.get( "remarks" ).getAsString() + "|";
				}
				else {
					thisRecord += "||||";
				}
			}
			else {
				thisRecord += "||||";
			}
		
			String cbsOfferId = "";
			String packCode = "";
			// PACKAGE_CODE
			elementCheck = jDoc.get( "fulfillmentList" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else {
				JsonArray fulfilListArray = elementCheck.getAsJsonArray();
				JsonObject thisList = fulfilListArray.get( 0 ).getAsJsonObject();
				JsonElement thisFulfilProduct = thisList.get( "fulfillmentProducts" );
				if( thisFulfilProduct == null || thisFulfilProduct.isJsonNull() )
					thisRecord += "|";
				else {
					JsonObject thisProduct = thisFulfilProduct.getAsJsonArray().get( 0 ).getAsJsonObject(); 
					JsonElement eDynParams = thisProduct.get( "dynamicParameters" );
					if( eDynParams == null || eDynParams.isJsonNull() )
						thisRecord += "|";
					else {
						JsonArray dynArray = eDynParams.getAsJsonArray();
						boolean packCodeFlag = false;
						for( int i = 0; i < dynArray.size(); i ++ ) {
							JsonObject thisObj = dynArray.get( i ).getAsJsonObject();
							if( thisObj.get( "key" ).getAsString().equals( "packCode" ) ) {
								thisRecord += thisObj.get( "value" ).getAsString() + "|";

								packCode = thisObj.get( "value" ).getAsString();
								packCodeFlag = true;
							}	
							if( thisObj.get( "key" ).getAsString().equals( "cbs_Offer_Id" ) ) {
								cbsOfferId = thisObj.get( "value" ).getAsString();
							}	
						}
						if( ! packCodeFlag )
							thisRecord += "|";
					}
				}
			}
			
			
			// BONUS_POCKET_CODE
			elementCheck = jDoc.get( "bonusType" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else
				thisRecord += elementCheck.getAsString() + "|";
			
			
			// BONUS_UNIT
			thisRecord += "THB|";
			
			// BONUS_PERCENTAGE
			elementCheck = jDoc.get( "bonusPercentage" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else
				thisRecord += elementCheck.getAsString() + "|";
			
			
			// BONUS_REASON_CODE
			thisRecord += "|";
			
			// BONUS_AMOUNT
			elementCheck = jDoc.get( "bonusValue" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else
				thisRecord += elementCheck.getAsString() + "|";
			
			
			// BONUS_VALIDITY
			elementCheck = jDoc.get( "bonusValidityDays" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else
				thisRecord += elementCheck.getAsString() + "|";
			
			
			// CBS_OFFER_ID
			thisRecord += cbsOfferId + "|";
			
			
			// PACKAGE_PRICE
			elementCheck = jDoc.get( "offerPrice" );
			if( elementCheck == null || elementCheck.isJsonNull() )
				thisRecord += "|";
			else
				thisRecord += elementCheck.getAsString() + "|";
			
			
			// OPT_IN_SHORTCODE
			thisRecord += "|";
			
			
			// OPT_IN_KEYWORD
			thisRecord += "|";
			
			
			// FULFILLMENT_SYSTEM
			thisRecord += packCode;
			
			thisRecord += "\n";
			writer.write( thisRecord );
		}
		writer.close();
		cache.shutdown();
		System.out.println( "ExtractConfigurations : Offer extraction completed" );
	}
 
	public static void main( String[ ] args ) throws Exception {
		if( args.length != 4 ){
			System.out.println("USAGE: java -jar DTACExtractMasterConfig.jar PersistAddress BucketName Operation[ Program|Offer|Product|Trigger ] OutputFileName");
			System.exit( 0 );
		}

		String persistAddress = args[ 0 ];
		String bucketName = args[ 1 ];
		String operation = args[ 2 ];
		String outputFileName = args[ 3 ];
		DTACExtractMasterConfigurations extractConfig = new DTACExtractMasterConfigurations();
		extractConfig.initializeCache( persistAddress, bucketName );

		if( operation.equalsIgnoreCase( "Program" ) )
			extractConfig.extractProgramConfig( outputFileName );

		else if( operation.equalsIgnoreCase( "Trigger" ) )
			extractConfig.extractTriggerConfig( outputFileName );

		else if( operation.equalsIgnoreCase( "Product" ) )
			extractConfig.extractProductConfig( outputFileName );

		else if( operation.equalsIgnoreCase( "Offer" ) ) 
			extractConfig.extractOfferConfig( outputFileName );

		else
			System.out.println("USAGE: java -jar ExtractConfigurations.jar PersistAddress BucketName Operation[ Program|Offer|Product|Trigger ] OutputFileName");
	}

}