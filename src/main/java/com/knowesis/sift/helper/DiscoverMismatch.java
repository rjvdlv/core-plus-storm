package com.knowesis.sift.helper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.LinkedList;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCache;
import com.knowesis.sift.Common.PandaCacheConfig;

public class DiscoverMismatch {

	/**
	 * @param args
	 * @throws URISyntaxException 
	 * @throws IOException 
	 */
	public static void main( String[ ] args ) throws URISyntaxException, IOException {
		if( args.length < 2 ) {
			System.err.println( "Usage : java -jar LoadConfigDocs.jar http://<persistAddress>:8091 csvFilePath" );
			System.exit( 0 );
		}
		String strPersistAddressList = args[ 0 ];
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );
		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddress.length; i++ ) {
			Commons.logger.info( arrPersistAddress[ i ] );
			uris.add( new URI( arrPersistAddress[ i ] ) );
		}
		PandaCacheConfig pandaCacheConfig = new PandaCacheConfig( uris );

		ArrayList< String > matchDocs = new ArrayList< String >();
		ArrayList< String > statusDifferent = new ArrayList< String >();
		ArrayList< String > expressionDifferent = new ArrayList< String >();
		ArrayList< String > bothDifferent = new ArrayList< String >();
		
        File f = new File( args[ 1 ] );

        JsonParser parser = new JsonParser();
        int mismatchedDocumentsCount = 0;
        
        BufferedReader b = new BufferedReader(new FileReader( f ) );
        String readLine = "";
        while ( ( readLine = b.readLine() ) != null ) {
        	String[] content = readLine.split( "~siftdelimiter~" );
        	String docOnServer = (String) pandaCacheConfig.get(content[ 0 ] );
        	if( docOnServer.equals( content[ 1 ] ) ) {
        		matchDocs.add( content[ 0 ] );
        		continue;
        	}
        	mismatchedDocumentsCount++;
        	JsonObject jServerDoc = (JsonObject) parser.parse( docOnServer );
        	JsonObject jDiscDoc = (JsonObject) parser.parse( content[ 1 ] );
        	boolean expressionMismatch = false;
        	boolean statusMismatch = false;
        	if( ! jServerDoc.get( "expression" ).getAsString().equals( jDiscDoc.get( "expression" ).getAsString() ) )
        		expressionMismatch = true;
        	if( ! jServerDoc.get( "status" ).getAsString().equals( jDiscDoc.get( "status" ).getAsString() ) )
        		statusMismatch = true;
            if( ! expressionMismatch &&  statusMismatch )
            	statusDifferent.add( content[ 0 ] );
            else if( expressionMismatch && ! statusMismatch )
            	expressionDifferent.add( content[ 0 ] );
            else
            	bothDifferent.add( content[ 0 ] );
        }
		b.close();
		
		System.out.println( "Matched Document Count : " + matchDocs.size() );
		System.out.println( "Mismatched Document Count : " + mismatchedDocumentsCount );
		
		System.out.println( "Status Mismatched Document Count : " + statusDifferent.size() );
		for( int i=0; i<statusDifferent.size(); i++ ) {
			System.out.println( i + " : " + statusDifferent.get( i ) );
		}

		System.out.println( "Expression Mismatched Document Count : " + expressionDifferent.size() );
		for( int i=0; i<expressionDifferent.size(); i++ ) {
			System.out.println( i + " : " + expressionDifferent.get( i ) );
		}

		System.out.println( "Both Mismatched Document Count : " + bothDifferent.size() );
		for( int i=0; i<bothDifferent.size(); i++ ) {
			System.out.println( i + " : " + bothDifferent.get( i ) );
		}
System.exit( 0 );
	}	
}
