package com.knowesis.sift.helper;

import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;

import au.com.bytecode.opencsv.CSVReader;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCacheConfig;

public class PayloadUpdater {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws URISyntaxException 
	 */
	public static void main( String[ ] args ) throws IOException, URISyntaxException {
		if( args.length < 2 ) {
			System.out.println( "Usage : java -jar PayloadUpadter.jar persist-address fileName" );
			return;
		}

		PayloadUpdater dep = new PayloadUpdater();

		String fileName = args[ 1 ];
		String strPersistAddressList = args[ 0 ];
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );

		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddress.length; i++ ) {
			Commons.logger.info( arrPersistAddress[ i ] );
			uris.add( new URI( arrPersistAddress[ i ] ) );
		}
		PandaCacheConfig pandaCacheConfig = new PandaCacheConfig( uris );
		
		JsonParser parser = new JsonParser();
		
		CSVReader reader = new CSVReader( new FileReader( fileName ), '|' );
		String[] fields = null;
		int nCount = 0;
		while( ( fields = reader.readNext() ) != null ) {
			String eventId = fields[ 0 ];
			String payload = fields[ 1 ];
			String strEvent = ( String ) pandaCacheConfig.get(  eventId  );
			JsonObject jEvent = ( JsonObject ) parser.parse( strEvent );
			jEvent.addProperty( "eventPayload", payload );
			System.out.println( "Updating : " + jEvent.toString() );
			pandaCacheConfig.set( eventId, 0, jEvent.toString() );
			nCount++;
		}
		System.out.println( "Updated Count : " + nCount );
		System.exit( 0 );
	}
}
