package com.knowesis.sift.helper;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.google.gson.JsonObject;

import au.com.bytecode.opencsv.CSVReader;

public class LoadPackageSpeed {

	public static void main(String[] args) throws IOException {
		CSVReader reader = new CSVReader( new FileReader( "/home/streamsadmin/Documents/DTAC/package_master20170718.csv" ), '|' );
		JsonObject jSpeed = new JsonObject();
		String[] masterLine = null;
		while( ( masterLine = reader.readNext() ) != null ) {
			if( masterLine[ 1 ].trim().toLowerCase().equals( "additional") ) {
				String speed = masterLine[ 9 ].trim().toUpperCase(); 
				String svalue = "0";
				if( speed.equals( "" ) )
					svalue = "0"; 
				else {
					String value = speed.substring( 0, speed.length() - 1 );
					char lastChar = speed.charAt( speed.length() - 1 ); 
					System.out.println( "Working on" + masterLine[ 0 ] + " " + speed + " - " + value + " - " + lastChar );
					if( lastChar == 'M' ) {
						int speedDbl = Integer.parseInt( value ) * 1024;
						svalue = Integer.toString( speedDbl );
					}
					else if( lastChar == 'G' ) {
						int speedDbl = Integer.parseInt( value ) * 1024 *  1024;
						svalue = Integer.toString( speedDbl );
					}
					else if( lastChar == 'K' )
						svalue = value;
				}
				System.out.println( "Adding " + masterLine[ 0 ] + " = " + svalue );
				jSpeed.addProperty( masterLine[ 0 ], svalue );
			}
		}
		System.out.println( jSpeed.toString() );
	}

}
