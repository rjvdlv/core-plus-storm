package com.knowesis.sift.core;

/**
 * @author Raja SP
 *
 */

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;

import org.apache.storm.generated.KillOptions;
import org.apache.storm.generated.Nimbus.Client;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.thrift.TException;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.apache.storm.utils.NimbusClient;
import org.apache.storm.utils.Utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.CustomerState;

public class PandamaticOperatorCaller extends PandamaticOperator {

	JsonObject tuple = null;
	private String strPersistAddressList = null;
	private String siftMode = null;
	private int cacheSize;
	private String topologyName = null;
	private String configPlacement = null;
	private int memoryLoad = 0;
	
	public String getStrPersistAddressList() {
		return strPersistAddressList;
	}


	public void setStrPersistAddressList( String strPersistAddressList ) {
		Commons.logger.info( "Setting the strPersistAddressList in PandamaticOperatorCaller as: " + strPersistAddressList );
		this.strPersistAddressList = strPersistAddressList;
	}


	public String getSiftMode() {
		return siftMode;
	}


	public void setSiftMode( String siftMode ) {
		Commons.logger.info( "Setting the siftMode in PandamaticOperatorCaller as: " + siftMode );
		this.siftMode = siftMode;
	}


	public int getCacheSize() {
		return cacheSize;
	}


	public void setCacheSize( int cacheSize ) {
		Commons.logger.info( "Setting the cacheSize in PandamaticOperatorCaller as: " + cacheSize );
		this.cacheSize = cacheSize;
	}

	public void setConfig( String configPlacement ) {
		Commons.logger.info( "Setting the configPlacement in PandamaticOperatorCaller as: " + configPlacement );
		this.configPlacement = configPlacement;
	}

	
	public int getMemoryLoad() {
		return memoryLoad;
	}


	public void setMemoryLoad( int memoryLoad ) {
		Commons.logger.info( "Setting the memory load in PandamaticOperatorCaller as: " + memoryLoad );
		this.memoryLoad = memoryLoad;
	}


	@Override
	 public Map< String, Object > getComponentConfiguration () {
		
		 Map< String, Object > configuration = super.getComponentConfiguration();
		 if ( configuration == null )
			 configuration = new HashMap< String, Object >();

		 Commons.logger.info( "default configuraion for Pandamatic operator instance: " + configuration.toString() );
		 Commons.logger.info( "Setting the configPlacement: " + configPlacement );
		 configuration.put( "configPlacement", configPlacement );
		 
		 if( memoryLoad != 0 ) {
			 Commons.logger.info( "Setting the memory for Pandamatic Instance: " + memoryLoad );
			 configuration.put( "topology.component.resources.onheap.memory.mb", memoryLoad );
		 }
			 
		 Commons.logger.info( "updated configuraion for Pandamatic operator instance: " + configuration.toString() );
		 return configuration;
	 }

	@Override
	public void prepare( Map stormConf, TopologyContext context, OutputCollector collector ) {
		try {
			super.initialize( context.getThisTaskIndex() + 1, strPersistAddressList, siftMode, cacheSize );
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		super.outputCollector = collector;
		topologyName = context.getStormId();
		Commons.logger.info( "Task index for PandamaticOperatorCaller: " + ( context.getThisTaskIndex() + 1 ) );
	}


	@Override
	public void execute( Tuple input ) {

		try {
			String streamName = input.getSourceComponent();
			Commons.logger.info( "StreamName.." + streamName );
			Commons.logger.info( "CommonsState nworkerId: " + commonsState.nWorkerId );
			
			if ( streamName.equals( "Sift_Config_Update" ) ) {
				handleConfigurationChanges( input );
				return;
			} else if ( streamName.contains( "Splitted_TagList_Out" ) ) {
				tuple = ( JsonObject ) input.getValueByField( "record" );
				handleTagListUpdates( tuple );
				return;
			} else if ( streamName.contains( "Scheduled_Trigger" ) ) {
				if ( ! Commons.EventsToBeProcessed.equals( "ALL" ) )
					return;

				// TODO: Check this time.
				long time = input.getLongByField( "timeStamp" );
				time = time - ( time % 60000 );
				Date thisDate = new java.util.Date( time );
				Calendar cal = Calendar.getInstance();
				cal.setTime(thisDate);

				handleScheduledReminder( time, "RE", thisDate, cal );
				handleScheduledReminder( time, "NE", thisDate, cal );
				handleScheduledReminder( time, "PE", thisDate, cal );
				handleScheduledReminder( time, "TL", thisDate, cal );
				handleScheduledReminder( time, "SE", thisDate, cal );
				handleScheduledNextWave( time, "NW", thisDate, cal );
				return;
			}

			tuple = ( JsonObject ) Commons.parser.parse( input.getString( 0 ) );
			java.util.Date thisRecordDate = null;
			String subscriber = null;
			String serviceType = tuple.get( "RECORD_TYPE" ).getAsString();
			
			subscriber = ( String ) input.getValueByField( "splitterKey" );
			long lCallDate = tuple.get( "RECORD_TIMESTAMP" ).getAsLong();
			thisRecordDate = new java.util.Date( lCallDate );
			Calendar cal = Calendar.getInstance();
			cal.setTime( thisRecordDate );

			SimpleDateFormat dtFormat = new SimpleDateFormat( "YYYY-MM-dd-HH:mm:ss" );
			dtFormat.setTimeZone( TimeZone.getTimeZone("GMT") );
			Commons.logPrefix = "[" + subscriber + "-" + dtFormat.format( lCallDate ) + "]  ";

			Commons.logger.info( Commons.logPrefix + "Processing now for msisdn : " + subscriber + " Tuple : " + tuple.toString());

			CustomerState customerState = getIndicatorsEventsTags( subscriber, tuple, thisRecordDate, serviceType, cal );

			int lifeCycleRC = processLifeCycleEvents(customerState);
			Commons.logger.info( Commons.logPrefix + "processLifeCycleEvents result : "  + lifeCycleRC );

			if ( lifeCycleRC == 0 ) 
				return;
			if ( lifeCycleRC == 2 ) {
				processAllIndicators( customerState );
				processAllEvents( customerState );
			}

			saveSubscriberIndicators( customerState, serviceType, subscriber );

			Commons.logger.info( Commons.logPrefix + "Before Processing the next wave : " + customerState.subscriberEventTags.toString() );
			processNextWave( customerState, subscriber, tuple, thisRecordDate, serviceType, cal );

			// synchronized( Commons.numTuples ) {
			if ( commonsState.numTuples == 0 )
				commonsState.firstTupleTimestamp = new java.util.Date().getTime();
			else if ( commonsState.numTuples % Commons.telcoPersistInterval == 0 ) {
				logPerformanceStatistics();
				if ( Commons.flowFunnelFlag ) {
					for ( String key : commonsState.flowFunnelMap.keySet() )
						Commons.pandaCache.set( "FLOWFUNNEL_" + customerState.tupleMap.get( "REQUESTER_CHANNEL" ) + "_" + key + "-" + commonsState.nWorkerId, 0,
								commonsState.flowFunnelMap.get( key ).toString() );
				}
				commonsState.numTuples = 0;
			}
			commonsState.numTuples++;
			
			outputCollector.emit( "telcorecord", new Values( serviceType, lCallDate ) );
		}
		catch( Exception ex ) {
			ex.printStackTrace();
			System.out.println( Commons.logPrefix + "General failure found in Sift: " + ( tuple.toString() ) );
			System.out.println( "Stopping the topology " + topologyName + ".. Please verify the logs for failure reasons." );
			
			Map< String, Object > conf = Utils.readStormConfig();
			Client client = NimbusClient.getConfiguredClient( conf ).getClient();
			KillOptions killOpts = new KillOptions();
			try {
				client.killTopologyWithOpts( "Sift-topology", killOpts );
			} catch ( TException e ) {
				e.printStackTrace();
			}
			
			throw new NullPointerException();
		}	
	}


	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declareStream( "record", new Fields( "SinkType", "message" ) );
		declarer.declareStream( "telcorecord", new Fields( "RECORD_TYPE", "RECORD_TIMESTAMP" ) );
	}

	private int processLifeCycleEvents(CustomerState customerState) throws NoSuchMethodException,
	InstantiationException, InvocationTargetException, IllegalAccessException, ParseException {

		Commons.logger.info( Commons.logPrefix + "TupleMap Before Calling manageSubscriberLifeCycle Function  : " + customerState.tupleMap.toString());

		// 0 means don't do anything, 1 means set in couchbase, 2 means
		// processOnlyIndicators, 3 processOnlyEvents, 4 processAll
		int lifeCycleRC = (int) evaluateExpression("manageSubscriberLifeCycle", null, false, null, customerState);

		// handle the offer communication failure scenarios here
		if (customerState.tupleMap.get("RECORD_TYPE").equals("REGISTER_ACTION")) {
			String actionType = customerState.tupleMap.get("ACTION_TYPE").getAsString();
			String flowId = customerState.tupleMap.get("FLOW_ID").getAsString();
			String eventId = customerState.tupleMap.get("EVENTID").getAsString();
			JsonObject thisEventDef = Commons.allEveDef.get(eventId);

			if (thisEventDef == null)
				return 0;
			Commons.logger.info( Commons.logPrefix + "Verifying if trigger is attached to programs : "  + eventId );

			if ( ( ! thisEventDef.has( "programs" ) || thisEventDef.get( "programs" ) == null || thisEventDef.get( "programs" ).isJsonNull()
					|| thisEventDef.get("programs").getAsJsonArray().size() == 0)) {
				if (actionType != null && actionType.equals("OFFER_FAILED")) {
					// Deleting Event Triggers if any
					customerState.subscriberTriggers = (JsonObject) customerState.subscriberIndicators
							.get("EVENT_TRIGGER_LifeTime-L");
					if (customerState.subscriberTriggers != null
							&& customerState.subscriberTriggers.entrySet().size() > 0) {
						JsonElement eventHist = customerState.subscriberTriggers.get(eventId);
						if (eventHist != null) {
							JsonObject jEventHist = eventHist.getAsJsonObject();
							JsonElement thisEventHist = jEventHist.get(eventId);
							if (thisEventHist != null) {
								JsonArray histArr = thisEventHist.getAsJsonArray();
								JsonArray finalArray = new JsonArray();

								for (int i = 0; i < histArr.size(); i++) {
									JsonArray thisEntry = histArr.get(i).getAsJsonArray();
									if (!thisEntry.get(1).getAsString().equals(flowId))
										finalArray.add(thisEntry);
								} 
								jEventHist.add(eventId, finalArray);
								Commons.logger.info( Commons.logPrefix + "EventHistory Deleted. : " + customerState.subscriberTriggers.toString());
								Commons.pandaCache.set(customerState.customerId + "-Indicators", 0, customerState.subscriberIndicators.toString());
								return 0;
							}
						}
					}
					return 0;
				} else
					return 2;
			} else {
				String programId = customerState.tupleMap.get( "PROGRAM_CODE" ).getAsString();
				String offerId = customerState.tupleMap.get( "OFFER_CODE" ).getAsString();
				JsonObject offerIdObj = Commons.allOfferDef.get(offerId).getAsJsonObject();
				String category = offerIdObj.get("category").getAsString();
				JsonObject thisProgram = Commons.allProgramDef.get(programId);

				if (thisProgram == null)
					return 0;

				Commons.logger.info( Commons.logPrefix + "Processing RA for program and offer: "  + programId + " --- " + offerId );
				String channel = "SMS";
				if ( customerState.tupleMap.has( "CHANNEL_TYPE" ) )
					channel = customerState.tupleMap.get( "CHANNEL_TYPE" ).getAsString();
				String isSimulated = "false";
				if ( thisProgram.get( "status" ).getAsString().equals( "Simulation" ) )
					isSimulated = "true";

				String countAsContact = "Y";
				if( offerIdObj.has( "isCountAsContact" ) && offerIdObj.get( "isCountAsContact" ) != null && 
						! offerIdObj.get( "isCountAsContact" ).isJsonNull() ) {
					String expId = programId + "_" + offerId + "_" + "COUNT_AS_CONTACT";
					Object result = evaluateExpression( expId, null, false, eventId, customerState );
					if( result != null )
						countAsContact = result.toString();
				}

				customerState.tupleMap.addProperty( "OFFER_CATEGORY", category );
				customerState.tupleMap.addProperty( "OFFER_ID", offerId );
				customerState.tupleMap.addProperty( "CHANNEL", channel ); 
				customerState.tupleMap.addProperty( "PROGRAM_ID", programId );
				customerState.tupleMap.addProperty( "COUNT_FACTOR", "-1" );
				customerState.tupleMap.addProperty( "TRANSACTION_STATUS", "FAILED" );
				customerState.tupleMap.addProperty( "IS_SIMULATED", isSimulated );
				customerState.tupleMap.addProperty( "FULFILLMENT_PRODUCT_PRESENT", "true" );
				customerState.tupleMap.addProperty( "OFFER_TYPE", offerIdObj.get( "offerType" ).getAsString() );
				customerState.tupleMap.addProperty( "COUNT_AS_CONTACT", countAsContact );

				if (actionType != null && actionType.equals("OFFER_FAILED")) {
					Commons.logger.info( Commons.logPrefix + "Reducing the contact count : " + programId + " - " + offerId + " - " + flowId);

					// remove the program history entry
					customerState.tupleMap.addProperty( "CONTACT_TIME", customerState.tupleMap.get( "RECORD_TIMESTAMP" ).getAsString() );
					customerState.tupleMap.addProperty( "SIFT_INTERNAL_RECORD_TYPE", "ContactCount" ); // corrected
					// the
					// value
					customerState.tupleMap.addProperty( "LIVECHART_TYPE", "MonitoringCounts" );
					Commons.logger.info( Commons.logPrefix + "TupleMap Before Calling manageOfferContactCount Function  : "
							+ customerState.tupleMap.toString());

					evaluateExpression("manageOfferContactCount", null, false, null, customerState);
					evaluateExpression("manageLiveChartIndicator", null, false, null, customerState);

					JsonElement ele = customerState.subscriberIndicators.get("PROGRAMS_LifeTime-L");
					if (ele != null) {
						JsonObject progHist = ele.getAsJsonObject();
						JsonElement thisProgHist = progHist.get(programId);
						if (thisProgHist != null) {
							JsonArray thisProgHistArray = thisProgHist.getAsJsonArray();
							for (int i = 0; i < thisProgHistArray.size(); i++) {
								JsonObject thisProgHistInst = thisProgHistArray.get(i).getAsJsonObject();
								JsonElement thisOfferInsance = thisProgHistInst.get(offerId);
								if (thisOfferInsance != null) {
									JsonArray offerArray = thisOfferInsance.getAsJsonArray();
									for (int j = offerArray.size() - 1; j >= 0; j--) {
										JsonObject thisAction = offerArray.get(j).getAsJsonObject();
										if (thisAction.has("flowId")
												&& thisAction.get("flowId").getAsString().equals(flowId)) {
											Commons.logger.info( Commons.logPrefix + 
													"Removing Program History for : " + programId + " - " + offerId
													+ " - " + flowId);
											if (thisAction.has("monitoringAction"))
												thisAction.remove("monitoringAction");
											else
												thisAction.remove("notificationAction");

											break;
										}
									}
								}
							}
						}
					}

					// remove the taglist entry
					Iterator<Entry<String, JsonElement>> itr = customerState.subscriberEventTags.entrySet().iterator();
					while (itr.hasNext()) {
						Entry<String, JsonElement> element = itr.next();
						if (element.getKey().equals("docType"))
							continue;
						JsonObject aTag = element.getValue().getAsJsonObject();
						if (!aTag.has("FLOW_ID") || aTag.get("FLOW_ID").getAsString().equals(flowId)) {
							Commons.logger.info( Commons.logPrefix + 
									"Removing TagList for : " + programId + " - " + offerId + " - " + flowId);
							itr.remove();
						}
					}
					Commons.pandaCache.set("EventTagList-" + customerState.customerId, 0,
							customerState.subscriberEventTags.toString());
					commonsState.eventsTaggedForSubscribersMap.put(customerState.customerId,
							customerState.subscriberEventTags);
					Commons.pandaCache.set(customerState.customerId + "-Indicators", 0,
							customerState.subscriberIndicators.toString());
					return 0;
				} else {
					if (actionType != null && actionType.equals("FULFILLMENT_FAILED")) {
						Commons.logger.info( Commons.logPrefix +  "Deleting Fulfillment History : "
								+ customerState.customerId + " - " + programId + " - " + offerId);
						JsonElement thisProgramHistory = customerState.subscriberPrograms.get(programId);
						if (thisProgramHistory == null)
							return 0;
						JsonArray thisProgramHistoryArray = thisProgramHistory.getAsJsonArray();
						JsonObject latestProgramInstance = thisProgramHistoryArray
								.get(thisProgramHistoryArray.size() - 1).getAsJsonObject();
						JsonElement jele = latestProgramInstance.get(offerId);
						if (jele == null)
							return 0;
						JsonArray offerHistory = jele.getAsJsonArray();
						JsonObject latestOffer = offerHistory.get(offerHistory.size() - 1).getAsJsonObject();
						latestOffer.remove("fulfillmentAction");

						Commons.logger.info( Commons.logPrefix + 
								"Reducing the contact count : " + programId + " - " + offerId + " - " + flowId);
						customerState.tupleMap.addProperty( "CONTACT_TIME", customerState.tupleMap.get( "TIMESTAMP" ).getAsString() );
						customerState.tupleMap.addProperty( "SIFT_INTERNAL_RECORD_TYPE", "FulfillmentCount" );
						customerState.tupleMap.addProperty( "LIVECHART_TYPE", "FulfillmentCounts" );
						Commons.logger.info( Commons.logPrefix + 
								"TupleMap Before Calling managefulfilmentCount Function  : "
								+ customerState.tupleMap.toString());
						evaluateExpression("manageFulfillmentContactCount", null, false, null, customerState);

						// synchronized( this.getClass() ) {
						evaluateExpression("manageLiveChartIndicator", null, false, null, customerState);
						// }

						Commons.pandaCache.set(customerState.customerId + "-Indicators", 0,
								customerState.subscriberIndicators.toString());
						return 0;
					}
					Commons.logger.info( Commons.logPrefix + "RA received with action type: "  + actionType );
				}
				return 2;
			}
		}
		return lifeCycleRC;
	}

	public static void main(String arg[]) throws Exception {
		/*PandamaticOperatorCaller thisOp = new PandamaticOperatorCaller();
		thisOp.initialize("http://127.0.0.1:8091/pools");

		HashMap<String, String> tupleMap = new HashMap<String, String>();

		long lCallDate = new java.util.Date().getTime();
		Date thisRecordDate = new java.util.Date(lCallDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(thisRecordDate);


		long time = 1512411840000l;
		thisOp.handleScheduledReminder( time, "RE", thisRecordDate, cal );*/

		/*String msisdn = "37810639040";
		String recordType = "PROFILE360";
		tupleMap.put("RECORD_TYPE", recordType);
		tupleMap.put("MSISDN", msisdn);
		tupleMap.put("CUSTOMER_ID", msisdn);
		tupleMap.put("RECORD_TIMESTAMP", new Long(SiftMath.getCurrentTime()).toString());
		thisOp.process(msisdn, tupleMap);*/
	}

	public synchronized void process(String subscriber, HashMap<String, String> tupleMap ) throws Exception {
		try {
			java.util.Date thisRecordDate = null;
			String serviceType = tuple.get( "RECORD_TYPE" ).getAsString();
			String scallDate = tuple.get( "RECORD_TIMESTAMP" ).getAsString();
			long lCallDate = Long.parseLong( scallDate );
			thisRecordDate = new java.util.Date( lCallDate );
			Calendar cal = Calendar.getInstance();
			cal.setTime( thisRecordDate );

			SimpleDateFormat dtFormat = new SimpleDateFormat( "YYYY-MM-dd-HH:mm:ss" );
			dtFormat.setTimeZone( TimeZone.getTimeZone("GMT") );
			Commons.logPrefix = "[" + subscriber + "-" + dtFormat.format( lCallDate ) + "]  ";

			Commons.logger.info( Commons.logPrefix + "Processing now for msisdn : " + subscriber + " Tuple : " + tuple.toString());

			CustomerState customerState = getIndicatorsEventsTags(subscriber, tuple, thisRecordDate, serviceType, cal);

			// STEROID-GOLDBISCUIT
			int lifeCycleRC = processLifeCycleEvents(customerState);
			Commons.logger.info( Commons.logPrefix + "processLifeCycleEvents result : "  + lifeCycleRC );

			if ( lifeCycleRC == 0 ) 
				return;
			if ( lifeCycleRC == 2 ) {
				processAllIndicators( customerState );
				processAllEvents( customerState );
			}

			saveSubscriberIndicators( customerState, serviceType, subscriber );

			Commons.logger.info( Commons.logPrefix +  "Before Processing the next wave : " + customerState.subscriberEventTags.toString() );
			processNextWave( customerState, subscriber, tuple, thisRecordDate, serviceType, cal );

			// synchronized( Commons.numTuples ) {
			if ( commonsState.numTuples == 0 )
				commonsState.firstTupleTimestamp = new java.util.Date().getTime();
			else if ( commonsState.numTuples % Commons.telcoPersistInterval == 0 ) {
				logPerformanceStatistics();
				if ( Commons.flowFunnelFlag ) {
					for ( String key : commonsState.flowFunnelMap.keySet() )
						Commons.pandaCache.set( "FLOWFUNNEL_" + customerState.tupleMap.get( "REQUESTER_CHANNEL" ) + "_" + key + "-" + commonsState.nWorkerId, 0,
								commonsState.flowFunnelMap.get( key ).toString() );
				}
				commonsState.numTuples = 0;
			}
			commonsState.numTuples++;
		}
		catch( Exception ex ) {
			ex.printStackTrace();
			System.out.println( Commons.logPrefix + "General failure found in Sift: " + ( tuple.toString() ) );
			throw new NullPointerException();
		}	
	}

}