package com.knowesis.sift.core.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.Properties;

import org.apache.storm.Config;

import com.knowesis.sift.Common.PandaCache;
import com.knowesis.sift.Common.PandaCacheConfig;

public class SiftUtils {

	public static Config loadConfigProperties( Config config ) {
		Properties properties = new Properties();
		InputStream configInput = null;
		try {
			configInput = new FileInputStream( "resources/config.cfg" );
			// load a properties file
			properties.load( configInput );
			config.put( "NO_OF_SPLITS", properties.getProperty( "NO_OF_SPLITS" ) );
			config.put( "MESSAGE_SOURCE_QUEUES", properties.getProperty( "CONFIG_UPDATE_QUEUES" ) );
			config.put( "MESSAGE_SOURCE_URLS", properties.getProperty( "CONFIG_UPDATE_URLS" ) );
			config.put( "MESSAGE_SOURCE_QUEUETYPE", properties.getProperty( "CONFIG_UPDATE_QUEUETYPE" ) );
			config.put( "MESSAGE_SOURCE_MESSAGETYPE", properties.getProperty( "CONFIG_UPDATE_MESSAGETYPE" ) );
			config.put( "MESSAGE_SOURCE_USERNAME", properties.getProperty( "CONFIG_UPDATE_USERNAME" ) );
			config.put( "MESSAGE_SOURCE_PWD", properties.getProperty( "CONFIG_UPDATE_PWD" ) );
		} catch ( IOException ex ) {
			ex.printStackTrace();
		}	
		return config;
	}

	public static PandaCache getPandaCacheClient( String[ ] strPersistAddress ) throws URISyntaxException {
		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < strPersistAddress.length; i++ ) {
			uris.add( new URI( strPersistAddress[ i ] ) );
		}
		return new PandaCache( uris );
	}
	
	public static PandaCacheConfig getPandaCacheConfigClient( String[ ] strPersistAddress ) throws URISyntaxException {
		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < strPersistAddress.length; i++ ) {
			uris.add( new URI( strPersistAddress[ i ] ) );
		}
		return new PandaCacheConfig( uris );
	}

}
