package com.knowesis.sift.core.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.PandaCacheConfig;

public class GenerateSiftCoreYaml {

	private FileWriter fileWriter = null;
	private FileWriter fileSinksWriter = null;
	private BufferedWriter bufferedWriter = null;
	private BufferedWriter bufferedSinksWriter = null;
	private List< String > spoutsList = new ArrayList< String >();
	private Properties properties = null;
	private String sourceName = null;
	private PandaCacheConfig pandaCacheConfig = null;
	private String integration = null;

	static ActiveMQConnectionFactory connectionFactoryList ;
	static Connection myConnection;

	static Session session;
	static MessageConsumer consumer;
	static JsonParser parser = null;

	private List< String > allSinks = null;
	private List< String > bufferSinks = null;

	public static void main( String[] args ) throws IOException, JMSException {

		if( args.length < 2 ) {
			System.out.println( "USAGE: java -jar GenerateSiftYaml <config.cfg Path> <YamlOutputPath>" );
			System.exit( 0 );
		}

		String configPath = args[ 0 ];
		String yamlOutputPath = args[ 1 ];

		File file = new File( configPath );
		FileInputStream fileInput = new FileInputStream( file );

		GenerateSiftCoreYaml thisObject = new GenerateSiftCoreYaml();
		thisObject.properties = new Properties();
		thisObject.properties.load( fileInput );
		thisObject.initialise( thisObject.properties.getProperty( "PERSIST_ADDRESS" ) );
		fileInput.close();
		thisObject.sourceName = "Sift-topology";

		thisObject.generateYaml( yamlOutputPath );
		System.out.println( "*******" + yamlOutputPath + "/" + thisObject.sourceName + ".yaml generated successfully..*******" );
		System.exit( 0 );
	}


	public GenerateSiftCoreYaml() {
	}

	public void initialise( String persistAddress ) throws IOException {

		String[] strPersistAddress = persistAddress.split(",");
		initializeCouchClient( strPersistAddress );
		parser = new JsonParser();
	}

	public void initializeCouchClient( String[] strPersistAddress ){

		LinkedList< URI > uris = new LinkedList< URI >();
		for ( int i = 0; i < strPersistAddress.length; i ++ ) {
			try {
				uris.add( new URI( strPersistAddress[ i ] ) );
			} catch ( URISyntaxException e ) {
				e.printStackTrace();
			}
		}
		pandaCacheConfig = new PandaCacheConfig( uris );
	}



	public StringBuilder generateComponent( HashMap< String, String > params, HashMap< String, String > paramTypes, String sourceName, String className, String configPlacement ) {
		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append( "   - id: " + "\"" + sourceName + "\"\n" );
		stringBuilder.append( "     className: \"" + className + "\"\n" );

		stringBuilder = addProperties( params, paramTypes, stringBuilder );
		stringBuilder = addConfig( configPlacement, stringBuilder );

		return stringBuilder;
	}


	public StringBuilder addAllSiftStreams() {

		StringBuilder stringBuilder = new StringBuilder();

		System.out.println( "Adding Stream: SiftConfigStream" );
		stringBuilder.append( addStreamProperties( "SiftConfigStream", "Sift_Config_Update", "PandamaticOperatorCaller", "ALL", null ) );

		System.out.println( "Adding Stream: Sift_Source_Data" );
		stringBuilder.append( addStreamProperties( "Sift_Source_Data", "DataSource_Consumer", "PandamaticOperatorCaller", "CUSTOM", null ) );

		System.out.println( "Adding Stream: BeaconSourceStream" );
		stringBuilder.append( addStreamProperties( "BeaconSourceStream", "Scheduled_Trigger", "PandamaticOperatorCaller", "ALL", null ) );

		if( properties.getProperty( "PANDAMATIC_TELCO_REQUIRED" ).equals( "Y" ) ) {
			System.out.println( "Adding Stream: SiftConfigStreamTelco" );
			stringBuilder.append( addStreamProperties( "SiftConfigStreamTelco", "Sift_Config_Update", "PandamaticOperatorTelco", "ALL", null ) );

			System.out.println( "Adding Stream: Sift_Source_Telco" );
			stringBuilder.append( addStreamProperties( "Sift_Source_Telco", "PandamaticOperatorCaller", "PandamaticOperatorTelco", "SHUFFLE", "telcorecord" ) );
		}

		System.out.println( "Adding Stream: SiftMessageSink" );
		stringBuilder.append( addStreamProperties( "SiftMessageSink", "PandamaticOperatorCaller", "ConfigurableSinks", "SHUFFLE", "record" ) );

		System.out.println( "Adding Stream: Sift_Config_Update_Sink_" + sourceName );
		stringBuilder.append( addStreamProperties( "Sift_Config_Update_Sink_" + sourceName, "Sift_Config_Update", "Sift_Config_Update_Sink_" + sourceName, "ALL", null ) );

		for( int i = 0; i < allSinks.size(); i ++ ) {
			String sinkName = allSinks.get( i );	
			System.out.println( "Adding Stream: SiftMessageSink_" + sinkName );
			stringBuilder.append( addStreamProperties( "SiftMessageSink_" + sinkName, "ConfigurableSinks", allSinks.get( i ), "SHUFFLE", sinkName ) );
		}
		return stringBuilder;
	}


	public StringBuilder addStreamProperties( String name, String from, String to, String grouping, String streamId ) {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append( "          - name: \"" + name + "\"\n" );
		stringBuilder.append( "            from: \"" + from + "\"\n" );
		stringBuilder.append( "            to: \"" + to + "\"\n" );
		stringBuilder.append( "            grouping:" + "\n" );
		stringBuilder.append( "               type: " + grouping + "\n" );
		if( streamId != null && grouping.equals( "SHUFFLE" ) )
			stringBuilder.append( "               streamId: \"" + streamId + "\"\n" );
		else if( streamId != null && grouping.equals( "FIELDS" ) )
			stringBuilder.append( "               args: [\"" + streamId + "\"]\n" );
		else if( grouping.equals( "CUSTOM" ) ) {
			stringBuilder.append( "               customClass:\n" );
			stringBuilder.append( "                 className: \"com.knowesis.sift.core.HashGrouping\"\n" );
		}

		stringBuilder.append( "\n\n" );

		return stringBuilder;
	}


	public void generateYaml( String yamlOutputPath ) throws IOException {

		allSinks = new ArrayList< String >();
		bufferSinks = new ArrayList< String >();
		bufferSinks.add( "buffer1" );
		bufferSinks.add( "buffer2" );

		fileWriter = new FileWriter( yamlOutputPath + "/" + sourceName + ".yaml" );
		bufferedWriter = new BufferedWriter( fileWriter );

		fileSinksWriter = new FileWriter( yamlOutputPath + "/" + sourceName + "_Sinks" );
		bufferedSinksWriter = new BufferedWriter( fileSinksWriter );

		bufferedWriter.write( "name: \"" + sourceName + "\"\n" );
		bufferedWriter.write( "# Spout Definitions:\n" );
		bufferedWriter.write( "spouts:\n" );
		addAllSiftSpouts();

		bufferedWriter.write( "# Bolt Definitions:\n" );
		bufferedWriter.write( "bolts:\n" );
		addAllSiftBolts();

		bufferedWriter.write( "# Stream Definitions:\n" );
		bufferedWriter.write( "streams:\n" );
		bufferedWriter.write( addAllSiftStreams().toString() );

		bufferedWriter.flush();
	}

	public void addAllSiftSpouts() throws IOException {

		JsonObject thisSpoutObject = null;

		// 1. Config update Spout.
		thisSpoutObject = new JsonObject();
		thisSpoutObject.addProperty( "type", "JMS" );
		thisSpoutObject.addProperty( "queueType", properties.getProperty( "CONFIG_UPDATE_QUEUE_TYPES" ) );
		thisSpoutObject.addProperty( "brokerURL", properties.getProperty( "CONFIG_UPDATE_BROKER_URLS" ) );
		thisSpoutObject.addProperty( "queueNames", properties.getProperty( "CONFIG_UPDATE_QUEUE_NAMES" ) );
		thisSpoutObject.addProperty( "messageType", properties.getProperty( "CONFIG_UPDATE_MESSAGE_TYPES" ) );
		thisSpoutObject.addProperty( "jmsuserName", properties.getProperty( "CONFIG_UPDATE_USERNAMES" ) );
		thisSpoutObject.addProperty( "jmspassword", properties.getProperty( "CONFIG_UPDATE_PASSWORDS" ) );		
		addSpout( thisSpoutObject, "Sift_Config_Update", "" );


		integration = properties.getProperty( "INTEGRATION_POINT", "AMQ" );
		if( integration.equalsIgnoreCase( "KAFKA" ) ) {
			// 2. Kafka Source Data Consumers Spout.
			thisSpoutObject = new JsonObject();
			thisSpoutObject.addProperty( "type", "KAFKA" );
			thisSpoutObject.addProperty( "groupId", "SiftDSConsumer" );
			thisSpoutObject.addProperty( "hostUrl", properties.getProperty( "KAFKA_SOURCE_HOSTURL" ) );
			thisSpoutObject.addProperty( "clientId", properties.getProperty( "KAFKA_SOURCE_CLIENTID" ) );
			thisSpoutObject.addProperty( "pollInterval", "100" );
			thisSpoutObject.addProperty( "topic", properties.getProperty( "KAFKA_SOURCE_TOPIC" ) );
			thisSpoutObject.addProperty( "readerType", "DS" );
			addSpout( thisSpoutObject, "DataSource_Consumer", "" );
		}
		else if( integration.equalsIgnoreCase( "AMQ" ) ) {
			
			thisSpoutObject = new JsonObject();
			thisSpoutObject.addProperty( "type", "JMS" );
			thisSpoutObject.addProperty( "queueType", "AMQ" );
			thisSpoutObject.addProperty( "brokerURL", "tcp://52.187.111.161:61616" );
			thisSpoutObject.addProperty( "queueNames", "sift.dataIntegration.in" );
			thisSpoutObject.addProperty( "messageType", "text" );
			thisSpoutObject.addProperty( "jmsuserName", "admin" );
			thisSpoutObject.addProperty( "jmspassword", "admin" );
			addSpout( thisSpoutObject, "DataSource_Consumer", "" );
		}

		// 3. Beacon Source Scheduled Trigger Spout.
		thisSpoutObject = new JsonObject();
		thisSpoutObject.addProperty( "type", "BEACON" );
		addSpout( thisSpoutObject, "Scheduled_Trigger", "" );		
	}


	public void addAllSiftBolts() throws IOException {
		// 1. Configurable Sinks Bolt..
		HashMap< String, String > params = new HashMap< String, String >();
		HashMap< String, String > paramTypes = new HashMap< String, String >();

		System.out.println( "Adding Bolt: ConfigurableSinks" );
		bufferedWriter.write( generateComponent( params, paramTypes, "ConfigurableSinks", "com.knowesis.sift.core.sinks.ConfigurableSinksHandler", null ).toString() );

		// 2. PandamaticOperatorCaller..
		params = new HashMap< String, String >();
		paramTypes = new HashMap< String, String >();
		params.put( "strPersistAddressList", properties.getProperty( "PERSIST_ADDRESS" ) );
		paramTypes.put( "strPersistAddressList", "String" );

		params.put( "siftMode", "Full" );
		paramTypes.put( "siftMode", "String" );

		params.put( "cacheSize", properties.getProperty( "CACHE_SIZE" ) );
		paramTypes.put( "cacheSize", "int" );

		params.put( "memoryLoad", properties.getProperty( "PANDAMATICOPERATOR_MEMORY_LOAD" ) );
		paramTypes.put( "memoryLoad", "int" );

		System.out.println( "Adding Bolt: PandamaticOperatorCaller" );
		bufferedWriter.write( generateComponent( params, paramTypes, "PandamaticOperatorCaller", "com.knowesis.sift.core.PandamaticOperatorCaller", properties.getProperty( "CORE_DISTRIBUTION" ) ).toString() );


		// 3. Configurable sinks..
		String configSinksString = ( String ) pandaCacheConfig.get( "ConfigurableSinks" );
		JsonObject configurableSinks = ( JsonObject ) parser.parse( configSinksString );
		Iterator< Entry< String, JsonElement > > iterator = configurableSinks.entrySet().iterator();
		while( iterator.hasNext() ) {
			Entry< String, JsonElement > thisEntry = iterator.next();
			String thisSinkName = thisEntry.getKey();
			JsonObject thisSink = thisEntry.getValue().getAsJsonObject();

			bufferedSinksWriter.write( thisSinkName + "\n" );
			if( ! thisSinkName.equals( "JMSLB1" ) && ! thisSinkName.equals( "FS1" ) ) {
				thisSinkName = bufferSinks.get( 0 );
				bufferSinks.remove( 0 );
			}

			allSinks.add( thisSinkName );
			bufferedWriter.write( addSink( thisSink, thisSinkName ).toString() );
		}	
		bufferedSinksWriter.flush();

		// 4. PandamaticOperatorTelco..
		if( properties.getProperty( "PANDAMATIC_TELCO_REQUIRED" ).equals( "Y" ) ) {
			params = new HashMap< String, String >();
			paramTypes = new HashMap< String, String >();
			params.put( "strPersistAddressList", properties.getProperty( "PERSIST_ADDRESS" ) );
			paramTypes.put( "strPersistAddressList", "String" );

			System.out.println( "Adding Bolt: PandamaticOperatorTelco" );
			bufferedWriter.write( generateComponent( params, paramTypes, "PandamaticOperatorTelco", "com.knowesis.sift.core.PandamaticOperatorTelco", properties.getProperty( "TELCO_DISTRIBUTION" ) ).toString() );
		}

		// 5. Kafka Sink for Config update to Megatrons.
		params = new HashMap< String, String >();
		paramTypes = new HashMap< String, String >();

		params.put( "hostUrl", properties.getProperty( "KAFKA_SOURCE_HOSTURL" ) );
		paramTypes.put( "hostUrl", "String" );

		params.put( "clientId", "CONFIG_UPDATE_SINK_CLIENT_" + sourceName );
		paramTypes.put( "clientId", "String" );

		params.put( "topic", "SiftConfigUpdate" );
		paramTypes.put( "topic", "String" );

		bufferedWriter.write( generateComponent( params, paramTypes, "Sift_Config_Update_Sink_" + sourceName, "com.knowesis.sift.core.sinks.KafkaSink", null ).toString() );
	}


	public StringBuilder addSink( JsonObject thisJsonObject, String sinkName ) {
		String thisDSType = thisJsonObject.get( "type" ).getAsString();
		HashMap< String, String > params = new HashMap< String, String >();
		HashMap< String, String > paramTypes = new HashMap< String, String >();
		StringBuilder thisBuilder = new StringBuilder();

		if( thisDSType.equals( "JMS" ) ) {

			params.put( "destinationType", thisJsonObject.get( "destinationType" ).getAsString() );
			paramTypes.put( "destinationType", "String" );

			params.put( "brokerURL", thisJsonObject.get( "brokerURL" ).getAsString() );
			paramTypes.put( "brokerURL", "String" );

			params.put( "queueNames", thisJsonObject.get( "queueNames" ).getAsString() );
			paramTypes.put( "queueNames", "String" );

			params.put( "messageType", thisJsonObject.get( "messageType" ).getAsString() );
			paramTypes.put( "messageType", "String" );

			params.put( "numberOfTimesToTryToReconnect", thisJsonObject.get( "numOfTimesToTryToReconnect" ).getAsString() );
			paramTypes.put( "numberOfTimesToTryToReconnect", "int" );

			params.put( "numberOfTimesToTryToSendMessage", thisJsonObject.get( "numberOfTimesToTryToSendMessage" ).getAsString() );
			paramTypes.put( "numberOfTimesToTryToSendMessage", "int" );

			params.put( "outputFileDir", thisJsonObject.get( "outputFileDirectory" ).getAsString() );
			paramTypes.put( "outputFileDir", "String" );

			params.put( "numberOfRecords", thisJsonObject.get( "numberOfRecordsToWriteInFile" ).getAsString() );
			paramTypes.put( "numberOfRecords", "int" );

			params.put( "writeToFile", thisJsonObject.get( "writeToFile" ).getAsString() );
			paramTypes.put( "writeToFile", "boolean" );

			params.put( "pushMessagesFromFile", thisJsonObject.get( "pushMessagesFromFile" ).getAsString() );
			paramTypes.put( "pushMessagesFromFile", "boolean" );

			params.put( "reconnectionInterval", thisJsonObject.get( "reconnectionInterval" ).getAsString() );
			paramTypes.put( "reconnectionInterval", "int" );

			System.out.println( "Adding Bolt: " + sinkName );

			thisBuilder = generateComponent( params, paramTypes, sinkName, "com.knowesis.sift.core.sinks.JMSLoadBalancerSink", null );
			return thisBuilder;
		}
		else if( thisDSType.equals( "FILE" ) ) {

			params.put( "filesinkPath", thisJsonObject.get( "filesinkPath" ).getAsString() );
			paramTypes.put( "filesinkPath", "String" );

			params.put( "recordsPerFile", thisJsonObject.get( "recordsPerFile" ).getAsString() );
			paramTypes.put( "recordsPerFile", "int" );

			System.out.println( "Adding Bolt: " + sinkName );

			thisBuilder = generateComponent( params, paramTypes, sinkName, "com.knowesis.sift.core.sinks.FileSink", null );
			return thisBuilder;
		}
		return thisBuilder;
	}

	public StringBuilder addProperties( HashMap< String, String > params, HashMap< String, String > paramTypes, StringBuilder thisBuilder ) {
		if( params.isEmpty() )
			return thisBuilder;

		thisBuilder.append( "     properties: \n" );
		Iterator< Entry< String, String > > iterator = params.entrySet().iterator();
		while( iterator.hasNext() ) {
			Entry< String, String > thisEntry = iterator.next();
			thisBuilder.append( "          - name: \"" + thisEntry.getKey()  +"\"\n" );
			if( paramTypes.get( thisEntry.getKey() ).equals( "int" ) || paramTypes.get( thisEntry.getKey() ).equals( "boolean" )  )
				thisBuilder.append( "            value: " + thisEntry.getValue() + "\n\n" );
			else
				thisBuilder.append( "            value: \"" + thisEntry.getValue() + "\"\n\n" );
		}

		return thisBuilder;
	}

	public StringBuilder addConfig( String configPlacement, StringBuilder thisBuilder ) {

		if( configPlacement == null ) {
			thisBuilder.append( "     parallelism: 1\n\n" );
			return thisBuilder;
		}

		String[] hostsArray = configPlacement.split( "," );
		int thisParallelism = 0;
		for( int i = 0; i < hostsArray.length; i ++ ) {
			String thisHost = hostsArray[ i ];
			String[] thisConfig = thisHost.split( ":" );
			thisParallelism += Integer.parseInt( thisConfig[ 1 ] );
		}

		thisBuilder.append( "     configMethods: \n" );
		thisBuilder.append( "          - name: \"setConfig\"\n" );
		thisBuilder.append( "            args:\n" );
		thisBuilder.append( "                 - \"" + configPlacement + "\"\n\n" );
		thisBuilder.append( "     parallelism: " + thisParallelism + "\n\n" );
		return thisBuilder;
	}


	public void addSpout( JsonObject dsProperty, String sourceName, String sourceNamePrefix ) throws IOException {

		HashMap< String, String > params = new HashMap< String, String >();
		HashMap< String, String > paramTypes = new HashMap< String, String >();

		String thisDSType = dsProperty.get( "type" ).getAsString();
		StringBuilder thisBuilder = new StringBuilder();

		if( thisDSType.equals( "JMS" ) ) {

			params.put( "queueType", dsProperty.get( "queueType" ).getAsString() );
			paramTypes.put( "queueType", "String" );

			params.put( "brokerURL", dsProperty.get( "brokerURL" ).getAsString() );
			paramTypes.put( "brokerURL", "String" );

			params.put( "queueNames", dsProperty.get( "queueNames" ).getAsString() );
			paramTypes.put( "queueNames", "String" );

			params.put( "messageType", dsProperty.get( "messageType" ).getAsString() );
			paramTypes.put( "messageType", "String" );

			params.put( "userName", dsProperty.get( "jmsuserName" ).getAsString() );
			paramTypes.put( "userName", "String" );

			params.put( "password", dsProperty.get( "jmspassword" ).getAsString() );
			paramTypes.put( "password", "String" );

			System.out.println( "Adding Spout: " + sourceNamePrefix + sourceName );

			thisBuilder = generateComponent( params, paramTypes, sourceNamePrefix + sourceName, "com.knowesis.sift.core.sources.MessagingQueueSource", null );
			spoutsList.add( sourceNamePrefix + sourceName );
			bufferedWriter.write( thisBuilder.toString() );
		}
		else if( thisDSType.equals( "FILE" ) ) {

			/*
			params.put( "inputDirectory", dsProperty.get( "inputDirectory" ).getAsString() );
			paramTypes.put( "inputDirectory", "String" );

			params.put( "compressionType", dsProperty.get( "compressionType" ).getAsString() );
			paramTypes.put( "compressionType", "String" );

			params.put( "fileNamePattern", dsProperty.get( "fileNamePattern" ).getAsString() );
			paramTypes.put( "fileNamePattern", "String" );

			params.put( "validationNeeded", dsProperty.get( "validationNeeded" ).getAsString() );
			paramTypes.put( "validationNeeded", "boolean" );

			params.put( "validPattern", dsProperty.get( "validPattern" ).getAsString() );
			paramTypes.put( "validPattern", "String" );

			System.out.println( "Adding Spout: " + sourceNamePrefix + sourceName + "_DirectoryScan" );
			thisBuilder = generateComponent( params, paramTypes, sourceNamePrefix + sourceName + "_DirectoryScan", 1, "com.knowesis.sift.core.sources.DirectoryScan" );
			//spoutsList.add( sourceName + "_" + index );
			bufferedWriter.write( thisBuilder.toString() );


			params.remove( "inputDirectory" );
			paramTypes.remove( "inputDirectory" );

			params.remove( "compressionType" );
			paramTypes.remove( "compressionType" );

			params.remove( "fileNamePattern" );
			paramTypes.remove( "fileNamePattern" );

			params.put( "processedDirectory", dsProperty.get( "processedDirectory" ).getAsString() );
			paramTypes.put( "processedDirectory", "String" );

			params.put( "deleteFile", dsProperty.get( "deleteFile" ).getAsString() );
			paramTypes.put( "deleteFile", "boolean" );

			StringBuilder stringBuilder = generateComponent( params, paramTypes, sourceNamePrefix + sourceName + "_FileSource", numberOfParallelProcess, "com.knowesis.sift.core.sources.FileSource" );
			fileBolts.put( sourceNamePrefix + sourceName + "_FileSource", stringBuilder );
			directoryFileMapping.put( sourceNamePrefix + sourceName + "_DirectoryScan", sourceNamePrefix + sourceName + "_FileSource" );
			 */
		}
		else if( thisDSType.equals( "KAFKA" ) ) {

			params.put( "groupId", dsProperty.get( "groupId" ).getAsString() );
			paramTypes.put( "groupId", "String" );

			params.put( "hostUrl", dsProperty.get( "hostUrl" ).getAsString() );
			paramTypes.put( "hostUrl", "String" );

			params.put( "clientId", dsProperty.get( "clientId" ).getAsString() + "_SOURCE_" + sourceName );
			paramTypes.put( "clientId", "String" );

			params.put( "pollInterval", dsProperty.get( "pollInterval" ).getAsString() );
			paramTypes.put( "pollInterval", "int" );

			params.put( "topic", dsProperty.get( "topic" ).getAsString() );
			paramTypes.put( "topic", "String" );

			if( dsProperty.has( "readerType" ) && ! dsProperty.get( "readerType" ).isJsonNull() ) {
				params.put( "readerType", dsProperty.get( "readerType" ).getAsString() );
				paramTypes.put( "readerType", "String" );
			}

			String configPlacement = null;
			if( sourceName.startsWith( "DataSource_Consumer" ) ) {
				configPlacement = properties.getProperty( "KAFKA_SOURCE_DISTRIBUTION" );
			}

			System.out.println( "Adding Spout: " + sourceNamePrefix + sourceName );
			thisBuilder = generateComponent( params, paramTypes, sourceNamePrefix + sourceName, "com.knowesis.sift.core.sources.KafkaSource", configPlacement );
			spoutsList.add( sourceNamePrefix + sourceName );
			bufferedWriter.write( thisBuilder.toString() );
		}
		else if( thisDSType.equals( "BEACON" ) ) {

			System.out.println( "Adding Spout: " + sourceNamePrefix + sourceName );
			thisBuilder = generateComponent( params, paramTypes, sourceNamePrefix + sourceName, "com.knowesis.sift.core.sources.BeaconSource", null );
			spoutsList.add( sourceNamePrefix + sourceName );
			bufferedWriter.write( thisBuilder.toString() );
		}
	}

}