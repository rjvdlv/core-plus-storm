package com.knowesis.sift.core.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.knowesis.sift.Common.PandaCacheConfig;

public class GenerateSiftDataSourceYaml {

	private String persistAddress = null;
	private FileWriter fileWriter = null;
	private BufferedWriter bufferedWriter = null;
	private FileWriter filterSinksWriter = null;
	private BufferedWriter bufferedFilterSinksWriter = null;
	private PandaCacheConfig pandaCacheConfig = null;
	private List< String > spoutsList = new ArrayList< String >();
	private HashMap< Integer, List< String > > hostWiseSpouts = new HashMap< Integer, List< String > >();
	private HashMap< String, StringBuilder > fileBolts = new HashMap< String, StringBuilder >();
	private HashMap< String, String > directoryFileMapping = new HashMap< String, String >();
	private Properties properties = null;
	private String sourceName = null;
	private String cimMapping = null;
	private String dataSourceName = null;
	private JsonArray allDestinations = null;
	private String integration = null;

	static ActiveMQConnectionFactory connectionFactoryList ;
	static Connection myConnection;

	static Session session;
	static Queue queue ;
	static Queue resultQueue;
	static MessageConsumer consumer;
	static JsonParser parser = null;

	private String hosts = null;
	private String[] hostsArray = null;
	private String parallelProcesses = null;
	private String[] parallelProcessesArray = null;
	private String MegaTronProcesses = null;
	private String[] MegaTronProcessesArray = null;
	private int numberOfParallelProcess = 0;

	public static void main( String[] args ) throws IOException, JMSException {

		if( args.length < 5 ) {
			System.out.println( "USAGE: java -jar GenerateSiftYaml <persistAddress> <activeMQAddress> <config.cfg Path> <YamlOutputPath> <addSourceScript>" );
			System.exit( 0 );
		}

		String persistAddress = args[ 0 ];
		String activeMQAddress = args[ 1 ];
		String configPath = args[ 2 ];
		String yamlOutputPath = args[ 3 ];
		String addSourceScript = args[ 4 ];

		GenerateSiftDataSourceYaml thisObject = new GenerateSiftDataSourceYaml( persistAddress, activeMQAddress );

		thisObject.properties = new Properties();
		File file = new File( configPath );
		FileInputStream fileInput = new FileInputStream( file );
		thisObject.properties.load( fileInput );
		fileInput.close();

		String mode;
		String docID;

		int counter = 0;
		while ( true ) {
			MapMessage message = ( MapMessage ) consumer.receive( 60 * 1000 * 10 );

			if( message == null )
				continue;

			if( counter != 0 )
				thisObject.resetData();

			counter ++;

			docID = message.getString( "id" );
			String[] DSDocId = docID.split( "DATA_SOURCE_" );
			thisObject.sourceName = DSDocId[ 1 ];
			thisObject.dataSourceName = docID;
			mode = message.getString( "mode" );

			System.out.println( "DocID: " + docID );
			System.out.println( "mode: " + mode );
			System.out.println( "source: " + thisObject.sourceName );

			if( mode.equals( "1" ) || mode.equals( "3" ) ) {
				String sDataSourceObj = ( String ) thisObject.pandaCacheConfig.get( docID );

				if ( sDataSourceObj == null ) {
					System.out.println( "WARNING: Data Source information not found.. Please validate and submit again.." );
					continue;
				}

				JsonObject dataSourceObj = ( JsonObject ) parser.parse( sDataSourceObj );
				thisObject.cimMapping = dataSourceObj.get( "cimMapping" ).getAsString();
				thisObject.hosts = dataSourceObj.get( "host" ).getAsString();
				thisObject.hostsArray = thisObject.hosts.trim().split( "," );
				thisObject.parallelProcesses = dataSourceObj.get( "numberOfParallelProcess" ).getAsString();
				thisObject.parallelProcessesArray = thisObject.parallelProcesses.trim().split( "," );


				// TODO: Change this to megatron processes.
				thisObject.MegaTronProcesses = dataSourceObj.get( "numberOfParallelProcess" ).getAsString();
				thisObject.MegaTronProcessesArray = thisObject.MegaTronProcesses.trim().split( "," );
				String[] parallelArray = thisObject.parallelProcesses.split( "," );

				thisObject.numberOfParallelProcess = 0;
				for( int i = 0; i < parallelArray.length; i ++ )
					thisObject.numberOfParallelProcess += Integer.parseInt( parallelArray[ i ] );

				thisObject.generateYaml( dataSourceObj, thisObject.sourceName, yamlOutputPath );
				System.out.println( "*******" + yamlOutputPath + "/" + thisObject.sourceName + ".yaml generated successfully..*******" );
			}

			// Script that submits the topology..
			String command = "sh " + addSourceScript + " " + thisObject.sourceName + " " + mode;
			System.out.println( command );
			Process proc = Runtime.getRuntime().exec( command );
			try {
				proc.waitFor();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}


	public void resetData() {
		spoutsList = new ArrayList< String >();
		hostWiseSpouts = new HashMap< Integer, List< String > >();
		fileBolts = new HashMap< String, StringBuilder >();
		directoryFileMapping = new HashMap< String, String >();
		sourceName = null;
		hosts = null;
		parallelProcesses = null;
	}


	public GenerateSiftDataSourceYaml( String persistAddress, String activeMQAddress ) throws IOException, JMSException {

		String[] strPersistAddress = persistAddress.split(",");
		initializeCouchClient( strPersistAddress );
		initialiseActiveMq( activeMQAddress );
		this.persistAddress = persistAddress;
		parser = new JsonParser();
	}

	public GenerateSiftDataSourceYaml() {

	}


	public void initializeCouchClient( String[] strPersistAddress ){

		LinkedList< URI > uris = new LinkedList< URI >();
		for ( int i = 0; i < strPersistAddress.length; i ++ ) {
			try {
				uris.add( new URI( strPersistAddress[ i ] ) );
			} catch ( URISyntaxException e ) {
				e.printStackTrace();
			}
		}
		pandaCacheConfig = new PandaCacheConfig( uris );
	}




	static void initialiseActiveMq( String activeMQAddress ) throws JMSException{
		connectionFactoryList = new ActiveMQConnectionFactory( activeMQAddress );
		myConnection = connectionFactoryList.createConnection();
		myConnection.start();
		session = myConnection.createSession( false , Session.AUTO_ACKNOWLEDGE );
		queue = session.createQueue( "sift.admin.in" );
		resultQueue = session.createQueue( "sift.result.in" );
		consumer = session.createConsumer( queue );
	}




	public void addSpout( JsonObject dsProperty, String sourceName, String sourceNamePrefix, int index ) throws IOException {

		HashMap< String, String > params = new HashMap< String, String >();
		HashMap< String, String > paramTypes = new HashMap< String, String >();

		String thisDSType = dsProperty.get( "type" ).getAsString();
		StringBuilder thisBuilder = null;

		if( thisDSType.equals( "JMS" ) ) {

			params.put( "queueType", dsProperty.get( "queueType" ).getAsString() );
			paramTypes.put( "queueType", "String" );

			params.put( "brokerURL", dsProperty.get( "brokerURL" ).getAsString() );
			paramTypes.put( "brokerURL", "String" );

			params.put( "queueNames", dsProperty.get( "queueNames" ).getAsString() );
			paramTypes.put( "queueNames", "String" );

			params.put( "messageType", dsProperty.get( "messageType" ).getAsString() );
			paramTypes.put( "messageType", "String" );

			params.put( "userName", dsProperty.get( "jmsuserName" ).getAsString() );
			paramTypes.put( "userName", "String" );

			params.put( "password", dsProperty.get( "jmspassword" ).getAsString() );
			paramTypes.put( "password", "String" );

			for( int i = 0; i < hostsArray.length; i ++ ) {
				System.out.println( "Adding Spout: " + sourceNamePrefix + sourceName + "_" + index );
				thisBuilder = generateComponent( params, paramTypes, sourceNamePrefix + sourceName, index, numberOfParallelProcess, "com.knowesis.sift.core.sources.MessagingQueueSource", i );
				bufferedWriter.write( thisBuilder.toString() );
				spoutsList.add( sourceNamePrefix + sourceName + "_" + index + "_" + i );
				List< String > thisList = null;
				if( hostWiseSpouts.containsKey( i ) )
					thisList = hostWiseSpouts.get( i );
				else
					thisList = new ArrayList< String >();

				thisList.add( sourceNamePrefix + sourceName + "_" + index + "_" + i );
				hostWiseSpouts.put( i, thisList );
			}
		}
		else if( thisDSType.equals( "FILE" ) ) {
			params.put( "inputDirectory", dsProperty.get( "inputDirectory" ).getAsString() );
			paramTypes.put( "inputDirectory", "String" );

			params.put( "compressionType", dsProperty.get( "compressionType" ).getAsString() );
			paramTypes.put( "compressionType", "String" );

			params.put( "fileNamePattern", dsProperty.get( "fileNamePattern" ).getAsString() );
			paramTypes.put( "fileNamePattern", "String" );

			params.put( "validationNeeded", dsProperty.get( "validationNeeded" ).getAsString() );
			paramTypes.put( "validationNeeded", "boolean" );

			params.put( "validPattern", dsProperty.get( "validPattern" ).getAsString() );
			paramTypes.put( "validPattern", "String" );

			System.out.println( "Adding Spout: " + sourceNamePrefix + sourceName + "_DirectoryScan" + "_" + index );
			thisBuilder = generateComponent( params, paramTypes, sourceNamePrefix + sourceName + "_DirectoryScan", index, 1, "com.knowesis.sift.core.sources.DirectoryScan", 0 );
			//spoutsList.add( sourceName + "_" + index );
			bufferedWriter.write( thisBuilder.toString() );


			params.remove( "inputDirectory" );
			paramTypes.remove( "inputDirectory" );

			params.remove( "compressionType" );
			paramTypes.remove( "compressionType" );

			params.remove( "fileNamePattern" );
			paramTypes.remove( "fileNamePattern" );

			params.put( "processedDirectory", dsProperty.get( "processedDirectory" ).getAsString() );
			paramTypes.put( "processedDirectory", "String" );

			params.put( "deleteFile", dsProperty.get( "deleteFile" ).getAsString() );
			paramTypes.put( "deleteFile", "boolean" );

			for( int i = 0; i < hostsArray.length; i ++ ) {
				StringBuilder stringBuilder = generateComponent( params, paramTypes, sourceNamePrefix + sourceName + "_FileSource", index, numberOfParallelProcess, "com.knowesis.sift.core.sources.FileSource", i );
				fileBolts.put( sourceNamePrefix + sourceName + "_FileSource_" + index + "_" + i, stringBuilder );

				if( directoryFileMapping.containsKey( sourceNamePrefix + sourceName + "_DirectoryScan" + "_" + index + "_0" ) ) {
					String existingFileSources = directoryFileMapping.get( sourceNamePrefix + sourceName + "_DirectoryScan" + "_" + index + "_0" );
					directoryFileMapping.put( sourceNamePrefix + sourceName + "_DirectoryScan" + "_" + index + "_0", existingFileSources + "|" + sourceNamePrefix + sourceName + "_FileSource_" + index + "_" + i );
				}
				else
					directoryFileMapping.put( sourceNamePrefix + sourceName + "_DirectoryScan" + "_" + index + "_0", sourceNamePrefix + sourceName + "_FileSource_" + index + "_" + i );
			}

		}
		else if( thisDSType.equals( "KAFKA" ) ) {

			params.put( "groupId", dsProperty.get( "groupId" ).getAsString() );
			paramTypes.put( "groupId", "String" );

			params.put( "hostUrl", dsProperty.get( "hostUrl" ).getAsString() );
			paramTypes.put( "hostUrl", "String" );

			params.put( "clientId", dsProperty.get( "clientId" ).getAsString() + "_SOURCE_" + sourceName );
			paramTypes.put( "clientId", "String" );

			params.put( "pollInterval", dsProperty.get( "pollInterval" ).getAsString() );
			paramTypes.put( "pollInterval", "int" );

			params.put( "topic", dsProperty.get( "topic" ).getAsString() );
			paramTypes.put( "topic", "String" );

			if( dsProperty.has( "readerType" ) && ! dsProperty.get( "readerType" ).isJsonNull() ) {
				params.put( "readerType", dsProperty.get( "readerType" ).getAsString() );
				paramTypes.put( "readerType", "String" );
			}

			for( int i = 0; i < hostsArray.length; i ++ ) {
				System.out.println( "Adding Spout: " + sourceNamePrefix + sourceName + "_" + index );
				thisBuilder = generateComponent( params, paramTypes, sourceNamePrefix + sourceName, index, numberOfParallelProcess, "com.knowesis.sift.core.sources.KafkaSource", i );
				bufferedWriter.write( thisBuilder.toString() );
				spoutsList.add( sourceNamePrefix + sourceName + "_" + index + "_" + i );
				List< String > thisList = null;
				if( hostWiseSpouts.containsKey( i ) )
					thisList = hostWiseSpouts.get( i );
				else
					thisList = new ArrayList< String >();

				thisList.add( sourceNamePrefix + sourceName + "_" + index + "_" + i );
				hostWiseSpouts.put( i, thisList );
			}
		}
		else if( thisDSType.equals( "CONFIG_UPDATE" ) ) {

			params.put( "groupId", dsProperty.get( "groupId" ).getAsString() );
			paramTypes.put( "groupId", "String" );

			params.put( "hostUrl", dsProperty.get( "hostUrl" ).getAsString() );
			paramTypes.put( "hostUrl", "String" );

			params.put( "clientId", dsProperty.get( "clientId" ).getAsString() + "_SOURCE_" + sourceName );
			paramTypes.put( "clientId", "String" );

			params.put( "pollInterval", dsProperty.get( "pollInterval" ).getAsString() );
			paramTypes.put( "pollInterval", "int" );

			params.put( "topic", dsProperty.get( "topic" ).getAsString() );
			paramTypes.put( "topic", "String" );

			System.out.println( "Adding Spout: " + sourceNamePrefix + sourceName + "_" + index );
			thisBuilder = generateComponent( params, paramTypes, sourceNamePrefix + sourceName, index, 1, "com.knowesis.sift.core.sources.KafkaSource", 0 );

			bufferedWriter.write( thisBuilder.toString() );
		}
	}


	public StringBuilder generateComponent( HashMap< String, String > params, HashMap< String, String > paramTypes, String sourceName, int index, int numberOfParallelProcess,
			String className, int hostId ) {
		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append( "   - id: " + "\"" + sourceName + "_" + index + "_" + hostId + "\"\n" );
		stringBuilder.append( "     className: \"" + className + "\"\n" );

		stringBuilder = addProperties( params, paramTypes, stringBuilder );
		stringBuilder = addConfig( sourceName, stringBuilder, hostId );
		if( sourceName.contains( "MegaTron" ) )
			stringBuilder.append( "     parallelism: " + MegaTronProcessesArray[ hostId ] + "\n\n" );
		else if( sourceName.contains( "_BadRecords" ) || sourceName.contains( "_DirectoryScan" ) || sourceName.contains( "Sift_Config_Update_Source_" ) )
			stringBuilder.append( "     parallelism: 1\n\n" );
		else
			stringBuilder.append( "     parallelism: " + parallelProcessesArray[ hostId ] + "\n\n" );

		return stringBuilder;
	}



	public void addBolts( String sourceName, JsonObject datasourceObject ) throws IOException {
		Iterator< Entry< String, StringBuilder > > thisIterator = fileBolts.entrySet().iterator();
		while( thisIterator.hasNext() ) {
			Entry< String, StringBuilder > thisEntry = thisIterator.next();
			System.out.println( "Adding Bolt: " + thisEntry.getKey() );
			bufferedWriter.write( thisEntry.getValue().toString() );
		}

		// MegaTron..
		HashMap< String, String > params = new HashMap< String, String >();
		HashMap< String, String > paramTypes = new HashMap< String, String >();
		params.put( "strPersistAddressList", persistAddress );
		paramTypes.put( "strPersistAddressList", "String" );

		params.put( "sourceName", sourceName );
		paramTypes.put( "sourceName", "String" );

		params.put( "memoryLoad", properties.getProperty( "MEGATRON_MEMORY_LOAD" ) );
		paramTypes.put( "memoryLoad", "int" );

		System.out.println( "Adding Bolt: " + sourceName + "_MegaTron_0" );
		for( int i = 0; i < hostsArray.length; i ++ ) {
			bufferedWriter.write( generateComponent( params, paramTypes, sourceName + "_MegaTron", 0, numberOfParallelProcess, 
					"com.knowesis.sift.core.MegaTron", i ).toString() );
		}

		// BadRecords..
		params = new HashMap< String, String >();
		paramTypes = new HashMap< String, String >();
		params.put( "filesinkPath", properties.getProperty( "BADRECORDS_PATH" ) );
		paramTypes.put( "filesinkPath", "String" );

		params.put( "recordsPerFile", properties.getProperty( "BADRECORDS_RECORDSPERFILE" ) );
		paramTypes.put( "recordsPerFile", "int" );

		params.put( "filePrefix", "BadRecords_" + sourceName );
		paramTypes.put( "filePrefix", "String" );

		System.out.println( "Adding Bolt: " + sourceName + "_BadRecords_0" );
		bufferedWriter.write( generateComponent( params, paramTypes, sourceName + "_BadRecords", 0, 1, 
				"com.knowesis.sift.core.sinks.FileSink", 0 ).toString() );


		integration = properties.getProperty( "INTEGRATION_POINT", "AMQ" );
		if( integration.equalsIgnoreCase( "KAFKA" ) ) {
			// KafkaSink..
			params = new HashMap< String, String >();
			paramTypes = new HashMap< String, String >();
			params.put( "hostUrl", properties.getProperty( "KAFKA_SINK_HOSTURL" ) );
			paramTypes.put( "hostUrl", "String" );

			params.put( "topic", properties.getProperty( "KAFKA_SINK_TOPIC" ) );
			paramTypes.put( "topic", "String" );

			params.put( "clientId", properties.getProperty( "KAFKA_SINK_CLIENTID" ) + "_SINK_" + sourceName );
			paramTypes.put( "clientId", "String" );

			System.out.println( "Adding Bolt: " + sourceName + "_DataIntegration_0" );
			for( int i = 0; i < hostsArray.length; i ++ ) {
				bufferedWriter.write( generateComponent( params, paramTypes, sourceName + "_DataIntegration", 0, numberOfParallelProcess, 
						"com.knowesis.sift.core.sinks.KafkaSink", i ).toString() );
			}
		}
		else if( integration.equalsIgnoreCase( "AMQ" ) ) {
			params = new HashMap< String, String >();
			paramTypes = new HashMap< String, String >();

			params.put( "destinationType", "AMQ" );
			paramTypes.put( "destinationType", "String" );

			params.put( "brokerURL", "tcp://52.187.111.161:61616" );
			paramTypes.put( "brokerURL", "String" );

			params.put( "queueNames", "sift.dataIntegration.in" );
			paramTypes.put( "queueNames", "String" );

			params.put( "messageType", "text" );
			paramTypes.put( "messageType", "String" );

			params.put( "numberOfTimesToTryToReconnect", "3" );
			paramTypes.put( "numberOfTimesToTryToReconnect", "int" );

			params.put( "numberOfTimesToTryToSendMessage", "3" );
			paramTypes.put( "numberOfTimesToTryToSendMessage", "int" );

			params.put( "outputFileDir", "/opt/knowesis/sift/core/sift/pworkspace/SiFT_Source_data/data/Messages" );
			paramTypes.put( "outputFileDir", "String" );

			params.put( "numberOfRecords", "10" );
			paramTypes.put( "numberOfRecords", "int" );

			params.put( "writeToFile", "true" );
			paramTypes.put( "writeToFile", "boolean" );

			params.put( "pushMessagesFromFile", "true" );
			paramTypes.put( "pushMessagesFromFile", "boolean" );

			params.put( "reconnectionInterval", "10" );
			paramTypes.put( "reconnectionInterval", "int" );

			System.out.println( "Adding Bolt: " + sourceName + "_DataIntegration_0" );

			for( int i = 0; i < hostsArray.length; i ++ ) {
				bufferedWriter.write( generateComponent( params, paramTypes, sourceName + "_DataIntegration", 0, numberOfParallelProcess, 
						"com.knowesis.sift.core.sinks.JMSLoadBalancerSink", i ).toString() );
			}

		}


		// Data Ingestion Pipeline related Bolts.
		JsonElement thisDSDestinationsElement = datasourceObject.get( "dataSourceDestinationProperties" );
		allDestinations = new JsonArray();
		if( thisDSDestinationsElement != null && ! thisDSDestinationsElement.isJsonNull() ) {
			JsonArray thisDSDestinations = thisDSDestinationsElement.getAsJsonArray();
			for( int i = 0; i < thisDSDestinations.size(); i ++ ) {
				JsonObject thisDestination = thisDSDestinations.get( i ).getAsJsonObject();
				String thisDestinationName = thisDestination.get( "destinationName" ).getAsString();
				bufferedFilterSinksWriter.write( thisDestinationName + "\n" );
				if( thisDestinationName.equals( "SIFT" ) )
					continue;

				for( int destInd = 0; destInd < hostsArray.length; destInd ++ ) {
					addFilterSink( thisDestination, thisDestinationName, destInd );
				}
				allDestinations.add( new JsonPrimitive( thisDestinationName ) );
			}
			bufferedFilterSinksWriter.flush();
		}
	}

	public StringBuilder addStreams() {

		StringBuilder stringBuilder = new StringBuilder();
		Iterator< Entry< String, String > > dsFileIterator = directoryFileMapping.entrySet().iterator();
		while( dsFileIterator.hasNext() ) {
			Entry< String, String > thisEntry = dsFileIterator.next();
			String thisDirectorySource = thisEntry.getKey();
			String[] thisFileSourceArray = thisEntry.getValue().split( "\\|" );

			for( int i = 0; i < thisFileSourceArray.length; i ++ ) {
				System.out.println( "Adding Stream: " + thisDirectorySource + "-->" + thisFileSourceArray[ i ] );
				stringBuilder.append( addStreamProperties( thisDirectorySource + "-->" + thisFileSourceArray[ i ], thisDirectorySource, thisFileSourceArray[ i ], "SHUFFLE", null ) );
				spoutsList.add( thisFileSourceArray[ i ] );
				List< String > thisList = null;
				if( hostWiseSpouts.containsKey( i ) )
					thisList = hostWiseSpouts.get( i );
				else
					thisList = new ArrayList< String >();

				thisList.add( thisFileSourceArray[ i ] );
				hostWiseSpouts.put( i, thisList );
			}
		}

		for( int i = 0; i < hostWiseSpouts.size(); i ++ ) {
			List< String > thisList = hostWiseSpouts.get( i );
			for( int j = 0; j < thisList.size(); j ++ ) {
				String thisSpout = thisList.get( j );
				System.out.println( "Adding Stream: " + thisSpout + "-->" + sourceName + "_MegaTron_0_" + i );
				stringBuilder.append( addStreamProperties( thisSpout + "-->MegaTron_0_" + i, thisSpout, sourceName + "_MegaTron_0_" + i, "SHUFFLE", null ) );
			}

			System.out.println( "Adding Stream: " + sourceName + "_MegaTron_0_" + i + "-->" + sourceName + "_DataIntegration_0_" + i );
			stringBuilder.append( addStreamProperties( sourceName + "_MegaTron_0_" + i + "-->" + sourceName + "_DataIntegration_0_" + i, sourceName + "_MegaTron_0_" + i, sourceName + "_DataIntegration_0_" + i, "SHUFFLE", "SIFT" ) );

			System.out.println( "Adding Stream: " + sourceName + "_MegaTron_0_" + i + "-->" + sourceName + "_BadRecords_0_0" );
			stringBuilder.append( addStreamProperties( sourceName + "_MegaTron_0_" + i + "-->" + sourceName + "_BadRecords_0_0", sourceName + "_MegaTron_0_" + i, sourceName + "_BadRecords_0_0", "SHUFFLE", "badrecord" ) );

			// Config Update Stream.
			System.out.println( "Adding Stream: " + "Sift_Config_Update_Source_" + sourceName + "_0_0-->" + sourceName + "_MegaTron_0_" + i );
			stringBuilder.append( addStreamProperties( "Sift_Config_Update_Source_" + sourceName + "_0_0-->" + sourceName + "_MegaTron_0_" + i, "Sift_Config_Update_Source_" + sourceName + "_0_0", sourceName + "_MegaTron_0_" + i, "ALL", null ) );

			// All Data ingestion pipeline streams.
			for( int iDest = 0; iDest < allDestinations.size(); iDest ++ ) {
				System.out.println( "Adding Stream: " + sourceName + "_MegaTron_0_" + i + "-->" + allDestinations.get( iDest ).getAsString() + "_0_" + i );
				stringBuilder.append( addStreamProperties( sourceName + "_MegaTron_0_" + i + "-->" + allDestinations.get( iDest ).getAsString() + "_0_" + i, sourceName + "_MegaTron_0_" + i, allDestinations.get( iDest ).getAsString() + "_0_" + i, "SHUFFLE", allDestinations.get( iDest ).getAsString() ) );
			}
		}

		return stringBuilder;
	}


	public StringBuilder addStreamProperties( String name, String from, String to, String grouping, String streamId ) {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append( "          - name: \"" + name + "\"\n" );
		stringBuilder.append( "            from: \"" + from + "\"\n" );
		stringBuilder.append( "            to: \"" + to + "\"\n" );
		stringBuilder.append( "            grouping:" + "\n" );
		stringBuilder.append( "               type: " + grouping + "\n" );
		if( streamId != null )
			stringBuilder.append( "               streamId: \"" + streamId + "\"\n" );

		stringBuilder.append( "\n\n" );

		return stringBuilder;
	}


	public void generateYaml( JsonObject datasourceObject, String sourceName, String yamlOutputPath ) throws IOException {

		fileWriter = new FileWriter( yamlOutputPath + "/" + sourceName + ".yaml" );
		bufferedWriter = new BufferedWriter( fileWriter );

		filterSinksWriter = new FileWriter( yamlOutputPath + "/" + sourceName + "_Filters" );
		bufferedFilterSinksWriter = new BufferedWriter( filterSinksWriter );

		bufferedWriter.write( "name: \"Sift_DataSource_" + sourceName + "\"\n" );

		bufferedWriter.write( "config:\n" );
		bufferedWriter.write( "  topology.workers: 1\n" );
		bufferedWriter.write( "  cimMapping: \"" + cimMapping + "\"\n" );
		bufferedWriter.write( "  dataSource: \"" + dataSourceName + "\"\n\n" );

		bufferedWriter.write( "# Spout Definitions:\n" );
		bufferedWriter.write( "spouts:\n" );

		JsonArray dsProperties = datasourceObject.get( "dataSourceProperties" ).getAsJsonArray();

		for( int i = 0; i < dsProperties.size(); i ++ ) {
			JsonObject thisDSProperty = dsProperties.get( i ).getAsJsonObject();
			addSpout( thisDSProperty, sourceName, "Source_data_", i );
		}

		// Config Update Spout.
		JsonObject thisDSProperty = new JsonObject();
		thisDSProperty.addProperty( "groupId", "CONFIG_UPDATE_" + sourceName );
		thisDSProperty.addProperty( "hostUrl", properties.getProperty( "KAFKA_SINK_HOSTURL" ) );
		thisDSProperty.addProperty( "clientId", "CONFIG_UPDATE" );
		thisDSProperty.addProperty( "pollInterval", 100 );
		thisDSProperty.addProperty( "topic", "SiftConfigUpdate" );
		thisDSProperty.addProperty( "type", "CONFIG_UPDATE" );
		addSpout( thisDSProperty, sourceName, "Sift_Config_Update_Source_", 0 );

		bufferedWriter.write( "# Bolt Definitions:\n" );
		bufferedWriter.write( "bolts:\n" );
		addBolts( sourceName, datasourceObject );

		bufferedWriter.write( "# Stream Definitions:\n" );
		bufferedWriter.write( "streams:\n" );

		bufferedWriter.write( addStreams().toString() );

		bufferedWriter.flush();
	}


	public StringBuilder addProperties( HashMap< String, String > params, HashMap< String, String > paramTypes, StringBuilder thisBuilder ) {
		thisBuilder.append( "     properties: \n" );
		Iterator< Entry< String, String > > iterator = params.entrySet().iterator();
		while( iterator.hasNext() ) {
			Entry< String, String > thisEntry = iterator.next();
			thisBuilder.append( "          - name: \"" + thisEntry.getKey()  +"\"\n" );
			if( paramTypes.get( thisEntry.getKey() ).equals( "int" ) || paramTypes.get( thisEntry.getKey() ).equals( "boolean" )  )
				thisBuilder.append( "            value: " + thisEntry.getValue() + "\n\n" );
			else
				thisBuilder.append( "            value: \"" + thisEntry.getValue() + "\"\n\n" );
		}

		return thisBuilder;
	}

	public StringBuilder addConfig( String sourceName, StringBuilder thisBuilder, int hostId ) {

		StringBuilder hostBuilder = new StringBuilder();

		if( sourceName.contains( "_BadRecords" ) || sourceName.contains( "_DirectoryScan" ) || sourceName.contains( "Sift_Config_Update_Source_" ) )
			hostBuilder.append( hostsArray[ 0 ] + ":1" );
		else {
			if( sourceName.contains( "MegaTron" ) )
				hostBuilder.append( hostsArray[ hostId ] + ":" + MegaTronProcessesArray[ hostId ] );
			else
				hostBuilder.append( hostsArray[ hostId ] + ":" + parallelProcessesArray[ hostId ] );
		}

		thisBuilder.append( "     configMethods: \n" );
		thisBuilder.append( "          - name: \"setConfig\"\n" );
		thisBuilder.append( "            args:\n" );
		thisBuilder.append( "                 - \"" + hostBuilder + "\"\n\n" );

		return thisBuilder;
	}

	public void addFilterSink( JsonObject thisJsonObject, String sinkName, int hostIndex ) throws IOException {
		String thisDSType = thisJsonObject.get( "destinationType" ).getAsString();
		HashMap< String, String > params = new HashMap< String, String >();
		HashMap< String, String > paramTypes = new HashMap< String, String >();
		StringBuilder thisBuilder = new StringBuilder();

		if( thisDSType.equals( "AMQ" ) ) {

			params.put( "destinationType", thisJsonObject.get( "amqQueueType" ).getAsString() );
			paramTypes.put( "destinationType", "String" );

			params.put( "brokerURL", thisJsonObject.get( "brokerUrl" ).getAsString() );
			paramTypes.put( "brokerURL", "String" );

			params.put( "queueNames", thisJsonObject.get( "queueName" ).getAsString() );
			paramTypes.put( "queueNames", "String" );

			params.put( "messageType", thisJsonObject.get( "msgType" ).getAsString() );
			paramTypes.put( "messageType", "String" );

			params.put( "numberOfTimesToTryToReconnect", "3" );
			paramTypes.put( "numberOfTimesToTryToReconnect", "int" );

			params.put( "numberOfTimesToTryToSendMessage", "3" );
			paramTypes.put( "numberOfTimesToTryToSendMessage", "int" );

			params.put( "writeToFile", "false" );
			paramTypes.put( "writeToFile", "boolean" );

			System.out.println( "Adding Bolt: " + sinkName );

			thisBuilder = generateComponent( params, paramTypes, sinkName, 0, 1, "com.knowesis.sift.core.sinks.JMSLoadBalancerSink", hostIndex );
			bufferedWriter.write( thisBuilder.toString() );
		}
		else if( thisDSType.equals( "File" ) ) {

			params.put( "filesinkPath", thisJsonObject.get( "filePath" ).getAsString() );
			paramTypes.put( "filesinkPath", "String" );

			params.put( "recordsPerFile", "10000" );
			paramTypes.put( "recordsPerFile", "int" );

			params.put( "filePrefix", thisJsonObject.get( "filePattern" ).getAsString() );
			paramTypes.put( "filePrefix", "String" );

			System.out.println( "Adding Bolt: " + sinkName );

			thisBuilder = generateComponent( params, paramTypes, sinkName, 0, 1, "com.knowesis.sift.core.sinks.FileSink", hostIndex );
			bufferedWriter.write( thisBuilder.toString() );
		}
	}

}