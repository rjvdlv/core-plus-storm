package com.knowesis.sift.core.sinks;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;

import com.ibm.msg.client.jms.JmsConnectionFactory;
import com.ibm.msg.client.jms.JmsFactoryFactory;
import com.ibm.msg.client.wmq.WMQConstants;
import com.knowesis.sift.Common.Commons;

public class JMSLoadBalancerSink extends BaseRichBolt {

	private int numberOfSinks;					
	private int numberOfTimesToTryToSendMessage;				
	private int numberOfTimesToTryToReconnect;		
	private String[] brokerURL;
	private int sinkIndex = 0;
	private String outputFileDir;
	private int numberOfRecords;
	private String[] channel;
	private String[] queueManager;
	private String[] port;
	private String configPlacement = null;

	private List<ActiveMQConnectionFactory> connectionFactoryList = new ArrayList< ActiveMQConnectionFactory >();
	private List< Connection > connectionList = new ArrayList< Connection >();
	private List< Session > sessionList = new ArrayList< Session >();
	private List< String > queueNamesList = new ArrayList< String >();
	private List< Destination > destinationsList = new ArrayList< Destination >(); 
	private List< MessageProducer > producerSinksList = new ArrayList< MessageProducer >();

	// Added for WMQ:
	private List< JmsFactoryFactory > jmsFactoryList = new ArrayList< JmsFactoryFactory >();
	private List< JmsConnectionFactory > jmsConnectionFactoryList = new ArrayList< JmsConnectionFactory >();

	private List< ActiveMQConnectionFactory > failedConnectionFactoryList = new ArrayList< ActiveMQConnectionFactory >();
	private List< JmsConnectionFactory > failedJmsConnectionFactoryList = new ArrayList< JmsConnectionFactory >();
	private List< String > failedQueueNamesList = new ArrayList< String >();
	private long lastRetryTime = 0;

	private PrintWriter writer = null;
	private int fileRecordCounter = 0;
	private boolean writeToFile = true;
	private String destinationType;
	private String messageType;
	protected Logger logger;
	private String[] queueNames = null;

	private boolean checkDirFlag = true;
	private boolean pushMessagesFromFile = false;
	private int reconnectionInterval = 10;

	public String[] getQueueNames() {
		return queueNames;
	}

	public void setQueueNames( String queueNames ) {
		System.out.println( Commons.logPrefix + "Queue Names: " + queueNames );
		this.queueNames = queueNames.split( "\\|" );
	}

	public int getNumberOfTimesToTryToSendMessage() {
		return numberOfTimesToTryToSendMessage;
	}

	public void setNumberOfTimesToTryToSendMessage( int numberOfTimesToTryToSendMessage ) {
		System.out.println( Commons.logPrefix + "Number of message retires: " + numberOfTimesToTryToSendMessage );
		this.numberOfTimesToTryToSendMessage = numberOfTimesToTryToSendMessage;
	}

	public int getNumberOfTimesToTryToReconnect() {
		return numberOfTimesToTryToReconnect;
	}

	public void setNumberOfTimesToTryToReconnect( int numberOfTimesToTryToReconnect ) {
		System.out.println( Commons.logPrefix + "Connection retries: " + numberOfTimesToTryToReconnect );
		this.numberOfTimesToTryToReconnect = numberOfTimesToTryToReconnect;
	}

	public String[] getBrokerURL() {
		return brokerURL;
	}

	public void setBrokerURL( String brokerURL ) {
		System.out.println( Commons.logPrefix + "Broker URL: " + brokerURL );
		this.brokerURL = brokerURL.split( "\\|" );
	}

	public String getOutputFileDir() {
		return outputFileDir;
	}

	public void setOutputFileDir( String outputFileDir ) {
		if( ! outputFileDir.endsWith( "/" ) )
			outputFileDir += "/";

		System.out.println( Commons.logPrefix + "Message output file directory: " + outputFileDir );
		this.outputFileDir = outputFileDir;
	}

	public int getNumberOfRecords() {
		return numberOfRecords;
	}

	public void setNumberOfRecords( int numberOfRecords ) {
		System.out.println( Commons.logPrefix + "Records per file: " + numberOfRecords );
		this.numberOfRecords = numberOfRecords;
	}

	public String[] getQueueManager() {
		return queueManager;
	}

	public void setQueueManager( String queueManager ) {
		System.out.println( Commons.logPrefix + "Queue manager: " + queueManager );
		this.queueManager = queueManager.split( "\\|" );
	}

	public String[] getPort() {
		return port;
	}

	public void setPort( String port ) {
		System.out.println( Commons.logPrefix + "Port: " + port );
		this.port = port.split( "\\|" );
	}

	public boolean isWriteToFile() {
		return writeToFile;
	}

	public void setWriteToFile( boolean writeToFile ) {
		System.out.println( Commons.logPrefix + "Write to file: " + writeToFile );
		this.writeToFile = writeToFile;
	}

	public String getDestinationType() {
		return destinationType;
	}

	public void setDestinationType( String destinationType ) {
		System.out.println( Commons.logPrefix + "Destination type: " + destinationType );
		this.destinationType = destinationType;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType( String messageType ) {
		System.out.println( Commons.logPrefix + "Message type: " + messageType );
		this.messageType = messageType;
	}

	public boolean isPushMessagesFromFile() {
		return pushMessagesFromFile;
	}

	public void setPushMessagesFromFile( boolean pushMessagesFromFile ) {
		System.out.println( Commons.logPrefix + "pushMessagesFromFile: " + pushMessagesFromFile );
		this.pushMessagesFromFile = pushMessagesFromFile;
	}

	public int getReconnectionInterval() {
		return reconnectionInterval;
	}

	public void setReconnectionInterval( int reconnectionInterval ) {
		System.out.println( Commons.logPrefix + "reconnectionInterval for failed connections: " + reconnectionInterval );
		this.reconnectionInterval = reconnectionInterval;
	}


	public void setConfig( String configPlacement ) {
		System.out.println( Commons.logPrefix + "Setting the configPlacement in JMSLoadBalancer as: " + configPlacement );
		this.configPlacement = configPlacement;
	}

	@Override
	public Map< String, Object > getComponentConfiguration () {

		Map< String, Object > configuration = super.getComponentConfiguration();
		if ( configuration == null )
			configuration = new HashMap< String, Object >();

		System.out.println( Commons.logPrefix + "Setting the configPlacement: " + configPlacement );
		configuration.put( "configPlacement", configPlacement );

		return configuration;
	}

	@Override
	public void prepare( Map stormConf, TopologyContext context, OutputCollector collector ) {

		numberOfSinks = queueNames.length;

		if( queueNames.length != brokerURL.length ) {
			numberOfSinks = ( queueNames.length < brokerURL.length ) ? queueNames.length : brokerURL.length;
			System.out.println( Commons.logPrefix + "The number of queue names and the number of server URL's are not equal. Number of queues and sinks used: " + numberOfSinks );
		}

		for ( int i = 0; i < numberOfSinks; i ++ ) {
			if( destinationType.equalsIgnoreCase( "AMQ" ) )
				connectionFactoryList.add( new  ActiveMQConnectionFactory( brokerURL[ i ] ) ); 
			else {

				try {
					jmsFactoryList.add( JmsFactoryFactory.getInstance( WMQConstants.WMQ_PROVIDER ) );
					jmsConnectionFactoryList.add( jmsFactoryList.get( i ).createConnectionFactory() );

					jmsConnectionFactoryList.get( i ).setStringProperty( WMQConstants.WMQ_HOST_NAME, brokerURL[ i ] );
					jmsConnectionFactoryList.get( i ).setIntProperty( WMQConstants.WMQ_PORT, Integer.parseInt( port[ i ] ));

					jmsConnectionFactoryList.get( i ).setStringProperty( WMQConstants.WMQ_CHANNEL, channel[ i ] );
					jmsConnectionFactoryList.get( i ).setIntProperty( WMQConstants.WMQ_CONNECTION_MODE, WMQConstants.WMQ_CM_CLIENT );
					jmsConnectionFactoryList.get( i ).setStringProperty( WMQConstants.WMQ_QUEUE_MANAGER, queueManager[ i ] );

				} catch (NumberFormatException | JMSException e) {
					System.out.println( "Error during JMS Load Balancer Initialization.." );
					e.printStackTrace();
					throw new NullPointerException();
				}

			}
			queueNamesList.add( queueNames[ i ] );

			for( int j = 0; j < numberOfTimesToTryToReconnect; j ++ ) {
				try {
					if( destinationType.equalsIgnoreCase( "AMQ" ) )
						connectionList.add( connectionFactoryList.get( i ).createConnection() );
					else
						connectionList.add( jmsConnectionFactoryList.get( i ).createConnection() );

					connectionList.get( i ).start();
					sessionList.add( connectionList.get( i ).createSession( false, Session.AUTO_ACKNOWLEDGE ) );
					break;
				}
				catch(Exception ex) {
					System.out.println( "Number of times failed to connect: " + ( j + 1 ) );
					ex.printStackTrace();
				}
			}

			if( connectionList.size() == 0 )
				System.out.println( "Failed to connect " + numberOfTimesToTryToReconnect + " times.");
			else {
				try {
					destinationsList.add( sessionList.get( i ).createQueue( queueNamesList.get( i ) ) );
					producerSinksList.add( sessionList.get( i ).createProducer( destinationsList.get( i ) ) );
					producerSinksList.get( i ).setDeliveryMode( DeliveryMode.PERSISTENT );
					System.out.println( "Connected Successfully - IP : " + brokerURL[ i ]);
				} catch (JMSException e) {
					System.out.println( "Error during JMS Load Balancer Initialization.." );
					e.printStackTrace();
					throw new NullPointerException();
				}

			}
		}
	}

	@Override
	public void execute( Tuple tuple ) {
		try {
			if( pushMessagesFromFile && checkDirFlag && numberOfSinks != 0 && producerSinksList.size() != 0 && connectionList.size() != 0 )
				processFileMessages();

			processMessage( tuple.getValue( 0 ).toString() );
		} catch ( IOException | JMSException e ) {
			e.printStackTrace();
		}
	}


	public void processMessage( String message ) throws JMSException {
		MapMessage mapMessage = null;
		TextMessage textMessage = null;

		if( numberOfSinks != 0 && producerSinksList.size() != 0 && connectionList.size() != 0 ) {
			boolean messageSent = false;
			try {     
				try {
					if( failedQueueNamesList.size() > 0 && ( new Date().getTime() - lastRetryTime ) > ( 1000 * 60 * reconnectionInterval ) )
						addFailedConnections();

					if( messageType.equalsIgnoreCase( "text" ) ) {
						textMessage = sessionList.get( sinkIndex ).createTextMessage( message );
						messageSent = sendMessage( textMessage, producerSinksList.get( sinkIndex ), numberOfTimesToTryToSendMessage );
					}
					else{
						mapMessage = sessionList.get( sinkIndex ).createMapMessage();
						mapMessage.setString( "message", message );
						messageSent = sendMessage( mapMessage, producerSinksList.get( sinkIndex ), numberOfTimesToTryToSendMessage );
					}
				}
				catch( Exception ex ) {
					ex.printStackTrace();
					System.out.println( "Messaging failed to queue.." + ex.getMessage()  );
				}

				while ( ! messageSent ) {
					Commons.logger.warn( Commons.logPrefix + "Removing Sink No: " + sinkIndex + ", Sink: " + ( queueNamesList.get( sinkIndex ).toString() ) );
					removeFailedConnections();

					numberOfSinks = producerSinksList.size();
					sinkIndex = ( sinkIndex >= numberOfSinks ) ? 0 : sinkIndex;

					if( ( numberOfSinks == 0  || producerSinksList.size() == 0 ) && writeToFile == true ) {
						Commons.logger.warn( Commons.logPrefix + "All the Queues are down! Writing the messages to the output file." );  
						fileRecordCounter = writeMessageToFile( fileRecordCounter, message );
						messageSent = true;
					}
					else {
						try {
							if( messageType.equalsIgnoreCase( "text" )) {
								textMessage = sessionList.get( sinkIndex ).createTextMessage( message );
								messageSent = sendMessage( textMessage, producerSinksList.get( sinkIndex ), numberOfTimesToTryToSendMessage );
							}
							else{
								mapMessage = sessionList.get( sinkIndex ).createMapMessage();
								mapMessage.setString( "message", message );
								messageSent = sendMessage( mapMessage, producerSinksList.get( sinkIndex ), numberOfTimesToTryToSendMessage );
							}
						}
						catch(Exception ex) {
							System.out.println( "Messaging failed to queue.." );
							ex.printStackTrace();
						}
					}
				}
			}
			catch(Exception e) {	
				System.out.println( "Issue sending message: " + e);
				e.printStackTrace();
			}
		}
		else if ( writeToFile ) {
			if( failedQueueNamesList.size() > 0 && ( new Date().getTime() - lastRetryTime ) > ( 1000 * 60 * reconnectionInterval ) )
				addFailedConnections();
			fileRecordCounter = writeMessageToFile( fileRecordCounter, message );
		}

		if( fileRecordCounter >= numberOfRecords ) {
			writer.close();
			fileRecordCounter = 0;
		}

		sinkIndex ++;
		sinkIndex = ( sinkIndex >= numberOfSinks ) ? 0 : sinkIndex;
	}


	// Method to send the message on a queue.
	public boolean sendMessage(TextMessage message, MessageProducer producer, int numberOfTimesToTryToSendMessage) throws JMSException {

		Commons.logger.info( Commons.logPrefix + "Recieved Text Message : " + message );
		boolean messageSent = false;

		for(int i = 0; i < numberOfTimesToTryToSendMessage; i ++) {
			try {
				producer.send( message );
				messageSent = true;
				break;
			}
			catch(JMSException ex) {
				System.out.println( Commons.logPrefix + "Number of times failed to send the message: " + ( i + 1 ) ); 
				ex.printStackTrace();
			}
		} 
		Commons.logger.info( Commons.logPrefix + "Message Sent Flag : " + messageSent  + " IP : " + producer.getDestination().toString() );
		return messageSent;
	}


	// Send a mapMessage
	public boolean sendMessage(MapMessage message, MessageProducer producer, int numberOfTimesToTryToSendMessage) {

		Commons.logger.info( Commons.logPrefix + "Recieved Map Message : " + message );
		boolean messageSent = false;

		for(int i = 0; i < numberOfTimesToTryToSendMessage; i ++) {
			try {
				producer.send( message );
				messageSent = true;
				break;
			}
			catch(JMSException ex) {
				System.out.println( Commons.logPrefix + "Number of times failed to send the message: " + ( i + 1 ) ); 
				ex.printStackTrace();
			}
		} 
		Commons.logger.info( Commons.logPrefix + "Message Sent Flag : " + messageSent );
		return messageSent;
	}


	// Method to write the messages to a file.
	public int writeMessageToFile( int fileRecordCounter, String message ) {
		if ( fileRecordCounter == 0 ) {    		    			
			// strDate = simpleDateFormat.format(new Date());				
			try {
				Commons.logger.info( Commons.logPrefix + "Trying to write message to file: " + outputFileDir + "MessagesFile_" + System.currentTimeMillis() + ".txt" );
				writer = new PrintWriter( outputFileDir + "MessagesFile_" + System.currentTimeMillis() + ".txt" );
			}
			catch( FileNotFoundException e ) {
				System.out.println( Commons.logPrefix + "The output directory is not present.");
				e.printStackTrace();
			}
		}

		writer.write( message + "\n" );
		writer.flush();
		fileRecordCounter ++;

		checkDirFlag = true;
		return fileRecordCounter;
	}


	// Method to add the failed connections back to the active connections list.
	public void addFailedConnections() throws JMSException {
		System.out.println( "Trying to retry failed Connections." );

		List< Integer > indexesToRemove = new ArrayList< Integer >();
		for( int i = 0; i < failedQueueNamesList.size(); i ++ ) {
			System.out.println( "Attemptimg queue: " + failedQueueNamesList.get( i ) );

			boolean successFlag = false;
			boolean addFlag = false;
			int fetchIndex = -1;
			try {
				if( destinationType.equalsIgnoreCase( "AMQ" ) )
					connectionList.add( failedConnectionFactoryList.get( i ).createConnection() );
				else
					connectionList.add( failedJmsConnectionFactoryList.get( i ).createConnection() );

				addFlag = true;
				fetchIndex = connectionList.size() - 1;
				connectionList.get( fetchIndex ).start();

				successFlag = true;
			}
			catch( Exception ex ) {
				ex.printStackTrace();
				if( addFlag )
					connectionList.remove( connectionList.size() - 1 );
			}

			if( successFlag ) {
				System.out.println( "Added the failed connection sink successfully..");
				if( destinationType.equalsIgnoreCase( "AMQ" ) )
					connectionFactoryList.add( failedConnectionFactoryList.get( i ) );
				else
					jmsConnectionFactoryList.add( failedJmsConnectionFactoryList.get( i ) );

				sessionList.add( connectionList.get( fetchIndex ).createSession( false, Session.AUTO_ACKNOWLEDGE ) );
				destinationsList.add( sessionList.get( fetchIndex ).createQueue( failedQueueNamesList.get( i ) ) );
				producerSinksList.add( sessionList.get( fetchIndex ).createProducer( destinationsList.get( fetchIndex ) ) );

				queueNamesList.add( failedQueueNamesList.get( i ) );
				indexesToRemove.add( i );
				numberOfSinks ++;
			}
		}

		for( int i = indexesToRemove.size() - 1; i >= 0; i -- ) {
			failedQueueNamesList.remove( i );
			if( destinationType.equalsIgnoreCase( "AMQ" ) )
				failedConnectionFactoryList.remove( i );
			else 
				failedJmsConnectionFactoryList.remove( i );
		}

		lastRetryTime = new Date().getTime();
	}


	// Method to remove the failed connections from the active connections list.
	public void removeFailedConnections() {
		Commons.logger.warn( Commons.logPrefix + "Removing the failed queue connection from the cluster: " + connectionFactoryList.get( sinkIndex ).getBrokerURL() );

		failedQueueNamesList.add( queueNamesList.get( sinkIndex ) );
		lastRetryTime = new Date().getTime();

		producerSinksList.remove( sinkIndex );
		sessionList.remove( sinkIndex );
		connectionList.remove( sinkIndex );
		queueNamesList.remove( sinkIndex );
		destinationsList.remove( sinkIndex );

		if( destinationType.equalsIgnoreCase( "AMQ" ) ) {
			failedConnectionFactoryList.add( connectionFactoryList.get( sinkIndex ) );
			connectionFactoryList.remove( sinkIndex );
		}
		else {
			failedJmsConnectionFactoryList.add( jmsConnectionFactoryList.get( sinkIndex ) );
			jmsConnectionFactoryList.remove( sinkIndex );
		}
	}


	// Method to read all the messages from files and push it to the queues.
	public void processFileMessages() throws IOException, JMSException {
		Commons.logger.warn( Commons.logPrefix + "Processing messages from the files." );
		File processedDirectory = new File( outputFileDir + "processed" );
		if( ! processedDirectory.exists() )
			processedDirectory.mkdirs();

		File outDirectory = new File( outputFileDir );
		String[] filesInDir = outDirectory.list();
		File thisFile = null;
		Path path = null;
		for ( int i = 0; i < filesInDir.length; i ++ ) {
			if( ! filesInDir[ i ].startsWith( "MessagesFile_" ) || ! filesInDir[ i ].endsWith( ".txt" ) )
				continue;

			thisFile = new File( outputFileDir + filesInDir[ i ] );
			path = thisFile.toPath();
			BufferedReader reader = Files.newBufferedReader( path, Charset.forName("UTF-8") );

			String currentLine = null;

			while( ( currentLine = reader.readLine() ) != null ) { 
				Commons.logger.info( Commons.logPrefix + "Printing within filesource: " + currentLine );	
				processMessage( currentLine );
			}

			thisFile.renameTo( new File( processedDirectory + "/" + thisFile.getName() ) );
		}

		checkDirFlag = false;
	}


	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {

	}
}

