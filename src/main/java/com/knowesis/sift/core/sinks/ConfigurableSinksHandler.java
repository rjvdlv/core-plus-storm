package com.knowesis.sift.core.sinks;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import com.knowesis.sift.Common.Commons;

public class ConfigurableSinksHandler extends BaseRichBolt {

	private OutputCollector outputCollector = null;
	private String sinkType = null;
	private String configPlacement = null;
	
	public void setConfig( String configPlacement ) {
		System.out.println( "Setting the configPlacement in ConfigurableSinksHandler as: " + configPlacement );
		this.configPlacement = configPlacement;
	}
	
	@Override
	 public Map< String, Object > getComponentConfiguration () {

		 Map< String, Object > configuration = super.getComponentConfiguration();
		 if ( configuration == null )
			 configuration = new HashMap< String, Object >();

		 System.out.println( Commons.logPrefix + "Setting the configPlacement: " + configPlacement );
		 configuration.put( "configPlacement", configPlacement );
		 
		 return configuration;
	 }
	
	@Override
	public void prepare( Map stormConf, TopologyContext context, OutputCollector collector ) {
		System.out.println( Commons.logPrefix + "Preparing configurable sinks.." );
		outputCollector = collector;
	}

	@Override
	public void execute( Tuple input ) {
		Commons.logger.info( Commons.logPrefix + "Input tuple in configurable sinks: " + input.toString() );
		Commons.logger.info( Commons.logPrefix + "first field in input: " + input.getString( 0 ) );
		String sinkTypeStr = ( String ) input.getValueByField( "SinkType" );
		Commons.logger.info( Commons.logPrefix + "Executing configurable sinks.." + sinkTypeStr );
		String[] sinkTypeArray = sinkTypeStr.split( "\\|" );
		for( int i = 0; i < sinkTypeArray.length; i ++ ) {
			sinkType = sinkTypeArray[ i ];
			outputCollector.emit( sinkType, new Values( input.getValueByField( "message" ) ) );
		}
	}

	@Override
	public void declareOutputFields( OutputFieldsDeclarer declarer ) {
		String sinksFile = "/opt/knowesis/sift/core/sift/scripts/SOURCES/Sift-topology_Sinks";
		Commons.logger.info( Commons.logPrefix + "----Reading Sinks file: " + sinksFile );
		try {
			File thisFile = new File( sinksFile );
			Path path = thisFile.toPath();
			BufferedReader reader = Files.newBufferedReader( path, Charset.forName( "UTF-8" ) );

			String currentLine = null;
			while( ( currentLine = reader.readLine() ) != null ) { 
				if( currentLine.trim().length() == 0 )
					continue;
				
				Commons.logger.info( Commons.logPrefix + "Adding the sink: " + currentLine );			
				declarer.declareStream( currentLine, new Fields( "message" ) );
			}
		} catch ( IOException e ) {
			e.printStackTrace();
		}
	}
}
