package com.knowesis.sift.core.sinks;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;

import com.knowesis.sift.Common.Commons;

public class FileSink extends BaseRichBolt {

	private String filesinkPath = null;
	private int recordsPerFile = 0;
	private String filePrefix = null;
	private FileWriter fileWriter = null;
	private BufferedWriter bufferedWriter = null;
	private int count = 0;
	private String configPlacement;
	private int taskIndex = 0;

	public String getFilesinkPath() {
		return filesinkPath;
	}

	public void setFilesinkPath( String filesinkPath ) {
		Commons.logger.info( Commons.logPrefix + "Filesink path: " + filesinkPath );
		this.filesinkPath = filesinkPath;
	}

	public int getRecordsPerFile() {
		return recordsPerFile;
	}

	public void setRecordsPerFile( int recordsPerFile ) {
		Commons.logger.info( Commons.logPrefix + "Records per file: " + recordsPerFile );
		this.recordsPerFile = recordsPerFile;
	}

	public String getFilePrefix() {
		return filePrefix;
	}

	public void setFilePrefix( String filePrefix ) {
		Commons.logger.info( Commons.logPrefix + "File prefix: " + filePrefix );
		this.filePrefix = filePrefix;
	}

	public void setConfig( String configPlacement ) {
		Commons.logger.info( Commons.logPrefix + "Setting the configPlacement in FileSink as: " + configPlacement );
		this.configPlacement = configPlacement;
	}
	
	@Override
	 public Map< String, Object > getComponentConfiguration () {

		 Map< String, Object > configuration = super.getComponentConfiguration();
		 if ( configuration == null )
			 configuration = new HashMap< String, Object >();

		 Commons.logger.info( Commons.logPrefix + "Setting the configPlacement: " + configPlacement );
		 configuration.put( "configPlacement", configPlacement );
		 
		 return configuration;
	 }
	
	@Override
	public void prepare( Map stormConf, TopologyContext context, OutputCollector collector ) {

		try {
			taskIndex = context.getThisTaskIndex();
			fileWriter = new FileWriter( filesinkPath + "/" + filePrefix + "_" + taskIndex + "_" + Long.toString( new Date().getTime() ) );
			bufferedWriter = new BufferedWriter( fileWriter );
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void execute( Tuple input ) {

		String thisRecord = input.getString( 0 );
		try {
			bufferedWriter.write( thisRecord + "\n" );
			count ++;

			if( count % recordsPerFile == 0 ) {
				bufferedWriter.flush();

				count = 0;
				fileWriter = new FileWriter( filesinkPath + "/" + filePrefix + "_" + taskIndex + "_" + Long.toString( new Date().getTime() ) );
				bufferedWriter = new BufferedWriter( fileWriter );
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			try {
				bufferedWriter.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
    public void cleanup() {
		try {
			bufferedWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
    } 

	@Override
	public void declareOutputFields( OutputFieldsDeclarer declarer ) {

	}

}
