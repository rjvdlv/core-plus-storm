package com.knowesis.sift.core.sinks;

/**
 * @author KNOWESIS SIFT PRODUCT DEVELOPMENT TEAM
 *
 */

import java.util.HashMap;
import java.util.Map;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;

public class BadRecordSink extends BaseBasicBolt {
	
	@Override
	public void execute( Tuple input, BasicOutputCollector collector ) {
		System.out.println( "Received a bad record: " + input.getString( 0 ) );
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare( new Fields( "record" ) );
	}

	public void prepare( Map stormConf, TopologyContext context, OutputCollector collector ) {
		System.out.println( "Initialised bad records stream.." );
	}
}
