package com.knowesis.sift.core.sinks;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;

import com.knowesis.sift.Common.Commons;

@SuppressWarnings("serial")
public class KafkaSink extends BaseRichBolt {

	private String hostUrl;
	private String topic;
	private String clientId;
	Properties props = null;
	private long lastResetTime = 0;
	private long resetInterval = 1000 * 60 * 5;
	private int resetCount = 3;
	private int currentResetCount;
	private int taskIndex;
	private String configPlacement;
	private KafkaProducer< Long, Object > kafkaProducer = null;

	public String getHostUrl() {
		return hostUrl;
	}


	public void setHostUrl( String hostUrl ) {
		this.hostUrl = hostUrl.replaceAll( "\\|", "," );
		System.out.println( "Kafka brokers: " + hostUrl );
	}


	public String getTopic() {
		return topic;
	}


	public void setTopic( String topic ) {
		this.topic = topic;
		System.out.println( "Kafka topic: " + topic );
	}


	public String getClientId() {
		return clientId;
	}


	public void setClientId( String clientId ) {
		System.out.println( "Kafka ClientId: " + clientId );
		this.clientId = clientId;
	}
	
	public void setConfig( String configPlacement ) {
		System.out.println( "Setting the configPlacement in KafkaSink as: " + configPlacement );
		this.configPlacement = configPlacement;
	}
	
	@Override
	 public Map< String, Object > getComponentConfiguration () {

		 Map< String, Object > configuration = super.getComponentConfiguration();
		 if ( configuration == null )
			 configuration = new HashMap< String, Object >();

		 System.out.println( "Setting the configPlacement: " + configPlacement );
		 configuration.put( "configPlacement", configPlacement );
		 
		 return configuration;
	 }


	@Override
	public void prepare( @SuppressWarnings("rawtypes") Map stormConf, TopologyContext context, OutputCollector collector ) {

		taskIndex = context.getThisTaskIndex();
		props = new Properties();
		props.put( ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, hostUrl );
		props.put( "metadata.broker.list", hostUrl );
		props.put( ProducerConfig.CLIENT_ID_CONFIG, clientId + "_" + taskIndex );
		props.put( "key.serializer", "org.apache.kafka.common.serialization.StringSerializer" );
		props.put( "value.serializer", "org.apache.kafka.common.serialization.StringSerializer" );
		
		kafkaProducer = new KafkaProducer< Long, Object >( props );
		lastResetTime = new Date().getTime();
		currentResetCount = 0;
	}


	@Override
	public void declareOutputFields( OutputFieldsDeclarer declarer ) {
		declarer.declare( new Fields( "record" ) );	
	}

	@Override
	public void execute( Tuple input ) {
		try {
			if( kafkaProducer == null && ( new Date().getTime() - lastResetTime ) > resetInterval ) {
				kafkaProducer = new KafkaProducer< Long, Object >( props );
				lastResetTime = new Date().getTime();
				currentResetCount ++;
			}
			
			Object recordObject = input.getValue( 0 );
			Commons.logger.info( Commons.logPrefix + "KAFKA: Received message: " + recordObject.toString() );
			final ProducerRecord< Long, Object > record =  new ProducerRecord< Long, Object >( topic, recordObject.toString() );
			RecordMetadata metadata = kafkaProducer.send( record ).get();
			
			Commons.logger.info( Commons.logPrefix + "Sent record(key= " + record.key() + "*** Value= " + record.value() + ") meta(partition= " + 
					metadata.partition() + ", offset= " + metadata.offset() + ") \n");
		} catch ( InterruptedException | ExecutionException e ) {
			System.out.println( "Error in KafkaSink. Resetting the sink in " + resetInterval + " milliseconds.." );
			kafkaProducer = null;
			e.printStackTrace();
			if( currentResetCount >= resetCount ) {
				System.out.println( "Exceeded the kafka sink reset count.. Shutting down.." );
				throw new NullPointerException();
			}
		}
	}
}
