package com.knowesis.sift.core;

/**
 * @author Raja SP
 */

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.renjin.primitives.vector.RowNamesVector;
import org.renjin.sexp.DoubleArrayVector;
import org.renjin.sexp.ListVector;
import org.renjin.sexp.StringArrayVector;
import org.renjin.sexp.Symbols;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.CustomerState;
import com.knowesis.sift.Common.LRUPandaCache;
import com.knowesis.sift.Common.PandaCache;
import com.knowesis.sift.Common.PandaCacheConfig;
import com.knowesis.sift.Commons.LocationNotDefinedException;
import com.knowesis.sift.api.dao.CIMDao;
import com.knowesis.sift.api.dao.EventDefDao;
import com.knowesis.sift.api.dao.ExpressionDao;
import com.knowesis.sift.api.dao.FunctionDao;
import com.knowesis.sift.api.dao.IndicatorDefDao;
import com.knowesis.sift.api.dao.OfferDAO;
import com.knowesis.sift.api.dao.ProgramsDAO;
import com.knowesis.sift.api.dao.SupportingDataDao;
import com.knowesis.sift.expression.ExpressionUtil;
import com.knowesis.sift.expression.PExpressionEvaluator;
import com.knowesis.sift.expression.PExpressionException;
import com.knowesis.sift.expression.SiftMath;

import au.com.bytecode.opencsv.CSVReader;

public class PandamaticOperator extends BaseRichBolt {

	protected static ExpressionDao expDao;
	protected OutputCollector outputCollector = null;
	protected Commons commonsState = null;

	public PandamaticOperator() {
		// TODO Auto-generated constructor stub
	}

	public void initialize( int workerId, String strPersistAddressList, String siftMode, int cacheSize ) throws Exception {

		commonsState = new Commons();
		commonsState.nWorkerId = workerId;
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );

		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddress.length; i++ ) {
			System.out.println( Commons.logPrefix + arrPersistAddress[ i ] );
			uris.add( new URI( arrPersistAddress[ i ] ) );
		}

		commonsState.subscriberIndicatorsMap = new LRUPandaCache< String, JsonObject >( Commons.pandaCacheSize, 0.75f );
		commonsState.eventsTaggedForSubscribersMap = new LRUPandaCache< String, JsonObject >( Commons.pandaCacheSize / 2, 0.75f );
		commonsState.eventTriggerCountsMap = new HashMap< String, JsonObject >();
		commonsState.summaryIndicatorsMap = new HashMap< String, JsonObject >();
		commonsState.flowFunnelMap = new HashMap< String, JsonObject >();
		commonsState.eventMonitoringCountsMap = new HashMap< String, JsonObject >();

		IndicatorDefDao indDao = new IndicatorDefDao();
		EventDefDao eveDao = new EventDefDao();


		if( Commons.currentUpdateStatus == -1 ) {
			Commons.currentUpdateStatus = workerId;
			Commons.logger.info( Commons.logPrefix + "Initialisation started by worker: " + workerId );
			Commons.siftMode = siftMode;
			Commons.pandaCacheSize = cacheSize;
			System.out.println( Commons.logPrefix + "******Recived New Cache Size : " + Commons.pandaCacheSize );
			Commons.pandaCache = new PandaCache( uris );
			System.out.println( Commons.logPrefix + "******initialised Pandacache : " + Commons.pandaCache.toString() );
			Commons.pandaCacheConfig = new PandaCacheConfig( uris );
			System.out.println( Commons.logPrefix + "******initialised PandacacheConfig : " + Commons.pandaCacheConfig.toString() );
			Commons.parser = new JsonParser();
			Commons.supportingData = new SupportingDataDao().getSupportingData();

			expDao = new ExpressionDao();
			Commons.allIndDef = ( LinkedHashMap< String, JsonObject > ) indDao.readActive();
			Commons.summaryIndDef = ( LinkedHashMap< String, JsonObject > ) indDao.readActiveSummaryIndicators(); // SUMMARY

			// INDICATOR
			Commons.allEveDef = eveDao.readActive();
			Commons.allFunctions = new FunctionDao().readUserDefined();

			Object[ ] resObj = expDao.prepareAllExpressions();
			Commons.expMap = ( HashMap< String, PExpressionEvaluator > ) resObj[ 0 ];
			Commons.fixedOfferParams = ( HashMap< String, String > ) resObj[ 1 ];
			CIMDao cimDao = new CIMDao();
			Commons.allCIM = cimDao.read();

			// Initialise the Sift Start Date
			Commons.siftConfig = Commons.supportingData.get( "SiftConfig" );
			SiftMath.siftStartDate = Commons.siftConfig.get( "siftStartDate" ).getAsLong();
			Commons.telcoPersistInterval = Commons.siftConfig.get( "telcoPersistInterval" ).getAsInt();
			Commons.numberOfInstances = Commons.siftConfig.get( "numberOfInstances" ).getAsInt(); // MAXTRIGGER_LIMIT
			if( Commons.siftConfig.has( "FlowFunnelFlag" ) )
				Commons.flowFunnelFlag = Commons.siftConfig.get( "FlowFunnelFlag" ).getAsBoolean();
			if( Commons.siftConfig.has( "TelcoIndicatorsRetentionPeriod" ) )
				Commons.telcoIndicatorsRetentionPeriod = Commons.siftConfig.get( "TelcoIndicatorsRetentionPeriod" ).getAsInt();
			if( Commons.siftConfig.has( "EventsToBeProcessed" ) )
				Commons.EventsToBeProcessed = Commons.siftConfig.get( "EventsToBeProcessed" ).getAsString();
			Commons.filterEvents = true;
			if( Commons.siftConfig.has( "lowScoreThreshold" ) )
				Commons.lowScoreThreshold = Commons.siftConfig.get( "lowScoreThreshold" ).getAsDouble();

			String contactPolicyString = ( String ) Commons.pandaCacheConfig.get( "ContactPolicy" );
			if( contactPolicyString != null )
				Commons.contactPolicyObject = ( JsonObject ) Commons.parser.parse( contactPolicyString );

			initialiseModelMasterTables();

			if( Commons.eventsTaggedforAll == null ) {
				Commons.eventsTaggedforAll = new JsonObject();
				Iterator< Entry< String, JsonObject > > itr = Commons.allEveDef.entrySet().iterator();
				while( itr.hasNext() ) {
					Entry< String, JsonObject > elemEntry = itr.next();
					JsonObject eveDef = elemEntry.getValue();
					if( eveDef.get( "taggedForAll" ).getAsString().equals( "true" ) ) {
						JsonObject thisTag = new JsonObject();
						thisTag.addProperty( "startTimestamp", new java.util.Date().getTime() );
						thisTag.addProperty( "endTimestamp", new java.util.Date().getTime() );
						Commons.eventsTaggedforAll.add( elemEntry.getKey(), thisTag );
					}
				}
			}

			Commons.customersForLock = new LRUPandaCache< String, String >( Commons.pandaCacheSize, 0.75f );

			if( Commons.allProgramDef == null ) {
				ProgramsDAO programsDAO = new ProgramsDAO();
				Commons.allProgramDef = programsDAO.readActive();
				OfferDAO offerDao = new OfferDAO();
				Commons.allOfferDef = offerDao.read();
			}

			Commons.currentUpdateStatus = -1;
		}
		else {
			Commons.logger.info( Commons.logPrefix + "Initialisation in progress. instance sleeping until initialisation completes.." );
			while( Commons.currentUpdateStatus != -1 ) {
				Thread.sleep( 3000 );
			}
			Commons.logger.info( Commons.logPrefix + "Initialisation completed. Thread wake up.." );
		}
	}

	private static void initialiseModelMasterTables() throws IOException {
		// read the list of files from Persist and initialise from there.
		JsonElement eModelMastersConfig = Commons.siftConfig.get( "modelMastersConfig" );
		if( eModelMastersConfig == null || eModelMastersConfig.isJsonNull() )
			return;

		Commons.modelMasterTable = new HashMap< String, Object >();
		JsonArray modelMastersConfig = eModelMastersConfig.getAsJsonArray();
		for( int i = 0; i < modelMastersConfig.size(); i++ ) {
			JsonObject thisTable = modelMastersConfig.get( i ).getAsJsonObject();
			CSVReader reader = new CSVReader( new FileReader( thisTable.get( "location" ).getAsString() ) );
			String[ ] record = null;

			// OFFER_ID_CVM | PRICE | SERVICE | VALIDITY | TYPE | STRATEGY |
			// DESCRIPTION
			StringArrayVector.Builder OFFER_ID_CVM = new StringArrayVector.Builder();
			DoubleArrayVector.Builder PRICE = new DoubleArrayVector.Builder();
			StringArrayVector.Builder SERVICE = new StringArrayVector.Builder();
			StringArrayVector.Builder VALIDITY = new StringArrayVector.Builder();
			StringArrayVector.Builder TYPE = new StringArrayVector.Builder();
			StringArrayVector.Builder STRATEGY = new StringArrayVector.Builder();
			StringArrayVector.Builder DESCRIPTION = new StringArrayVector.Builder();

			while( ( record = reader.readNext() ) != null ) {
				OFFER_ID_CVM.add( record[ 0 ] );
				PRICE.add( Double.parseDouble( record[ 2 ].replace( ",", "" ) ) );
				SERVICE.add( record[ 3 ] );
				VALIDITY.add( record[ 4 ] );
				TYPE.add( record[ 5 ] );
				STRATEGY.add( record[ 6 ] );
				DESCRIPTION.add( record[ 7 ] );
			}

			ListVector.NamedBuilder offerTableBuilder = new ListVector.NamedBuilder();
			offerTableBuilder.setAttribute( Symbols.CLASS, StringArrayVector.valueOf( "data.frame" ) );
			offerTableBuilder.setAttribute( Symbols.ROW_NAMES, new RowNamesVector( 2 ) );
			offerTableBuilder.add( "OFFER_ID_CVM", OFFER_ID_CVM.build() );
			offerTableBuilder.add( "PRICE", PRICE.build() );
			offerTableBuilder.add( "SERVICE", SERVICE.build() );
			offerTableBuilder.add( "VALIDITY", VALIDITY.build() );
			offerTableBuilder.add( "TYPE", TYPE.build() );
			offerTableBuilder.add( "STRATEGY", STRATEGY.build() );
			offerTableBuilder.add( "DESCRIPTION", DESCRIPTION.build() );

			ListVector offerTable = offerTableBuilder.build();
			Commons.modelMasterTable.put( thisTable.get( "key" ).getAsString(), offerTable );
			System.out.println( thisTable.get( "key" ).getAsString() + " ====== " + offerTable.getNames() );
		}
	}

	protected void handleTagListUpdates( JsonObject tuple ) {
		String msisdn = tuple.get( "MSISDN" ).getAsString();
		String eventId = tuple.get( "EVENTID_TOBE_MONITORED" ).getAsString();

		String strTagList = ( String ) Commons.pandaCache.get( "EventTagList-" + msisdn );
		JsonObject jTagList = new JsonObject();
		if( strTagList == null ) {
			jTagList = new JsonObject();
			jTagList.addProperty( "docType", "TagList" );
		}else
			jTagList = ( JsonObject ) Commons.parser.parse( strTagList );

		JsonObject jTag = new JsonObject();

		jTag.addProperty( "SUBSCRIBER_ID", tuple.get( "SUBSCRIBER_ID" ).getAsString() );
		jTag.addProperty( "siftvar1", tuple.get( "siftvar1" ).getAsString() );
		jTag.addProperty( "siftvar2", tuple.get( "siftvar2" ).getAsString() );
		jTag.addProperty( "MONITORING_START_TIME", tuple.get( "MONITORING_START_TIME" ).getAsLong() );
		jTag.addProperty( "MONITORING_END_TIME", tuple.get( "MONITORING_END_TIME" ).getAsLong() );

		jTag.addProperty( "SCHEDULED_REMINDER_HOURS", tuple.get( "SCHEDULED_REMINDER_HOURS" ).getAsFloat() );
		jTag.addProperty( "ACTIVITY_TRIGGERED_REMINDER_COUNT", tuple.get( "ACTIVITY_TRIGGERED_REMINDER_COUNT" ).getAsInt() );
		jTag.addProperty( "REPEAT_EVENT_REQUIRED", tuple.get( "REPEAT_EVENT_REQUIRED" ).getAsBoolean() );
		jTag.addProperty( "RECURRING_EVENT_REQUIRED", tuple.get( "RECURRING_EVENT_REQUIRED" ).getAsBoolean() );
		jTag.addProperty( "REPEAT_RECURRING_COUNT", tuple.get( "REPEAT_RECURRING_COUNT" ).getAsInt() );

		jTag.addProperty( "NO_EVENT_REQUIRED", tuple.get( "NO_EVENT_REQUIRED" ).getAsBoolean() );
		jTag.addProperty( "NO_EVENT_SCHEDULE_HOURS", tuple.get( "NO_EVENT_SCHEDULE_HOURS" ).getAsFloat() );
		jTag.addProperty( "PARTIAL_EVENT_REQUIRED", tuple.get( "PARTIAL_EVENT_REQUIRED" ).getAsBoolean() );
		jTag.addProperty( "PARTIAL_EVENT_SHEDULE_HOURS", tuple.get( "PARTIAL_EVENT_SHEDULE_HOURS" ).getAsFloat() );

		jTag.addProperty( "FLOW_ID", tuple.get( "FLOW_ID" ).getAsString() );
		boolean isControl = tuple.get( "IS_CONTROL" ).getAsBoolean();
		jTag.addProperty( "IS_CONTROL", isControl );

		if( tuple.has( "TRIGGER_LATER" ) ) {
			Boolean triggerLater = tuple.get( "TRIGGER_LATER" ).getAsBoolean();
			if( triggerLater )
				jTag.addProperty( "TRIGGER_LATER", tuple.get( "TRIGGER_LATER" ).getAsBoolean() );
		}

		jTagList.add( eventId, jTag );
		Commons.pandaCache.set( "EventTagList-" + msisdn, 0, jTagList.toString() );

		/******
		 * Logic to create schedules if needed
		 *******************************/
		String strEventDef = ( String ) Commons.pandaCacheConfig.get( eventId );
		if( strEventDef == null )
			return;
		JsonObject thisEvent = ( JsonObject ) Commons.parser.parse( strEventDef );
		// If the tag list has the endTime, always turn the Schedule on for
		// Reminder Event
		// int schedHours = tuple.getInt( "SCHEDULED_REMINDER_HOURS" ) *
		// 3600000;
		float schedHours = tuple.get( "SCHEDULED_REMINDER_HOURS" ).getAsFloat();
		long nSchedMilliseconds = Math.round( schedHours * 60 ) * 60000;

		if( tuple.has( "TRIGGER_LATER" ) && tuple.get( "TRIGGER_LATER" ).getAsBoolean() ) {
			createScheduleForExternalMonitoringRequest( eventId, eventId, tuple, "TL", tuple.get( "MONITORING_END_TIME" ).getAsLong() );
			return;
		}

		// logger.log( java.util.logging.Level.INFO, Commons.logPrefix + "Reminder Schedule Hours :
		// " + schedHours );

		if( schedHours > 0 ) {
			long finalSchedule = nSchedMilliseconds + tuple.get( "MONITORING_START_TIME" ).getAsLong();
			// logger.log( java.util.logging.Level.INFO, Commons.logPrefix + "Reminder Monitoring
			// start time in input : " + tuple.getLong( "MONITORING_START_TIME"
			// ) );
			createScheduleForExternalMonitoringRequest( eventId, thisEvent.get( "scheduledReminderCondition" ).getAsString(), tuple, "RE", finalSchedule );
		}
		boolean noEveReq = tuple.get( "NO_EVENT_REQUIRED" ).getAsBoolean();
		if( noEveReq ) {
			// logger.log( java.util.logging.Level.INFO, Commons.logPrefix + "No Event schedule in
			// input : " + tuple.getLong( "NO_EVENT_SCHEDULE_HOURS" ) );
			schedHours = tuple.get( "NO_EVENT_SCHEDULE_HOURS" ).getAsFloat();
			nSchedMilliseconds = Math.round( schedHours * 60 ) * 60000;
			long finalSchedule = nSchedMilliseconds + tuple.get( "MONITORING_END_TIME" ).getAsLong();
			// logger.log( java.util.logging.Level.INFO, Commons.logPrefix + "Monitoring End Time in
			// input : " + tuple.getLong( "MONITORING_END_TIME" ) );
			createScheduleForExternalMonitoringRequest( eventId, thisEvent.get( "noEventTriggerCondition" ).getAsString(), tuple, "NE", finalSchedule );
		}
		boolean partialEveReq = tuple.get( "PARTIAL_EVENT_REQUIRED" ).getAsBoolean();
		if( partialEveReq ) {
			// logger.log( java.util.logging.Level.INFO, Commons.logPrefix + "Partial Event schedule
			// in input : " + tuple.getLong( "PARTIAL_EVENT_SHEDULE_HOURS" ) );
			schedHours = tuple.get( "PARTIAL_EVENT_SHEDULE_HOURS" ).getAsFloat();
			nSchedMilliseconds = Math.round( schedHours * 60 ) * 60000;
			long finalSchedule = nSchedMilliseconds + tuple.get( "MONITORING_END_TIME" ).getAsLong();
			// logger.log( java.util.logging.Level.INFO, Commons.logPrefix + "Monitoring End Time in
			// input : " + tuple.getLong( "MONITORING_END_TIME" ) );
			createScheduleForExternalMonitoringRequest( eventId, thisEvent.get( "partialEventTriggerCondition" ).getAsString(), tuple, "PE", finalSchedule );
		}

		// update the internal memory
		// remove the doctype from the taglist if any
		jTagList.remove( "docType" );
		commonsState.eventsTaggedForSubscribersMap.put( msisdn, jTagList );
		Commons.logger.info( Commons.logPrefix + "Updated Internal Memory - eventsTaggedForSubscriber - " + msisdn + " - " + jTagList.toString() );

		/***********************
		 * Maintain Monitroring count for Event Live Chart
		 **************************/
		JsonObject monitoringCount = commonsState.eventMonitoringCountsMap.get( eventId );
		if( monitoringCount == null ) {
			String strMonitoringCount = ( String ) Commons.pandaCache.get( "TELCO_" + eventId + "-" + commonsState.nWorkerId + "-MonitoringCounts" );
			if( strMonitoringCount == null ) {
				monitoringCount = new JsonObject();
				monitoringCount.addProperty( "docType", "EventMonitoringCount" );
				monitoringCount.addProperty( "count", new Long( 0 ) );
				// monitoringCount.addProperty( "count_control", new Long( 0 )
				// );
			}else {
				monitoringCount = ( JsonObject ) Commons.parser.parse( strMonitoringCount );
			}
			commonsState.eventMonitoringCountsMap.put( eventId, monitoringCount );
		}
		Long lCount = monitoringCount.get( "count" ).getAsLong() + 1;
		monitoringCount.addProperty( "count", lCount );
		/*
		 * if( isControl ) monitoringCount.addProperty( "count_control",
		 * monitoringCount.get( "count_control" ).getAsLong() + 1 );
		 */
		// if( lCount % 10 == 0 )
		/***********************
		 * Maintain Monitroring count for Event Live Chart
		 **************************/

		/***********************
		 * Maintain Hourly/Daily Trigger Count for MeasurementLive Chart
		 **************************/
		JsonElement eDailyCount = monitoringCount.get( "DailyCount" );
		JsonArray dailyCountArray = null;
		if( eDailyCount == null )
			dailyCountArray = new JsonArray();
		else
			dailyCountArray = eDailyCount.getAsJsonArray();
		JsonArray dailyEntry = null;
		Date today = new java.util.Date();
		long ltoday = today.getTime();
		ltoday += java.util.TimeZone.getDefault().getOffset( ltoday );
		ltoday = ltoday - ltoday % SiftMath.msecPerDay;
		long count = 1;
		if( dailyCountArray.size() > 0 ) {
			dailyEntry = dailyCountArray.get( dailyCountArray.size() - 1 ).getAsJsonArray();
			long dateInArray = dailyEntry.get( 0 ).getAsLong();
			if( dateInArray == ltoday ) {
				count = dailyEntry.get( 1 ).getAsLong() + 1;
				dailyCountArray.remove( dailyCountArray.size() - 1 );
			}
		}
		dailyEntry = new JsonArray();
		dailyEntry.add( new JsonPrimitive( ltoday ) );
		dailyEntry.add( new JsonPrimitive( count ) );
		dailyCountArray.add( dailyEntry );
		monitoringCount.add( "DailyCount", dailyCountArray );

		// Hourly Counts
		JsonElement eHourlyCount = monitoringCount.get( "HourlyCount-" + ltoday );
		JsonArray hourlyCountArray = null;
		if( eHourlyCount == null )
			hourlyCountArray = new JsonArray();
		else
			hourlyCountArray = eHourlyCount.getAsJsonArray();
		JsonArray hourlyEntry = null;
		Calendar thisCal = Calendar.getInstance();
		thisCal.setTime( today );
		long thisHour = thisCal.get( Calendar.HOUR_OF_DAY );
		count = 1;
		if( hourlyCountArray.size() > 0 ) {
			hourlyEntry = hourlyCountArray.get( hourlyCountArray.size() - 1 ).getAsJsonArray();
			long hourInArray = hourlyEntry.get( 0 ).getAsLong();
			if( hourInArray == thisHour ) {
				count = hourlyEntry.get( 1 ).getAsLong() + 1;
				hourlyCountArray.remove( hourlyCountArray.size() - 1 );
			}
		}
		hourlyEntry = new JsonArray();
		hourlyEntry.add( new JsonPrimitive( thisHour ) );
		hourlyEntry.add( new JsonPrimitive( count ) );
		hourlyCountArray.add( hourlyEntry );
		monitoringCount.add( "HourlyCount-" + ltoday, hourlyCountArray );
		Commons.pandaCache.set( "TELCO_" + eventId + "-" + commonsState.nWorkerId + "-MonitoringCounts", 0, monitoringCount.toString() );
		/***********************
		 * Maintain Hourly/Daily Trigger Count for MeasurementLive Chart
		 **************************/
	}

	protected void handleConfigurationChanges( Tuple tuple ) throws PExpressionException, IOException, ParseException, InterruptedException {
		Commons.logger.info( Commons.logPrefix + "Configuration Change recieved : " + tuple.toString() );
		Commons.logger.info( Commons.logPrefix + "------------" + "Configuration Change recieved : " + tuple.toString() + " ----------------" );
		String thisRecord = tuple.getStringByField( "In_CDR" );
		String[] thisRecordSplit = thisRecord.split( "\\|" );
		String id = thisRecordSplit[ 0 ];
		String type = thisRecordSplit[ 1 ];
		String operation = thisRecordSplit[ 2 ];

		if( type.equals( "IndicatorInstance" ) ) {
			if( operation.equals( "U" ) ) {
				commonsState.subscriberIndicatorsMap.remove( id );
				Commons.logger.info( Commons.logPrefix + "Invalidated Indicator Instance for : " + id );
			}else if( operation.equals( "D" ) ) {
				commonsState.subscriberIndicatorsMap.remove( id );
				Commons.pandaCache.delete( id + "-Indicators" );
				// remove the monitoring entries too
				commonsState.eventsTaggedForSubscribersMap.remove( id );
				Commons.pandaCache.delete( "EventTagList-" + id );
				Commons.logger.info( Commons.logPrefix + "Deleted Indicator Instance for : " + id );
			}
		}else if( type.equals( "SupportingData" ) ) {
			String existingData = ( String ) Commons.pandaCacheConfig.get( id );
			if( operation.equals( "R" ) ) {
				Commons.supportingData.put( id, ( JsonObject ) Commons.parser.parse( existingData ) );

				if( id.equals( "SiftConfig" ) ) {
					Commons.siftConfig = Commons.supportingData.get( id );
					SiftMath.siftStartDate = Commons.siftConfig.get( "siftStartDate" ).getAsLong();
					Commons.telcoPersistInterval = Commons.siftConfig.get( "telcoPersistInterval" ).getAsInt();
					Commons.numberOfInstances = Commons.siftConfig.get( "numberOfInstances" ).getAsInt();
					if( Commons.siftConfig.has( "FlowFunnelFlag" ) )
						Commons.flowFunnelFlag = Commons.siftConfig.get( "FlowFunnelFlag" ).getAsBoolean();
					if( Commons.siftConfig.has( "TelcoIndicatorsRetentionPeriod" ) )
						Commons.telcoIndicatorsRetentionPeriod = Commons.siftConfig.get( "TelcoIndicatorsRetentionPeriod" ).getAsInt();
					if( Commons.siftConfig.has( "EventsToBeProcessed" ) )
						Commons.EventsToBeProcessed = Commons.siftConfig.get( "EventsToBeProcessed" ).getAsString();
				}
			}else {
				String data = tuple.getString( 3 );
				JsonObject jData = ( JsonObject ) Commons.parser.parse( data );
				// get and update

				if( existingData == null ) {
					Commons.pandaCacheConfig.set( id, 0, data );
					Commons.supportingData.put( id, jData );
				}else {
					// update to the existing data
					JsonObject jExistingData = ( JsonObject ) Commons.parser.parse( existingData );
					Set< Entry< String, JsonElement > > eset = jData.entrySet();
					Iterator< Entry< String, JsonElement > > itr = eset.iterator();
					while( itr.hasNext() ) {
						Entry< String, JsonElement > aEntry = itr.next();
						jExistingData.add( aEntry.getKey(), aEntry.getValue() );
					}
					Commons.pandaCacheConfig.set( id, 0, jExistingData.toString() );
					Commons.supportingData.put( id, jExistingData );
				}
			}
		}else if( type.equals( "CacheOperation" ) ) {
			if( operation.equals( "D" ) ) {
				commonsState.subscriberIndicatorsMap.remove( id );
				commonsState.eventsTaggedForSubscribersMap.remove( id );
				Commons.logger.info( Commons.logPrefix + "Invalidated the cache entries for " + id );
			}
		}else {
			if( Commons.currentUpdateStatus == -1 ) {
				Commons.currentUpdateStatus = commonsState.nWorkerId;
				Commons.logger.info( Commons.logPrefix + "Initialisation started by worker: " + commonsState.nWorkerId );
				IndicatorDefDao indDao = new IndicatorDefDao();
				EventDefDao eveDao = new EventDefDao();
				expDao = new ExpressionDao();
				Commons.allIndDef = ( LinkedHashMap< String, JsonObject > ) indDao.readActive();
				Commons.summaryIndDef = ( LinkedHashMap< String, JsonObject > ) indDao.readActiveSummaryIndicators(); // SUMMARY
				// INDICATOR
				Commons.allEveDef = eveDao.readActive();
				Commons.allFunctions = new FunctionDao().readUserDefined();
				Object[ ] resObj = expDao.prepareAllExpressions();
				Commons.expMap = ( HashMap< String, PExpressionEvaluator > ) resObj[ 0 ];
				Commons.fixedOfferParams = ( HashMap< String, String > ) resObj[ 1 ];
				CIMDao cimDao = new CIMDao();
				Commons.allCIM = cimDao.read();

				Commons.eventsTaggedforAll = new JsonObject();
				Iterator< Entry< String, JsonObject > > itr = Commons.allEveDef.entrySet().iterator();
				while( itr.hasNext() ) {
					Entry< String, JsonObject > elemEntry = itr.next();
					JsonObject eveDef = elemEntry.getValue();
					if( eveDef.get( "taggedForAll" ).getAsString().equals( "true" ) ) {
						JsonObject thisTag = new JsonObject();
						thisTag.addProperty( "startTimestamp", new java.util.Date().getTime() );
						thisTag.addProperty( "endTimestamp", new java.util.Date().getTime() );
						Commons.eventsTaggedforAll.add( elemEntry.getKey(), thisTag );
					}
				}
				// }

				ProgramsDAO programsDAO = new ProgramsDAO();
				Commons.allProgramDef = programsDAO.readActive();
				OfferDAO offerDao = new OfferDAO();
				Commons.allOfferDef = offerDao.read();

				Commons.currentUpdateStatus = -1;
			}
			else {
				Commons.logger.info( Commons.logPrefix + "Config update in progress. instance sleeping until update completes.." );
				while( Commons.currentUpdateStatus != -1 ) {
					Thread.sleep( 3000 );
				}
				Commons.logger.info( Commons.logPrefix + "Config update completed. Thread wake up.." );
			}
		}

		Commons.logger.info( Commons.logPrefix + "------------ Configuration Changes are updated ----------------" );
	}

	protected CustomerState getIndicatorsEventsTags( String msisdn, JsonObject tupleMap, Date callDate, String serviceType, Calendar cal ) {
		CustomerState customerState = new CustomerState();
		Commons.logger.info( Commons.logPrefix + "Retriveing existing indicators & events for : " + msisdn );
		customerState.subscriberEventTags = null;
		customerState.subscriberNextWaveEventTags = null;
		customerState.subscriberPrograms = null;
		customerState.subscriberIndicators = null;

		customerState.sessionIndicators = new JsonObject();

		customerState.subscriberIndicators = commonsState.subscriberIndicatorsMap.get( msisdn );
		if( customerState.subscriberIndicators == null ) {
			String strIndicators = ( String ) Commons.pandaCache.get( msisdn + "-Indicators" );
			if( strIndicators == null ) {
				customerState.subscriberIndicators = new JsonObject();
				customerState.subscriberIndicators.addProperty( "docType", "subscriberIndicators" );
			}else {
				customerState.subscriberIndicators = ( JsonObject ) Commons.parser.parse( strIndicators );
			}
		}

		customerState.subscriberTriggers = ( JsonObject ) customerState.subscriberIndicators.get( "EVENT_TRIGGER_LifeTime-L" );
		if( customerState.subscriberTriggers == null ) {
			customerState.subscriberTriggers = new JsonObject();
			customerState.subscriberIndicators.add( "EVENT_TRIGGER_LifeTime-L", customerState.subscriberTriggers );
		}

		customerState.subscriberPrograms = ( JsonObject ) customerState.subscriberIndicators.get( "PROGRAMS_LifeTime-L" );
		if( customerState.subscriberPrograms == null ) {
			customerState.subscriberPrograms = new JsonObject();
			customerState.subscriberIndicators.add( "PROGRAMS_LifeTime-L", customerState.subscriberPrograms );
		}

		customerState.subscriberEventTags = commonsState.eventsTaggedForSubscribersMap.get( msisdn );
		if( customerState.subscriberEventTags == null ) {
			String strTagList = ( String ) Commons.pandaCache.get( "EventTagList-" + msisdn );
			if( strTagList == null )
				customerState.subscriberEventTags = new JsonObject();
			else {
				customerState.subscriberEventTags = ( JsonObject ) Commons.parser.parse( strTagList );
				customerState.subscriberEventTags.remove( "docType" );
			}
			commonsState.eventsTaggedForSubscribersMap.put( msisdn, customerState.subscriberEventTags );
		}

		// Merge the events that are tagged for all
		if( Commons.eventsTaggedforAll == null ) {
			String strTagListAll = ( String ) Commons.pandaCache.get( "EventTagList-ALL" );
			if( strTagListAll != null ) {
				Commons.eventsTaggedforAll = ( JsonObject ) Commons.parser.parse( strTagListAll );
				Commons.eventsTaggedforAll.remove( "docType" );
			}
		}

		// synchronized( Commons.eventsTaggedforAll ) {
		if( Commons.eventsTaggedforAll != null ) {
			Set< Entry< String, JsonElement > > entries = Commons.eventsTaggedforAll.entrySet();
			Iterator< Entry< String, JsonElement > > itr = entries.iterator();
			while( itr.hasNext() ) {
				Entry< String, JsonElement > thisEntry = itr.next();
				String eventId = thisEntry.getKey();
				JsonObject aTag = ( JsonObject ) thisEntry.getValue();
				customerState.subscriberEventTags.add( eventId, aTag );
			}
		}
		// }

		customerState.recordType = serviceType;
		customerState.cal = cal;
		customerState.customerId = msisdn;
		customerState.tupleMap = tupleMap;
		customerState.thisRecordDate = callDate;

		customerState.newOfferActions = new ArrayList< JsonObject >();
		customerState.fulfillmentActions = new JsonArray();
		customerState.reminderActions = new JsonArray();

		customerState.triggerSinkMap = new HashMap< String, JsonArray >();
		customerState.processedInd = new LinkedHashSet< String >();

		customerState.siftMath = new SiftMath( customerState );
		customerState.siftMath.commonsState = commonsState;

		return customerState;
	}

	protected void handleScheduledNextWave( long timestamp, String eventType, Date thisDate, Calendar cal ) throws Exception {

		Commons.logger.info( Commons.logPrefix + "Recieved Schedule Beacon : " + timestamp + "-" + commonsState.nWorkerId + "-" + eventType );
		String strSched = ( String ) Commons.pandaCache.get( timestamp + "-" + commonsState.nWorkerId + "-" + eventType );
		if ( strSched == null )
			return;

		Commons.logger.info( Commons.logPrefix + "Schedule Next Wave: " + strSched );

		JsonObject jSched = ( JsonObject )Commons.parser.parse( strSched );
		Set< Map.Entry< String, JsonElement > > entries = jSched.entrySet();
		Iterator< Map.Entry< String, JsonElement > > itr = entries.iterator();
		while( itr.hasNext() ) {
			Map.Entry< String, JsonElement > entry = ( Map.Entry< String, JsonElement > )itr.next();
			String msisdn = ( String )entry.getKey();
			String value = ( ( JsonElement )entry.getValue() ).getAsString();

			CustomerState customerState = getIndicatorsEventsTags( msisdn, new JsonObject(), thisDate, "SCHEDULED_NEXT_WAVE", cal );
			if( customerState.tupleMap == null )
				customerState.tupleMap = new JsonObject();

			String[] events = value.split( "," );
			if( eventType.equals( "NW" ) ) {
				String monitoringEvent = events[ 0 ];
				String behaviouralEvent = events[ 1 ];

				JsonObject thisEventDef = ( JsonObject ) Commons.allEveDef.get( monitoringEvent );
				if( ( thisEventDef == null ) || ( thisEventDef.isJsonNull() ) ) {
					//Commons.logger.warn( "------------- Event Definition not available  : " + behaviouralEvent + " ---------------------------------");
					continue;
				}
				JsonElement ele = Commons.siftConfig.get( "eventLocations" );
				if ( ele == null )
					throw new LocationNotDefinedException("No Event Locations Defined in SiftConfig");

				JsonObject jLoc = ele.getAsJsonObject();
				if ( jLoc.get( behaviouralEvent ) == null ) 
					throw new LocationNotDefinedException("No Location Defined for event Id : " + behaviouralEvent);

				JsonObject eventLoc = jLoc.get( behaviouralEvent ).getAsJsonObject();
				customerState.tupleMap.addProperty( "REQUESTER_ZONE", eventLoc.get( "REQUESTER_ZONE" ).getAsString() );
				customerState.tupleMap.addProperty( "REQUESTER_CHANNEL", eventLoc.get( "REQUESTER_CHANNEL" ).getAsString() );
				customerState.tupleMap.addProperty( "REQUESTER_APPLICATION", eventLoc.get( "REQUESTER_APPLICATION" ).getAsString() );
				customerState.tupleMap.addProperty( "REQUESTER_LOCATION", eventLoc.get( "REQUESTER_LOCATION" ).getAsString() );

				JsonObject aTag = new JsonObject();
				int result = processEvent( aTag, monitoringEvent, monitoringEvent, thisEventDef, true, customerState );

				Commons.logger.info( Commons.logPrefix + behaviouralEvent + " - Execution Result : " + result );
				if ( result == 0 ) {
					String targetSystemId = "JMSLB1";
					if (thisEventDef.has( "targetSystemID") ) {
						JsonElement targetSystemIdEle = thisEventDef.get( "targetSystemID" );
						if ( ( targetSystemIdEle != null ) && ( ! targetSystemIdEle.isJsonNull() ) )
							targetSystemId = targetSystemIdEle.getAsString();
					}

					JsonElement progElem = thisEventDef.get( "programs" );
					if ( ( progElem == null ) || ( progElem.isJsonNull() ) || ( progElem.getAsJsonArray().size() == 0 ) )
						triggerEvent(thisEventDef, customerState.subscriberTriggers, monitoringEvent, behaviouralEvent, aTag, true, eventType, customerState, targetSystemId);
					else {
						processPrograms( thisEventDef, behaviouralEvent, aTag, customerState );
						processAllActions( customerState );
					}
				}
			}
			commonsState.subscriberIndicatorsMap.put( msisdn, customerState.subscriberIndicators );
			Commons.pandaCache.set( msisdn + "-Indicators", 0, customerState.subscriberIndicators.toString() );
		}
		Commons.pandaCache.delete( timestamp + "-" + commonsState.nWorkerId + "-" + eventType );
	}

	protected void handleScheduledReminder( long timestamp, String eventType, Date thisDate, Calendar cal ) throws Exception {
		Commons.logger.info( Commons.logPrefix + "Recieved Reminder Beacon : " + timestamp + " - " + eventType );
		String strSched = ( String ) Commons.pandaCache.get( timestamp + "-" + commonsState.nWorkerId + "-" + eventType );
		if( strSched == null )
			return;
		Commons.logger.info( Commons.logPrefix + "Schedule : " + strSched );

		JsonObject jSched = ( JsonObject ) Commons.parser.parse( strSched );
		Set< Entry< String, JsonElement > > entries = jSched.entrySet();
		Iterator< Entry< String, JsonElement > > itr = entries.iterator();
		while( itr.hasNext() ) {
			Entry< String, JsonElement > entry = itr.next();
			String msisdn = entry.getKey();
			String value = entry.getValue().getAsString();

			CustomerState customerState = this.getIndicatorsEventsTags( msisdn, new JsonObject(), thisDate, "SCHEDULED_REMINDER", cal );
			if( customerState.tupleMap == null )
				customerState.tupleMap = new JsonObject();

			String[ ] events = value.split( "," );

			if( eventType.equals( "SE" ) ) {
				String monitoringEvent = events[ 0 ];
				String scheduledEventId = events[ 1 ];

				// execute the scheduled reminder for this msisdn and event it.

				JsonObject thisEventDef = new JsonObject();

				thisEventDef = Commons.allEveDef.get( scheduledEventId );
				thisEventDef.addProperty( "taggedForAll", true );

				JsonObject aTag = thisEventDef;

				if( thisEventDef == null || thisEventDef.isJsonNull() ) {
					//Commons.logger.warn( "------------- Event Definition not available  : " + scheduledEventId + " ---------------------------------" );
					continue;
				}
				int result = processEvent( thisEventDef, scheduledEventId, monitoringEvent, thisEventDef, true, customerState );

				Commons.logger.info( Commons.logPrefix + scheduledEventId + " - Execution Result : " + result );
				if( result == 0 ) {
					String targetSystemId = "JMSLB1";
					if( thisEventDef.has( "targetSystemID" ) ) {
						JsonElement targetSystemIdEle = thisEventDef.get( "targetSystemID" );
						if( targetSystemIdEle != null && !targetSystemIdEle.isJsonNull() )
							targetSystemId = targetSystemIdEle.getAsString();
					}
					if( !targetSystemId.contains( "JMSLB1" ) )
						triggerEvent( thisEventDef, customerState.subscriberTriggers, monitoringEvent, scheduledEventId, aTag, true, eventType, customerState, targetSystemId );
					else {
						this.processPrograms( thisEventDef, scheduledEventId, aTag, customerState );
						this.processAllActions( customerState );
					}
				}
			}else if( ! eventType.equals( "NE" ) ) {
				String monitoringEvent = events[ 0 ];
				String scheduledEventId = events[ 1 ];

				// execute the scheduled reminder for this msisdn and event it.
				JsonObject aTag = ( JsonObject ) customerState.subscriberEventTags.get( monitoringEvent );
				if( aTag == null ) {
					Commons.logger.info( Commons.logPrefix + "Event Tags in memory : " + customerState.subscriberEventTags.toString() );
					Commons.logger.info( Commons.logPrefix + "Unable to get Tags for : " + monitoringEvent );
					continue;
				}
				Commons.logger.info( Commons.logPrefix + "tag definition : " + aTag.toString() );

				JsonObject thisEventDef = Commons.allEveDef.get( scheduledEventId );
				if( thisEventDef == null || thisEventDef.isJsonNull() ) {
					//Commons.logger.warn( "------------- Event Definition not available  : " + scheduledEventId + " ---------------------------------" );
					continue;
				}

				if( aTag.has( "OFFER_ID" ) ) {
					if( aTag.has( "REQUESTER_LOCATION" ) && ! aTag.get( "REQUESTER_LOCATION" ).isJsonNull() )
						customerState.tupleMap.addProperty( "REQUESTER_LOCATION", aTag.get( "REQUESTER_LOCATION" ).getAsString() );
					else {
						JsonElement ele = Commons.siftConfig.get( "eventLocations" );
						String behEventId = aTag.get( "BEHAVIOURAL_TRIGGER" ).getAsString();
						if( ele == null )
							throw new LocationNotDefinedException( "No Event Locations Defined in SiftConfig" );
						JsonObject jLoc = ele.getAsJsonObject();
						if( jLoc.get( behEventId ) == null )
							throw new LocationNotDefinedException( "No Location Defined for event Id : " + behEventId );

						JsonObject eventLoc = jLoc.get( behEventId ).getAsJsonObject();
						customerState.tupleMap.addProperty( "REQUESTER_LOCATION", eventLoc.get( "REQUESTER_LOCATION" ).getAsString() );
					}

					String offerId = aTag.get( "OFFER_ID" ).getAsString();

					JsonObject offerDef = Commons.allOfferDef.get( offerId ).getAsJsonObject();
					if( offerDef.has( "RESPONSE_CHANNEL" ) && ! offerDef.get( "RESPONSE_CHANNEL" ).isJsonNull() ) {
						customerState.tupleMap.addProperty( "REQUESTER_ZONE", offerDef.get( "RESPONSE_ZONE" ).getAsString() );
						customerState.tupleMap.addProperty( "REQUESTER_CHANNEL", offerDef.get( "RESPONSE_CHANNEL" ).getAsString() );
						customerState.tupleMap.addProperty( "REQUESTER_APPLICATION", offerDef.get( "RESPONSE_APPLICATION" ).getAsString() );
					} else {
						customerState.tupleMap.addProperty( "REQUESTER_ZONE", "OUTBOUND" );
						customerState.tupleMap.addProperty( "REQUESTER_CHANNEL", "SMS" );
						customerState.tupleMap.addProperty( "REQUESTER_APPLICATION", "OUTBOUND" );
					}
				}
				int result = processEvent( aTag, scheduledEventId, monitoringEvent, thisEventDef, false, customerState );

				Commons.logger.info( Commons.logPrefix + scheduledEventId + " - Execution Result : " + result );
				if( result == 0 ) {
					String targetSystemId = "JMSLB1";
					if( thisEventDef.has( "targetSystemID" ) ) {
						JsonElement targetSystemIdEle = thisEventDef.get( "targetSystemID" );
						if( targetSystemIdEle != null && !targetSystemIdEle.isJsonNull() )
							targetSystemId = targetSystemIdEle.getAsString();
					}
					if( !targetSystemId.contains( "JMSLB1" ) )
						triggerEvent( thisEventDef, customerState.subscriberTriggers, monitoringEvent, scheduledEventId, aTag, true, eventType, customerState, targetSystemId );
					else {
						this.processPrograms( thisEventDef, scheduledEventId, aTag, customerState );
						this.processAllActions( customerState );
					}
				}
			}else {
				Commons.logger.info( Commons.logPrefix + "Executing no-event for " + value );
				String programId = events[ 0 ];
				String offerId = events[ 1 ];
				String noEventTriggerId = events[ 2 ];
				String flowId = events[ 3 ];
				String monEventId = events[ 4 ];

				JsonObject aTag = ( JsonObject ) customerState.subscriberEventTags.get( monEventId );
				if( aTag == null ) {
					Commons.logger.info( Commons.logPrefix + "Event Tags in memory : " + customerState.subscriberEventTags.toString() );
					Commons.logger.info( Commons.logPrefix + "Unable to get Tags for : " + noEventTriggerId );
					continue;
				}
				Commons.logger.info( Commons.logPrefix + "tag definition : " + aTag.toString() );

				JsonElement thisProgramHistory;
				thisProgramHistory = customerState.subscriberPrograms.get( programId );

				JsonObject offerDef = Commons.allOfferDef.get( offerId ).getAsJsonObject();
				if( offerDef.has( "RESPONSE_CHANNEL" ) && ! offerDef.get( "RESPONSE_CHANNEL" ).isJsonNull() ) {
					customerState.tupleMap.addProperty( "REQUESTER_ZONE", offerDef.get( "RESPONSE_ZONE" ).getAsString() );
					customerState.tupleMap.addProperty( "REQUESTER_CHANNEL", offerDef.get( "RESPONSE_CHANNEL" ).getAsString() );
					customerState.tupleMap.addProperty( "REQUESTER_APPLICATION", offerDef.get( "RESPONSE_APPLICATION" ).getAsString() );
				} else {
					customerState.tupleMap.addProperty( "REQUESTER_ZONE", "OUTBOUND" );
					customerState.tupleMap.addProperty( "REQUESTER_CHANNEL", "SMS" );
					customerState.tupleMap.addProperty( "REQUESTER_APPLICATION", "OUTBOUND" );
				}
				if( aTag.has( "REQUESTER_LOCATION" ) && ! aTag.get( "REQUESTER_LOCATION" ).isJsonNull() )
					customerState.tupleMap.addProperty( "REQUESTER_LOCATION", aTag.get( "REQUESTER_LOCATION" ).getAsString() );
				else {
					JsonElement ele = Commons.siftConfig.get( "eventLocations" );
					String behEventId = aTag.get( "BEHAVIOURAL_TRIGGER" ).getAsString();
					if( ele == null )
						throw new LocationNotDefinedException( "No Event Locations Defined in SiftConfig" );
					JsonObject jLoc = ele.getAsJsonObject();
					if( jLoc.get( behEventId ) == null )
						throw new LocationNotDefinedException( "No Location Defined for event Id : " + behEventId );

					JsonObject eventLoc = jLoc.get( behEventId ).getAsJsonObject();
					customerState.tupleMap.addProperty( "REQUESTER_LOCATION", eventLoc.get( "REQUESTER_LOCATION" ).getAsString() );
				}

				JsonArray thisProgramHistoryArray = thisProgramHistory.getAsJsonArray();
				JsonObject latestProgramInstance = thisProgramHistoryArray.get( thisProgramHistoryArray.size() - 1 ).getAsJsonObject();
				JsonElement jele = latestProgramInstance.get( offerId );
				JsonArray offerHistory = jele.getAsJsonArray();
				for( int i = offerHistory.size() - 1; i >= 0; i-- ) {
					JsonObject currentOffer = offerHistory.get( i ).getAsJsonObject();
					if( currentOffer.get( "flowId" ).getAsString().equals( flowId ) && !currentOffer.has( "fulfillmentAction" ) ) {
						Commons.logger.info( Commons.logPrefix + "No fulfillment exist, executing the program" );
						this.processPrograms( Commons.allEveDef.get( noEventTriggerId ), noEventTriggerId, aTag, customerState );
						this.processAllActions( customerState );
						break;
					}
				}
			}
			commonsState.subscriberIndicatorsMap.put( msisdn, customerState.subscriberIndicators );
			Commons.pandaCache.set( msisdn + "-Indicators", 0, customerState.subscriberIndicators.toString() );
			// }
		}
		Commons.pandaCache.delete( timestamp + "-" + commonsState.nWorkerId + "-" + eventType );
	}

	protected int processEvent( JsonObject aTag, String eventId, String mainEventId, JsonObject thisEventDef, boolean taggedForAll, CustomerState customerState ) throws Exception {
		Commons.logger.info( Commons.logPrefix + "Tagged For All : " + taggedForAll );
		Commons.logger.info( Commons.logPrefix + "tag definition : " + aTag.toString() );
		Commons.logger.info( Commons.logPrefix + "Event definition : " + thisEventDef.toString() );

		SiftMath.thisTag = aTag;

		PExpressionEvaluator exp = Commons.expMap.get( eventId );

		// Perform Window Checks
		if( ! taggedForAll ) {
			long startTime = aTag.get( "MONITORING_START_TIME" ).getAsLong();
			long endTime = aTag.get( "MONITORING_END_TIME" ).getAsLong();
			long curTime = customerState.thisRecordDate.getTime();
			Commons.logger.info( Commons.logPrefix + "Start : " + startTime + " current : " + curTime + " End : " + endTime );
			if( curTime < startTime || curTime > endTime ) {
				Commons.logger.info( Commons.logPrefix + "Date Check Failed" );
				return 3;
			}
		}

		if( exp == null ) {
			System.out.println( "***********Expression not available: Subscriber: " + customerState.customerId + ", EventID: " + eventId + ", Maine EventId: " + mainEventId );
			return 4; // TODO: REMOVE THIS RETURN STATEMENT.
		}

		Object expressionValue = evaluateExpression( eventId, aTag, false, mainEventId, customerState );
		Commons.logger.info( Commons.logPrefix + "Event : " + eventId + " Value : " + expressionValue.toString() );
		Commons.logger.info( Commons.logPrefix + "--------------------------------------------------------------------" );
		if( expressionValue.toString().equals( "false" ) )
			return 4;
		return 0;
	}

	protected void processAllEvents( CustomerState customerState ) throws Exception {
		// Iterate over the taglists and process one event at a time.
		if( Commons.filterEvents && Commons.EventsToBeProcessed.equals( "NONE" ) )
			return;

		Set< Entry< String, JsonElement > > entries = customerState.subscriberEventTags.entrySet();
		Iterator< Entry< String, JsonElement > > itr = entries.iterator();

		while( itr.hasNext() ) {
			Entry< String, JsonElement > thisEntry = itr.next();
			String eventId = thisEntry.getKey();
			JsonObject thisEventDef = Commons.allEveDef.get( eventId );
			JsonObject aTag = ( JsonObject ) thisEntry.getValue();
			if( thisEventDef == null || thisEventDef.isJsonNull() ) {
				Commons.logger.info( Commons.logPrefix + "------------- Event Definition not available  : " + eventId + " ---------------------------------" );
				continue;
			}

			if( Commons.filterEvents && Commons.EventsToBeProcessed.equals( "MONITORING_EVENTS" ) && thisEventDef.get( "taggedForAll" ).getAsBoolean() ) {
				Commons.logger.info( Commons.logPrefix + "-------Skipping the event as EventsToBeProcessed: " + Commons.EventsToBeProcessed + " and eventType: " + thisEventDef.get( "type" ).getAsString() );
				continue;
			}

			if( Commons.filterEvents && Commons.EventsToBeProcessed.equals( "FULFILLMENT_EVENTS" ) && thisEventDef.get( "type" ).getAsString().equals( "Fulfillment" ) ) {
				Commons.logger.info( Commons.logPrefix + "-------Skipping the event as EventsToBeProcessed: " + Commons.EventsToBeProcessed + " and eventType: " + thisEventDef.get( "type" ).getAsString() );
				continue;
			}
			String recordType = customerState.tupleMap.get( "RECORD_TYPE" ).getAsString();
			String intent = "";
			if( recordType.equals( "HANDLE_CUSTOMER_INTENT" ) )
				intent = customerState.tupleMap.get( "INTENT" ).getAsString();

			if( ( recordType.equals( "HANDLE_CUSTOMER_INTENT" ) && ( intent.equals( "REDEEM" ) || intent.equals( "ACCEPT" ) || intent.equals( "BUY" ) )
					&& ( !aTag.has( "FLOW_ID" ) || ( aTag.has( "FLOW_ID" ) && !customerState.tupleMap.get( "FLOW_ID" ).getAsString().equals( aTag.get( "FLOW_ID" ).getAsString() ) ) ) )
					|| ( recordType.equals( "GET_OFFER" ) && !thisEventDef.get( "taggedForAll" ).getAsBoolean() && !thisEventDef.get( "type" ).getAsString().equals( "NonBehavioural" ) ) )
				continue;

			if( recordType.equals( "REGISTER_ACTION" ) && aTag.has( "FLOW_ID" ) && !customerState.tupleMap.get( "FLOW_ID" ).getAsString().equals( aTag.get( "FLOW_ID" ).getAsString() ) )
				continue;

			Commons.logger.info( Commons.logPrefix + "------------------ Processing Event : " + eventId + " ---------------------------------" );

			boolean recordTypeCheck = Commons.siftConfig.get( "recordTypeCheckForIndicators" ).getAsBoolean();
			String thisExpression = thisEventDef.get( "expression" ).getAsString();
			if( recordTypeCheck && thisExpression.contains( "RECORD_TYPE.equals" ) ) {
				if( !thisExpression.contains( "\"" + recordType + "\"" ) ) {
					Commons.logger.info( Commons.logPrefix + "Skipping this Event " + eventId + "  as RECORD_TYPE does not match" );
					continue;
				}
			}

			JsonElement jtagFlag = thisEventDef.get( "taggedForAll" );
			boolean taggedForAll = false;
			if( jtagFlag == null )
				taggedForAll = true;
			else
				taggedForAll = jtagFlag.getAsBoolean();

			JsonElement elem = thisEventDef.get( "programs" );
			if( ( elem == null || elem.isJsonNull() ) && isDailyTriggerLimitExceeded( eventId, thisEventDef ) )
				continue;

			// Perform Window Checks
			if( taggedForAll ) {
				JsonElement startTimeEle = thisEventDef.get( "eventStartTime" );
				JsonElement endTimeEle = thisEventDef.get( "eventEndTime" );
				if( startTimeEle != null && !startTimeEle.isJsonNull() && endTimeEle != null && !endTimeEle.isJsonNull() ) {
					long startTime = startTimeEle.getAsLong();
					long endTime = endTimeEle.getAsLong();
					long curTime = customerState.siftMath.getCurrentTime();
					Commons.logger.info( Commons.logPrefix + "Start : " + startTime + " current : " + curTime + " End : " + endTime );

					if( curTime < startTime || curTime > endTime ) {
						Commons.logger.info( Commons.logPrefix + "Date Check Failed for the event: " + eventId );
						continue;
					}
				}
			}

			String targetSystemId = "JMSLB1";
			if( thisEventDef.has( "targetSystemID" ) ) {
				JsonElement targetSystemIdEle = thisEventDef.get( "targetSystemID" );
				if( targetSystemIdEle != null && !targetSystemIdEle.isJsonNull() )
					targetSystemId = targetSystemIdEle.getAsString();
			}

			int result = processEvent( aTag, eventId, eventId, thisEventDef, taggedForAll, customerState );

			// handle activity reminders for the events-only engine
			if( result != 0 && !targetSystemId.contains( "JMSLB1" ) ) {
				if( !taggedForAll && aTag.get( "ACTIVITY_TRIGGERED_REMINDER_COUNT" ).getAsInt() > 0 ) {
					JsonElement eActivityTriggeredReminderId = thisEventDef.get( "activityTriggeredReminderCondition" );
					if( eActivityTriggeredReminderId != null ) {
						JsonArray arrActivityTriggeredReminders = eActivityTriggeredReminderId.getAsJsonArray();
						for( int i = 0; i < arrActivityTriggeredReminders.size(); i++ ) {
							String actTrigEvent = arrActivityTriggeredReminders.get( i ).getAsString();
							Commons.logger.info( Commons.logPrefix + "---------Processing Activity Triggered Reminder " + actTrigEvent + "---------------" );
							if( processEvent( aTag, actTrigEvent, eventId, thisEventDef, taggedForAll, customerState ) == 0 )
								triggerEvent( thisEventDef, customerState.subscriberTriggers, eventId, actTrigEvent, aTag, !taggedForAll, "IRE", customerState, targetSystemId );
						}
					}
				}
			}
			Commons.logger.info( Commons.logPrefix + eventId + " - Execution Result for : " + eventId + " === " + result + " === Record type: " + recordType + " === MSISDN: " + customerState.customerId );
			String tagOfferId = "NA", tagProgId = "NA";
			if( aTag.has( "OFFER_ID" ) )
				tagOfferId = aTag.get( "OFFER_ID" ).getAsString();
			if( aTag.has( "PROGRAM_ID" ) )
				tagProgId = aTag.get( "PROGRAM_ID" ).getAsString();
			if( result == 4 ) {
				if( !taggedForAll && customerState.tupleMap.has( "FLOW_ID" ) && customerState.tupleMap.get( "FLOW_ID" ).getAsString().equals( aTag.get( "FLOW_ID" ).getAsString() ) )
					this.handleActionFailureReasons( customerState, customerState.customerId, "Fulfillment Criteria Does not Match", "004", tagProgId, tagOfferId );
				continue;
			}else if( result == 3 ) {
				if( !taggedForAll && customerState.tupleMap.has( "FLOW_ID" ) && customerState.tupleMap.get( "FLOW_ID" ).getAsString().equals( aTag.get( "FLOW_ID" ).getAsString() ) )
					this.handleActionFailureReasons( customerState, customerState.customerId, "Customer Sends the campaign response after the offer validity date is expired", "003", tagProgId, tagOfferId );
				continue;
			}

			if( elem == null || elem.isJsonNull() || elem.getAsJsonArray().size() == 0 )
				triggerEvent( thisEventDef, customerState.subscriberTriggers, eventId, eventId, aTag, !taggedForAll, "FE", customerState, targetSystemId );
			else {
				//				if( customerState.tupleMap.get( "REQUESTER_LOCATION" ) == null ) {
				if( ! customerState.recordType.equals( "GET_OFFER" ) && ! customerState.recordType.equals( "HANDLE_CUSTOMER_INTENT" ) ) {
					if( taggedForAll ) {
						JsonElement ele = Commons.siftConfig.get( "eventLocations" );
						if( ele == null )
							throw new LocationNotDefinedException( "No Event Locations Defined in SiftConfig" );
						JsonObject jLoc = ele.getAsJsonObject();
						if( jLoc.get( eventId ) == null )
							throw new LocationNotDefinedException( "No Location Defined for event Id : " + eventId );

						JsonObject eventLoc = jLoc.get( eventId ).getAsJsonObject();
						customerState.tupleMap.addProperty( "REQUESTER_ZONE", eventLoc.get( "REQUESTER_ZONE" ).getAsString() );
						customerState.tupleMap.addProperty( "REQUESTER_CHANNEL", eventLoc.get( "REQUESTER_CHANNEL" ).getAsString() );
						customerState.tupleMap.addProperty( "REQUESTER_APPLICATION", eventLoc.get( "REQUESTER_APPLICATION" ).getAsString() );
						customerState.tupleMap.addProperty( "REQUESTER_LOCATION", eventLoc.get( "REQUESTER_LOCATION" ).getAsString() );
					}else {
						if( aTag.has( "REQUESTER_LOCATION" ) && ! aTag.get( "REQUESTER_LOCATION" ).isJsonNull() )
							customerState.tupleMap.addProperty( "REQUESTER_LOCATION", aTag.get( "REQUESTER_LOCATION" ).getAsString() );
						else {
							JsonElement ele = Commons.siftConfig.get( "eventLocations" );
							String behEventId = aTag.get( "BEHAVIOURAL_TRIGGER" ).getAsString();
							if( ele == null )
								throw new LocationNotDefinedException( "No Event Locations Defined in SiftConfig" );
							JsonObject jLoc = ele.getAsJsonObject();
							if( jLoc.get( behEventId ) == null )
								throw new LocationNotDefinedException( "No Location Defined for event Id : " + behEventId );

							JsonObject eventLoc = jLoc.get( behEventId ).getAsJsonObject();
							customerState.tupleMap.addProperty( "REQUESTER_LOCATION", eventLoc.get( "REQUESTER_LOCATION" ).getAsString() );
						}
						String offerId = aTag.get( "OFFER_ID" ).getAsString();

						JsonObject offerDef = Commons.allOfferDef.get( offerId ).getAsJsonObject();
						if( offerDef.has( "RESPONSE_CHANNEL" ) && ! offerDef.get( "RESPONSE_CHANNEL" ).isJsonNull() ) {
							customerState.tupleMap.addProperty( "REQUESTER_ZONE", offerDef.get( "RESPONSE_ZONE" ).getAsString() );
							customerState.tupleMap.addProperty( "REQUESTER_CHANNEL", offerDef.get( "RESPONSE_CHANNEL" ).getAsString() );
							customerState.tupleMap.addProperty( "REQUESTER_APPLICATION", offerDef.get( "RESPONSE_APPLICATION" ).getAsString() );
						} else {
							customerState.tupleMap.addProperty( "REQUESTER_ZONE", "OUTBOUND" );
							customerState.tupleMap.addProperty( "REQUESTER_CHANNEL", "SMS" );
							customerState.tupleMap.addProperty( "REQUESTER_APPLICATION", "OUTBOUND" );
						}
					}
				}
				processPrograms( thisEventDef, eventId, aTag, customerState );
			}
		}
		this.processAllActions( customerState );
	}

	public void processAllActions( CustomerState customerState ) throws Exception {
		// submit all the event triggers if there are any
		for( Map.Entry< String, JsonArray > entry : customerState.triggerSinkMap.entrySet() ) {
			String message = entry.getValue().toString();
			Commons.logger.info( Commons.logPrefix + "Attempt to send triggerSink Message to SO : " + message );

			outputCollector.emit( "record", new Values( entry.getKey(), message ) );
			Commons.logger.info( Commons.logPrefix + "Sent triggerSink Message to SO : " + message );
		}

		// when called from scheduled reminder or no-event reminder, the
		// tuplemap is sent as null. So need to initialise
		if( customerState.tupleMap == null ) {
			customerState.tupleMap = new JsonObject();
		}

		processAllFulfillmentActions( customerState );
		processAllNewOfferActions( customerState );
		processAllReminderActions( customerState );

		communicaTeToSO( customerState, "JMSLB1" );

		// Send failure reason only in case of outbound.\
		// send the error messages too
		Iterator< Entry< String, JsonElement > > erritr = customerState.siftOutboundNoActionReasons.entrySet().iterator();
		while( erritr.hasNext() ) {
			Entry< String, JsonElement > entry = erritr.next();
			JsonObject errorObj = entry.getValue().getAsJsonObject();

			outputCollector.emit( "record", new Values( "JMSLB1", errorObj.toString() ) );
			Commons.logger.info( Commons.logPrefix + "Sent Error Message to SO : " + errorObj.toString() );
		}

		// Process Next Wave Events Here
		if( customerState.subscriberNextWaveEventTags == null )
			return;
		if( customerState.subscriberNextWaveEventTags != null ) {
			String strTagList = ( String ) Commons.pandaCache.get( "EventTagList-" + customerState.customerId );
			JsonObject jTagList = null;
			if( strTagList == null ) {
				jTagList = new JsonObject();
				jTagList.addProperty( "docType", "TagList" );
			}else
				jTagList = ( JsonObject ) Commons.parser.parse( strTagList );

			Iterator< Entry< String, JsonElement > > itr = customerState.subscriberNextWaveEventTags.entrySet().iterator();
			while( itr.hasNext() ) {
				Entry< String, JsonElement > thisEntry = itr.next();
				JsonObject thisWaveObject = thisEntry.getValue().getAsJsonObject();
				double duration = thisWaveObject.get( "MONITORING_DURATION" ).getAsDouble();
				if( duration != -1 )
					jTagList.add( thisEntry.getKey(), thisWaveObject );
			}
			Commons.pandaCache.set( "EventTagList-" + customerState.customerId, 0, jTagList.toString() );
			jTagList.remove( "docType" );
			commonsState.eventsTaggedForSubscribersMap.put( customerState.customerId, jTagList );
		}
	}

	private void populateTupleMap( CustomerState customerState, String internalRecordType, String offerId, String countAsContact, String programId, JsonObject thisAction ) {
		customerState.tupleMap.addProperty( "SIFT_INTERNAL_RECORD_TYPE", internalRecordType );
		customerState.tupleMap.addProperty( "OFFER_CATEGORY", Commons.allOfferDef.get( offerId ).getAsJsonObject().get( "category" ).getAsString() );
		customerState.tupleMap.addProperty( "OFFER_ID", offerId );
		customerState.tupleMap.addProperty( "CONTACT_TIME", Long.toString( customerState.siftMath.getCurrentTime() ) );
		customerState.tupleMap.addProperty( "IS_SIMULATED", thisAction.get( "IS_SIMULATED" ).getAsString() );
		customerState.tupleMap.addProperty( "COUNT_AS_CONTACT", countAsContact );
		customerState.tupleMap.addProperty( "OFFER_TYPE", thisAction.get( "offerType" ).getAsString() );
		customerState.tupleMap.addProperty( "PROGRAM_ID", programId );
	}

	// handles the conditional fulfillments and unconditional fulfillments
	protected void processAllFulfillmentActions( CustomerState customerState ) throws Exception {
		// If the action appears in the list, Max Fulfillment per subscriber has
		// not been reached. The check was already done in isActionApplicable.
		// Max Fulfillment Across Subscriber will be handled as part of the
		// product criteria expressions by using the getContactCounts() function
		// So here its just to set Control Group logic and to Send out the
		// messages.
		long timestamp = customerState.siftMath.getCurrentTime();
		for( int i = 0; i < customerState.fulfillmentActions.size(); i++ ) {
			JsonObject thisAction = customerState.fulfillmentActions.get( i ).getAsJsonObject();
			JsonObject thisTagListItem = thisAction.get( "tagList" ).getAsJsonObject();
			String programId = thisAction.get( "programId" ).getAsString();
			String offerId = thisAction.get( "offerId" ).getAsString();
			String offerType = thisAction.get( "offerType" ).getAsString();
			String actionType = thisAction.get( "actionType" ).getAsString();
			String eventId = thisAction.get( "actualTriggeredEvent" ).getAsString();
			String internalRecordType = "FulfillmentCount";

			Boolean isSimulated = false;
			if( Commons.allProgramDef.get( programId ).getAsJsonObject().get( "status" ).getAsString().equals( "Simulation" ) )
				isSimulated = true;

			String countAsContact = "Y";
			if( thisAction.has( "isCountAsContact" ) && thisAction.get( "isCountAsContact" ) != null && !thisAction.get( "isCountAsContact" ).isJsonNull() ) {
				String expId = programId + "_" + offerId + "_" + "COUNT_AS_CONTACT";
				Object result = this.evaluateExpression( expId, thisTagListItem, false, eventId, customerState );
				if( result != null )
					countAsContact = result.toString();
			}

			populateTupleMap( customerState, internalRecordType, offerId, countAsContact, programId, thisAction );

			Commons.logger.info( Commons.logPrefix + "Processing fulfilment for offer: " + offerId );

			boolean isControl = false;
			// ArrayList< String > applicableChannels = new ArrayList< String
			// >();
			String flowId = customerState.customerId + siftRandomNumberGenerator();
			// String flowId = customerState.customerId +
			// Math.round(Math.random() * 100000);
			String participationType = "AUTO";
			String fulfillmentType = "AUTO";

			if( offerType.equals( "Conditional" ) ) {

				// Verify Max Fulfilments Per Subscriber.
				String expKey = "MAX_FULFILLMENTS_" + offerId;
				Object fulfilResult = this.evaluateExpression( expKey, thisTagListItem, true, eventId, customerState );
				if( fulfilResult.toString().equals( "false" ) ) {
					Commons.logger.info( Commons.logPrefix + "Max Fulfilment Count Expression Failed. Skipping the fulfilment." );
					continue;
				}

				isControl = thisTagListItem.get( "IS_CONTROL" ).getAsBoolean();
				flowId = thisTagListItem.get( "FLOW_ID" ).getAsString();

				if( customerState.recordType.equals( "HANDLE_CUSTOMER_INTENT" ) && 
						customerState.tupleMap.has( "FLOW_ID" ) && ! customerState.tupleMap.get( "FLOW_ID" ).getAsString().equals( flowId ) ) {
					Commons.logger.info( Commons.logPrefix + "Flow id does not match. Skipping the fulfilment" );
					continue;
				}
				if( thisTagListItem.has( "IS_SIMULATED" ) ) {
					if( thisTagListItem.has( "IS_SIMULATED" ) ) {
						isSimulated = thisTagListItem.get( "IS_SIMULATED" ).getAsBoolean();
						customerState.tupleMap.addProperty( "IS_SIMULATED", isSimulated.toString() );
					}

					customerState.tupleMap.addProperty( "IS_SIMULATED", isSimulated.toString() );
				}

				JsonElement eParticipationType = thisAction.get( "participationType" );
				if( eParticipationType != null && !eParticipationType.isJsonNull() & !eParticipationType.getAsString().equals( "" ) ) {
					String key = "PARTICIPATION_TYPE_" + offerId;
					Object result = evaluateExpression( key, thisTagListItem, false, eventId, customerState );
					participationType = result.toString();
				}

			}else if( offerType.equals( "UnConditional" ) && !customerState.isBuyNotificationOnline ) {
				String skipGlobalContactPolicy = thisAction.get( "skipGlobalContactPolicy" ).getAsString();
				String alwaysOnOffer = thisAction.get( "alwaysOnOffer" ).getAsString();
				if( skipGlobalContactPolicy.equals( "N" ) && alwaysOnOffer.equals( "N" ) ) {
					boolean globalContactPolicy = applyGlobalContactPolicy( thisAction, customerState, eventId, offerId, programId, null );
					if( !globalContactPolicy )
						continue;
				}else {
					//manageFlowFunnelCounts( eventId, programId, offerId, "GLOBAL_CONTACT_POLICY_SKIPPED", customerState );
					Commons.logger.info( Commons.logPrefix + "Skipping Global contact rules check for offer: " + offerId );
				}
				if( alwaysOnOffer.equals( "N" ) ) {
					if( !applyContactPolicy( thisAction, customerState, timestamp, eventId ) )
						continue;
				}else {
					Commons.logger.info( Commons.logPrefix + "Skipping offer level contact rules check for offer: " + offerId );
					//manageFlowFunnelCounts( eventId, programId, offerId, "OFFER_CONTACT_POLICY_SKIPPED", customerState );
				}
			}

			JsonObject thisEventDef = thisAction.get( "eventDef" ).getAsJsonObject();
			JsonObject triggerMessage = processFulfillmentProductsMessages( thisAction, programId, offerId, thisEventDef, eventId, thisTagListItem, timestamp, null, eventId, customerState, flowId );
			triggerMessage.addProperty( "FLOW_ID", flowId );
			triggerMessage.addProperty( "IS_CONTROL", isControl );
			triggerMessage.addProperty( "IS_SIMULATED", isSimulated );
			triggerMessage.addProperty( "PARTICIPATION_TYPE", participationType );
			triggerMessage.addProperty( "COUNT_AS_CONTACT", countAsContact );

			if( thisTagListItem.has( "REQUEST_ID" ) )
				triggerMessage.addProperty( "ORIG_REQUEST_ID", thisTagListItem.get( "REQUEST_ID" ).getAsString() );
			else
				triggerMessage.addProperty( "ORIG_REQUEST_ID", customerState.customerId + "-" + SiftMath.getCurrentTime() );

			if( offerType.equals( "Conditional" ) ) {
				long monEndDate = thisTagListItem.get( "MONITORING_END_TIME" ).getAsLong();
				triggerMessage.addProperty( "OFFER_START_DATE", thisTagListItem.get( "MONITORING_START_TIME" ).getAsLong() );
				triggerMessage.addProperty( "OFFER_END_DATE", monEndDate );
				triggerMessage.addProperty( "OFFER_END_DATE_LTZ", ( monEndDate - java.util.TimeZone.getDefault().getOffset( monEndDate ) ) );
			}
			//			else {
			//				String reqId = customerState.customerId + "-" + SiftMath.getCurrentTime();
			//				triggerMessage.addProperty( "REQUEST_ID", reqId );
			//			}

			JsonElement targetSystemIdele = thisEventDef.get( "targetSystemID" );
			String targetSystemId = "JMSLB1";
			if( targetSystemIdele != null && !targetSystemIdele.isJsonNull() && !targetSystemIdele.getAsString().equals( "-1" ) )
				targetSystemId = targetSystemIdele.getAsString();
			if( !customerState.isBuyNotificationOnline && !this.gatherResponse( customerState, targetSystemId, triggerMessage, actionType, false ) )
				continue;

			this.handleCommunicationHistory( thisEventDef, triggerMessage, thisAction, timestamp, programId, offerId, actionType, flowId, eventId, isControl, customerState );
		}
	}

	// handles new offer notifications and monitoring offer notifications
	private void processAllNewOfferActions( CustomerState customerState ) throws Exception {
		// TODO : Sort the offers based on the score
		Collections.sort( customerState.newOfferActions, new OfferPriorityScoreComparator() );
		long timestamp = customerState.siftMath.getCurrentTime();
		for( int i = 0; i < customerState.newOfferActions.size(); i++ ) {
			JsonObject thisAction = customerState.newOfferActions.get( i ).getAsJsonObject();
			JsonObject thisTagListItem = thisAction.get( "tagList" ).getAsJsonObject();
			String programId = thisAction.get( "programId" ).getAsString();
			String offerId = thisAction.get( "offerId" ).getAsString();
			String actionType = thisAction.get( "actionType" ).getAsString();
			String eventId = thisAction.get( "actualTriggeredEvent" ).getAsString();
			ArrayList< String > applicableChannels = new ArrayList< String >();
			String skipGlobalContactPolicy = thisAction.get( "skipGlobalContactPolicy" ).getAsString();
			String alwaysOnOffer = thisAction.get( "alwaysOnOffer" ).getAsString();
			String internalRecordType = "ContactCount";

			String countAsContact = "Y";
			if( thisAction.has( "isCountAsContact" ) && thisAction.get( "isCountAsContact" ) != null && !thisAction.get( "isCountAsContact" ).isJsonNull() ) {
				String expId = programId + "_" + offerId + "_" + "COUNT_AS_CONTACT";
				Object result = this.evaluateExpression( expId, thisTagListItem, false, eventId, customerState );
				if( result != null )
					countAsContact = result.toString();
			}

			populateTupleMap( customerState, internalRecordType, offerId, countAsContact, programId, thisAction );

			Commons.logger.info( Commons.logPrefix + "Processing new offer: " + offerId );

			JsonArray allMessages = thisAction.get( "offerMessages" ).getAsJsonArray();
			Commons.logger.info( Commons.logPrefix + "thisAction value in processAllNewOffers : " + thisAction.toString() );
			Commons.logger.info( Commons.logPrefix + "Size of messages array: " + allMessages.size() + " ---- " + allMessages.toString() );

			for( int j = 0; j < allMessages.size(); j++ ) {
				String thisMessageChannel = allMessages.get( j ).getAsJsonObject().get( "channelName" ).getAsString();
				Commons.logger.info( Commons.logPrefix + "Before applying offer contact policy for channel------ " + thisMessageChannel );
				String requestChannel = "," + customerState.tupleMap.get( "REQUESTER_CHANNEL" ).getAsString() + ",";
				if( ! requestChannel.contains( thisMessageChannel ) )
					continue;

				customerState.tupleMap.addProperty( "CHANNEL", thisMessageChannel );
				if( skipGlobalContactPolicy.equals( "N" ) && alwaysOnOffer.equals( "N" ) ) {
					boolean globalContactPolicy = applyGlobalContactPolicy( thisAction, customerState, eventId, offerId, programId, thisMessageChannel );
					if( ! globalContactPolicy )
						continue;
				}else {
					//					manageFlowFunnelCounts( eventId, programId, offerId, "GLOBAL_CONTACT_POLICY_SKIPPED", customerState );
					Commons.logger.info( Commons.logPrefix + "Skipping Global contact rules check for offer: " + offerId );
				}

				if( alwaysOnOffer.equals( "N" ) ) {
					if( applyContactPolicy( thisAction, customerState, timestamp, eventId ) ) {
						applicableChannels.add( thisMessageChannel );
						Commons.logger.info( Commons.logPrefix + "Aplicable channesl for offer: " + offerId + "------ " + thisMessageChannel );
					}
				}else {
					applicableChannels.add( thisMessageChannel );
					//					manageFlowFunnelCounts( eventId, programId, offerId, "OFFER_CONTACT_POLICY_SKIPPED", customerState );
					Commons.logger.info( Commons.logPrefix + "Aplicable channesl for offer: " + offerId + "------ " + thisMessageChannel );
				}
			}

			if( applicableChannels.size() == 0 )
				continue;

			String flowId = customerState.customerId + siftRandomNumberGenerator();
			// String flowId = customerState.customerId +
			// Math.round(Math.random() * 100000);
			boolean isControl = false;
			if( thisTagListItem.has( "IS_CONTROL" ) ) {
				isControl = thisTagListItem.get( "IS_CONTROL" ).getAsBoolean();
				// flowId = thisTagListItem.get( "FLOW_ID" ).getAsString();
			}else {
				String expKey = programId + "_" + offerId + "_CONTROL_GROUP_EXP";
				if( Commons.expMap.containsKey( expKey ) ) {
					Object result = this.evaluateExpression( expKey, thisTagListItem, false, eventId, customerState );
					if( result.toString().equals( "true" ) ) {
						Commons.logger.info( Commons.logPrefix + customerState.customerId + " - " + programId + " - " + offerId + " - Control Group Evaluated to be true" );
						isControl = true;
					}
				}else {
					JsonElement jeleCG = thisAction.get( "controlGroupCriteria" );
					if( jeleCG != null && !jeleCG.isJsonNull() ) {
						if( jeleCG.isJsonObject() && jeleCG.getAsJsonObject().get( "expression" ).getAsString().trim().length() == 0 ) {
							isControl = false;
						}else if( ( jeleCG.isJsonObject() && jeleCG.getAsJsonObject().get( "expression" ).getAsString().trim().length() > 0 ) || jeleCG.getAsString().trim().length() > 0 ) {
							expKey = "CONTROL_GROUP_CRITERIA_" + offerId;
							Object result = this.evaluateExpression( expKey, thisTagListItem, false, eventId, customerState );
							if( result.toString().equals( "true" ) ) {
								Commons.logger.info( Commons.logPrefix + customerState.customerId + " - " + offerId + " - Control Group Evaluated to be true" );
								isControl = true;
							}
						}
					}
				}
			}

			JsonObject thisEventDef = thisAction.get( "eventDef" ).getAsJsonObject();
			JsonObject triggerMessage = new JsonObject();
			JsonElement targetSystemIdele = thisEventDef.get( "targetSystemID" );
			String targetSystemId = "JMSLB1";
			if( targetSystemIdele != null && !targetSystemIdele.isJsonNull() && !targetSystemIdele.getAsString().equals( "-1" ) )
				targetSystemId = targetSystemIdele.getAsString();

			String handleActionType = "notification";
			boolean isConditionalNotification = false;
			if( actionType.equals( "monitoringAction" ) ) {
				handleActionType = "monitoring";
				isConditionalNotification = true;
			}

			triggerMessage = this.handleNotificationAction( thisAction, programId, offerId, thisEventDef, eventId, thisTagListItem, timestamp, applicableChannels, customerState, handleActionType, flowId, isConditionalNotification );

			triggerMessage.addProperty( "FLOW_ID", flowId );
			triggerMessage.addProperty( "IS_CONTROL", isControl );
			triggerMessage.addProperty( "IS_SIMULATED", thisAction.get( "IS_SIMULATED" ).getAsBoolean() );
			triggerMessage.addProperty( "COUNT_AS_CONTACT", countAsContact );
			triggerMessage.addProperty( "PARTICIPATION_TYPE", "AUTO" ); //default initialisation. Will be reset in the handleMonitoringAction fucntion

			if( !this.gatherResponse( customerState, targetSystemId, triggerMessage, actionType, true ) )
				continue;

			triggerMessage.addProperty( "ORIG_REQUEST_ID", customerState.siftResponses.get( "REQUEST_ID" ).getAsString() );

			if( actionType.equals( "monitoringAction" ) ) {
				triggerMessage = this.handleMonitoringAction( thisAction, programId, offerId, thisEventDef, eventId, thisTagListItem, timestamp, flowId, isControl, applicableChannels, customerState, triggerMessage );
				String partcipationType = triggerMessage.get( "PARTICIPATION_TYPE" ).getAsString();
				if ( partcipationType.equals("MANUAL"))
					triggerMessage.addProperty( "FULFILLMENT_STATUS", "VIEWED" );
				else
					triggerMessage.addProperty( "FULFILLMENT_STATUS", "INTERESTED" );
			}
			this.handleCommunicationHistory( thisEventDef, triggerMessage, thisAction, timestamp, programId, offerId, actionType, flowId, eventId, isControl, customerState );
		}
	}

	private void processAllReminderActions( CustomerState customerState ) throws Exception {
		// TODO : Sort the offers based on the score
		long timestamp = customerState.siftMath.getCurrentTime();
		for( int i = 0; i < customerState.reminderActions.size(); i++ ) {
			JsonObject thisAction = customerState.reminderActions.get( i ).getAsJsonObject();
			JsonObject thisTagListItem = thisAction.get( "tagList" ).getAsJsonObject();
			String programId = thisAction.get( "programId" ).getAsString();
			String offerId = thisAction.get( "offerId" ).getAsString();
			String actionType = thisAction.get( "actionType" ).getAsString();
			JsonObject thisEventDef = thisAction.get( "eventDef" ).getAsJsonObject();
			String eventId = thisAction.get( "actualTriggeredEvent" ).getAsString();
			String offerType = thisAction.get( "offerType" ).getAsString();
			Commons.logger.info( Commons.logPrefix + "Processing reminder for offer: " + offerId );

			// Double check if fulfillment is not yet made first before sending reminder
			JsonElement thisProgramHistory = customerState.subscriberPrograms.get( programId );

			// Verify Max Fulfilments Per Subscriber.
			String expKey = "MAX_FULFILLMENTS_" + offerId;
			Object fulfilResult = this.evaluateExpression( expKey, thisTagListItem, true, eventId, customerState );
			if( fulfilResult.toString().equals( "false" ) ) {
				Commons.logger.info( "Max Fulfilment Count Expression Failed. Skipping the reminder." );
				continue;
			}

			JsonArray allMessages = thisAction.get( "reminderMessages" ).getAsJsonArray();
			boolean bContactMade = false;
			ArrayList< String > applicableChannels = new ArrayList< String >();
			populateTupleMap( customerState, "ContactCount", offerId, "N", programId, thisAction );

			for( int j = 0; j < allMessages.size(); j++ ) {
				String thisMessageChannel = allMessages.get( j ).getAsJsonObject().get( "channelName" ).getAsString();
				customerState.tupleMap.addProperty( "CHANNEL", thisMessageChannel );

				// check for reminder counts
				if( thisProgramHistory == null )
					continue;
				JsonArray thisProgramHistoryArray = thisProgramHistory.getAsJsonArray();
				JsonObject latestProgramInstance = thisProgramHistoryArray.get( thisProgramHistoryArray.size() - 1 ).getAsJsonObject();
				JsonElement jele = latestProgramInstance.get( offerId );
				if( jele == null )
					continue;
				JsonArray offerHistory = jele.getAsJsonArray();
				JsonObject latestOffer = offerHistory.get( offerHistory.size() - 1 ).getAsJsonObject();
				if( thisAction.has( "reminderCount" ) ) {
					if( thisAction.has( "reminderType" ) && thisAction.get( "reminderType" ).getAsString().equals( "Hybrid" ) )
						actionType = "hybridReminderAction";
					JsonElement jactionHistory = latestOffer.get( actionType );
					if( jactionHistory != null ) {
						JsonObject jReminderHistory = jactionHistory.getAsJsonObject();
						JsonElement thisReminderHistory = jReminderHistory.get( eventId );
						if( thisReminderHistory != null ) {
							if( thisReminderHistory.getAsJsonArray().size() >= thisAction.get( "reminderCount" ).getAsInt() ) {
								Commons.logger.info( Commons.logPrefix + "Reminder Count Exceeded  : " + eventId );
								continue;
							}
						}
					}
				}

				bContactMade = true;
				applicableChannels.add( thisMessageChannel );
			}
			if( !bContactMade )
				continue;

			boolean isControl = false;
			String flowId = customerState.customerId + siftRandomNumberGenerator();
			// String flowId = customerState.customerId +
			// Math.round(Math.random() * 100000);

			if( offerType.equals( "Conditional" ) ) {
				isControl = thisTagListItem.get( "IS_CONTROL" ).getAsBoolean();
				flowId = thisTagListItem.get( "FLOW_ID" ).getAsString();
			}

			if( thisAction.has( "reminderType" ) && thisAction.get( "reminderType" ).getAsString().equals( "Hybrid" ) ) {
				createScheduleForReminders( thisAction, customerState, eventId, flowId, isControl );
				this.constructProgramHistory( customerState, timestamp, programId, offerId, actionType, flowId, eventId, false, isControl, thisAction );
				Commons.logger.info( Commons.logPrefix + "TagList: " + thisTagListItem.toString() );
				if( commonsState.eventsTaggedForSubscribersMap.get( customerState.customerId ).getAsJsonObject().has( eventId ) )
					continue;
			}

			JsonArray constructedMessages = constructCommunicationMessages( thisAction, thisAction.get( "reminderMessages" ).getAsJsonArray(), "REMINDER_MESSAGE_", eventId, thisTagListItem, false, "reminder", null, eventId, customerState,
					programId, flowId );
			JsonObject triggerMessage = this.createTriggerMessage( eventId, programId, offerId, thisEventDef, thisTagListItem, timestamp, thisAction, customerState );
			triggerMessage.add( "REMINDER_MESSAGES", constructedMessages.get( 0 ) );

			triggerMessage.addProperty( "FLOW_ID", flowId );
			triggerMessage.addProperty( "IS_CONTROL", isControl );
			triggerMessage.addProperty( "IS_SIMULATED", thisAction.get( "IS_SIMULATED" ).getAsBoolean() );
			triggerMessage.addProperty( "PARTICIPATION_TYPE", "AUTO" );
			triggerMessage.addProperty( "COUNT_AS_CONTACT", "Y" );
			if( thisTagListItem.has( "REQUEST_ID" ) )
				triggerMessage.addProperty( "ORIG_REQUEST_ID", thisTagListItem.get( "REQUEST_ID" ).getAsString() );
			else
				triggerMessage.addProperty( "ORIG_REQUEST_ID", customerState.customerId + "-" + SiftMath.getCurrentTime() );

			JsonElement targetSystemIdele = thisEventDef.get( "targetSystemID" );
			String targetSystemId = "JMSLB1";
			if( targetSystemIdele != null && !targetSystemIdele.isJsonNull() && !targetSystemIdele.getAsString().equals( "-1" ) )
				targetSystemId = targetSystemIdele.getAsString();

			if( !this.gatherResponse( customerState, targetSystemId, triggerMessage, actionType, true ) )
				continue;

			this.handleCommunicationHistory( thisEventDef, triggerMessage, thisAction, timestamp, programId, offerId, actionType, flowId, eventId, isControl, customerState );
		}
	}

	public int siftRandomNumberGenerator() {
		Random r = new Random( System.currentTimeMillis() );
		return ( ( 1 + r.nextInt( 2 ) ) * 10000 + r.nextInt( 10000 ) );
	}

	protected boolean applyGlobalContactPolicy( JsonObject thisAction, CustomerState customerState, String eventId, String offerId, String programId, String thisLocation )
			throws NoSuchMethodException, InstantiationException, InvocationTargetException, IllegalAccessException, ParseException {

		// Offer Global max contacts Per subscribers.
		String globalExpKey = "GLOBAL_MAX_CONTACT_PER_SUBSCRIBER";
		if( Commons.expMap.containsKey( globalExpKey ) ) {
			Object maxContactPerSubsGlobalResult = this.evaluateExpression( globalExpKey, null, false, eventId, customerState );
			if( maxContactPerSubsGlobalResult.toString().equals( "false" ) ) {
				Commons.logger.info( Commons.logPrefix + "*********** Global max contact per subscriber rule failed. Skipping the Offer: " + offerId );
				return false;
			}
		}
		// manageFlowFunnelCounts( eventId, programId, offerId,
		// "GLOBAL_MAX_CONTACT_PER_SUBS", customerState );

		globalExpKey = "GLOBAL_MAX_CONTACT_ACROSS_SUBSCRIBERS";
		if( Commons.expMap.containsKey( globalExpKey ) ) {
			Object maxContactAcrossSubsGlobalResult = this.evaluateExpression( globalExpKey, null, false, eventId, customerState );
			if( maxContactAcrossSubsGlobalResult.toString().equals( "false" ) ) {
				Commons.logger.info( Commons.logPrefix + "*********** Global max contact across subscribers rule failed. Skipping the Offer: " + offerId );
				return false;
			}
		}
		// manageFlowFunnelCounts( eventId, programId, offerId,
		// "GLOBAL_MAX_CONTACT_ACROSS_SUBS", customerState );

		String globalCoolDownKey = "GLOBAL_COOLDOWN_RULE";
		if( Commons.expMap.containsKey( globalCoolDownKey ) ) {
			Object cooldownRuleGlobalResult = this.evaluateExpression( globalCoolDownKey, null, false, eventId, customerState );
			if( cooldownRuleGlobalResult.toString().equals( "true" ) ) {
				Commons.logger.info( Commons.logPrefix + "*********** Global cool down rule failed. Skipping the Offer : " + offerId );
				return false;
			}
		}
		manageFlowFunnelCounts( eventId, programId, offerId, "GLOBAL_COOLDOWN", customerState );

		// Campaign Global max contacts Per subscribers.
		if( Commons.contactPolicyObject != null && Commons.contactPolicyObject.has( "advancedContactRules" ) ) {
			JsonObject advContactRulesObj = Commons.contactPolicyObject.get( "advancedContactRules" ).getAsJsonObject();

			JsonObject advMaxContactAcrossSubsObj = advContactRulesObj.get( "maxContactAcrossSubscribers" ).getAsJsonObject();
			if( advMaxContactAcrossSubsObj.has( "program" ) ) {
				JsonObject prgObject = advMaxContactAcrossSubsObj.get( "program" ).getAsJsonObject();
				if( prgObject.has( programId ) && ! prgObject.get( programId ).isJsonNull() ) {
					JsonArray thisProgramArray = prgObject.get( programId ).getAsJsonArray();
					Object maxAcrossSubsRes = customerState.siftMath.execSiftFunction( "hasProgramSummaryContactExceeded", programId, thisProgramArray );
					if( maxAcrossSubsRes.toString().equals( "true" ) ) {
						Commons.logger.info( Commons.logPrefix + "*********** Program Global max contact across subscriber rule failed. Skipping the Offer: " + offerId + " in Program: " + programId );
						return false;
					}
				}
			}

			JsonObject advMaxContactPerSubsObj = advContactRulesObj.get( "maxContactPerSubscriber" ).getAsJsonObject();
			if( advMaxContactPerSubsObj.has( "program" ) ) {
				JsonObject prgPerSubsObject = advMaxContactPerSubsObj.get( "program" ).getAsJsonObject();
				if( prgPerSubsObject.has( programId ) && ! prgPerSubsObject.get( programId ).isJsonNull() ) {
					JsonArray thisProgramArray = prgPerSubsObject.get( programId ).getAsJsonArray();
					Object maxProgPerSubsRes = customerState.siftMath.execSiftFunction( "hasProgramContactExceeded", programId, thisProgramArray );
					if( maxProgPerSubsRes.toString().equals( "true" ) ) {
						Commons.logger.info( Commons.logPrefix + "*********** Program Global max contact per subscriber rule failed. Skipping the Offer: " + offerId + " in Program: " + programId );
						return false;
					}
				}
			}

			Commons.logger.info( Commons.logPrefix + "Processing cool down: " + programId );
			JsonObject advCoolDownObj = advContactRulesObj.get( "cooldownPeriodForOfferPerSubscriber" ).getAsJsonObject();
			if( advCoolDownObj.has( programId ) ) {
				JsonObject prgCoolDownObject = advCoolDownObj.get( programId ).getAsJsonObject();
				Iterator< Entry< String, JsonElement > > prgIterator = prgCoolDownObject.entrySet().iterator();
				while( prgIterator.hasNext() ) {
					Entry< String, JsonElement > entry = prgIterator.next();
					String compareProgramId = entry.getKey();
					String thisExpId = programId + "_" + compareProgramId + "_COOLDOWNEXP";
					if( Commons.expMap.containsKey( thisExpId ) ) {
						Object thisProgCoolDownResult = this.evaluateExpression( thisExpId, null, false, eventId, customerState );
						if( thisProgCoolDownResult.toString().equals( "true" ) ) {
							Commons.logger.info( Commons.logPrefix + "*********** Program Global Cool Down per subscriber rule failed. Skipping the Offer: " + offerId + " in Program: " + programId );
							return false;
						}
					}
				}
			}

			Commons.logger.info( Commons.logPrefix + "Processing Location based global contact policy: " + programId );
			JsonObject locsPerSubsObject = advMaxContactPerSubsObj.get( "locations" ).getAsJsonObject();
			if( thisLocation != null && locsPerSubsObject.has( thisLocation ) ) {
				JsonArray thisLocArray = locsPerSubsObject.get( thisLocation ).getAsJsonArray();
				Object maxLocsPerSubsRes = customerState.siftMath.execSiftFunction( "hasLocationContactExceeded", thisLocation, thisLocArray );

				if( maxLocsPerSubsRes.toString().equals( "true" ) ) {
					Commons.logger.info( Commons.logPrefix + "*********** Locations Global max contact per subscriber rule failed. Skipping the Offer: " + offerId + " in Program: " + programId );
					return false;
				}
			}
		}

		Commons.logger.info( Commons.logPrefix + "Global contact rules passed for offer" );
		return true;
	}

	protected boolean applyContactPolicy( JsonObject thisAction, CustomerState customerState, long timestamp, String eventId )
			throws NoSuchMethodException, InstantiationException, InvocationTargetException, IllegalAccessException, ParseException {
		JsonObject thisTagListItem = thisAction.get( "tagList" ).getAsJsonObject();
		String programId = thisAction.get( "programId" ).getAsString();
		String offerId = thisAction.get( "offerId" ).getAsString();
		String actionType = thisAction.get( "actionType" ).getAsString();

		// Cool Down Rule
		JsonElement jCoolExpObj = thisAction.get( "cooldownPeriodForOfferPerSubscriber" );
		if( jCoolExpObj != null && !jCoolExpObj.isJsonNull() && jCoolExpObj.getAsJsonObject().has( "expression" ) && !jCoolExpObj.getAsJsonObject().get( "expression" ).isJsonNull()
				&& jCoolExpObj.getAsJsonObject().get( "expression" ).getAsString().trim().length() > 0 ) {
			String expKey = "COOLDOWN_PERIOD_FOR_OFFER_PER_SUBSCRIBER_" + offerId;
			Object result = this.evaluateExpression( expKey, thisTagListItem, false, eventId, customerState );
			if( result.toString().equals( "true" ) ) {
				Commons.logger.info( Commons.logPrefix + "Cool Down Period Expression Failed" );
				return false;
			}
		}

		/*
		 * BACKWARD COMPATIBILITY CODE - Commented Off else if( thisAction.get(
		 * "cooldownPeriod" ) != null && ! thisAction.get( "cooldownPeriod"
		 * ).isJsonNull() && thisAction.get( "cooldownPeriod" ).getAsLong() !=
		 * -1 ){ JsonElement thisProgramHistory =
		 * customerState.subscriberPrograms.get( programId ); if(
		 * thisProgramHistory != null ) { JsonArray thisProgramHistoryArray =
		 * thisProgramHistory.getAsJsonArray(); JsonObject latestProgramInstance
		 * = thisProgramHistoryArray.get( thisProgramHistoryArray.size() - 1
		 * ).getAsJsonObject(); JsonElement jele = latestProgramInstance.get(
		 * offerId ); if( jele != null ) { JsonArray offerHistory =
		 * jele.getAsJsonArray(); JsonObject latestOffer = offerHistory.get(
		 * offerHistory.size() - 1 ).getAsJsonObject(); JsonElement
		 * jactionHistory = latestOffer.get( actionType ); if( jactionHistory !=
		 * null ) { long lLastActionDate = jactionHistory.getAsLong(); long
		 * lCooldownPeriod = thisAction.get( "cooldownPeriod" ).getAsLong();
		 * long cooldownEnd = lLastActionDate + lCooldownPeriod *
		 * SiftMath.msecPerDay; cooldownEnd = ( cooldownEnd / 1000 ) * 1000;
		 * Calendar calEnd = Calendar.getInstance(); calEnd.setTimeInMillis(
		 * cooldownEnd ); calEnd.set( calEnd.get( Calendar.YEAR ), calEnd.get(
		 * Calendar.MONTH ), calEnd.get( Calendar.DATE ), 23, 59, 0 );
		 * cooldownEnd = calEnd.getTimeInMillis() +
		 * java.util.TimeZone.getDefault().getOffset( cooldownEnd ); if(
		 * timestamp < cooldownEnd ) { //Commons.logger.log(
		 * java.util.logging.Level.INFO, Commons.logPrefix + "Cool Down Period Rule Failed  : " );
		 * return false; } } } } }
		 */

		manageFlowFunnelCounts( eventId, programId, offerId, "OFFER_COOLDOWN", customerState );

		// 2. Check for Max Offer Per Customer Rule -
		// maxContactPerSubscriber
		JsonElement jMaxContactPerSubExpObj = thisAction.get( "maxContactPerSubscriber" );
		if( jMaxContactPerSubExpObj != null && !jMaxContactPerSubExpObj.isJsonNull() && jMaxContactPerSubExpObj.getAsJsonObject().has( "expression" ) ) {
			JsonElement jMaxContactPerSubExp = jMaxContactPerSubExpObj.getAsJsonObject().get( "expression" );
			if( jMaxContactPerSubExp != null && !jMaxContactPerSubExp.isJsonNull() && jMaxContactPerSubExp.getAsString().trim().length() > 0 ) {
				String expKey = "MAX_CONTACT_PER_SUBSCRIBER_" + offerId;
				Object result = this.evaluateExpression( expKey, thisTagListItem, false, eventId, customerState );
				if( result.toString().equals( "false" ) ) {
					Commons.logger.info( Commons.logPrefix + "maxContactPerSubscriber Expression Failed" );
					return false;
				}
			}
		}
		//manageFlowFunnelCounts( eventId, programId, offerId, "OFFER_MAX_CONTACT_PER_SUBS", customerState );

		// 3. Check for Max Offer Across Customers Rule -
		// maxContactAcrossSubscribers
		JsonElement jMaxContactAcrossSubExpObj = thisAction.get( "maxContactAcrossSubscribers" );
		if( jMaxContactAcrossSubExpObj != null && !jMaxContactAcrossSubExpObj.isJsonNull() && jMaxContactAcrossSubExpObj.getAsJsonObject().has( "expression" ) ) {
			JsonElement jMaxContactAcrossSubExp = jMaxContactAcrossSubExpObj.getAsJsonObject().get( "expression" );
			if( jMaxContactAcrossSubExp != null && !jMaxContactAcrossSubExp.isJsonNull() && jMaxContactAcrossSubExp.getAsString().trim().length() > 0 ) {
				String expKey = "MAX_CONTACT_ACROSS_SUBSCRIBERS_" + offerId;
				Object result = this.evaluateExpression( expKey, thisTagListItem, false, eventId, customerState );
				if( result.toString().equals( "false" ) ) {
					Commons.logger.info( Commons.logPrefix + "maxContactAcrossSubscribers Expression Failed" );
					return false;
				}
			}
		}
		//manageFlowFunnelCounts( eventId, programId, offerId, "OFFER_MAX_CONTACT_ACROSS_SUBS", customerState );

		return true;
	}

	// STEROID-GOLDBISCUIT
	protected void communicaTeToSO( CustomerState customerState, String targetSystemId ) throws Exception {
		if( customerState.siftResponses.entrySet().size() == 0 )
			return;
		removeNonApplicableMessages( customerState );

		Commons.logger.info( Commons.logPrefix + "Attempting to Send Message to SO : " + customerState.siftResponses.toString() + "--on: " + targetSystemId );
		outputCollector.emit( "record", new Values( targetSystemId, customerState.siftResponses.toString() ) );
		Commons.logger.info( Commons.logPrefix + "Sent Message to SO : " + customerState.siftResponses.toString() );
	}

	// STEROID-GOLDBISCUIT
	protected boolean gatherResponse( CustomerState customerState, String targetSystemId, JsonObject triggerMessage, String actionType, boolean newOffer ) throws Exception {
		if( customerState.siftResponses.entrySet().size() == 0 ) {
			JsonObject finalResponse = new JsonObject();
			finalResponse.addProperty( "STATUS", "SUCCESS" );
			JsonObject customerId = new JsonObject();
			customerId.addProperty( "MSISDN", customerState.customerId );
			finalResponse.add( "CUSTOMER_IDENTIFIER", customerId );

			if( customerState.request != null ) {
				finalResponse.addProperty( "REQUEST_ID", customerState.request.get( "REQUEST_ID" ).getAsString() );
				finalResponse.add( "CONTEXT_DATA", customerState.request.get( "CONTEXT_DATA" ) );				
				JsonObject locations = new JsonObject();
				JsonArray responselocations = new JsonArray();
				JsonArray requestlocations = customerState.request.get("REQUESTER_LOCATION").getAsJsonObject().get("LOCATIONS").getAsJsonArray();
				for (int i = 0; i < requestlocations.size(); i++) {
					responselocations.add(requestlocations.get(i));
				}
				locations.add("LOCATIONS", responselocations);
				finalResponse.add("REQUESTER_LOCATION", locations);

				if( customerState.request.has( "FULLFILLMENT_DETAILS" ) )
					finalResponse.add( "FULLFILLMENT_DETAILS", customerState.request.get( "FULLFILLMENT_DETAILS" ).getAsJsonObject() );
			}else {
				String reqId = customerState.customerId + "-" + SiftMath.getCurrentTime();
				finalResponse.addProperty( "REQUEST_ID", reqId );
				triggerMessage.addProperty( "REQUEST_ID", reqId );

				finalResponse.add( "CONTEXT_DATA", new JsonObject() );

				JsonArray locations = new JsonArray();
				//				String[ ] incomingLocations = customerState.tupleMap.get( "REQUESTER_LOCATION" ).split( "," );
				//String[ ] incomingLocations = customerState.offerLocationsMap.get( triggerMessage.get( "OFFER_ID" ).getAsString() )[ 0 ].split( "," );
				String offerLocations = triggerMessage.get( "APPLICABLE_LOCATIONS" ).getAsString();
				String[ ] incomingLocations = offerLocations.split( "," );
				for( int j = 0; j < incomingLocations.length; j++ ) {
					JsonObject thisLocation = new JsonObject();
					thisLocation.addProperty( "id", incomingLocations[ j ] );
					thisLocation.addProperty( "numOffers", 1000 );
					thisLocation.addProperty( "SHOW_EXISTING_OFFERS", false );
					locations.add( thisLocation );
				}
				JsonObject jLocations = new JsonObject();
				jLocations.add( "LOCATIONS", locations );
				finalResponse.add( "REQUESTER_LOCATION", jLocations );
			}

			finalResponse.addProperty( "REQUESTER_CHANNEL", customerState.tupleMap.get( "REQUESTER_CHANNEL" ).getAsString() );
			finalResponse.addProperty( "REQUESTER_APPLICATION", customerState.tupleMap.get( "REQUESTER_APPLICATION" ).getAsString() );
			finalResponse.addProperty( "REQUESTER_ZONE", customerState.tupleMap.get( "REQUESTER_ZONE" ).getAsString() );

			// initialise the offers, fulfillemnts, reminders sections
			JsonArray locations = finalResponse.get( "REQUESTER_LOCATION" ).getAsJsonObject().get( "LOCATIONS" ).getAsJsonArray();
			for( int i = 0; i < locations.size(); i++ ) {
				JsonObject thisLocation = locations.get( i ).getAsJsonObject();
				thisLocation.add( "OFFERS", new JsonArray() );
				thisLocation.add( "FULFILMENTS", new JsonArray() );
				thisLocation.add( "REMINDERS", new JsonArray() );
			}
			customerState.siftResponses = finalResponse;
		} else { 
			JsonObject existingLocations = customerState.siftResponses.get( "REQUESTER_LOCATION" ).getAsJsonObject();
			JsonArray existingLocArr = existingLocations.get( "LOCATIONS" ).getAsJsonArray();
			String existignLocs = ",";
			for( int loc=0; loc<existingLocArr.size(); loc++ ) 
				existignLocs += existingLocArr.get( loc ).getAsJsonObject().get( "id" ).getAsString() + ",";	
			//String[ ] incomingLocations = customerState.offerLocationsMap.get( triggerMessage.get( "OFFER_ID" ).getAsString() )[ 0 ].split( "," );
			String offerLocations = triggerMessage.get( "APPLICABLE_LOCATIONS" ).getAsString();
			String[ ] incomingLocations = offerLocations.split( "," );
			for( int j = 0; j < incomingLocations.length; j++ ) {
				if( ! existignLocs.contains( "," + incomingLocations[ j ] + "," ) ) {
					JsonObject thisLocation = new JsonObject();
					thisLocation.addProperty( "id", incomingLocations[ j ] );
					thisLocation.addProperty( "numOffers", 1000 );
					thisLocation.addProperty( "SHOW_EXISTING_OFFERS", false );
					thisLocation.add( "OFFERS", new JsonArray() );
					thisLocation.add( "FULFILMENTS", new JsonArray() );
					thisLocation.add( "REMINDERS", new JsonArray() );
					existingLocArr.add( thisLocation );
				}
			}
		}

		if( actionType == null )
			return false;

		// Add to the right location here
		boolean allowDuplicates = false;
		if( customerState.request != null ) {
			JsonElement eDup = customerState.request.get( "REQUESTER_LOCATION" ).getAsJsonObject().get( "ALLOW_DUPLICATES" );
			if( eDup != null )
				allowDuplicates = eDup.getAsBoolean();
		}

		// TODO : how to handle the applicationLocations is empty
		String offerLocations = triggerMessage.get( "APPLICABLE_LOCATIONS" ).getAsString();
		String[ ] arrOfferLocations = offerLocations.split( "," );
		boolean offerAlreadyAdded = false;
		for( int i = 0; i < arrOfferLocations.length; i++ ) {
			String thisOfferLocation = arrOfferLocations[ i ];
			JsonArray jReqLocations = new JsonArray();
			if ( customerState.request != null )
				jReqLocations = customerState.request.get( "REQUESTER_LOCATION" ).getAsJsonObject().get( "LOCATIONS" ).getAsJsonArray();
			else
				jReqLocations = customerState.siftResponses.get( "REQUESTER_LOCATION" ).getAsJsonObject().get( "LOCATIONS" ).getAsJsonArray();
			for( int j = 0; j < jReqLocations.size(); j++ ) {
				JsonObject thisReqLocation = jReqLocations.get( j ).getAsJsonObject();
				String thisRequestLocId = thisReqLocation.get( "id" ).getAsString();
				if( thisRequestLocId.equals( thisOfferLocation ) ) {
					if( actionType.equals( "monitoringAction" ) || actionType.equals( "notificationAction" ) ) {
						int numOffersInReq = thisReqLocation.get( "numOffers" ).getAsInt();
						JsonArray thisArray = thisReqLocation.get( "OFFERS" ).getAsJsonArray();
						if( newOffer && thisArray.size() >= numOffersInReq )
							continue;

						if( !newOffer && thisReqLocation.has( "SHOW_EXISTING_OFFERS" ) && thisReqLocation.get( "SHOW_EXISTING_OFFERS" ).getAsBoolean() == false )
							continue;

						thisArray.add( triggerMessage );
						customerState.newOffersList.add( triggerMessage.get( "FLOW_ID" ).getAsString() );
					}else if( actionType.equals( "fulfillmentAction" ) )
						thisReqLocation.get( "FULFILMENTS" ).getAsJsonArray().add( triggerMessage );
					else if( actionType.equals( "reminderAction" ) || actionType.equals( "hybridReminderAction" ) )
						thisReqLocation.get( "REMINDERS" ).getAsJsonArray().add( triggerMessage );

					offerAlreadyAdded = true;
					if( !allowDuplicates ) {
						break;
					}
				}
			}
			if( offerAlreadyAdded && !allowDuplicates )
				break;
		}
		if( !offerAlreadyAdded )
			return false;

		return true;
	}

	public void removeNonApplicableMessages( CustomerState customerState ) {
		JsonObject siftResponsesClone = ( JsonObject ) Commons.parser.parse( customerState.siftResponses.get( "REQUESTER_LOCATION" ).getAsJsonObject().toString() );
		JsonArray allLocations = siftResponsesClone.get( "LOCATIONS" ).getAsJsonArray();
		String requesterChannel = customerState.siftResponses.get( "REQUESTER_CHANNEL" ).getAsString();
		for( int i = 0; i < allLocations.size(); i++ ) {
			JsonObject thisLocation = allLocations.get( i ).getAsJsonObject();
			JsonArray thisLocOffers = thisLocation.get( "OFFERS" ).getAsJsonArray();
			for( int j = 0; j < thisLocOffers.size(); j++ ) {
				JsonObject thisOffer = thisLocOffers.get( j ).getAsJsonObject();
				JsonElement eNotifyMessage = thisOffer.get( "NOTIFICATION_MESSAGES" );
				if( eNotifyMessage == null )
					continue;

				JsonArray allNotifyMessages = eNotifyMessage.getAsJsonArray();
				for( int msg = allNotifyMessages.size() - 1; msg >= 0; msg-- ) {
					JsonObject thisMessage = allNotifyMessages.get( msg ).getAsJsonObject();
					if( !requesterChannel.equals( thisMessage.get( "channelName" ).getAsString() ) )
						allNotifyMessages.remove( msg );
				}
			}
		}
		customerState.siftResponses.add( "REQUESTER_LOCATION", siftResponsesClone );
	}

	protected void handleActionFailureReasons( CustomerState customerState, String msisdn, String reason, String reasonCode, String programId, String offerId ) throws Exception {
		// Commons.logger.info( Commons.logPrefix + "CUSTOMER : " +
		// msisdn + " Action Failure for Program-Offer " + programId + " - " +
		// offerId + " Reason : " + reason );
		if( ! ( reasonCode.equals( "002" ) && customerState.tupleMap.has( "RECORD_TYPE" ) && customerState.tupleMap.get( "RECORD_TYPE" ).getAsString().equals( "REGISTER_ACTION" ) ) )
			return;
		/*
		 * { "STATUS", "ERROR", "ERROR_CODE", "002", "MSISDN" : 9132432342,
		 * "PROGRAM_ID" : "NBO_OUTBOUND", "OFFER_ID" : "S_100_Data_2G",
		 * "FLOW_ID" : "1232432-ABDdsfsdfs-3423433243", "ERROR_MESSAGE",
		 * "Customer has already taken the offer" }
		 */
		Commons.logger.info( Commons.logPrefix + "CUSTOMER : " + msisdn + " Action Failure for Program-Offer " + programId + " - " + offerId + " Reason : " + reason );
		JsonObject reasonObj = new JsonObject();
		reasonObj.addProperty( "STATUS", "ERROR" );
		reasonObj.addProperty( "MSISDN", msisdn );
		reasonObj.addProperty( "PROGRAM_ID", programId );
		reasonObj.addProperty( "OFFER_ID", offerId );
		reasonObj.addProperty( "ERROR_CODE", reasonCode );
		reasonObj.addProperty( "ERROR_MESSAGE", reason );
		reasonObj.addProperty( "FLOW_ID", customerState.tupleMap.get( "FLOW_ID" ).getAsString() );
		customerState.siftOutboundNoActionReasons.add( reasonCode, reasonObj );
	}

	protected void handleCommunicationHistory( JsonObject thisEventDef, JsonObject triggerMessage, JsonObject thisAction, long timestamp, String programId, String offerId, String actionType, String flowId, String eventId, Boolean isControl,
			CustomerState customerState ) throws Exception {
		JsonElement targetSystemIdele = thisEventDef.get( "targetSystemID" );
		String targetSystemId = "JMSLB1";
		if( targetSystemIdele != null && !targetSystemIdele.isJsonNull() && !targetSystemIdele.getAsString().equals( "-1" ) )
			targetSystemId = targetSystemIdele.getAsString();

		String participationType = triggerMessage.get( "PARTICIPATION_TYPE" ).getAsString();

		String contactMessageType = "FULFILLMENT_MESSAGES";
		String internalRecordTye = "FulfillmentCount";
		String contactCountFunction = "manageFulfillmentContactCount";
		String liveChartType = "FulfillmentCounts";
		if( actionType.equals( "monitoringAction" ) || actionType.equals( "notificationAction" ) ) {
			contactMessageType = "NOTIFICATION_MESSAGES";
			internalRecordTye = "ContactCount";
			contactCountFunction = "manageOfferContactCount";
			liveChartType = "MonitoringCounts";
		}else if( actionType.equals( "reminderAction" ) || actionType.equals( "hybridReminderAction" ) ) {
			contactMessageType = "REMINDER_MESSAGES";
			internalRecordTye = "ReminderCount";
			contactCountFunction = "manageReminderContactCount";
			liveChartType = "ReminderCounts";
		}else if( actionType.equals( "fulfillmentAction" ) ) {
			customerState.tupleMap.addProperty( "FULFILLMENT_PRODUCT_PRESENT", triggerMessage.get( "FULFILLMENT_PRODUCT_PRESENT" ).getAsString() );
		}

		String isSimulated = triggerMessage.get( "IS_SIMULATED" ).getAsString();
		String allChannels = "";
		JsonArray messages = triggerMessage.get( contactMessageType ).getAsJsonArray();
		String requesterLocation = customerState.tupleMap.get( "REQUESTER_CHANNEL" ).getAsString();
		for( int k = 0; k < messages.size(); k++ ) {
			JsonObject thisMessage = messages.get( k ).getAsJsonObject();
			if( allChannels.length() > 0 )
				allChannels += ",";
			String thisMessageLocation = thisMessage.get( "channelName" ).getAsString();
			if( requesterLocation.contains( thisMessageLocation ) )
				allChannels += thisMessageLocation;
		}

		if( actionType.equals( "fulfillmentAction" ) )
			customerState.tupleMap.addProperty( "CONTACT_TIME", Long.toString( customerState.siftMath.getCurrentTime() ) );
		else {
			customerState.tupleMap.addProperty( "CONTACT_TIME", Long.toString( timestamp ) );
		}

		JsonObject thisOffer = Commons.allOfferDef.get( offerId );
		customerState.tupleMap.addProperty( "COUNT_AS_CONTACT", triggerMessage.get( "COUNT_AS_CONTACT" ).getAsString() );
		customerState.tupleMap.addProperty( "OFFER_MESSAGE", triggerMessage.toString() );
		customerState.tupleMap.addProperty( "SIFT_INTERNAL_RECORD_TYPE", internalRecordTye );
		customerState.tupleMap.addProperty( "CHANNEL", allChannels ); // TODO: Shoud we get the correct channel or go by * itself ?
		customerState.tupleMap.addProperty( "IS_SIMULATED", isSimulated );
		customerState.tupleMap.addProperty( "PROGRAM_ID", programId );
		customerState.tupleMap.addProperty( "IS_CONTROL", isControl.toString() );
		customerState.tupleMap.addProperty( "COUNT_FACTOR", "1" );
		customerState.tupleMap.addProperty( "TRANSACTION_STATUS", "SUCCESS" );
		customerState.tupleMap.addProperty( "OFFER_CATEGORY", thisOffer.get( "category" ).getAsString() );
		customerState.tupleMap.addProperty( "OFFER_ID", offerId );
		customerState.tupleMap.addProperty( "OFFER_TYPE", thisOffer.get( "offerType" ).getAsString() );
		customerState.tupleMap.addProperty( "FLOW_ID", flowId );
		customerState.tupleMap.addProperty( "PARTICIPATION_TYPE", participationType );

		Commons.logger.info( Commons.logPrefix + "TupleMap Before Calling " + contactCountFunction + " Function  : " + customerState.tupleMap.toString() );

		evaluateExpression( contactCountFunction, null, false, null, customerState );
		customerState.tupleMap.addProperty( "LIVECHART_TYPE", liveChartType );

		Commons.logger.info( Commons.logPrefix + "SubIndicator After Calling " + contactCountFunction + " Function  : " + customerState.subscriberIndicators.toString() );
		// if( ! isSimulated.equals( "true" ) )
		if( participationType.equals( "AUTO" ) ) {
			evaluateExpression( "manageLiveChartIndicator", null, false, null, customerState );
			this.constructProgramHistory( customerState, timestamp, programId, offerId, actionType, flowId, eventId, false, isControl, thisAction );
			JsonElement eNextWave = thisAction.get( "nextwave" );
			if( eNextWave != null && !eNextWave.isJsonNull() ) {
				JsonArray jNextWaves = eNextWave.getAsJsonArray();
				for( int n = 0; n < jNextWaves.size(); n++ ) {
					JsonObject aNextWave = jNextWaves.get( n ).getAsJsonObject();
					createTagForNextWave( thisAction, programId, offerId, customerState, thisEventDef, eventId, flowId, isControl, aNextWave, triggerMessage );
				}
			}
		}
	}

	protected void processAllIndicators( CustomerState customerState ) throws ParseException, NoSuchMethodException, InstantiationException, InvocationTargetException, IllegalAccessException {
		Set< String > keyset = Commons.allIndDef.keySet();
		Iterator< String > itr = keyset.iterator();
		while( itr.hasNext() ) {
			String thisIndId = itr.next();
			JsonObject thisInd = Commons.allIndDef.get( thisIndId );
			String frequency = thisInd.get( "frequency" ).getAsString();
			if( frequency.equals( "CustomTime" ) || frequency.equals( "Session" ) || frequency.equals( "TriggerTime" ) || frequency.equals( "Summary" ) )
				continue;
			Commons.logger.info( Commons.logPrefix + "--------------------------------------------------------------------" );
			Commons.logger.info( Commons.logPrefix + "Processing Indicator : " + thisIndId );

			if( customerState.processedInd.contains( thisIndId ) ) {
				Commons.logger.info( Commons.logPrefix + "Skipping Already Executed Indicator. Present in ProcessedInd " + thisIndId );
				continue;
			}

			boolean recordTypeCheck = Commons.siftConfig.get( "recordTypeCheckForIndicators" ).getAsBoolean();
			String thisExpression = thisInd.get( "expression" ).getAsString();
			if( recordTypeCheck && thisExpression.contains( "RECORD_TYPE.equals" ) ) {
				if( !thisExpression.contains( "\"" + customerState.recordType + "\"" ) ) {
					Commons.logger.info( Commons.logPrefix + "Skipping this Indicator " + thisIndId + "  as RECORD_TYPE does not match" );
					customerState.processedInd.add( thisIndId );
					continue;
				}
			}

			Object expressionValue = evaluateExpression( thisIndId, null, false, null, customerState );
			String indKey = generatekey( null, thisIndId, frequency, customerState.thisRecordDate, customerState.cal, null );
			addToIndicator( customerState, indKey, expressionValue, Commons.expMap.get( thisIndId ).getOptionalExpressionType(), false, frequency );
			customerState.processedInd.add( thisIndId );

			if( customerState.subscriberIndicators.get( indKey ) != null )
				Commons.logger.info( Commons.logPrefix + "Processed Indicator : " + thisIndId + " - Value : " + customerState.subscriberIndicators.get( indKey ).toString() );
			else if( customerState.sessionIndicators.get( indKey ) != null )
				Commons.logger.info( Commons.logPrefix + "Processed Session Indicator : " + thisIndId + " - Value : " + customerState.sessionIndicators.get( indKey ).toString() );
			Commons.logger.info( Commons.logPrefix + "--------------------------------------------------------------------" );
		}

		// SUMMARY INDICATOR
		keyset = Commons.summaryIndDef.keySet();
		itr = keyset.iterator();
		Commons.logger.info( Commons.logPrefix + "Processing all the summary indicators." );
		while( itr.hasNext() ) {
			String aKey = itr.next();
			Commons.logger.info( Commons.logPrefix + "Processing summary indicator: " + aKey );
			customerState.siftMath.processSummaryIndicator( aKey );
		}

	}

	private void addToIndicator( CustomerState customerState, String key, Object value, Class type, boolean programGroup, String frequency ) {
		JsonObject destination = customerState.subscriberIndicators;
		if( frequency.equals( "Session" ) )
			destination = customerState.sessionIndicators;
		if( programGroup ) {
			JsonElement eDesitnation = customerState.subscriberIndicators.get( "PROGRAM_PARAMETERS-L" );
			if( eDesitnation == null ) {
				destination = new JsonObject();
				customerState.subscriberIndicators.add( "PROGRAM_PARAMETERS-L", destination );
			}else
				destination = eDesitnation.getAsJsonObject();
		}

		boolean addDefaultvalueInd = Commons.siftConfig.get( "storeDefaultValueIndicators" ).getAsBoolean();

		if( type == double.class ) {
			destination.addProperty( key, ( Double ) value );
			if( !addDefaultvalueInd && ( ( Double ) value ).doubleValue() == 0 )
				destination.remove( key );
		}else if( type == int.class ) {
			destination.addProperty( key, ( Integer ) value );
			if( !addDefaultvalueInd && ( ( Integer ) value ).intValue() == 0 )
				destination.remove( key );
		}else if( type == long.class ) {
			destination.addProperty( key, ( Long ) value );
			if( !addDefaultvalueInd && ( ( Long ) value ).longValue() == 0 )
				destination.remove( key );
		}else if( type == Double.class )
			destination.addProperty( key, ( Double ) value );
		else if( type == Boolean.class )
			destination.addProperty( key, ( Boolean ) value );
		else if( type == Float.class )
			destination.addProperty( key, ( Float ) value );
		else if( type == Integer.class )
			destination.addProperty( key, ( Integer ) value );
		else if( type == String.class )
			destination.addProperty( key, ( String ) value );
		else if( type == boolean.class )
			destination.addProperty( key, ( Boolean ) value );
		else if( type == JsonArray.class )
			destination.add( key, ( JsonArray ) value );
		else if( type == JsonObject.class )
			destination.add( key, ( JsonObject ) value );
	}

	protected void processPrograms( JsonObject thisEventDef, String actualTriggeredEvent, JsonObject thisTagListItem, CustomerState customerState ) throws Exception {
		JsonElement elem = thisEventDef.get( "programs" );
		JsonArray arr = elem.getAsJsonArray();
		long timestamp = customerState.siftMath.getCurrentTime();

		Commons.logger.info( Commons.logPrefix + "Programs Tagged : " + elem.toString() );
		for( int i = 0; i < arr.size(); i++ ) {
			String programId = arr.get( i ).getAsString();

			JsonObject thisProgram = Commons.allProgramDef.get( programId );
			// check if program is within the date limits.
			if( thisProgram == null || thisProgram.isJsonNull() || ! ( ( thisProgram.get( "status" ).getAsString().equals( "Active" ) || thisProgram.get( "status" ).getAsString().equalsIgnoreCase( "simulation" ) )
					&& timestamp >= thisProgram.get( "startDateTime" ).getAsLong() && timestamp <= thisProgram.get( "endDateTime" ).getAsLong() ) ) {
				Commons.logger.info( Commons.logPrefix + "Program attached to trigger is either inactive or the timestamp does not match: " + programId );
				continue;
			}

			JsonElement thisProgramActionsMap = thisProgram.get( "programActions" );
			if( thisProgramActionsMap == null )
				continue;
			JsonElement programActions = thisProgramActionsMap.getAsJsonObject().get( actualTriggeredEvent );
			if( programActions == null )
				continue;

			Commons.logger.info( Commons.logPrefix + "Processing Program : " + programId );
			JsonArray programActionsArray = programActions.getAsJsonArray();

			Boolean isSimulated = false;
			if( thisProgram.get( "status" ).getAsString().equals( "Simulation" ) )
				isSimulated = true;

			if( thisEventDef.has( "type" ) && thisEventDef.get( "type" ).getAsString().equals( "Behavioural" ) )
				manageFlowFunnelCounts( actualTriggeredEvent, programId, null, "TRIGGER_CRITERIA", customerState );

			for( int j = 0; j < programActionsArray.size(); j++ ) {
				JsonObject thisAction = programActionsArray.get( j ).getAsJsonObject();
				String actionType = thisAction.get( "actionType" ).getAsString();
				if( actionType.equals( "nextBestDynamicAction" ) ) {
					handleNextBestActionDynamicAction( thisAction, thisProgramActionsMap.getAsJsonObject(), thisEventDef, actualTriggeredEvent, thisTagListItem, customerState, programId, isSimulated.toString(), timestamp );
				}else if( actionType.equals( "nextBestAction" ) ) {
					handleNextBestAction( thisAction, thisProgramActionsMap.getAsJsonObject(), thisEventDef, actualTriggeredEvent, thisTagListItem, customerState, programId, isSimulated.toString(), timestamp );
				}else if( actionType.equals( "scheduledTriggerAction" ) ) {
					createScheduleForScheduledAction( thisAction, customerState, actualTriggeredEvent );
				}else if( actionType.equals( "offerDecisionAction" ) ) {
					handleOfferDecisionAction( thisAction, thisProgramActionsMap.getAsJsonObject(), thisEventDef, actualTriggeredEvent, thisTagListItem, customerState, programId, isSimulated.toString(), timestamp );
				}else
					handleOtherActions( thisAction, thisEventDef, actualTriggeredEvent, thisTagListItem, customerState, programId, isSimulated.toString(), timestamp );
			}
		}
	}

	private void handleOtherActions( JsonObject thisAction, JsonObject thisEventDef, String actualTriggeredEvent, JsonObject thisTagListItem, CustomerState customerState, String programId, String isSimulated, long timestamp )
			throws Exception {
		String actionType = thisAction.get( "actionType" ).getAsString();
		String offerId = thisAction.get( "offerId" ).getAsString();
		String offerType = thisAction.get( "offerType" ).getAsString();
		Commons.logger.info( Commons.logPrefix + "Evaluating offer : " + offerId + " with actionType : " + actionType + " Offer Type : " + offerType );
		Commons.logger.info( Commons.logPrefix + "EventType: " + thisEventDef.get( "type" ).getAsString() );
		Commons.logger.info( Commons.logPrefix + "thisAction value in processPrograms : " + thisAction.toString() );
		if( !isActionApplicable( thisAction, actionType, programId, offerId, actualTriggeredEvent, timestamp, thisTagListItem, customerState ) ) {
			Commons.logger.info( Commons.logPrefix + "*********** Skipping the offer. Offer Application Rules failed *********" );
			return;
		}
		Commons.logger.info( Commons.logPrefix + "Applicable Conditions Passed for offer : " + offerId + " with actionType : " + actionType );
		addAction( thisAction, thisEventDef, actualTriggeredEvent, thisTagListItem, customerState, programId, isSimulated, timestamp, actionType, offerId, offerType );
	}

	protected void addAction( JsonObject thisAction, JsonObject thisEventDef, String actualTriggeredEvent, JsonObject thisTagListItem, CustomerState customerState, String programId, String isSimulated, long timestamp, String actionType,
			String offerId, String offerType ) throws NoSuchMethodException, InstantiationException, InvocationTargetException, IllegalAccessException, ParseException {

		JsonObject offerDef = Commons.allOfferDef.get( offerId ).getAsJsonObject();
		long curDate = SiftMath.getCurrentTime();
		if( offerDef.has( "effectiveDate" ) && curDate < offerDef.get( "effectiveDate" ).getAsLong() ) {
			Commons.logger.info( Commons.logPrefix + "Offer effective date does not satisfy: " + offerId );
			return;
		}
		if( offerDef.has( "expiryDate" ) && curDate > offerDef.get( "expiryDate" ).getAsLong() ) {
			Commons.logger.info( Commons.logPrefix + "Offer expiry date does not satisfy: " + offerId );
			return;
		}
		thisAction.add( "eventDef", thisEventDef );
		thisAction.add( "tagList", thisTagListItem );
		thisAction.addProperty( "offerId", offerId );
		thisAction.addProperty( "offerType", offerType );
		thisAction.addProperty( "programId", programId );
		thisAction.addProperty( "actualTriggeredEvent", actualTriggeredEvent );
		thisAction.addProperty( "actionType", actionType );
		thisAction.addProperty( "IS_SIMULATED", isSimulated );

		manageFlowFunnelCounts( thisEventDef.get( "id" ).getAsString(), programId, offerId, "OFFER_SEGMENTATION", customerState );

		String countAsContact = "Y";
		if( thisAction.has( "isCountAsContact" ) && thisAction.get( "isCountAsContact" ) != null && !thisAction.get( "isCountAsContact" ).isJsonNull() ) {
			customerState.tupleMap.addProperty( "NBO_OFFER_ID", offerId );
			String expId = programId + "_" + offerId + "_" + "COUNT_AS_CONTACT";
			Object result = this.evaluateExpression( expId, thisTagListItem, false, actualTriggeredEvent, customerState );
			if( result != null )
				countAsContact = result.toString();
		}

		customerState.tupleMap.addProperty( "COUNT_AS_CONTACT", countAsContact );

		if( customerState.offerLocationsMap.containsKey( offerId ) ) {
			customerState.tupleMap.addProperty( "SELECTED_OFFER_LOCATION", customerState.offerLocationsMap.get( offerId )[ 0 ] );
			customerState.tupleMap.addProperty( "SELECTED_OFFER_CATEGORY", customerState.offerLocationsMap.get( offerId )[ 1 ] );
		} else {
			String[] selLoc = new String[ 2 ];
			selLoc[ 0 ] = customerState.tupleMap.get( "REQUESTER_LOCATION" ).getAsString();
			selLoc[ 1 ] = offerDef.get( "category" ).getAsString();
			customerState.offerLocationsMap.put( offerId, selLoc );
		}

		customerState.tupleMap.addProperty( "NBO_OFFER_ID", offerId );
		String offerScoreId = programId + "_" + offerId + "_OFFER_PRIORITY_SCORE_EXP";
		if( Commons.expMap.containsKey( offerScoreId ) ) {
			Object result = evaluateExpression( offerScoreId, thisTagListItem, true, thisEventDef.get( "id" ).getAsString(), customerState );
			thisAction.addProperty( "offerPriorityScore", Double.valueOf( result.toString() ) );
		}else {
			offerScoreId = "OFFER_SCORE_" + offerId;
			if( Commons.expMap.containsKey( offerScoreId ) ) {
				Object result = evaluateExpression( offerScoreId, thisTagListItem, true, thisEventDef.get( "id" ).getAsString(), customerState );
				thisAction.addProperty( "offerPriorityScore", Double.valueOf( result.toString() ) );
			}
		}

		//FILTER OFFER BASED ON LOW SCORE THRESHOLD
		if( thisAction.get( "offerPriorityScore").getAsDouble() <= Commons.lowScoreThreshold ) {
			Commons.logger.info( Commons.logPrefix + "Skipping the action because of LowScoreThreshold : " + offerId );
			return;
		}

		if( ( actionType.equals( "notificationAction" ) && offerType.equals( "Notification" ) ) || actionType.equals( "monitoringAction" ) )
			customerState.newOfferActions.add( thisAction ); // contains all the
		else if( actionType.equals( "fulfillmentAction" ) )
			customerState.fulfillmentActions.add( thisAction );
		else if( actionType.equals( "reminderAction" ) )
			customerState.reminderActions.add( thisAction );
	}

	private void handleNextBestActionDynamicAction( JsonObject thisAction, JsonObject thisProgramActionsMap, JsonObject thisEventDef, String actualTriggeredEvent, 
			JsonObject thisTagListItem, CustomerState customerState, String programId,
			String isSimulated, long timestamp ) throws NoSuchMethodException, InstantiationException, InvocationTargetException, IllegalAccessException, ParseException {
		String nextBestActionTriggerId = thisAction.get( "nextBestActionTriggerId" ).getAsString();
		JsonArray allOfferDecisionCriterias = thisAction.get( "nextBestActionDecisionCriteria" ).getAsJsonArray();
		JsonArray jActions = new JsonArray();
		// iterate over all offers
		JsonObject allApplicableOffers = thisProgramActionsMap.get( nextBestActionTriggerId ).getAsJsonArray().get( 0 ).getAsJsonObject();
		Iterator< Entry< String, JsonElement > > itr = allApplicableOffers.entrySet().iterator();
		String[] reqLocations = customerState.tupleMap.get( "REQUESTER_LOCATION" ).getAsString().split( "," );  
		while( itr.hasNext() ) {
			String thisOffer = itr.next().getKey();
			customerState.tupleMap.addProperty( "NBO_OFFER_ID", thisOffer );
			for( int loc=0; loc<reqLocations.length; loc++ ) {
				customerState.tupleMap.addProperty( "CURRENT_REQUESTER_LOCATION", reqLocations[ loc ] );
				Commons.logger.info( Commons.logPrefix + "Evaluating NBO Criteria for offer : " + thisOffer + " for " + reqLocations[ loc ] );
				for( int i = 0; i < allOfferDecisionCriterias.size(); i++ ) {
					JsonArray thisCriteriaArray = allOfferDecisionCriterias.get( i ).getAsJsonArray();
					for( int j = 0; j < thisCriteriaArray.size(); j++ ) {
						JsonObject aCriteria = thisCriteriaArray.get( j ).getAsJsonObject();
						JsonArray resultArray = new JsonArray();
						evaluateOfferDecisionCriteria( aCriteria, nextBestActionTriggerId + "_" + i + "_" + j + "_0", resultArray, thisTagListItem, actualTriggeredEvent, customerState );
						if( resultArray.size() > 0 ) {
							jActions.add( new JsonPrimitive( thisOffer ) );
							String[] offerLocation = new String[ 2 ];
							offerLocation[ 0 ] = reqLocations[ loc ];
							offerLocation[ 1 ] = resultArray.get( 0  ).getAsString();
							if( ! customerState.offerLocationsMap.containsKey( thisOffer ) )
								customerState.offerLocationsMap.put( thisOffer, offerLocation );
							break;
						}	
					}
				}
			}
		}
		Commons.logger.info( Commons.logPrefix + "Applicable Actions for Next Best Dynamic Action : " + jActions.toString() );
		String nbaActionTriggerId = thisAction.get( "nextBestActionTriggerId" ).getAsString();
		JsonObject nbaActions = thisProgramActionsMap.get( nbaActionTriggerId ).getAsJsonArray().get( 0 ).getAsJsonObject();
		for( int i = 0; i < jActions.size(); i++ ) {
			JsonObject thisNBAAction = nbaActions.get( jActions.get( i ).getAsString() ).getAsJsonObject();
			String offerId = thisNBAAction.get( "offerId" ).getAsString();
			String offerType = thisNBAAction.get( "offerType" ).getAsString();
			addAction( thisNBAAction, thisEventDef, actualTriggeredEvent, thisTagListItem, customerState, programId, isSimulated, timestamp, thisNBAAction.get( "actionType" ).getAsString(), offerId, offerType );
		}
	}

	private void handleNextBestAction( JsonObject thisAction, JsonObject thisProgramActionsMap, JsonObject thisEventDef, String actualTriggeredEvent, JsonObject thisTagListItem, CustomerState customerState, String programId,
			String isSimulated, long timestamp ) throws NoSuchMethodException, InstantiationException, InvocationTargetException, IllegalAccessException, ParseException {
		String nextBestActionId = thisAction.get( "nextBestActionId" ).getAsString();
		Commons.logger.info( Commons.logPrefix + "Evaluating Next Best Action Criteria : " + nextBestActionId );
		String expressionId = programId + "_" + nextBestActionId;
		Object result = evaluateExpression( expressionId, thisTagListItem, false, actualTriggeredEvent, customerState );
		if( result == null )
			return;
		JsonArray jActions = ( JsonArray ) result;
		Commons.logger.info( Commons.logPrefix + "Applicable Actions for Next Best Action : " + jActions.toString() );
		String nbaActionTriggerId = thisAction.get( "nextBestActionTriggerId" ).getAsString();
		JsonObject nbaActions = thisProgramActionsMap.get( nbaActionTriggerId ).getAsJsonArray().get( 0 ).getAsJsonObject();
		for( int i = 0; i < jActions.size(); i++ ) {
			JsonObject thisNBAAction = nbaActions.get( jActions.get( i ).getAsString() ).getAsJsonObject();
			String offerId = thisNBAAction.get( "offerId" ).getAsString();
			String offerType = thisNBAAction.get( "offerType" ).getAsString();
			addAction( thisNBAAction, thisEventDef, actualTriggeredEvent, thisTagListItem, customerState, programId, isSimulated, timestamp, thisNBAAction.get( "actionType" ).getAsString(), offerId, offerType );
		}
	}

	private void handleOfferDecisionAction( JsonObject thisAction, JsonObject thisProgramActionsMap, JsonObject thisEventDef, String actualTriggeredEvent, JsonObject thisTagListItem, CustomerState customerState, String programId,
			String isSimulated, long timestamp ) throws NoSuchMethodException, InstantiationException, InvocationTargetException, IllegalAccessException, ParseException {
		String offerDecisionId = thisAction.get( "offerDecisionTriggerId" ).getAsString();
		Commons.logger.info( Commons.logPrefix + "Evaluating Offer Decision Table : " + offerDecisionId );

		JsonArray jActions = new JsonArray();
		JsonArray allOfferDecisionCriterias = thisAction.get( "offerDecisionCriteria" ).getAsJsonArray();
		for( int i = 0; i < allOfferDecisionCriterias.size(); i++ ) {
			JsonArray thisCriteriaArray = allOfferDecisionCriterias.get( i ).getAsJsonArray();
			for( int j = 0; j < thisCriteriaArray.size(); j++ ) {
				JsonObject jTree = thisCriteriaArray.get( j ).getAsJsonObject();
				evaluateOfferDecisionCriteria( jTree, offerDecisionId + "_" + i + "_" + j + "_0", jActions, thisTagListItem, actualTriggeredEvent, customerState );
			}
		}

		Commons.logger.info( Commons.logPrefix + "Applicable Actions for Decistion Table : " + offerDecisionId + "  = " + jActions.toString() );
		JsonObject decisionTableActions = thisProgramActionsMap.get( offerDecisionId ).getAsJsonArray().get( 0 ).getAsJsonObject();
		for( int i = 0; i < jActions.size(); i++ ) {
			JsonObject thisDecisionAction = decisionTableActions.get( jActions.get( i ).getAsString() ).getAsJsonObject();
			String offerId = thisDecisionAction.get( "offerId" ).getAsString();
			String offerType = thisDecisionAction.get( "offerType" ).getAsString();
			addAction( thisDecisionAction, thisEventDef, actualTriggeredEvent, thisTagListItem, customerState, programId, isSimulated, timestamp, thisDecisionAction.get( "actionType" ).getAsString(), offerId, offerType );
		}
	}

	private void evaluateOfferDecisionCriteria( JsonObject aNode, String id, JsonArray resultArray, JsonObject thisTagListItem, String actualTriggeredEvent, CustomerState customerState )
			throws NoSuchMethodException, InstantiationException, InvocationTargetException, IllegalAccessException, ParseException {
		Commons.logger.info( Commons.logPrefix + "Processing Decision Table Entry : " + aNode.get( "expressionName" ).getAsString() +  " ( " +  id + " )" );
		Object result = evaluateExpression( id, thisTagListItem, false, actualTriggeredEvent, customerState );
		if( result == null || result.toString().equals( "false" ) )
			return;

		if( aNode.get( "nextLevel" ) == null || aNode.get( "nextLevel" ).isJsonNull() ) {
			if( aNode.get( "resultType" ).getAsString().equals( "Offer" ) )
				resultArray.add( new JsonPrimitive( aNode.get( "result" ).getAsString() ) );
			return;
		}
		JsonArray jNextLevel = aNode.get( "nextLevel" ).getAsJsonArray();
		for( int i = 0; i < jNextLevel.size(); i++ ) {
			evaluateOfferDecisionCriteria( jNextLevel.get( i ).getAsJsonObject(), id + "_" + i, resultArray, thisTagListItem, actualTriggeredEvent, customerState );
		}
	}


	// MULTIWAVE
	protected void createTagForNextWave( JsonObject thisAction, String programId, String offerId, CustomerState customerState, JsonObject thisEventDef, String actualTriggeredEvent, String flowId, boolean isControl, JsonObject nextWave,
			JsonObject triggerMessage ) {
		// Add a new tag to subscriberEventTags
		// Add it to the eventsTaggedForSubscribersMap
		// Save to persist will happen automatically after the processing..
		// TODO : Check Livechart update requirements.
		JsonObject jTag = new JsonObject();
		JsonElement jSubsId = customerState.subscriberIndicators.get( "SUBSCRIBER_ID_LifeTime-L" );
		String subsId = "-99";
		if( jSubsId != null )
			subsId = jSubsId.getAsString();
		jTag.addProperty( "SUBSCRIBER_ID", subsId );
		boolean triggerInNextContactWindow = false;
		if ( nextWave.has( "triggerInNextContactWindow" ) )
			triggerInNextContactWindow = nextWave.get("triggerInNextContactWindow").getAsBoolean();

		long lMonitoringStart = ( long ) ( customerState.siftMath.getCurrentTime() + ( nextWave.get( "cooldownDuration" ).getAsDouble() * SiftMath.msecPerDay ) );

		double dur = nextWave.get( "monitoringDuration" ).getAsDouble();
		long monioringEnd = -1;
		if( dur == -1 ) {
			monioringEnd = ( lMonitoringStart / 1000 ) * 1000; // forces the milliseconds part to 000
			monioringEnd += ( 5 * 60000 );
			jTag.addProperty( "TRIGGER_IN_NEXT_CONTACT_WINDOW", triggerInNextContactWindow );
		}else {
			monioringEnd = ( long ) ( lMonitoringStart + ( dur * SiftMath.msecPerDay ) );
			monioringEnd = ( monioringEnd / 1000 ) * 1000;
			monioringEnd = ( monioringEnd - ( monioringEnd % 86400000 ) ) + 86340000;
		}

		String behaviorEventId = Commons.allProgramDef.get( programId ).getAsJsonObject().get( "triggers" ).getAsJsonObject().get( "Behavioural" ).getAsJsonArray().get( 0 ).getAsString();

		jTag.addProperty( "MONITORING_DURATION", dur );
		jTag.addProperty( "MONITORING_START_TIME", lMonitoringStart );
		jTag.addProperty( "MONITORING_END_TIME", monioringEnd );
		jTag.addProperty( "FLOW_ID", flowId );
		jTag.addProperty( "PROGRAM_ID", programId );
		jTag.addProperty( "OFFER_ID", offerId );
		jTag.addProperty( "IS_CONTROL", isControl );
		jTag.addProperty( "BEHAVIOURAL_TRIGGER", behaviorEventId );
		jTag.addProperty( "REQUESTER_LOCATION", customerState.tupleMap.get( "REQUESTER_LOCATION" ).getAsString() );
		jTag.addProperty( "REQUEST_ID", triggerMessage.get( "ORIG_REQUEST_ID" ).getAsString() );

		if( customerState.subscriberNextWaveEventTags == null )
			customerState.subscriberNextWaveEventTags = new JsonObject();

		String nextWaveTriggerId = nextWave.get( "triggerId" ).getAsString();
		customerState.subscriberNextWaveEventTags.add( nextWaveTriggerId, jTag );

		// Check if an automated schedule has to be created. This is to be used
		// if the next wave does not depend on any customer action
		//createScheduleForScheduledNextWave( thisAction, customerState, nextWaveTriggerId, 3, nextWaveTriggerId );
	}

	private JsonObject createTriggerMessage( String eventId, String programId, String offerId, JsonObject thisEventDef, JsonObject aTag, long triggerTimeStamp, JsonObject thisAction, CustomerState customerState )
			throws NoSuchMethodException, InstantiationException, InvocationTargetException, IllegalAccessException, ParseException {
		// create the trigger message
		JsonObject triggerMessage = new JsonObject();
		triggerMessage.addProperty( "MSISDN", customerState.customerId );
		String subsId = "-99";
		JsonElement jSubsId = customerState.subscriberIndicators.get( "SUBSCRIBER_ID_LifeTime-L" );
		if( jSubsId != null )
			subsId = jSubsId.getAsString();
		JsonObject thisOffer = Commons.allOfferDef.get( offerId ).getAsJsonObject();
		String offerDescription = "";
		JsonElement offerDescEle = thisOffer.get( "description" );
		if( offerDescEle != null && !offerDescEle.isJsonNull() )
			offerDescription = offerDescEle.getAsString();
		double offerScore = 0;
		JsonElement offerScoreEle = thisAction.get( "offerPriorityScore" );
		if( offerScoreEle != null && !offerScoreEle.isJsonNull() )
			offerScore = offerScoreEle.getAsDouble();
		triggerMessage.addProperty( "SUBSCRIBER_ID", subsId );
		triggerMessage.addProperty( "CATEGORY", thisOffer.get( "category" ).getAsString() );
		triggerMessage.addProperty( "PROGRAM_ID", programId );
		triggerMessage.addProperty( "OFFER_ID", offerId );
		triggerMessage.addProperty( "EVENT_ID", eventId );
		triggerMessage.addProperty( "EVENT_TIMESTAMP", triggerTimeStamp );
		triggerMessage.addProperty( "OFFER_DESCRIPTION", offerDescription );
		triggerMessage.addProperty( "OFFER_TYPE", thisOffer.get( "offerType" ).getAsString() );
		triggerMessage.addProperty( "OFFER_SCORE", offerScore );

		triggerMessage.addProperty( "APPLICABLE_LOCATIONS", thisOffer.get( "applicableLocations" ).getAsString() );
		triggerMessage.addProperty( "NEW_OFFER_FLAG", true );
		if( customerState.offerLocationsMap.containsKey( offerId ) )
			triggerMessage.addProperty( "SELECTED_OFFER_CATEGORY", customerState.offerLocationsMap.get( offerId )[ 1 ] );

		JsonArray payload = constructEventPayload( thisEventDef, eventId, aTag, customerState );
		Commons.logger.info( Commons.logPrefix + "Event Payload : " + payload.toString() );
		triggerMessage.add( "EVENT_PAYLOAD", payload );
		return triggerMessage;
	}

	private boolean isOfferInTheRequesterLocation( String offerId, CustomerState customerState ) {
		// STEROID-GOLDBISCUIT
		JsonObject offerDef = Commons.allOfferDef.get( offerId );
		if( !offerDef.has( "applicationLocations" ) )
			return true;

		JsonArray offerLocs = offerDef.get( "applicationLocations" ).getAsJsonArray();
		String[ ] incomingLocations = customerState.tupleMap.get( "REQUESTER_LOCATION" ).getAsString().split( "," );
		for( int i = 0; i < offerLocs.size(); i++ )
			for( int j = 0; j < incomingLocations.length; j++ )
				if( offerLocs.get( i ).getAsString().equals( incomingLocations[ j ] ) )
					return true;

		return false;
	}

	private boolean isActionApplicable( JsonObject thisAction, String actionType, String programId, String offerId, String eventId, long timestamp, JsonObject thisTagListItem, CustomerState customerState ) throws Exception {
		// check if offer segmentation is applicable. Ignore this for
		// conditional fulfillments and reminders

		// STEROID-GOLDBISCUIT
		if( !isOfferInTheRequesterLocation( offerId, customerState ) )
			return false;

		boolean condFulfullment = thisAction.get( "offerType" ).getAsString().equals( "Conditional" ) && actionType.equals( "fulfillmentAction" );
		//to pass to evaluateExpression
		boolean condNotification = thisAction.get( "offerType" ).getAsString().equals( "Conditional" ) && actionType.equals( "monitoringAction" );

		Commons.logger.info( Commons.logPrefix + "Inside isActionApplicable for offer: " + offerId );
		if( !condFulfullment && !actionType.equals( "reminderAction" ) ) {

			if( thisAction.has( "offerCriteriaArray" ) && !thisAction.get( "offerCriteriaArray" ).isJsonNull() ) {
				JsonArray criteriaArray = thisAction.get( "offerCriteriaArray" ).getAsJsonArray();
				for( int i = 0; i < criteriaArray.size(); i++ ) {
					String expressionId = "OFFER_SEGMENTATION_" + offerId + "_" + i;
					Object result = evaluateExpression( expressionId, thisTagListItem, condNotification, eventId, customerState );
					if( result.toString().equals( "false" ) ) {
						Commons.logger.info( Commons.logPrefix + "*********** Segmentation Criteria failed : " + programId + " - " + offerId );
						return false;
					}
				}
			}else {
				if( thisAction.has( "offerCriteria" ) && !thisAction.get( "offerCriteria" ).isJsonNull() ) {
					String expressionId = "OFFER_SEGMENTATION_" + offerId;
					Object result = evaluateExpression( expressionId, thisTagListItem, condNotification, eventId, customerState );
					if( result.toString().equals( "false" ) ) {
						Commons.logger.info( Commons.logPrefix + "*********** Segmentation Criteria failed : " + programId + " - " + offerId );
						return false;
					}
				}
			}
		}

		// check for max fulfillment limits per customer
		// Commented below for multiple fulfilments enhancement.
		/*
		 * if( condFulfullment ) { JsonElement thisProgramHistory =
		 * customerState.subscriberPrograms.get( programId ); if(
		 * thisProgramHistory == null ) return true; JsonArray
		 * thisProgramHistoryArray = thisProgramHistory.getAsJsonArray();
		 * JsonObject latestProgramInstance = thisProgramHistoryArray.get(
		 * thisProgramHistoryArray.size() - 1 ).getAsJsonObject(); JsonElement
		 * jele = latestProgramInstance.get( offerId ); if( jele == null )
		 * return true; JsonArray offerHistory = jele.getAsJsonArray();
		 * JsonObject latestOffer = offerHistory.get( offerHistory.size() - 1
		 * ).getAsJsonObject();
		 * 
		 * JsonElement jactionHistory = latestOffer.get( actionType ); if(
		 * jactionHistory != null ) { //Commons.logger.log(
		 * java.util.logging.Level.INFO, Commons.logPrefix +
		 * "Max Fulfillments Per Customer Exceeded. Fulfillment already existing  : "
		 * + eventId ); this.handleActionFailureReasons( customerState,
		 * customerState.customerId,
		 * "Customer has already taken the Offer. Customer is trying to retry the campaign response action"
		 * , "002", programId, offerId ); return false; } }
		 */
		return true;
	}

	private JsonObject handleNotificationAction( JsonObject thisAction, String programId, String offerId, JsonObject thisEventDef, String actualTriggeredEvent, JsonObject thisTagListItem, long timestamp,
			ArrayList< String > applicableChannels, CustomerState customerState, String action, String flowId, boolean isConditionalNotification ) throws Exception {
		JsonArray constructedMessages = constructCommunicationMessages( thisAction, thisAction.get( "offerMessages" ).getAsJsonArray(), "OFFER_MESSAGE_", thisAction.get( "offerId" ).getAsString(), thisTagListItem, isConditionalNotification,
				action, applicableChannels, actualTriggeredEvent, customerState, programId, flowId );
		JsonObject triggerMessage = this.createTriggerMessage( actualTriggeredEvent, programId, offerId, thisEventDef, thisTagListItem, timestamp, thisAction, customerState );
		triggerMessage.add( "NOTIFICATION_MESSAGES", constructedMessages.get( 0 ) );
		if( constructedMessages.size() > 0 ) {
			JsonObject jObjTarget = constructedMessages.get( 1 ).getAsJsonObject();
			if( jObjTarget.has( "monitoredValue" ) ) {
				double target = jObjTarget.get( "monitoredValue" ).getAsDouble();
			}
			triggerMessage.add( "OFFER_PAYLOAD", jObjTarget );
		}
		return triggerMessage;
	}

	public JsonObject createTagList( JsonObject thisAction, String programId, String offerId, JsonObject thisEventDef, String triggeredEvent, JsonObject thisTagListItem, long timestamp, String flowId, boolean isControl,
			CustomerState customerState ) throws Exception {
		JsonObject jObjMon = thisAction.get( "fulfillmentTrigger" ).getAsJsonObject();
		String monitoringEvent = jObjMon.get( "triggerId" ).getAsString();

		JsonObject jTagList = null;

		// TODO: Get the taglist from internal memory first.
		String strTagList = ( String ) Commons.pandaCache.get( "EventTagList-" + customerState.customerId );
		if( strTagList == null ) {
			jTagList = new JsonObject();
			jTagList.addProperty( "docType", "TagList" );
		}else
			jTagList = ( JsonObject ) Commons.parser.parse( strTagList );

		JsonObject jTag = new JsonObject();
		long lCurrentTime = customerState.siftMath.getCurrentTime();
		jTag.addProperty( "MONITORING_START_TIME", lCurrentTime );

		double dMonDuration = 0;
		JsonElement monDurExp = jObjMon.get( "monitoringDuration1" );
		if( monDurExp != null ) {
			// "MONITORING_DURATION_" + fulfillmentTriggerId;
			String key = "MONITORING_DURATION_" + monitoringEvent;
			Object result = evaluateExpression( key, thisTagListItem, false, triggeredEvent, customerState );
			dMonDuration = Double.parseDouble( result.toString() );
		}else {
			dMonDuration = jObjMon.get( "monitoringDuration" ).getAsDouble();
		}

		long monioringEnd;
		// handle session indicators
		if( dMonDuration == -1 ) {
			double sessionDuration = 60000;
			JsonElement eSess = Commons.siftConfig.get( "sessionOfferMonitoringDuration" );
			if( eSess != null && !eSess.isJsonNull() )
				sessionDuration = eSess.getAsDouble();
			dMonDuration = sessionDuration;
			monioringEnd = ( long ) ( lCurrentTime + dMonDuration );
		}else {
			monioringEnd = ( long ) ( lCurrentTime + ( dMonDuration * SiftMath.msecPerDay ) );
			monioringEnd = ( monioringEnd / 1000 ) * 1000;
			// For getting the 23:59:59 time of the day
			monioringEnd = ( monioringEnd - ( monioringEnd % 86400000 ) ) + 86399000;
		}
		String isSimulated = customerState.tupleMap.get( "IS_SIMULATED" ).getAsString();

		jTag.addProperty( "MONITORING_END_TIME", monioringEnd );
		jTag.addProperty( "FLOW_ID", flowId );
		jTag.addProperty( "PROGRAM_ID", programId );
		jTag.addProperty( "OFFER_ID", offerId );
		jTag.addProperty( "IS_CONTROL", isControl );
		jTag.addProperty( "BEHAVIOURAL_TRIGGER", triggeredEvent );
		jTag.addProperty( "REQUESTER_LOCATION", customerState.tupleMap.get( "REQUESTER_LOCATION" ).getAsString() );
		jTag.addProperty( "REQUEST_ID", customerState.siftResponses.get( "REQUEST_ID" ).getAsString() );

		if( "true".equals( isSimulated ) ) {
			jTag.addProperty( "IS_SIMULATED", true );
		}else {
			jTag.addProperty( "IS_SIMULATED", false );
		}
		Commons.logger.info( Commons.logPrefix + "*********** jTag while creating tag list: " + jTag.toString() );
		jTagList.add( monitoringEvent, jTag );

		return jTagList;
	}

	public JsonObject handleMonitoringAction( JsonObject thisAction, String programId, String offerId, JsonObject thisEventDef, String triggeredEvent, JsonObject thisTagListItem, long timestamp, String flowId, boolean isControl,
			ArrayList< String > applicableChannels, CustomerState customerState, JsonObject triggerMessage ) throws Exception {

		JsonObject jObjMon = thisAction.get( "fulfillmentTrigger" ).getAsJsonObject();
		String monitoringEvent = jObjMon.get( "triggerId" ).getAsString();

		String participationType = "AUTO";
		JsonElement eParticipationType = thisAction.get( "participationType" );
		if( eParticipationType != null && !eParticipationType.isJsonNull() & !eParticipationType.getAsString().equals( "" ) ) {
			String key = "PARTICIPATION_TYPE_" + offerId;
			Object result = evaluateExpression( key, thisTagListItem, false, triggeredEvent, customerState );
			participationType = result.toString();
		}
		triggerMessage.addProperty( "PARTICIPATION_TYPE", participationType );

		JsonObject TagList = createTagList( thisAction, programId, offerId, thisEventDef, triggeredEvent, thisTagListItem, timestamp, flowId, isControl, customerState );

		JsonObject thisTag = TagList.get( monitoringEvent ).getAsJsonObject();

		thisAction.add( "tagList", thisTag );

		long monStartTime = thisTag.get( "MONITORING_START_TIME" ).getAsLong();
		long monEndTime = thisTag.get( "MONITORING_END_TIME" ).getAsLong();
		triggerMessage.addProperty( "OFFER_END_DATE_LTZ", monEndTime - java.util.TimeZone.getDefault().getOffset( monEndTime ) );
		triggerMessage.addProperty( "OFFER_START_DATE", monStartTime );
		triggerMessage.addProperty( "OFFER_END_DATE", monEndTime );

		long noEventTriggerTime = monEndTime;

		if( participationType.equals( "AUTO" ) ) {
			String isSimulated = customerState.tupleMap.get( "IS_SIMULATED" ).getAsString();
			if( !isControl && !isSimulated.equals( "true" ) ) {
				createScheduleForReminders( thisAction, customerState, monitoringEvent, flowId, isControl );
				createTagForActivityBasedReminders( customerState, thisAction, customerState.customerId, programId, offerId, TagList, monEndTime, flowId, isControl, thisAction.get( "activityReminderTriggers" ).getAsJsonArray() );
				// Create Tag for Hybrid reminders.
				if( thisAction.has( "hybridReminderTriggers" ) && ! thisAction.get( "hybridReminderTriggers" ).isJsonNull() ) 
					createTagForActivityBasedReminders( customerState, thisAction, customerState.customerId, programId, offerId, TagList, monEndTime, flowId, isControl, thisAction.get( "hybridReminderTriggers" ).getAsJsonArray() );
				if( jObjMon.has( "noEventTriggerId" ) && !jObjMon.get( "noEventTriggerId" ).isJsonNull() )
					createScheduleForNonTakeup( customerState, jObjMon, monitoringEvent, flowId, programId, noEventTriggerTime, offerId );
			}
			Commons.pandaCache.set( "EventTagList-" + customerState.customerId, 0, TagList.toString() );
			TagList.remove( "docType" );
			commonsState.eventsTaggedForSubscribersMap.put( customerState.customerId, TagList );
			return triggerMessage;
		}
		return triggerMessage;
	}

	public void createTagForActivityBasedReminders( CustomerState customerState, JsonObject thisAction, String msisdn, String programId, String offerId, 
			JsonObject jTagList, long monioringEnd, String flowId, boolean isControl, JsonArray activityRemArray ) {
		String behaviorEventId = null;
		if( jTagList.has( "BEHAVIOURAL_TRIGGER" ) )
			behaviorEventId = jTagList.get( "BEHAVIOURAL_TRIGGER" ).getAsString();
		else
			behaviorEventId = Commons.allProgramDef.get( programId ).getAsJsonObject().get( "triggers" ).getAsJsonObject().get( "Behavioural" ).getAsJsonArray().get( 0 ).getAsString();
		for( int i = 0; i < activityRemArray.size(); i++ ) {
			JsonObject aActivityRem = activityRemArray.get( i ).getAsJsonObject();
			if( aActivityRem.has( "reminderType" ) && ! aActivityRem.get( "reminderType" ).getAsString().equals( "Activity" ) )
				continue;
			Commons.logger.info( Commons.logPrefix + "Creating activity based reminders for offer: " + offerId );
			Commons.logger.info( Commons.logPrefix + "Reminder: " + aActivityRem.toString() );
			JsonObject jTag = new JsonObject();
			long lCurrentTime = customerState.siftMath.getCurrentTime();
			jTag.addProperty( "MONITORING_START_TIME", lCurrentTime );
			jTag.addProperty( "MONITORING_END_TIME", monioringEnd );
			jTag.addProperty( "FLOW_ID", flowId );
			jTag.addProperty( "PROGRAM_ID", programId );
			jTag.addProperty( "OFFER_ID", offerId );
			jTag.addProperty( "IS_CONTROL", isControl );
			jTag.addProperty( "BEHAVIOURAL_TRIGGER", behaviorEventId );
			jTag.addProperty( "REQUESTER_LOCATION", customerState.tupleMap.get( "REQUESTER_LOCATION" ).getAsString() );
			jTag.addProperty( "REQUEST_ID", customerState.siftResponses.get( "REQUEST_ID" ).getAsString() );
			String monitoringEvent = aActivityRem.get( "triggerId" ).getAsString();
			jTagList.add( monitoringEvent, jTag );
		}
	}

	protected void createScheduleForNonTakeup( CustomerState customerState, JsonObject jObjMon, String monitoringEvent, String flowId, String programId, long monioringEnd, String offerId ) {
		String noEventTriggerId = jObjMon.get( "noEventTriggerId" ).getAsString();
		long contactTime = customerState.siftMath.getNextContactTime( customerState.tupleMap.get( "REQUESTER_CHANNEL" ).getAsString(), monioringEnd );
		if( contactTime == -1 )
			contactTime = monioringEnd;
		Commons.logger.info( Commons.logPrefix + "Monitoring end date of previous wave: " + monioringEnd + "***Contact time for next wave: " + contactTime );
		String key = Long.toString( contactTime ) + "-" + commonsState.nWorkerId + "-NE";

		String jVal = ( String ) Commons.pandaCache.get( key );
		JsonObject jObj = null;
		if( jVal == null )
			jObj = new JsonObject();
		else
			jObj = ( JsonObject ) Commons.parser.parse( jVal );
		jObj.addProperty( customerState.customerId, programId + "," + offerId + "," + noEventTriggerId + "," + flowId + "," + monitoringEvent );
		Commons.pandaCache.set( key, 0, jObj.toString() );
		Commons.logger.info( Commons.logPrefix + "Created a new no-event schedule : " + key + "***schedule created: " + jObj.toString() );
	}

	private void createScheduleForExternalMonitoringRequest( String eventId, String depEvent, JsonObject tuple, String schedType, long schedHours ) {
		// logger.log( java.util.logging.Level.INFO, Commons.logPrefix + "Creating the for timestamp
		// : " + schedHours );
		String msisdn = tuple.get( "MSISDN" ).getAsString();
		Calendar cal = Calendar.getInstance();
		cal.setTime( new java.util.Date( schedHours ) );
		int nMin = cal.get( Calendar.MINUTE );

		// int quotient = nMin / scheduleMinutesWindow;
		// int reminder = nMin % scheduleMinutesWindow;
		//
		// if( reminder > ( scheduleMinutesWindow / 2 ) )
		// quotient++;
		//
		// int nMinSchedule = quotient * scheduleMinutesWindow;

		cal.set( cal.get( Calendar.YEAR ), cal.get( Calendar.MONTH ), cal.get( Calendar.DATE ), cal.get( Calendar.HOUR_OF_DAY ), nMin, 0 );

		// logger.log( java.util.logging.Level.INFO, Commons.logPrefix + "New Calendar value without
		// seconds : " + cal.getTimeInMillis() );

		String key = cal.getTimeInMillis() + "-" + commonsState.nWorkerId + "-" + schedType;

		String jVal = ( String ) Commons.pandaCache.getClient().get( key );
		JsonObject jObj = null;
		if( jVal == null )
			jObj = new JsonObject();
		else
			jObj = ( JsonObject ) Commons.parser.parse( jVal );
		jObj.addProperty( msisdn, eventId + "," + depEvent );
		Commons.pandaCache.set( key, 0, jObj.toString() );
		Commons.logger.info( Commons.logPrefix + "Created a new schedule : " + key + " ' " + jObj.toString() );
	}

	protected void createScheduleForReminders( JsonObject thisAction, CustomerState customerState, String monitoringEvent, String flowId, boolean isControl ) {
		JsonArray schedRemArray = thisAction.get( "scheduledReminderTriggers" ).getAsJsonArray();
		for( int i = 0; i < schedRemArray.size(); i++ ) {
			JsonObject aSchedRem = schedRemArray.get( i ).getAsJsonObject();
			long schedMins = aSchedRem.get( "remindAfter" ).getAsLong();
			long scheduleTime = customerState.siftMath.getCurrentTime();
			scheduleTime = ( scheduleTime / 1000 ) * 1000; // forces the
			// milliseconds part
			// to 000
			scheduleTime += ( schedMins * 60000 );
			Calendar cal = Calendar.getInstance();
			cal.setTime( new java.util.Date( scheduleTime ) );
			int nMin = cal.get( Calendar.MINUTE );
			cal.set( cal.get( Calendar.YEAR ), cal.get( Calendar.MONTH ), cal.get( Calendar.DATE ), cal.get( Calendar.HOUR_OF_DAY ), nMin, 0 );
			long curTime = cal.getTimeInMillis();
			String key = curTime + "-" + commonsState.nWorkerId + "-RE";

			String jVal = ( String ) Commons.pandaCache.get( key );
			JsonObject jObj = null;
			if( jVal == null )
				jObj = new JsonObject();
			else
				jObj = ( JsonObject ) Commons.parser.parse( jVal );
			jObj.addProperty( customerState.customerId, monitoringEvent + "," + aSchedRem.get( "triggerId" ).getAsString() );
			int expiryInSec = ( int ) ( ( curTime - customerState.siftMath.getCurrentTime() ) + 3 * SiftMath.msecPerDay ) / 1000;
			Commons.pandaCache.set( key, expiryInSec, jObj.toString() );
			Commons.logger.info( Commons.logPrefix + "Created a new schedule : " + key + " ' " + jObj.toString() + " expiring after secs: " + expiryInSec );
		}
	}

	private void createScheduleForScheduledAction( JsonObject thisAction, CustomerState customerState, String monitoringEvent ) {
		String scheduledEvent = thisAction.get( "scheduledEventId" ).getAsString();
		long schedMins = thisAction.get( "scheduledDuration" ).getAsLong();
		long scheduleTime = customerState.siftMath.getCurrentTime();
		scheduleTime = ( scheduleTime / 1000 ) * 1000; // forces the
		// milliseconds part
		// to 000
		scheduleTime += ( schedMins * 60000 );
		Calendar cal = Calendar.getInstance();
		cal.setTime( new java.util.Date( scheduleTime ) );
		int nMin = cal.get( Calendar.MINUTE );
		cal.set( cal.get( Calendar.YEAR ), cal.get( Calendar.MONTH ), cal.get( Calendar.DATE ), cal.get( Calendar.HOUR_OF_DAY ), nMin, 0 );
		long curTime = cal.getTimeInMillis();
		String key = curTime + "-" + commonsState.nWorkerId + "-SE";

		String jVal = ( String ) Commons.pandaCache.get( key );
		JsonObject jObj = null;
		if( jVal == null )
			jObj = new JsonObject();
		else
			jObj = ( JsonObject ) Commons.parser.parse( jVal );
		jObj.addProperty( customerState.customerId, monitoringEvent + "," + scheduledEvent );
		Commons.pandaCache.set( key, 0, jObj.toString() );
		Commons.logger.info( Commons.logPrefix + "Created a new schedule : " + key + " ' " + jObj.toString() );
	}

	private void createScheduleForScheduledNextWave( JsonObject thisAction, CustomerState customerState, String monitoringEvent, int schedMins, String scheduledEvent ) {
		long scheduleTime = customerState.siftMath.getCurrentTime();
		scheduleTime = ( scheduleTime / 1000 ) * 1000; // forces the
		// milliseconds part to
		// 000
		scheduleTime += ( schedMins * 60000 );
		Calendar cal = Calendar.getInstance();
		cal.setTime( new java.util.Date( scheduleTime ) );
		int nMin = cal.get( Calendar.MINUTE );
		cal.set( cal.get( Calendar.YEAR ), cal.get( Calendar.MONTH ), cal.get( Calendar.DATE ), cal.get( Calendar.HOUR_OF_DAY ), nMin, 0 );
		long curTime = cal.getTimeInMillis();
		String key = curTime + "-" + commonsState.nWorkerId + "-RE";

		String jVal = ( String ) Commons.pandaCache.get( key );
		JsonObject jObj = null;
		if( jVal == null )
			jObj = new JsonObject();
		else
			jObj = ( JsonObject ) Commons.parser.parse( jVal );
		jObj.addProperty( customerState.customerId, monitoringEvent + "," + scheduledEvent );
		Commons.pandaCache.set( key, 0, jObj.toString() );
		Commons.logger.info( Commons.logPrefix + "Created a new schedule for Automated Next Wave : " + key + " ' " + jObj.toString() );
	}

	public void processNextWave(CustomerState customerState, String subscriber, JsonObject tupleMap,
			Date thisRecordDate, String serviceType, Calendar cal) throws Exception {

		if (customerState.subscriberNextWaveEventTags != null) {
			JsonObject nextWaveEventTags = (JsonObject) Commons.parser
					.parse(customerState.subscriberNextWaveEventTags.toString());
			Commons.logger.info( Commons.logPrefix + "Processing the next wave : " + nextWaveEventTags.toString());
			customerState = this.getIndicatorsEventsTags(subscriber, tupleMap, thisRecordDate, serviceType, cal);

			Iterator<Entry<String, JsonElement>> itr = nextWaveEventTags.entrySet().iterator();
			while (itr.hasNext()) {
				Entry<String, JsonElement> thisEntry = itr.next();
				String scheduledEventId = thisEntry.getKey();
				JsonObject thisWaveObject = thisEntry.getValue().getAsJsonObject();
				double duration = thisWaveObject.get("MONITORING_DURATION").getAsDouble();
				if (duration != -1)
					continue;

				JsonObject aTag = thisWaveObject;
				JsonObject thisEventDef = Commons.allEveDef.get(scheduledEventId);
				if (thisEventDef == null || thisEventDef.isJsonNull()) {
					Commons.logger.info( "------------- Event Definition not available  : " + scheduledEventId + " ---------------------------------");
					continue;
				}

				String behaviorEventId = aTag.get("BEHAVIOURAL_TRIGGER").getAsString();
				JsonElement ele = Commons.siftConfig.get("eventLocations");
				if (ele == null)
					throw new LocationNotDefinedException("Within Scehdule: No Event Locations Defined in SiftConfig");
				JsonObject jLoc = ele.getAsJsonObject();
				if (jLoc.get(behaviorEventId) == null)
					throw new LocationNotDefinedException(
							"Within Scehdule: No Location Defined for event Id : " + behaviorEventId);

				boolean triggerInNextContactWindow = false;
				if (thisWaveObject.has("TRIGGER_IN_NEXT_CONTACT_WINDOW"))
					triggerInNextContactWindow = thisWaveObject.get("TRIGGER_IN_NEXT_CONTACT_WINDOW").getAsBoolean();

				JsonObject eventLoc = jLoc.get(behaviorEventId).getAsJsonObject();
				boolean contactLaterFlag = false;
				long contactTime = SiftMath.getCurrentTime();
				String programId = thisWaveObject.get("PROGRAM_ID").getAsString();
				String expKey = "NEXTWAVE_CONTACT_TIME_EXP_" + programId + "_" + scheduledEventId;

				if (Commons.expMap.containsKey(expKey)) {
					Object result = this.evaluateExpression(expKey, thisWaveObject, false, scheduledEventId,
							customerState);
					contactTime = Long.parseLong(result.toString());
					if (contactTime == -1)
						contactTime = SiftMath.getCurrentTime();
					else
						contactLaterFlag = true;

					Commons.logger.info( Commons.logPrefix + "Next wave schedule before processing contact window: " + contactTime);
				}

				if (triggerInNextContactWindow) {
					contactTime = customerState.siftMath
							.getNextContactTime(eventLoc.get("REQUESTER_CHANNEL").getAsString(), contactTime);
					if (contactTime != -1)
						contactLaterFlag = true;
				}

				if (contactLaterFlag) {
					cal.setTime(new java.util.Date(contactTime));
					int nMin = cal.get(Calendar.MINUTE);
					cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE),
							cal.get(Calendar.HOUR_OF_DAY), nMin, 0);
					contactTime = cal.getTimeInMillis();

					String key = contactTime + "-" + commonsState.nWorkerId + "-NW";

					String jVal = (String) Commons.pandaCache.get(key);
					JsonObject jObj = null;
					if (jVal == null)
						jObj = new JsonObject();
					else
						jObj = (JsonObject) Commons.parser.parse(jVal);

					jObj.addProperty(customerState.customerId, scheduledEventId + "," + behaviorEventId);
					Commons.pandaCache.set(key, 0, jObj.toString());
					Commons.logger.info( Commons.logPrefix + "Created a new schedule for next wave offer: " + key + " ' " + jObj.toString());
				} else {
					customerState.tupleMap.addProperty("REQUESTER_ZONE", eventLoc.get("REQUESTER_ZONE").getAsString());
					customerState.tupleMap.addProperty("REQUESTER_CHANNEL", eventLoc.get("REQUESTER_CHANNEL").getAsString());
					customerState.tupleMap.addProperty("REQUESTER_APPLICATION", eventLoc.get("REQUESTER_APPLICATION").getAsString());
					customerState.tupleMap.addProperty("REQUESTER_LOCATION", eventLoc.get("REQUESTER_LOCATION").getAsString());

					this.processPrograms(thisEventDef, scheduledEventId, aTag, customerState);
					this.processAllActions(customerState);
				}
			}

			saveSubscriberIndicators(customerState, serviceType, subscriber);
		}
	}

	public void saveSubscriberIndicators(CustomerState customerState, String serviceType, String subscriber) {
		if (customerState.subscriberIndicators != null) {
			JsonObject supData = Commons.supportingData.get("NON_ACTIVITY_FEEDS");
			if (supData != null) {
				String nonActivityFeeds = supData.get("feeds").getAsString();
				if (!nonActivityFeeds.contains(serviceType)
						|| commonsState.subscriberIndicatorsMap.containsKey(subscriber)) {
					Commons.logger.info( "Record Type is an activity or already in the map. Adding into internal cache : " + subscriber + " - " + serviceType);
					commonsState.subscriberIndicatorsMap.put(subscriber, customerState.subscriberIndicators);
				} else {
					Commons.logger.info( "Record Type is a NON-Activity and not in the map. SKIPPING the addition into internal cache : " + subscriber + " - " + serviceType);
				}
			} else {
				Commons.logger.info( "NON_ACTIVITY_FEEDS supporting Data Missing. " + "Adding into internal cache : " + subscriber + " - " + serviceType);
				commonsState.subscriberIndicatorsMap.put(subscriber, customerState.subscriberIndicators);
			}
			Commons.pandaCache.set(subscriber + "-Indicators", 0, customerState.subscriberIndicators.toString());
		}
	}
	private JsonObject processFulfillmentProductsMessages( JsonObject thisAction, String programId, String offerId, JsonObject thisEventDef, String actualTriggeredEvent, JsonObject thisTagListItem, long timestamp,
			ArrayList< String > applicableChannels, String eventId, CustomerState customerState, String flowId ) throws Exception {
		JsonArray constructedMessages = constructCommunicationMessages( thisAction, thisAction.get( "fulfillmentMessages" ).getAsJsonArray(), "FULFILLMENT_MESSAGE_", offerId, thisTagListItem, false, "fulfillment", applicableChannels,
				eventId, customerState, programId, flowId );

		JsonObject triggerMessage = this.createTriggerMessage( actualTriggeredEvent, programId, offerId, thisEventDef, thisTagListItem, timestamp, thisAction, customerState );

		triggerMessage.add( "FULFILLMENT_MESSAGES", constructedMessages.get( 0 ) );
		if( constructedMessages.size() == 2 ) {
			triggerMessage.add( "OFFER_PAYLOAD", constructedMessages.get( 1 ) );
		}
		JsonArray fulfillmentProducts = thisAction.get( "fulfillmentProducts" ).getAsJsonArray();
		JsonArray resultProductArray = new JsonArray();
		boolean productAttached = false;
		for( int i = 0; i < fulfillmentProducts.size(); i++ ) {
			JsonObject aProduct = fulfillmentProducts.get( i ).getAsJsonObject();
			String productId = aProduct.get( "productId" ).getAsString();
			if( aProduct.get( "productCriteria" ) != null && !aProduct.get( "productCriteria" ).isJsonNull() && aProduct.get( "productCriteria" ).getAsString().trim().length() > 0 ) {
				Commons.logger.info( Commons.logPrefix + "Executing Product Criteria : " + aProduct.get( "productCriteria" ).getAsString() );
				String key = "FULFILLMENT_PRODUCT_CRITERIA_" + offerId + "_" + productId + "_" + i;
				Object result = evaluateExpression( key, thisTagListItem, false, eventId, customerState );
				if( result.toString().equalsIgnoreCase( "false" ) )
					continue;
			}
			productAttached = true;
			resultProductArray.add( aProduct );
			JsonArray dynamicParamsArray = aProduct.get( "dynamicParameters" ).getAsJsonArray();
			for( int j = 0; j < dynamicParamsArray.size(); j++ ) {
				JsonObject aDynamicParam = dynamicParamsArray.get( j ).getAsJsonObject();
				String dynExpressionId = "FULFILLMENT_PRODUCT_" + offerId + "_" + productId + "_" + i + "_" + aDynamicParam.get( "key" ).getAsString();
				Object result = evaluateExpression( dynExpressionId, thisTagListItem, false, eventId, customerState );
				aDynamicParam.addProperty( "resolvedValue", result.toString() );
			}
		}
		triggerMessage.addProperty( "FULFILLMENT_PRODUCT_PRESENT", productAttached );
		triggerMessage.add( "FULFILLMENT_PRODUCTS", resultProductArray );
		return triggerMessage;
	}

	private JsonArray constructCommunicationMessages( JsonObject thisAction, JsonArray messageInputs, String prefix, String offerId, JsonObject thisTagListItem, boolean isCondOfferNotification, String actionType,
			ArrayList< String > applicableChannels, String eventId, CustomerState customerState, String programId, String flowId ) throws Exception {
		String strPrefLang = "";

		if( actionType.equals( "fulfillment" ) && !thisAction.get( "offerType" ).getAsString().equals( "UnConditional" ) ) {
			JsonElement eOfferHist = customerState.subscriberIndicators.get( "OFFER_HISTORY_LifeTime-L" );
			if( eOfferHist != null ) {
				JsonObject offerHistObj = eOfferHist.getAsJsonObject();
				if( offerHistObj.has( flowId ) ) {
					JsonElement ePrefLang = offerHistObj.get( flowId ).getAsJsonObject().get( "CONTACT_LANG" );
					if( ePrefLang != null && !ePrefLang.equals( "" ) )
						strPrefLang = ePrefLang.getAsString();
				}
			}
		}else {
			JsonElement ePrefLang = customerState.subscriberIndicators.get( "LANG_PREF_LifeTime-L" );
			if( ePrefLang != null )
				strPrefLang = ePrefLang.getAsString();
		}

		if( strPrefLang.equals( "" ) ) {
			if( Commons.siftConfig.has( "defaultPreferredLanguage" ) )
				strPrefLang = Commons.siftConfig.get( "defaultPreferredLanguage" ).getAsString();
			else
				strPrefLang = "en_US";
		}

		JsonArray messageOutput = new JsonArray();
		JsonArray allApplicableChannelMessages = new JsonArray();
		for( int i = 0; i < messageInputs.size(); i++ ) {
			JsonObject aChannelMessage = ( JsonObject ) messageInputs.get( i );
			JsonObject outputChannelMessage = new JsonObject();
			String channelName = aChannelMessage.get( "channelName" ).getAsString();
			outputChannelMessage.addProperty( "channelName", channelName );
			outputChannelMessage.add( "contactWindows", aChannelMessage.get( "contactWindows" ) );
			outputChannelMessage.add( "shortCode", aChannelMessage.get( "shortCode" ) );
			outputChannelMessage.add( "optInShortCode", thisAction.get( "optinShortCode" ) );
			if( applicableChannels != null && applicableChannels.contains( channelName ) )
				customerState.tupleMap.addProperty( "CHANNEL", channelName );
			// optinKeyword change
			JsonElement jele = thisAction.get( "optinKeyword" );
			if( jele != null && !jele.isJsonNull() && jele.getAsString().trim().length() > 0 ) {
				String optInKeyword = jele.getAsString();
				String expressionId = "OFFER_OPTINKEYWORD_" + programId + "_" + offerId + "_" + optInKeyword.replace( " ", "_" ).replace( "\"", "" );
				Object result = evaluateExpression( expressionId, thisTagListItem, isCondOfferNotification, eventId, customerState );
				if( result != null ) {
					Commons.logger.info( Commons.logPrefix + "Processed Value for OptInKeyWord : " + expressionId + " = " + result.toString() );
					outputChannelMessage.addProperty( "optInKeyWord", result.toString() );
				}else
					outputChannelMessage.add( "optInKeyWord", jele );
			}
			outputChannelMessage.add( "optInChannel", thisAction.get( "optinChannel" ) );
			JsonArray langMessages = aChannelMessage.get( "messages" ).getAsJsonArray();
			JsonArray outputLangMessages = new JsonArray();
			for( int j = 0; j < langMessages.size(); j++ ) {
				JsonObject aLangMessage = ( JsonObject ) langMessages.get( j );
				String strLang = aLangMessage.get( "lang" ).getAsString();
				if( strLang.equals( strPrefLang ) || strLang.equals( "en_US" ) || strLang.equals( "NA" ) ) {
					JsonObject aLangMessageOutput = new JsonObject();
					aLangMessageOutput.addProperty( "lang", strLang );
					aLangMessageOutput.addProperty( "contentId", aLangMessage.get( "contentId" ).getAsString() );
					String expressionId = prefix + offerId + "_" + channelName + "_" + strLang;
					Object result = evaluateExpression( expressionId, thisTagListItem, isCondOfferNotification, eventId, customerState );
					aLangMessageOutput.addProperty( "textTobeSent", result.toString() );
					if( prefix.equals( "FULFILLMENT_MESSAGE_" ) ) {
						JsonElement fulfilConfMessage = thisAction.get( "fulfillmentConfirmationMessageChannels" );
						if( fulfilConfMessage != null && !fulfilConfMessage.isJsonNull() && fulfilConfMessage.getAsJsonArray().size() > 0 ) {
							expressionId = "FULFILLMENT_CONFIRMATION_MESSAGE_" + offerId + "_" + channelName + "_" + strLang;
							result = evaluateExpression( expressionId, thisTagListItem, isCondOfferNotification, eventId, customerState );
							if( result == null )
								System.out.println( "----------------- Expression result is null: " + expressionId );
							aLangMessageOutput.addProperty( "reConfirmationText", result.toString() );
						}
					}

					if( strLang.equals( strPrefLang ) ) {
						if( outputLangMessages.size() == 1 )
							outputLangMessages.set( 0, aLangMessageOutput );
						else
							outputLangMessages.add( aLangMessageOutput );

						break;
					}
					outputLangMessages.add( aLangMessageOutput );
				}
			}

			outputChannelMessage.add( "messages", outputLangMessages );
			allApplicableChannelMessages.add( outputChannelMessage );
		}

		messageOutput.add( allApplicableChannelMessages );
		JsonObject jReportParams = new JsonObject();

		//		if( ! ( actionType.equals( "fulfillment" ) || actionType.equals( "monitoring" ) ) ) {
		//			messageOutput.add( jReportParams );
		//			return messageOutput;
		//		}
		JsonObject thisOffer = Commons.allOfferDef.get( offerId );
		if( thisOffer != null && thisOffer.get( "reportParameter" ) != null ) {
			JsonObject reportParam = thisOffer.get( "reportParameter" ).getAsJsonObject();
			JsonElement eMonitoredValue = reportParam.get( "monitoredValue" );
			if( eMonitoredValue != null && eMonitoredValue.getAsJsonObject().entrySet().size() > 0 ) {
				String expressionKey = "REPORT_PARAM_" + offerId + "_MONITORED_VALUE";
				Object result = this.evaluateExpression( expressionKey, thisTagListItem, isCondOfferNotification, eventId, customerState );
				jReportParams.addProperty( "monitoredValue", Double.parseDouble( result.toString() ) );
			}
			if( !actionType.equals( "monitoring" ) ) {
				JsonElement efulfillmentValue = reportParam.get( "fulfillmentValue" );
				if( efulfillmentValue != null && efulfillmentValue.getAsJsonObject().entrySet().size() > 0 ) {
					String expressionKey = "REPORT_PARAM_" + offerId + "_FULFILLMENT_VALUE";
					Object result = this.evaluateExpression( expressionKey, thisTagListItem, isCondOfferNotification, eventId, customerState );
					jReportParams.addProperty( "fulfillmentValue", Double.parseDouble( result.toString() ) );
				}
				JsonElement eRewardValueInDollarUnits = reportParam.get( "rewardValueInDollarUnits" );
				if( eRewardValueInDollarUnits != null && eRewardValueInDollarUnits.getAsJsonObject().entrySet().size() > 0 ) {
					String expressionKey = "REPORT_PARAM_" + offerId + "_REWARD_VALUE_CURRENCY";
					Object result = this.evaluateExpression( expressionKey, thisTagListItem, isCondOfferNotification, eventId, customerState );
					jReportParams.addProperty( "rewardValueInDollarUnits", Double.parseDouble( result.toString() ) );
				}
				JsonElement eRewardValueInActualUnits = reportParam.get( "rewardValueInActualUnits" );
				if( eRewardValueInActualUnits != null && eRewardValueInActualUnits.getAsJsonObject().entrySet().size() > 0 ) {
					String expressionKey = "REPORT_PARAM_" + offerId + "_REWARD_VALUE_ACTUAL";
					Object result = this.evaluateExpression( expressionKey, thisTagListItem, isCondOfferNotification, eventId, customerState );
					jReportParams.addProperty( "rewardValueInActualUnits", Double.parseDouble( result.toString() ) );
				}
				JsonElement eActivityChannel = reportParam.get( "activityChannel" );
				if( eActivityChannel != null && eActivityChannel.getAsJsonObject().entrySet().size() > 0 ) {
					String expressionKey = "REPORT_PARAM_" + offerId + "_ACTIVITY_CHANNEL";
					Object result = this.evaluateExpression( expressionKey, thisTagListItem, isCondOfferNotification, eventId, customerState );
					jReportParams.addProperty( "activityChannel", result.toString() );
				}
			}
		}

		// handle Dynamic Report Parameters
		customerState.tupleMap.addProperty( "PROGRAM_ID", programId );
		customerState.tupleMap.addProperty( "OFFER_ID", offerId );
		if( thisOffer != null && thisOffer.get( "offerReportParameters" ) != null ) {
			JsonArray reportParameterArray = thisOffer.get( "offerReportParameters" ).getAsJsonArray();
			for( int i = 0; i < reportParameterArray.size(); i++ ) {
				JsonObject reportParamObj = reportParameterArray.get( i ).getAsJsonObject();
				String thisParamId = reportParamObj.get( "key" ).getAsString();
				String expressionKey = "REPORT_PARAM_" + offerId + "_" + thisParamId;
				Object result = this.evaluateExpression( expressionKey, thisTagListItem, isCondOfferNotification, eventId, customerState );
				jReportParams.addProperty( thisParamId, result.toString() );
				jReportParams.addProperty( thisParamId + "_type", reportParamObj.get( "type" ).getAsString() );
			}
		}
		messageOutput.add( jReportParams );
		return messageOutput;
	}

	private void constructProgramHistory( CustomerState customerState, long timestamp, String programId, String offerId, String actionType, String flowId, 
			String eventId, boolean isSimulated, boolean isControl, JsonObject thisAction ) {
		Commons.logger.info( Commons.logPrefix + "Adding Program History for : " + customerState.customerId + " - " + programId + " - " + offerId + " - " + " - " + actionType + " - " + flowId );
		JsonElement thisProgramHistory;
		JsonArray thisProgramHistoryArray = null;

		thisProgramHistory = customerState.subscriberPrograms.get( programId );
		if( thisProgramHistory != null ) {
			thisProgramHistoryArray = thisProgramHistory.getAsJsonArray();
		}else {
			thisProgramHistoryArray = new JsonArray();
			customerState.subscriberPrograms.add( programId, thisProgramHistoryArray );
		}

		// get the latest program Instance
		JsonObject jProgramInstance = null;
		if( thisProgramHistoryArray.size() == 0 ) {
			jProgramInstance = new JsonObject();
			thisProgramHistoryArray.add( jProgramInstance );
		}else {
			jProgramInstance = thisProgramHistoryArray.get( thisProgramHistoryArray.size() - 1 ).getAsJsonObject();
		}

		JsonElement thisOfferHistory = jProgramInstance.get( offerId );
		JsonArray thisOfferHistoryArray = null;
		JsonObject latestOfferHistory = null;
		if( thisOfferHistory == null ) {
			thisOfferHistoryArray = new JsonArray();
			jProgramInstance.add( offerId, thisOfferHistoryArray );
			latestOfferHistory = new JsonObject();
			thisOfferHistoryArray.add( latestOfferHistory );
			latestOfferHistory.addProperty( "flowId", flowId );
		}else {
			thisOfferHistoryArray = thisOfferHistory.getAsJsonArray();
			latestOfferHistory = thisOfferHistoryArray.get( thisOfferHistoryArray.size() - 1 ).getAsJsonObject();
			if( !latestOfferHistory.get( "flowId" ).getAsString().equals( flowId ) ) {
				latestOfferHistory = new JsonObject();
				thisOfferHistoryArray.add( latestOfferHistory );
				latestOfferHistory.addProperty( "flowId", flowId );
			}

		}
		// If its reminder, handle it differently.
		if( !actionType.equals( "reminderAction" ) && ! actionType.equals( "hybridReminderAction" ) ) {
			latestOfferHistory.addProperty( actionType, timestamp );
			if( isControl == true )
				latestOfferHistory.addProperty( "isControl", isControl );
		}else {

			// reminderAction : { EventId1 : [ timestamp1, timestamp2 ],
			// EventId2 : [ timestamp1, timestamp2 ]
			// }
			JsonElement jeReminderActionHistory = latestOfferHistory.get( actionType );
			JsonObject jObjReminderActionHistory = null;
			if( jeReminderActionHistory == null ) {
				jObjReminderActionHistory = new JsonObject();
				latestOfferHistory.add( actionType, jObjReminderActionHistory );
			}else
				jObjReminderActionHistory = jeReminderActionHistory.getAsJsonObject();
			JsonElement thisEventhistory = jObjReminderActionHistory.get( eventId );
			if( thisEventhistory == null ) {
				JsonArray arr = new JsonArray();
				arr.add( new JsonPrimitive( timestamp ) );
				jObjReminderActionHistory.add( eventId, arr );
			}else {
				JsonArray arr = thisEventhistory.getAsJsonArray();
				arr.add( new JsonPrimitive( timestamp ) );
			}
		}
	}

	protected JsonArray constructEventPayload( JsonObject thisEventDef, String eventId, JsonObject thisTagListItem, CustomerState customerState )
			throws ParseException, NoSuchMethodException, InstantiationException, InvocationTargetException, IllegalAccessException {
		// PAYLOAD
		String strPayload = null;
		JsonElement jPayload = thisEventDef.get( "eventPayload" );
		if( jPayload != null && !jPayload.isJsonNull() )
			strPayload = jPayload.getAsString();
		JsonArray arrPayload = new JsonArray();

		if( strPayload == null && Commons.siftConfig.has( "eventPayload" ) )
			strPayload = Commons.siftConfig.get( "eventPayload" ).getAsString();

		if( strPayload != null ) {
			String[ ] attributes = strPayload.split( "," );
			for( int i = 0; i < attributes.length; i++ ) {
				String thisAttrib = attributes[ i ];
				if( thisAttrib.trim().length() == 0 )
					continue;
				JsonObject thisEntry = new JsonObject();
				if( Commons.allCIM.containsKey( thisAttrib ) ) {
					JsonObject thisCIM = Commons.allCIM.get( thisAttrib );
					if( thisCIM != null && thisCIM.get( "alias" ) != null && thisCIM.get( "alias" ).getAsString().trim().length() > 0 )
						thisAttrib = thisCIM.get( "alias" ).getAsString();

					String cimType = thisCIM.get( "type" ).getAsString();
					thisEntry.addProperty( "name", thisAttrib );
					thisEntry.addProperty( "dataType", convertDataType( cimType ) );

					if( customerState.tupleMap != null && customerState.tupleMap.has( attributes[ i ] ) ) {
						thisEntry.addProperty( "value", customerState.tupleMap.get( attributes[ i ] ).getAsString() );
					}else {
						thisEntry.addProperty( "value", getDefaultPayloadValue( cimType ) );
					}
					arrPayload.add( thisEntry );
				}else {
					JsonObject thisInd = Commons.allIndDef.get( thisAttrib );
					if( thisInd == null ) {
						if( thisEventDef.get( thisAttrib ) != null && !thisEventDef.get( thisAttrib ).isJsonNull() ) {
							thisEntry.addProperty( "name", thisAttrib );
							thisEntry.addProperty( "value", thisEventDef.get( thisAttrib ).getAsString() );
							thisEntry.addProperty( "dataType", "String" );
							arrPayload.add( thisEntry );
						}
						continue;
					}
					String key = generatekey( eventId, thisAttrib, thisInd.get( "frequency" ).getAsString(), customerState.thisRecordDate, customerState.cal, thisTagListItem );
					String frequency = thisInd.get( "frequency" ).getAsString();
					if( frequency.equals( "TriggerTime" ) || ( frequency.equals( "Session" ) && !customerState.processedInd.contains( thisAttrib ) ) ) {
						Object value = evaluateExpression( thisAttrib, thisTagListItem, false, eventId, customerState );
						addToIndicator( customerState, key, value, Commons.expMap.get( thisAttrib ).getOptionalExpressionType(), false, frequency );
						customerState.processedInd.add( thisAttrib );
					}
					JsonElement indVal = ( JsonElement ) getValueFromIndicator( thisAttrib, customerState, eventId, thisTagListItem );
					JsonElement alias = thisInd.get( "alternateNames" );
					if( alias != null && alias.getAsString().length() > 0 )
						thisAttrib = alias.getAsString();
					if( indVal != null ) {
						thisEntry.addProperty( "name", thisAttrib );
						thisEntry.addProperty( "value", indVal.isJsonObject() ? indVal.toString() : indVal.getAsString() );
						thisEntry.addProperty( "dataType", convertDataType( thisInd.get( "expressionType" ).getAsString() ) );
						arrPayload.add( thisEntry );
					}else { // DEF_PAYLOAD The indicator is not found
						thisEntry.addProperty( "name", thisAttrib );
						String dataType = thisInd.get( "expressionType" ).getAsString();
						thisEntry.addProperty( "value", getDefaultPayloadValue( dataType ) );
						thisEntry.addProperty( "dataType", convertDataType( dataType ) );
						arrPayload.add( thisEntry );
					}
				}
			}
		}
		return arrPayload;
	}

	private String convertDataType( String dataType ) {
		if( dataType.contains( "String" ) )
			return "String";
		if( dataType.contains( "double" ) )
			return "Numeric";
		if( dataType.contains( "int" ) )
			return "Numeric";
		if( dataType.contains( "long" ) )
			return "Numeric";
		if( dataType.contains( "JsonObject" ) )
			return "JsonObject";
		return "null";
	}

	private String getDefaultPayloadValue( String dataType ) { // DEF_PAYLOAD
		if( dataType.contains( "String" ) )
			return "-99";
		if( dataType.contains( "double" ) )
			return "0";
		if( dataType.contains( "int" ) )
			return "0";
		if( dataType.contains( "long" ) )
			return "0";
		if( dataType.contains( "JsonObject" ) )
			return "{}";
		return "null";
	}

	protected void processIndicatorsFullMode( String[ ] dimensions, JsonObject tuple, java.util.Date callDate, Calendar cal, JsonObject subscriberIndicators ) throws ParseException {
		String serviceType = tuple.get( "RECORD_TYPE" ).getAsString();
		String[ ] frequency = { "Daily", "Weekly", "Monthly", "Yearly", "LifeTime" };
		String[ ] measure = { "Count", "Spend", "MoU" };
		double[ ] lFactors = null;
		if( serviceType.equals( "VOICE" ) )
			lFactors = new double[] { 1, tuple.get( "CALL_CHARGE" ).getAsFloat(), tuple.get( "CALL_DURATION" ).getAsFloat() };
		else if( serviceType.equals( "SMS" ) ) {
			measure = new String[] { "Count", "Spend" };
			lFactors = new double[] { 1, tuple.get( "CALL_CHARGE" ).getAsFloat() };
		}else if( serviceType.equals( "DATA" ) ) {
			measure = new String[] { "Spend", "Volume" };
			lFactors = new double[] { tuple.get( "CALL_CHARGE" ).getAsFloat(), tuple.get( "VOLUME" ).getAsDouble() };
		}else if( serviceType.equals( "CREDIT" ) ) {
			measure = new String[] { "Count", "Value" };
			lFactors = new double[] { 1, tuple.get( "TRANSACTION_VALUE" ).getAsDouble() };
		}else {
			return;
		}

		for( int i = 0; i < frequency.length; i++ ) {
			for( int j = 0; j < measure.length; j++ ) {
				for( int k = 0; k < dimensions.length; k++ ) {
					if( subscriberIndicators != null )
						computeIndicatorFullMode( callDate, cal, subscriberIndicators, serviceType + "_" + dimensions[ k ] + "_" + measure[ j ], frequency[ i ], lFactors[ j ] );
					if( frequency[ i ].equals( "Weekly" ) || frequency[ i ].equals( "Monthly" ) ) {
						if( subscriberIndicators != null ) {
							computeIndicatorMaxFullMode( callDate, cal, subscriberIndicators, serviceType + "_" + dimensions[ k ] + "_" + measure[ j ], frequency[ i ] );
							computeIndicatorMeanFullMode( callDate, cal, subscriberIndicators, serviceType + "_" + dimensions[ k ] + "_" + measure[ j ], frequency[ i ] );
						}
					}
				}
			}
		}
	}

	protected void computeIndicatorMeanFullMode( Date callDate, Calendar cal, JsonObject indicatorInstance, String indicatorPrefix, String frequency ) throws ParseException {
		String key = this.generatekey( null, indicatorPrefix, frequency, callDate, cal, null );
		JsonElement element = indicatorInstance.get( key );
		Float lValue = ( float ) 0;
		if( element != null )
			lValue = ( float ) element.getAsDouble();

		if( frequency.equals( "Weekly" ) )
			java.util.Calendar.getInstance().get( java.util.Calendar.DAY_OF_WEEK );
		else
			java.util.Calendar.getInstance().get( java.util.Calendar.DAY_OF_MONTH );

		float lResult = lValue / java.util.Calendar.getInstance().get( java.util.Calendar.DAY_OF_MONTH );
		key = this.generatekey( null, indicatorPrefix + "_Mean", frequency, callDate, cal, null );
		indicatorInstance.addProperty( key, lResult );
	}

	protected void computeIndicatorMaxFullMode( Date callDate, Calendar cal, JsonObject indicatorInstance, String indicatorPrefix, String frequency ) throws ParseException {
		String key = this.generatekey( null, indicatorPrefix, "Daily", callDate, cal, null );
		JsonElement element = indicatorInstance.get( key );
		Float lValueDaily = ( float ) 0;
		if( element != null )
			lValueDaily = ( float ) element.getAsDouble();

		key = this.generatekey( null, indicatorPrefix + "_Max", frequency, callDate, cal, null );
		element = indicatorInstance.get( key );
		Float lValue = ( float ) 0;
		if( element != null )
			lValue = ( float ) element.getAsDouble();
		if( lValueDaily > lValue )
			indicatorInstance.addProperty( key, lValueDaily );
	}

	protected double computeIndicatorFullMode( java.util.Date callDate, Calendar cal, JsonObject indicatorInstance, String indicatorPrefix, String frequency, double lFactor ) throws ParseException {
		String key = this.generatekey( null, indicatorPrefix + "_" + frequency, frequency, callDate, cal, null );
		JsonElement element = indicatorInstance.get( key );
		double lValue = ( double ) 0;
		if( element != null )
			lValue = element.getAsDouble();
		double result = lValue + lFactor;
		indicatorInstance.addProperty( key, result );
		return result;
	}

	protected String generatekey( String eventId, String key, String frequency, java.util.Date callDate, Calendar cal, JsonObject aTag ) throws ParseException {
		if( frequency == null )
			frequency = "Yearly";
		String temporalId = null;
		long thisYear = cal.get( Calendar.YEAR ) - 2000;

		Long numParam;

		if( frequency.equals( "Daily" ) ) {
			numParam = new Long( cal.get( Calendar.DAY_OF_YEAR ) );
			temporalId = numParam.toString() + "D" + thisYear;
		}else if( frequency.equals( "Weekly" ) ) {
			numParam = new Long( cal.get( Calendar.WEEK_OF_YEAR ) );
			temporalId = numParam.toString() + "W" + thisYear;
		}else if( frequency.equals( "Monthly" ) ) {
			numParam = new Long( cal.get( Calendar.MONTH ) );
			temporalId = numParam.toString() + "M" + thisYear;
		}else if( frequency.equals( "Yearly" ) ) {
			numParam = thisYear;
			temporalId = thisYear + "Y";
		}else if( frequency.equals( "CustomTime" ) ) {
			if( aTag != null ) {
				numParam = aTag.get( "MONITORING_START_TIME" ).getAsLong();
				temporalId = aTag.get( "FLOW_ID" ).getAsString();
			}else
				temporalId = eventId;
		}else if( frequency.equals( "TriggerTime" ) ) {
			temporalId = eventId;
		}else if( frequency.equals( "Hourly" ) ) {
			numParam = new Long( cal.get( Calendar.HOUR_OF_DAY ) );
			temporalId = numParam + "H";
		}else if( frequency.equals( "LifeTimeHourly" ) ) {
			numParam = new Long( cal.get( Calendar.HOUR_OF_DAY ) );
			temporalId = numParam + "H";
		}else if( frequency.equals( "Session" ) )
			temporalId = "S";

		if( frequency.equals( "LifeTime" ) )
			key += "-L";
		else
			key += "-" + temporalId;
		return key;
	}


	protected void logPerformanceStatistics() {
		// dump the performance metrics here
		// Commons.lastTupleTimestamp = new java.util.Date().getTime();
		/*
		 * Commons.logger.info( Commons.logPrefix +
		 * "*** Total number of Records Processed : " + numTuples + " ***" );
		 * Commons.logger.info( Commons.logPrefix +
		 * "*** Total number of Records Processed : " + numTuples + " ***" );
		 * Commons.logger.info( Commons.logPrefix +
		 * "*** Total Duration for Indicators : " + totalDurationIndicators +
		 * " ***" ); Commons.logger.info( Commons.logPrefix +
		 * "*** Average Duration for Indicators : " + totalDurationIndicators /
		 * numTuples + " ***" ); //Commons.logger.log(
		 * java.util.logging.Level.INFO, Commons.logPrefix +
		 * "*** Total Duration for Persist Access : " + totalDurationPersist +
		 * " ***" ); Commons.logger.info( Commons.logPrefix +
		 * "*** Average Duration for Persist Access : " + totalDurationPersist /
		 * numTuples + " ***" );
		 * 
		 * Commons.logger.info( Commons.logPrefix +
		 * "*** First Record Arrival Timestamp : " + firstTupleTimestamp +
		 * " ***" ); Commons.logger.info( Commons.logPrefix +
		 * "*** Last Record Arrival Timestamp : " + lastTupleTimestamp + " ***"
		 * ); Commons.logger.info( Commons.logPrefix +
		 * "*** Elapsed Duration : " + ( lastTupleTimestamp -
		 * firstTupleTimestamp ) + " ms  ***" );
		 * 
		 * Commons.logger.info( Commons.logPrefix +
		 * "*** First Record Arrival Timestamp : " + firstTupleTimestamp +
		 * " ***" ); Commons.logger.info( Commons.logPrefix +
		 * "*** Last Record Arrival Timestamp : " + lastTupleTimestamp + " ***"
		 * ); Commons.logger.info( Commons.logPrefix +
		 * "*** Elapsed Duration : " + ( lastTupleTimestamp -
		 * firstTupleTimestamp ) + " ms  ***" );
		 * 
		 * System.out.println(
		 * "\n\n----------------------Indicator Computation Duration Logs---------\n\n"
		 * );
		 * 
		 * Set< Entry< Long, Integer >> entries =
		 * indicatorPerformanceMap.entrySet(); Iterator< Entry< Long, Integer >>
		 * itr = entries.iterator(); while( itr.hasNext() ) { Entry< Long,
		 * Integer > entry = itr.next(); System.out.println( "Range : " +
		 * entry.getKey() + " --- Number of Records : " + entry.getValue() ); }
		 * 
		 * System.out.println(
		 * "\n\n--------------------Persist Access Duration Logs----------------\n"
		 * );
		 * 
		 * entries = persistPerformanceMap.entrySet(); itr = entries.iterator();
		 * while( itr.hasNext() ) { Entry< Long, Integer > entry = itr.next();
		 * System.out.println( "Range : " + entry.getKey() +
		 * " --- Number of Records : " + entry.getValue() ); }
		 */
		// System.out.println( "Synchronised Time Taken for : " +
		// Commons.numTuples + " = " + ( Commons.lastTupleTimestamp -
		// Commons.firstTupleTimestamp ) + " - ms" );
	}

	protected Object getValueFromIndicator( String indicator, CustomerState customerState, String eventId, JsonObject aTag ) throws ParseException {
		if( Commons.allIndDef.containsKey( indicator ) ) {
			String frequency = Commons.allIndDef.get( indicator ).get( "frequency" ).getAsString();
			String keyInInd = generatekey( eventId, indicator, frequency, customerState.thisRecordDate, customerState.cal, aTag );
			if( customerState.subscriberIndicators.has( keyInInd ) )
				return customerState.subscriberIndicators.get( keyInInd );
			else if( customerState.sessionIndicators.has( keyInInd ) )
				return customerState.sessionIndicators.get( keyInInd );
		}else { // if( Commons.fixedOfferParams.containsKey( indicator ) ) {
			JsonElement programParam = customerState.subscriberIndicators.get( "PROGRAM_PARAMETERS-L" );
			if( programParam != null && programParam.getAsJsonObject().has( indicator ) ) {
				return programParam.getAsJsonObject().get( indicator );
			}
		}
		return null;
	}

	protected Object evaluateExpression( String expressionId, JsonObject aTag, boolean isCondOfferNotification, String eventId, CustomerState customerState )
			throws NoSuchMethodException, InstantiationException, InvocationTargetException, IllegalAccessException, ParseException {

		PExpressionEvaluator exp = Commons.expMap.get( expressionId );
		if( exp == null ) {
			System.out.println( "COMPILED EXPRESSION NOT AVAILABLE : " + expressionId );
			//Commons.logger.warn( "COMPILED EXPRESSION NOT AVAILABLE : " + expressionId );
			return null;
		}

		Commons.logger.info( Commons.logPrefix + "Processing Expression : " + expressionId ); // +

		String[ ] paramNames = exp.getParameterNames();
		Class[ ] paramTypes = exp.getParameterTypes();
		Object[ ] parameterValues = new Object[ paramNames.length ];
		Commons.logger.info( Commons.logPrefix + "Parameter List " + expressionId + " : " + Arrays.toString( paramNames ) + " : " + Arrays.toString( paramTypes ) );
		for( int j = 0; j < paramNames.length; j++ ) {
			String thisParam = paramNames[ j ];
			Object jValue = null;

			if( Commons.allCIM.containsKey( thisParam ) ) {
				if( customerState.tupleMap != null && customerState.tupleMap.has( thisParam ) )
					jValue = new JsonPrimitive( customerState.tupleMap.get( thisParam ).getAsString() );
			}else if( thisParam.equals( "SIFTMATH" ) )
				jValue = customerState.siftMath;
			else if( Commons.modelMasterTable.containsKey( thisParam ) )
				jValue = Commons.modelMasterTable.get( thisParam );
			else if( thisParam.startsWith( "siftvar" ) ) {
				jValue = aTag.get( thisParam );
			}else {
				if( thisParam.equals( expressionId ) || customerState.processedInd.contains( thisParam ) || ( Commons.fixedOfferParams.containsKey( thisParam ) && !isCondOfferNotification ) )
					jValue = getValueFromIndicator( thisParam, customerState, eventId, aTag );
				else {
					jValue = evaluateExpression( thisParam, aTag, isCondOfferNotification, eventId, customerState );
					customerState.processedInd.add( thisParam );
					if( Commons.allIndDef.containsKey( thisParam ) ) {
						String frequency = Commons.allIndDef.get( thisParam ).get( "frequency" ).getAsString();
						String keyInInd = generatekey( eventId, thisParam, frequency, customerState.thisRecordDate, customerState.cal, aTag );
						addToIndicator( customerState, keyInInd, jValue, Commons.expMap.get( thisParam ).getOptionalExpressionType(), false, frequency );
					}else { // if( Commons.fixedOfferParams.containsKey(
						// thisParam ) ) {
						addToIndicator( customerState, thisParam, jValue, Commons.expMap.get( thisParam ).getOptionalExpressionType(), true, "" );
					}
					Commons.logger.info( Commons.logPrefix + "Processed Parameter : " + thisParam + ". Value : " + jValue.toString() );
				}
			}
			parameterValues[ j ] = ExpressionUtil.createObject( paramTypes[ j ], jValue );
		}
		Commons.logger.info( Commons.logPrefix + "Bound Values for " + expressionId + " : " + Arrays.toString( paramNames ) + " = " + Arrays.toString( parameterValues ) );
		Object expressionValue = null;
		try {
			Commons.logger.info( Commons.logPrefix + "Result of Evaluation " + expressionId + " : " + ( ( expressionValue != null ) ? expressionValue.toString() : "null" ) );
			expressionValue = exp.evaluate( parameterValues );
		}
		catch( Exception ex ) {
			ex.printStackTrace();
			System.out.println( "Exception found in the expression: " + expressionId + " : " + customerState.tupleMap.toString() );
			throw new NullPointerException( ex.getMessage() );
		}
		Commons.logger.info( Commons.logPrefix + "Result of Evaluation " + expressionId + " : " + ( ( expressionValue != null ) ? expressionValue.toString() : "null" ) );
		return expressionValue;
	}

	protected void triggerEvent( JsonObject thisEventDef, JsonObject ownerEvents, String mainEventId, String dependantEventId, JsonObject thisTagListItem, boolean isMonitored, String triggerType, CustomerState customerState,
			String targetSystemId ) throws Exception {

		// control the number of triggers
		// EVENT_TRIGGER_LifeTime-L": {
		// "DTAC_REVENUE_STRETCH_TARGET_DETECTED": {
		// DTAC_REVENUE_STRETCH_TARGET_DETECTED : [ [ 1428268802781,
		// "66813044911-DTAC_REVENUE_STRETCH_TARGET_DETECTED-1428268802781" ] ],
		// DTAC_REVENUE_STRETCH_TARGET_DETECTED_SCHEDULED_REMINDER : [ [
		// 1428268802781,
		// "66813044911-DTAC_REVENUE_STRETCH_TARGET_DETECTED-1428268802781" ] ],
		// DTAC_REVENUE_STRETCH_TARGET_DETECTED_PARTIAL_REMINDER : [ [
		// 1428268802781,
		// "66813044911-DTAC_REVENUE_STRETCH_TARGET_DETECTED-1428268802781" ] ],
		// }
		Commons.logger.info( Commons.logPrefix + "---- Inside Triggering Event --- " + customerState.customerId + " = MainEvent : " + mainEventId + " - DepEvent : " + dependantEventId + " - TriggerType : " + triggerType );
		String actualTriggeredEvent = mainEventId;
		if( !triggerType.equals( "FE" ) )
			actualTriggeredEvent = dependantEventId;

		JsonElement eAllTriggerHistory = ownerEvents.get( mainEventId );
		JsonObject mainEventTriggerHistory = null;
		if( eAllTriggerHistory == null )
			mainEventTriggerHistory = new JsonObject();
		else
			mainEventTriggerHistory = ( JsonObject ) eAllTriggerHistory;
		JsonElement eThisTriggerHistory = null;

		eThisTriggerHistory = mainEventTriggerHistory.get( actualTriggeredEvent );
		JsonArray thisTriggerHistory = null;
		if( eThisTriggerHistory == null ) // create for the first time.
			thisTriggerHistory = new JsonArray();
		else
			thisTriggerHistory = eThisTriggerHistory.getAsJsonArray();

		int numTriggersSoFar = thisTriggerHistory.size();

		// if its a non-monitored event, check for the max trigger limits.
		// MAXTRIGGER_LIMIT
		/*
		 * if( ! isMonitored ) { JsonElement jMaxTriggerLimit =
		 * thisEventDef.get( "maxTriggerLimit"); int nMaxTriggerLimit = (
		 * jMaxTriggerLimit != null ) ? jMaxTriggerLimit.getAsInt() : -1; if(
		 * nMaxTriggerLimit != -1 && numTriggersSoFar >= ( nMaxTriggerLimit /
		 * numberOfInstances ) ) return; }
		 */

		if( numTriggersSoFar > 0 ) {
			// get the corresponding recurring counts and limit the trigger.
			// ACTIVITY_TRIGGERED_REMINDER_COUNT, REPEAT_RECURRING_COUNT
			int nRecurringLimit = -1;
			if( isMonitored ) {
				numTriggersSoFar = 0;
				String monFlowId = thisTagListItem.get( "FLOW_ID" ).getAsString();
				// //Commons.logger.log(
				// java.util.logging.Level.INFO, Commons.logPrefix +"Number of instances in trigger
				// : "
				// + thisTriggerHistory.size() + "monFlowTimestamp : " +
				// monFlowTimestamp );
				for( int i = 0; i < thisTriggerHistory.size(); i++ ) {
					JsonArray arr = thisTriggerHistory.get( i ).getAsJsonArray();
					String thisFlowId = arr.get( 1 ).getAsString();
					// //Commons.logger.log(
					// java.util.logging.Level.INFO, Commons.logPrefix +"Timestamp in this trigger :
					// "
					// + timestamp );
					if( thisFlowId.equals( monFlowId ) ) {
						// //Commons.logger.log(
						// java.util.logging.Level.INFO, Commons.logPrefix +"Match Found : " +
						// timestamp );
						numTriggersSoFar++;
					}
				}

				if( triggerType.equals( "IRE" ) )
					nRecurringLimit = thisTagListItem.get( "ACTIVITY_TRIGGERED_REMINDER_COUNT" ).getAsInt();
				else
					nRecurringLimit = thisTagListItem.get( "REPEAT_RECURRING_COUNT" ).getAsInt();
			}else {
				if( triggerType.equals( "FE" ) )
					nRecurringLimit = thisEventDef.get( "recurringCount" ).getAsInt();
			}
			Commons.logger.info( Commons.logPrefix + "Recurring limit Check : " + nRecurringLimit + " / " + numTriggersSoFar );
			if( nRecurringLimit != -1 && ( numTriggersSoFar - 1 ) >= nRecurringLimit )
				return;
		}

		/***********************
		 * Maintain Trigger count for Event Live Chart
		 **************************/
		Date today = new java.util.Date();
		long ltoday = today.getTime();
		ltoday += java.util.TimeZone.getDefault().getOffset( ltoday );
		long triggerTime = ltoday; // 22JUL
		String flowId = null;
		boolean isControl = false;
		boolean isSummaryTrigger = false;
		if( thisEventDef.has( "type" ) && thisEventDef.get( "type" ).getAsString().equals( "Summary" ) )
			isSummaryTrigger = true;

		if( !isMonitored ) {
			flowId = customerState.customerId + "-" + mainEventId + "-" + triggerTime;
			// flowId = customerState.customerId + Math.round(Math.random() *
			// 100000);
		}else { // monitored event
			flowId = thisTagListItem.get( "FLOW_ID" ).getAsString();
		}

		long totalCountForThisEvent = 0;
		if( triggerType.equals( "FE" ) ) {
			JsonObject triggerCount = commonsState.eventTriggerCountsMap.get( mainEventId );
			if( triggerCount == null ) {
				String strTriggerCount = ( String ) Commons.pandaCache.get( "TELCO_" + mainEventId + "-" + commonsState.nWorkerId + "-TriggerCounts" );
				if( strTriggerCount == null ) {
					triggerCount = new JsonObject();
					triggerCount.addProperty( "docType", "EventTriggerCount" );
					triggerCount.addProperty( "count", new Long( 0 ) );
					// triggerCount.addProperty( "count_control", new Long( 0 )
					// );
				}else {
					triggerCount = ( JsonObject ) Commons.parser.parse( strTriggerCount );
				}
				commonsState.eventTriggerCountsMap.put( mainEventId, triggerCount );
			}
			// synchronized( triggerCount ) {
			totalCountForThisEvent = triggerCount.get( "count" ).getAsLong() + 1;
			triggerCount.addProperty( "count", totalCountForThisEvent );

			if( isSummaryTrigger ) {
				isControl = false;
			}else if( !isMonitored ) {
				isControl = ( ( totalCountForThisEvent % Commons.supportingData.get( "CONTROL_GROUP_MODULUS" ).get( "VALUE" ).getAsInt() ) == 0 ) ? true : false;
			}else { // monitored event
				isControl = thisTagListItem.get( "IS_CONTROL" ).getAsBoolean();
			}
			// if( isControl )
			// triggerCount.addProperty( "count_control", triggerCount.get(
			// "count_control" ).getAsLong() + 1 );

			/***********************
			 * Maintain Hourly/Daily Trigger Count for MeasurementLive Chart
			 **************************/
			JsonElement eDailyCount = triggerCount.get( "DailyCount" );
			JsonArray dailyCountArray = null;
			if( eDailyCount == null )
				dailyCountArray = new JsonArray();
			else
				dailyCountArray = eDailyCount.getAsJsonArray();
			JsonArray dailyEntry = null;

			// long ltoday = today.getTime();
			// Commons.logger.info( Commons.logPrefix +
			// "------------Adding the live chart trigger count for the
			// day---------- : "
			// + ltoday );
			ltoday = ltoday - ltoday % SiftMath.msecPerDay;
			// Commons.logger.info( Commons.logPrefix +
			// "------------Adding the live chart trigger count for the day
			// after reducing milliseconds ---------- : "
			// + ltoday );
			long count = 1;
			if( dailyCountArray.size() > 0 ) {
				dailyEntry = dailyCountArray.get( dailyCountArray.size() - 1 ).getAsJsonArray();
				long dateInArray = dailyEntry.get( 0 ).getAsLong();
				if( dateInArray == ltoday ) {
					count = dailyEntry.get( 1 ).getAsLong() + 1;
					dailyCountArray.remove( dailyCountArray.size() - 1 );
				}
			}
			dailyEntry = new JsonArray();
			dailyEntry.add( new JsonPrimitive( ltoday ) );
			dailyEntry.add( new JsonPrimitive( count ) );
			dailyCountArray.add( dailyEntry );
			triggerCount.add( "DailyCount", dailyCountArray );

			// Hourly Counts
			// Commons.logger.info( Commons.logPrefix +
			// "------------Adding the live chart trigger count for the Hour -
			// using key ---------- : HourlyCount-"
			// + ltoday );
			JsonElement eHourlyCount = triggerCount.get( "HourlyCount-" + ltoday );
			JsonArray hourlyCountArray = null;
			if( eHourlyCount == null )
				hourlyCountArray = new JsonArray();
			else
				hourlyCountArray = eHourlyCount.getAsJsonArray();
			JsonArray hourlyEntry = null;
			Calendar thisCal = Calendar.getInstance();
			thisCal.setTime( today );
			long thisHour = thisCal.get( Calendar.HOUR_OF_DAY );
			count = 1;
			if( hourlyCountArray.size() > 0 ) {
				hourlyEntry = hourlyCountArray.get( hourlyCountArray.size() - 1 ).getAsJsonArray();
				long hourInArray = hourlyEntry.get( 0 ).getAsLong();
				if( hourInArray == thisHour ) {
					count = hourlyEntry.get( 1 ).getAsLong() + 1;
					hourlyCountArray.remove( hourlyCountArray.size() - 1 );
				}
			}
			hourlyEntry = new JsonArray();
			hourlyEntry.add( new JsonPrimitive( thisHour ) );
			hourlyEntry.add( new JsonPrimitive( count ) );
			hourlyCountArray.add( hourlyEntry );
			triggerCount.add( "HourlyCount-" + ltoday, hourlyCountArray );
			Commons.pandaCache.set( "TELCO_" + mainEventId + "-" + commonsState.nWorkerId + "-TriggerCounts", 0, triggerCount.toString() );
			Commons.logger.info( Commons.logPrefix + "Telco Trigger Counts : " + triggerCount.toString() );
			/***********************
			 * Maintain Hourly/Daily Trigger Count for MeasurementLive Chart
			 **************************/
			// }
		}

		/***********************
		 * Store the trigger history in the indicator instance
		 ****************/
		if( !isSummaryTrigger ) {
			JsonArray thisTrigger = new JsonArray();
			thisTrigger.add( new JsonPrimitive( triggerTime ) );
			thisTrigger.add( new JsonPrimitive( flowId ) );
			thisTriggerHistory.add( thisTrigger );
			mainEventTriggerHistory.add( actualTriggeredEvent, thisTriggerHistory );
			ownerEvents.add( mainEventId, mainEventTriggerHistory );
		}

		/*********************** Send output tuple **************************/
		SimpleDateFormat dformat = new SimpleDateFormat( "yyyyMMddHHmmssSSS" );
		JsonObject triggerMessage = new JsonObject();
		triggerMessage.addProperty( "MSISDN", customerState.customerId );
		String subid = null;
		if( isMonitored ) {
			JsonElement esubid = thisTagListItem.get( "SUBSCRIBER_ID" );
			subid = esubid.getAsString();
		}else {
			JsonElement ele = customerState.subscriberIndicators.get( "SUBSCRIBER_ID_LifeTime-L" );
			if( ele != null )
				subid = ele.getAsString();
		}

		triggerMessage.addProperty( "SUBSCRIBER_ID", subid );
		triggerMessage.addProperty( "EVENT_ID", mainEventId );
		if( !triggerType.equals( "FE" ) )
			triggerMessage.addProperty( "TRIGGEREDACTIVITY", dependantEventId );
		triggerMessage.addProperty( "EVENT_TIMESTAMP", dformat.format( today ) );
		if( triggerType.equals( "FE" ) && numTriggersSoFar > 0 ) {
			triggerType = "RECURRING";
			if( isMonitored && thisTagListItem.get( "REPEAT_EVENT_REQUIRED" ).getAsBoolean() )
				triggerType = "REPEAT";
		}
		triggerMessage.addProperty( "EVENT_TYPE", triggerType );
		triggerMessage.addProperty( "FLOW_ID", flowId );
		triggerMessage.addProperty( "IS_CONTROL", isControl );
		triggerMessage.addProperty( "IS_MONITORED", isMonitored );
		if( customerState.tupleMap != null && customerState.tupleMap.has( "USSD_SHORT_CODE" ) )
			triggerMessage.addProperty( "USSD_SHORT_CODE", customerState.tupleMap.get( "USSD_SHORT_CODE" ).getAsString() );

		JsonArray payload = constructEventPayload( thisEventDef, mainEventId, thisTagListItem, customerState );
		Commons.logger.info( Commons.logPrefix + "Event Payload : " + payload.toString() );
		triggerMessage.add( "EVENT_PAYLOAD", payload );

		// Event Prioritisation 21Apr2015
		if( !isMonitored && ( triggerType.equals( "FE" ) || triggerType.equals( "REPEAT" ) || triggerType.equals( "RECURRING" ) ) ) {
			// put them into an array and send it one shot
			String[ ] targetSystemIDs = thisEventDef.get( "targetSystemID" ).getAsString().split( "\\|" );
			for( int i = 0; i < targetSystemIDs.length; i++ ) {
				JsonArray triggerArr = null;
				if( customerState.triggerSinkMap.containsKey( targetSystemIDs[ i ] ) ) {
					triggerArr = customerState.triggerSinkMap.get( targetSystemIDs[ i ] );
					triggerArr.add( triggerMessage );
				}else {
					triggerArr = new JsonArray();
					triggerArr.add( triggerMessage );
				}
				customerState.triggerSinkMap.put( targetSystemIDs[ i ], triggerArr );
			}
		}else {
			/***********************
			 * Send output tuple
			 **************************/

			Commons.logger.info( Commons.logPrefix + "Event Trigger Tuple - SinkType : " + targetSystemId );
			outputCollector.emit( "record", new Values( targetSystemId, triggerMessage.toString() ) );

		}
	}

	protected boolean isDailyTriggerLimitExceeded( String eventId, JsonObject eventDef ) {
		Commons.logger.info( Commons.logPrefix + "Checking if the Max Trigger Limit exceeded for the event." );

		/*
		 * String key = "TELCO_" + eventId + "-" + commonsState.nWorkerId +
		 * "-TriggerCounts"; JsonObject jOfferCount = summaryIndicatorsMap.get(
		 * key );
		 * 
		 * if( jOfferCount == null ) { String strOfferCount = ( String )
		 * Commons.pandaCache.get( key ); if( strOfferCount != null ) {
		 * jOfferCount = ( JsonObject )Commons.parser.parse( strOfferCount ); }
		 * }
		 * 
		 * if( jOfferCount != null ) { JsonArray targetCountsDaily =
		 * jOfferCount.get( "DailyCount" ).getAsJsonArray();
		 * 
		 * long today = SiftMath.getCurrentTime(); today = today - today %
		 * SiftMath.msecPerDay; String dayKey = new Long( today ).toString();
		 * JsonArray eCount = targetCountsDaily.get( targetCountsDaily.size() -
		 * 1 ).getAsJsonArray(); if( eCount != null ) { int todayCount =
		 * Integer.parseInt( eCount.get( 1 ).getAsString() ); if( eCount.get( 0
		 * ).toString().equals( dayKey ) ) { JsonElement maxTriggerCountEle =
		 * eventDef.get( "maxTriggerLimit" ); if( maxTriggerCountEle != null ) {
		 * int maxTriggerCount = maxTriggerCountEle.getAsInt();
		 * Commons.logger.info( Commons.logPrefix + "for Event Id " +
		 * eventId + " targetCountTillNowForTheDay :" + todayCount +
		 * " For The Day :" + dayKey + " maxTriggerCount :" + ( maxTriggerCount
		 * / Commons.numberOfInstances ) ); if( maxTriggerCount == -1 ) return
		 * false; int compareValue = maxTriggerCount %
		 * Commons.numberOfInstances;
		 * 
		 * if( commonsState.nWorkerId <= compareValue ) compareValue = (
		 * maxTriggerCount / Commons.numberOfInstances ) + 1; else compareValue
		 * = ( maxTriggerCount / Commons.numberOfInstances ); if( compareValue
		 * <= todayCount ) { Commons.logger.info( Commons.logPrefix +
		 * "Max Trigger Count Across Subscriber Exceeded  : for Event Id " +
		 * eventId ); return true; } } } } }
		 */
		return false;
	}

	public void manageFlowFunnelCounts( String triggerId, String programId, String offerId, String regionPassed, CustomerState customerState ) {
		if( ! Commons.flowFunnelFlag )
			return;

		// synchronized( this.getClass() ) {
		int numberOfEntriesToKeep = 30;
		JsonObject thisProgram = null;
		Commons.logger.info( Commons.logPrefix + "Processing manageFlowFunnelCounts---------- triggerId: " + triggerId + "   offerId: " + offerId + "   programId: " + programId + "   region: " + regionPassed );
		if( commonsState.flowFunnelMap.containsKey( programId ) )
			thisProgram = commonsState.flowFunnelMap.get( programId ).getAsJsonObject();
		else {
			Object tmpDoc = Commons.pandaCache.get( "FLOWFUNNEL_" + customerState.tupleMap.get( "REQUESTER_CHANNEL" ).getAsString() + "_" + programId + "-" + commonsState.nWorkerId );
			if( tmpDoc == null )
				thisProgram = new JsonObject();
			else
				thisProgram = ( JsonObject ) Commons.parser.parse( tmpDoc.toString() );
		}

		JsonObject thisTrigger = null;
		if( thisProgram.has( triggerId ) )
			thisTrigger = thisProgram.get( triggerId ).getAsJsonObject();
		else
			thisTrigger = new JsonObject();

		long currentDay = customerState.siftMath.getCurrentDay();
		String currentDayString = Long.toString( currentDay );

		JsonObject thisDay = null;
		if( thisTrigger.has( currentDayString ) )
			thisDay = thisTrigger.get( currentDayString ).getAsJsonObject();
		else {
			thisDay = new JsonObject();
			thisDay.add( "count", new JsonPrimitive( 0 ) );

			if( thisProgram.has( "keys" ) ) {
				JsonArray keysArrAfterAdd = customerState.siftMath.insertIntoKeyArray( thisProgram.get( "keys" ).getAsJsonArray(), currentDay );

				if( keysArrAfterAdd.size() > numberOfEntriesToKeep ) {
					String keyRemoved = Long.toString( keysArrAfterAdd.remove( 0 ).getAsLong() );
					thisTrigger.remove( keyRemoved );
				}
				thisProgram.add( "keys", keysArrAfterAdd );
			}else
				thisProgram.add( "keys", customerState.siftMath.insertIntoKeyArray( new JsonArray(), currentDay ) );
		}

		if( regionPassed.equals( "TRIGGER_CRITERIA" ) ) {
			thisDay.addProperty( "count", thisDay.get( "count" ).getAsLong() + 1 );
			thisTrigger.add( currentDayString, thisDay );
			thisProgram.add( triggerId, thisTrigger );
			commonsState.flowFunnelMap.put( programId, thisProgram );

			return;
		}

		JsonArray thisOffer = null;
		if( thisDay.has( offerId ) )
			thisOffer = thisDay.get( offerId ).getAsJsonArray();
		else {
			thisOffer = new JsonArray();
			for( int i = 0; i < 7; i++ )
				thisOffer.add( new JsonPrimitive( 0 ) );
		}

		if( regionPassed.equals( "OFFER_SEGMENTATION" ) )
			thisOffer.set( 0, new JsonPrimitive( thisOffer.get( 0 ).getAsLong() + 1 ) );
		if( regionPassed.equals( "GLOBAL_MAX_CONTACT_PER_SUBS" ) || regionPassed.equals( "GLOBAL_CONTACT_POLICY_SKIPPED" ) )
			thisOffer.set( 1, new JsonPrimitive( thisOffer.get( 1 ).getAsLong() + 1 ) );
		if( regionPassed.equals( "GLOBAL_MAX_CONTACT_ACROSS_SUBS" ) || regionPassed.equals( "GLOBAL_CONTACT_POLICY_SKIPPED" ) )
			thisOffer.set( 2, new JsonPrimitive( thisOffer.get( 2 ).getAsLong() + 1 ) );
		if( regionPassed.equals( "GLOBAL_COOLDOWN" ) || regionPassed.equals( "GLOBAL_CONTACT_POLICY_SKIPPED" ) )
			thisOffer.set( 3, new JsonPrimitive( thisOffer.get( 3 ).getAsLong() + 1 ) );
		if( regionPassed.equals( "OFFER_MAX_CONTACT_PER_SUBS" ) || regionPassed.equals( "OFFER_CONTACT_POLICY_SKIPPED" ) )
			thisOffer.set( 4, new JsonPrimitive( thisOffer.get( 4 ).getAsLong() + 1 ) );
		if( regionPassed.equals( "OFFER_MAX_CONTACT_ACROSS_SUBS" ) || regionPassed.equals( "OFFER_CONTACT_POLICY_SKIPPED" ) )
			thisOffer.set( 5, new JsonPrimitive( thisOffer.get( 5 ).getAsLong() + 1 ) );
		if( regionPassed.equals( "OFFER_COOLDOWN" ) || regionPassed.equals( "OFFER_CONTACT_POLICY_SKIPPED" ) )
			thisOffer.set( 6, new JsonPrimitive( thisOffer.get( 6 ).getAsLong() + 1 ) );

		thisDay.add( offerId, thisOffer );
		thisTrigger.add( currentDayString, thisDay );
		thisProgram.add( triggerId, thisTrigger );
		commonsState.flowFunnelMap.put( programId, thisProgram );

		// }
	}

	protected static void initialize( String strPersistAddressList ) throws Exception {
		Commons commonsState = new Commons();
		commonsState.nWorkerId = 1;
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );

		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddress.length; i++ ) {
			System.out.println( Commons.logPrefix + Commons.logPrefix + arrPersistAddress[ i ] );
			uris.add( new URI( arrPersistAddress[ i ] ) );
		}
		Commons.siftMode = "";
		Commons.pandaCacheSize = 100;

		// PandaCache cache = new PandaCache( uris );
		// pandaCache = cache.getClient();
		Commons.pandaCache = new PandaCache( uris );
		System.out.println( Commons.logPrefix + "******initialised Pandacache : " + Commons.pandaCache.toString() );
		Commons.pandaCacheConfig = new PandaCacheConfig( uris );
		System.out.println( Commons.logPrefix + "******initialised PandacacheConfig : " + Commons.pandaCacheConfig.toString() );

		Commons.parser = new JsonParser();

		// Initialise the Sift Start Date
		Commons.supportingData = new SupportingDataDao().getSupportingData();
		Commons.siftConfig = Commons.supportingData.get( "SiftConfig" );
		SiftMath.siftStartDate = Commons.siftConfig.get( "siftStartDate" ).getAsLong();
		Commons.telcoPersistInterval = Commons.siftConfig.get( "telcoPersistInterval" ).getAsInt();
		Commons.numberOfInstances = Commons.siftConfig.get( "numberOfInstances" ).getAsInt(); // MAXTRIGGER_LIMIT
		if( Commons.siftConfig.has( "FlowFunnelFlag" ) )
			Commons.flowFunnelFlag = Commons.siftConfig.get( "FlowFunnelFlag" ).getAsBoolean();
		if( Commons.siftConfig.has( "TelcoIndicatorsRetentionPeriod" ) )
			Commons.telcoIndicatorsRetentionPeriod = Commons.siftConfig.get( "TelcoIndicatorsRetentionPeriod" ).getAsInt();
		if( Commons.siftConfig.has( "EventsToBeProcessed" ) )
			Commons.EventsToBeProcessed = Commons.siftConfig.get( "EventsToBeProcessed" ).getAsString();

		String contactPolicyString = ( String ) Commons.pandaCacheConfig.get( "ContactPolicy" );
		if( contactPolicyString != null )
			Commons.contactPolicyObject = ( JsonObject ) Commons.parser.parse( contactPolicyString );

		IndicatorDefDao indDao = new IndicatorDefDao();
		EventDefDao eveDao = new EventDefDao();
		expDao = new ExpressionDao();
		Commons.allIndDef = ( LinkedHashMap< String, JsonObject > ) indDao.readActive();
		Commons.summaryIndDef = ( LinkedHashMap< String, JsonObject > ) indDao.readActiveSummaryIndicators(); // SUMMARY
		// INDICATOR
		Commons.allEveDef = eveDao.readActive();
		Commons.allFunctions = new FunctionDao().readUserDefined();

		initialiseModelMasterTables();

		Object[ ] resObj = expDao.prepareAllExpressions();
		Commons.expMap = ( HashMap< String, PExpressionEvaluator > ) resObj[ 0 ];
		Commons.fixedOfferParams = ( HashMap< String, String > ) resObj[ 1 ];
		CIMDao cimDao = new CIMDao();
		Commons.allCIM = cimDao.read();

		commonsState.subscriberIndicatorsMap = new LRUPandaCache< String, JsonObject >( Commons.pandaCacheSize, 0.75f );

		// Merge the events that are tagged for all
		// Removing the dependency on the EventsTagList-All. Construct by
		// iterating through.
		Commons.eventsTaggedforAll = new JsonObject();
		Iterator< Entry< String, JsonObject > > itr = Commons.allEveDef.entrySet().iterator();
		while( itr.hasNext() ) {
			Entry< String, JsonObject > elemEntry = itr.next();
			JsonObject eveDef = elemEntry.getValue();
			if( eveDef.get( "taggedForAll" ).getAsString().equals( "true" ) ) {
				JsonObject thisTag = new JsonObject();
				thisTag.addProperty( "startTimestamp", new java.util.Date().getTime() );
				thisTag.addProperty( "endTimestamp", new java.util.Date().getTime() );
				Commons.eventsTaggedforAll.add( elemEntry.getKey(), thisTag );
			}
		}
		commonsState.eventsTaggedForSubscribersMap = new LRUPandaCache< String, JsonObject >( Commons.pandaCacheSize / 2, 0.75f );
		commonsState.eventTriggerCountsMap = new HashMap< String, JsonObject >();
		commonsState.eventMonitoringCountsMap = new HashMap< String, JsonObject >();
		commonsState.summaryIndicatorsMap = new HashMap< String, JsonObject >();
		commonsState.flowFunnelMap = new HashMap< String, JsonObject >();

		ProgramsDAO programsDAO = new ProgramsDAO();
		Commons.allProgramDef = programsDAO.readActive();
		OfferDAO offerDao = new OfferDAO();
		Commons.allOfferDef = offerDao.read();

		Commons.customersForLock = new LRUPandaCache< String, String >( Commons.pandaCacheSize, 0.75f );
	}


	public static void main( String arg[] ) throws Exception {
		PandamaticOperator thisOp = new PandamaticOperator();
		thisOp.initialize( "http://127.0.0.1:8091/pools" );

		JsonObject tupleMap = new JsonObject();

		long lCallDate = new java.util.Date().getTime();
		Date thisRecordDate = new java.util.Date( lCallDate );
		Calendar cal = Calendar.getInstance();
		cal.setTime( thisRecordDate );

		String msisdn = "66123123123";
		String recordType = "VOICE";
		tupleMap.addProperty( "RECORD_TYPE", recordType );
		tupleMap.addProperty( "MSISDN", msisdn );
		tupleMap.addProperty( "CUSTOMER_ID", msisdn );
		tupleMap.addProperty( "REQUESTER_APPLICATION", "DTAC_APP" );
		tupleMap.addProperty( "REQUESTER_LOCATION", "DealoftheDay|Recommended|CrossSell|MostBuy" );

		CustomerState customerState = thisOp.getIndicatorsEventsTags( msisdn, tupleMap, thisRecordDate, recordType, cal );
		System.out.println( customerState.siftMath.evaluateRScript( "/StreamsData/siftmeta/rscripts/PP_ROCKET_NBO_R.json" ) );

	}

	@Override
	public void prepare( Map stormConf, TopologyContext context, OutputCollector collector ) {
		outputCollector = collector;
	}

	@Override
	public void execute(Tuple input) {
		// TODO Auto-generated method stub

	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declareStream( "record", new Fields( "SinkType", "message" ) );
		declarer.declareStream( "telcorecord", new Fields( "RECORD_TYPE", "RECORD_TIMESTAMP" ) );
	}
}
