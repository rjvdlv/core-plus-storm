package com.knowesis.sift.core;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;

import com.knowesis.sift.core.sources.BeaconSource;
import com.knowesis.sift.core.utils.SiftUtils;

public class SiftTopology {

	public static void main(String[] args) {
		System.out.println( "Starting Sift test topology now.." );
		TopologyBuilder builder = new TopologyBuilder();
		builder.setSpout( "beaconSpout", new BeaconSource() );
		builder.setBolt( "printBeacon", new TestBolt(), 4 ).allGrouping( "beaconSpout" );

		// Configuration
		Config conf = new Config();
		conf.setDebug( true );
		conf = SiftUtils.loadConfigProperties( conf );
		
		//Topology run
		LocalCluster cluster = null;
		try{
			cluster = new LocalCluster();
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		cluster.submitTopology( "Sift-Topology", conf, builder.createTopology() );
	}

}
