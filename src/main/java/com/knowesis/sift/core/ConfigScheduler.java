package com.knowesis.sift.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.storm.generated.Bolt;
import org.apache.storm.generated.KillOptions;
import org.apache.storm.generated.SpoutSpec;
import org.apache.storm.generated.StormTopology;
import org.apache.storm.generated.TopologySummary;
import org.apache.storm.generated.Nimbus.Client;
import org.apache.storm.scheduler.Cluster;
import org.apache.storm.scheduler.ExecutorDetails;
import org.apache.storm.scheduler.IScheduler;
import org.apache.storm.scheduler.SupervisorDetails;
import org.apache.storm.scheduler.Topologies;
import org.apache.storm.scheduler.TopologyDetails;
import org.apache.storm.scheduler.WorkerSlot;
import org.apache.storm.thrift.TException;
import org.apache.storm.utils.NimbusClient;
import org.apache.storm.utils.Utils;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ConfigScheduler implements IScheduler {

	HashMap< SupervisorDetails, List< ExecutorDetails > > supervisorExecutorsMap = null;
	List< ExecutorDetails > unassignedExecutorsList = null;
	int executorsSize;

	@Override
	public void prepare( Map conf ) {
		System.out.println( "Scheduling with Config Scheduler.." );
		executorsSize = 15;
	}

	@Override
	public void schedule( Topologies topologies, Cluster cluster ) {
		System.out.println( "Scheduling the config placements.." );
		Collection< TopologyDetails > topologyDetails = topologies.getTopologies();
		Collection< SupervisorDetails > supervisorDetails = cluster.getSupervisors().values();
		Map< String, SupervisorDetails > taggedSupervisors = new HashMap< String, SupervisorDetails >();
		List< SupervisorDetails > untaggedSupervisors = new ArrayList< SupervisorDetails >();
		System.out.println( "Looping for supervisors...." );
		for( SupervisorDetails thisSupervisor : supervisorDetails ) {
			Map< String, Object > metadata = ( Map< String, Object > ) thisSupervisor.getSchedulerMeta();
			System.out.println( "supervisor: " + thisSupervisor.getHost() );
			System.out.println( "Supervisor metadata: " + metadata.toString() );
			if( metadata.get( "hostname" ) != null ) {
				taggedSupervisors.put( ( String ) metadata.get( "hostname" ), thisSupervisor );
				System.out.println( thisSupervisor.getId() + "--- has configPlacement details for hostname: " + ( String ) metadata.get( "hostname" ) );
			}
			else {
				untaggedSupervisors.add( thisSupervisor );
				System.out.println( "Untagged supervisor: " +  thisSupervisor.getId() );
			}
		}

		for( TopologyDetails thisTopologyDetails : topologyDetails ){
			System.out.println("looping topologies..." + thisTopologyDetails.getName());
			supervisorExecutorsMap = new HashMap< SupervisorDetails, List< ExecutorDetails > >();
			unassignedExecutorsList = new ArrayList< ExecutorDetails >();
			StormTopology topology = thisTopologyDetails.getTopology();
			System.out.println( "Got untill topologies get having schduling..." + thisTopologyDetails.getId() );
			Map<String, Bolt> bolts = topology.get_bolts();
			Map<String, SpoutSpec> spouts = topology.get_spouts();

			System.out.println( "Bolts: " + bolts.toString() );
			System.out.println( "Spouts: " + spouts.toString() );
			JsonParser parser = new JsonParser();
			System.out.println("Supervisors details: " + taggedSupervisors.toString());
			try{
				boolean addFlag = true;
				for( String name : bolts.keySet() ) {
					Bolt bolt = bolts.get( name );
					System.out.println( "this bolt: " + name );
					JsonObject boltConfig = ( JsonObject ) parser.parse( bolt.get_common().get_json_conf() );
					if( boltConfig.get( "configPlacement" ) != null && ! boltConfig.get( "configPlacement" ).isJsonNull() ) {
						addFlag = addExecutorsToMap( boltConfig, cluster, taggedSupervisors, thisTopologyDetails, name );
						if( ! addFlag ) {
							System.out.println( "Bolt placement failed: " + name );
							break;
						}
					}
					else
						addExecutorToUnassignedList( cluster, name, thisTopologyDetails );
				}
				if( ! addFlag ) {
					stopTopology( thisTopologyDetails );
					continue;
				}
				for( String name : spouts.keySet() ) {
					SpoutSpec spout = spouts.get( name );
					System.out.println( "this spout: " + name );
					JsonObject spoutConfig = ( JsonObject ) parser.parse( spout.get_common().get_json_conf() );
					if( spoutConfig.get( "configPlacement" ) != null && ! spoutConfig.get( "configPlacement" ).isJsonNull() ) {
						addFlag = addExecutorsToMap( spoutConfig, cluster, taggedSupervisors, thisTopologyDetails, name );
						if( ! addFlag ) {
							System.out.println( "Spout placement failed: " + name );
							break;
						}
					}
					else
						addExecutorToUnassignedList( cluster, name, thisTopologyDetails );
				}
				if( ! addFlag ) {
					stopTopology( thisTopologyDetails );
					continue;
				}

				System.out.println( "Assigning the supervisors with tasks.." );
				assignExecutorsToSupervisorSlots( cluster, thisTopologyDetails );
				
				System.out.println( "Assigning the unassigned executors to the supervisors.." );
				assignUnassignedExecutorsToSupervisors( cluster, thisTopologyDetails, taggedSupervisors );
			}catch( Exception pe){
				pe.printStackTrace();
			}
		}
	}

	public boolean addExecutorsToMap( JsonObject thisConfig, Cluster cluster, Map< String, SupervisorDetails > taggedSupervisors, 
			TopologyDetails thisTopologyDetails, String componentName ) {
		System.out.println( "This component conf: " + thisConfig.toString() );

		String thisComponentPlacementConfig = thisConfig.get( "configPlacement" ).getAsString();
		String[] allGroupsConfig = thisComponentPlacementConfig.split( "," );
		List< ExecutorDetails > executors = cluster.getNeedsSchedulingComponentToExecutors( thisTopologyDetails ).get( componentName );

		int index = 0;
		for( int i = 0; i < allGroupsConfig.length; i ++ ) {
			if( executors != null && executors.size() != 0 ) {
				System.out.println( "Total number of executors to be assigned: " + executors.size() );
				
				System.out.println( "Current allocation: " + allGroupsConfig[ i ] );
				String[] thisGroupConfig = allGroupsConfig[ i ].split( ":" );
				if( taggedSupervisors.get( thisGroupConfig[ 0 ] ) != null ){
					String gid = thisGroupConfig[ 0 ];
					SupervisorDetails supervisor = taggedSupervisors.get( gid );
					int numOfInstances = Integer.parseInt( thisGroupConfig[ 1 ] );
					for( int j = 0; j < numOfInstances; j ++ ) {
						ExecutorDetails thisExecutorDetails = executors.get( index );
						index ++;
						System.out.println( "This executor details: " + thisExecutorDetails.toString() );
						List< ExecutorDetails > currentList = null;
						if( supervisorExecutorsMap.containsKey( supervisor ) )
							currentList = supervisorExecutorsMap.get( supervisor );
						else
							currentList = new ArrayList< ExecutorDetails >();

						currentList.add( thisExecutorDetails );
						supervisorExecutorsMap.put( supervisor, currentList );
						System.out.println( "Added the executor to the supervisor: " + gid );
					}
				}
				else
					return false;
			}
		}
		
		return true;
	}
	
	public void assignUnassignedExecutorsToSupervisors( Cluster cluster, TopologyDetails thisTopologyDetails, Map< String, SupervisorDetails > taggedSupervisors ) {
		
		System.out.println( "Total unassigned executors in the Topology: " + unassignedExecutorsList.size() );
		List< ExecutorDetails > thisExecutorList = new ArrayList< ExecutorDetails >();
		for( int i = 0; i < unassignedExecutorsList.size(); i ++ ) {
			
			thisExecutorList.add( unassignedExecutorsList.get( i ) );
			if( ( i + 1 ) % executorsSize == 0 || ( i + 1 ) == unassignedExecutorsList.size() ) {
				SupervisorDetails thisSupervisorDetails = findBestSupervisorFromCluster( taggedSupervisors, cluster );
				
				List< WorkerSlot > availableSlots = cluster.getAvailableSlots( thisSupervisorDetails );
				System.out.println( "Available slots size for component: " + availableSlots.size() );

				if( ! availableSlots.isEmpty() && thisExecutorList != null){
					System.out.println( "Assigning the untagged executor to supervisor: " + thisSupervisorDetails.getHost() + "*** " + ( i + 1 ) );

					cluster.assign( availableSlots.get( 0 ), thisTopologyDetails.getId(), thisExecutorList );
				}
				else
					System.out.println("No place to schedule this..");

				thisExecutorList = new ArrayList< ExecutorDetails >();
			}
		}
	}
	
	public SupervisorDetails findBestSupervisorFromCluster( Map< String, SupervisorDetails > taggedSupervisors, Cluster cluster ) {
		
		SupervisorDetails nextBestSupervisor = null;
		int maxAvailablePorts = 0;
		Iterator< Entry< String, SupervisorDetails > > iterator = taggedSupervisors.entrySet().iterator();
		while( iterator.hasNext() ) {
			Entry< String, SupervisorDetails > thisEntry = iterator.next();
			SupervisorDetails thisSupervisor = thisEntry.getValue();
			Set< Integer > currentSupervisorAvailblePorts = cluster.getAvailablePorts( thisSupervisor );
			if( currentSupervisorAvailblePorts.size() > maxAvailablePorts ) {
				maxAvailablePorts = currentSupervisorAvailblePorts.size();
				nextBestSupervisor = thisSupervisor;
			}
		}
		
		return nextBestSupervisor;
	}

	public void addExecutorToUnassignedList( Cluster cluster, String componentName, TopologyDetails thisTopologyDetails ) {
		List< ExecutorDetails > executors = cluster.getNeedsSchedulingComponentToExecutors( thisTopologyDetails ).get( componentName );

		if( executors != null && executors.size() != 0 ) {
			System.out.println( "Total number of unassigned executors to be assigned for component: " + componentName + "***" + executors.size() );
			for( int i = 0; i < executors.size(); i ++ ) {
				ExecutorDetails thisExecutorDetails = executors.get( i );
				System.out.println( "This executor details: " + thisExecutorDetails.toString() );
				unassignedExecutorsList.add( thisExecutorDetails );
				System.out.println( "Added the executor to the unassigned list..." );
			}
		}
	}


	public void assignExecutorsToSupervisorSlots( Cluster cluster, TopologyDetails thisTopologyDetails ) {
		Iterator< Entry< SupervisorDetails, List< ExecutorDetails > > > iterator = supervisorExecutorsMap.entrySet().iterator();
		while( iterator.hasNext() ) {
			Entry< SupervisorDetails, List< ExecutorDetails > > thisEntry = iterator.next();
			SupervisorDetails thisSupervisorDetails = thisEntry.getKey();
			List< ExecutorDetails > thisSupervisorExecutorsList = thisEntry.getValue();

			System.out.println( "Assignment started for supervisor: " + thisSupervisorDetails.getHost() + "*** Number of executors: " + thisSupervisorExecutorsList.size() );
			int i = 0;
			List< ExecutorDetails > thisExecutorList = new ArrayList< ExecutorDetails >();
			for( int ind = 0; ind < thisSupervisorExecutorsList.size(); ind ++ ) {
				thisExecutorList.add( thisSupervisorExecutorsList.get( ind ) );
				i ++;

				if( i % executorsSize == 0 || i == thisSupervisorExecutorsList.size() ) {
					List< WorkerSlot > availableSlots = cluster.getAvailableSlots( thisSupervisorDetails );
					System.out.println( "Available slots size for component: " + availableSlots.size() );

					if( ! availableSlots.isEmpty() && thisExecutorList != null){
						System.out.println( "Assigning the executor to gid: " + thisSupervisorDetails.getHost() + "*** " + i );

						cluster.assign( availableSlots.get( 0 ), thisTopologyDetails.getId(), thisExecutorList );
					}
					else
						System.out.println("No place to schedule this..");

					thisExecutorList = new ArrayList< ExecutorDetails >();
				}
			}
		}
	}
	
	public void stopTopology( TopologyDetails topologyDetails ) {
		System.out.println( "Supervisors criteria for topology not met. trying to stop now: " + topologyDetails.getName() );
		Map< String, Object > conf = Utils.readStormConfig();
		Client client = NimbusClient.getConfiguredClient( conf ).getClient();
		KillOptions killOpts = new KillOptions();
		try {
			List< TopologySummary > topologySummaries = client.getClusterInfo().get_topologies();
			for( int i = 0; i < topologySummaries.size(); i ++ ) {
				TopologySummary thisTopologySummary = topologySummaries.get( i );
				System.out.println( "Current topology: " + thisTopologySummary.get_name() );
				if( thisTopologySummary.get_name().equals( topologyDetails.getName() ) ) {
					System.out.println( "Stopping the topology: " + topologyDetails.getName() );
					client.killTopologyWithOpts( topologyDetails.getName(), killOpts );
					break;
				}
			}
		} catch ( TException e ) {
			e.printStackTrace();
		}
	}

}

