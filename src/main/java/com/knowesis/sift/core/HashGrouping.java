package com.knowesis.sift.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.storm.generated.GlobalStreamId;
import org.apache.storm.grouping.CustomStreamGrouping;
import org.apache.storm.task.WorkerTopologyContext;

import com.knowesis.sift.Common.Commons;

public class HashGrouping implements CustomStreamGrouping, Serializable {

	private static final long serialVersionUID = 1L;
	int numberOfTasks = 0;
	private ArrayList<List<Integer>> choices;

	@Override
	public void prepare( WorkerTopologyContext context, GlobalStreamId stream, List< Integer > targetTasks ) {
		numberOfTasks = targetTasks.size();
		System.out.println( "Initialising HashGrouping with number of tasks as: " + numberOfTasks );
		System.out.println( "Global stream id: " + stream.get_streamId() + "****" + stream.get_componentId() );

		choices = new ArrayList<List< Integer > >( targetTasks.size() );
		for ( Integer i: targetTasks ) {
			choices.add( Arrays.asList( i ) );
		}
	}

	@Override
	public List< Integer > chooseTasks( int taskId, List< Object > values ) {
		/*List< Integer > boltIds = new ArrayList< Integer >();
		if( values.size() > 0 ) {
			long subscriberId = Long.parseLong( values.get( 1 ).toString() );
			Commons.logger.info( Commons.logPrefix + "Current subscriber Id in HashGrouping: " + subscriberId );
			long mod = subscriberId % numberOfTasks;
			Commons.logger.info( Commons.logPrefix + "Submitting the task in HashGrouping with taskIndex: " + mod );
			boltIds.add( ( int ) mod );
		}
		return boltIds;*/


		long subscriberId = Long.parseLong( values.get( 1 ).toString() );
		Commons.logger.info( Commons.logPrefix + "Current subscriber Id in HashGrouping: " + subscriberId );
		long mod = subscriberId % numberOfTasks;
		Commons.logger.info( Commons.logPrefix + "Submitting the task in HashGrouping with taskIndex: " + mod );

		return choices.get( ( int ) mod );
	}
}