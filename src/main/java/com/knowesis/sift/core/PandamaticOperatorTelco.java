package com.knowesis.sift.core;

/**
 * @author Raja SP
 *
 */

import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.tuple.Tuple;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.PandaCache;
import com.knowesis.sift.Common.PandaCacheConfig;
import com.knowesis.sift.api.dao.SupportingDataDao;
import com.knowesis.sift.expression.SiftMath;

public class PandamaticOperatorTelco extends PandamaticOperator {

	private JsonObject telcoIndicators = null;
	private static long persistInterval = 0;
	private String strPersistAddressList;
	private String configPlacement = null;

	public String getStrPersistAddressList() {
		return strPersistAddressList;
	}


	public void setStrPersistAddressList( String strPersistAddressList ) {
		this.strPersistAddressList = strPersistAddressList;
	}

	public void setConfig( String configPlacement ) {
		System.out.println( Commons.logPrefix + "Setting the configPlacement in PandamaticOperatorTelco as: " + configPlacement );
		this.configPlacement = configPlacement;
	}
	
	@Override
	 public Map< String, Object > getComponentConfiguration () {

		 Map< String, Object > configuration = super.getComponentConfiguration();
		 if ( configuration == null )
			 configuration = new HashMap< String, Object >();

		 System.out.println( Commons.logPrefix + "Setting the configPlacement: " + configPlacement );
		 configuration.put( "configPlacement", configPlacement );
		 
		 return configuration;
	 }
	
	@Override
	public void prepare( Map stormConf, TopologyContext context, OutputCollector collector ) {

		if( commonsState == null )
			commonsState = new Commons();
		commonsState.nWorkerId = context.getThisTaskIndex() + 1;
		String[ ] arrPersistAddress = strPersistAddressList.split( "," );

		LinkedList< URI > uris = new LinkedList< URI >();
		for( int i = 0; i < arrPersistAddress.length; i++ ) {
			System.out.println( Commons.logPrefix + Commons.logPrefix + arrPersistAddress[ i ] );
			try {
				uris.add( new URI( arrPersistAddress[ i ] ) );
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}

		Commons.pandaCache = new PandaCache( uris );
		Commons.pandaCacheConfig = new PandaCacheConfig( uris );
		System.out.println( Commons.logPrefix + "******initialised Pandacache : " + Commons.pandaCache.toString() );

		Commons.parser = new JsonParser();
		Commons.supportingData = new SupportingDataDao().getSupportingData();

		Commons.siftConfig = Commons.supportingData.get( "SiftConfig" );
		if( Commons.siftConfig.has( "TelcoIndicatorsRetentionPeriod" ) )
			Commons.telcoIndicatorsRetentionPeriod = Commons.siftConfig.get( "TelcoIndicatorsRetentionPeriod" ).getAsInt();
		if( Commons.siftConfig.has( "telcoPersistInterval" ) )
			Commons.telcoPersistInterval = Commons.siftConfig.get( "telcoPersistInterval" ).getAsInt();
		if( Commons.siftConfig.has( "populateTelcoIndicators" ) )
			Commons.populateTelcoIndicators = Commons.siftConfig.get( "populateTelcoIndicators" ).getAsBoolean();
	}


	@Override
	public void execute( Tuple input ) {

		String streamName =  input.getSourceComponent();
		if( streamName.equals( "Sift_Config_Update" ) ) {
			return;
		}

		if ( ! Commons.populateTelcoIndicators )
			return;
		
		if( telcoIndicators == null ) {
			JsonParser parser = new JsonParser();
			String strTelcoIndicators = ( String ) Commons.pandaCache.get( "telco-" + commonsState.nWorkerId + "-Indicators" );
			if( strTelcoIndicators == null ) {
				telcoIndicators = new JsonObject();
				telcoIndicators.addProperty( "docType", "telcoIndicators" );
			}else {
				telcoIndicators = ( JsonObject ) parser.parse( strTelcoIndicators );
			}
		}

		if( commonsState.numTuples == 0 )
			commonsState.firstTupleTimestamp = new java.util.Date().getTime();

		commonsState.numTuples++;
		persistInterval++;

		long lCallDate = input.getLongByField( "RECORD_TIMESTAMP" );
		Date thisRecordDate = new java.util.Date( lCallDate );

		String[ ] allDimensions = { "ALL" };

		try {
			processTelecomIndicators( allDimensions, input, thisRecordDate, telcoIndicators );
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if( persistInterval == Commons.telcoPersistInterval ) {
			Commons.pandaCache.set( "telco-" + commonsState.nWorkerId + "-Indicators", 0, telcoIndicators.toString() );
			persistInterval = 0;
		}

	}


	private void processTelecomIndicators( String[ ] dimensions, Tuple tuple, Date callDate, JsonObject telcoIndicators ) throws ParseException {
		String serviceType = tuple.getStringByField( "RECORD_TYPE" );
		Calendar cal = Calendar.getInstance();
		cal.setTime( callDate );

		String[ ] measure = { "Count" };
		double[ ] lFactors = new double[] { 1 };

		for( int j = 0; j < measure.length; j++ ) {
			for( int k = 0; k < dimensions.length; k++ ) {
				computeTelcoIndicator( callDate, cal, telcoIndicators, serviceType + "_" + dimensions[ k ] + "_"
						+ measure[ j ], lFactors[ j ] );
			}
		}
	}

	protected void computeTelcoIndicator( java.util.Date callDate, Calendar cal, JsonObject indicatorInstance,
			String indicatorPrefix, double lFactor ) throws ParseException {

		/***********************Maintain  count for Event Live Chart **************************/
		JsonObject thisIndicatorInMemory = ( JsonObject ) telcoIndicators.get( indicatorPrefix );
		String pandaKey = indicatorPrefix;
		if( thisIndicatorInMemory == null ) {
			String strIndicatorInPersist = ( String ) Commons.pandaCache.get( indicatorPrefix );
			if( strIndicatorInPersist == null ) {
				thisIndicatorInMemory = new JsonObject();
				//thisIndicatorInMemory.addProperty( "docType", "telcoCount" );
				thisIndicatorInMemory.addProperty( "count", new Double( 0 ) );
			}else {
				thisIndicatorInMemory = ( JsonObject ) Commons.parser.parse( strIndicatorInPersist );
			}
			telcoIndicators.add( pandaKey, thisIndicatorInMemory );
		}
		Double lCount = thisIndicatorInMemory.get( "count" ).getAsDouble() + lFactor;
		thisIndicatorInMemory.addProperty( "count", lCount );
		/***********************Maintain count for Event Live Chart **************************/


		/***********************Maintain Hourly/Daily Trigger Count for MeasurementLive Chart **************************/
		JsonElement eDailyCount = thisIndicatorInMemory.get( "DailyCount" );		
		JsonArray dailyCountArray = null;
		if( eDailyCount == null )
			dailyCountArray = new JsonArray();
		else
			dailyCountArray = eDailyCount.getAsJsonArray();
		JsonArray dailyEntry = null;
		long ltoday = SiftMath.getCurrentTime();
		ltoday = ltoday - ltoday % SiftMath.msecPerDay;
		Double count = lFactor;
		if( dailyCountArray.size() > 0 ) {
			dailyEntry = dailyCountArray.get( dailyCountArray.size() - 1 ).getAsJsonArray();
			long dateInArray = dailyEntry.get( 0  ).getAsLong() ;
			if( dateInArray == ltoday ){
				count = dailyEntry.get( 1 ).getAsDouble() + lFactor;
				dailyCountArray.remove( dailyCountArray.size() - 1  );
			}
		} 
		dailyEntry = new JsonArray();
		dailyEntry.add( new JsonPrimitive( ltoday ) );
		dailyEntry.add( new JsonPrimitive( count ) );
		dailyCountArray.add( dailyEntry );

		if ( dailyCountArray.size() > Commons.telcoIndicatorsRetentionPeriod )
			dailyCountArray.remove( 0 );
		thisIndicatorInMemory.add( "DailyCount", dailyCountArray );

		//Hourly Counts    	
		JsonElement eHourlyCount = thisIndicatorInMemory.get( "HourlyCount-" + ltoday );		
		JsonArray hourlyCountArray = null;
		if( eHourlyCount == null )
			hourlyCountArray = new JsonArray();
		else
			hourlyCountArray = eHourlyCount.getAsJsonArray();
		JsonArray hourlyEntry = null;
		java.util.Date today = new java.util.Date();
		Calendar thisCal = Calendar.getInstance();
		thisCal.setTime( today );
		//thisCal.setTimeZone( TimeZone.getTimeZone( "GMT" ) );
		long thisHour = thisCal.get( Calendar.HOUR_OF_DAY );
		count = lFactor;
		if( hourlyCountArray.size() > 0 ) {
			hourlyEntry = hourlyCountArray.get( hourlyCountArray.size() - 1 ).getAsJsonArray();
			long hourInArray = hourlyEntry.get( 0  ).getAsLong() ;
			if( hourInArray == thisHour ){
				count = hourlyEntry.get( 1 ).getAsDouble() + lFactor;
				hourlyCountArray.remove( hourlyCountArray.size() - 1  );
			}
		} 
		hourlyEntry = new JsonArray();
		hourlyEntry.add( new JsonPrimitive( thisHour ) );
		hourlyEntry.add( new JsonPrimitive( count ) );
		hourlyCountArray.add( hourlyEntry );
		thisIndicatorInMemory.add( "HourlyCount-" + ltoday, hourlyCountArray );	

		long dateToDelete = ltoday - ( Commons.telcoIndicatorsRetentionPeriod + 1 ) * SiftMath.msecPerDay;
		thisIndicatorInMemory.remove( "HourlyCount-" + dateToDelete );
	}

}
