package com.knowesis.sift.core.dynamicDataSource;

import java.net.URISyntaxException;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.PandaCacheConfig;
import com.knowesis.sift.core.sources.MessagingQueueSource;
import com.knowesis.sift.core.utils.SiftUtils;

public class DynamicDSTopology {

	public static void main( String[] args ) throws URISyntaxException {

		String[] persistAddress = args[ 0 ].split( "," );
		String dsName = args[ 1 ];
		String mode = args[ 2 ];
		JsonParser parser = new JsonParser();
		String dsType = null;
		String cimMapping = null;
		int numOfParallelProcesses = 0;
		JsonArray dataSourcePropertiesArray = null;

		PandaCacheConfig pandaCacheConfig = SiftUtils.getPandaCacheConfigClient( persistAddress );
		Object dsDocumentObject = pandaCacheConfig.get( "DATA_SOURCE_" + dsName );
		JsonObject dsDocument = null;
		if( dsDocumentObject == null || dsDocumentObject.toString().length() <= 2 ) {
			System.out.println( "Data source Document not available for: " + dsName );
			System.exit( 0 );
		}else {
			dsDocument = ( JsonObject ) parser.parse( dsDocumentObject.toString() );
			numOfParallelProcesses = dsDocument.get( "numberOfParallelProcess" ).getAsInt(); 
			cimMapping = dsDocument.get( "cimMapping" ).getAsString();
			dataSourcePropertiesArray = dsDocument.get( "dataSourceProperties" ).getAsJsonArray();
		}

		//Configuration
		Config conf = new Config();
		conf.put( "dsType", dsType );
		conf.put( "dsName", dsName );
		conf.setDebug( true );
		conf.put( Config.NIMBUS_HOST, "streamsqse.localdomain" );
		conf.setNumWorkers(1);
		conf.setMaxSpoutPending(5);

		TopologyBuilder DataSourceTopologyBuilder = new TopologyBuilder();

		for( int i = 0; i < dataSourcePropertiesArray.size(); i ++ ) {
			JsonObject thisDS = dataSourcePropertiesArray.get( i ).getAsJsonObject();
			System.out.println("printing DS: " + i + "\n"+thisDS.toString());
			dsType = thisDS.get( "type" ).getAsString();
			if( dsType.equalsIgnoreCase( "JMS" ) ) {
				conf.put( "MESSAGE_SOURCE_QUEUETYPE", thisDS.get( "queueType" ).getAsString() );
				conf.put( "MESSAGE_SOURCE_URL", thisDS.get( "brokerURL" ).getAsString() );
				conf.put( "MESSAGE_SOURCE_QUEUE", thisDS.get( "queueNames" ).getAsString() );
				conf.put( "MESSAGE_SOURCE_MESSAGETYPE", thisDS.get( "messageType" ).getAsString() );
				conf.put( "MESSAGE_SOURCE_USERNAME", thisDS.get( "jmsuserName" ).getAsString() );
				conf.put( "MESSAGE_SOURCE_PWD", thisDS.get( "jmspassword" ).getAsString() );
			
				DataSourceTopologyBuilder.setSpout( "Spout_" + dsType + "_ " + i, new MessagingQueueSource() );
			}
		}


		LocalCluster cluster = null;
		try{
			cluster = new LocalCluster();
		}
		catch( Exception e ) {
			e.printStackTrace();
		}

		cluster.submitTopology( "DS-Topology", conf, DataSourceTopologyBuilder.createTopology() );
	}

}
