package com.knowesis.sift.core;

import java.util.Comparator;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class OfferPriorityScoreComparator implements Comparator< JsonObject > {
	public int compare( JsonObject programActionA, JsonObject programActionB )
	{
		double priorityScoreA;
		double priorityScoreB;

		JsonElement ePriorityScoreA = programActionA.get( "offerPriorityScore" );
		if( ePriorityScoreA == null || ePriorityScoreA.isJsonNull() ) {
			ePriorityScoreA = programActionA.get( "OFFER_SCORE" );
			if( ePriorityScoreA == null || ePriorityScoreA.isJsonNull() )
				priorityScoreA = 0;
			else
				priorityScoreA = ePriorityScoreA.getAsDouble();
		}
		else
			priorityScoreA = ePriorityScoreA.getAsDouble();

		JsonElement ePriorityScoreB = programActionB.get( "offerPriorityScore" );
		if( ePriorityScoreB == null || ePriorityScoreB.isJsonNull() ) {
			ePriorityScoreB = programActionB.get( "OFFER_SCORE" );
			if( ePriorityScoreB == null || ePriorityScoreB.isJsonNull() )
				priorityScoreB = 0;
			else
				priorityScoreB = ePriorityScoreB.getAsDouble();
		}
		else
			priorityScoreB = ePriorityScoreB.getAsDouble();

		if( priorityScoreA > priorityScoreB )
			return -1;

		if( priorityScoreA < priorityScoreB )
			return 1;

		return 0;    
	}
}
