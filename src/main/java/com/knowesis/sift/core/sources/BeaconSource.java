package com.knowesis.sift.core.sources;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;

public class BeaconSource extends BaseRichSpout {
	private long timeInMinute = 0;
	private Calendar calendar = Calendar.getInstance();
	long millisInMinute = 0;
	SpoutOutputCollector collector = null;
	private String configPlacement = null;
	
	public void setConfig( String configPlacement ) {
		System.out.println( "Setting the configPlacement in Beacon source as: " + configPlacement );
		this.configPlacement = configPlacement;
	}
	
	@Override
	 public Map< String, Object > getComponentConfiguration () {

		 Map< String, Object > configuration = super.getComponentConfiguration();
		 if ( configuration == null )
			 configuration = new HashMap< String, Object >();

		 System.out.println( "Setting the configPlacement: " + configPlacement );
		 configuration.put( "configPlacement", configPlacement );
		 
		 return configuration;
	 }
	
	@Override
	public void open( Map conf, TopologyContext context, SpoutOutputCollector collector ) {
		Date currentDate = new java.util.Date();
		calendar.setTime( currentDate );
		int nMin = calendar.get( Calendar.MINUTE );
		calendar.set( calendar.get( Calendar.YEAR ), calendar.get( Calendar.MONTH ), calendar.get( Calendar.DATE ), calendar.get( Calendar.HOUR_OF_DAY ), nMin, 0 );
		timeInMinute = calendar.getTimeInMillis();
		timeInMinute += TimeZone.getDefault().getOffset( timeInMinute );
		millisInMinute = 1000 * 60;
		this.collector = collector;
	}

	@Override
	public void nextTuple() {
		while( true ) {
			try {
				timeInMinute = ( timeInMinute / 1000 ) * 1000;
				this.collector.emit( new Values( timeInMinute ) );
				Thread.sleep( millisInMinute );
				timeInMinute += millisInMinute;
			} catch ( InterruptedException e ) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void declareOutputFields( OutputFieldsDeclarer declarer ) {
		declarer.declare( new Fields( "timeStamp" ) );
	}
}
