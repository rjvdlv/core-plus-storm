package com.knowesis.sift.core.sources;

/**
 * @author KNOWESIS SIFT PRODUCT DEVELOPMENT TEAM
 *
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import com.knowesis.sift.Common.Commons;

public class FileSource extends BaseRichBolt {

	private OutputCollector outputCollector = null;
	private String processedDirectory;
	private boolean deleteFile;
	private File thisFile = null;
	private String thisFileName = null;
	private boolean validationNeeded;
	private String validPattern;
	private File validationFile;
	private Path path;
	private String configPlacement = null;
	int i = 0;

	public String getProcessedDirectory() {
		return processedDirectory;
	}

	public void setProcessedDirectory( String processedDirectory ) {
		System.out.println( "FileSource: ProcessedDirectory= " + processedDirectory );
		this.processedDirectory = processedDirectory;
	}

	public boolean isDeleteFile() {
		return deleteFile;
	}

	public void setDeleteFile( boolean deleteFile ) {
		System.out.println( "FileSource: deleteFile= " + deleteFile );
		this.deleteFile = deleteFile;
	}

	public boolean isValidationNeeded() {
		return validationNeeded;
	}

	public void setValidationNeeded( boolean validationNeeded ) {
		System.out.println( "FileSource: validationNeeded= " + validationNeeded );
		this.validationNeeded = validationNeeded;
	}

	public String getValidPattern() {
		return validPattern;
	}

	public void setValidPattern( String validPattern ) {
		System.out.println( "FileSource: validPattern= " + validPattern );
		this.validPattern = validPattern;
	}
	
	public void setConfig( String configPlacement ) {
		System.out.println( "Setting the configPlacement in File source as: " + configPlacement );
		this.configPlacement = configPlacement;
	}
	
	@Override
	 public Map< String, Object > getComponentConfiguration () {

		 Map< String, Object > configuration = super.getComponentConfiguration();
		 if ( configuration == null )
			 configuration = new HashMap< String, Object >();

		 System.out.println( "Setting the configPlacement: " + configPlacement );
		 configuration.put( "configPlacement", configPlacement );
		 
		 return configuration;
	 }

	@Override
	public void prepare( Map stormConf, TopologyContext context, OutputCollector collector ) {
		outputCollector = collector;
	}

	@Override
	public void execute( Tuple tuple ) {

		String file = tuple.getStringByField( "File" );
		Commons.logger.info( Commons.logPrefix + "----Tuple in fs: " + tuple.toString() );
		try {
			thisFile = new File( file );
			path = thisFile.toPath();
			BufferedReader reader = Files.newBufferedReader( path, Charset.forName("UTF-8") );

			String currentLine = null;
			
			while( ( currentLine = reader.readLine() ) != null ) { 
				Commons.logger.info( Commons.logPrefix + "Printing within filesource: " + currentLine );			
				outputCollector.emit( new Values( currentLine ) );
			}
			
			Commons.logger.info( Commons.logPrefix + "end of file read: " + path.toString() );
			
			if( deleteFile )
				thisFile.delete();
			else
				thisFile.renameTo( new File( processedDirectory + "/" + thisFile.getName() ) );

			if( validationNeeded ) {
				int index = file.lastIndexOf( "." );
				String validationFileName = file.substring( 0, index + 1 ) + validPattern;
				Commons.logger.info( Commons.logPrefix + "Validation file: " + validationFileName );

				validationFile = new File( validationFileName );
				String validationFileActualName = validationFile.getName();
				validationFile.renameTo( new File( processedDirectory + "/" + validationFileActualName ) );
			}
			

		} catch ( IOException e ) {
			e.printStackTrace();
		}
	}

	@Override
	public void declareOutputFields( OutputFieldsDeclarer declarer ) {
		declarer.declare( new Fields( "In_CDR" ) );
	}

}
