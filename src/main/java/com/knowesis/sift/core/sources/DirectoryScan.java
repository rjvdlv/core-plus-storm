package com.knowesis.sift.core.sources;

/**
 * @author KNOWESIS SIFT PRODUCT DEVELOPMENT TEAM
 *
 */

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;

import com.google.common.collect.Ordering;
import com.knowesis.sift.Common.Commons;

public class DirectoryScan extends BaseRichSpout {

	private static final long serialVersionUID = 6725461286484035943L;
	private String inputDirectory;
	private String compressionType;
	private String fileNamePattern;
	private boolean validationNeeded;
	private String validPattern;
	private File validationFile;
	private SpoutOutputCollector collector;
	private Path inputPathDir = null;
	private List< Path > existingPaths;
	private Path thisPath;
	private Map< String, String > waitingFiles = null;
	private long lastScanTime;
	private long waitFileScanInterval = 1000 * 5;
	private String configPlacement = null;
	private FileSystem fileSystem = null;
	private PathMatcher pathMatcher = null;


	public String getInputDirectory() {
		return inputDirectory;
	}


	public void setInputDirectory( String inputDirectory ) {
		System.out.println( "Directory Scan: input directory= " + inputDirectory );
		this.inputDirectory = inputDirectory;
	}


	public String getCompressionType() {
		return compressionType;
	}


	public void setCompressionType( String compressionType ) {
		System.out.println( "Directory Scan: compressionType= " + compressionType );
		this.compressionType = compressionType;
	}


	public String getFileNamePattern() {
		return fileNamePattern;
	}


	public void setFileNamePattern( String fileNamePattern ) {
		System.out.println( "Directory Scan: fileNamePattern= " + fileNamePattern );
		this.fileNamePattern = fileNamePattern;
	}


	public boolean isValidationNeeded() {
		return validationNeeded;
	}


	public void setValidationNeeded( boolean validationNeeded ) {
		System.out.println( "Directory Scan: validationNeeded= " + validationNeeded );
		this.validationNeeded = validationNeeded;
	}


	public String getValidPattern() {
		return validPattern;
	}


	public void setValidPattern( String validPattern ) {
		System.out.println( "Directory Scan: validPattern= " + validPattern );
		this.validPattern = validPattern;
	}

	public void setConfig( String configPlacement ) {
		System.out.println( "Setting the configPlacement in Directory scan source as: " + configPlacement );
		this.configPlacement = configPlacement;
	}

	@Override
	public Map< String, Object > getComponentConfiguration () {

		Map< String, Object > configuration = super.getComponentConfiguration();
		if ( configuration == null )
			configuration = new HashMap< String, Object >();

		System.out.println( "Setting the configPlacement: " + configPlacement );
		configuration.put( "configPlacement", configPlacement );

		return configuration;
	}

	@Override
	public void open( Map conf, TopologyContext context, SpoutOutputCollector collector ) {
		this.collector = collector;
		existingPaths = null;
		thisPath = null;
		fileSystem = FileSystems.getDefault();
		inputPathDir = fileSystem.getPath( inputDirectory );
		pathMatcher = fileSystem.getPathMatcher( "glob:" + fileNamePattern );
		waitingFiles = new HashMap< String, String >();
		lastScanTime = 0;
	}


	@Override
	public void nextTuple() {

		// Read the existing files in the input directory first.
		existingPaths = getStream( inputPathDir, fileNamePattern );
		Commons.logger.info( Commons.logPrefix + "Trying to fetch files with filenamepattern: " + fileNamePattern );
		Collections.sort( existingPaths, new PathComparator() );
		for( int k = 0; k < existingPaths.size(); k ++ ) {
			String thisPath = existingPaths.get( k ).toString();
			Commons.logger.info( Commons.logPrefix + "--------Emitting existing file in DirectoryScan: " + thisPath );
			if( validationNeeded ) {
				if( isValidFile( thisPath ) )
					this.collector.emit( new Values( thisPath ) );
				else
					waitingFiles.put( thisPath, getValidFileName( thisPath ) );
			}
			else
				this.collector.emit( new Values( thisPath ) );
		}

		// Reading the continuously incoming files in the input directory..
		while( true ) {
			try {
				Commons.logger.info( Commons.logPrefix + "trying to fetch new files.." );
				WatchService watcher = inputPathDir.getFileSystem().newWatchService();
				inputPathDir.register( watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY );
				WatchKey watchKey = watcher.take();
				Thread.sleep(1000);
				List< WatchEvent< ? > > events = watchKey.pollEvents();
				List< Path > newPaths = new ArrayList< Path >();

				for ( WatchEvent event : events ) {
					Commons.logger.info( Commons.logPrefix + "New Events.. " + event.kind().toString() );
					//if ( event.kind() == StandardWatchEventKinds.ENTRY_CREATE || event.kind() == StandardWatchEventKinds.ENTRY_MODIFY ) {
					if ( event.kind() == StandardWatchEventKinds.ENTRY_CREATE ) {	
						thisPath = ( Path ) event.context();
						Commons.logger.info( Commons.logPrefix + "Listing: " + thisPath.toString() );

						if ( ! pathMatcher.matches( thisPath ) ) {
							Commons.logger.info( Commons.logPrefix + "No Match.." + thisPath );
		                	continue;
		                }

						newPaths.add( Paths.get( inputDirectory + "/" + thisPath ) );
					}
				}

				Collections.sort( newPaths, new PathComparator() );
				for( int i = 0; i < newPaths.size(); i ++ ) {
					String thisPathStr = newPaths.get( i ).toString();
					Commons.logger.info( Commons.logPrefix + "--------Emitting in DirectoryScan: " + thisPathStr );
					if( validationNeeded ) {
						if( isValidFile( thisPathStr ) )
							this.collector.emit( new Values( thisPathStr ) );
						else
							waitingFiles.put( thisPathStr, getValidFileName( thisPathStr ) );
					}
					else
						this.collector.emit( new Values( thisPathStr ) );
				}

				if( ( ( new Date().getTime() - lastScanTime ) > waitFileScanInterval ) && waitingFiles.size() > 0 ) {

					Commons.logger.info( Commons.logPrefix + "Scanning waiting files.." );
					Iterator< Entry< String, String > > iterator = waitingFiles.entrySet().iterator();
					while( iterator.hasNext() ) {
						Entry< String, String > thisEntry = iterator.next();
						String file = thisEntry.getKey();
						Path thisWaitingPath = Paths.get( thisEntry.getValue() );

						if( Files.exists( thisWaitingPath ) ) {
							Commons.logger.info( Commons.logPrefix + "Emitting from waiting files: " + file );
							this.collector.emit( new Values( file ) );
							waitingFiles.remove( file );
						}
					}
				}
			} catch (Exception e) {
				System.out.println( "Error: " + e.toString() );
			}
		}
	}

	private boolean isValidFile( String filePath ) {
		String validationFileName = getValidFileName( filePath );
		validationFile = new File( validationFileName );

		if( validationFile.exists() && ! validationFile.isDirectory() )
			return true;

		return false;
	}

	private String getValidFileName( String filePath ) {
		int index = filePath.lastIndexOf( "." );
		String validationFileName = filePath.substring( 0, index + 1 ) + validPattern;
		System.out.println( "Validation file: " + validationFileName );
		return validationFileName;
	}


	private List< Path > getStream( Path pdir, String fileNamePattern ) {
		try ( DirectoryStream< Path > paths = Files.newDirectoryStream( pdir, fileNamePattern ) ) {
			return Ordering.natural().sortedCopy( paths );
		} catch (IOException e) {
			return Collections.emptyList();
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare( new Fields( "File" ) );
	}

	public static class PathComparator implements Comparator< Path > {

		@Override
		public int compare( Path o1, Path o2 ) {
			// TODO Auto-generated method stub
			File file1 = new File( o1.toString() );
			File file2 = new File( o2.toString() );

			long lastModifiedA = file1.lastModified();
			long lastModifiedB = file2.lastModified();

			if ( lastModifiedA > lastModifiedB )
				return 1;
			else if ( lastModifiedB > lastModifiedA )
				return -1;

			return 0;
		}
	}

}
