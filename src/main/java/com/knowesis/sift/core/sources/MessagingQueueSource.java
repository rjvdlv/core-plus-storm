package com.knowesis.sift.core.sources;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;

import com.knowesis.sift.Common.Commons;

public class MessagingQueueSource extends BaseRichSpout {
	
	private String queueType;
	private String brokerURL;
	private String queueNames;
	private String messageType;
	private String userName;
	private String password;
	private String configPlacement;
	
	private ConnectionFactory connectionFactory = null;
	private Connection connection = null;
	private Session session = null;
	private Destination destination = null;
	private MessageConsumer consumer = null;
	private SpoutOutputCollector collector = null;
	
	
	public String getQueueType() {
		return queueType;
	}

	public void setQueueType( String queueType ) {
		System.out.println( "MessagingQueueSource: Queuetype= " + queueType );
		this.queueType = queueType;
	}

	public String getBrokerURL() {
		return brokerURL;
	}

	public void setBrokerURL( String brokerURL ) {
		System.out.println( "MessagingQueueSource: brokerURL= " + brokerURL );
		this.brokerURL = brokerURL;
	}

	public String getQueueNames() {
		return queueNames;
	}

	public void setQueueNames( String queueNames ) {
		System.out.println( "MessagingQueueSource: queueNames= " + queueNames );
		this.queueNames = queueNames;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType( String messageType ) {
		System.out.println( "MessagingQueueSource: messageType= " + messageType );
		this.messageType = messageType;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName( String userName ) {
		System.out.println( "MessagingQueueSource: userName= " + userName );
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword( String password ) {
		System.out.println( "MessagingQueueSource: password= " + password );
		this.password = password;
	}
	
	public void setConfig( String configPlacement ) {
		System.out.println( "Setting the configPlacement in Message queue source as: " + configPlacement );
		this.configPlacement = configPlacement;
	}
	
	@Override
	 public Map< String, Object > getComponentConfiguration () {

		 Map< String, Object > configuration = super.getComponentConfiguration();
		 if ( configuration == null )
			 configuration = new HashMap< String, Object >();

		 System.out.println( "Setting the configPlacement: " + configPlacement );
		 configuration.put( "configPlacement", configPlacement );
		 
		 return configuration;
	 }
	
	@Override
	public void open( Map conf, TopologyContext context, SpoutOutputCollector collector ) {
		
		this.collector = collector;
		System.out.println( "Initializing MessagingQueueSource..." );
	}

	@Override
	public void nextTuple() {
		try {
			if( queueType.equals( "AMQ" ) )
				produceAMQTuples();
		} catch( Exception e ) {
			consumer = null;
			connection = null;
			session = null;
			destination = null;
			e.printStackTrace();
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		System.out.println( "declaring the declare output.." );
		declarer.declare( new Fields( "In_CDR" ) );
	}
	
	private void produceAMQTuples() throws Exception  {
		System.out.println( "Ready to fetch messaging queue messages" );

		if( session == null || connection == null || destination == null || consumer == null ) {
			try {
				System.out.println( new Date() + ": Trying to initialise the consumer connection." );
				String thisMessage = null;

				// Create a ConnectionFactory
				connectionFactory = new ActiveMQConnectionFactory( userName, password, brokerURL );
				// Create a Connection
				connection = connectionFactory.createConnection();
				connection.start();
				// Create a Session
				session = connection.createSession( false, Session.AUTO_ACKNOWLEDGE );
				// Create the destination
				destination = session.createQueue( queueNames );
				// Create a MessageProducer from the Session to the Queue
				consumer = session.createConsumer( destination );

				System.out.println( new Date() + ": Consumer connection Successful." );
				Message msg = null;
				while( session != null && connection != null && destination != null && consumer != null ) {
					Message messagesReceived = consumer.receive( 10 * 60 * 1000 );
					if( "map".equalsIgnoreCase( messageType ) ) {
						msg = ( MapMessage ) messagesReceived;
					}else if( "text".equalsIgnoreCase( messageType ) ) {
						msg = ( TextMessage ) messagesReceived;
					}
					if( messagesReceived == null ) {
			 			continue;
					}else {
						if( "map".equals( messageType ) ) {
							MapMessage mapMessage = ( MapMessage ) msg;
							thisMessage = mapMessage.getString( "message" );
							Commons.logger.info( Commons.logPrefix + "Received the map message: " + thisMessage );
						} 
						else if( "text".equals( messageType ) ) {
							TextMessage textMessage = ( TextMessage ) msg;
							thisMessage = textMessage.getText();
							Commons.logger.info( Commons.logPrefix + "Received the text message: " + thisMessage );
						}

						this.collector.emit( new Values( thisMessage ), thisMessage );
						Commons.logger.info( Commons.logPrefix + "Emitted the message from MessagingQueueSource: " + thisMessage );
					}
				}
			}catch( Exception e ) {
				try{ 
					connection.stop();
					consumer.close();
					session.close();
				}
				catch (Exception ex) {
				}
				consumer = null;
				connection = null;
				session = null;
				destination = null;
				System.out.println( e );
				e.printStackTrace();
				
				System.out.println( "Will retry after 1 minutes.." );
				Thread.sleep( 1000 * 60 * 1 );
				produceAMQTuples();
			}
		}
	}
}