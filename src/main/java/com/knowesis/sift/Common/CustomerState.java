package com.knowesis.sift.Common;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.knowesis.sift.expression.SiftMath;

public class CustomerState {
	public String customerId;
	public JsonObject subscriberEventTags;
	public JsonObject subscriberNextWaveEventTags;
	public JsonObject subscriberTriggers;
	public JsonObject subscriberPrograms;
	public JsonObject subscriberIndicators;
	public HashMap< String, JsonArray > triggerSinkMap;
	public LinkedHashSet< String > processedInd;
	public JsonObject tupleMap;
	public java.util.Date thisRecordDate;
	public String recordType; 
	public Calendar cal;
	public List< JsonObject > newOfferActions;
	public JsonArray fulfillmentActions;
	public JsonArray reminderActions;
	public JsonObject currentMapping;
	public String tupleString;
	public String[ ] tupleStringArray; 
	public String cellId;
	public SiftMath siftMath;
	
	//online customer state attributes
	public JsonObject siftOnlineResponses = new JsonObject();
	public JsonObject siftOutboundNoActionReasons = new JsonObject();
	public JsonObject siftOnlineNoActionReasons = new JsonObject();
	public JsonObject request;
	public JsonElement filterElement;
	public int numOffersInRequest = 100;
	public JsonObject sessionIndicators;
	public String contactDirection;
	
	//STEROID-GOLDBISCUIT
	public JsonObject siftResponses = new JsonObject();
	public List< String > newOffersList = new ArrayList< String >();
	public boolean isBuyNotificationOnline = false;
	public HashMap< String, String[] > offerLocationsMap = new HashMap< String, String[] >();
	
}