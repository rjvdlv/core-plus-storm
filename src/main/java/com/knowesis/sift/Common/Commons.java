package com.knowesis.sift.Common;

import java.util.HashMap;
import java.util.LinkedHashMap;
//import java.util.logging.Logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.expression.PExpressionEvaluator;

public class Commons {
	public static int telcoPersistInterval = 1000;
	public static int telcoIndicatorsRetentionPeriod = 30;
	public static HashMap< String, PExpressionEvaluator > expMap = null;
	public static LinkedHashMap< String, JsonObject > allIndDef = null;
	public static LinkedHashMap< String, JsonObject > summaryIndDef = null;
	public static LinkedHashMap< String, JsonObject > allEveDef = null;
	public static LinkedHashMap< String, JsonObject > allProgramDef = null;
	public static LinkedHashMap< String, JsonObject > allOfferDef = null;
	public static LinkedHashMap< String, JsonObject > allFunctions = null;
	public static LinkedHashMap< String, JsonObject > allCIM = null;
	
	public static int pandaCacheSize = 1000000;
	
	public static HashMap< String, JsonObject > supportingData = null;
	public static PandaCache pandaCache = null;
	public static PandaCacheConfig pandaCacheConfig = null;

	public static String siftMode = "Full";
	
	public static JsonObject eventsTaggedforAll = null;
	
	// Event Prioritisation 21Apr2015
	public static String defaultTargetSystems = "FS1|JMS1|SNMP1|JMSLB1";
	public static HashMap< String, String > fixedOfferParams = new HashMap< String, String >();

	public static JsonObject siftConfig;
	// Tuple.Owner = 0 For Caller
	// 1 for Callee
	public static int numberOfInstances;
	//public static Logger logger = Logger.getLogger( "com.knowesis.sift.core.PandamaticOperator.log" );
	public static Logger logger = LoggerFactory.getLogger( "com.knowesis.sift.core.PandamaticOperator.log" );
	
	public static JsonParser parser = new JsonParser();
	
	public static HashMap< String, Object > modelMasterTable = new HashMap< String, Object >();
	public static boolean flowFunnelFlag = false;
	public static String EventsToBeProcessed = "ALL";
	public static boolean filterEvents = false;
	public static boolean populateTelcoIndicators = false;
	public static JsonObject contactPolicyObject = new JsonObject(); 
	
	public static LRUPandaCache< String, String > customersForLock = null;
	
	public static double lowScoreThreshold = 0.0;
	public static String logPrefix = "";
	
	public static int currentUpdateStatus = -1;

	public int nWorkerId = 1;
	public LRUPandaCache< String, JsonObject > subscriberIndicatorsMap = null;
	public LRUPandaCache< String, JsonObject > eventsTaggedForSubscribersMap = null;
	public HashMap< String, JsonObject > eventTriggerCountsMap = null;
	public HashMap< String, JsonObject > summaryIndicatorsMap = null;
	public HashMap< String, JsonObject > eventMonitoringCountsMap = null;
	public HashMap< String, JsonObject > flowFunnelMap = null;
	public Long firstTupleTimestamp;
	public Long lastTupleTimestamp;
	public Integer numTuples = 0;
}
