package com.knowesis.sift.Common;

import java.io.IOException;
import java.net.URI;
import java.util.LinkedList;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.CouchbaseConnectionFactoryBuilder;
import com.couchbase.client.protocol.views.View;

/**
 * @author Raja SP
 * 
 */

public class PandaCache {
	private  static CouchbaseClient couchClient = null;
	private static String designDoc = "pandalytics";

	public PandaCache(){
		if( couchClient == null ) {
			LinkedList< URI > uris = new LinkedList< URI >();
			//uris.add( URI.create( "http://119.81.94.148:8091/pools" ) );
			uris.add( URI.create( "http://127.0.0.1:8091/pools" ) );
			initCacheClient( uris );
		}
	}

	public PandaCache( LinkedList< URI > uris ){
		if( couchClient == null )
			initCacheClient( uris );
	}

	private void initCacheClient( LinkedList< URI > uris ) {
		try {
			CouchbaseConnectionFactoryBuilder cfb = new CouchbaseConnectionFactoryBuilder();
			cfb.setOpTimeout( 10000 );
			//	cfb.setTimeoutExceptionThreshold( 240000 );
			couchClient = new CouchbaseClient( cfb.buildCouchbaseConnection( uris, "default", "" ) );
			System.out.println( "************************************* Initialised CouchClient ********** " + couchClient.toString() );
		}catch( IOException e ) {
			System.err.println( "IOException connecting to Panda Cache: " + e.getMessage() );
			System.exit( 1 );
		}
	}

	public CouchbaseClient getClient() {
		return couchClient;
	}

	public View getView( String viewName ) {
		return couchClient.getView( designDoc, viewName );
	}
	
	public void set( String id, int expiry, String data ) {
		try {
			couchClient.set( id, expiry, data );
		} catch( Exception e ) {
			System.out.println( "Node Down? While setting the document for id: " + id );
			System.out.println( "Exception : " + e.toString() ); 
		}
	}

	public void delete( String id ) {
		try {
			couchClient.delete( id );
		} catch( Exception e ) {
			System.out.println( "Node Down? While deleting the document for id: " + id );
			System.out.println( "Exception : " + e.toString() ); 
		}
	}

	
	public Object get( String id ) {
		Object result = null;
 		try {
		    result = getClient().get( id );
		} catch( Exception e ) {
			System.out.println( "Node Down? While getting the document for id: " + id );
			System.out.println( "Will try to get from Replica : " + e.toString() ); 
			try {
			    result = couchClient.getFromReplica( id );
			}  catch( Exception eRep ) {
				System.out.println( "Node Down? While getting the document for id: " + id );
				System.out.println( "Getting from Replica Failed. May be no replica available : " + eRep.toString() ); 
				return null;
			}
		}
		return result;
	}
}