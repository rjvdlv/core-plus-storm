package com.knowesis.sift.Common;

import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.JsonObject;

public class LRUPandaCache<K,T> extends LinkedHashMap<K,T> {
	private static final long serialVersionUID = 1L;
	private int capacity;

	public LRUPandaCache( int capacity, float d ){
		super( capacity, d, true );
		this.capacity = capacity;
	}

	/**
	 * removeEldestEntry() should be overridden by the user, otherwise it will
	 * not remove the oldest object from the Map.
	 */
	@Override
	protected boolean removeEldestEntry( Map.Entry< K, T > eldest ) {
		return size() > this.capacity;
	}

	/**
	 * @param args
	 */
	public static void main( ) {
		LRUPandaCache< String, JsonObject > cache = new LRUPandaCache< String, JsonObject >( 100, 0.5f );
		cache.put( "93855012", new JsonObject() );
	}

}
