package com.knowesis.sift.Commons;

public class LocationNotDefinedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LocationNotDefinedException(String exp) {
		super( exp );
	}
}
