package com.knowesis.sift.expression;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

import org.codehaus.commons.compiler.CompileException;
import org.codehaus.janino.ExpressionEvaluator;
import org.codehaus.janino.ScriptEvaluator;

/**
 * @author Raja SP
 * 
 */
public class PExpressionEvaluator implements java.io.Serializable {

	private Class optionalExpressionType;
	private String[ ] parameterNames;
	private Class[ ] parameterTypes;
	private Class[ ] thrownExceptions;
	private String[ ] optionalDefaultImports;
	private String expression;
	private String name;
	private String type; // is this a rule or a computation
	private String serviceType;
	private String serviceSubType;
	private String frequency;
	private org.codehaus.janino.ScriptEvaluator evaluator;
	private String scriptOrExpression;
	
	public PExpressionEvaluator( String type ) {
		scriptOrExpression = type;
		evaluator = new ExpressionEvaluator();
		if( type != null && type.equals( "script") ) {
			evaluator = new ScriptEvaluator();
		}
		evaluator.setDefaultImports( new String[] { "com.google.gson.*", "com.ibm.streams.operator.*", "com.knowesis.sift.expression.*",
				"com.knowesis.sift.Common.*" } );
		evaluator.setThrownExceptions( new Class[] { PExpressionException.class , SiftFeedRejectException.class, 
				                    java.text.ParseException.class, NoSuchMethodException.class, InstantiationException.class,
				                    InvocationTargetException.class, IllegalAccessException.class } );
	}
	
	public Object evaluate( Object[] paramValues ) throws InvocationTargetException {
		return evaluator.evaluate( paramValues );
	}

	public String getServiceSubType() {
		return serviceSubType;
	}

	public void setServiceSubType( String serviceSubType ) {
		this.serviceSubType = serviceSubType;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType( String serviceType ) {
		this.serviceType = serviceType;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency( String frequency ) {
		this.frequency = frequency;
	}

	/**
	 * @return the expressionType
	 */
	public String getType() { // computation or detection
		return type;
	}

	/**
	 * @param expressionType
	 *            the expressionType to set
	 */
	public void setType( String type ) {
		this.type = type;
	}

	public Class getOptionalExpressionType() {
		return optionalExpressionType;
	}

	public void setExpressionType( Class optionalExpressionType ) {
		this.optionalExpressionType = optionalExpressionType;
		if( scriptOrExpression == null || scriptOrExpression.equals( "expression" ) )
		     ( ( ExpressionEvaluator )evaluator ).setExpressionType( optionalExpressionType );
		else
			evaluator.setReturnType( optionalExpressionType );
	}

	public String[ ] getParameterNames() {
		return parameterNames;
	}

	public void setParameterNames( String[ ] parameterNames ) {
		this.parameterNames = parameterNames;
	}

	public Class[ ] getParameterTypes() {
		return parameterTypes;
	}

	public void setParameterTypes( Class[ ] parameterTypes ) {
		this.parameterTypes = parameterTypes;
	}

	public void setParameters( String[ ] parameterNames, Class[ ] parameterTypes ) {
		this.parameterNames = parameterNames;
		this.parameterTypes = parameterTypes;
		evaluator.setParameters( parameterNames, parameterTypes );
	}

	public Class[ ] getThrownExceptions() {
		return thrownExceptions;
	}

	public void setThrownExceptions( Class[ ] thrownExceptions ) {
		this.thrownExceptions = thrownExceptions;
	}

	public String[ ] getOptionalDefaultImports() {
		return optionalDefaultImports;
	}

	public void setOptionalDefaultImports( String[ ] optionalDefaultImports ) {
		this.optionalDefaultImports = optionalDefaultImports;
	}

	public String getExpression() {
		return expression;
	}

	public void setExpression( String expression ) {
		this.expression = expression;
	}

	public String getName() {
		return name;
	}

	public void setName( String name2 ) {
		this.name = name2;
		// TODO Auto-generated method stub
	}
	
	public void cook( String expression ) throws CompileException {
		this.evaluator.cook( expression );
	}

	public void validateExpression( String name, Class optionalExpressionType, String[ ] parameterNames,
			Class[ ] parameterTypes, String type, String serviceType, String frequency, String expression, String scriptOrExpression )
			throws PExpressionException {

		PExpressionEvaluator anExpEval = new PExpressionEvaluator( scriptOrExpression );
		anExpEval.setName( name );
		anExpEval.setExpressionType( optionalExpressionType );
		anExpEval.setParameters( parameterNames, parameterTypes );
		anExpEval.setType( type );
		anExpEval.setServiceType( serviceType );
		anExpEval.setFrequency( frequency );
		anExpEval.setThrownExceptions( new Class[] { PExpressionException.class , SiftFeedRejectException.class, 
                java.text.ParseException.class, NoSuchMethodException.class, InstantiationException.class,
                InvocationTargetException.class, IllegalAccessException.class } );
		try {
			anExpEval.cook( expression );
		}catch( CompileException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new PExpressionException( e.toString() );
		}
	}

	public void validateExpression() throws PExpressionException {
		try {
			cook( expression );
		}catch( CompileException e ) {
			e.printStackTrace();
			throw new PExpressionException( e.getMessage() );
		}
	}

	@Override
	public String toString() {
		return "PExpressionEvaluator [optionalExpressionType=" + optionalExpressionType + ", parameterNames="
				+ Arrays.toString( parameterNames ) + ", parameterTypes=" + Arrays.toString( parameterTypes )
				+ ", thrownExceptions=" + Arrays.toString( thrownExceptions ) + ", optionalDefaultImports="
				+ Arrays.toString( optionalDefaultImports ) + ", expression=" + expression + ", name=" + name
				+ ", indicatorType=" + type + ", serviceType=" + serviceType + ", frequency=" + frequency + "]";
	}

}