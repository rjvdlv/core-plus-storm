package com.knowesis.sift.expression;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.codehaus.commons.compiler.CompileException;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.CustomerState;
import com.knowesis.sift.Common.PandaCacheConfig;

public class ExpressionProcessor {

	/**
	 * All indicator definitions.
	 */
	private LinkedHashMap< String, JsonObject > allIndicators;

	/**
	 * All event definitions.
	 */
	private LinkedHashMap< String, JsonObject > allEvents;

	/**
	 * All function definitions.
	 */
	private LinkedHashMap< String, JsonObject > allFunctions;

	/**
	 * All CIM records.
	 */
	private LinkedHashMap< String, JsonObject > allCIM;

	/**
	 * All User Defined Functions.
	 */
	private LinkedHashMap< String, JsonObject > userDefinedFunctions;

	private LinkedHashMap< String, JsonObject > allIndicatorsEvents;

	private HashMap<String, PExpressionEvaluator> existingExpressions = null;
	
	
	
	public HashMap< String, PExpressionEvaluator > getExistingExpressions() {
		return existingExpressions;
	}

	public void setExistingExpressions( HashMap< String, PExpressionEvaluator > existingExpressions ) {
		this.existingExpressions = existingExpressions;
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public PExpressionEvaluator getCompiledExpression( JsonObject jObj ) throws PExpressionException {
		String strId = jObj.get( "id" ).getAsString();
		String strExpression = jObj.get( "expression" ).getAsString();
		strExpression = preprocessExpression( strExpression );
		System.out.println( "Preprocessed Expression : " + strExpression );
		
		String expressionType = "boolean";
		
		if (jObj.has( "expressionType" ) && !jObj.get( "expressionType" ).isJsonNull()) {
			expressionType = jObj.get( "expressionType" ).getAsString();
		}
		String scriptOrExpression = null;

		scriptOrExpression = "expression";
		String[ ] lines = strExpression.split( ";" );
		if( lines.length > 1 || ( lines.length == 1 && strExpression.contains( "return" ) ) )
			scriptOrExpression = "script";
		// }

		if( strExpression == null || strExpression.trim().length() == 0 )
			return null;

		// EXPRESSION REFERING TO SELF- VALIDATION
		Object[ ] paramNamesTypes = inferParamNamesTypes( strId, expressionType, strExpression, null );
		String[ ] arrInferredParamNames = ( String[] )paramNamesTypes[ 0 ];
		Class[ ] arrInferredParamTypes = ( Class[] )paramNamesTypes[ 1 ];
		
		String[] arrFinalParamNames = null;
		Class[] arrFinalParamTypes = null;
		
		//if this is a function, add all the userdefined parameters to this.
		JsonElement jUDF =  jObj.get( "userDefinedFunction" );
		if( jUDF != null && ! jUDF.isJsonNull() && jUDF.getAsBoolean() == true ) {
			String udfParamNames = jObj.get( "paramNames" ).getAsString();
			String udfParamTypes = jObj.get( "paramTypes" ).getAsString();
			String[ ] arrParamNames = ExpressionUtil.explode( udfParamNames );
			Class[ ] arrParamTypes = ExpressionUtil.stringToTypes( udfParamTypes );
			List consolidatedParamNames = new ArrayList();
			for( int i=0; i<arrParamNames.length; i++ )
				consolidatedParamNames.add( arrParamNames[ i ] );
			for( int i=0; i<arrInferredParamNames.length; i++ )
				consolidatedParamNames.add( arrInferredParamNames[ i ] );

			List consolidatedParamTypes = new ArrayList();
			for( int i=0; i<arrParamNames.length; i++ )
				consolidatedParamTypes.add( arrParamTypes[ i ] );
			for( int i=0; i<arrInferredParamNames.length; i++ )
				consolidatedParamTypes.add( arrInferredParamTypes[ i ] );

			arrFinalParamNames = ( String[] ) consolidatedParamNames.toArray( new String[ consolidatedParamNames.size() ] );
			arrFinalParamTypes = ( Class[] ) consolidatedParamTypes.toArray( new Class[ consolidatedParamTypes.size() ] );
		} else {
			arrFinalParamNames = arrInferredParamNames;
			arrFinalParamTypes = arrInferredParamTypes;
		}
		
		PExpressionEvaluator expEval = new PExpressionEvaluator( scriptOrExpression );
		expEval.setExpression( strExpression );

		expEval.setExpressionType( ExpressionUtil.stringToType( expressionType ) );
		expEval.setParameters( arrFinalParamNames, arrFinalParamTypes);

		boolean isValid = false;
		
		try {
			expEval.validateExpression();
			isValid = true;
		} catch (Exception ex) {
			ex.printStackTrace();
			isValid = false;
		}
		
		if (isValid) {
			try {
				expEval.cook( strExpression );
				System.out.println( "Setting Parameters to : " + strId + " - " + arrFinalParamNames.toString() );
			}catch( CompileException e ) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new PExpressionException( e.toString() );
			}
			return expEval;
		} else {
			return null;
		}	
	}

	
	/**
	 * PExpressionEvaluator creation. This will be called by camel API.
	 * 
	 * @param jObj
	 *            - JsonObject
	 * @return PEExpressionEvualator (Expression Evaluator, it has
	 *         parameterNames, parameterTypes, expression, ScriptEvualuator).
	 * @throws PExpressionException
	 *             if there is any error in processing expression.
	 * @throws ScanException
	 * @throws ParseException
	 */
	public PExpressionEvaluator createByDefinition( JsonObject jObj ) throws PExpressionException, ParseException {
		String strId = jObj.get( "id" ).getAsString();
		String strExpression = jObj.get( "expression" ).getAsString();

		strExpression = preprocessExpression( strExpression );
		System.out.println( "Preprocessed Expression : " + strExpression );
		String expressionType = jObj.get( "expressionType" ).getAsString();
		String scriptOrExpression = null;
		scriptOrExpression = "expression";
		String[ ] lines = strExpression.split( ";" );
		if( lines.length > 1 || ( lines.length == 1 && strExpression.contains( "return" ) ) ) {
			scriptOrExpression = "script";
		}

		if( strExpression == null || strExpression.trim().length() == 0 ) {
			return null;
		}

		Object[ ] paramNamesTypes = inferParamNamesTypes( strId, expressionType, strExpression, null );
		String[ ] arrInferredParamNames = ( String[ ] ) paramNamesTypes[ 0 ];
		Class[ ] arrInferredParamTypes = ( Class[ ] ) paramNamesTypes[ 1 ];
		
		//TODO : Remove this SOP
		for( int i = 0; i < arrInferredParamNames.length; i++ )
			System.out.println( "------------Infered params " + arrInferredParamNames[ i ] + " -- " + arrInferredParamTypes[ i ].getCanonicalName()
					+ " -- " + arrInferredParamTypes[ i ].getSimpleName() );

		String[ ] arrFinalParamNames = null;
		Class[ ] arrFinalParamTypes = null;

		JsonElement jUDF = jObj.get( "userDefinedFunction" );

		if( jUDF != null && !jUDF.isJsonNull() && jUDF.getAsBoolean() == true ) {
			String udfParamNames = "";
			String udfParamTypes = "";
			if( jObj.get( "paramNames" ) != null && ! jObj.get( "paramNames" ).isJsonNull() ) {
				udfParamNames = jObj.get( "paramNames" ).getAsString();
				udfParamTypes = jObj.get( "paramTypes" ).getAsString();
			}
			String[ ] arrParamNames = ExpressionUtil.explode( udfParamNames );
			Class[ ] arrParamTypes = ExpressionUtil.stringToTypes( udfParamTypes );
			List< String > consolidatedParamNames = new ArrayList< String >();
			for( int i = 0; i < arrParamNames.length; i++ )
				consolidatedParamNames.add( arrParamNames[ i ] );
			for( int i = 0; i < arrInferredParamNames.length; i++ )
				consolidatedParamNames.add( arrInferredParamNames[ i ] );

			List< Class > consolidatedParamTypes = new ArrayList< Class >();
			for( int i = 0; i < arrParamNames.length; i++ )
				consolidatedParamTypes.add( arrParamTypes[ i ] );
			for( int i = 0; i < arrInferredParamNames.length; i++ )
				consolidatedParamTypes.add( arrInferredParamTypes[ i ] );

			arrFinalParamNames = ( String[ ] ) consolidatedParamNames.toArray( new String[ consolidatedParamNames.size() ] );
			arrFinalParamTypes = ( Class[ ] ) consolidatedParamTypes.toArray( new Class[ consolidatedParamTypes.size() ] );
		}else {
			arrFinalParamNames = arrInferredParamNames;
			arrFinalParamTypes = arrInferredParamTypes;
		}

		PExpressionEvaluator expEval = new PExpressionEvaluator( scriptOrExpression );
		expEval.setExpression( strExpression );

		expEval.setExpressionType( ExpressionUtil.stringToType( expressionType ) );
		expEval.setParameters( arrFinalParamNames, arrFinalParamTypes );

		expEval.validateExpression();

		return expEval;
	}

	/**
	 * Validate expression.
	 * 
	 * @param thisExpId
	 *            - expression id (sift function name).
	 * @param strExpression
	 *            - expression (sift function body).
	 * @param expressionType
	 *            - sift function return type.
	 * @param scriptOrExpression
	 *            - whether script or expression.
	 * @throws PExpressionException
	 *             if expression is not valid.
	 * @throws IOException
	 * @throws ScanException
	 * @throws ParseException
	 */
	public void validateExpression( String thisExpId, String strExpression, String expressionType, String scriptOrExpression, JsonObject dynamicParameters )
			throws PExpressionException, IOException, ParseException {
		String[ ] lines = strExpression.split( ";" );
		if( lines.length > 1 || ( lines.length == 1 && strExpression.contains( "return" ) ) )
			scriptOrExpression = "script";

		System.out.println( "Before PREPROCESS :" + strExpression );
		strExpression = preprocessExpression( strExpression );
		System.out.println( "After PREPROCESS :" + strExpression );
		Object[ ] paramNamesTypes = inferParamNamesTypes( thisExpId, expressionType, strExpression, dynamicParameters );
		PExpressionEvaluator expEval = new PExpressionEvaluator( scriptOrExpression );
		expEval.setExpression( strExpression );
		expEval.setExpressionType( ExpressionUtil.stringToType( expressionType ) );
		expEval.setParameters( ( String[ ] ) paramNamesTypes[ 0 ], ( Class[ ] ) paramNamesTypes[ 1 ] );
		System.out.println( expEval.toString() );
		expEval.validateExpression();

	}

	/**
	 * 
	 * @param expId
	 *            - expression id (sift function name)
	 * @param expType
	 *            - expression type (return type of sift function)
	 * @param strExpression
	 *            - expression (method body of sift function)
	 * @param allAccums
	 *            - all indicators.
	 * @param allEvents
	 *            - all events.
	 * @param cimList
	 *            - CIM list composed of
	 *            (id,name,docType,type,alias,category,description,expression).
	 * @return
	 * @throws PExpressionException
	 */
	public Object[ ] inferParamNamesTypes( String expId, String expType, String strExpression, JsonObject dynamicParameters ) throws PExpressionException {
		String paramNames = new String();
		String paramTypes = new String();

		org.antlr.v4.runtime.CharStream cs = new ANTLRInputStream( strExpression );
		JavaLexer jl = new JavaLexer( cs );
		CommonTokenStream tokens = new CommonTokenStream( jl );
		JavaParser jp = new JavaParser( tokens );
		ParserRuleContext t = jp.compilationUnit();
		String st = t.toStringTree( jp );
		st.replace( "(compilationUnit", "" );
		st = st.substring( 0, st.length() - 1 );
		StringTokenizer tk = new StringTokenizer( st );
		while( tk.hasMoreTokens() ) {
			String token = tk.nextToken();
			if( token.startsWith( "\"" ) && token.endsWith( "\"" ) )
				continue;
			String type = null;
			boolean found = false;
			JsonObject entity = this.allIndicatorsEvents.get( token );
			if( entity != null ) {
				found = true;
				type = entity.get( "expressionType" ).getAsString();
			}else {
				entity = allEvents.get( token );
				if( entity != null ) {
					found = true;
					type = entity.get( "expressionType" ).getAsString();
				}else if( token.startsWith( "siftvar" ) ) {
					found = true;
					type = "double";
				}else {
					// TODO : not sure about this one, need to check
					JsonObject cim = this.allCIM.get( token );
					if( cim != null ) {
						found = true;
						type = cim.get( "type" ).getAsString();
					} else if( Commons.modelMasterTable.containsKey( token ) ) {
						found = true;
						type = "data.frame";
					} else if( token.equals( "SIFTMATH" ) ) {
						found = true;
						type = "com.knowesis.sift.expression.SiftMath";
					} else if( existingExpressions != null ) { // Its not a CIM field also. It should be one of the Offer Related Expressions
						PExpressionEvaluator existingExpression = existingExpressions.get( token );
						if( existingExpression != null ) {
							found = true;
							type = existingExpression.getOptionalExpressionType().toString();
						}
					} 
				}
			}
			// EXPRESSION REFERING TO SELF- VALIDATION

			// {
			// "topup_target" : { "type" : "int", "value" : "10" }
			// }

			if( ! found ) {
				if( token.equals( expId ) ) {
					found = true;
					type = expType;
				}else {
					if( dynamicParameters != null ) {
						JsonElement matchingDynParam = dynamicParameters.get( token );
						if( matchingDynParam != null ) {
							found = true;
							type = matchingDynParam.getAsJsonObject().get( "type" ).getAsString();
						}
					}
				}
			}

			if( found ) {
				if( paramNames.length() > 0 ) {
					String[ ] arrParamNames = paramNames.split( "," );
					boolean added = false;
					for( int j = 0; j < arrParamNames.length; j++ ) {
						if( arrParamNames[ j ].equals( token ) ) {
							added = true;
							break;
						}
					}
					if( added )
						continue;
					paramNames += "," + token;
				}else
					paramNames += token;

				if( paramTypes.length() > 0 )
					paramTypes += "~" + type;
				else
					paramTypes += type;
			}
		}

		String pattern = "\\s*\\S*\\s*(SIFTMATH.evaluateRScript\\s*\\()\\s*(\\S*)(\\s*\\))";
		Pattern p = Pattern.compile( pattern );
		Matcher matcher = p.matcher( strExpression );
		boolean matches = matcher.find();
		if( matches ) {
			System.out.println( "Evaluate R Model Pattern Matched" );
			String fileName = matcher.group( 2 );
			try {
				String strScriptDef = readFile( fileName );
				JsonObject scriptDef = ( JsonObject ) new JsonParser().parse( strScriptDef );
				JsonArray arrParamNames = scriptDef.get( "siftParamMap" ).getAsJsonArray();
				JsonArray arrParamTypes = scriptDef.get( "paramTypes" ).getAsJsonArray();
				for( int k = 0; k < arrParamNames.size(); k++ ) {
					String thistok = arrParamNames.get( k ).getAsString();
					String thisType = arrParamTypes.get( k ).getAsString();
					JsonObject entity = allIndicators.get( thistok );
					if( entity == null ) {
						if( Commons.modelMasterTable.containsKey( thistok ) ) {
	    					if( paramNames != null && paramNames.length() > 0 ) {
	    						if( ! exactMatchFound( paramNames, thistok ) ) {
	    						    paramNames += "," + thistok;
	    						    paramTypes += "~" + thisType;
	    						}
	    					}else {
	    						paramNames = thistok;
	    						paramTypes = thisType;
	    					}
						} else {
    						//this is an expression - get all the parameters
    						Object[ ] expResult = inferParamNamesTypes( thistok, thisType, thistok, null );
    						String[] paramsInExpression = ( String[ ] ) expResult[ 0 ];
    						Class[] paramTypesInExpression = ( Class[] ) expResult[ 1 ];
    						for( int x=0; x<paramsInExpression.length; x++ ) {
    							String tokInExp =  paramsInExpression[ x ];
    							String typeInExp = paramTypesInExpression[ x ].getName();
            					if( paramNames != null && paramNames.length() > 0 ) {
            						if( ! exactMatchFound( paramNames, tokInExp ) ) {
            						    paramNames += "," + tokInExp;
            						    paramTypes += "~" + typeInExp;
            						}
            					}else {
            						paramNames = tokInExp;
            						paramTypes = typeInExp;
            					}
    						}
    					} 
    				} else {	
    					if( paramNames != null && paramNames.length() > 0 ) {
    						if( ! exactMatchFound( paramNames, thistok ) ) {
    						    paramNames += "," + thistok;
    						    paramTypes += "~" + thisType;
    						}
    					}else {
    						paramNames = thistok;
    						paramTypes = thisType;
    					}
					}
				}
			}catch( IOException e ) {
				throw new PExpressionException( e.toString() );
			}
		}

		String[ ] allParamNames = ExpressionUtil.explode( paramNames );
		@SuppressWarnings( "rawtypes" )
		Class[ ] allParamTypes = ExpressionUtil.stringToTypes( paramTypes );

		Object[ ] result = new Object[ 2 ];
		result[ 0 ] = allParamNames;
		result[ 1 ] = allParamTypes;

		return result;
	}

	
	static boolean exactMatchFound( String existingString, String matchString ) {
		String[ ] strArrParamNames = existingString.split( "," );
		for( int m = 0; m < strArrParamNames.length; m++ ) {
			if( strArrParamNames[ m ].equals( matchString ) )
				return true;	
		}
		return false;
	}
	
	/**
	 * Constructors
	 * 
	 * @param allIndicators
	 * @param allEvents
	 * @param allFunctions
	 * @param allCIM
	 * @throws ParseException 
	 * @throws PExpressionException 
	 */
	public ExpressionProcessor( LinkedHashMap< String, JsonObject > allIndicators, LinkedHashMap< String, JsonObject > allEvents,
			LinkedHashMap< String, JsonObject > allFunctions, LinkedHashMap< String, JsonObject > allCIM ) throws PExpressionException, ParseException{
		this.allEvents = allEvents;
		this.allIndicators = allIndicators;
		this.allFunctions = allFunctions;
		this.allCIM = allCIM;

		allIndicatorsEvents = ( LinkedHashMap< String, JsonObject > ) allIndicators.clone();
		String eventType = "";

		if( allEvents != null ) {
			Iterator< JsonObject > itr = allEvents.values().iterator();
			while( itr.hasNext() ) {
				JsonObject eveObj = itr.next();

				if( eveObj.has( "type" ) && !eveObj.get( "type" ).isJsonNull() ) {
					eventType = eveObj.get( "type" ).getAsString();
				}

				if( eventType.equals( "" ) || eventType.equals( "Behavioural" ) || eventType.equals( "NonBehavioural" ) ) {
					allIndicatorsEvents.put( eveObj.get( "id" ).getAsString(), eveObj );
				}
			}
		}
		
		/*Gson gson = new Gson();
		Iterator< Entry< String, JsonObject >> funcItr = allFunctions.entrySet().iterator();
		HashMap< String, PExpressionEvaluator > allFuncExpressions = new HashMap< String, PExpressionEvaluator >();
		while( funcItr.hasNext() ) {
			Entry< String, JsonObject > entry = funcItr.next();
			JsonObject funcDef = entry.getValue().getAsJsonObject();
			System.out.println( "Compiling Function : " + funcDef.get( "name" ) + " -- > " + funcDef.get( "expression" ) );
			funcDef.addProperty( "expressionType", funcDef.get( "returnType" ).getAsString() );
			funcDef.addProperty( "userDefinedFunction", true );
			PExpressionEvaluator thisExp = createByDefinition( funcDef );
			String params = gson.toJson( thisExp.getParameterNames() );
			funcDef.addProperty( "parameters", params );
			PandaCacheConfig cache = new PandaCacheConfig();
			cache.getClient().set( entry.getKey(), 0, funcDef.toString() );
			System.out.println( "Setting Parameters to : " + entry.getKey() + " - " + params );
			if( thisExp != null ) 
				allFuncExpressions.put( funcDef.get( "id" ).getAsString(), thisExp );
		}
		Commons.expMap = allFuncExpressions;*/
	}

	/**
	 * 
	 * @return all indicators.
	 */
	public LinkedHashMap< String, JsonObject > getAllIndicators() {
		return allIndicators;
	}

	/**
	 * Set all indicators.
	 * 
	 * @param allIndicators
	 */
	public void setAllIndicators( LinkedHashMap< String, JsonObject > allIndicators ) {
		this.allIndicators = allIndicators;
	}

	/**
	 * 
	 * @return all events.
	 */
	public LinkedHashMap< String, JsonObject > getAllEvents() {
		return allEvents;
	}

	/**
	 * Set all events.
	 * 
	 * @param allEvents
	 */
	public void setAllEvents( LinkedHashMap< String, JsonObject > allEvents ) {
		this.allEvents = allEvents;
	}

	/**
	 * 
	 * @return all functions.
	 */
	public LinkedHashMap< String, JsonObject > getAllFunctions() {
		return allFunctions;
	}

	/**
	 * Set all functions.
	 * 
	 * @param allFunctions
	 *            to be set.
	 */
	public void setAllFunctions( LinkedHashMap< String, JsonObject > allFunctions ) {
		this.allFunctions = allFunctions;
	}

	/**
	 * 
	 * @return all CIM definitions. (field, type, alias,category).
	 */
	public LinkedHashMap< String, JsonObject > getAllCIM() {
		return allCIM;
	}

	/**
	 * Set all CIM definitions.
	 * 
	 * @param allCIM
	 */
	public void setAllCIM( LinkedHashMap< String, JsonObject > allCIM ) {
		this.allCIM = allCIM;
	}

	static String readFile( String fileName ) throws IOException {
		fileName = fileName.replace( "\"", "" );
		BufferedReader br = new BufferedReader( new FileReader( fileName ) );
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while( line != null ) {
				sb.append( line );
				sb.append( "\n" );
				line = br.readLine();
			}
			return sb.toString();
		}finally {
			br.close();
		}
	}

	/**
	 * 
	 * @param strExpression
	 *            expression to be preprocessed.
	 * @return modified expression.
	 */
	protected String preprocessExpression( String strExpression ) {
		for( int i = 0; i < SiftMath.functions.length; i++ ) {
			String pattern = SiftMath.functions[ i ] + "\\s*\\(";
			strExpression = strExpression.replaceAll( pattern, " SIFTMATH." + SiftMath.functions[ i ] + "(" );
		}
		if( allFunctions != null && !allFunctions.isEmpty() ) {
			Iterator< Entry< String, JsonObject >> itr = allFunctions.entrySet().iterator();

			while( itr.hasNext() ) {
				Entry< String, JsonObject > entry = itr.next();
				String funcId = entry.getKey();
				JsonObject funcDesc = entry.getValue().getAsJsonObject();
				String pattern = funcId + "\\s*\\(";
				Pattern p = Pattern.compile( pattern );
				Matcher matcher = p.matcher( strExpression );
				boolean matches = matcher.find();
				if( matches ) {
					JsonElement expressionType = funcDesc.get( "expressionType" );
					if( expressionType != null ) {
						String retType = expressionType.getAsString();
						String paramNames = funcDesc.get( "paramNames" ).getAsString();
						String paramTypes = funcDesc.get( "paramTypes" ).getAsString();
						String endTok = "\", ";
						if( paramNames.isEmpty() && paramTypes.isEmpty() )
							endTok = "\" ";
						if( retType.equals( "Integer" ) )
							strExpression = strExpression.replaceAll( pattern, " SIFTMATH.execIntFunction(" + "\""
									+ funcId + endTok );
						else if( retType.equals( "Long" ) )
							strExpression = strExpression.replaceAll( pattern, " SIFTMATH.execLongFunction(" + "\""
									+ funcId + endTok );
						else if( retType.equals( "Double" ) )
							strExpression = strExpression.replaceAll( pattern, " SIFTMATH.execDoubleFunction("
									+ "\"" + funcId + endTok ); 
						else if( retType.equals( "Boolean" ) )
							strExpression = strExpression.replaceAll( pattern, " SIFTMATH.execBooleanFunction("
									+ "\"" + funcId + endTok );
						else if( retType.equals( "Float" ) )
							strExpression = strExpression.replaceAll( pattern, " SIFTMATH.execFloatFunction("
									+ "\"" + funcId + endTok );
						else if( retType.equals( "String" ) )
							strExpression = strExpression.replaceAll( pattern, " SIFTMATH.execStringFunction("
									+ "\"" + funcId + endTok );
						else if( retType.equals( "JsonArray" ) )
							strExpression = strExpression.replaceAll( pattern, " SIFTMATH.execJsonArrayFunction("
									+ "\"" + funcId + endTok );
						else if( retType.equals( "JsonObject" ) )
							strExpression = strExpression.replaceAll( pattern, " SIFTMATH.execJsonObjectFunction("
									+ "\"" + funcId + endTok );
						else if( retType.equals( "Double[]" ) )
							strExpression = strExpression.replaceAll( pattern, " SIFTMATH.execDoubleArrayFunction("
									+ "\"" + funcId + endTok );
						else if( retType.equals( "String[]" ) )
							strExpression = strExpression.replaceAll( pattern, " SIFTMATH.execStringArrayFunction("
									+ "\"" + funcId + endTok );
						else if( retType.equals( "Integer[]" ) )
							strExpression = strExpression.replaceAll( pattern,
									" SIFTMATH.execIntegerArrayFunction(" + "\"" + funcId + endTok );
						else if( retType.equals( "Long[]" ) )
							strExpression = strExpression.replaceAll( pattern, " SIFTMATH.execLongArrayFunction("
									+ "\"" + funcId + endTok );
					}
				}
			}

		}else {
			System.out.println( "User defined functions either null or empty" );
		}
		return strExpression;
	}

	public LinkedHashMap< String, JsonObject > getUserDefinedFunctions() {
		return userDefinedFunctions;
	}

	public void setUserDefinedFunctions( LinkedHashMap< String, JsonObject > userDefinedFunctions ) {
		this.userDefinedFunctions = userDefinedFunctions;
	}
	
	protected PExpressionEvaluator compileExpression(String expression, String expressionType,
			JsonObject parametersObject, String paramNames, String paramTypes) throws CompileException {
		System.out.println( "Pre-processed expression Before: " + expression );
		expression = preprocessExpression(expression);
		System.out.println( "Pre-processed expression After: " + expression );
		String type = "expression";
		if (expression.contains(";"))
			type = "script";
		PExpressionEvaluator expr = new PExpressionEvaluator(type);

		try {
			expr.setOptionalDefaultImports(new String[] { "com.google.gson.*", "com.ibm.streams.operator.*",
					"com.knowesis.sift.expression.*" });
			expr.setExpression(expression);
			expr.setExpressionType(ExpressionUtil.stringToType(expressionType));
			expr.setName("thisExpression");
			expr.setParameters(ExpressionUtil.explode(paramNames), ExpressionUtil.stringToTypes(paramTypes));
			expr.cook(expr.getExpression());
		} catch (PExpressionException ex) {
			System.out.println(ex.getMessage());
		}

		System.out.println( "Compilation successful.." + expression );
		return expr;
	}

	public String[] executeExpression(String expression, String expressionType, JsonObject parametersObject)
			throws CompileException, NoSuchMethodException, InstantiationException, InvocationTargetException,
			IllegalAccessException, PExpressionException {

		Commons.expMap = existingExpressions;
		System.out.println( "Parameters object: " + parametersObject.toString() );
		String allParamNames = "myParam"; // dummy
		String allParamTypes = "int"; // dummy

		Object[] parameterValues = new Object[parametersObject.entrySet().size() + 2];
		String[] output = new String[ 2 ];

		Iterator<Entry<String, JsonElement>> iterator = parametersObject.entrySet().iterator();
		parameterValues[0] = 0;
		int i = 1;
		while( iterator.hasNext() ) {
			Entry< String, JsonElement > thisEntry = iterator.next();
			String thisParamName = thisEntry.getKey();
			JsonObject thisParamObject = thisEntry.getValue().getAsJsonObject();
			String thisParamType = thisParamObject.get( "type" ).getAsString();

			allParamNames += "," + thisParamName;
			allParamTypes += "~" + thisParamType;
			
			parameterValues[ i ] = ExpressionUtil.createObject( ExpressionUtil.stringToType( thisParamType ), ( Object ) thisParamObject.get( "value" ) );
			
			System.out.println( "Param Name: " + thisParamName );
			System.out.println( "Param Value: " + parameterValues[ i ].toString() );
			
			i ++;
		}

		SiftMath siftmath = new SiftMath( new CustomerState() );
		allParamNames += "," + "SIFTMATH";
		allParamTypes += "~" + "com.knowesis.sift.expression.SiftMath";
		parameterValues[ i ] = ( Object ) siftmath;
		
		PExpressionEvaluator thisExp = compileExpression(expression, expressionType, parametersObject, allParamNames,
				allParamTypes);
		Object result = null;
		long startTime = 0;
		long endTime = 0;
		double totalTime = 0;
		
		try {
			startTime = System.currentTimeMillis();
			result = thisExp.evaluate(parameterValues);
			endTime = System.currentTimeMillis();
			totalTime = endTime - startTime;
			output[ 1 ] = Double.toString( totalTime / 1000 );
		} catch (Exception e) {
			System.out.println("#### Exception While Executing >> ");
			e.printStackTrace();
			String exceptionType = e.getCause().getClass().getCanonicalName();
			output[ 0 ] = exceptionType;
			output[ 1 ] = "0";
			return output;
		}

		if (result != null)
			output[ 0 ] =  result.toString();
		else {
			System.out.println( "Returning null in Execute Expression.." );
			output[ 0 ] =  null;
		}
		return output;
	}
	
	public static void main( String[] args ) throws CompileException, NoSuchMethodException, InstantiationException, InvocationTargetException, IllegalAccessException, PExpressionException, ParseException {
		String exp = "return a + b.get( \"a\" ).getAsInt() + getNumberOfSiftInstances();";
		
		
		JsonObject abc = new JsonObject();
		JsonObject f1 = new JsonObject();
		f1.addProperty( "type", "int" );
		f1.addProperty( "value", 212 );

		JsonObject f2 = new JsonObject();
		f2.addProperty( "type", "JsonObject" );
		JsonObject test = new JsonObject();
		test.addProperty( "a", 180 );
		f2.add( "value", test );
		
		abc.add( "a", f1 );
		abc.add( "b", f2 );
		
		ExpressionProcessor thisObj = new ExpressionProcessor(new LinkedHashMap< String, JsonObject >(), 
																new LinkedHashMap< String, JsonObject >(),
																new LinkedHashMap< String, JsonObject >(),
																new LinkedHashMap< String, JsonObject >());
		
		System.out.println("Value of evaluation: " + thisObj.executeExpression( exp, "int", abc )[ 0 ] );
	}
}
