package com.knowesis.sift.expression;

public class SiftMandatoryFieldException extends PExpressionException {
	public SiftMandatoryFieldException( String exp ){
		super( exp );
	}
}