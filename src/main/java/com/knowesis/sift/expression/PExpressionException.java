package com.knowesis.sift.expression;

public class PExpressionException extends Exception {
	public PExpressionException( String exp ){
		super( exp );
	}
}
