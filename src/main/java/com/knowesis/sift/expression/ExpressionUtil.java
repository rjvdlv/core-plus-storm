package com.knowesis.sift.expression;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import org.renjin.sexp.ListVector;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.CustomerState;


public class ExpressionUtil {
	public static String[ ] explode( String s ) {
		StringTokenizer st = new StringTokenizer( s, "," );
		List l = new ArrayList();
		while( st.hasMoreTokens() )
			l.add( st.nextToken() );
		return ( String[ ] ) l.toArray( new String[ l.size() ] );
	}

	public static Class stringToType( String s ) throws PExpressionException {
		// int brackets = 0;
		// while( s.endsWith( "[]" ) ) {
		// ++brackets;
		// s = s.substring( 0, s.length() - 2 );
		// }
		//
		// if( brackets == 0 ) {
		// // "Class.forName("C")" does not work.
		if( s.equals( "double" ) )
			return double.class;
		else if( s.equals( "boolean" ) )
			return boolean.class;
		else if( s.equals( "long" ) )
			return long.class;
		else if( s.equals( "int" ) )
			return int.class;
		else if( s.equals( "void" ) )
			return void.class;
		else if( s.equals( "char" ) )
			return char.class;
		else if( s.equals( "byte" ) )
			return byte.class;
		else if( s.equals( "short" ) )
			return short.class;
		else if( s.equals( "float" ) )
			return float.class;
		else if( s.equals( "double[]" ) )
			return double[ ].class;
		else if( s.contains( "String[]" ) )
			return new String[0].getClass();
		else if( s.contains( "java.util.HashMap" ))
			return new HashMap().getClass();
		else if( s.contains( "JsonArray" ))
			return new com.google.gson.JsonArray().getClass();
		else if( s.contains( "String" ) )
			return new String().getClass();
		else if( s.contains( "JsonObject" ))
			return new com.google.gson.JsonObject().getClass();
		else if( s.equals( "Integer" ) )
			return Integer.class;
		else if( s.equals( "Long" ) )
			return Long.class;
		else if( s.equals( "Double" ) )
			return Double.class;
		else if( s.equals( "Boolean" ) )
			return Boolean.class;
		else if( s.equals( "Float" ) )
			return Float.class;
		else if( s.equals( "Double[]" ) )
			return Double[].class;
		else if( s.equals( "Integer[]" ) )
			return Integer[].class;
		else if( s.equals( "Long[]" ) )
			return Long[].class;
		else if( s.equals(  "data.frame" ) )
			return ListVector.class;
		// }
		//
		// // Automagically convert primitive type names.
		// if( s.equals( "void" ) ) {
		// s = "V";
		// }else if( s.equals( "boolean" ) ) {
		// s = "Z";
		// }else if( s.equals( "char" ) ) {
		// s = "C";
		// }else if( s.equals( "byte" ) ) {
		// s = "B";
		// }else if( s.equals( "short" ) ) {
		// s = "S";
		// }else if( s.equals( "int" ) ) {
		// s = "I";
		// }else if( s.equals( "long" ) ) {
		// s = "J";
		// }else if( s.equals( "float" ) ) {
		// s = "F";
		// }else if( s.equals( "double" ) ) {
		// s = "D";
		// }
		//
		// while( --brackets >= 0 )
		// s = '[' + s;
		try {
			return Class.forName( s );
		}catch( ClassNotFoundException ex ) {
			ex.printStackTrace();
			throw new PExpressionException( ex.toString() );
		}
	}

	public static Class[ ] stringToTypes( String s ) throws PExpressionException {
		StringTokenizer st = new StringTokenizer( s, "~" );
		List l = new ArrayList();
		while( st.hasMoreTokens() )
			l.add( stringToType( st.nextToken() ) );
		Class[ ] res = new Class[ l.size() ];
		l.toArray( res );
		return res;
	}

	// public static Object createObject( Class type, String value ) throws
	// NoSuchMethodException, InstantiationException,
	// InvocationTargetException, IllegalAccessException {
	//
	// // Wrap primitive parameters.
	// if( type.isPrimitive() ) {
	// type = ( type == double.class ? Double.class : type == boolean.class ?
	// Boolean.class
	// : type == long.class ? Long.class : type == int.class ? Integer.class
	// : type == byte.class ? Byte.class : type == short.class ? Short.class
	// : type == float.class ? Float.class : type == char.class ?
	// Character.class
	// : void.class );
	// }
	//
	// // Construct object, assuming it has a default constructor or a
	// // constructor with one single "String" argument.
	// if( value.equals( "" ) ) {
	// return type.getConstructor( new Class[ 0 ] ).newInstance( new Object[ 0 ]
	// );
	// }else {
	// return type.getConstructor( new Class[] { String.class } ).newInstance(
	// new Object[] { value } );
	// }
	// }

	public static Object createObject( Class type, Object jvalue ) throws NoSuchMethodException, InstantiationException, InvocationTargetException,
	IllegalAccessException {

		// if( jvalue == null ) {
		// System.out.println( "Creating Object : " + type.getName() +
		// " -- null" );
		// } else
		// System.out.println( "Creating Object : " + type.getName() + " -- " +
		// jvalue.toString() );

		if( type.isPrimitive() ) {
			type = ( type == double.class ? Double.class : type == boolean.class ? Boolean.class : type == long.class ? Long.class
					: type == int.class ? Integer.class : type == byte.class ? Byte.class : type == short.class ? Short.class
							: type == float.class ? Float.class : type == char.class ? Character.class : void.class );
//			if( jvalue == null ) {
//				return type.getConstructor( new Class[] { String.class } ).newInstance( new Object[] { "0" } );
//			}else {
//				return type.getConstructor( new Class[] { String.class } ).newInstance( new Object[] { ( ( JsonElement ) jvalue ).getAsString() } );
			}
//		}else 
		if( type == String.class ) {
			if( jvalue == null ) {
				return new String();
			}else {
				if( jvalue.getClass() == String.class )
					return ( String ) jvalue;

				return ( ( JsonElement ) jvalue ).getAsString();
			}
		}else if( type == Double.class ) {
			if( jvalue == null ) {
				return new Double( 0 );
			}else {
				if( jvalue.getClass() == Double.class )
					return ( Double ) jvalue;
				return ( ( JsonElement ) jvalue ).getAsDouble();
			}
		} else if( type == Long.class ) {
			if( jvalue == null ) {
				return new Long( 0 );
			}else {
				if( jvalue.getClass() == Long.class )
					return ( Long ) jvalue;
				return ( ( JsonElement ) jvalue ).getAsLong();
			}
		} else if( type == Integer.class ) {
			if( jvalue == null ) {
				return new Integer( 0 );
			}else {
				if( jvalue.getClass() == Integer.class )
					return ( Integer ) jvalue;
				return ( ( JsonElement ) jvalue ).getAsInt();
			}
		}else if( type == Float.class ) {
			if( jvalue == null ) {
				return new Float( 0 );
			}else {
				if( jvalue.getClass() == Float.class )
					return ( Float ) jvalue;
				return ( ( JsonElement ) jvalue ).getAsFloat();
			}
		}else if( type == Boolean.class ) {
			if( jvalue == null ) {
				return new Boolean( false );
			}else {
				if( jvalue.getClass() == Boolean.class )
					return ( Boolean ) jvalue;
				return ( ( JsonElement ) jvalue ).getAsBoolean();
			}
		}else if( type == JsonArray.class ) {
			if( jvalue == null ) {
				return new JsonArray();
			}else {
				if( jvalue.getClass() == JsonArray.class )
					return ( JsonArray ) jvalue;
				return ( ( JsonElement ) jvalue ).getAsJsonArray();
			}
		} else if( type == HashMap.class )
			return ( HashMap< String, JsonObject > ) jvalue;
		else if( type == JsonObject.class ) {
			if( jvalue == null ) {
				return new JsonObject();
			}else {
				return ( JsonObject ) jvalue;
			}
		}else if( type == double[ ].class ) {
			if( jvalue == null ) {
				return new double[] {};
			}else {
				if( jvalue.getClass() == double[ ].class )
					return ( double[] ) jvalue;

				JsonArray jArr = ( ( JsonElement ) jvalue ).getAsJsonArray();
				double[ ] res = new double[ jArr.size() ];
				for( int i = 0; i < jArr.size(); i++ ) {
					res[ i ] = jArr.get( i ).getAsDouble();
				}
				return res;
			}
		} else if( type == String[ ].class ) {
			if( jvalue == null ) {
				return new String [] {};
			}else {
				if( jvalue.getClass() == String[ ].class )
					return ( String[] ) jvalue;

				JsonArray jArr = ( ( JsonElement ) jvalue ).getAsJsonArray();
				String[ ] res = new String[ jArr.size() ];
				for( int i = 0; i < jArr.size(); i++ ) {
					res[ i ] = jArr.get( i ).getAsString();
				}
				return res;
			}
		} else if( type == SiftMath.class ) {
			if( jvalue == null ) 
				return new SiftMath( new CustomerState() );
			else 
				return ( SiftMath ) jvalue;
		} else if( type == ListVector.class ) {
			if( jvalue == null ) 
				return new ListVector();
			else 
				return ( ListVector ) jvalue;
		}
		throw new InstantiationException();
	}

	

	public static Object evaluateExpression( CustomerState customerState, String id, // cim name
			String expressionId, // expression name
			java.util.HashMap< String, String > rawValMap, // in case of cim -  1-1 mapped data
			JsonObject evalCIMMap, // already evaluated cims
			java.util.HashMap< String, JsonObject > allCIMNamesMap, // all cim names available insift
			JsonObject subscriberIndicators, // indicator object
			java.util.HashMap< String, JsonObject > indicatorDefs, // all indicator defs
			java.util.HashMap< String, PExpressionEvaluator > expDef, // all exp defs
			JsonObject allCIMMapping ) throws Exception // cim  mapping used in transformer
	{
		Commons.logger.info( Commons.logPrefix + "inside evalueExpression for : " + expressionId);
		PExpressionEvaluator exp = expDef.get( expressionId );
		String[ ] paramNames = exp.getParameterNames();
		Class[ ] paramTypes = exp.getParameterTypes();
		Object[ ] parameterValues = new Object[ paramNames.length ];

		for( int j = 0; j < paramNames.length; j++ ) {
			String thisParam = paramNames[ j ];
			Object jValue = null;
			//System.out.println("inside for loop for : " + thisParam);
			if( thisParam.equals( "SIFTMATH" ) ) 
				jValue = customerState.siftMath;
			else if( allCIMNamesMap.get( thisParam ) != null ) {
				//System.out.println("This is a CIM " + thisParam );
				if( evalCIMMap.get( thisParam ) != null ) {
					jValue = new JsonPrimitive ( evalCIMMap.get( thisParam ).getAsString() );
				} else if( rawValMap.get( thisParam ) == null )
					jValue = null;
				else if( ! id.equals( thisParam ) ) {
					JsonElement expression = allCIMMapping.get( thisParam ).getAsJsonObject().get( "expression" );
					if( expression == null || expression.getAsString().trim().length() == 0 ) {
						jValue = new JsonPrimitive( rawValMap.get( thisParam ) );
					} else {
						String thisParamExpressionId = allCIMMapping.get( thisParam ).getAsJsonObject().get( "id" ).getAsString();
						Object result = evaluateExpression( customerState, thisParam, thisParamExpressionId, rawValMap, evalCIMMap, allCIMNamesMap, subscriberIndicators,
								indicatorDefs, expDef, allCIMMapping );
						jValue = new JsonPrimitive( result.toString() );
						evalCIMMap.addProperty( thisParam, result.toString() );
					}	
					//System.out.println("jvalue for this param : " + thisParam + " is :" + jValue.toString());
				}else
					jValue = new JsonPrimitive( rawValMap.get( id ).toString() );
			}else if( jValue == null ) {
				JsonObject paramIndDef = indicatorDefs.get( thisParam );
				if( paramIndDef != null ) {
					String frequency = paramIndDef.get( "frequency" ).getAsString();
					String paramIndKey = generatekey( null, thisParam, frequency, null, Calendar.getInstance(), null );
					jValue = subscriberIndicators.get( paramIndKey );
				}
			}else
				jValue = null; // TODO: default values
			//System.out.println("Creating Object for : " + thisParam);
			//System.out.println("Data Type of " + thisParam + " is " + jValue.getClass() + " and Value is " + jValue.toString());
			parameterValues[ j ] = createObject( paramTypes[ j ], jValue );
			//System.out.println("Object Created for : " + thisParam);
		}
		return exp.evaluate( parameterValues );
	}
	

	protected static String generatekey( String eventId, String key, String frequency, java.util.Date callDate, Calendar cal, JsonObject aTag )
			throws ParseException {
		if( frequency == null )
			frequency = "Yearlgeneratekey(y";
		String temporalId = null;
		long thisYear = cal.get( Calendar.YEAR ) - 2000;

		Long numParam;

		if( frequency.equals( "Daily" ) ) {
			numParam = new Long( cal.get( Calendar.DAY_OF_YEAR ) );
			temporalId = numParam.toString() + "D" + thisYear;
		}else if( frequency.equals( "Weekly" ) ) {
			numParam = new Long( cal.get( Calendar.WEEK_OF_YEAR ) );
			temporalId = numParam.toString() + "W" + thisYear;
		}else if( frequency.equals( "Monthly" ) ) {
			numParam = new Long( cal.get( Calendar.MONTH ) );
			temporalId = numParam.toString() + "M" + thisYear;
		}else if( frequency.equals( "Yearly" ) ) {
			numParam = thisYear;
			temporalId = thisYear + "Y";
		}else if( frequency.equals( "CustomTime" ) ) {
			if( aTag != null ) {
				numParam = aTag.get( "MONITORING_START_TIME" ).getAsLong();
				// long endDate = taggedInd.getEndDate();
				// int duration = taggedInd.getDuration();
				temporalId = eventId + "-" + numParam.toString();
			}else
				temporalId = eventId;
		}else if( frequency.equals( "TriggerTime" ) ) {
			temporalId = eventId;
		}else if( frequency.equals( "Hourly" ) ) {
			numParam = new Long( cal.get( Calendar.HOUR_OF_DAY ) );// get the hour from date
			temporalId = numParam + "H";
		}else if( frequency.equals( "LifeTimeHourly" ) ) {
			numParam = new Long( cal.get( Calendar.HOUR_OF_DAY ) );// get the hour from date
			temporalId = numParam + "H";
		}

		if( frequency.equals( "LifeTime" ) )
			key += "-L";
		else
			key += "-" + temporalId;
		return key;
	}
}
