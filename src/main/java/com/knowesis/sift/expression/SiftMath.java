/**
 * @author Raja SP
 * 
 */
package com.knowesis.sift.expression; 
 
//D Version

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.sax.SAXSource;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.apache.commons.math3.stat.descriptive.rank.Max;
import org.apache.commons.math3.stat.descriptive.rank.Median;
import org.apache.commons.math3.stat.descriptive.rank.Min;
import org.apache.commons.math3.stat.descriptive.summary.Sum;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.codehaus.commons.compiler.CompileException;
import org.dmg.pmml.FieldName;
import org.dmg.pmml.ObjectFactory;
import org.dmg.pmml.PMML;
import org.joda.time.LocalDate;
import org.jpmml.evaluator.Evaluator;
import org.jpmml.evaluator.ModelEvaluatorFactory;
import org.jpmml.manager.PMMLManager;
import org.jpmml.model.ImportFilter;
import org.renjin.sexp.SEXP;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.knowesis.sift.Common.Commons;
import com.knowesis.sift.Common.CustomerState;
import com.knowesis.sift.Common.PandaCache;
import com.knowesis.sift.Common.PandaCacheConfig;

public class SiftMath {

	public static final long msecPerDay = 86400000L;
	public static final double dmsecPerDay = 86400000.00;

	public static double dblPi = 3.14159265;
	public static double dbl2Pi = 2 * dblPi;

	public static long siftStartDate;

	public static HashMap< String, String > rscriptsMap;
	public static HashMap< String, JsonObject > rScriptDescriptorssMap;
	public static HashMap< String, String > filesMap;
	static ScriptEngineManager scriptEngineManager = null;
	static ScriptEngine scriptEngine = null;

	public static int DAYS_SINCE_DEFAULT = -1;

	public static JsonObject thisTag;

	public static int PERIOD_DEFAULT = 1000000;
	public static int QUANTITY_DEFAULT = 0;

	public static Node xmlNode;
	public static NodeList xmlNodeList;
	public static Element xmlElement;
	public static Document doc;
	public static XPathFactory xpathFactory = XPathFactory.newInstance();
	
	public static JsonObject tupleJsonData = null;

	public static HashMap< String, JsonObject > pmmlDefMap;
	private static JAXBContext jaxbCtx = null;
	private static java.util.HashMap< String, PMML > pmmlSrcMap = null;
	
	public CustomerState customerState;
	public Commons commonsState;
	private static HttpClient httpClient = null;

	public static String[ ] functions = { "standardDeviation", "mean", "sum", "min", "max", "count", "qualified", "inLocation", "hasProduct",
			"getCalendarFactor", "getQuotaDurationConsumedPercentage", "getQuotaDurationLeftPercentage", "getActivityIntervalAverage", "getIndicator",
			"daysSinceLastActivity", "idleDaysAboveValue", "getAverageLowBalanceBeforeTopup", "getNumberOfCellVisits", "getSupportingData",
			"getAverageQuotaConsumptionPercentage", "packageSubscriptionCount", "getActivityCalendarFactor", "getQuotaConsumptionPercentage", "getChurnScore",
			"getAgeOnNetwork", "evaluateScoringModel", "isFirstUsage", "IMEIChanged", "getTopCalledNumbers", "getTopCalledNumberMoU",
			"getDaysWithTopCalledNumbers", "inGeoFenceGroup", "getNumericSupportingData", "getQuotaDurationLeft", "getActivePackage", "evaluateRScript",
			"getSegments", "getKPIByBucket", "getTotalFromSeries", "getAverageFromSeries", "getTimeSinceLastTrigger", "getNumberOfTriggers",
			"getSeriesElement", "getSeriesElements", "getContactCountExceeded", "getNumerOfContactsByChannel", "timeSinceLastMonitoringStart",
			"insertIntoSeries", "getPackageRevenue", "getCreditExpiryDaysCount", "getLatestCreditValue", "getPreviousCreditValue", "getLatestBalance",
			"getLatestCreditMethod", "getDaysSinceLastCredit", "getDaysSinceLastBalanceUpdate", "housekeep", "isBirthDay", "getDaysGivenEvent", "quantile",
			"getIntervalInSeries", "getDaysSinceLastTrigger", "getDaysSinceLastReminderTrigger", "getDaysSinceCommercialOfferChange", "getRechargeCounts",
			"matchInFile", "getValueFromMultiSeries", "convertDateTimeToLong", "convertStringToDollars", "getBucketValue", "getRawField", "formatDate",
			"getCurrentRecordTimeBlock", "getWeekendValueFromMultiSeries", "getMostActiveTimeBlock", "getDaysSinceOfferTriggered", "getTopupFrequencyMatch",
			"getTopupFrequencyMatchForXMonths", "houseKeepByIndicatorNames", "getxmlElement", "getxmlElementList", "getAccumulatedMonthlyTopups",
			"getAccumulatedTopups", "getKeyFromKeyArray", "insertIntoKeyArray", "insertArrayIntoKeyArray", "getNumOfDaysForPackageExpiry", "getPackageCount",
			"getQuotaConsumedPercent", "getNumberOfTimesDAExceeded", "getUsageCount", "getDataUsage", "getAveragePackagePurchaseDates",
			"getCurrentPackageIdAndPrice", "getDAExceededDate", "getTelcoCounts", "isFulfillmentTriggered", "evaluatePMMLModel", "processIndicator",
			"processSummaryIndicator", "processSummaryCounter", "getSummaryCounter", "getCurrentTime", "getSiftware", "getSiftwareAttribute",
			"getLiveChartIndicator", "setLiveChartIndicator", "getSiftInstance", "getNumberOfSiftInstances", "getSummaryIndicator", "getCurrentDay",
			"deleteSummaryIndicator", "getFrequencyFromDateArray", "getDaPackageFrequency", "getPackageFrequency", "getIDDSum", "getDABalances",
			"getMonthlyDataPPUSum", "isLatestOfferControlled", "packageExclusionCheck", "getMonthlyDASum", "getQuotaRemaining", "getIDDBucketPeakDetails",
			"getOfferCountLegacy", "getIDDLastCountryCalled", "hasActivePackage", "getNthDeviceType", "getDeviceChangeCount", "getAppUsedDayCount",
			"getAreaTypeUsagePercent", "getNonTakenOffers", "getNumberOfContactsInCalenderMonth", "getDaysSinceSubscription", "deleteSubscriber",
			"getDaysToPackageExpiryExtended", "getUtilizationPercentage", "maxDailyARPU", "isARPUInDecrOrder", "getTimeSinceOfferTriggered", 
			"getMostFrequentRechargeValue", "houseKeepTriggerHistory", "houseKeepMapSeriesByElementCount", "getRawInputTuple", "getDailyOfferCount",
			"getAllSummaryIndicators", "getAttributeByName", "resetIndicators", "swapNumbers", "evaluateExpression", "getLastFulfilDate", "getNextContactTime",
			"getMonitoringEndtime", "httpPostRequest", "createMapping", "deleteMapping", "getMappedMsisdn","executeRModel" }; 

	
	public SiftMath( CustomerState custState ) {
		customerState = custState;
	}
	
	
	public String getRawInputTuple() {
		return customerState.tupleString;
	}
	
	
	public void deleteSubscriber( String msisdn ) {
		commonsState.subscriberIndicatorsMap.remove( msisdn );
		Commons.pandaCache.getClient().delete( msisdn + "-Indicators" );
		commonsState.eventsTaggedForSubscribersMap.remove( msisdn );
		Commons.pandaCache.getClient().delete( "EventTagList-" + msisdn );	
	}
	
	public void resetIndicators( String msisdn ) {
		JsonObject newIndObj = new JsonObject();
		newIndObj.addProperty("docType", "subscriberIndicators");
		commonsState.subscriberIndicatorsMap.put(msisdn, newIndObj);
		customerState.subscriberIndicators=newIndObj;
		commonsState.eventsTaggedForSubscribersMap.remove( msisdn );
		Commons.pandaCache.getClient().delete( "EventTagList-" + msisdn );	
	}
	
	
	public void swapNumbers( String msisdn, String newMsisdn, int expiryInDays ) {
		JsonObject oldMsisdnObj = commonsState.subscriberIndicatorsMap.get(msisdn);
		if ( oldMsisdnObj == null ) {
			Object tmpOldMsisdnObj = Commons.pandaCache.get( msisdn + "-Indicators");
			if ( tmpOldMsisdnObj != null )
				oldMsisdnObj = (JsonObject)Commons.parser.parse(tmpOldMsisdnObj.toString());
			else {
				oldMsisdnObj = new JsonObject();
				oldMsisdnObj.addProperty("docType", "subscriberIndicators");
			}
		}
		if ( oldMsisdnObj.entrySet().size() == 1 )
			return;
		customerState.subscriberIndicators=oldMsisdnObj;
		Commons.pandaCache.set( newMsisdn + "-Indicators", expiryInDays*86400 ,oldMsisdnObj.toString() );
		commonsState.subscriberIndicatorsMap.put(newMsisdn, oldMsisdnObj);

		commonsState.subscriberIndicatorsMap.remove( msisdn );
		Commons.pandaCache.getClient().delete( msisdn + "-Indicators" );


		JsonObject oldMsisdnTagListObj = null;
		Object tmpOldMsisdnObj = Commons.pandaCache.get( "EventTagList-" + msisdn) ;
		if ( tmpOldMsisdnObj != null )
			oldMsisdnTagListObj = (JsonObject)Commons.parser.parse(tmpOldMsisdnObj.toString());
		else {
			oldMsisdnTagListObj = new JsonObject();
			oldMsisdnTagListObj.addProperty("docType", "TagList");
		}
		if ( oldMsisdnTagListObj.entrySet().size() == 1 )
			return;
		
		Commons.pandaCache.set( "EventTagList-" + newMsisdn, expiryInDays*86400 ,oldMsisdnTagListObj.toString() );
		
		oldMsisdnTagListObj.remove( "docType" );
		commonsState.eventsTaggedForSubscribersMap.put(newMsisdn , oldMsisdnTagListObj);
		customerState.subscriberEventTags = oldMsisdnTagListObj;

		commonsState.eventsTaggedForSubscribersMap.remove( msisdn );
		Commons.pandaCache.getClient().delete( "EventTagList-" + 	msisdn );
	}
	
	
	public  int getSiftInstance() {
		return commonsState.nWorkerId;
	}

	public  int getNumberOfSiftInstances() {
		return Commons.numberOfInstances;
	}

	public JsonObject getSummaryIndicator( String key ) {
		JsonParser parser = new JsonParser();
		JsonObject jOfferCount = commonsState.summaryIndicatorsMap.get( key + "-" + commonsState.nWorkerId );
		if( jOfferCount == null ) {
			String strOfferCount = ( String ) Commons.pandaCache.getClient().get( key + "-" + commonsState.nWorkerId );
			if( strOfferCount == null ) {
				jOfferCount = new JsonObject();
				jOfferCount.addProperty( "docType", "summaryContactCount" );
			}else {
				jOfferCount = ( JsonObject ) parser.parse( strOfferCount );
			}
			commonsState.summaryIndicatorsMap.put( key, jOfferCount );
		}
		return jOfferCount;
	}
	
	public JsonObject[] getAllSummaryIndicators( String key ) {
		JsonParser parser = new JsonParser();
		int numOfInstances = getNumberOfSiftInstances();
		JsonObject[] allSummaryObjects = new JsonObject[ numOfInstances ];
		for( int i = 1; i <= numOfInstances; i ++ ) {
			JsonObject jOfferCount = commonsState.summaryIndicatorsMap.get( key + "-" + i );
			if( jOfferCount == null ) {
				String strOfferCount = ( String ) Commons.pandaCache.getClient().get( key + "-" + i );
				if( strOfferCount != null ) {
					jOfferCount = ( JsonObject ) parser.parse( strOfferCount );
				}
			}
			allSummaryObjects[ i - 1 ] = jOfferCount;
		}
		return allSummaryObjects;
	}
 
	public JsonObject getLiveChartIndicator( String key, String countType ) {
		JsonParser parser = new JsonParser();
		JsonObject jOfferCount = commonsState.summaryIndicatorsMap.get( key + "-" + commonsState.nWorkerId + "-" + countType );
		if( jOfferCount == null ) {
			String strOfferCount = ( String ) Commons.pandaCache.getClient().get( key + "-" + commonsState.nWorkerId + "-" + countType );
			if( strOfferCount == null ) {
				jOfferCount = new JsonObject();
				jOfferCount.addProperty( "docType", countType );
				jOfferCount.addProperty( "count", new Long( 0 ) );
				jOfferCount.addProperty( "count_control", new Long( 0 ) );
			}else {
				jOfferCount = ( JsonObject ) parser.parse( strOfferCount );
			}
			commonsState.summaryIndicatorsMap.put( key, jOfferCount );
		}
		return jOfferCount;
	}

	
	public void setLiveChartIndicator( String key, String countTyoe, JsonObject object ) {
		synchronized( this.getClass() ) {
			commonsState.summaryIndicatorsMap.put( key + "-" + commonsState.nWorkerId + "-" + countTyoe, object );
    		Commons.pandaCache.getClient().set( key + "-" + commonsState.nWorkerId + "-" + countTyoe, object.toString() );
		}
	}
	

	public  JsonObject getSiftware( String defType, String id ) {
		if( defType.equals( "Indicator" ) )
			return Commons.allIndDef.get( id );
		else if( defType.equals( "Event" ) )
			return Commons.allEveDef.get( id );
		else if( defType.equals( "Offer" ) )
			return Commons.allOfferDef.get( id );
		else if( defType.equals( "Program" ) )
			return Commons.allProgramDef.get( id );
		return null;
	}

	public  JsonElement getSiftwareAttribute( String defType, String id, String attibute ) {
		if( defType.equals( "Indicator" ) )
			return Commons.allIndDef.get( id ).get( attibute );
		else if( defType.equals( "Event" ) )
			return Commons.allEveDef.get( id ).get( attibute );
		else if( defType.equals( "Offer" ) )
			return Commons.allOfferDef.get( id ).get( attibute );
		else if( defType.equals( "Program" ) )
			return Commons.allProgramDef.get( id ).get( attibute );
		return null;
	}

	public Object evaluateExpression( String id ) throws NoSuchMethodException, InstantiationException, InvocationTargetException,
	IllegalAccessException, ParseException {
		try {
			PExpressionEvaluator exp = Commons.expMap.get( id );
			if( exp == null )
				return null;

			String[ ] paramNames = exp.getParameterNames();
			Class[ ] paramTypes = exp.getParameterTypes();
			Object[ ] parameterValues = new Object[ paramNames.length ];
			for( int j = 0; j < paramNames.length; j++ ) {
				String thisParam = paramNames[ j ];
				//System.out.println( "Inside SiftMath.evaluateExpression. evaluating : " + thisParam  );
				Object jValue = null;
				if( customerState.tupleMap != null && customerState.tupleMap.has( thisParam ) )
					jValue = new JsonPrimitive( customerState.tupleMap.get( thisParam ).getAsString() );
				else if( thisParam.equals( "SIFTMATH" ) ) 
					jValue = customerState.siftMath;
				if( jValue == null ) {// it must be one of the indicator
					// instances
					if( Commons.allIndDef.containsKey( thisParam ) ) {
//						String key = getIndicatorKey( thisParam );
//						jValue = customerState.subscriberIndicators.get( key );
						jValue = getValueFromIndicator( thisParam, customerState, null, null );
					}else
						return null;
				}
				//System.out.println( "Inside SiftMath.evaluateExpression.  " + thisParam + " == " + jValue.toString() );
				parameterValues[ j ] = ExpressionUtil.createObject( paramTypes[ j ], jValue );
			}
			Object expressionValue = null;
			try {
				expressionValue = exp.evaluate( parameterValues );
			}
			catch( Exception ex ) {
				ex.printStackTrace();
				System.out.println( "Exception found in the expression: " + id + " : " + customerState.tupleMap.toString() );
				throw new InvocationTargetException( null, ex.getMessage() );
			}
			return expressionValue;
		}catch( Exception e ) {
			e.printStackTrace();
			throw new IllegalAccessException();
		}
	}
	
	
	public double getDaPackageFrequency( String indicator, String DAID, String packageId, int start, int duration ) {
		JsonElement ele = customerState.subscriberIndicators.get( indicator );
		if( ele == null || ele.isJsonNull() || ele.toString().length() <= 2 )
			return 0;

		JsonObject packageObj = ele.getAsJsonObject();
		JsonElement daEle = packageObj.get( DAID );
		if( daEle == null || daEle.isJsonNull() )
			return 0;

		JsonObject daObj = daEle.getAsJsonObject();
		JsonElement rechargeDatesEle = daObj.get( "DARechargeDate" );
		if( rechargeDatesEle == null || rechargeDatesEle.isJsonNull() )
			return 0;

		JsonArray rechargeDateArray = rechargeDatesEle.getAsJsonArray();
		JsonArray pkgIdArray = daObj.get( "PKG_ID" ).getAsJsonArray();

		int end = duration - 1;
		if( ( start < 0 ) || ( end < 0 ) ) {
			return 0.0D;
		}
		long lStart = getCurrentTime() - start * 86400000L;
		// lStart -= lStart % 86400000L;
		long lEnd = ( lStart - ( lStart % 86400000L ) ) - end * 86400000L;
		double count = 0.0;

		for( int i = rechargeDateArray.size() - 1; i >= 0; i-- ) {
			if( rechargeDateArray.get( i ).getAsLong() > lStart )
				continue;

			if( rechargeDateArray.get( i ).getAsLong() < lEnd )
				return count;

			if( pkgIdArray.get( i ).getAsString().equalsIgnoreCase( packageId ) )
				count++;
		}
		return count;
	}

	public  double max( double d1, double d2 ) {
		if( d1 > d2 )
			return d1;
		return d2;
	}

	public static long getCurrentTime() {
		long currentTime = new java.util.Date().getTime();
		currentTime += java.util.TimeZone.getDefault().getOffset( currentTime );
		return currentTime;
	}

	public static long getCurrentDay() {
		long currentTime = getCurrentTime();
		return currentTime - ( currentTime % msecPerDay );
	}

	public double getDaysWithTopCalledNumbers( int limit, int percent, int startDay, int numDays ) {
		int dayPart = getActivityCalendarFactor( "DAY_OF_YEAR" );
		int yearPart = getActivityCalendarFactor( "YEAR" ) - 2000;
		// today = 210, startday = 0, 3, 5 numdays = 5, 5, 2
		// start = dayPart - (startDay), end = start - (numDays - 1)
		// start = 210, end = 206
		// start = 208, end = 203
		// start = 205, end = 204
		int result = 0, start = dayPart - startDay, end = start - ( numDays - 1 );
		for( int i = end; i <= start; i++ ) {
			String indKey = "CALLED_NUMBER_MoU_Daily-" + i + "D" + yearPart;
			JsonElement elem = customerState.subscriberIndicators.get( indKey );
			if( elem == null )
				continue;
			// System.out.println( "Working on : " + indKey + " --  value : " +
			// elem.toString() );
			JsonObject jObj = ( JsonObject ) elem;
			double topUsage = getTopCalledNumberMoU( limit, jObj );
			// System.out.println( "MoU From Top 3 numbers  : " + indKey +
			// " --  value : " + topUsage );
			String totalMoUKey = "VOICE_ALL_MoU_Daily-" + i + "D" + yearPart;
			JsonElement elemMoU = customerState.subscriberIndicators.get( totalMoUKey );
			if( elemMoU == null )
				continue;
			// System.out.println( "Total MoU of  : " + totalMoUKey +
			// " --  value : " + elemMoU.toString() );
			if( topUsage / elemMoU.getAsDouble() * 100 >= percent )
				result++;
		}
		return result;
	}

	public  double getTopCalledNumberMoU( int limit, JsonObject jobj ) {
		ArrayList< Double > sortedArr = new ArrayList< Double >();
		Set< Entry< String, JsonElement >> es = jobj.entrySet();
		Iterator< Entry< String, JsonElement >> itr = es.iterator();
		while( itr.hasNext() ) {
			Entry< String, JsonElement > elem = itr.next();
			double thisVal = elem.getValue().getAsDouble();
			if( sortedArr.size() == 0 )
				sortedArr.add( new Double( thisVal ) );
			else {
				int position = 0;
				for( int i = 0; i < sortedArr.size(); i++ ) {
					if( sortedArr.get( i ).doubleValue() > thisVal ) {
						position++;
						continue;
					}
				}
				if( position <= sortedArr.size() )
					sortedArr.add( position, thisVal );
				else
					sortedArr.add( thisVal );
			}
		}
		double retVal = 0;
		for( int i = 0; i < limit; i++ ) {
			if( i <= sortedArr.size() - 1 )
				retVal += sortedArr.get( i ).doubleValue();
		}
		return retVal;
	}

	public double getTopCalledNumberMoU( int limit ) {
		JsonElement jele = customerState.subscriberIndicators.get( "CALLED_NUMBER_MoU_LifeTime-L" );
		if( jele == null )
			return -1;
		else {
			JsonObject jobj = ( JsonObject ) jele;
			return getTopCalledNumberMoU( limit, jobj );
		}
	}

	public boolean isFirstUsage( String serviceType ) {
		JsonElement jelemStartDate = customerState.subscriberIndicators.get( "AccountStartDate" );
		if( jelemStartDate != null ) {
			// profile exists. Check if it is after Sift Start Date
			if( jelemStartDate.getAsLong() < siftStartDate )
				return false;
		}
		// profile does not exist or he started after Sift. So he is a new guy.
		// Check for this usage
		String key = null;
		if( serviceType.equals( "VOICE" ) )
			key = getIndicatorKey( "VOICE_ACTIVITY_TIME_SERIES_LifeTime" );
		else if( serviceType.equals( "DATA" ) )
			key = getIndicatorKey( "DATA_ACTIVITY_TIME_SERIES_LifeTime" );
		else if( serviceType.equals( "SMS" ) )
			key = getIndicatorKey( "SMS_ACTIVITY_TIME_SERIES_LifeTime" );
		else
			return false;

		JsonElement jeleVal = customerState.subscriberIndicators.get( key );
		if( jeleVal != null ) {
			JsonArray arrActivity = jeleVal.getAsJsonArray();
			if( arrActivity.size() == 1 ) {
				long dayActivity = arrActivity.get( 0 ).getAsLong();
				long currentTime = customerState.thisRecordDate.getTime();
				long currentDay = currentTime - ( currentTime % msecPerDay );
				if( dayActivity == currentDay )
					return true;
			}else
				// more days in the activity. So not the first time
				return false;
		}
		return false;
	}

	//
	// Used for IMEI Change Array Indicator
	// public static boolean IMEIChanged() {
	// JsonElement jelemDates = customerState.subscriberIndicators.get(
	// "IMEI_CHANGE_ACTIVITY_TIME_SERIES_LifeTime-L" );
	// if( jelemDates != null ) {
	// JsonArray arr = jelemDates.getAsJsonArray();
	// if( arr.size() > 0 ) {
	// long latestChangeDay = arr.get( arr.size() - 1 ).getAsLong();
	// long currentTime = lcallDate;
	// long currentDay = currentTime - ( currentTime % msecPerDay );
	// if( currentDay == latestChangeDay )
	// return true;
	// }
	// }
	// return false;
	// }

	public boolean IMEIChanged() {
		JsonElement jelemDates = customerState.subscriberIndicators.get( "IMEI_CHANGE_SERIES_LifeTime-L" );
		if( jelemDates != null ) {
			JsonElement eChangeDate = ( ( JsonObject ) jelemDates ).get( "latestIMEIChangeDate" );
			if( eChangeDate != null ) {
				if( eChangeDate.getAsLong() == customerState.thisRecordDate.getTime() )
					return true;
			}
		}
		return false;
	}

	public int getActivityCalendarFactor( String factor ) {
		Calendar cal = Calendar.getInstance();
		cal.setTime( customerState.thisRecordDate );
		if( factor.equals( "DAY_OF_WEEK" ) )
			return cal.get( Calendar.DAY_OF_WEEK );
		else if( factor.equals( "DAY_OF_MONTH" ) )
			return cal.get( Calendar.DAY_OF_MONTH );
		else if( factor.equals( "HOUR_OF_DAY" ) )
			return cal.get( Calendar.HOUR_OF_DAY );
		else if( factor.equals( "DAY_OF_YEAR" ) )
			return cal.get( Calendar.DAY_OF_YEAR );
		else if( factor.equals( "YEAR" ) )
			return cal.get( Calendar.YEAR );
		return 0;
	}

	public double[ ] getValues( String indicatorName, int start, int pastCount ) {
		double[ ] values = new double[ pastCount ];
		// Currently it works only for "Daily" frequency
		Calendar cal = Calendar.getInstance();
		cal.setTime(  customerState.thisRecordDate );
		int dayToday = 0;

		String freq = "D";
		if( indicatorName.contains( "Daily" ) ) {
			dayToday = new Integer( cal.get( Calendar.DAY_OF_YEAR ) );
		}else if( indicatorName.contains( "Weekly" ) ) {
			dayToday = new Integer( cal.get( Calendar.WEEK_OF_YEAR ) );
			freq = "W";
		}else if( indicatorName.contains( "Monthly" ) ) {
			dayToday = new Integer( cal.get( Calendar.MONTH ) );
			freq = "M";
		}
		// if( start > 0 )
		dayToday = dayToday - start;
		int thisYear = cal.get( Calendar.YEAR ) - 2000;

		int j = 0;
		for( int i = ( pastCount - 1 ); i >= 0; i-- ) {
			int nDayPart = dayToday - i;
			String key = indicatorName + "-" + nDayPart + freq + thisYear;
			// System.out.println( key );
			JsonElement jValue = customerState.subscriberIndicators.get( key );
			if( jValue == null )
				values[ j++ ] = 0;
			else
				values[ j++ ] = jValue.getAsDouble();
		}
		return values;
	}

	public double standardDeviation( String indicatorName, int start, int pastCount ) {
		double[ ] values = getValues( indicatorName, start, pastCount );
		StandardDeviation stdev = new StandardDeviation();
		double result = stdev.evaluate( values );
		return result;
	}

	public double mean( String indicatorName, int start, int pastCount ) {
		double[ ] values = getValues( indicatorName, start, pastCount );
		Mean mean = new Mean();
		double result = mean.evaluate( values );
		return result;
	}

	public double sum( String indicatorName, int start, int pastCount ) {
		double[ ] values = getValues( indicatorName, start, pastCount );
		double result = 0;
		for( int i = 0; i < values.length; i++ )
			result += values[ i ];
		// System.out.println( "Sum Called for : " + SiftMath.msisdn +
		// " start : " + start + " End : " + pastCount + "  Result : " + result
		// );
		return result;
	}

	public double min( String indicatorName, int start, int pastCount ) {
		double[ ] values = getValues( indicatorName, start, pastCount );
		Min min = new Min();
		return min.evaluate( values );
	}

	public double max( String indicatorName, int start, int pastCount ) {
		double[ ] values = getValues( indicatorName, start, pastCount );
		Max max = new Max();
		return max.evaluate( values );
	}

	public boolean qualified( String eventId ) {
		if( customerState.subscriberTriggers.has( eventId ) ) {
			// System.out.println( "Subscriber : " + msisdn +
			// " was not triggered the Event " + eventId );
			return true;
		}else
			return false;
	}

	
	public boolean inGeoFenceGroup( String zoneGroup, String key, String RefDocName, int latIndex, int longIndex ) {

		String cellIdVal = getSupportingData( RefDocName, key );

		if( cellIdVal == null || cellIdVal == "" )
			return false;

		String cellIdValArr[] = cellIdVal.split( "\\|" );

		if( ! ( cellIdValArr.length >= latIndex && cellIdValArr.length >= longIndex && latIndex >= 0 && longIndex >= 0 && latIndex != longIndex ) )
			return false;

		double thisLat = Double.parseDouble( cellIdValArr[ latIndex ] );
		double thisLong = Double.parseDouble( cellIdValArr[ longIndex ] );

		String geoFenceObj = getJsonSupportingData( "GeoFences", zoneGroup );
		if( geoFenceObj == null || geoFenceObj.equals( "" ) )
			return false;
		JsonObject group = ( JsonObject ) new JsonParser().parse( geoFenceObj );

		Set< Entry< String, JsonElement >> es = group.entrySet();
		Iterator< Entry< String, JsonElement >> itr = es.iterator();
		while( itr.hasNext() ) {
			Entry< String, JsonElement > entry = itr.next();
			JsonObject jZone = ( JsonObject ) entry.getValue();
			JsonArray latArray = jZone.get( "lat" ).getAsJsonArray();
			JsonArray longArray = jZone.get( "long" ).getAsJsonArray();

			if( isCurrentLocationInTargetAreas( thisLat, thisLong, latArray, longArray ) )
				return true;
		}
		return false;
	}
	
	public boolean inLocation( String fgroup, String zone ) {
		String thisCell = getSupportingData( "cellIdMap", customerState.tupleMap.get( "CELL_ID" ).getAsString() ); //cellIdMap.get( cellId );
		if( thisCell == null )
			return false;

		double thisLat = Double.parseDouble( thisCell.split( "\\|" )[ 0 ] );
		double thisLong = Double.parseDouble( thisCell.split( "\\|" )[ 1 ] );

		String geoFenceObj = getJsonSupportingData( "GeoFences", fgroup );
		if( geoFenceObj == null || geoFenceObj.equals( "" ) )
			return false;
		JsonObject group = ( JsonObject ) new JsonParser().parse( geoFenceObj );
		
		if( group == null )
			return false;
		JsonObject jZone = ( JsonObject ) group.get( zone );
		if( jZone == null )
			return false;

		JsonArray latArray = jZone.get( "lat" ).getAsJsonArray();
		JsonArray longArray = jZone.get( "long" ).getAsJsonArray();

		return isCurrentLocationInTargetAreas( thisLat, thisLong, latArray, longArray );
	}

	public boolean inLocation( String fgroup, String zone, String key, String RefDocName, int latIndex, int longIndex ) {
		String cellIdVal = getSupportingData( RefDocName, key );

		if( cellIdVal == null || cellIdVal == "" )
			return false;

		String cellIdValArr[] = cellIdVal.split( "\\|" );

		if( ! ( cellIdValArr.length >= latIndex && cellIdValArr.length >= longIndex && latIndex >= 0 && longIndex >= 0 && latIndex != longIndex ) )
			return false;

		double thisLat = Double.parseDouble( cellIdValArr[ latIndex ] );
		double thisLong = Double.parseDouble( cellIdValArr[ longIndex ] );

		String geoFenceObj = getJsonSupportingData( "GeoFences", fgroup );
		if( geoFenceObj == null || geoFenceObj.equals( "" ) )
			return false;
		JsonObject group = ( JsonObject ) new JsonParser().parse( geoFenceObj );

		JsonObject jZone = ( JsonObject ) group.get( zone );
		if( jZone == null )
			return false;

		JsonArray latArray = jZone.get( "lat" ).getAsJsonArray();
		JsonArray longArray = jZone.get( "long" ).getAsJsonArray();

		return isCurrentLocationInTargetAreas( thisLat, thisLong, latArray, longArray );
	}

	public boolean isCurrentLocationInTargetAreas( double latitude, double longitude, JsonArray dblLatArray, JsonArray dblLongArray ) {
		int i;
		double angle = 0;
		double dblPoint1Lat;
		double dblPoint1Long;
		double dblPoint2Lat;
		double dblPoint2Long;
		int n = dblLatArray.size();

		for( i = 0; i < n; i++ ) {
			dblPoint1Lat = dblLatArray.get( i ).getAsDouble() - latitude;
			dblPoint1Long = dblLongArray.get( i ).getAsDouble() - longitude;
			dblPoint2Lat = dblLatArray.get( ( i + 1 ) % n ).getAsDouble() - latitude;
			dblPoint2Long = dblLongArray.get( ( i + 1 ) % n ).getAsDouble() - longitude;
			angle += get2DAngle( dblPoint1Lat, dblPoint1Long, dblPoint2Lat, dblPoint2Long );
		}

		if( Math.abs( angle ) < dblPi )
			return false;
		else
			return true;
	}

	public double get2DAngle( double y1, double x1, double y2, double x2 ) {
		double dtheta, theta1, theta2;

		theta1 = Math.atan2( y1, x1 );
		theta2 = Math.atan2( y2, x2 );
		dtheta = theta2 - theta1;
		while( dtheta > dblPi )
			dtheta -= dbl2Pi;
		while( dtheta < -dblPi )
			dtheta += dbl2Pi;

		return ( dtheta );
	}

	public boolean hasProduct( String productId ) {
		JsonElement pkgSeries = customerState.subscriberIndicators.get( "DATA_SUBSCRIPTION_PKG_SERIES_LifeTime-L" );
		if( pkgSeries == null )
			return false;
		JsonArray jPkgArray = pkgSeries.getAsJsonArray();
		if( !productId.equals( jPkgArray.get( jPkgArray.size() - 1 ).getAsString() ) )
			return false;

		// get the subscription date and check if its still not expired
		JsonElement subSeries = customerState.subscriberIndicators.get( "DATA_SUBSCRIPTION_TIME_SERIES_LifeTime-L" );
		JsonArray jSubArray = subSeries.getAsJsonArray();
		long subStart = jSubArray.get( jSubArray.size() - 1 ).getAsLong();

		// get the subscription duration
		double packageDuration = getNumericSupportingData( "PACKAGE_DURATION", productId );
		long subEnd = ( long ) ( subStart + packageDuration * msecPerDay );
		if(  customerState.thisRecordDate.getTime() < subEnd )
			return true;
		return false;
	}

	public boolean hasProduct( String serviceType, String productId ) {
		JsonElement pkgSeries = customerState.subscriberIndicators.get( "DATA_SUBSCRIPTION_PKG_SERIES_LifeTime-L" );
		if( pkgSeries == null )
			return false;
		JsonArray jPkgArray = pkgSeries.getAsJsonArray();
		if( productId.equals( "*" ) )
			productId = jPkgArray.get( jPkgArray.size() - 1 ).getAsString();
		else {
			if( !productId.equals( jPkgArray.get( jPkgArray.size() - 1 ).getAsString() ) )
				return false;
		}
		// get the subscription date and check if its still not expired
		JsonElement subSeries = customerState.subscriberIndicators.get( "DATA_SUBSCRIPTION_TIME_SERIES_LifeTime-L" );
		JsonArray jSubArray = subSeries.getAsJsonArray();
		long subStart = jSubArray.get( jSubArray.size() - 1 ).getAsLong();

		// get the subscription duration
		double packageDuration = getNumericSupportingData( "PACKAGE_DURATION", productId );
		long subEnd = ( long ) ( subStart + packageDuration * msecPerDay );
		if(  customerState.thisRecordDate.getTime() < subEnd ) {
			String packageType = getSupportingData( "PACKAGE_TYPE", productId );
			if( packageType != null && packageType.equals( serviceType ) )
				return true;
		}
		return false;
	}

	// works for the currently active product
	public double getQuotaConsumptionPercentage() {
		JsonElement pkgSeries = customerState.subscriberIndicators.get( "DATA_SUBSCRIPTION_PKG_SERIES_LifeTime-L" );
		if( pkgSeries == null )
			return DAYS_SINCE_DEFAULT;
		JsonArray jPkgArray = pkgSeries.getAsJsonArray();
		String activePackage = jPkgArray.get( jPkgArray.size() - 1 ).getAsString();

		JsonElement subSeries = customerState.subscriberIndicators.get( "DATA_SUBSCRIPTION_CONSUMPTION_SERIES_LifeTime-L" );
		JsonArray jSubArray = subSeries.getAsJsonArray();
		double lConsumption = jSubArray.get( jSubArray.size() - 1 ).getAsDouble();

		double quota = getNumericSupportingData( "PACKAGE_QUOTA", activePackage );

		// System.out.println( "getQuotaConsumptionPercentage : " + new Double(
		// ( lConsumption / quota ) * 100 ) );
		return ( lConsumption / quota ) * 100;
	}

	public  int getCalendarFactor( String type ) {
		if( type.equals( "DAY_OF_WEEK" ) )
			return Calendar.getInstance().get( Calendar.DAY_OF_WEEK );
		else if( type.equals( "DAY_OF_MONTH" ) )
			return Calendar.getInstance().get( Calendar.DAY_OF_MONTH );
		else if( type.equals( "HOUR_OF_DAY" ) )
			return Calendar.getInstance().get( Calendar.HOUR_OF_DAY );
		else if( type.equals( "MONTH_OF_YEAR" ) )
			return Calendar.getInstance().get( Calendar.MONTH );
		else if( type.equals( "DAY_OF_MONTH" ) )
			return Calendar.getInstance().get( Calendar.DAY_OF_MONTH );
		return Calendar.getInstance().get( Calendar.DAY_OF_YEAR );
	}

	public double getQuotaDurationLeftPercentage() {
		return 100 - getQuotaDurationConsumedPercentage();
	}

	public double getQuotaDurationConsumedPercentage() {
		JsonElement pkgSeries = customerState.subscriberIndicators.get( "DATA_SUBSCRIPTION_PKG_SERIES_LifeTime-L" );
		if( pkgSeries == null )
			return -1;
		JsonArray jPkgArray = pkgSeries.getAsJsonArray();
		String activePackage = jPkgArray.get( jPkgArray.size() - 1 ).getAsString();

		JsonElement pkgTimeSeries = customerState.subscriberIndicators.get( "DATA_SUBSCRIPTION_TIME_SERIES_LifeTime-L" );
		if( pkgTimeSeries == null )
			return -1;
		JsonArray jPkgTimeArray = pkgTimeSeries.getAsJsonArray();
		long activePackageSubscriptionDate = jPkgTimeArray.get( jPkgArray.size() - 1 ).getAsLong();

		// System.out.println( "Active  Package : " + activePackage );
		double quota = getNumericSupportingData( "PACKAGE_DURATION", activePackage ) * ( double ) msecPerDay;

		// how many days have elapsed since the subscription.
		long elapsedDays = (  customerState.thisRecordDate.getTime() - activePackageSubscriptionDate );
		return ( ( double ) elapsedDays / quota ) * 100;
	}

	public String getActivePackage() {
		JsonElement pkgSeries = customerState.subscriberIndicators.get( "DATA_SUBSCRIPTION_PKG_SERIES_LifeTime-L" );
		if( pkgSeries == null )
			return null;
		JsonArray jPkgArray = pkgSeries.getAsJsonArray();
		return jPkgArray.get( jPkgArray.size() - 1 ).getAsString();
	}

	public double getQuotaDurationLeft() {
		JsonElement pkgSeries = customerState.subscriberIndicators.get( "DATA_SUBSCRIPTION_PKG_SERIES_LifeTime-L" );
		if( pkgSeries == null )
			return -1;
		JsonArray jPkgArray = pkgSeries.getAsJsonArray();
		String activePackage = jPkgArray.get( jPkgArray.size() - 1 ).getAsString();

		JsonElement pkgTimeSeries = customerState.subscriberIndicators.get( "DATA_SUBSCRIPTION_TIME_SERIES_LifeTime-L" );
		if( pkgTimeSeries == null )
			return -1;
		JsonArray jPkgTimeArray = pkgTimeSeries.getAsJsonArray();
		long activePackageSubscriptionDate = jPkgTimeArray.get( jPkgArray.size() - 1 ).getAsLong();

		// System.out.println( "Active  Package : " + activePackage );
		double quota = getNumericSupportingData( "PACKAGE_DURATION", activePackage ) * ( double ) msecPerDay;
		// System.out.println( "Quota Days :" + quota );

		// how many days have elapsed since the subscription.
		long elapsedDays = (  customerState.thisRecordDate.getTime() - activePackageSubscriptionDate );
		// System.out.println( "Elapsed Days :" + elapsedDays );
		return ( quota - elapsedDays ) / msecPerDay;
	}

	public long daysSinceLastActivity() {
		return daysSinceLastActivity( null );
	}

	public long daysSinceLastActivity( String activityType ) {
		String ind = activityType + "_ACTIVITY_TIME_SERIES_LifeTime-L";
		if( activityType == null )
			ind = "ACTIVITY_TIME_SERIES_LifeTime-L";
		JsonElement jValue = customerState.subscriberIndicators.get( ind );
		if( jValue != null ) {
			JsonArray arr = jValue.getAsJsonArray();
			if( arr.size() == 0 )
				return PERIOD_DEFAULT;
			long latestEntry = 0;
			if( arr.size() >= 2 )
				latestEntry = arr.get( arr.size() - 1 ).getAsLong();
			else
				latestEntry = arr.get( 0 ).getAsLong();
			// long currentTime = new java.util.Date().getTime();
			// long currentDay = currentTime - ( currentTime % msecPerDay );
			// currentDay += java.util.TimeZone.getDefault().getOffset(
			// currentDay );

			long currentDay = getCurrentTime();
			// add the year factor
			// System.out.println( "Days Since Last Activity : " + Math.round( (
			// currentDay - latestEntry ) / dmsecPerDay ) );
			return Math.round( ( currentDay - latestEntry ) / dmsecPerDay );
		}else
			return PERIOD_DEFAULT;
	}

	public double getActivityIntervalAverage( String activityType, long start, long count ) {
		String ind = activityType + "_ACTIVITY_TIME_SERIES_LifeTime-L";
		if( activityType == null )
			ind = "ACTIVITY_TIME_SERIES_LifeTime-L";
		JsonElement jValue = customerState.subscriberIndicators.get( ind );
		if( jValue != null ) {
			JsonArray arr = jValue.getAsJsonArray();
			int size = arr.size();
			long lstart = size - 1 - start;
			long lend = lstart - count + 1;
			if( lstart == lend )
				lend--;
			if( lend < 0 )
				lend = 0;
			long sumOfIntetvals = 0;
			int numItems = 0;
			for( int i = ( int ) lstart; i > lend; i-- ) {
				sumOfIntetvals += ( arr.get( i ).getAsLong() - arr.get( i - 1 ).getAsLong() ) / msecPerDay;
				numItems++;
			}
			if( numItems == 0 )
				return -1;
			return sumOfIntetvals / numItems;
		}
		return -1;
	}

	public double getActivityInterval( String activityType, long start ) {
		return getActivityIntervalAverage( activityType, start, 1 );
	}

	public long[ ] idleDaysAboveValue( long start, long end, int value ) {
		return idleDaysAboveValue( null, start, end, value );
	}

	public long[ ] idleDaysAboveValue( String activityType, long start, long end, int value ) {
		String ind = activityType + "_ACTIVITY_TIME_SERIES_LifeTime-L";
		if( activityType == null )
			ind = "ACTIVITY_TIME_SERIES_LifeTime-L";
		JsonElement jValue = customerState.subscriberIndicators.get( ind );
		if( jValue != null ) {
			JsonArray arr = jValue.getAsJsonArray();
			Gson gson = new Gson();
			long[ ] arrActivitySeries = gson.fromJson( arr, long[ ].class );
			Arrays.sort( arrActivitySeries );
			// long currentTime = new java.util.Date().getTime();

			long currentTime = getCurrentTime();

			long currentDay = currentTime - ( currentTime % msecPerDay );
			long startDay = currentDay - ( start * msecPerDay );
			long endDay = currentDay - ( end * msecPerDay );
			ArrayList< Long > finalArray = new ArrayList< Long >();
			for( int i = arrActivitySeries.length - 1; i >= 0; i-- )
				if( arrActivitySeries[ i ] <= startDay && arrActivitySeries[ i ] >= endDay )
					finalArray.add( arrActivitySeries[ i ] );

			ArrayList< Long > gapArray = new ArrayList< Long >();
			if( finalArray.size() > 1 ) {
				for( int i = 0; i < finalArray.size() - 1; i++ ) {
					long gap = ( finalArray.get( i ) - finalArray.get( i + 1 ) ) / msecPerDay;
					if( gap >= value )
						gapArray.add( gap );
				}
			}
			// convert to primitive array
			long[ ] retArray = new long[ gapArray.size() ];
			for( int i = 0; i < gapArray.size(); i++ )
				retArray[ i ] = gapArray.get( i );
			return retArray;
		}else
			return null;
	}

	public  long max( long[ ] input ) {
		if( input.length == 0 )
			return -1;
		long max = input[ 0 ];
		for( int i = 0; i > input.length; i++ )
			if( input[ i ] > max )
				max = input[ i ];
		return max;
	}

	public  long min( long[ ] input ) {
		long min = input[ 0 ];
		for( int i = 0; i > input.length; i++ )
			if( input[ i ] < min )
				min = input[ i ];
		return min;
	}

	public double getAverageLowBalanceBeforeTopup() {
		return getAverageLowBalanceBeforeTopup( 100 );
	}

	public double getAverageLowBalanceBeforeTopup( int count ) {
		JsonElement jValue = customerState.subscriberIndicators.get( "BALANCE_BEFORE_CREDIT_SERIES_LifeTime-L" );
		if( jValue == null )
			return -1;
		JsonArray jArrValue = jValue.getAsJsonArray();

		if( count > jArrValue.size() )
			count = jArrValue.size();
		double[ ] selArray = new double[ count ];
		int j = 0;
		for( int i = jArrValue.size() - 1; i >= 0; i-- ) {
			selArray[ j++ ] = jArrValue.get( i ).getAsDouble();
			if( j >= count )
				break;
		}
		Mean mean = new Mean();
		return mean.evaluate( selArray );
	}

	public  int count( long[ ] input ) {
		return input.length;
	}

	public int count( String seriesName, String value ) {
		JsonArray jArray = getSeriesIndicator( seriesName, 0, 0 );
		int count = 0;
		if( jArray != null ) {
			for( int i = 0; i < jArray.size(); i++ ) {
				if( jArray.get( i ).getAsString().equals( value ) )
					count++;
			}
			return count;
		}
		return -1;
	}

	public int packageSubscriptionCount( String packageName, int start, int end ) {
		long ltoday = new Date().getTime();
		long lStart = ltoday - ( start * msecPerDay );
		long lEnd = lStart - ( end * msecPerDay );

		JsonElement element = customerState.subscriberIndicators.get( "DATA_SUBSCRIPTION_PKG_SERIES_LifeTime-L" );
		if( element == null )
			return DAYS_SINCE_DEFAULT;
		JsonArray pkgSeries = element.getAsJsonArray();
		element = customerState.subscriberIndicators.get( "DATA_SUBSCRIPTION_TIME_SERIES_LifeTime-L" );
		if( element == null )
			return DAYS_SINCE_DEFAULT;
		JsonArray pkgTimeSeries = element.getAsJsonArray();

		int count = 0;
		for( int i = 0; i < pkgSeries.size(); i++ ) {
			if( pkgSeries.get( i ).getAsString().equals( packageName ) ) {
				long lpkgTime = pkgTimeSeries.get( i ).getAsLong();
				if( lpkgTime >= lEnd && lpkgTime <= lStart )
					count++;
			}
		}
		// System.out.println( "packageSubscriptionCount - " + packageName +
		// " = " + count );
		return count;
	}

	public double getAverageQuotaConsumptionPercentage( String packageName, int start, int end ) {
		long ltoday = new Date().getTime();
		long lStart = ltoday - ( start * msecPerDay );
		long lEnd = lStart - ( end * msecPerDay );

		JsonElement element = customerState.subscriberIndicators.get( "DATA_SUBSCRIPTION_PKG_SERIES_LifeTime-L" );
		if( element == null )
			return DAYS_SINCE_DEFAULT;
		JsonArray pkgSeries = element.getAsJsonArray();
		element = customerState.subscriberIndicators.get( "DATA_SUBSCRIPTION_TIME_SERIES_LifeTime-L" );
		if( element == null )
			return DAYS_SINCE_DEFAULT;
		JsonArray pkgTimeSeries = element.getAsJsonArray();

		element = customerState.subscriberIndicators.get( "DATA_SUBSCRIPTION_CONSUMPTION_SERIES_LifeTime-L" );
		if( element == null )
			return DAYS_SINCE_DEFAULT;
		JsonArray consumptionSeries = element.getAsJsonArray();

		ArrayList< Double > conArray = new ArrayList< Double >();
		for( int i = 0; i < pkgSeries.size() - 1; i++ ) {
			String pkgNameToUse = packageName;
			String thisPkg = pkgSeries.get( i ).getAsString();
			if( packageName.equals( "ALL" ) )
				pkgNameToUse = thisPkg;
			if( !pkgNameToUse.equals( thisPkg ) )
				continue;
			long lpkgTime = pkgTimeSeries.get( i ).getAsLong();
			double quota = getNumericSupportingData( "PACKAGE_QUOTA", pkgNameToUse );
			if( lpkgTime >= lEnd && lpkgTime <= lStart ) {
				double lConsumption = consumptionSeries.get( i ).getAsDouble();
				double percent = ( lConsumption / quota ) * 100;
				conArray.add( percent );
			}
		}
		if( conArray.size() == 0 )
			return 0;
		double sum = 0;
		for( int i = 0; i < conArray.size(); i++ ) {
			sum += conArray.get( i );
		}
		// System.out.println( "getAverageQuotaConsumptionPercentage - " +
		// packageName + " = " + new Double( sum / conArray.size() ) );
		return sum / conArray.size();
	}

	// TODO: The return type must be an error code if the data is not present.
	public String getSupportingData( String type, String key ) {
		JsonObject supportingData = Commons.supportingData.get( type );
		if( supportingData == null ) {
			// System.out.println( "******Get Supporting Data is -1 " );
			return null;
		}
		if( customerState.tupleMap.has( key ) ) {
			key = customerState.tupleMap.get( key ).getAsString();
		}
		// System.out.println( " Get Supporting Data for " + type + " - " + key
		// + " = " + supportingData.get( key ).getAsDouble() );

		JsonElement ele = supportingData.get( key );
		if( ele != null )
			return supportingData.get( key ).getAsString();
		else
			return "";
	}

	
	public String getJsonSupportingData( String type, String key ) {
		JsonObject supportingData = Commons.supportingData.get( type );
		if( supportingData == null ) {
			// System.out.println( "******Get Supporting Data is -1 " );
			return null;
		}
		if( customerState.tupleMap.has( key ) ) {
			key = customerState.tupleMap.get( key ).getAsString();
		}
		// System.out.println( " Get Supporting Data for " + type + " - " + key
		// + " = " + supportingData.get( key ).getAsDouble() );

		JsonElement ele = supportingData.get( key );
		if( ele != null )
			return supportingData.get( key ).toString();
		else
			return "";
	}

	// TODO: The return type must be an error code if the data is not present.
	public double getNumericSupportingData( String type, String key ) {
		JsonObject supportingData = Commons.supportingData.get( type );
		if( supportingData == null ) {
			// System.out.println( "******Get Supporting Data is -1 " );
			return -1;
		}
		if( customerState.tupleMap.has( key ) ) {
			key = customerState.tupleMap.get( key ).getAsString();
		}
		// System.out.println( " Get Supporting Data for " + type + " - " + key
		// + " = " + supportingData.get( key ).getAsDouble() );
		JsonElement ele = supportingData.get( key );
		if( ele != null )
			return supportingData.get( key ).getAsDouble();
		else
			return 0;
	}

	public int getNumberOfCellVisits( int startDay, int endDay, int startHour, int endHour ) {
		String key = customerState.cellId + "_CELLS_USED_TIME_SERIES_LifeTime-L";
		JsonElement cellsUsedTimeSeries = customerState.subscriberIndicators.get( key );

		JsonArray arrcellsUsedTimeSeries = null;
		int count = 0;
		if( cellsUsedTimeSeries != null ) {
			arrcellsUsedTimeSeries = cellsUsedTimeSeries.getAsJsonArray();
			long today = customerState.thisRecordDate.getTime();
			long start = today + ( startDay * msecPerDay );
			long end = start - ( endDay * msecPerDay );
			for( int i = 0; i < arrcellsUsedTimeSeries.size(); i++ ) {
				long entry = arrcellsUsedTimeSeries.get( i ).getAsLong();
				Calendar cal = Calendar.getInstance();
				cal.setTimeInMillis( entry );
				int entryHour = cal.get( Calendar.HOUR_OF_DAY );
				if( entry >= end && entry <= start && entryHour >= startHour && entryHour <= endHour )
					count++;
			}
		}else
			return DAYS_SINCE_DEFAULT;

		return count;
	}

	public double getChurnScore() {
		JsonElement elem = customerState.subscriberIndicators.get( "CHURN_SCORE" );
		if( elem != null )
			return elem.getAsDouble();
		return DAYS_SINCE_DEFAULT;
	}

	public String getSegments() {
		if( customerState.subscriberIndicators == null )
			return "";
		JsonElement elem = customerState.subscriberIndicators.get( "SEGMENTS" );
		if( elem != null ) {
			return elem.getAsString();
		}
		return "";
	}

	public String getIndicatorKey( String indicatorName ) {
		Calendar cal = Calendar.getInstance();
		cal.setTime( customerState.thisRecordDate );
		int dayToday = 0;

		JsonObject thisInd = Commons.allIndDef.get( indicatorName );
		String freq = thisInd.get( "frequency" ).getAsString();
		if( freq.equals( "Daily" ) ) {
			dayToday = new Integer( cal.get( Calendar.DAY_OF_YEAR ) );
		} else if( freq.equals( "Weekly" ) ) {
			dayToday = new Integer( cal.get( Calendar.WEEK_OF_YEAR ) );
			freq = "W";
		} else if( freq.equals( "Monthly" ) ) {
			dayToday = new Integer( cal.get( Calendar.MONTH ) );
			freq = "M";
		} else if( freq.equals( "LifeTime" ) ) {
			freq = "L";
		} else if( freq.equals( "Session" ) )
			freq = "S";

		int thisYear = cal.get( Calendar.YEAR ) - 2000;
		String key = indicatorName + "-" + dayToday + freq + thisYear;

		if( freq.equals( "L" ) )
			key = indicatorName + "-L";
		else if( freq.equals( "S") )
			key = indicatorName + "-S";
		return key;
	}

	public JsonArray getSeriesIndicator( String indicatorName, int start, int pastCount ) {
		// Currently it works only for "Daily" frequency
		Calendar cal = Calendar.getInstance();
		cal.setTime( customerState.thisRecordDate );
		int dayToday = 0;

		String freq = "D";
		if( indicatorName.contains( "Daily" ) ) {
			dayToday = new Integer( cal.get( Calendar.DAY_OF_YEAR ) );
		}else if( indicatorName.contains( "Weekly" ) ) {
			dayToday = new Integer( cal.get( Calendar.WEEK_OF_YEAR ) );
			freq = "W";
		}else if( indicatorName.contains( "Monthly" ) ) {
			dayToday = new Integer( cal.get( Calendar.MONTH ) );
			freq = "M";
		}else if( indicatorName.contains( "LifeTime" ) ) {
			freq = "L";
		}
		if( start > 0 )
			dayToday = dayToday - ( start - 1 );
		int thisYear = cal.get( Calendar.YEAR ) - 2000;

		int nDayPart = dayToday - ( pastCount ) + 1;
		String key = indicatorName + "-" + nDayPart + freq + thisYear;

		if( freq.equals( "L" ) )
			key = indicatorName + "-L";
		// System.out.println( key );
		JsonElement jValue = customerState.subscriberIndicators.get( key );
		if( jValue == null )
			return null;
		else
			return jValue.getAsJsonArray();
	}

	public double getAgeOnNetwork() {
		JsonElement jele = customerState.subscriberIndicators.get( "AccountStartDate" );
		if( jele == null )
			return DAYS_SINCE_DEFAULT;
		long lStart = jele.getAsLong();
		return ( new java.util.Date().getTime() - lStart ) / msecPerDay;
	}

	public double evaluateScoringModel( String pmml ) {
		return 1;
	}

	private String readFile( String fileName ) throws IOException {
		BufferedReader br = new BufferedReader( new FileReader( fileName ) );
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while( line != null ) {
				sb.append( line );
				sb.append( "\n" );
				line = br.readLine();
			}
			return sb.toString();
		}finally {
			br.close();
		}
	}

	public boolean matchInFile( String stringtoMatch, String fileName ) {
		if( filesMap == null )
			filesMap = new HashMap< String, String >();
		if( !filesMap.containsKey( fileName ) ) {
			try {
				filesMap.put( fileName, readFile( fileName ) );
			}catch( IOException e ) {
				filesMap.put( fileName, "" );
				e.printStackTrace();
			}
		}
		return filesMap.get( fileName ).contains( stringtoMatch );
	}

	protected Object getValueFromIndicator( String indicator, CustomerState customerState, String eventId, JsonObject aTag ) throws ParseException {
		if( Commons.allIndDef.containsKey( indicator ) ) {
			String keyInInd = getIndicatorKey( indicator );
			if( customerState.subscriberIndicators.has( keyInInd ) )
				return customerState.subscriberIndicators.get( keyInInd );
			else if( customerState.sessionIndicators.has( keyInInd ) ) 
				return customerState.sessionIndicators.get( keyInInd );
		} else { //if( Commons.fixedOfferParams.containsKey( indicator ) ) {
			JsonElement programParam = customerState.subscriberIndicators.get( "PROGRAM_PARAMETERS-L" );
			if( programParam != null && programParam.getAsJsonObject().has( indicator ) ) {
				return programParam.getAsJsonObject().get( indicator );
			}
		}
		return null;
	}

	
	public JsonObject evaluateRScript( String jsonFileName ) {
		//System.out.println( "Evaluating RScript in  : " + jsonFileName );
		JsonObject scriptDef = null;
		SEXP result;
		JsonObject jResults = new JsonObject();
		String rscript = null;
		try {
			if( rScriptDescriptorssMap != null ) {
				scriptDef = rScriptDescriptorssMap.get( jsonFileName );
				rscript = rscriptsMap.get( jsonFileName );
			}
			if( scriptDef == null ) {
				String strScriptDef = readFile( jsonFileName );
				scriptDef = ( JsonObject ) new JsonParser().parse( strScriptDef );
				rScriptDescriptorssMap = new java.util.HashMap< String, JsonObject >();
				rScriptDescriptorssMap.put( jsonFileName, scriptDef );

				rscriptsMap = new java.util.HashMap< String, String >();
				rscript = readFile( scriptDef.get( "RscriptPath" ).getAsString() );
				rscriptsMap.put( jsonFileName, rscript );
			}

			if( scriptEngineManager == null )
				scriptEngineManager = new ScriptEngineManager();
			if( scriptEngine == null )
				scriptEngine = scriptEngineManager.getEngineByName( "Renjin" );
			if( scriptEngine == null ) {
				throw new RuntimeException( "Renjin Script Engine not found on the classpath." );
			}
			//System.out.println( "R Engine Loaded" );

			JsonArray arrParamNames = scriptDef.get( "paramNames" ).getAsJsonArray();
			JsonArray paramTypes = scriptDef.get( "paramTypes" ).getAsJsonArray();
			String strParamTypes = new String();
			for( int i = 0; i < paramTypes.size(); i++ ) {
				if( i == 0 )
					strParamTypes += paramTypes.get( i ).getAsString();
				else
					strParamTypes += "~" + paramTypes.get( i ).getAsString();
			}
			Class[ ] allParamTypes = ExpressionUtil.stringToTypes( strParamTypes );
			JsonArray arrSiftParamNames = scriptDef.get( "siftParamMap" ).getAsJsonArray();
			Object[ ] parameterValues = new Object[ arrParamNames.size() ];

			// populate variables from Sift to R
			for( int j = 0; j < arrSiftParamNames.size(); j++ ) {
				String thisParam = arrSiftParamNames.get( j ).getAsString();
				Object jValue = null;

				if( customerState.tupleMap != null && customerState.tupleMap.has( thisParam ) )
					jValue =  new JsonPrimitive( customerState.tupleMap.get( thisParam ).getAsString() );
				else if( Commons.modelMasterTable.containsKey( thisParam ) )
					jValue = Commons.modelMasterTable.get( thisParam );
				else if( thisParam.equals( "SIFTMATH" ) ) 
					jValue = customerState.siftMath;
				if( jValue == null ) {
					if( Commons.allIndDef.containsKey( thisParam ) ) {
						//String key = getIndicatorKey( thisParam );
						jValue = getValueFromIndicator(thisParam, customerState, null, null );
					}else { // check in the statistical profile
						//						JsonElement statInd = customerState.subscriberIndicators.get( "SUBSCRIBER_STATISTICS_LifeTime-L" );
						//						if( statInd != null ) {
						//							JsonObject jStatInd = ( JsonObject ) statInd;
						//							JsonElement ele = jStatInd.get( thisParam );
						//							if( ele != null )
						//								jValue = ele;
						//						}else {
						// evalaute the expression
						String expressionId = jsonFileName.replace( "/", "_" ).replace( ".", "" ) + "_" + j;
						Commons.logger.info( "Rscript While Param Bindings - Evaluating Expression : " + expressionId );
						Object expres = evaluateExpression( expressionId );
						if( allParamTypes[ j ].isPrimitive() )
							jValue = new JsonPrimitive( expres.toString() );
						else
							jValue = expres;
						//						}
					}
				}
				//System.out.println( "Rscript While Param Bindings : " + arrParamNames.get( j ).getAsString() + " == " + jValue.toString() );
				// if( jValue != null )
				parameterValues[ j ] = ExpressionUtil.createObject( allParamTypes[ j ], jValue );
				// else
				// parameterValues[ j ] = new Double( 0 );
			}

			// System.out.println(
			// "----------------Start Listing Params to R Sscript-------" );
			for( int i = 0; i < arrParamNames.size(); i++ ) {
				if( allParamTypes[ i ].getSimpleName().contains( "double[]" ) )
					Commons.logger.info( "Rscript Bindings : " + arrParamNames.get( i ).getAsString() + " == " + Arrays.toString( ( double[ ] ) parameterValues[ i ] ) );
				else if( allParamTypes[ i ].getSimpleName().contains( "String[]" ) )
					Commons.logger.info( "Rscript Bindings : " + arrParamNames.get( i ).getAsString() + " == " + Arrays.toString( ( String[ ] ) parameterValues[ i ] ) );
				else 
					Commons.logger.info( "Rscript Bindings : " + arrParamNames.get( i ).getAsString() + " == " +  parameterValues[ i ].toString() );
				scriptEngine.put( arrParamNames.get( i ).getAsString(), parameterValues[ i ] );
			}
			// System.out.println(
			// "----------------End Listing Params to R Sscript-------" );
			result = ( SEXP ) scriptEngine.eval( rscript );
		}catch( Exception e ) {
			e.printStackTrace();
			// System.out.println( "--- Finished Evaluating RScript in  : " +
			// jsonFileName + " but returning empty" );
			return new JsonObject();
		}
		JsonArray arrResults = scriptDef.get( "returnValues" ).getAsJsonArray();
		JsonArray arrResultTypes = scriptDef.get( "returnTypes" ).getAsJsonArray();

		for( int i = 0; i < arrResults.size(); i++ ) {
			String retName = arrResults.get( i ).getAsString();
			SEXP sexpVal = ( SEXP ) scriptEngine.get( retName );
//			if( sexpVal.toString().equals( "NA" ) )
//				continue;
			String retType = arrResultTypes.get( i ).getAsString();
			if( retType.equals( "double" ) )
				jResults.addProperty( retName, sexpVal.asReal() );
			else if( retType.equals( "String" ) || retType.equals( "java.lang.String" ) )
				jResults.addProperty( retName, sexpVal.asString() );
		}
		// System.out.println( "--- Finished Evaluating RScript in  : " +
		// jsonFileName );
		return jResults;
	}
	
	public JsonObject executeRModel( String modelId ) {
		//System.out.println( "Evaluating RScript in  : " + jsonFileName );
		JsonObject scriptDef = null;
		SEXP result;
		JsonObject jResults = new JsonObject();
		String rscript = null;
		try {
			if( rScriptDescriptorssMap != null ) {
				scriptDef = rScriptDescriptorssMap.get( modelId );
				rscript = rscriptsMap.get( modelId );
			}
			if( scriptDef == null ) {
				String strScriptDef = Commons.pandaCacheConfig.get( modelId.replace( "\"", "") ).toString();
				scriptDef = ( JsonObject ) Commons.parser.parse( strScriptDef );
				rScriptDescriptorssMap = new java.util.HashMap< String, JsonObject >();
				rScriptDescriptorssMap.put( modelId, scriptDef );

				rscriptsMap = new java.util.HashMap< String, String >();
				rscript = Commons.pandaCacheConfig.get( scriptDef.get( "scriptDocId" ).getAsString() ).toString();
				rscriptsMap.put( modelId, rscript );
			}

			if( scriptEngineManager == null )
				scriptEngineManager = new ScriptEngineManager();
			if( scriptEngine == null )
				scriptEngine = scriptEngineManager.getEngineByName( "Renjin" );
			if( scriptEngine == null ) {
				throw new RuntimeException( "Renjin Script Engine not found on the classpath." );
			}
			//System.out.println( "R Engine Loaded" );

			JsonArray arrParamNames = new JsonArray();
			JsonArray paramTypes = new JsonArray();
			JsonArray arrSiftParamNames = new JsonArray();

			
			JsonArray inputParams = scriptDef.get("modelParams").getAsJsonArray();
			for(int i=0; i< inputParams.size();i++){
				JsonObject thisParam = inputParams.get(i).getAsJsonObject();
				arrParamNames.add(thisParam.get("paramName"));
				paramTypes.add(thisParam.get("paramDataType"));
				arrSiftParamNames.add(thisParam.get("paramMapping"));
			}

			
			String strParamTypes = new String();
			for( int i = 0; i < paramTypes.size(); i++ ) {
				if( i == 0 )
					strParamTypes += paramTypes.get( i ).getAsString();
				else
					strParamTypes += "~" + paramTypes.get( i ).getAsString();
			}
			Class[ ] allParamTypes = ExpressionUtil.stringToTypes( strParamTypes );
			
			Object[ ] parameterValues = new Object[ arrParamNames.size() ];

			// populate variables from Sift to R
			for( int j = 0; j < arrSiftParamNames.size(); j++ ) {
				String thisParam = arrSiftParamNames.get( j ).getAsString();
				Object jValue = null;

				if( customerState.tupleMap != null && customerState.tupleMap.has( thisParam ) )
					jValue =  new JsonPrimitive( customerState.tupleMap.get( thisParam ).getAsString() );
				else if( Commons.modelMasterTable.containsKey( thisParam ) )
					jValue = Commons.modelMasterTable.get( thisParam );
				else if( thisParam.equals( "SIFTMATH" ) ) 
					jValue = customerState.siftMath;
				if( jValue == null ) {
					if( Commons.allIndDef.containsKey( thisParam ) ) {
						//String key = getIndicatorKey( thisParam );
						jValue = getValueFromIndicator(thisParam, customerState, null, null );
					}else { // check in the statistical profile
						//						JsonElement statInd = customerState.subscriberIndicators.get( "SUBSCRIBER_STATISTICS_LifeTime-L" );
						//						if( statInd != null ) {
						//							JsonObject jStatInd = ( JsonObject ) statInd;
						//							JsonElement ele = jStatInd.get( thisParam );
						//							if( ele != null )
						//								jValue = ele;
						//						}else {
						// evalaute the expression
						String expressionId = modelId.replace( "/", "_" ).replace( ".", "" ) + "_" + j;
						Commons.logger.info( "Rscript While Param Bindings - Evaluating Expression : " + modelId );
						Object expres = evaluateExpression( expressionId );
						if( allParamTypes[ j ].isPrimitive() )
							jValue = new JsonPrimitive( expres.toString() );
						else
							jValue = expres;
						//						}
					}
				}
				//System.out.println( "Rscript While Param Bindings : " + arrParamNames.get( j ).getAsString() + " == " + jValue.toString() );
				// if( jValue != null )
				parameterValues[ j ] = ExpressionUtil.createObject( allParamTypes[ j ], jValue );
				// else
				// parameterValues[ j ] = new Double( 0 );
			}

			// System.out.println(
			// "----------------Start Listing Params to R Sscript-------" );
			for( int i = 0; i < arrParamNames.size(); i++ ) {
				if( allParamTypes[ i ].getSimpleName().contains( "double[]" ) )
					Commons.logger.info( "Rscript Bindings : " + arrParamNames.get( i ).getAsString() + " == " + Arrays.toString( ( double[ ] ) parameterValues[ i ] ) );
				else if( allParamTypes[ i ].getSimpleName().contains( "String[]" ) )
					Commons.logger.info( "Rscript Bindings : " + arrParamNames.get( i ).getAsString() + " == " + Arrays.toString( ( String[ ] ) parameterValues[ i ] ) );
				else 
					Commons.logger.info( "Rscript Bindings : " + arrParamNames.get( i ).getAsString() + " == " +  parameterValues[ i ].toString() );
				scriptEngine.put( arrParamNames.get( i ).getAsString(), parameterValues[ i ] );
			}
			// System.out.println(
			// "----------------End Listing Params to R Sscript-------" );
			result = ( SEXP ) scriptEngine.eval( rscript );
		}catch( Exception e ) {
			e.printStackTrace();
			// System.out.println( "--- Finished Evaluating RScript in  : " +
			// jsonFileName + " but returning empty" );
			return new JsonObject();
		}

		JsonArray arrResults = new JsonArray();
		JsonArray arrResultTypes = new JsonArray();
		JsonArray outputParams = scriptDef.get("returnValues").getAsJsonArray();
		for(int i=0; i< outputParams.size();i++){
			JsonObject thisParam = outputParams.get(i).getAsJsonObject();
			arrResults.add(thisParam.get("returnValues"));
			arrResultTypes.add(thisParam.get("returnTypes"));
		}
		
		for( int i = 0; i < arrResults.size(); i++ ) {
			String retName = arrResults.get( i ).getAsString();
			SEXP sexpVal = ( SEXP ) scriptEngine.get( retName );
//			if( sexpVal.toString().equals( "NA" ) )
//				continue;
			String retType = arrResultTypes.get( i ).getAsString();
			if( retType.equals( "double" ) )
				jResults.addProperty( retName, sexpVal.asReal() );
			else if( retType.equals( "String" ) || retType.equals( "java.lang.String" ) )
				jResults.addProperty( retName, sexpVal.asString() );
		}
		// System.out.println( "--- Finished Evaluating RScript in  : " +
		// jsonFileName );
		return jResults;
	}

	public double getKPIByBucket( String kpiPrefix, String range ) {
		String cimName = getSupportingData( kpiPrefix, range );
		// System.out.println( tuple.getStreamSchema().toString() );
		// System.out.println( cimName );
		if( cimName != null ) {
			// System.out.println( cimName + " - " + tuple.getFloat( cimName )
			// );
			return customerState.tupleMap.get( cimName ).getAsDouble();
		}else
			return -1;
	}

	public double getAverageFromSeries( String timeSeriesIndicator, int start, int end, boolean active ) {
		end = end - 1;
		if( start < 0 || end < 0 )
			return QUANTITY_DEFAULT;

		String indKey = timeSeriesIndicator + "-L";
		JsonElement timeseries = customerState.subscriberIndicators.get( indKey );
		if( timeseries == null )
			return QUANTITY_DEFAULT;

		double totalValue = 0;
		double totalActiveDays = 0;

		long lStart = getCurrentTime() - ( start * msecPerDay );
		lStart = lStart - ( lStart % msecPerDay );
		long lEnd = lStart - ( end * msecPerDay );

		if( timeseries.isJsonArray() ) {
			JsonArray timeArray = timeseries.getAsJsonArray();
			// System.out.println( lStart + " - " + lEnd );

			for( int i = timeArray.size() - 1; i >= 0; i-- ) {
				JsonArray thisEntry = timeArray.get( i ).getAsJsonArray();
				long timeInArray = thisEntry.get( 0 ).getAsLong();
				if( timeInArray >= lEnd && timeInArray <= lStart ) {
					double value = thisEntry.get( 1 ).getAsDouble();
					if( active && value > 0 )
						totalActiveDays++;
					totalValue += value;
					// System.out.println( "Adding Value : " + value );
				}else
					continue;
			}
		}else if( timeseries.isJsonObject() ) {
			JsonObject jObj = ( JsonObject ) timeseries;
			for( long i = lEnd; i <= lStart; i += msecPerDay ) {
				String index = new Long( i ).toString();
				JsonElement e = jObj.get( index );
				if( e != null ) {
					if( active )
						totalActiveDays++;
					totalValue += e.getAsDouble();
				}
			}
		}
		// System.out.println( "Total Value : " + totalValue +
		// " - Active Days  " + totalActiveDays );
		if( active ) {
			if( totalActiveDays == 0 )
				return 0;
			return totalValue / totalActiveDays;
		}else
			return totalValue / ( end - start + 2 );
	}

	// Zero based index 1 = current day, 35 - actually 35th day
	public boolean getContactCountExceeded( String channel, int start, int end ) {
		double countSoFar = getNumerOfContactsByChannel( channel, start, end );
		// System.out.println( "Contact So far : " + countSoFar );
		double limit = getNumericSupportingData( "GLOBAL_CONTACT_POLICY", channel + "_CONTACT_LIMIT_" + ( end - start ) );
		// System.out.println( "limit for : " + channel + " between " + start +
		// " - " + end + " = " + limit );
		if( countSoFar >= limit )
			return true;
		return false;
	}

	public double getNumerOfContactsByChannel( String channel, int start, int end ) {
		String indKey = channel + "_CONTACT_TIME_SERIES_LifeTime";
		return getTotalFromSeriesUsingSystemDate( indKey, start, end );
	}

	public JsonArray insertIntoSeries( String series, long newElementDayStamp, double newValue ) {
		String indKey = series + "-L";
		JsonElement timeseries = customerState.subscriberIndicators.get( indKey );
		JsonArray arrSeries = null;
		if( timeseries != null )
			arrSeries = timeseries.getAsJsonArray();
		else {
			arrSeries = new JsonArray();
			JsonArray newElement = new JsonArray();
			newElement.add( new JsonPrimitive( newElementDayStamp ) );
			newElement.add( new JsonPrimitive( newValue ) );
			arrSeries.add( newElement );
			return arrSeries;
		}

		boolean inserted = false;
		int indexToInsert = -1;

		for( indexToInsert = arrSeries.size() - 1; indexToInsert >= 0; indexToInsert-- ) {
			JsonArray existingEntry = arrSeries.get( indexToInsert ).getAsJsonArray();
			long existingDay = existingEntry.get( 0 ).getAsLong();
			if( existingDay == newElementDayStamp ) {
				newValue += existingEntry.get( 1 ).getAsDouble();
				JsonArray newElement = new JsonArray();
				newElement.add( new JsonPrimitive( newElementDayStamp ) );
				newElement.add( new JsonPrimitive( newValue ) );
				arrSeries.set( indexToInsert, newElement );
				inserted = true;
				break;
			}else if( existingDay < newElementDayStamp ) {
				break;
			}
		}
		if( inserted == false ) {
			JsonArray result = slice( arrSeries, 0, indexToInsert );
			JsonArray newElement = new JsonArray();
			newElement.add( new JsonPrimitive( newElementDayStamp ) );
			newElement.add( new JsonPrimitive( newValue ) );
			result.add( newElement );
			result.addAll( slice( arrSeries, indexToInsert + 1, arrSeries.size() - 1 ) );
			return result;
		}
		return arrSeries;
	}

	public JsonArray insertIntoSeries( JsonArray arrSeries, long newElementDayStamp, double newValue ) {
		// String indKey = series + "-L";
		// JsonElement timeseries = customerState.subscriberIndicators.get( indKey );
		// JsonArray arrSeries = null;
		if( arrSeries == null ) {
			arrSeries = new JsonArray();
			JsonArray newElement = new JsonArray();
			newElement.add( new JsonPrimitive( newElementDayStamp ) );
			newElement.add( new JsonPrimitive( newValue ) );
			arrSeries.add( newElement );
			return arrSeries;
		}

		boolean inserted = false;
		int indexToInsert = -1;

		for( indexToInsert = arrSeries.size() - 1; indexToInsert >= 0; indexToInsert-- ) {
			JsonArray existingEntry = arrSeries.get( indexToInsert ).getAsJsonArray();
			long existingDay = existingEntry.get( 0 ).getAsLong();
			if( existingDay == newElementDayStamp ) {
				newValue += existingEntry.get( 1 ).getAsDouble();
				JsonArray newElement = new JsonArray();
				newElement.add( new JsonPrimitive( newElementDayStamp ) );
				newElement.add( new JsonPrimitive( newValue ) );
				arrSeries.set( indexToInsert, newElement );
				inserted = true;
				break;
			}else if( existingDay < newElementDayStamp ) {
				break;
			}
		}
		if( inserted == false ) {
			JsonArray result = slice( arrSeries, 0, indexToInsert );
			JsonArray newElement = new JsonArray();
			newElement.add( new JsonPrimitive( newElementDayStamp ) );
			newElement.add( new JsonPrimitive( newValue ) );
			result.add( newElement );
			result.addAll( slice( arrSeries, indexToInsert + 1, arrSeries.size() - 1 ) );
			return result;
		}
		return arrSeries;
	}

	public JsonArray slice( JsonArray array, int start, int end ) {
		JsonArray result = new JsonArray();
		for( int i = start; i <= end; i++ )
			result.add( array.get( i ) );
		return result;
	}

	public double getTotalFromSeries( String timeSeriesIndicator, int start, int end ) {
		end = end - 1;
		if( start < 0 || end < 0 )
			return QUANTITY_DEFAULT;

		String indKey = timeSeriesIndicator + "-L";
		JsonElement timeseries = customerState.subscriberIndicators.get( indKey );
		if( timeseries == null )
			return QUANTITY_DEFAULT;

		double totalValue = 0;

		long lStart = getCurrentTime() - ( start * msecPerDay );
		lStart = lStart - ( lStart % msecPerDay );
		long lEnd = lStart - ( end * msecPerDay );

		if( timeseries.isJsonArray() ) {
			JsonArray timeArray = timeseries.getAsJsonArray();
			// System.out.println( lStart + " - " + lEnd );

			for( int i = timeArray.size() - 1; i >= 0; i-- ) {
				JsonArray thisEntry = timeArray.get( i ).getAsJsonArray();
				long timeInArray = thisEntry.get( 0 ).getAsLong();
				if( timeInArray >= lEnd && timeInArray <= lStart ) {
					double value = thisEntry.get( 1 ).getAsDouble();
					totalValue += value;
				}else
					continue;
			}
		}else if( timeseries.isJsonObject() ) {
			JsonObject jObj = ( JsonObject ) timeseries;
			for( long i = lEnd; i <= lStart; i += msecPerDay ) {
				String index = new Long( i ).toString();
				JsonElement e = jObj.get( index );
				if( e != null )
					totalValue += e.getAsDouble();
			}
		}
		return totalValue;
	}

	public double getTotalFromSeriesUsingSystemDate( String timeSeriesIndicator, int start, int end ) {
		if( end != 0 )
			end = end - 1;
		if( start < 0 || end < 0 )
			return QUANTITY_DEFAULT;

		String indKey = timeSeriesIndicator + "-L";
		JsonElement timeseries = customerState.subscriberIndicators.get( indKey );
		if( timeseries == null )
			return QUANTITY_DEFAULT;

		JsonArray timeArray = timeseries.getAsJsonArray();
		double totalValue = 0;

		long lStart = getCurrentTime() - ( start * msecPerDay );
		lStart = lStart - ( lStart % msecPerDay );
		long lEnd = lStart - ( end * msecPerDay );

		// System.out.println( lStart + " - " + lEnd );

		for( int i = timeArray.size() - 1; i >= 0; i-- ) {
			JsonArray thisEntry = timeArray.get( i ).getAsJsonArray();
			long timeInArray = thisEntry.get( 0 ).getAsLong();
			if( timeInArray >= lEnd && timeInArray <= lStart ) {
				double value = thisEntry.get( 1 ).getAsDouble();
				totalValue += value;
			}else
				continue;
		}
		return totalValue;
	}

	public ArrayList< Double > getSeriesElements( String timeSeriesIndicator, int start, int end ) {
		end = end - 1;
		if( start < 0 || end < 0 )
			return new ArrayList< Double >();

		ArrayList< Double > result = new ArrayList< Double >();
		String indKey = timeSeriesIndicator + "-L";
		JsonElement timeseries = customerState.subscriberIndicators.get( indKey );
		if( timeseries == null )
			return result;

		JsonArray timeArray = timeseries.getAsJsonArray();

		long lStart = customerState.thisRecordDate.getTime() - ( start * msecPerDay );
		lStart = lStart - ( lStart % msecPerDay );
		long lEnd = lStart - ( end * msecPerDay );

		// System.out.println( lStart + " - " + lEnd );

		for( int i = timeArray.size() - 1; i >= 0; i-- ) {
			JsonArray thisEntry = timeArray.get( i ).getAsJsonArray();
			long timeInArray = thisEntry.get( 0 ).getAsLong();
			if( timeInArray >= lEnd && timeInArray <= lStart ) {
				double value = thisEntry.get( 1 ).getAsDouble();
				result.add( new Double( value ) );
			}else
				continue;
		}
		return result;
	}

	public double getSeriesElement( String timeSeriesIndicator, int position ) {
		if( position < 0 )
			return DAYS_SINCE_DEFAULT;

		String indKey = timeSeriesIndicator + "-L";
		JsonElement timeseries = customerState.subscriberIndicators.get( indKey );
		if( timeseries == null )
			return DAYS_SINCE_DEFAULT;

		JsonArray timeArray = timeseries.getAsJsonArray();

		long lStart = customerState.thisRecordDate.getTime() - ( position * msecPerDay );
		lStart = lStart - ( lStart % msecPerDay );

		// System.out.println( position + " - " + lStart );

		for( int i = timeArray.size() - 1; i >= 0; i-- ) {
			JsonArray thisEntry = timeArray.get( i ).getAsJsonArray();
			long timeInArray = thisEntry.get( 0 ).getAsLong();
			if( timeInArray == lStart ) {
				return thisEntry.get( 1 ).getAsDouble();
			}else if( timeInArray < lStart )
				return 0;
		}
		return 0;
	}

	// "EVENT_TRIGGER_LifeTime-L": {
	// "DTAC_CHKBAL_DETECTED": {
	// "DTAC_CHKBAL_DETECTED": [
	// [
	// 1432107214322,
	// "66946527420-DTAC_CHKBAL_DETECTED-1432107214322"
	// ]
	// ]
	// }
	// },
	public double getTimeSinceLastTrigger( String eventId ) {
		JsonElement subscriberEvents = customerState.subscriberIndicators.get( "EVENT_TRIGGER_LifeTime-L" );
		if( subscriberEvents == null )
			return PERIOD_DEFAULT;
		JsonElement allTriggersForthisEvent = ( ( JsonObject ) subscriberEvents ).get( eventId );
		if( allTriggersForthisEvent == null )
			return PERIOD_DEFAULT;

		// main event and dependant event in the same object
		JsonObject thisTriggerHistory = ( JsonObject ) allTriggersForthisEvent;

		JsonElement thisTrigger = thisTriggerHistory.get( eventId );
		if( thisTrigger == null )
			return PERIOD_DEFAULT;

		JsonArray triggerArray = thisTrigger.getAsJsonArray();
		JsonArray lastTrigger = triggerArray.get( triggerArray.size() - 1 ).getAsJsonArray();
		return ( Calendar.getInstance().getTimeInMillis() - lastTrigger.get( 0 ).getAsLong() ) / ( 60 * 1000 );
	}

	public long getDaysSinceLastTrigger(String eventId) {
		JsonElement subscriberEvents = customerState.subscriberIndicators.get("EVENT_TRIGGER_LifeTime-L");
		if (subscriberEvents == null)
			return DAYS_SINCE_DEFAULT;
		JsonElement allTriggersForthisEvent = ((JsonObject) subscriberEvents).get(eventId);
		if (allTriggersForthisEvent == null)
			return DAYS_SINCE_DEFAULT;

		// main event and dependant event in the same object
		JsonObject thisTriggerHistory = (JsonObject) allTriggersForthisEvent;

		JsonElement thisTrigger = thisTriggerHistory.get(eventId);
		if (thisTrigger == null)
			return DAYS_SINCE_DEFAULT;

		Date today = new java.util.Date();
		long ltoday = today.getTime();
		ltoday += java.util.TimeZone.getDefault().getOffset(ltoday);
		JsonArray triggerArray = thisTrigger.getAsJsonArray();
		if (triggerArray.size() <= 0)
			return DAYS_SINCE_DEFAULT;
		JsonArray lastTrigger = triggerArray.get(triggerArray.size() - 1).getAsJsonArray();
		long trigTime = lastTrigger.get(0).getAsLong();

		ltoday = ltoday - (ltoday % 86400000l);
		trigTime = trigTime - (trigTime % 86400000l);
		if (ltoday < trigTime) {
			return DAYS_SINCE_DEFAULT;
		} else {
			return (ltoday - trigTime) / 86400000l;
		}
	}

	public double getDaysSinceLastReminderTrigger( String eventId, String reminderEventId ) {
		JsonElement subscriberEvents = customerState.subscriberIndicators.get( "EVENT_TRIGGER_LifeTime-L" );
		if( subscriberEvents == null )
			return PERIOD_DEFAULT;
		JsonElement allTriggersForthisEvent = ( ( JsonObject ) subscriberEvents ).get( eventId );
		if( allTriggersForthisEvent == null )
			return PERIOD_DEFAULT;

		// main event and dependant event in the same object
		JsonObject thisTriggerHistory = ( JsonObject ) allTriggersForthisEvent;

		JsonElement thisTrigger = thisTriggerHistory.get( reminderEventId );
		if( thisTrigger == null )
			return PERIOD_DEFAULT;

		// JsonArray triggerArray = thisTrigger.getAsJsonArray();
		// JsonArray lastTrigger = triggerArray.get( triggerArray.size() - 1
		// ).getAsJsonArray();
		// long trigTime = lastTrigger.get( 0 ).getAsLong();
		//
		// Calendar cal = Calendar.getInstance();
		// cal.setTimeInMillis( trigTime );
		//
		// Date today = new java.util.Date();
		// long ltoday = today.getTime();
		// ltoday += java.util.TimeZone.getDefault().getOffset( ltoday );
		// Calendar calToday = Calendar.getInstance();
		// calToday.setTimeInMillis( ltoday );

		Date today = new java.util.Date();
		long ltoday = today.getTime();

		JsonArray triggerArray = thisTrigger.getAsJsonArray();
		JsonArray lastTrigger = triggerArray.get( triggerArray.size() - 1 ).getAsJsonArray();
		long trigTime = lastTrigger.get( 0 ).getAsLong() - java.util.TimeZone.getDefault().getOffset( ltoday );

		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis( trigTime );

		// Date today = new java.util.Date();
		// long ltoday = today.getTime();
		// ltoday += java.util.TimeZone.getDefault().getOffset( ltoday );
		Calendar calToday = Calendar.getInstance();
		calToday.setTimeInMillis( ltoday );

		if( cal.get( Calendar.DAY_OF_MONTH ) == calToday.get( Calendar.DAY_OF_MONTH ) )
			return 0;
		else
			return calToday.get( Calendar.DAY_OF_MONTH ) - cal.get( Calendar.DAY_OF_MONTH );

	}

	public double getNumberOfTriggers( String eventId, int minutes ) {
		JsonElement subscriberEvents = customerState.subscriberIndicators.get( "EVENT_TRIGGER_LifeTime-L" );
		if( subscriberEvents == null )
			return QUANTITY_DEFAULT;
		JsonElement allTriggersForthisEvent = ( ( JsonObject ) subscriberEvents ).get( eventId );
		if( allTriggersForthisEvent == null )
			return QUANTITY_DEFAULT;

		// main event and dependant event in the same object
		JsonObject thisTriggerHistory = ( JsonObject ) allTriggersForthisEvent;

		JsonElement thisTrigger = thisTriggerHistory.get( eventId );
		if( thisTrigger == null )
			return QUANTITY_DEFAULT;

		JsonArray triggerArray = thisTrigger.getAsJsonArray();

		// long start = Calendar.getInstance().getTimeInMillis();

		Date today = new java.util.Date();
		long ltoday = today.getTime();
		ltoday += java.util.TimeZone.getDefault().getOffset( ltoday );
		long start = ltoday; // 22JUL

		long end = start - ( minutes * 60000l );
		int nCount = 0;
		for( int i = triggerArray.size() - 1; i >= 0; i-- ) {
			JsonArray lastTrigger = ( JsonArray ) triggerArray.get( i );
			long lthisTime = lastTrigger.get( 0 ).getAsLong();
			if( lthisTime >= end && lthisTime <= start ) {
				nCount++;
			}
		}
		return nCount;
	}

	public double getRevenueStretchCustomTime() {
		double r1 = getAverageFromSeries( "DTAC_REVENUE_SERIES_LifeTime", 1, 30, true );
		// System.out.println( "r1 : " + r1 );

		ArrayList< Double > r2Array = getSeriesElements( "DTAC_REVENUE_SERIES_LifeTime", 1, 29 );
		// for( int i=0; i<r2Array.size(); i++ )
		// System.out.println( "i = " + i + " : " + r2Array.get( i ) );
		double r2sum = 0;
		int numDays = 0;
		double limit = r1 * 0.5;
		for( int i = 0; i < r2Array.size(); i++ ) {
			double thisEntry = r2Array.get( i ).doubleValue();
			if( thisEntry > limit ) {
				r2sum += thisEntry;
				numDays++;
			}
		}
		double r2 = r2sum / numDays;
		// System.out.println( "r2 : " + r2 );
		double prevCyc = getAverageFromSeries( "DTAC_REVENUE_SERIES_LifeTime", 26, 10, true );
		double sens = prevCyc / r1;
		// System.out.println( "sens :" + sens );
		double s = getSeriesElement( "DTAC_REVENUE_SERIES_LifeTime", 0 );
		double r3 = java.lang.Math.max( r1, r2 );
		return ( r3 * sens ) - s;
	}

	public double timeSinceLastMonitoringStart( String eventId ) {
		long today = Calendar.getInstance().getTimeInMillis();
		JsonElement thisTag = customerState.subscriberEventTags.get( eventId );
		if( thisTag == null )
			return PERIOD_DEFAULT;
		JsonObject thisTagobj = ( JsonObject ) thisTag;
		JsonElement jele = thisTagobj.get( "MONITORING_START_TIME" );
		if( jele == null )
			return PERIOD_DEFAULT;
		return ( today - jele.getAsLong() ) / msecPerDay;
	}

	public double getPackageRevenue( int start, int end, String pkgCd, String mainAdd, String category, String duration, String totAvg, boolean active ) {
		// get the days. //sum the values.
		int numdays = end - 1;
		long lStart = customerState.thisRecordDate.getTime() - ( start * msecPerDay );
		lStart = lStart - ( lStart % msecPerDay );
		long lEnd = lStart - ( numdays * msecPerDay );

		JsonElement ePkgSeries = customerState.subscriberIndicators.get( "DTAC_PKG_SERIES_LifeTime-L" );
		if( ePkgSeries == null )
			return 0;
		JsonObject pkgSeries = ( JsonObject ) ePkgSeries;
		double totCharges = 0;
		int activeDays = 0;
		for( long i = lEnd; i <= lStart; i += msecPerDay ) {
			String index = new Long( i ).toString();
			JsonElement e = pkgSeries.get( index );
			if( e == null )
				continue;
			activeDays++;
			JsonArray allSubscriptions = ( JsonArray ) e;
			for( int j = 0; j < allSubscriptions.size(); j++ ) {
				JsonArray thisSubscription = allSubscriptions.get( j ).getAsJsonArray();
				JsonElement jCat = thisSubscription.get( 3 );
				String thisCat = "";
				if( !jCat.isJsonNull() )
					thisCat = jCat.getAsString();
				if( ( pkgCd.equals( "ALL" ) || thisSubscription.get( 1 ).getAsString().equals( pkgCd ) )
						&& ( mainAdd.equals( "ALL" ) || thisSubscription.get( 2 ).getAsString().equals( mainAdd ) )
						&& ( category.equals( "ALL" ) || category.indexOf( thisCat ) >= 0 )
						&& ( duration.equals( "ALL" ) || thisSubscription.get( 4 ).getAsString().equals( duration ) ) ) {
					totCharges += thisSubscription.get( 0 ).getAsDouble();
				}
			}
		}
		// System.out.println( "Active Days : " + activeDays );
		if( totAvg.equals( "TOTAL" ) )
			return totCharges;
		else {
			if( active )
				if( activeDays > 0 )
					return totCharges / activeDays;
				else
					return 0;
			else
				return totCharges / end;
		}
	}

	public double getCreditExpiryDaysCount() {
		JsonElement eCreditSeries = customerState.subscriberIndicators.get( "CREDIT_SERIES_LifeTime-L" );
		if( eCreditSeries == null )
			return QUANTITY_DEFAULT;
		JsonObject jCreditSeries = ( JsonObject ) eCreditSeries;
		JsonElement elatestCreditExpiryDate = jCreditSeries.get( "latestCreditExpiryDate" );
		if( elatestCreditExpiryDate == null )
			return QUANTITY_DEFAULT;
		long expiryDate = elatestCreditExpiryDate.getAsLong();
		// System.out.println( "callDate.getTime() : " + callDate.getTime() );
		// System.out.println( "expiryDate : " + expiryDate );
		// System.out.println(
		// "( expiryDate - callDate.getTime() ) / msecPerDay " + ( expiryDate -
		// callDate.getTime() ) / msecPerDay );
		// System.out.println( Math.round( ( expiryDate - callDate.getTime() ) /
		// dmsecPerDay ) );
		return Math.round( ( expiryDate - customerState.thisRecordDate.getTime() ) / dmsecPerDay );
	}

	public double getLatestCreditValue() {
		JsonElement eCreditSeries = customerState.subscriberIndicators.get( "CREDIT_SERIES_LifeTime-L" );
		if( eCreditSeries == null )
			return DAYS_SINCE_DEFAULT;
		JsonObject jCreditSeries = ( JsonObject ) eCreditSeries;
		JsonElement elatestCreditValue = jCreditSeries.get( "latestCreditValue" );
		if( elatestCreditValue == null )
			return DAYS_SINCE_DEFAULT;
		return elatestCreditValue.getAsDouble();
	}

	public double getPreviousCreditValue() {
		JsonElement eCreditSeries = customerState.subscriberIndicators.get( "CREDIT_SERIES_LifeTime-L" );
		if( eCreditSeries == null )
			return DAYS_SINCE_DEFAULT;
		JsonObject jCreditSeries = ( JsonObject ) eCreditSeries;
		JsonElement ePrevCreditValue = jCreditSeries.get( "previousCreditValue" );
		if( ePrevCreditValue == null )
			return DAYS_SINCE_DEFAULT;
		return ePrevCreditValue.getAsDouble();
	}

	public double getDaysSinceLastCredit() {
		JsonElement eCreditSeries = customerState.subscriberIndicators.get( "CREDIT_SERIES_LifeTime-L" );
		if( eCreditSeries == null )
			return PERIOD_DEFAULT;
		JsonObject jCreditSeries = ( JsonObject ) eCreditSeries;
		JsonElement elatestCreditActivityDate = jCreditSeries.get( "latestCreditActivityDate" );
		if( elatestCreditActivityDate == null )
			return PERIOD_DEFAULT;
		long latestDate = elatestCreditActivityDate.getAsLong();
		long currentTime = getCurrentTime();
		return Math.round( ( currentTime - latestDate ) / dmsecPerDay );
	}

	public double getDaysSinceLastBalanceUpdate() {
		JsonElement eLatestBalance = customerState.subscriberIndicators.get( "LATEST_BALANCE_LifeTime-L" );
		if( eLatestBalance == null )
			return PERIOD_DEFAULT;
		JsonObject jLatestBalance = ( JsonObject ) eLatestBalance;
		JsonElement elatestDate = jLatestBalance.get( "timestamp" );
		if( elatestDate == null )
			return PERIOD_DEFAULT;
		long latestDate = elatestDate.getAsLong();
		long currentTime = getCurrentTime();
		return Math.round( ( currentTime - latestDate ) / dmsecPerDay );
	}

	public String getLatestCreditMethod() {
		JsonElement eCreditSeries = customerState.subscriberIndicators.get( "CREDIT_SERIES_LifeTime-L" );
		if( eCreditSeries == null )
			return "";
		JsonObject jCreditSeries = ( JsonObject ) eCreditSeries;
		JsonElement elatestCreditMethod = jCreditSeries.get( "latestCreditMethod" );
		if( elatestCreditMethod == null )
			return "";
		return elatestCreditMethod.getAsString();
	}

	public double getLatestBalance() {
		JsonElement eLatestBalance = customerState.subscriberIndicators.get( "LATEST_BALANCE_LifeTime-L" );
		if( eLatestBalance == null )
			return -1;
		JsonObject jLatestBalance = ( JsonObject ) eLatestBalance;
		JsonElement eBalance = jLatestBalance.get( "balance" );
		if( eBalance == null )
			return -1;
		return eBalance.getAsDouble();
	}

	public boolean housekeepListSeries( JsonArray arr, int retentionDays ) {
		int delLimit = arr.size() - retentionDays;
		if( delLimit < 0 )
			return true;
		for( int i = 0; i < delLimit; i++ ) {
			arr.remove( 0 );
		}
		return true;
	}

	public boolean housekeepArraySeries( JsonArray arr, long limitDate, int cleanDaysCount ) {
		for( int i = 0; i < cleanDaysCount; i++ ) {
			if( arr.size() == 0 )
				return true;
			long thisDate = arr.get( 0 ).getAsJsonArray().get( 0 ).getAsLong();
			if( thisDate > limitDate )
				break;
			arr.remove( 0 );
		}

		return true;
	}

	public boolean housekeepMapSeries( JsonObject jobj, long limitDate, int cleanDaysCount ) {
		long startDay = limitDate;
		for( int i = 0; i < cleanDaysCount; i++ ) {
			limitDate = startDay - i * 86400000L;
			jobj.remove( new Long( limitDate ).toString() );
		}
		return true;
	}

	public boolean housekeep( String indicatorName, String indicatorType, int retentionDays, int cleanDaysCount ) {
		long limitDate = getCurrentTime() - retentionDays * 86400000L;
		limitDate -= limitDate % 86400000L;
		JsonElement ele = customerState.subscriberIndicators.get( indicatorName );
		if( ele == null )
			return true;
		if( indicatorType.equals( "SERIES" ) ) {
			JsonArray arr = ele.getAsJsonArray();
			if( arr.size() == 0 )
				return true;
			return housekeepArraySeries( arr, limitDate, cleanDaysCount );
		}else if( indicatorType.equals( "MAP" ) ) {
			JsonObject jobj = ele.getAsJsonObject();
			return housekeepMapSeries( jobj, limitDate, cleanDaysCount );
		}
		JsonArray arr = ele.getAsJsonArray();
		if( arr.size() == 0 )
			return true;
		return housekeepListSeries( arr, retentionDays );
	}

	public boolean isBirthDay() {
		int curMonth = getCalendarFactor( "MONTH_OF_YEAR" ) + 1;
		int curDay = getCalendarFactor( "DAY_OF_MONTH" );

		JsonElement ele = customerState.subscriberIndicators.get( "DOB_LifeTime-L" );
		if( ele == null )
			return false;
		String strbday = ele.getAsString();
		String[ ] comp = strbday.split( "-" );
		if( curDay == Integer.parseInt( comp[ 2 ] ) && curMonth == Integer.parseInt( comp[ 1 ] ) )
			return true;
		return false;
	}

	public double getDaysGivenEvent( String type ) {
		if( type.equals( "ACCOUNT_START" ) ) {
			JsonElement jele = customerState.subscriberIndicators.get( "ACCOUNT_STARTDATE_LifeTime-L" );
			if( jele == null )
				return PERIOD_DEFAULT;
			long accst = jele.getAsLong();
			return Math.round( ( customerState.tupleMap.get( "RECORD_TIMESTAMP" ).getAsLong() - accst ) / SiftMath.dmsecPerDay );
		}else if( type.equals( "INACTIVITY_END" ) ) {
			JsonElement jele = customerState.subscriberIndicators.get( "INACTIVITY_ENDDATE_LifeTime-L" );
			if( jele == null )
				return PERIOD_DEFAULT;
			long accst = jele.getAsLong();
			return Math.round( ( accst - Long.parseLong( customerState.tupleMap.get( "RECORD_TIMESTAMP" ).toString() ) ) / SiftMath.dmsecPerDay );
		}
		return PERIOD_DEFAULT;
	}

	public double quantile( JsonArray arr, double percent ) {
		// System.out.println( "Quantile Function Input : " + arr.size() + " - "
		// + arr.toString() );

		// Let n be the length of the (sorted) array and 0 < p <= 100 be the
		// desired percentile.
		// If n = 1 return the unique array element (regardless of the value of
		// p); otherwise
		// Compute the estimated percentile position pos = p * (n + 1) / 100 and
		// the difference, d between pos and floor(pos) (i.e. the fractional
		// part of pos).
		// If pos < 1 return the smallest element in the array.
		// Else if pos >= n return the largest element in the array.
		// Else let lower be the element in position floor(pos) in the array and
		// let upper be the next element in the array. Return lower + d * (upper
		// - lower)

		if( arr == null || arr.size() == 0 || percent > 1 )
			return -1;

		double[ ] darr = new double[ arr.size() ];
		for( int i = 0; i < arr.size(); i++ )
			darr[ i ] = arr.get( i ).getAsDouble();
		Arrays.sort( darr );

		int n = darr.length;
		if( n == 1 )
			return darr[ 0 ];

		double pos = percent * ( n + 1 );
		double ceil = Math.ceil( percent * ( n + 1 ) );
		double floor = Math.floor( percent * ( n + 1 ) );

		double d = pos - floor;

		if( pos >= n )
			return darr[ darr.length - 1 ];
		else if( floor - 1 < 0 )
			return darr[ 0 ];
		else {
			double lower = darr[ ( int ) floor - 1 ];
			double upper = darr[ ( int ) ceil - 1 ];
			return lower + d * ( upper - lower );
		}
	}

	public double getIntervalInSeries( String indicator, int start, int duration, String gapType ) {
		// get the days. //sum the values.
		int numdays = duration - 1;
		long lStart = customerState.thisRecordDate.getTime() - ( start * msecPerDay );
		lStart = lStart - ( lStart % msecPerDay );
		long lEnd = lStart - ( numdays * msecPerDay );

		// //Ignore the latest entry
		// if( start == 0 )
		// lStart = lStart - msecPerDay;

		JsonElement series = customerState.subscriberIndicators.get( indicator + "-L" );
		if( series == null )
			return 1000000;
		JsonObject jSeries = ( JsonObject ) series;
		java.util.ArrayList< Double > intervals = new java.util.ArrayList< Double >();
		long prev = 0;
		int count = -1;
		for( long i = lEnd; i <= lStart; i += msecPerDay ) {
			String index = new Long( i ).toString();
			JsonElement e = jSeries.get( index );
			if( e == null ) {
				// System.out.println( i + " - Skipping" );
				continue;
			}
			count++;
			// System.out.println( i + " - Porcessing---" );
			if( count == 0 ) {
				prev = i;
				continue;
			}
			double interval = ( i - prev ) / msecPerDay;
			intervals.add( ( interval > 0 ) ? ( interval - 1 ) : 0 );
			prev = i;
		}
		// System.out.println( "Active Days : " + activeDays );

		if( intervals.size() == 0 )
			return 1000000;

		double result = 0;
		if( gapType.equals( "SUM" ) ) {
			for( int i = 0; i < intervals.size(); i++ ) {
				result += intervals.get( i );
			}
			return result;
		}else if( gapType.equals( "AVERAGE" ) ) {
			for( int i = 0; i < intervals.size(); i++ ) {
				result += intervals.get( i );
			}
			return result / intervals.size();
		}else if( gapType.equals( "MIN" ) ) {
			result = 1000000;
			for( int i = 0; i < intervals.size(); i++ ) {
				if( intervals.get( i ) < result )
					result = intervals.get( i );
			}
			return result;
		}else if( gapType.equals( "MAX" ) ) {
			for( int i = 0; i < intervals.size(); i++ ) {
				if( intervals.get( i ) > result )
					result = intervals.get( i );
			}
			return result;
		}
		return duration;
	}

	public double getDaysSinceCommercialOfferChange() {
		JsonElement eCommOffer = customerState.subscriberIndicators.get( "COMMERCIAL_OFFER_LifeTime-L" );
		if( eCommOffer == null ) {
			return PERIOD_DEFAULT;
		}

		JsonObject jCommOffer = ( JsonObject ) eCommOffer;
		JsonElement prevCommOffer = jCommOffer.get( "previousCommercialOffer" );
		if( prevCommOffer == null )
			return PERIOD_DEFAULT;

		JsonElement eUpdateDate = jCommOffer.get( "updateDate" );
		if( eUpdateDate == null ) {
			return PERIOD_DEFAULT;
		}

		long latestDate = eUpdateDate.getAsLong();
		return Math.round( ( getCurrentTime() - latestDate ) / dmsecPerDay );
	}

	public int getRechargeCounts( double lowerLimit, double upperLimit, int startDay, int numDays ) {
		JsonElement eCreditSeries = customerState.subscriberIndicators.get( "CREDIT_SERIES_LifeTime-L" );
		if( eCreditSeries == null )
			return QUANTITY_DEFAULT;
		JsonObject jCreditSeries = ( JsonObject ) eCreditSeries;
		int numdays = numDays - 1;
		long lStart = getCurrentTime() - ( startDay * msecPerDay );
		lStart = lStart - ( lStart % msecPerDay );
		long lEnd = lStart - ( numdays * msecPerDay );

		int result = 0;
		for( long i = lEnd; i <= lStart; i += msecPerDay ) {
			String index = new Long( i ).toString();
			JsonElement e = jCreditSeries.get( index );
			if( e == null )
				continue;
			JsonArray outerArray = e.getAsJsonArray();
			for( int j = 0; j < outerArray.size(); j++ ) {
				JsonArray innerArray = outerArray.get( j ).getAsJsonArray();
				JsonElement eCreditValue = innerArray.get( 0 );
				if( eCreditValue != null && eCreditValue.getAsDouble() >= lowerLimit && ( upperLimit == -1 || eCreditValue.getAsDouble() <= upperLimit ) )
					result++;
			}
		}
		return result;
	}
	

	public static boolean evaluateStringOperations( String lhs, String rhs, String operation ) {
		if( operation.equals( "=" ) )
			return lhs.equals( rhs );
		else if( operation.equals( "!=" ) )
			return ! lhs.equals( rhs );
		else if( operation.equals( "CONTAINS" ) ) {
			String[] rhsValues = rhs.split( "\\|" );
			for( int i = 0; i < rhsValues.length; i++ ) {
				if( lhs.contains( rhsValues[ i ] ) )
					return true;
			}
			return false;
		}
		else
			return false;
	}

	public boolean evaluateNumericOperation( double lhs, double rhs, String operation ) {
		// =, >, >=, <, <=, !=
		if( operation.equals( "=" ) )
			return lhs == rhs;
		else if( operation.equals( ">" ) )
			return lhs > rhs;
		else if( operation.equals( ">=" ) )
			return lhs >= rhs;
		else if( operation.equals( "<" ) )
			return lhs < rhs;
		else if( operation.equals( "<=" ) )
			return lhs <= rhs;
		else if( operation.equals( "!=" ) )
			return lhs != rhs;
		return false;
	}

	public double getValueFromMultiSeries( JsonObject series, String operationType, int opColumn, String condColIndexs, String conditionValues,
			String condOperators, String condColTypes, int start, int duration ) {
		int end = duration - 1;
		if( ( start < 0 ) || ( end < 0 ) ) {
			return 0.0D;
		}

		if( series == null )
			return 0;

		String[ ] strCondColIndexs = condColIndexs.split( "," );
		String[ ] strConditionValues = conditionValues.split( "," );
		String[ ] strCondOperators = condOperators.split( "," );
		String[ ] strCondColTypes = condColTypes.split( "," );

		int[ ] nCondColIndexes = new int[ strCondColIndexs.length ];
		for( int i = 0; i < strCondColIndexs.length; i++ ) {
			nCondColIndexes[ i ] = Integer.parseInt( strCondColIndexs[ i ] );
		}
		long lStart = getCurrentTime() - start * 86400000L;
		lStart -= lStart % 86400000L;
		long lEnd = lStart - end * 86400000L;

		JsonElement eKeysArr = series.get( "keys" );
		ArrayList< Double > selectedValues = new ArrayList< Double >();
		int nActiveDays = 0;
		if( eKeysArr == null || eKeysArr.isJsonNull() ) {
			for( long i = lEnd; i <= lStart; i += msecPerDay ) {
				String index = new Long( i ).toString();
				JsonElement e = series.get( index );
				if( e == null )
					continue;
				JsonArray jValue = e.getAsJsonArray();
				boolean bAddedFromThisDay = false;
				for( int k = 0; k<jValue.size(); k++ ) {
					JsonArray jInnerValue = jValue.get( k ).getAsJsonArray();
					ArrayList< Boolean > results = new ArrayList< Boolean >();
					for( int j=0; j<nCondColIndexes.length; j++ ) {
						String lhs = jInnerValue.get( nCondColIndexes[ j ] ).getAsString();
						String rhs = strConditionValues[ j ];
						if( strCondColTypes[ j ].equals( "String" ) && ! evaluateStringOperations( lhs, rhs, strCondOperators[ j ] ) ) 
							break;
						else if( ! strCondColTypes[ j ].equals( "String" ) && ! evaluateNumericOperation( Double.parseDouble( lhs ), Double.parseDouble( rhs ), strCondOperators[ j ] ) )
							break;
						results.add( true );
					}
					if( results.size() == nCondColIndexes.length ) {
						selectedValues.add( jInnerValue.get( opColumn ).getAsDouble() );
						bAddedFromThisDay = true;
					}    
				}
				if( bAddedFromThisDay )
					nActiveDays++;
			}
		}else {
			JsonArray jKeysArr = eKeysArr.getAsJsonArray();
			for( int i = ( jKeysArr.size() - 1 ); i >= 0; i-- ) {
				long currentKey = jKeysArr.get( i ).getAsLong();
				if( currentKey < lEnd )
					break;
				if( currentKey > lStart )
					continue;
				String index = new Long( currentKey ).toString();
				JsonElement e = series.get( index );
				if( e == null )
					continue;
				JsonArray jValue = e.getAsJsonArray();
				ArrayList< Boolean > results = new ArrayList< Boolean >();
				for( int j = 0; j < nCondColIndexes.length; j++ ) {
					String lhs = jValue.get( nCondColIndexes[ j ] ).getAsString();
					String rhs = strConditionValues[ j ];
					if( strCondColTypes[ j ].equals( "String" ) && !evaluateStringOperations( lhs, rhs, strCondOperators[ j ] ) )
						break;
					else if( !strCondColTypes[ j ].equals( "String" )
							&& !evaluateNumericOperation( Double.parseDouble( lhs ), Double.parseDouble( rhs ), strCondOperators[ j ] ) )
						break;
					results.add( true );
				}
				if( results.size() == nCondColIndexes.length )
					selectedValues.add( jValue.get( opColumn ).getAsDouble() );
			}
		}
		
		if( selectedValues.size() == 0 )
			return 0;

		double[] dRes = new double[ selectedValues.size() ];
		for( int i=0; i<selectedValues.size(); i++ )
			dRes[ i ] = selectedValues.get( i );
		///SUM,COUNT,MODE,MEAN,MAX
		if(  operationType.equals( "SUM" ) ) {
			Sum sum = new Sum();
			return sum.evaluate( dRes );
		} else if( operationType.equals( "COUNT"  ) )
		    return dRes.length;
		else if( operationType.equals( "MEAN" ) ) {
			Mean mean = new Mean();
			return mean.evaluate( dRes );
		} else if( operationType.equals( "MEAN_ALLDAYS" ) ) {
			Sum sum = new Sum();
			return sum.evaluate( dRes ) / duration;
		} else if( operationType.equals( "MEAN_ACTIVEDAYS" ) ) {
			Sum sum = new Sum();
			return sum.evaluate( dRes ) / nActiveDays;
		} else if(operationType.equals( "MIN" ) ) {
			Min min = new Min();
			return min.evaluate( dRes );
		} else if(operationType.equals( "MEDIAN" ) ) {
			Median med = new Median();
			return med.evaluate( dRes );
		} else if( operationType.equals( "MAX" ) ) {
			Max max = new Max();
			return max.evaluate( dRes );
		} else if( operationType.equals( "MODE" ) ) { 
			double maxValue = 0, maxCount = 0;
		    for (int i = 0; i < dRes.length; ++i) {
		        double count = 0;
		        for (int j = 0; j < dRes.length; ++j) {
		            if (dRes[j] == dRes[i]) ++count;
		        }
		        if (count >= maxCount) {
		            maxCount = count;
		            maxValue = dRes[i];
		        }
		    }
		    return maxValue;
		}
		return QUANTITY_DEFAULT;
	}

	public long convertDateTimeToLong( String dateTime, String format, int hours, int mins ) throws SiftDateFormatException {
		// System.out.println("inside convertDateTimeToLong - converting string : "
		// + dateTime + " from " + format + " to long");
		SimpleDateFormat SDF = new SimpleDateFormat( format );
		SDF.setTimeZone( TimeZone.getTimeZone( "GMT" ) );
		Date date;
		long finalDateTime = 0;
		try {
			if( dateTime.length() != format.length() )
				throw new Exception();
			date = SDF.parse( dateTime );
			finalDateTime = date.getTime() + ( hours * 3600 * 1000 ) + ( mins * 60 * 1000 );
			if( finalDateTime < 0 )
				throw new Exception();
		}catch( Exception e ) {
			throw new SiftDateFormatException( e.getMessage() );
		}

		return finalDateTime;

	}

	public double convertStringToDollars( String val ) throws NumberFormatException {
		// +00061000000
		if( val.length() == 12 ) {
			double result;
			String sign = val.substring( 0, 1 );
			String dollars = val.substring( 1, 6 );
			String cents = val.substring( 6, 12 );
			result = Double.parseDouble( dollars );
			result += Double.parseDouble( "0." + cents );
			if( sign.equals( "-" ) )
				result *= -1.0d;
			return result;
		}else
			return 0.0d;
	}

	public Object getBucketValue( String indicatorName, String Id ) throws NumberFormatException {
		JsonElement jElement = customerState.subscriberIndicators.get( indicatorName + "-L" );
		if( jElement != null ) {
			JsonObject jObj = ( JsonObject ) jElement;
			JsonElement jDAElementVal = jObj.get( Id );
			if( jDAElementVal != null )
				return jDAElementVal;
			else
				return DAYS_SINCE_DEFAULT;
		}
		return DAYS_SINCE_DEFAULT;
	}

	public String getRawField( int start, int length ) throws SiftFeedRejectException, SiftMandatoryFieldException {
		if( start < 0 || ( start + length ) > customerState.tupleString.length() )
			throw new SiftFeedRejectException( "Substring Out of Bounds" );
		String result = customerState.tupleString.substring( start, start + length ).trim();
		if( result.length() == 0 ) {
			if( ! customerState.currentMapping.get( "isMandatory" ).getAsString().equals( "Y" ) )
				return customerState.currentMapping.get( "defaultValue" ).getAsString();
			else
				throw new SiftMandatoryFieldException( "Missing Mandatory Field" );
		}
		return result;
	}

	public String getRawField( int index ) throws SiftFeedRejectException, SiftMandatoryFieldException {
		if( index > customerState.tupleStringArray.length )
			throw new SiftFeedRejectException( "Substring Out of Bounds" );
		String result = customerState.tupleStringArray[ index ].trim();
		if( result.length() == 0 ) {
			if( ! customerState.currentMapping.get( "isMandatory" ).getAsString().equals( "Y" ) )
				return customerState.currentMapping.get( "defaultValue" ).getAsString();
			else
				throw new SiftMandatoryFieldException( "Missing Mandatory Field" );
		}
		return result;
	}

	public String formatDate( int dayOffset, String format ) {
		long now = getCurrentTime() + ( dayOffset * SiftMath.msecPerDay );
		SimpleDateFormat df = new SimpleDateFormat( format );
		df.setTimeZone( TimeZone.getTimeZone( "GMT" ) );
		return df.format( new java.util.Date( now ) );
	}

	public String getCurrentRecordTimeBlock() {
		long recTime = customerState.tupleMap.get( "RECORD_TIMESTAMP" ).getAsLong();
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis( recTime );
		int recordHour = cal.get( java.util.Calendar.HOUR_OF_DAY );
		String key = "-99";
		if( recordHour >= 9 && recordHour <= 11 )
			key = "BLOCK_9-11";
		else if( recordHour >= 12 && recordHour <= 14 )
			key = "BLOCK_12-14";
		else if( recordHour >= 15 && recordHour <= 17 )
			key = "BLOCK_15-17";
		else if( recordHour == 18 || recordHour == 19 )
			key = "BLOCK_18-19";
		return key;
	}

	public double getWeekendValueFromMultiSeries( JsonObject series, String operationType, int opColumn, String condColIndexs, String conditionValues,
			String condOperators, String condColTypes, int start, int duration ) {
		int end = duration - 1;
		if( ( start < 0 ) || ( end < 0 ) ) {
			return 0.0D;
		}
		String[ ] strCondColIndexs = condColIndexs.split( "," );
		String[ ] strConditionValues = conditionValues.split( "," );
		String[ ] strCondOperators = condOperators.split( "," );
		String[ ] strCondColTypes = condColTypes.split( "," );

		int[ ] nCondColIndexes = new int[ strCondColIndexs.length ];
		for( int i = 0; i < strCondColIndexs.length; i++ ) {
			nCondColIndexes[ i ] = Integer.parseInt( strCondColIndexs[ i ] );
		}
		long lStart = getCurrentTime() - start * 86400000L;
		lStart -= lStart % 86400000L;
		long lEnd = lStart - end * 86400000L;
		ArrayList selectedValues = new ArrayList();

		Date date = new Date( lStart );
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone( TimeZone.getTimeZone( "GMT" ) );
		int dayOfWeek = 0;

		for( long i = lEnd; i <= lStart; i += 86400000L ) {
			cal.setTime( new Date( i ) );

			dayOfWeek = cal.get( Calendar.DAY_OF_WEEK );

			if( dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY ) {

				String index = new Long( i ).toString();
				JsonElement e = series.get( index );
				if( e == null )
					continue;
				JsonArray jValue = e.getAsJsonArray();

				ArrayList results = new ArrayList();
				for( int j = 0; j < nCondColIndexes.length; j++ ) {
					String lhs = jValue.get( nCondColIndexes[ j ] ).getAsString();
					String rhs = strConditionValues[ j ];
					if( ( strCondColTypes[ j ].equals( "String" ) ) && ( ! evaluateStringOperations( lhs, rhs, strCondOperators[ j ] ) ) )
						break;
					if( ( !strCondColTypes[ j ].equals( "String" ) )
							&& ( ! evaluateNumericOperation( Double.parseDouble( lhs ), Double.parseDouble( rhs ), strCondOperators[ j ] ) ) )
						break;
					results.add( Boolean.valueOf( true ) );
				}
				if( results.size() == nCondColIndexes.length ) {
					selectedValues.add( Double.valueOf( jValue.get( opColumn ).getAsDouble() ) );

					// System.out.println("Selected values: " +
					// selectedValues.toString());
				}

				if( dayOfWeek == Calendar.SUNDAY ) {

					i += 6 * SiftMath.msecPerDay;
					cal.setTime( new Date( i ) );
				}
			}
		}
		double[ ] dRes = new double[ selectedValues.size() ];
		for( int i = 0; i < selectedValues.size(); i++ ) {
			dRes[ i ] = ( ( Double ) selectedValues.get( i ) ).doubleValue();
		}
		if( operationType.equals( "SUM" ) ) {
			Sum sum = new Sum();
			return sum.evaluate( dRes );
		}
		if( operationType.equals( "COUNT" ) )
			return dRes.length;
		if( operationType.equals( "MEAN" ) ) {
			Mean mean = new Mean();
			return mean.evaluate( dRes );
		}
		if( operationType.equals( "MIN" ) ) {
			Min min = new Min();
			return min.evaluate( dRes );
		}
		if( operationType.equals( "MEDIAN" ) ) {
			Median med = new Median();
			return med.evaluate( dRes );
		}
		if( operationType.equals( "MAX" ) ) {
			Max max = new Max();
			return max.evaluate( dRes );
		}
		if( operationType.equals( "MODE" ) ) {
			double maxValue = 0.0D;
			double maxCount = 0.0D;
			for( int i = 0; i < dRes.length; i++ ) {
				double count = 0.0D;
				for( int j = 0; j < dRes.length; j++ ) {
					if( dRes[ j ] != dRes[ i ] )
						continue;
					count += 1.0D;
				}
				if( count >= maxCount ) {
					maxCount = count;
					maxValue = dRes[ i ];
				}
			}
			return maxValue;
		}
		return 0.0D;

	}

	public String getMostActiveTimeBlock() {
		JsonElement jElement = customerState.subscriberIndicators.get( "PEAK_ACTIVITY_TIME_LifeTime-L" );
		if( jElement == null || jElement.toString().length() <= 2 )
			return "-99";
		else
			return jElement.getAsJsonObject().get( "MostActiveTimeBlock" ).getAsString();
	}

	public double getDaysSinceOfferTriggered( String programID, String offerID, String offerTriggerType ) {
		if( offerTriggerType.equals( "notify" ) || offerTriggerType.equals( "monitor" ) || offerTriggerType.equals( "fulfil" ) ) {
			JsonElement jProgramSet = customerState.subscriberIndicators.get( "PROGRAMS_LifeTime-L" );
			if( jProgramSet != null ) {
				JsonObject jProgramInstance = null;
				JsonArray programHistoryArray = null;
				JsonElement programHistory = ( ( JsonObject ) jProgramSet ).get( programID );
				if( programHistory != null ) {
					programHistoryArray = programHistory.getAsJsonArray();
					if( programHistoryArray.size() != 0 ) {
						for( int programElePos = programHistoryArray.size() - 1; programElePos >= 0; programElePos-- ) {
							jProgramInstance = programHistoryArray.get( programElePos ).getAsJsonObject();
							if( ! offerID.equals( "*" ) ) {
								JsonElement thisOfferHistory = jProgramInstance.get( offerID );
								if( thisOfferHistory != null ) {
									JsonArray thisOfferHistoryArray = thisOfferHistory.getAsJsonArray();
									if( thisOfferHistoryArray.size() > 0 ) {
										JsonObject latestOfferHistory = thisOfferHistoryArray.get( thisOfferHistoryArray.size() - 1 ).getAsJsonObject();
										JsonElement jele = null;
										if( offerTriggerType == "notify" )
											jele = latestOfferHistory.get( "notificationAction" );
										else if( offerTriggerType == "fulfil" )
											jele = latestOfferHistory.get( "fulfillmentAction" );
										else
											jele = latestOfferHistory.get( "monitoringAction" );
										// System.out.println("getDaysSinceOfferTriggered date  : "+jele
										// + " for type: "+ offerTriggerType);
										// System.out.println("getDaysSinceOfferTriggered currentTimestamp  : "+
										// getCurrentTime() );
										long currtime = getCurrentTime();
										long lCurrentDay = currtime - ( currtime % 86400000 );
										if( jele != null ) {
											long lTimeVal = jele.getAsLong();
											lTimeVal = lTimeVal - ( lTimeVal % 86400000 );
											return ( lCurrentDay - lTimeVal ) / 86400000;
										}
										break;
									}
								}
							}else {
								
								long latestOfferTriggerTime = 0;
								Iterator< Entry< String, JsonElement >> itr = jProgramInstance.entrySet().iterator();

								while( itr.hasNext() ) {
									String key = itr.next().getKey();
									JsonArray jOfferIdArr = jProgramInstance.get( key ).getAsJsonArray();
									JsonObject thisOfferIns = jOfferIdArr.get( jOfferIdArr.size() - 1 ).getAsJsonObject();
									JsonElement jele = null;
									if( offerTriggerType == "notify" )
										jele = thisOfferIns.get( "notificationAction" );
									else if( offerTriggerType == "fulfil" )
										jele = thisOfferIns.get( "fulfillmentAction" );
									else
										jele = thisOfferIns.get( "monitoringAction" );
									if ( jele != null ) {
									long contactTime = jele.getAsLong();
									if ( contactTime > latestOfferTriggerTime )
										latestOfferTriggerTime = contactTime;
									}
								}
								
								if( latestOfferTriggerTime == 0 )
									return DAYS_SINCE_DEFAULT;
								latestOfferTriggerTime = latestOfferTriggerTime - ( latestOfferTriggerTime % 86400000L );
								return ( getCurrentDay() - latestOfferTriggerTime ) / 86400000L;
							}
						}
					}
				}
			}

		}
		return DAYS_SINCE_DEFAULT;	
	}
	

	// This method checks if the packages frequencies match with the
	// corresponding values specified as per the corresponding operations.
	// canIncludeOtherPackages argument - checks if any other recharge packages
	// can be present in the subscriber indicator or not.
	public boolean getTopupFrequencyMatch( String packageNames, int startMonth, int numOfMonths, String topupFrequencies, String operations,
			boolean canIncludeOtherPackages ) {
		JsonElement ele = customerState.subscriberIndicators.get( "MONTHLY_RECHARGE_PACKAGES_LifeTime-L" );
		if( ele == null || ele.isJsonNull() || ele.toString().length() <= 2 )
			return false;

		Date dt = null;
		SimpleDateFormat df = new SimpleDateFormat( "yyyyMM" );
		try {
			dt = df.parse( Integer.toString( startMonth ) );
		}catch( ParseException pe ) {
			pe.printStackTrace();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime( dt );

		String[ ] packages = packageNames.split( "\\|" );
		String[ ] topupFreq = topupFrequencies.split( "\\|" );
		String[ ] opers = operations.split( "\\|" );

		JsonObject monthlyRechargeObj = ele.getAsJsonObject();
		String currMonthStr;
		int currMonthInt;
		String currMonth;
		JsonElement currentMonthPackagesEle;
		JsonObject currentActualMonthObj = null;

		for( int i = numOfMonths; i > 0; i-- ) {
			currMonthInt = cal.get( Calendar.MONTH );
			currMonthInt++; // As Calendar month index starts from 0.
			if( currMonthInt < 10 )
				currMonthStr = "0" + currMonthInt;
			else
				currMonthStr = Integer.toString( currMonthInt );
			currMonth = Integer.toString( cal.get( Calendar.YEAR ) ) + currMonthStr;

			currentMonthPackagesEle = monthlyRechargeObj.get( currMonth );

			for( int j = 0; j < packages.length; j++ ) {
				if( currentMonthPackagesEle == null && ( !opers[ j ].equalsIgnoreCase( "le" ) ) && ( ! ( Integer.parseInt( topupFreq[ j ] ) == 0 ) ) )
					return false;

				if( currentMonthPackagesEle != null ) {
					currentActualMonthObj = monthlyRechargeObj.get( currMonth ).getAsJsonObject();

					JsonElement actualPackageEle = currentActualMonthObj.get( packages[ j ].trim() );
					if( actualPackageEle == null && ( !opers[ j ].equalsIgnoreCase( "le" ) ) && ( ! ( Integer.parseInt( topupFreq[ j ] ) == 0 ) ) )
						return false;

					if( actualPackageEle != null ) {
						int packageCount = currentActualMonthObj.get( packages[ j ].trim() ).getAsInt();

						if( ( opers[ j ].equalsIgnoreCase( "eq" ) && packageCount != Integer.parseInt( topupFreq[ j ] ) )
								|| ( opers[ j ].equalsIgnoreCase( "ge" ) && packageCount < Integer.parseInt( topupFreq[ j ] ) )
								|| ( opers[ j ].equalsIgnoreCase( "le" ) && packageCount > Integer.parseInt( topupFreq[ j ] ) )
								|| ( opers[ j ].equalsIgnoreCase( "gt" ) && packageCount <= Integer.parseInt( topupFreq[ j ] ) )
								|| ( opers[ j ].equalsIgnoreCase( "lt" ) && packageCount >= Integer.parseInt( topupFreq[ j ] ) ) )
							return false;
					}
				}
			}

			if( !canIncludeOtherPackages && currentActualMonthObj != null ) {
				Iterator< Entry< String, JsonElement >> itr = currentActualMonthObj.entrySet().iterator();

				List< String > packageList = Arrays.asList( packages );
				while( itr.hasNext() ) {
					Entry< String, JsonElement > entry = itr.next();
					String existingPackage = entry.getKey();
					if( !packageList.contains( existingPackage ) )
						return false;
				}
			}
			cal.add( Calendar.MONTH, -1 );
		}
		return true;
	}

	// This method checks if the packages frequencies match with the
	// corresponding values specified as per the corresponding operations for x
	// months.
	// canIncludeOtherPackages argument - checks if any other recharge packages
	// can be present in the subscriber indicator or not.
	public boolean getTopupFrequencyMatchForXMonths( String packageNames, int startMonth, int numOfMonths, String topupFrequencies, String operations,
			boolean canIncludeOtherPackages ) {
		JsonElement ele = customerState.subscriberIndicators.get( "MONTHLY_RECHARGE_PACKAGES_LifeTime-L" );
		if( ele == null || ele.isJsonNull() || ele.toString().length() <= 2 )
			return false;

		Date dt = null;
		SimpleDateFormat df = new SimpleDateFormat( "yyyyMM" );
		try {
			dt = df.parse( Integer.toString( startMonth ) );
		}catch( ParseException pe ) {
			pe.printStackTrace();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime( dt );

		String[ ] packages = packageNames.split( "\\|" );
		String[ ] topupFreq = topupFrequencies.split( "\\|" );
		String[ ] opers = operations.split( "\\|" );
		Integer[ ] frequencies = new Integer[ packages.length ];
		Arrays.fill( frequencies, 0 );

		JsonObject monthlyRechargeObj = ele.getAsJsonObject();
		String currMonthStr;
		int currMonthInt;
		String currMonth;
		JsonElement currentMonthPackagesEle;
		JsonObject currentActualMonthObj = null;
		List< String > packageList = Arrays.asList( packages );

		for( int i = numOfMonths; i > 0; i-- ) {
			currMonthInt = cal.get( Calendar.MONTH );
			currMonthInt++; // As Calendar month index starts from 0.
			if( currMonthInt < 10 )
				currMonthStr = "0" + currMonthInt;
			else
				currMonthStr = Integer.toString( currMonthInt );
			currMonth = Integer.toString( cal.get( Calendar.YEAR ) ) + currMonthStr;

			currentMonthPackagesEle = monthlyRechargeObj.get( currMonth );
			for( int j = 0; j < packages.length; j++ ) {
				if( currentMonthPackagesEle == null )
					continue;

				currentActualMonthObj = monthlyRechargeObj.get( currMonth ).getAsJsonObject();

				JsonElement actualPackageEle = currentActualMonthObj.get( packages[ j ].trim() );
				if( actualPackageEle == null )
					continue;

				frequencies[ j ] += currentActualMonthObj.get( packages[ j ].trim() ).getAsInt();
			}

			if( !canIncludeOtherPackages && currentMonthPackagesEle != null ) {
				Iterator< Entry< String, JsonElement >> itr = currentActualMonthObj.entrySet().iterator();

				while( itr.hasNext() ) {
					Entry< String, JsonElement > entry = itr.next();
					String existingPackage = entry.getKey();
					if( !packageList.contains( existingPackage ) )
						return false;
				}
			}
			cal.add( Calendar.MONTH, -1 );
		}

		for( int h = 0; h < frequencies.length; h++ ) {
			if( ( opers[ h ].equalsIgnoreCase( "eq" ) && frequencies[ h ] != Integer.parseInt( topupFreq[ h ] ) )
					|| ( opers[ h ].equalsIgnoreCase( "ge" ) && frequencies[ h ] < Integer.parseInt( topupFreq[ h ] ) )
					|| ( opers[ h ].equalsIgnoreCase( "le" ) && frequencies[ h ] > Integer.parseInt( topupFreq[ h ] ) )
					|| ( opers[ h ].equalsIgnoreCase( "gt" ) && frequencies[ h ] <= Integer.parseInt( topupFreq[ h ] ) )
					|| ( opers[ h ].equalsIgnoreCase( "lt" ) && frequencies[ h ] >= Integer.parseInt( topupFreq[ h ] ) ) )
				return false;
		}

		return true;
	}

	public boolean houseKeepByIndicatorNames( String indicatorNames ) {
		String[ ] indicators = indicatorNames.split( "," );
		Iterator< Entry< String, JsonElement >> itr = customerState.subscriberIndicators.entrySet().iterator();
		while( itr.hasNext() ) {
			Entry< String, JsonElement > entry = itr.next();
			String key = entry.getKey();
			for( int i = 0; i < indicators.length; i++ ) {
				if( key.startsWith( indicators[ i ] ) && customerState.subscriberIndicators.has( key ) ) {
					// System.out.println( "Housekeeping : " + indicators[i] +
					// " - " + key );
					itr.remove();
				}
			}
		}
		return true;
	}

	public String getxmlElement( String tagName, String tagType ) throws SiftMandatoryFieldException {
		Element eElement = ( Element ) xmlNode;
		String[ ] innerTags = tagName.split( "/" );
		int k = 0;
		for( k = 0; k < innerTags.length - 1; k++ ) {
			NodeList nl = eElement.getElementsByTagName( innerTags[ k ] );
			if( nl.getLength() > 0 )
				eElement = ( Element ) nl.item( 0 );
			else {
				if( ! customerState.currentMapping.get( "isMandatory" ).getAsString().equals( "Y" ) )
					return customerState.currentMapping.get( "defaultValue" ).getAsString();
				else
					throw new SiftMandatoryFieldException( "Missing Mandatory Field" );
			}
		}
		if( tagType.equalsIgnoreCase( "element" ) ) {

			Node childNode = eElement.getFirstChild();
			eElement = ( Element ) childNode;
			if( eElement.getNodeName().equalsIgnoreCase( innerTags[ k ] ) ) {
				if( eElement.hasChildNodes() && eElement.getFirstChild().getNodeType() == Node.ELEMENT_NODE )
					return "-1";

				String rawField = eElement.getTextContent();

				if( rawField == null || rawField.equals( "" ) ) {
					if( ! customerState.currentMapping.get( "isMandatory" ).getAsString().equals( "Y" ) )
						return customerState.currentMapping.get( "defaultValue" ).getAsString();
					else
						throw new SiftMandatoryFieldException( "Missing Mandatory Field" );
				}else
					return rawField;
			}

			while( childNode.getNextSibling() != null ) {
				childNode = childNode.getNextSibling();
				eElement = ( Element ) childNode;
				if( eElement.getNodeName().equalsIgnoreCase( innerTags[ k ] ) ) {
					if( eElement.hasChildNodes() && eElement.getFirstChild().getNodeType() == Node.ELEMENT_NODE )
						return "-1";
					String rawField = eElement.getTextContent();

					if( rawField == null || rawField.equals( "" ) ) {
						if( ! customerState.currentMapping.get( "isMandatory" ).getAsString().equals( "Y" ) )
							return customerState.currentMapping.get( "defaultValue" ).getAsString();
						else
							throw new SiftMandatoryFieldException( "Missing Mandatory Field" );
					}else
						return rawField;
				}
			}
			return "-1";
			/*
			 * eElement = ( Element ) eElement.getElementsByTagName( innerTags[
			 * k ] ).item( 0 );//.getElementsByTagName( innerTags[ k ] ).item( 0
			 * ); if( ! eElement.getNodeName().equalsIgnoreCase( innerTags[ k ]
			 * ) ) return "-1";
			 * 
			 * System.out.println("Node name: " + eElement.getNodeName());
			 * String rawField = eElement.getTextContent();
			 * 
			 * if( rawField.equals( "" ) || rawField.equals( null ) ){ if( !
			 * currentMapping.get( "isMandatory" ).getAsString().equals( "Y" ) )
			 * return currentMapping.get( "defaultValue" ).getAsString(); else
			 * throw new SiftMandatoryFieldException( "Missing Mandatory Field"
			 * ); } else return rawField;
			 */
		}else if( tagType.equalsIgnoreCase( "attribute" ) ) {
			if( !eElement.hasAttribute( innerTags[ k ] ) ) {
				if( ! customerState.currentMapping.get( "isMandatory" ).getAsString().equals( "Y" ) )
					return customerState.currentMapping.get( "defaultValue" ).getAsString();
				else
					throw new SiftMandatoryFieldException( "Missing Mandatory Field" );
			}else
				return eElement.getAttribute( innerTags[ k ] );
		}else if( tagType.equalsIgnoreCase( "tag" ) ) {
			eElement = ( Element ) eElement.getElementsByTagName( innerTags[ k ] ).item( 0 );
			Node childNode = eElement.getFirstChild();
			if( childNode == null ) {
				if( ! customerState.currentMapping.get( "isMandatory" ).getAsString().equals( "Y" ) )
					return customerState.currentMapping.get( "defaultValue" ).getAsString();
				else
					throw new SiftMandatoryFieldException( "Missing Mandatory Field" );
			}else
				return childNode.getNodeName();
		}

		return "-1";
	}

	public List< String > getxmlElementList( String tagName ) {
		List< String > result = new ArrayList< String >();

		parseXMLRecursively( xmlNode, tagName, result );
		return result;
	}

	public void parseXMLRecursively( Node node, String tagName, List< String > values ) {
		NodeList list = node.getChildNodes();
		for( int i = 0; i < list.getLength(); i++ ) {
			Node childNode = list.item( i );
			if( childNode.getNodeType() == Node.ELEMENT_NODE ) {
				String nodeName = childNode.getNodeName();
				if( tagName.contains( "/" ) && !tagName.substring( 0, tagName.indexOf( "/" ) ).equalsIgnoreCase( nodeName ) )
					continue;

				if( !tagName.contains( "/" ) && tagName.equalsIgnoreCase( nodeName ) )
					values.add( childNode.getTextContent() );

				if( tagName.contains( "/" ) )
					parseXMLRecursively( childNode, tagName.substring( tagName.indexOf( "/" ) + 1, tagName.length() ), values );
			}
		}
	}

	public double getAccumulatedMonthlyTopups( int month, String excludePackages ) {
		JsonElement ele = customerState.subscriberIndicators.get( "MONTHLY_RECHARGE_PACKAGES_LifeTime-L" );
		if( ele == null || ele.isJsonNull() || ele.toString().length() <= 2 )
			return 0.0;

		double accumulatedTopups = 0.0;
		JsonObject monthlyRechargeObj = ele.getAsJsonObject();
		JsonElement currentMonthele = monthlyRechargeObj.get( Integer.toString( month ) );
		if( currentMonthele == null )
			return accumulatedTopups;

		JsonObject currentMonthObj = currentMonthele.getAsJsonObject();
		Iterator< Entry< String, JsonElement > > iter = currentMonthObj.entrySet().iterator();

		List< String > excludePkgList = Arrays.asList( excludePackages.split( "\\|" ) );
		while( iter.hasNext() ) {
			Entry< String, JsonElement > entry = iter.next();
			String packageName = entry.getKey().toString();

			if( excludePkgList.contains( packageName ) )
				continue;

			String amount = getSupportingData( "TOPUPCODE_PACKAGE_AMOUNT_MAPPING", packageName );
			if( amount != null && amount.length() != 0 )
				accumulatedTopups += entry.getValue().getAsDouble() * Double.valueOf( amount );
		}
		return accumulatedTopups;
	}

	public double getAccumulatedTopups( int start, int duration ) {
		JsonElement ele = customerState.subscriberIndicators.get( "CREDIT_SERIES_LifeTime-L" );
		if( ele == null || ele.isJsonNull() || ele.toString().length() <= 2 )
			return 0.0;

		double accumulatedTopups = 0.0;
		JsonObject creditSeriesObj = ele.getAsJsonObject();
		int end = duration - 1;
		if( ( start < 0 ) || ( end < 0 ) ) {
			return 0.0D;
		}
		long lStart = getCurrentTime() - start * 86400000L;
		lStart -= lStart % 86400000L;
		long lEnd = lStart - end * 86400000L;

		for( long i = lEnd; i <= lStart; i += 86400000 ) {
			JsonElement dayele = creditSeriesObj.get( Long.toString( i ) );
			if( dayele == null )
				continue;

			JsonObject dayObj = dayele.getAsJsonObject();
			Iterator< Entry< String, JsonElement > > iter = dayObj.entrySet().iterator();
			while( iter.hasNext() ) {
				Entry< String, JsonElement > entry = iter.next();
				accumulatedTopups += entry.getValue().getAsJsonArray().get( 0 ).getAsDouble();
			}
		}
		return accumulatedTopups;
	}

	public long getKeyFromKeyArray( JsonArray keyArr, long recordTimestamp ) {
		int keyArrSize = keyArr.size();
		for( int i = ( keyArrSize - 1 ); i >= 0; i-- ) {
			long currentElement = keyArr.get( i ).getAsLong();
			if( currentElement <= recordTimestamp )
				return currentElement;
			if( recordTimestamp < currentElement && i > 0 ) {
				long previousElement = keyArr.get( i - 1 ).getAsLong();
				if( previousElement < recordTimestamp )
					return previousElement;
			}else
				return keyArr.get( 0 ).getAsLong();

		}
		return -1L;
	}

	public JsonArray insertIntoKeyArray( JsonArray currentKeyArr, long newElement ) {
		int keyArrSize = currentKeyArr.size();
		if( keyArrSize == 0 ) {
			currentKeyArr.add( new JsonPrimitive( newElement ) );
			return currentKeyArr;
		}
		for( int i = ( keyArrSize - 1 ); i >= 0; i-- ) {
			long currentElement = currentKeyArr.get( i ).getAsLong();
			if( currentElement == newElement )
				return currentKeyArr;
			if( currentElement < newElement ) {
				currentKeyArr.add( new JsonPrimitive( newElement ) );
				return currentKeyArr;
			}
			if( newElement < currentElement && i > 0 ) {
				long previousElement = currentKeyArr.get( i - 1 ).getAsLong();
				if( previousElement < newElement ) {
					JsonArray newKeyArr = new JsonArray();
					for( int j = 0; j <= ( i - 1 ); j++ ) {
						newKeyArr.add( currentKeyArr.get( j ) );
					}
					newKeyArr.add( new JsonPrimitive( newElement ) );
					for( int k = i; k < keyArrSize; k++ ) {
						newKeyArr.add( currentKeyArr.get( k ) );
					}
					return newKeyArr;
				}
			}else {
				JsonArray newKeyArr = new JsonArray();
				newKeyArr.add( new JsonPrimitive( newElement ) );
				for( int j = 0; j < keyArrSize; j++ ) {
					newKeyArr.add( currentKeyArr.get( j ) );
				}
				return newKeyArr;
			}

		}
		return new JsonArray();
	}

	public JsonArray insertArrayIntoKeyArray( JsonArray currentKeyArr, JsonArray newArray ) {
		int keyArrSize = currentKeyArr.size();
		if( keyArrSize == 0 ) {
			currentKeyArr.add( newArray );
			return currentKeyArr;
		}
		for( int i = ( keyArrSize - 1 ); i >= 0; i-- ) {
			long currentElement = currentKeyArr.get( i ).getAsJsonArray().get( 0 ).getAsLong();
			long newElement = newArray.get( 0 ).getAsLong();

			if( currentElement <= newElement ) {
				currentKeyArr.add( newArray );
				return currentKeyArr;
			}
			if( newElement < currentElement && i > 0 ) {
				long previousElement = currentKeyArr.get( i - 1 ).getAsJsonArray().get( 0 ).getAsLong();
				if( previousElement < newElement ) {
					JsonArray newKeyArr = new JsonArray();
					for( int j = 0; j <= ( i - 1 ); j++ ) {
						newKeyArr.add( currentKeyArr.get( j ) );
					}
					newKeyArr.add( newArray );
					for( int k = i; k < keyArrSize; k++ ) {
						newKeyArr.add( currentKeyArr.get( k ) );
					}
					return newKeyArr;
				}
			}else {
				JsonArray newKeyArr = new JsonArray();
				newKeyArr.add( newArray );
				for( int j = 0; j < keyArrSize; j++ ) {
					newKeyArr.add( currentKeyArr.get( j ) );
				}
				return newKeyArr;
			}
		}
		return new JsonArray();
	}

	public double getNumOfDaysForPackageExpiry( String packageCategory ) {
		JsonElement ele = customerState.subscriberIndicators.get( "PACKAGE_TIMESERIES_LifeTime-L" );
		if( ele == null || ele.isJsonNull() || ele.toString().length() <= 2 )
			return -1;

		JsonObject packageObj = ele.getAsJsonObject();
		String currentPkg = "current" + packageCategory + "Package";

		JsonElement currentPkgEle = packageObj.get( currentPkg );
		if( currentPkgEle == null )
			return -1;
		if( !currentPkgEle.isJsonArray() )
			return -1;
		JsonArray currentPackage = currentPkgEle.getAsJsonArray();

		long expDate = currentPackage.get( 0 ).getAsLong();
		long currentTime = getCurrentTime();
		long currentDay = currentTime - ( currentTime % msecPerDay );
		if( expDate < currentDay )
			return -1;
		double numOfDaysToExpire = ( expDate - currentDay ) / msecPerDay;

		return numOfDaysToExpire;
	}

	public double getPackageCount( String packageCategory, String packageFrequency, String subscriptionType, String packageDefinition, String atlFlag,
			int start, int duration ) {
		JsonElement ele = customerState.subscriberIndicators.get( "PACKAGE_TIMESERIES_LifeTime-L" );
		if( ele == null || ele.isJsonNull() || ele.toString().length() <= 2 )
			return 0.0;

		int end = duration - 1;
		if( ( start < 0 ) || ( end < 0 ) ) {
			return 0.0D;
		}
		long lStart = getCurrentTime() - start * msecPerDay;
		lStart -= lStart % msecPerDay;
		long lEnd = lStart - end * msecPerDay;

		double count = 0.0;
		JsonObject packageObj = ele.getAsJsonObject();

		JsonElement keysElement = packageObj.get( "keys" );
		if( keysElement == null || keysElement.isJsonNull() )
			return 0.0;
		JsonArray keysArr = keysElement.getAsJsonArray();

		List< String > pkgCategoryList = Arrays.asList( packageCategory.split( "\\|" ) );
		List< String > pkgFrequencyList = Arrays.asList( packageFrequency.split( "\\|" ) );
		List< String > pkgSubsTypeList = Arrays.asList( subscriptionType.split( "\\|" ) );
		String[ ] pkgDefinitionArray = packageDefinition.split( "\\|" );

		for( int i = keysArr.size() - 1; i >= 0; i-- ) {
			long currentKey = keysArr.get( i ).getAsLong();
			if( currentKey < lEnd )
				return count;

			if( keysArr.get( i ).getAsLong() > lStart )
				continue;

			JsonElement dayEle = packageObj.get( Long.toString( currentKey ) );
			if( dayEle != null ) {
				JsonObject dayObj = dayEle.getAsJsonObject();
				Iterator< Entry< String, JsonElement > > iter = dayObj.entrySet().iterator();
				while( iter.hasNext() ) {
					Entry< String, JsonElement > entry = iter.next();
					JsonArray pkgArr = entry.getValue().getAsJsonArray();
					if( ( packageCategory.equalsIgnoreCase( "all" ) || pkgCategoryList.contains( pkgArr.get( 1 ).getAsString() ) )
							&& ( packageFrequency.equalsIgnoreCase( "all" ) || pkgFrequencyList.contains( pkgArr.get( 4 ).getAsString() ) )
							&& ( atlFlag.equalsIgnoreCase( "all" ) || atlFlag.equals( pkgArr.get( 7 ).getAsString() ) )
							&& ( subscriptionType.equalsIgnoreCase( "all" ) || pkgSubsTypeList.contains( pkgArr.get( 5 ).getAsString() ) ) ) {
						boolean flag = false;

						for( int j = 0; j < pkgDefinitionArray.length; j++ ) {
							if( pkgDefinitionArray[ j ].equalsIgnoreCase( "all" ) ) {
								flag = true;
								break;
							}
							if( ( pkgDefinitionArray[ j ].trim().startsWith( "!" ) ) && pkgDefinitionArray[ j ].trim().length() > 1 ) {
								if( pkgArr.get( 6 ).getAsString().toLowerCase()
										.contains( pkgDefinitionArray[ j ].trim().toLowerCase().substring( 1, pkgDefinitionArray[ j ].length() - 1 ) ) ) {
									flag = false;
									break;
								}else
									flag = true;
							}else if( ( !pkgDefinitionArray[ j ].trim().toLowerCase().startsWith( "!" ) && pkgArr.get( 6 ).getAsString().toLowerCase()
									.contains( pkgDefinitionArray[ j ].trim().toLowerCase() ) ) ) {
								flag = true;
							}
						}
						if( flag )
							count += pkgArr.get( 3 ).getAsDouble();
					}
				}
			}else
				continue;
		}

		return count;
	}

	public double getQuotaConsumedPercent( String daId ) {
		JsonElement ele = customerState.subscriberIndicators.get( "QUOTA_DAILY_SERIES_LifeTime-L" );
		if( ele == null || ele.isJsonNull() || ele.toString().length() <= 2 )
			return -1;

		JsonObject quotaObj = ele.getAsJsonObject();
		JsonElement daElement = quotaObj.get( daId );
		if( daElement == null || daElement.isJsonNull() )
			return -1;

		JsonObject daObject = daElement.getAsJsonObject();
		JsonElement totalQuotaEle = daObject.get( "QuotaLimit" );
		if( totalQuotaEle == null || totalQuotaEle.isJsonNull() )
			return -1;

		double totalQuota = totalQuotaEle.getAsDouble();
		JsonElement quotaConsumedEle = daObject.get( "QuotaConsumption" );
		if( quotaConsumedEle == null || quotaConsumedEle.isJsonNull() )
			return -1;

		JsonArray quotaConsumedArr = quotaConsumedEle.getAsJsonArray();
		double quotaConsumed = quotaConsumedArr.get( quotaConsumedArr.size() - 1 ).getAsDouble();

		return ( ( quotaConsumed / totalQuota ) * 100 );
	}

	public int getNumberOfTimesDAExceeded( String daIds, int start, int duration ) {
		JsonElement ele = customerState.subscriberIndicators.get( "QUOTA_DAILY_SERIES_LifeTime-L" );
		if( ele == null || ele.isJsonNull() || ele.toString().length() <= 2 )
			return -1;

		JsonObject quotaObject = ele.getAsJsonObject();
		int end = duration - 1;
		if( ( start < 0 ) || ( end < 0 ) ) {
			return 0;
		}

		long lStart = getCurrentTime() - start * msecPerDay;
		lStart -= lStart % msecPerDay;
		long lEnd = lStart - end * msecPerDay;
		lStart = lStart + ( msecPerDay - 1000 );

		if( daIds.equalsIgnoreCase( "all" ) ) {
			daIds = "";
			Iterator< Entry< String, JsonElement > > iter = quotaObject.entrySet().iterator();
			while( iter.hasNext() ) {
				Entry< String, JsonElement > entry = iter.next();
				String da = entry.getKey().toString();

				daIds += da + "|";
			}
			daIds = daIds.substring( 0, daIds.length() - 1 );
		}

		int count = 0;
		String[ ] daArr = daIds.split( "\\|" );

		for( int k = 0; k < daArr.length; k++ ) {
			JsonElement daEle = quotaObject.get( daArr[ k ] );
			if( daEle == null )
				continue;

			JsonObject daObject = daEle.getAsJsonObject();
			JsonElement exceededDatesEle = daObject.get( "QuotaExceededDates" );
			if( exceededDatesEle == null || exceededDatesEle.isJsonNull() )
				continue;

			JsonArray exceededDatesArr = exceededDatesEle.getAsJsonArray();
			for( int dateCount = exceededDatesArr.size() - 1; dateCount >= 0; dateCount-- ) {
				long currentDate = exceededDatesArr.get( dateCount ).getAsLong();
				if( currentDate > lStart )
					continue;

				if( currentDate != 0 && currentDate < lEnd )
					break;

				if( currentDate != 0 )
					count++;
			}
		}
		return count;
	}

	public int getUsageCount( String usageType, String network, int start, int duration ) {
		JsonElement ele = customerState.subscriberIndicators.get( "QUOTA_DAILY_SERIES_LifeTime-L" );
		if( ele == null || ele.isJsonNull() || ele.toString().length() <= 2 )
			return -1;

		JsonObject usageObj = ele.getAsJsonObject();
		JsonArray keysArray = usageObj.get( "keys" ).getAsJsonArray();

		int end = duration - 1;
		if( ( start < 0 ) || ( end < 0 ) ) {
			return -1;
		}

		long lStart = getCurrentTime() - start * 86400000L;
		lStart -= lStart % 86400000L;
		long lEnd = lStart - end * 86400000L;

		int count = 0;

		for( int i = keysArray.size() - 1; i >= 0; i-- ) {
			long currentKey = keysArray.get( i ).getAsLong();
			if( currentKey < lEnd )
				break;

			if( keysArray.get( i ).getAsLong() > lStart )
				continue;

			JsonElement dayEle = usageObj.get( Long.toString( currentKey ) );
			if( dayEle == null )
				continue;

			JsonArray dayArray = dayEle.getAsJsonArray();

			if( usageType.equalsIgnoreCase( "voice" ) ) {
				if( network.equalsIgnoreCase( "onnet" ) || network.equalsIgnoreCase( "both" ) ) {
					count += dayArray.get( 3 ).getAsInt();
				}else if( network.equalsIgnoreCase( "offnet" ) || network.equalsIgnoreCase( "both" ) ) {
					count += dayArray.get( 6 ).getAsInt();
				}
			}else if( usageType.equalsIgnoreCase( "sms" ) ) {
				if( network.equalsIgnoreCase( "onnet" ) || network.equalsIgnoreCase( "both" ) ) {
					count += dayArray.get( 14 ).getAsInt();
				}else if( network.equalsIgnoreCase( "offnet" ) || network.equalsIgnoreCase( "both" ) ) {
					count += dayArray.get( 16 ).getAsInt();
				}
			}
		}
		return count;
	}

	public double getDataUsage( int start, int duration ) {
		JsonElement ele = customerState.subscriberIndicators.get( "QUOTA_DAILY_SERIES_LifeTime-L" );
		if( ele == null || ele.isJsonNull() || ele.toString().length() <= 2 )
			return -1;

		JsonObject usageObj = ele.getAsJsonObject();
		JsonArray keysArray = usageObj.get( "keys" ).getAsJsonArray();

		int end = duration - 1;
		if( ( start < 0 ) || ( end < 0 ) ) {
			return -1;
		}

		long lStart = getCurrentTime() - start * 86400000L;
		lStart -= lStart % 86400000L;
		long lEnd = lStart - end * 86400000L;

		double count = 0;

		for( int i = keysArray.size() - 1; i >= 0; i-- ) {
			long currentKey = keysArray.get( i ).getAsLong();
			if( currentKey < lEnd )
				break;

			if( keysArray.get( i ).getAsLong() > lStart )
				continue;

			JsonElement dayEle = usageObj.get( Long.toString( currentKey ) );
			if( dayEle == null )
				continue;

			JsonArray dayArray = dayEle.getAsJsonArray();

			count += dayArray.get( 20 ).getAsDouble();
			count += dayArray.get( 21 ).getAsDouble();
		}
		return count;
	}

	public double getAveragePackagePurchaseDates( String packageName, int numOfRecharges, int numLatestRechargesExclude ) {
		JsonElement ele = customerState.subscriberIndicators.get( "PACKAGE_PURCHASE_SERIES_LifeTime-L" );
		if( ele == null || ele.isJsonNull() || ele.toString().length() <= 2 )
			return -1;

		if( numOfRecharges <= 1 )
			return 0.0;

		int numOfElements = 0;
		long currentDay = 0l;
		double differenceDaysCount = 0.0;

		JsonObject packageObj = ele.getAsJsonObject();
		JsonElement currentPkgEle = packageObj.get( packageName );
		if( currentPkgEle == null || currentPkgEle.toString().length() <= 2 )
			return 0.0;

		JsonArray currentPkgArray = currentPkgEle.getAsJsonArray();
		int currentSize = currentPkgArray.size();
		if( currentSize <= ( 1 + numLatestRechargesExclude ) )
			return 0.0;

		for( int i = ( currentSize - numLatestRechargesExclude ) - 1; i >= 0; i-- ) {
			numOfElements++;
			if( numOfElements > numOfRecharges ) {
				numOfElements--;
				break;
			}

			if( i == ( currentSize - numLatestRechargesExclude ) - 1 ) {
				currentDay = currentPkgArray.get( i ).getAsJsonArray().get( 0 ).getAsLong();
				continue;
			}

			long tmpDay = currentDay;
			currentDay = currentPkgArray.get( i ).getAsJsonArray().get( 0 ).getAsLong();
			differenceDaysCount += ( tmpDay - currentDay ) / msecPerDay;
		}
		return ( differenceDaysCount / ( numOfElements - 1 ) );
	}

	public String[ ] getCurrentPackageIdAndPrice( String packageCategory, String frequency ) {
		JsonElement ele = customerState.subscriberIndicators.get( "PACKAGE_TIMESERIES_LifeTime-L" );
		String[ ] packgOut = new String[ 2 ];
		if( ele == null || ele.isJsonNull() || ele.toString().length() <= 2 )
			return packgOut;

		JsonObject packageObj = ele.getAsJsonObject();
		String currentPkg = "current" + packageCategory + "Package";

		JsonElement currentPkgEle = packageObj.get( currentPkg );
		if( currentPkgEle == null )
			return packgOut;
		if( !currentPkgEle.isJsonArray() )
			return packgOut;

		JsonArray currentPackage = currentPkgEle.getAsJsonArray();
		String packageName = currentPackage.get( 8 ).getAsString();

		if( frequency.equalsIgnoreCase( currentPackage.get( 4 ).getAsString() ) ) {
			packgOut[ 0 ] = packageName;
			packgOut[ 1 ] = currentPackage.get( 2 ).getAsString();
		}

		return packgOut;
	}

	public boolean isFulfillmentTriggered( String programID, String offerID ) {
		JsonElement jProgramSet = customerState.subscriberIndicators.get( "PROGRAMS_LifeTime-L" );
		if( jProgramSet == null || jProgramSet.isJsonNull() )
			return false;

		JsonObject jProgramInstance = null;
		JsonArray programHistoryArray = null;
		JsonElement programHistory = ( ( JsonObject ) jProgramSet ).get( programID );
		if( programHistory != null ) {
			programHistoryArray = programHistory.getAsJsonArray();
			if( programHistoryArray.size() != 0 ) {
				for( int programElePos = programHistoryArray.size() - 1; programElePos >= 0; programElePos-- ) {
					jProgramInstance = programHistoryArray.get( programElePos ).getAsJsonObject();
					JsonElement thisOfferHistory = jProgramInstance.get( offerID );
					if( thisOfferHistory != null ) {
						JsonArray thisOfferHistoryArray = thisOfferHistory.getAsJsonArray();
						if( thisOfferHistoryArray.size() > 0 ) {
							JsonObject latestOfferHistory = thisOfferHistoryArray.get( thisOfferHistoryArray.size() - 1 ).getAsJsonObject();
							JsonElement jele = latestOfferHistory.get( "fulfillmentAction" );

							if( jele != null )
								return true;

							break;
						}
					}
				}
			}
		}
		return false;
	}

	public long getDAExceededDate( String daId ) {
		JsonElement ele = customerState.subscriberIndicators.get( "QUOTA_DAILY_SERIES_LifeTime-L" );
		if( ele == null || ele.isJsonNull() || ele.toString().length() <= 2 )
			return -1;

		JsonObject quotaObject = ele.getAsJsonObject();
		JsonElement daEle = quotaObject.get( daId );
		if( daEle == null )
			return -1;

		JsonObject daObj = daEle.getAsJsonObject();
		JsonElement quotaExceededDatesEle = daObj.get( "QuotaExceededDates" );
		if( quotaExceededDatesEle == null || quotaExceededDatesEle.isJsonNull() )
			return -1;

		JsonArray quotaExceededDatesArr = quotaExceededDatesEle.getAsJsonArray();
		long latestExceededDate = quotaExceededDatesArr.get( quotaExceededDatesArr.size() - 1 ).getAsLong();
		if( latestExceededDate != 0 )
			return latestExceededDate;

		return -1;
	}

	public int getTelcoCounts( String programId, String offerId, String triggerType ) {
		if( !triggerType.equals( "MonitoringCounts" ) && !triggerType.equals( "FulfillmentCounts" ) )
			return -1;

		String key = "TELCO_" + programId + "_" + offerId + "_" + triggerType;
		String strValue = ( String ) Commons.pandaCache.getClient().get( key );
		if( strValue == null )
			return 0;

		return Integer.parseInt( strValue );
	}

	public JsonObject evaluatePMMLModel( String jsonFileName ) {
		// System.out.println( "Evaluating RScript in  : " + jsonFileName );
		JsonObject pmmlDef = null;
		JsonObject jResults = new JsonObject();
		Map< FieldName, ? > pmmlResult = null;
		try {
			if( pmmlDefMap != null )
				pmmlDef = pmmlDefMap.get( jsonFileName );
			if( pmmlDef == null ) {
				String strScriptDef = readFile( jsonFileName );
				pmmlDef = ( JsonObject ) new JsonParser().parse( strScriptDef );
				pmmlDefMap = new java.util.HashMap< String, JsonObject >();
				pmmlDefMap.put( jsonFileName, pmmlDef );
			}

			String srcFile = pmmlDef.get( "pmmlSourcePath" ).getAsString();
			if( pmmlSrcMap == null ) {
				pmmlSrcMap = new java.util.HashMap< String, PMML >();
			}
			PMML thisModel = pmmlSrcMap.get( srcFile );
			if( thisModel == null ) {
				File thisFile = new File( srcFile );
				thisModel = readPmml( thisFile );
			}
			PMMLManager pmmlManager = new PMMLManager( thisModel );
			Evaluator evaluator = ( Evaluator ) pmmlManager.getModelManager( null, ModelEvaluatorFactory.getInstance() );
			Map< FieldName, org.jpmml.evaluator.FieldValue > pmmlArguments = new LinkedHashMap< FieldName, org.jpmml.evaluator.FieldValue >();

			JsonArray arrParamNames = pmmlDef.get( "paramNames" ).getAsJsonArray();
			JsonArray paramTypes = pmmlDef.get( "paramTypes" ).getAsJsonArray();
			String strParamTypes = new String();
			for( int i = 0; i < paramTypes.size(); i++ ) {
				if( i == 0 )
					strParamTypes += paramTypes.get( i ).getAsString();
				else
					strParamTypes += "~" + paramTypes.get( i ).getAsString();
			}
			Class[ ] allParamTypes = ExpressionUtil.stringToTypes( strParamTypes );
			JsonArray arrSiftParamNames = pmmlDef.get( "siftParamMap" ).getAsJsonArray();
			Object[ ] parameterValues = new Object[ arrParamNames.size() ];

			// populate variables from Sift to R
			for( int j = 0; j < arrSiftParamNames.size(); j++ ) {
				String thisParam = arrSiftParamNames.get( j ).getAsString();
				Object jValue = null;

				if( customerState.tupleMap != null && customerState.tupleMap.has( thisParam ) )
					jValue =  new JsonPrimitive( customerState.tupleMap.get( thisParam ).getAsString() );
				if( jValue == null ) {
					if( Commons.allIndDef.containsKey( thisParam ) ) {
						String key = getIndicatorKey( thisParam );
						jValue = customerState.subscriberIndicators.get( key );
					}else { // check in the statistical profile
						JsonElement statInd = customerState.subscriberIndicators.get( "SUBSCRIBER_STATISTICS_LifeTime-L" );
						if( statInd != null ) {
							JsonObject jStatInd = ( JsonObject ) statInd;
							JsonElement ele = jStatInd.get( thisParam );
							if( ele != null )
								jValue = ele;
						}
					}
				}
				// if( jValue != null )
				parameterValues[ j ] = ExpressionUtil.createObject( allParamTypes[ j ], jValue );
				// else
				// parameterValues[ j ] = new Double( 0 );
			}

			// System.out.println(
			// "----------------Start Listing Params to R Sscript-------" );
			for( int i = 0; i < arrParamNames.size(); i++ ) {
				// System.out.println( arrParamNames.get( i ).getAsString() +
				// " + " + parameterValues[ i ] );
				FieldName thisField = new FieldName( arrParamNames.get( i ).getAsString() );
				org.jpmml.evaluator.FieldValue pmmlValue = evaluator.prepare( thisField, ( parameterValues[ i ] != null ? parameterValues[ i ].toString()
						: null ) );
				pmmlArguments.put( thisField, pmmlValue );
			}
			// System.out.println(
			// "----------------End Listing Params to R Sscript-------" );
			pmmlResult = evaluator.evaluate( pmmlArguments );
		}catch( Exception e ) {
			System.out.println( e.toString() );
			// System.out.println( "--- Finished Evaluating RScript in  : " +
			// jsonFileName + " but returning empty" );
			return new JsonObject();
		}
		// System.out.println( "RComputed Value : " + result.get( 0 ) );
		JsonArray arrResults = pmmlDef.get( "returnValues" ).getAsJsonArray();
		JsonArray arrResultTypes = pmmlDef.get( "returnTypes" ).getAsJsonArray();
		for( int i = 0; i < arrResults.size(); i++ ) {
			String retName = arrResults.get( i ).getAsString();
			String retType = arrResultTypes.get( i ).getAsString();
			if( retType.equals( "double" ) )
				jResults.addProperty( retName, Double.parseDouble( pmmlResult.get( new FieldName( retName ) ).toString() ) );
			else if( retType.equals( "String" ) )
				jResults.addProperty( retName, pmmlResult.get( retName ).toString() );
		}
		// System.out.println( "--- Finished Evaluating RScript in  : " +
		// jsonFileName );
		return jResults;
	}

	public PMML readPmml( File file ) throws Exception {
		InputStream is = new FileInputStream( file );
		try {
			InputSource source = new InputSource( is );
			XMLReader reader = XMLReaderFactory.createXMLReader();
			SAXSource filteredSource = new SAXSource( new ImportFilter( reader ), source );
			Unmarshaller unmarshaller = getJAXBContext().createUnmarshaller();
			return ( PMML ) unmarshaller.unmarshal( filteredSource );
		}finally {
			is.close();
		}
	}

	private  JAXBContext getJAXBContext() throws JAXBException {
		if( jaxbCtx == null ) {
			jaxbCtx = JAXBContext.newInstance( ObjectFactory.class );
		}
		return jaxbCtx;
	}

	public Object execSiftFunction( String functionName, Object... args ) throws ParseException, NoSuchMethodException, InstantiationException,
			InvocationTargetException, IllegalAccessException {
		PExpressionEvaluator exp = Commons.expMap.get( functionName );

		String[ ] paramNames = exp.getParameterNames();
		Class[ ] paramTypes = exp.getParameterTypes();
		Object[ ] parameterValues = new Object[ paramNames.length ];
		//System.out.println( "********************************In ExecuteSift Function :" + functionName );
		for( int i = 0; i < paramNames.length; i++ ) {
			if( i < args.length )
				parameterValues[ i ] = args[ i ];
			else {
				String thisParam = paramNames[ i ];
				Object jValue = null;

				if( customerState.tupleMap != null && customerState.tupleMap.has( thisParam ) )
					jValue = new JsonPrimitive( customerState.tupleMap.get( thisParam ).getAsString() );
				if( jValue == null ) {// it must be one of the indicator
									  // instances
					JsonObject paramIndDef = Commons.allIndDef.get( thisParam );
					if( paramIndDef == null ) {
						if( Commons.allCIM.containsKey( thisParam ) )
							jValue = null;
						else if( thisParam.equals( "SIFTMATH" ) ) 
							jValue = customerState.siftMath;
						else { // this expression is not for this type of cdr
							break;
						}
					}else {
						if( ! customerState.processedInd.contains( thisParam ) )
							processIndicatorInFunctions( thisParam ); 
						String frequency = paramIndDef.get( "frequency" ).getAsString();
						String paramIndKey = generatekey( null, thisParam, frequency, customerState.thisRecordDate, thisTag );
						if(frequency.equals( "Session" ) )
							jValue = customerState.sessionIndicators.get( paramIndKey );
						else
							jValue = customerState.subscriberIndicators.get( paramIndKey );
					}
				}
				parameterValues[ i ] = ExpressionUtil.createObject( paramTypes[ i ], jValue );
			}
		}

		/*for( int i=0; i<paramNames.length; i++ ) { 
			System.out.println(  paramNames[ i ] ); 
			System.out.println( parameterValues[ i ] ); 
		}*/

		Object expressionValue = null;
		try {
			expressionValue = exp.evaluate( parameterValues );
		}
		catch( Exception ex ) {
			ex.printStackTrace();
			System.out.println( "Exception found in the expression: " + functionName + " : " + customerState.tupleMap.toString() );
			throw new NullPointerException();
		}
		return expressionValue;
	}

	public Integer execIntFunction( String functionName, Object... args ) throws NoSuchMethodException, InstantiationException,
			InvocationTargetException, IllegalAccessException, ParseException {
		return ( ( Integer ) execSiftFunction( functionName, args ) );
	}

	public Double execDoubleFunction( String functionName, Object... args ) throws NoSuchMethodException, InstantiationException,
			InvocationTargetException, IllegalAccessException, ParseException {
		return ( ( Double ) execSiftFunction( functionName, args ) );
	}

	public Long execLongFunction( String functionName, Object... args ) throws NoSuchMethodException, InstantiationException, InvocationTargetException,
			IllegalAccessException, ParseException {
		return ( ( Long ) execSiftFunction( functionName, args ) );
	}

	public Boolean execBooleanFunction( String functionName, Object... args ) throws NoSuchMethodException, InstantiationException,
			InvocationTargetException, IllegalAccessException, ParseException {
		return ( ( Boolean ) execSiftFunction( functionName, args ) );
	}

	public Float execFloatFunction( String functionName, Object... args ) throws NoSuchMethodException, InstantiationException,
			InvocationTargetException, IllegalAccessException, ParseException {
		return ( ( Float ) execSiftFunction( functionName, args ) );
	}

	public String execStringFunction( String functionName, Object... args ) throws NoSuchMethodException, InstantiationException,
			InvocationTargetException, IllegalAccessException, ParseException {
		return ( ( String ) execSiftFunction( functionName, args ) );
	}

	public JsonArray execJsonArrayFunction( String functionName, Object... args ) throws NoSuchMethodException, InstantiationException,
			InvocationTargetException, IllegalAccessException, ParseException {
		return ( ( JsonArray ) execSiftFunction( functionName, args ) );
	}

	public JsonObject execJsonObjectFunction( String functionName, Object... args ) throws NoSuchMethodException, InstantiationException,
			InvocationTargetException, IllegalAccessException, ParseException {
		return ( ( JsonObject ) execSiftFunction( functionName, args ) );
	}

	public Double[ ] execDoubleArrayFunction( String functionName, Object... args ) throws NoSuchMethodException, InstantiationException,
			InvocationTargetException, IllegalAccessException, ParseException {
		return ( ( Double[ ] ) execSiftFunction( functionName, args ) );
	}

	public String[ ] execStringArrayFunction( String functionName, Object... args ) throws NoSuchMethodException, InstantiationException,
			InvocationTargetException, IllegalAccessException, ParseException {
		return ( ( String[ ] ) execSiftFunction( functionName, args ) );
	}

	public Integer[ ] execIntegerArrayFunction( String functionName, Object... args ) throws NoSuchMethodException, InstantiationException,
			InvocationTargetException, IllegalAccessException, ParseException {
		return ( ( Integer[ ] ) execSiftFunction( functionName, args ) );
	}

	public Long[ ] execLongArrayFunction( String functionName, Object... args ) throws NoSuchMethodException, InstantiationException,
			InvocationTargetException, IllegalAccessException, ParseException {
		return ( ( Long[ ] ) execSiftFunction( functionName, args ) );
	}

	public void processIndicator( String thisId ) throws ParseException, NoSuchMethodException, InstantiationException, InvocationTargetException, IllegalAccessException {
		customerState.processedInd.add( thisId );

		PExpressionEvaluator exp = Commons.expMap.get( thisId );
		if( exp == null )
			return;

		String[ ] paramNames = exp.getParameterNames();
		Class[ ] paramTypes = exp.getParameterTypes();
		Object[ ] parameterValues = new Object[ paramNames.length ];
		boolean expressionApplicable = true;
		for( int j = 0; j < paramNames.length; j++ ) {
			String thisParam = paramNames[ j ];
			Object jValue = null;

			if( customerState.tupleMap != null && customerState.tupleMap.has( thisParam ) )
				jValue = new JsonPrimitive( customerState.tupleMap.get( thisParam ).getAsString() );
			if( jValue == null ) {// it must be one of the indicator instances
				JsonObject paramIndDef = Commons.allIndDef.get( thisParam );
				if( paramIndDef == null ) {
					if( Commons.allCIM.containsKey( thisParam ) )
						jValue = null;
					else if( thisParam.equals( "SIFTMATH" ) )
						jValue = customerState.siftMath;
					else { // this expression is not for this type of cdr
						expressionApplicable = false;
						break;
					}
				}else {
					String frequency = paramIndDef.get( "frequency" ).getAsString();
					String paramIndKey = generatekey( null, thisParam, frequency, customerState.thisRecordDate, thisTag );
					if( !thisId.equals( thisParam ) )
						processIndicator( thisParam );
					if( frequency.equals( "Session" ) )
						jValue = customerState.sessionIndicators.get( paramIndKey );
					else
						jValue = customerState.subscriberIndicators.get( paramIndKey );
				}
			}
			parameterValues[ j ] = ExpressionUtil.createObject( paramTypes[ j ], jValue );
		}
		if( !expressionApplicable ) {
			return;
		}
		Object expressionValue = null;
		try{ 
			expressionValue = exp.evaluate( parameterValues );
		}
		catch( Exception ex ) {
			ex.printStackTrace();
			System.out.println( "Exception found in the expression: " + thisId + " : " + customerState.tupleMap.toString() );
			throw new NullPointerException();
		}
		
		JsonObject thisIndDef = Commons.allIndDef.get( thisId );
		String frequency = thisIndDef.get( "frequency" ).getAsString();
		String paramIndKey = generatekey( null, thisId, frequency, customerState.thisRecordDate, thisTag );
		addToIndicator( paramIndKey, expressionValue, exp.getOptionalExpressionType(), false, frequency );
		// System.out.println( thisId +
		// " : ------Sift Math Computed Indicator : " +
		// customerState.subscriberIndicators.toString() );
	}

	public void processIndicatorInFunctions( String thisId ) throws ParseException, NoSuchMethodException, InstantiationException, InvocationTargetException,
			IllegalAccessException {
		if( customerState.processedInd.contains( thisId ) )
			return;

		Commons.logger.info( "Processing Indciator : " + thisId );
		customerState.processedInd.add( thisId );

		PExpressionEvaluator exp = Commons.expMap.get( thisId );
		if( exp == null )
			return;

		String[ ] paramNames = exp.getParameterNames();
		Class[ ] paramTypes = exp.getParameterTypes();
		Object[ ] parameterValues = new Object[ paramNames.length ];
		boolean expressionApplicable = true;
		for( int j = 0; j < paramNames.length; j++ ) {
			String thisParam = paramNames[ j ];
			Object jValue = null;

			if( customerState.tupleMap != null && customerState.tupleMap.has( thisParam ) )
				jValue = new JsonPrimitive( customerState.tupleMap.get( thisParam ).getAsString() );
			if( jValue == null ) {// it must be one of the indicator instances
				JsonObject paramIndDef = Commons.allIndDef.get( thisParam );
				if( paramIndDef == null ) {
					if( Commons.allCIM.containsKey( thisParam ) )
						jValue = null;
					else if( thisParam.equals( "SIFTMATH" ) ) 
						jValue = customerState.siftMath;
					else { // this expression is not for this type of cdr
						expressionApplicable = false;
						break;
					}
				}else {
					String frequency = paramIndDef.get( "frequency" ).getAsString();
					String paramIndKey = generatekey( null, thisParam, frequency, customerState.thisRecordDate, thisTag );
					if( !thisId.equals( thisParam ) )
						processIndicatorInFunctions( thisParam );
					if(frequency.equals("Session"))
						jValue = customerState.sessionIndicators.get( paramIndKey );
					else
					jValue = customerState.subscriberIndicators.get( paramIndKey );
				}
			}
			parameterValues[ j ] = ExpressionUtil.createObject( paramTypes[ j ], jValue );
		}
		if( !expressionApplicable ) {
			return;
		}
		Object expressionValue = null;
		try {
			expressionValue = exp.evaluate( parameterValues );
		}
		catch( Exception ex ) {
			ex.printStackTrace();
			System.out.println( "Exception found in the expression: " + thisId + " : " + customerState.tupleMap.toString() );
			throw new InvocationTargetException( null, ex.getMessage() );
		}
		
		JsonObject thisIndDef = Commons.allIndDef.get( thisId );
		String frequency = thisIndDef.get( "frequency" ).getAsString();
		String paramIndKey = generatekey( null, thisId, frequency, customerState.thisRecordDate, thisTag );
		addToIndicator( paramIndKey, expressionValue, exp.getOptionalExpressionType(), false, frequency );
		Commons.logger.info( "Execution Result for Indciator : " + thisId + " = " +  expressionValue.toString() );
	}

	public void addToIndicator( String key, Object value, Class type, boolean programGroup, String frequency ) {
		JsonObject destination = customerState.subscriberIndicators;
		if( frequency.equals( "Session" ) )
			destination = customerState.sessionIndicators;
		if( programGroup ) {
			JsonElement eDesitnation = customerState.subscriberIndicators.get( "PROGRAM_PARAMETERS-L" );
			if( eDesitnation == null ) {
				destination = new JsonObject();
				customerState.subscriberIndicators.add( "PROGRAM_PARAMETERS-L", destination );
			}else
				destination = eDesitnation.getAsJsonObject();
		}
		
		boolean addDefaultvalueInd = Commons.siftConfig.get( "storeDefaultValueIndicators" ).getAsBoolean();
		
		if( type == double.class ) {
			destination.addProperty( key, ( Double ) value );
			if( ! addDefaultvalueInd && ( ( Double ) value ).doubleValue() == 0 ) 
				destination.remove( key );
		}else if( type == int.class ) {
			destination.addProperty( key, ( Integer ) value );
			if( ! addDefaultvalueInd && ( ( Integer ) value ).intValue() == 0  )
				destination.remove( key );
		}else if( type == long.class ) {
			destination.addProperty( key, ( Long ) value );
			if( ! addDefaultvalueInd && ( ( Long ) value ).longValue() == 0 )
				destination.remove( key );
		}else if( type == Double.class )
			destination.addProperty( key, ( Double ) value );
		else if( type == Boolean.class )
			destination.addProperty( key, ( Boolean ) value );
		else if( type == Float.class )
			destination.addProperty( key, ( Float ) value );
		else if( type == Integer.class )
			destination.addProperty( key, ( Integer ) value );
		else if( type == String.class )
			destination.addProperty( key, ( String ) value );
		else if( type == boolean.class )
			destination.addProperty( key, ( Boolean ) value );
		else if( type == JsonArray.class )
			destination.add( key, ( JsonArray ) value );
		else if( type == JsonObject.class )
			destination.add( key, ( JsonObject ) value );
	}
	

	public void processSummaryIndicator( String thisId ) throws ParseException, NoSuchMethodException, InstantiationException,
			InvocationTargetException, IllegalAccessException {
		synchronized( this.getClass() ) {
    		customerState.processedInd.add( thisId );
    		JsonParser parser = new JsonParser();
    
    		PExpressionEvaluator exp = Commons.expMap.get( thisId );
    		if( exp == null ) {
    			System.out.println( "expression is null" + thisId );
    			return;
    		}
    
    		String[ ] paramNames = exp.getParameterNames();
    		Class[ ] paramTypes = exp.getParameterTypes();
    		Object[ ] parameterValues = new Object[ paramNames.length ];
    		boolean expressionApplicable = true;
    		for( int j = 0; j < paramNames.length; j++ ) {
    			String thisParam = paramNames[ j ];
    			Object jValue = null;
    
    			if( customerState.tupleMap != null && customerState.tupleMap.has( thisParam ) )
    				jValue = new JsonPrimitive( customerState.tupleMap.get( thisParam ).getAsString() );
    			if( jValue == null ) {// it must be one of the indicator instances
    				JsonObject paramIndDef = Commons.allIndDef.get( thisParam );
    				if( paramIndDef == null && ! Commons.summaryIndDef.containsKey( thisParam ) ) {
    					if( Commons.allCIM.containsKey( thisParam ) )
    						jValue = null;
    					else if( thisParam.equals( "SIFTMATH" ) ) 
    						jValue = customerState.siftMath;
    					else { // this expression is not for this type of cdr
    						Commons.logger.info( "Verify the parameter: " + thisParam );
    						expressionApplicable = false;
    						break;
    					}
    				}else {
    					if( !thisId.equals( thisParam ) ) {
    						String frequency = paramIndDef.get( "frequency" ).getAsString();
    						String paramIndKey = generatekey( null, thisParam, frequency, customerState.thisRecordDate, thisTag );
    						// processIndicator( thisParam );
    						jValue = customerState.subscriberIndicators.get( paramIndKey );
    					}else {
    						// get from Panda cache
    						jValue = commonsState.summaryIndicatorsMap.get( thisParam + "-" + commonsState.nWorkerId );
    						if( jValue == null ) {
    							Object data = Commons.pandaCache.getClient().get( thisParam + "-" + commonsState.nWorkerId );
    							if( data == null )
    								jValue = null;
    							else
    								jValue = ( JsonObject ) parser.parse( data.toString() );
    						}
    					}
    				}
    			}
    			parameterValues[ j ] = ExpressionUtil.createObject( paramTypes[ j ], jValue );
    		}
    		if( !expressionApplicable ) {
    			return;
    		}
    		Object expressionValue = null;
    		try {
    			expressionValue = exp.evaluate( parameterValues );
    		}
    		catch( Exception ex ) {
				ex.printStackTrace();
				System.out.println( "Exception found in the expression: " + thisId + " : " + customerState.tupleMap.toString() );
				throw new InvocationTargetException( null, ex.getMessage() );
			}
    		/*
    		 * JsonObject thisIndDef = allIndDef.get( thisId ); String frequency =
    		 * thisIndDef.get( "frequency" ).getAsString(); String paramIndKey =
    		 * SiftMath.generatekey( null, thisId, frequency, SiftMath.callDate,
    		 * thisTag );
    		 */
    		commonsState.summaryIndicatorsMap.put( thisId + "-" + commonsState.nWorkerId, ( JsonObject ) expressionValue );
    		Commons.pandaCache.getClient().set( thisId + "-" + commonsState.nWorkerId, expressionValue.toString() );
    		// SiftMath.cache.set( thisId + "-" + nWorkerId, expressionValue );
    		// System.out.println(
    		// "------------------------------Sift Math Computed Indicator : " +
    		// expressionValue );
		}
	}

	private  String getSummaryIndicatorId( String id, String frequency ) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone( TimeZone.getTimeZone( "GMT" ) );

		long thisYear = cal.get( Calendar.YEAR ) - 2000;
		Long numParam;
		String temporalId = null;

		if( frequency.equalsIgnoreCase( "Daily" ) ) {
			numParam = new Long( cal.get( Calendar.DAY_OF_YEAR ) );
			temporalId = numParam.toString() + "D" + thisYear;
		}else if( frequency.equalsIgnoreCase( "Weekly" ) ) {
			numParam = new Long( cal.get( Calendar.WEEK_OF_YEAR ) );
			temporalId = numParam.toString() + "W" + thisYear;
		}else if( frequency.equalsIgnoreCase( "Monthly" ) ) {
			numParam = new Long( cal.get( Calendar.MONTH ) );
			temporalId = numParam.toString() + "M" + thisYear;
		}else if( frequency.equalsIgnoreCase( "Yearly" ) ) {
			numParam = thisYear;
			temporalId = thisYear + "Y";
		}else if( frequency.equalsIgnoreCase( "Hourly" ) ) {
			numParam = new Long( cal.get( Calendar.HOUR_OF_DAY ) );// get the
																   // hour from
																   // date
			temporalId = numParam + "H";
		}

		if( frequency.equals( "LifeTime" ) )
			id += "-L";
		else
			id += "-" + temporalId;
		return id;
	}

	public void processSummaryCounter( String id, int factor, String frequency ) {
		id = getSummaryIndicatorId( id, frequency );
		Commons.pandaCache.getClient().incr( id, factor, 1 );
	}

	public void processSummaryCounter( String id, int factor, int expiry, String frequency ) {
		id = getSummaryIndicatorId( id, frequency );
		Commons.pandaCache.getClient().incr( id, factor, 1, expiry );
	}

	public void deleteSummaryIndicator( String id, String frequnecy ) {
		id = getSummaryIndicatorId( id, frequnecy );
		Commons.pandaCache.getClient().delete( id );
	}

	public long getSummaryCounter( String id, String frequency ) {
		id = getSummaryIndicatorId( id, frequency );
		Object ind = Commons.pandaCache.getClient().get( id );
		if( ind == null )
			return 0;
		long result = Long.parseLong( ind.toString() );
		// System.out.println( "***********GetSummaryCoutner for id : " + id +
		// " = " + result );
		return result;
	}

	public String generatekey( String eventId, String key, String frequency, java.util.Date callDate, JsonObject aTag ) throws ParseException {
		Calendar cal = Calendar.getInstance();
		cal.setTime( callDate );

		if( frequency == null )
			frequency = "Yearly";
		String temporalId = null;
		long thisYear = cal.get( Calendar.YEAR ) - 2000;

		Long numParam;

		if( frequency.equals( "Daily" ) ) {
			numParam = new Long( cal.get( Calendar.DAY_OF_YEAR ) );
			temporalId = numParam.toString() + "D" + thisYear;
		}else if( frequency.equals( "Weekly" ) ) {
			numParam = new Long( cal.get( Calendar.WEEK_OF_YEAR ) );
			temporalId = numParam.toString() + "W" + thisYear;
		}else if( frequency.equals( "Monthly" ) ) {
			numParam = new Long( cal.get( Calendar.MONTH ) );
			temporalId = numParam.toString() + "M" + thisYear;
		}else if( frequency.equals( "Yearly" ) ) {
			numParam = thisYear;
			temporalId = thisYear + "Y";
		}else if( frequency.equals( "CustomTime" ) ) {
			if( aTag != null ) {
				numParam = aTag.get( "MONITORING_START_TIME" ).getAsLong();
				// long endDate = taggedInd.getEndDate();
				// int duration = taggedInd.getDuration();
				// temporalId = aTag.get( "PROGRAM_ID" ).getAsString() + "-" +
				// aTag.get( "OFFER_ID" ).getAsString() + "-" +
				// numParam.toString();
				temporalId = aTag.get( "FLOW_ID" ).getAsString();
			}else
				temporalId = eventId;
		}else if( frequency.equals( "TriggerTime" ) ) {
			temporalId = eventId;
		}else if( frequency.equals( "Hourly" ) ) {
			numParam = new Long( cal.get( Calendar.HOUR_OF_DAY ) );// get the
																   // hour from
																   // date
			temporalId = numParam + "H";
		}else if( frequency.equals( "LifeTimeHourly" ) ) {
			numParam = new Long( cal.get( Calendar.HOUR_OF_DAY ) );// get the
																   // hour from
																   // date
			temporalId = numParam + "H";
		} else if( frequency.equals( "Session" ) )
			temporalId = "S";

		if( frequency.equals( "LifeTime" ) )
			key += "-L";
		else
			key += "-" + temporalId;
		return key;
	}

	
	public double getFrequencyFromDateArray( JsonArray actualArray, int start, int duration ) {
		double count = 0;
		int end = duration - 1;
		if( ( start < 0 ) || ( end < 0 ) ) {
			return 0.0D;
		}
		long lStart = getCurrentTime() - start * 86400000L;
		// lStart -= lStart % 86400000L;
		long lEnd = ( lStart - ( lStart % 86400000L ) ) - end * 86400000L;

		for( int i = actualArray.size() - 1; i >= 0; i-- ) {
			if( actualArray.get( i ).getAsLong() > lStart )
				continue;

			if( actualArray.get( i ).getAsLong() < lEnd )
				return count;

			count++;
		}
		return count;
	}

	public List< Integer > getPackageFrequency( String indicator, String packageName, int startMonth, int numOfMonths ) {
		JsonElement ele = customerState.subscriberIndicators.get( indicator );
		List< Integer > frequency = new ArrayList< Integer >();
		if( ele == null || ele.isJsonNull() || ele.toString().length() <= 2 )
			return frequency;

		JsonObject indicatorObject = ele.getAsJsonObject();

		Date dt = null;
		SimpleDateFormat df = new SimpleDateFormat( "yyyyMM" );
		try {
			dt = df.parse( Integer.toString( startMonth ) );
		}catch( ParseException pe ) {
			pe.printStackTrace();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime( dt );
		String currMonthStr;
		int currMonthInt;
		String currMonth;

		for( int i = 0; i < numOfMonths; i++ ) {
			currMonthInt = cal.get( Calendar.MONTH );
			currMonthInt++; // As Calendar month index starts from 0.
			currMonthStr = ( currMonthInt < 10 ) ? "0" + currMonthInt : Integer.toString( currMonthInt );

			currMonth = Integer.toString( cal.get( Calendar.YEAR ) ) + currMonthStr;

			JsonElement monthEle = indicatorObject.get( currMonth );
			if( monthEle == null || monthEle.isJsonNull() ) {
				frequency.add( 0 );
				cal.add( Calendar.MONTH, -1 );
				continue;
			}

			JsonObject monthObject = monthEle.getAsJsonObject();
			JsonElement packageEle = monthObject.get( packageName );
			if( packageEle == null || packageEle.isJsonNull() ) {
				frequency.add( 0 );
				cal.add( Calendar.MONTH, -1 );
				continue;
			}

			frequency.add( packageEle.getAsInt() );

			cal.add( Calendar.MONTH, -1 );
		}

		return frequency;
	}

	public double getIDDSum( int index, int start, int duration ) {
		JsonElement ele = customerState.subscriberIndicators.get( "IDD_DAILY_USAGE_SERIES-L" );
		if( ele == null || ele.isJsonNull() || ele.toString().length() <= 2 )
			return -1;

		JsonObject iddObject = ele.getAsJsonObject();
		int end = duration - 1;
		if( ( start < 0 ) || ( end < 0 ) ) {
			return 0;
		}

		long lStart = getCurrentTime() - start * msecPerDay;
		lStart -= lStart % msecPerDay;
		long lEnd = lStart - end * msecPerDay;

		JsonElement keysEle = iddObject.get( "keys" );
		if( keysEle == null || keysEle.isJsonNull() )
			return 0;

		double count = 0;
		JsonArray keysArray = keysEle.getAsJsonArray();
		for( int i = keysArray.size() - 1; i >= 0; i-- ) {
			long currentKey = keysArray.get( i ).getAsLong();
			if( currentKey < lEnd )
				break;

			if( keysArray.get( i ).getAsLong() > lStart )
				continue;

			JsonElement dayEle = iddObject.get( Long.toString( currentKey ) );
			if( dayEle == null || dayEle.isJsonNull() )
				continue;

			Iterator< Entry< String, JsonElement > > iterator = dayEle.getAsJsonObject().entrySet().iterator();
			while( iterator.hasNext() ) {
				Entry< String, JsonElement > entry = iterator.next();
				JsonArray currentCountry = entry.getValue().getAsJsonArray();
				count += currentCountry.get( index ).getAsDouble();
			}
		}
		return count;
	}

	/**
	 * 
	 * @param daIDs
	 *            there can be multiple DA ids each will be separated by "|".
	 * @return sum of the balances, which are not expired.
	 */
	public List< Double > getDABalances( String daIDs ) {
		List< Double > sumList = new ArrayList< Double >();

		if( daIDs != null && daIDs.trim().length() > 0 ) {
			JsonElement jDAUsageSet = customerState.subscriberIndicators.get( "DA_USAGE_SERIES_LifeTime-L" );
			// TODO : why we are checking length
			if( jDAUsageSet == null || jDAUsageSet.isJsonNull() )
				return sumList;

			// Get all the ids
			String[ ] ids = daIDs.split( "\\|" );
			JsonObject packageObj = jDAUsageSet.getAsJsonObject();
			for( String eachID : ids ) {
				// loop through each id
				JsonObject idDetails = ( JsonObject ) packageObj.get( eachID );
				if( idDetails == null ) {
					sumList.add( 0.0 );
					continue;
				}

				JsonElement daRechargeExpiry = idDetails.get( "DARechargeExpiry" );
				if( daRechargeExpiry != null && !daRechargeExpiry.isJsonNull() ) {
					JsonArray expiryArray = daRechargeExpiry.getAsJsonArray();
					// Always have to check the last (latest) of this array.
					if( expiryArray != null && expiryArray.size() > 0 ) {
						long latestExpiry = expiryArray.get( expiryArray.size() - 1 ).getAsLong();

						if( latestExpiry != -99 && latestExpiry > getCurrentTime() ) {
							// Add DABalance
							JsonElement daBalance = idDetails.get( "DABalance" );
							if( daBalance != null && !daBalance.isJsonNull() ) {
								sumList.add( daBalance.getAsDouble() );
							}else
								sumList.add( 0.0 );
						}else
							sumList.add( 0.0 );
					}else
						sumList.add( 0.0 );
				}else
					sumList.add( 0.0 );
			}
		}
		return sumList;
	}

	public List< Double > getMonthlyDataPPUSum( int startMonth, int numOfMonths, String DAIds, String key ) {
		List< Double > monthlyDASum = new ArrayList< Double >( numOfMonths );
		JsonElement dataPPUMonthlySpendUsage = customerState.subscriberIndicators.get( "DATA_PPU_MONTHLY_SpendUsage_LifeTime-L" );

		int monthCount = 0;

		String processingMonth = String.valueOf( startMonth );
		// First four characters are year
		int year = Integer.parseInt( processingMonth.substring( 0, 4 ) );
		// Last two characters are month
		int month = Integer.parseInt( processingMonth.substring( 4 ) );

		if( dataPPUMonthlySpendUsage != null && !dataPPUMonthlySpendUsage.isJsonNull() ) {
			while( monthCount < numOfMonths ) {
				JsonObject monthlyUsage = dataPPUMonthlySpendUsage.getAsJsonObject();

				// get monthly data
				JsonObject eachMonthDetails = ( JsonObject ) monthlyUsage.get( processingMonth );

				double monthlySum = 0.00d;
				if( eachMonthDetails != null && !eachMonthDetails.isJsonNull() ) {
					if( DAIds.equals( "*" ) ) {
						// Have to consider all ID(s)

						Set< Entry< String, JsonElement >> monthlyEntrySet = eachMonthDetails.entrySet();
						Iterator< Entry< String, JsonElement >> monthlySetIterator = monthlyEntrySet.iterator();
						while( monthlySetIterator.hasNext() ) {
							Entry< String, JsonElement > eachEntry = monthlySetIterator.next();
							JsonObject jsonObjectValue = ( JsonObject ) eachEntry.getValue();
							JsonElement keyValue = jsonObjectValue.get( key );
							if( keyValue != null && !keyValue.isJsonNull() ) {
								monthlySum += keyValue.getAsDouble();
							}
						}

					}else {

						// Consider specific ID(s)
						String[ ] ids = DAIds.split( "\\|" );
						for( String eachId : ids ) {
							JsonElement monthlyDAIdElement = eachMonthDetails.get( eachId );
							if( monthlyDAIdElement != null && !monthlyDAIdElement.isJsonNull() ) {
								// Ensure DA id is existing
								JsonElement keyValue = ( ( JsonObject ) monthlyDAIdElement ).get( key );
								if( keyValue != null && !keyValue.isJsonNull() ) {
									monthlySum += keyValue.getAsDouble();
								}
							}
						}
					}
				}
				monthlyDASum.add( monthlySum );

				month--;
				if( month == 0 ) {
					month = 12;
					year--;
				}
				processingMonth = "" + year + ( month < 10 ? "0" + month : month );
				monthCount++;
			}

		}
		return monthlyDASum;
	}

	public String isLatestOfferControlled( String programId, String triggerId, String offerId ) {
		JsonElement eProgram = Commons.allProgramDef.get( programId );
		if( eProgram == null || eProgram.isJsonNull() )
			return "-1";

		JsonObject jProgram = eProgram.getAsJsonObject();
		JsonElement eProgramActions = jProgram.get( "programActions" );
		if( eProgramActions == null || eProgramActions.isJsonNull() )
			return "-1";

		JsonElement eTrigger = eProgramActions.getAsJsonObject().get( triggerId );
		if( eTrigger == null || eTrigger.isJsonNull() )
			return "-1";

		JsonArray aTrigger = eTrigger.getAsJsonArray();
		String fulfilmentTriggerId = "";
		for( int i = 0; i < aTrigger.size(); i++ ) {
			JsonElement eCurrentTriggerElement = aTrigger.get( i );
			if( eCurrentTriggerElement == null || eCurrentTriggerElement.isJsonNull() )
				continue;

			JsonObject jCurrentTriggerElement = eCurrentTriggerElement.getAsJsonObject();
			JsonElement eOfferId = jCurrentTriggerElement.get( "offerId" );
			if( eOfferId == null || eOfferId.isJsonNull() )
				continue;

			if( !offerId.equalsIgnoreCase( eOfferId.getAsString() ) )
				continue;

			fulfilmentTriggerId = jCurrentTriggerElement.get( "fulfillmentTrigger" ).getAsJsonObject().get( "triggerId" ).getAsString();
			break;
		}

		if( fulfilmentTriggerId.equals( "" ) )
			return "-1";

		JsonElement eEventTag = customerState.subscriberEventTags.get( fulfilmentTriggerId );
		if( eEventTag == null || eEventTag.isJsonNull() )
			return "false";

		return eEventTag.getAsJsonObject().get( "IS_CONTROL" ).getAsString();
	}

	public void deleteIndicator( String id ) {
		String[ ] indicators = id.trim().split( "," );

		for( int i = 0; i < indicators.length; i++ )
			Commons.pandaCache.getClient().delete( indicators[ i ] );
	}

	public boolean packageExclusionCheck( String excludePackages, int start, int duration ) {
		JsonElement ele = customerState.subscriberIndicators.get( "PACKAGE_SERIES_LifeTime-L" );
		if( ele == null || ele.isJsonNull() )
			return false;

		JsonObject packageSeriesObj = ele.getAsJsonObject();
		List< String > excludePackagesList = Arrays.asList( excludePackages.split( "\\|" ) );
		int end = duration - 1;
		if( ( start < 0 ) || ( end < 0 ) )
			return false;

		long lStart = getCurrentTime() - start * 86400000L;
		lStart -= lStart % 86400000L;
		long lEnd = lStart - end * 86400000L;

		JsonElement ekeysArr = packageSeriesObj.get( "keys" );
		if( ekeysArr == null || ekeysArr.isJsonNull() )
			return false;

		JsonArray keysArr = ekeysArr.getAsJsonArray();
		for( int i = keysArr.size() - 1; i > 0; i-- ) {
			long currentKey = keysArr.get( i ).getAsLong();
			if( currentKey < lEnd )
				return false;

			if( keysArr.get( i ).getAsLong() > lStart )
				continue;

			JsonElement eCurrentKeyDay = packageSeriesObj.get( Long.toString( currentKey ) );
			if( eCurrentKeyDay == null || eCurrentKeyDay.isJsonNull() )
				continue;

			JsonObject currentKeyDayObj = eCurrentKeyDay.getAsJsonObject();
			Iterator< Entry< String, JsonElement > > iterator = currentKeyDayObj.entrySet().iterator();
			while( iterator.hasNext() ) {
				Entry< String, JsonElement > entry = iterator.next();
				String key = entry.getKey().toString();
				if( excludePackagesList.contains( key ) )
					continue;

				return true;
			}
		}
		return false;
	}

	public List< Double > getMonthlyDASum( int startMonth, int numOfMonths, String DAIds, String key ) {
		List< Double > monthlyDASum = new ArrayList< Double >( numOfMonths );
		JsonElement dataPPUMonthlySpendUsage = customerState.subscriberIndicators.get( "DATA_PPU_MONTHLY_SpendUsage_LifeTime-L" );

		int monthCount = 0;

		String processingMonth = String.valueOf( startMonth );
		// First four characters are year
		int year = Integer.parseInt( processingMonth.substring( 0, 4 ) );
		// Last two characters are month
		int month = Integer.parseInt( processingMonth.substring( 4 ) );

		if( dataPPUMonthlySpendUsage != null && !dataPPUMonthlySpendUsage.isJsonNull() ) {
			while( monthCount < numOfMonths ) {
				JsonObject monthlyUsage = dataPPUMonthlySpendUsage.getAsJsonObject();

				// get monthly data
				JsonObject eachMonthDetails = ( JsonObject ) monthlyUsage.get( processingMonth );

				double monthlySum = 0.00d;
				if( eachMonthDetails != null && !eachMonthDetails.isJsonNull() ) {
					if( DAIds.equals( "*" ) ) {
						// Have to consider all ID(s)

						Set< Entry< String, JsonElement >> monthlyEntrySet = eachMonthDetails.entrySet();
						Iterator< Entry< String, JsonElement >> monthlySetIterator = monthlyEntrySet.iterator();
						while( monthlySetIterator.hasNext() ) {
							Entry< String, JsonElement > eachEntry = monthlySetIterator.next();
							JsonObject jsonObjectValue = ( JsonObject ) eachEntry.getValue();
							JsonElement keyValue = jsonObjectValue.get( key );
							if( keyValue != null && !keyValue.isJsonNull() ) {
								monthlySum += keyValue.getAsDouble();
							}
						}

					}else {

						// Consider specific ID(s)
						String[ ] ids = DAIds.split( "\\|" );
						for( String eachId : ids ) {
							JsonElement monthlyDAIdElement = eachMonthDetails.get( eachId );
							if( monthlyDAIdElement != null && !monthlyDAIdElement.isJsonNull() ) {
								// Ensure DA id is existing
								JsonElement keyValue = ( ( JsonObject ) monthlyDAIdElement ).get( key );
								if( keyValue != null && !keyValue.isJsonNull() ) {
									monthlySum += keyValue.getAsDouble();
								}
							}
						}
					}
				}
				monthlyDASum.add( monthlySum );

				month--;
				if( month == 0 ) {
					month = 12;
					year--;
				}
				processingMonth = "" + year + ( month < 10 ? "0" + month : month );
				monthCount++;
			}

		}

		return monthlyDASum;
	}

	public double getQuotaRemaining( String daID ) {
		JsonElement ele = customerState.subscriberIndicators.get( "QUOTA_DAILY_SERIES_LifeTime-L" );
		if( ele == null || ele.isJsonNull() || ele.toString().length() <= 2 )
			return -1;

		JsonObject quotaObject = ele.getAsJsonObject();
		JsonElement packageEle = quotaObject.get( daID );
		if( packageEle == null || packageEle.isJsonNull() )
			return -1;

		JsonObject packageObj = packageEle.getAsJsonObject();
		JsonElement quotaLimitEle = packageObj.get( "QuotaLimit" );
		JsonElement quotaConsumptionEle = packageObj.get( "QuotaConsumption" );
		if( quotaLimitEle == null || quotaLimitEle.isJsonNull() || quotaConsumptionEle == null || quotaConsumptionEle.isJsonNull() )
			return -1;

		double quotaLimit = quotaLimitEle.getAsDouble();
		JsonArray quotaConsumptionArray = quotaConsumptionEle.getAsJsonArray();
		double quotaConsumption = quotaConsumptionArray.get( quotaConsumptionArray.size() - 1 ).getAsDouble();

		return ( quotaLimit - quotaConsumption );
	}

	public String getIDDBucketPeakDetails( JsonObject indicator, String countries, int index, int startDay, int duration, double percentage ) {
		String[ ] destinationCountries = countries.split( "\\|" );
		double[ ] destinationDuration = new double[ destinationCountries.length ];
		List< String > destinationCountriesList = Arrays.asList( destinationCountries );

		Calendar weekDayDecider = Calendar.getInstance();
		long currentTime = getCurrentTime();

		long currentDay = currentTime - ( currentTime % msecPerDay );
		long startingRange = currentDay - ( startDay * msecPerDay );
		long endRange = startingRange - ( duration * msecPerDay );

		// System.out.println("Start range: " + startingRange + "\nEndRange: " +
		// endRange);

		double[ ][ ] dayWiseSum = new double[ destinationCountries.length ][ 7 ];
		double[ ] countryWiseLongestDuration = new double[ destinationCountries.length ];

		// Which country has maximum call value ?
		int maxCountryIndex = 0;
		// Which day has maximum call value ? (For the max country only)
		int maxDayIndex = 0;

		double totalMax = 0.00;

		double totalCallDuration = 0.00;

		JsonElement keyElement = indicator.get( "keys" );
		if( keyElement != null && !keyElement.isJsonNull() ) {
			// All the keys, each key represent a day;
			// They day when subscriber made a IDD call.
			JsonArray callingdayArray = keyElement.getAsJsonArray();

			for( int i = callingdayArray.size() - 1; i > -1; i-- ) {
				JsonElement callingDay = callingdayArray.get( i );

				if( callingDay != null && !callingDay.isJsonNull() ) {
					long callingDayValue = callingDay.getAsLong();

					if( callingDayValue > startingRange )
						continue;

					if( callingDayValue >= endRange ) {
						// System.out.println("Evaluating date: " +
						// callingDayValue);
						// Within the period.
						JsonElement callDetailsElement = indicator.get( String.valueOf( callingDayValue ) );

						if( callDetailsElement != null && !callDetailsElement.isJsonNull() ) {
							Set< Entry< String, JsonElement >> callDetailsEntrySet = callDetailsElement.getAsJsonObject().entrySet();
							if( callDetailsEntrySet != null && !callDetailsEntrySet.isEmpty() ) {
								Iterator< Entry< String, JsonElement >> callDetailsEntryIterator = callDetailsEntrySet.iterator();

								// loop through all the destinations, keep on
								// adding the duration.
								while( callDetailsEntryIterator.hasNext() ) {
									Entry< String, JsonElement > nextEntry = callDetailsEntryIterator.next();
									String destination = nextEntry.getKey();

									// This is IDD call details.
									JsonElement iddDestinationCallDetails = nextEntry.getValue();
									if( iddDestinationCallDetails != null && !iddDestinationCallDetails.isJsonNull() ) {
										JsonArray callDetailsArray = iddDestinationCallDetails.getAsJsonArray();

										if( iddDestinationCallDetails.isJsonArray() ) {
											int destinationIndex = destinationCountriesList.indexOf( destination );

											// index of call duration field.
											if( index < callDetailsArray.size() ) {

												double callDuration = callDetailsArray.get( index ).getAsDouble();
												totalCallDuration += callDuration;

												if( destinationIndex > -1 ) {
													// Destination is from the
													// passed
													// country list.
													// Call day of the week
													// determination
													weekDayDecider.setTimeInMillis( callingDayValue );
													int weekDay = weekDayDecider.get( Calendar.DAY_OF_WEEK );

													dayWiseSum[ destinationIndex ][ weekDay - 1 ] += callDuration;

													destinationDuration[ destinationIndex ] += callDuration;
													if( countryWiseLongestDuration[ destinationIndex ] < callDuration ) {
														countryWiseLongestDuration[ destinationIndex ] = callDuration;
													}

													if( destinationDuration[ destinationIndex ] > totalMax ) {
														totalMax = destinationDuration[ destinationIndex ];
														maxCountryIndex = destinationIndex;
													}

												}

											}

										}
									}
								}

							}
						}
					}else {
						break;
					}
				}
			}
		}

		double maxDayAmount = 0.00d;
		maxDayIndex = 0;
		for( int dayIndex = 0; dayIndex < 7; dayIndex++ ) {
			if( dayWiseSum[ maxCountryIndex ][ dayIndex ] > maxDayAmount ) {
				maxDayAmount = dayWiseSum[ maxCountryIndex ][ dayIndex ];
				maxDayIndex = dayIndex;
			}
		}

		if( totalCallDuration > 0.00d ) {
			double currentMaxPerc = totalMax * 100 / totalCallDuration;
			if( currentMaxPerc >= percentage ) {
				StringBuilder finalResult = new StringBuilder( destinationCountries[ maxCountryIndex ] );
				finalResult.append( "|" );
				finalResult.append( totalMax );
				finalResult.append( "|" );
				finalResult.append( countryWiseLongestDuration[ maxCountryIndex ] );
				finalResult.append( "|" );
				finalResult.append( maxDayIndex );
				return finalResult.toString();
			}else {
				return "-99";
			}
		}else {
			return "-99";
		}
	}

	public int getOfferCountLegacy( String programId, String offerId ) {
		int count = 0;
		JsonElement programHistoryEle = customerState.subscriberIndicators.get( "PROGRAMS_LifeTime-L" );
		if( programHistoryEle == null || programHistoryEle.isJsonNull() || programHistoryEle.toString().trim().length() <= 2 )
			return count;

		JsonElement thisProgramEle = programHistoryEle.getAsJsonObject().get( programId );
		if( thisProgramEle == null || thisProgramEle.isJsonNull() )
			return count;

		JsonArray thisProgramHistory = thisProgramEle.getAsJsonArray();
		JsonObject latestProgramInstance = thisProgramHistory.get( thisProgramHistory.size() - 1 ).getAsJsonObject();
		JsonElement jele = latestProgramInstance.get( offerId );
		if( jele == null )
			return count;
		return jele.getAsJsonArray().size();
	}

	public String getIDDLastCountryCalled( String indicatorName, String groupId, int start, int duration ) {
		String lastCalledCountryname = "-99";
		if( ! ( groupId.equalsIgnoreCase( "group1" ) || groupId.equalsIgnoreCase( "group2" ) ) ) {
			return lastCalledCountryname;
		}
		JsonElement ele = customerState.subscriberIndicators.get( indicatorName );
		if( ele == null || ele.isJsonNull() )
			return lastCalledCountryname;
		JsonObject jo = ele.getAsJsonObject();
		JsonElement ekeysArray = jo.get( "keys" );
		if( ekeysArray == null )
			return lastCalledCountryname;

		JsonArray jsonArr = ekeysArray.getAsJsonArray();
		long locStartDate = getCurrentTime();

		locStartDate = locStartDate - ( locStartDate % 86400000l );
		locStartDate = locStartDate - start * 86400000l;
		long locEndDate = locStartDate - duration * 86400000l;

		for( int i = jsonArr.size() - 1; i >= 0; i-- ) {
			long objTimeStamp = jsonArr.get( i ).getAsLong();
			long objday = objTimeStamp - ( objTimeStamp % 86400000l );
			String objTimeStampString = jsonArr.get( i ).getAsString();
			if( objday > locStartDate ) {
				continue;
			}else if( objday < locEndDate ) {
				break;
			}else {
				JsonArray jsonArr1 = jo.getAsJsonArray( objTimeStampString );

				if( groupId.equalsIgnoreCase( "group1" ) ) {
					if( jsonArr1.get( 1 ).getAsString().equals( "-99" ) || jsonArr1.get( 1 ).isJsonNull() || jsonArr1.get( 1 ).toString().equals( "" ) ) {
						continue;
					}else {
						lastCalledCountryname = jsonArr1.get( 1 ).getAsString();
						break;
					}
				}else if( groupId.equalsIgnoreCase( "group2" ) ) {

					if( jsonArr1.get( 2 ).getAsString().equals( "-99" ) || jsonArr1.get( 2 ).isJsonNull() || jsonArr1.get( 2 ).toString().equals( "" ) ) {
						continue;
					}else {
						lastCalledCountryname = jsonArr1.get( 2 ).getAsString();
						break;
					}
				}
			}
		}
		return lastCalledCountryname;
	}

	public boolean hasActivePackage( String packageCategory, String packageFrequency, boolean isSinglePackage, int start, int duration ) {
		JsonElement ele = customerState.subscriberIndicators.get( "PACKAGE_TIMESERIES_LifeTime-L" );
		if( ele == null || ele.isJsonNull() )
			return false;

		int end = duration - 1;
		if( ( start < 0 ) || ( end < 0 ) )
			return false;

		long lStart = getCurrentTime() - start * msecPerDay;
		lStart -= lStart % msecPerDay;
		long lEnd = lStart - end * msecPerDay;
		lStart = lStart + msecPerDay - 1000;

		JsonObject packageObj = ele.getAsJsonObject();
		JsonElement keysElement = packageObj.get( "keys" );
		if( keysElement == null || keysElement.isJsonNull() )
			return false;
		JsonArray keysArr = keysElement.getAsJsonArray();

		for( int i = keysArr.size() - 1; i >= 0; i-- ) {
			long currentKey = keysArr.get( i ).getAsLong();

			if( keysArr.get( i ).getAsLong() > lStart )
				continue;

			JsonElement dayEle = packageObj.get( Long.toString( currentKey ) );
			if( dayEle == null || dayEle.isJsonNull() )
				continue;

			JsonObject dayObj = dayEle.getAsJsonObject();
			Iterator< Entry< String, JsonElement > > iter = dayObj.entrySet().iterator();
			while( iter.hasNext() ) {
				Entry< String, JsonElement > entry = iter.next();
				JsonArray pkgArr = entry.getValue().getAsJsonArray();

				if( packageFrequency.equalsIgnoreCase( "ALL" ) || packageFrequency.equalsIgnoreCase( pkgArr.get( 4 ).getAsString() ) ) {
					long currentExpiryDate = pkgArr.get( 0 ).getAsLong();
					if( currentExpiryDate >= lStart ) {
						if( !isSinglePackage && pkgArr.get( 1 ).getAsString().contains( packageCategory ) )
							return true;
						else if( isSinglePackage && pkgArr.get( 1 ).getAsString().equals( packageCategory ) )
							return true;
					}
				}
			}
		}
		return false;
	}

	public String getNthDeviceType( int index, JsonObject indicatorName ) {

		if( index < 0 ) {
			return "-99";
		}
		JsonElement keyAsElement = indicatorName.get( "keys" );
		if( keyAsElement != null ) {
			JsonArray keyArray = keyAsElement.getAsJsonArray();
			if( keyArray.size() - 1 < index ) {
				return "-99";
			}
			JsonElement timeStampAsElement = keyArray.get( ( keyArray.size() - 1 ) - index );
			if( timeStampAsElement != null ) {
				JsonElement timeStampObject = indicatorName.get( timeStampAsElement.getAsString() );
				if( timeStampObject != null ) {
					JsonArray timeStamArray = timeStampObject.getAsJsonArray();
					JsonElement deviceType = timeStamArray.get( 1 );
					if( deviceType != null ) {
						return deviceType.getAsString();
					}
				}
			}
		}
		return "-99";
	}

	public int getDeviceChangeCount( String deviceType, int start, int duration, JsonObject indicatorName ) {

		if( start < 0 || duration < 0 ) {
			return 0;
		}
		duration = duration - 1;
		int count = 0;
		long locStartDateTime = getCurrentTime();
		long tommorrowDateTime = locStartDateTime + 86400000l;
		long currentDayEndTime = tommorrowDateTime - ( tommorrowDateTime % 86400000l );
		long DurStartDateTime = currentDayEndTime - start * 86400000l;
		long DurEndDateTime = DurStartDateTime - duration * 86400000l;
		JsonElement keyAsElement = indicatorName.get( "keys" );
		if( keyAsElement != null && keyAsElement.getAsJsonArray().size() > 0 ) {
			JsonArray keyArray = keyAsElement.getAsJsonArray();
			for( int i = keyArray.size() - 1; i >= 0; i-- ) {
				if( keyArray.get( i ).getAsLong() >= DurStartDateTime ) {
					continue;
				}else if( keyArray.get( i ).getAsLong() < DurEndDateTime ) {
					break;
				}else {
					if( deviceType != null ) {
						if( "*".equals( deviceType ) ) {
							count++;
						}else {
							JsonElement timeStampElement = indicatorName.get( keyArray.get( i ).getAsString() );
							String genearationTypeString = "";
							if( timeStampElement != null ) {
								JsonArray timeStampArray = timeStampElement.getAsJsonArray();
								if( timeStampArray.size() == 2 ) {
									JsonElement genearationType = timeStampArray.get( 1 );
									if( genearationType != null ) {
										genearationTypeString = genearationType.getAsString();
									}
								}
							}
							if( "2G".equalsIgnoreCase( deviceType ) && "2G".equalsIgnoreCase( genearationTypeString ) ) {
								count++;
							}else if( "3G".equalsIgnoreCase( deviceType ) && "3G".equalsIgnoreCase( genearationTypeString ) ) {
								count++;
							}else if( "4G".equalsIgnoreCase( deviceType ) && "4G".equalsIgnoreCase( genearationTypeString ) ) {
								count++;
							}
						}
					}
				}
			}
		}
		return count;
	}

	public double getAppUsedDayCount( JsonObject indicatorName, String apps, int start, int duration ) {

		if( start < 0 || duration < 0 ) {
			return 0;
		}
		double count = 0.0;
		duration = duration - 1;
		String[ ] allApps = apps.split( "," );
		long startDate = getCurrentTime();
		startDate = startDate - ( startDate % 86400000l );
		startDate = startDate - start * 86400000l;
		long endDate = startDate - duration * 86400000l;
		JsonElement keyAsElement = indicatorName.get( "keys" );
		if( keyAsElement != null && keyAsElement.getAsJsonArray().size() > 0 ) {
			JsonArray keyArray = keyAsElement.getAsJsonArray();
			for( int i = keyArray.size() - 1; i >= 0; i-- ) {
				if( keyArray.get( i ).getAsLong() > startDate ) {
					continue;
				}else if( keyArray.get( i ).getAsLong() < endDate ) {
					break;
				}else {
					String thisDay = keyArray.get( i ).getAsString();
					JsonObject thisDayObj = indicatorName.get( thisDay ).getAsJsonObject();
					for( int j = 0; j < allApps.length; j++ ) {
						if( thisDayObj.has( allApps[ j ] ) ) {
							count++;
							break;
						}
					}
				}
			}
		}

		return count;
	}

	public double getAreaTypeUsagePercent( JsonObject indicatorName, String ratTypes, int start, int duration ) {

		if( start < 0 || duration < 0 ) {
			return 0.0;
		}
		duration = duration - 1;

		String[ ] ratTypeArr = ratTypes.split( "," );
		double ratTypeVal = 0.0, total = 0.0;

		long startDate = getCurrentTime();
		startDate = startDate - ( startDate % 86400000l );
		startDate = startDate - start * 86400000l;
		long endDate = startDate - duration * 86400000l;
		JsonElement keyAsElement = indicatorName.get( "keys" );
		if( keyAsElement != null && keyAsElement.getAsJsonArray().size() > 0 ) {
			JsonArray keyArray = keyAsElement.getAsJsonArray();
			for( int i = keyArray.size() - 1; i >= 0; i-- ) {
				if( keyArray.get( i ).getAsLong() > startDate ) {
					continue;
				}else if( keyArray.get( i ).getAsLong() < endDate ) {
					break;
				}else {
					String thisDay = keyArray.get( i ).getAsString();
					JsonObject thisDayObj = indicatorName.get( thisDay ).getAsJsonObject();
					for( int j = 0; j < ratTypeArr.length; j++ ) {
						if( thisDayObj.has( ratTypeArr[ j ] ) ) {
							ratTypeVal += thisDayObj.get( ratTypeArr[ j ] ).getAsDouble();
						}
					}

					if( thisDayObj.has( "4G" ) )
						total += thisDayObj.get( "4G" ).getAsDouble();
					if( thisDayObj.has( "NON4G" ) )
						total += thisDayObj.get( "NON4G" ).getAsDouble();
				}
			}
		}
		// System.out.println("a:" + ratTypeVal + "b:" + total);
		if( ratTypeVal == 0.0 && total == 0.0 )
			return 0.0;
		else
			return Math.round( ( ( ratTypeVal * 100.0 ) / total ) * 100.0 ) / 100.0;
	}

	public String getNonTakenOffers( String programId ) {
		if( customerState.subscriberEventTags == null )
			return "";

		JsonElement jProgramSet = customerState.subscriberIndicators.get( "PROGRAMS_LifeTime-L" );
		if( jProgramSet == null )
			return "";
		JsonObject jProgramHistory = jProgramSet.getAsJsonObject();
		String retVal = null;
		Iterator< Entry< String, JsonElement >> itr = customerState.subscriberEventTags.entrySet().iterator();
		while( itr.hasNext() ) {
			Entry< String, JsonElement > entry = itr.next();
			JsonObject aTag = ( JsonObject ) entry.getValue();
			if( !aTag.has( "PROGRAM_ID" ) )
				continue;
			String thisTagProgramId = aTag.get( "PROGRAM_ID" ).getAsString();
			if( !programId.equals( "*" ) && !programId.equals( thisTagProgramId ) )
				continue;
			String offerId = aTag.get( "OFFER_ID" ).getAsString();
			// check of the monitoring has ended yesterday
			if( !aTag.has( "MONITORING_END_TIME" ) )
				continue;
			long monEnd = aTag.get( "MONITORING_END_TIME" ).getAsLong();
			long yday = getCurrentDay() - SiftMath.msecPerDay;
			long monEndDay = monEnd - ( monEnd % SiftMath.msecPerDay );
			/*
			 * Calendar ydayCal = Calendar.getInstance();
			 * ydayCal.setTimeInMillis( yday ); Calendar monEndCal =
			 * Calendar.getInstance(); monEndCal.setTimeInMillis( monEnd );
			 * System.out.println("going to match day"); if( ydayCal.get(
			 * Calendar.DAY_OF_MONTH ) != monEndCal.get( Calendar.DAY_OF_MONTH )
			 * ) continue;
			 */
			if( yday != monEndDay )
				continue;
			String offerType = getSiftwareAttribute( "Offer", offerId, "offerType" ).getAsString();
			if( offerType.equals( "Conditional" ) && !isFulfillmentTriggered( thisTagProgramId, offerId ) ) {
				if( retVal == null )
					retVal = thisTagProgramId + "~" + offerId;
				else if( !retVal.contains( thisTagProgramId + "~" + offerId ) )
					retVal = retVal + "," + thisTagProgramId + "~" + offerId;
			}
		}
		return retVal;
	}

	public int getNumberOfContactsInCalenderMonth( String programId, String action, boolean isOnlySuccessRequired, boolean isOnlyTargetRequied ) {
		int count = 0;

		JsonElement progLifeTime = customerState.subscriberIndicators.get( "PROGRAMS_LifeTime-L" );
		if( progLifeTime == null || progLifeTime.toString().length() <= 2 )
			return count;

		JsonObject jProgLifeTime = progLifeTime.getAsJsonObject();
		JsonElement thisProg = jProgLifeTime.get( programId );

		if( thisProg == null || thisProg.toString().length() <= 2 )
			return count;

		String actionToCheck = "notificationAction";
		if( action.equalsIgnoreCase( "monitor" ) )
			actionToCheck = "monitoringAction";
		else if( action.equalsIgnoreCase( "fulfil" ) )
			actionToCheck = "fulfillmentAction";

		Calendar cal = Calendar.getInstance();
		int noOfDaysLeftInThisMonth = cal.get( Calendar.DAY_OF_MONTH );
		long startDate = getCurrentDay() + 86399999L;
		long endDate = getCurrentDay() - ( ( noOfDaysLeftInThisMonth - 1 ) * 86400000L );

		JsonArray jThisProgArr = thisProg.getAsJsonArray();

		JsonObject jThisProgObj = jThisProgArr.get( 0 ).getAsJsonObject();

		Iterator< Entry< String, JsonElement >> itr = jThisProgObj.entrySet().iterator();

		while( itr.hasNext() ) {
			String key = itr.next().getKey();
			JsonArray thisOfferArr = jThisProgObj.get( key ).getAsJsonArray();
			for( int i = 0; i < thisOfferArr.size(); i++ ) {
				JsonObject thisContact = thisOfferArr.get( i ).getAsJsonObject();
					if( thisContact.has( actionToCheck ) ) {

						long contactTime = thisContact.get( actionToCheck ).getAsLong();
						if( contactTime >= endDate && contactTime <= startDate ) {
							if( isOnlyTargetRequied ) {
								if( !thisContact.has( "isControl" ) )
									count++;
							}else
								count++;
						}
					}


			}
		}
		return count;
	}

	/**
	 * Return number of days since last subscription. Range starts from the
	 * number Of days to traverse.
	 * 
	 * @param packageId
	 *            separated by "|".
	 * @param packageCategory
	 *            separated by "|".
	 * @param numberOfDaysToTraverse
	 * @return days since package was purchased. -1 if there is no package
	 *         purchase within numberOfDaysToTraverse.
	 */
	public int getDaysSinceSubscription( String packageId, String packageCategory, int numberOfDaysToTraverse ) {
		String[ ] allPackages = packageId.split( "\\|" );
		String[ ] allCategories = packageCategory.split( "\\|" );

		if( allPackages.length != allCategories.length ) {
			throw new IllegalArgumentException( "Package id and package category should have same number of elements." );
		}

		JsonElement ele = customerState.subscriberIndicators.get( "PACKAGE_TIMESERIES_LifeTime-L" );
		if( ele != null && !ele.isJsonNull() ) {

			// numberOfDaysToTraverse to number of mills seconds.
			long lEndMiliSeconds = numberOfDaysToTraverse * msecPerDay;
			long lEnd = getCurrentTime() - lEndMiliSeconds;
			lEnd -= lEnd % msecPerDay;

			JsonObject packageObj = ele.getAsJsonObject();
			JsonElement keysElement = packageObj.get( "keys" );

			if( keysElement != null && !keysElement.isJsonNull() ) {

				JsonArray keysArr = keysElement.getAsJsonArray();

				for( int keyIndex = keysArr.size() - 1; keyIndex >= 0; keyIndex-- ) {
					long currentKey = keysArr.get( keyIndex ).getAsLong();
					if( currentKey < lEnd ) {
						break;
					}else {
						// Any keys which is greater than equal to this value
						// will be
						// considered.
						// Within numberOfDaysToTraverse range
						JsonElement element = packageObj.get( Long.toString( currentKey ) );
						if( element != null && !element.isJsonNull() && element.isJsonObject() ) {
							JsonObject packageDetails = element.getAsJsonObject();

							//
							// if( packageDetails != null &&
							// !packageDetails.isJsonNull() ) {
							for( int packageIndex = 0; packageIndex < allPackages.length; packageIndex++ ) {
								// Check package
								if( "All".equalsIgnoreCase( allPackages[ packageIndex ] ) ) {
									String packageName = allCategories[ packageIndex ];

									// For entries like packageId All and
									// package category like data or voice.
									if( !"All".equalsIgnoreCase( packageName ) ) {
										Set< Entry< String, JsonElement >> packageEntrySet = packageDetails.getAsJsonObject().entrySet();
										Iterator< Entry< String, JsonElement >> packageEntrySetIterator = packageEntrySet.iterator();
										while( packageEntrySetIterator.hasNext() ) {
											Entry< String, JsonElement > nextEntry = packageEntrySetIterator.next();
											JsonElement values = nextEntry.getValue();
											if( values != null && !values.isJsonNull() && values.isJsonArray() ) {
												JsonArray allValues = values.getAsJsonArray();
												if( allValues != null && !allValues.isJsonNull() ) {
													if( packageName.equalsIgnoreCase( allValues.get( 1 ).getAsString() ) ) {
														return ( numberOfDaysToTraverse - ( int ) ( ( currentKey - lEnd ) / msecPerDay ) );
													}
												}
											}
										}
									}else {
										// All, All
										return ( numberOfDaysToTraverse - ( int ) ( ( currentKey - lEnd ) / msecPerDay ) );
									}

								}else {
									JsonElement insidePackage = packageDetails.get( allPackages[ packageIndex ] );

									if( insidePackage != null && !insidePackage.isJsonNull() && insidePackage.isJsonArray() ) {
										JsonArray packageContent = insidePackage.getAsJsonArray();
										if( packageContent != null && !packageContent.isJsonNull() ) {
											JsonElement categoryElement = packageContent.get( 1 );
											if( categoryElement != null && !categoryElement.isJsonNull() ) {
												// Check Category
												if( categoryElement.getAsString().equalsIgnoreCase( allCategories[ packageIndex ] ) ) {
													return ( numberOfDaysToTraverse - ( int ) ( ( currentKey - lEnd ) / msecPerDay ) );
												}
											}
										}
									}
								}
							}
							// }
						}
					}
				}

			}

		}
		return -1;
	}

	
	public double getDaysToPackageExpiryExtended( String packageCategory, String packagefrequency, String packageDescription) {
		JsonElement ele = customerState.subscriberIndicators.get( "PACKAGE_TIMESERIES_LifeTime-L" );
		if( ele == null || ele.isJsonNull() || ele.toString().length() <= 2 )
			return -1;

		String[] pkgDescArray = packageDescription.split( "\\|", -1 );
		JsonObject packageObj = ele.getAsJsonObject();
		String currentPkg = "current" + packageCategory + "Package";
		JsonArray currentPackage = null;
		JsonArray currentComboPackage = null;
		long currentPackageExpiryDate = 0l;
		long currentComboPackageExpiryDate = 0l;
		long finalExpDate = 0l;
		long currentTime = SiftMath.getCurrentTime();
		long currentDay = currentTime - ( currentTime % msecPerDay );

			JsonElement currentPkgEle = packageObj.get( currentPkg );
			JsonElement currentComboPkgEle = packageObj.get( "currentComboPackage" );

			if( ( currentPkgEle == null || !currentPkgEle.isJsonArray() ) && ( currentComboPkgEle == null || !currentComboPkgEle.isJsonArray() ) )
				return -1;
			boolean checkKeyArray = false;
			if( ( currentPkgEle != null && currentPkgEle.isJsonArray() ) ) {
				currentPackage = currentPkgEle.getAsJsonArray();
				if( currentPackage.size() == 10 
						&& currentPackage.get( 4 ).getAsString().toUpperCase().equals( packagefrequency.toUpperCase() )) {
					boolean descFlag = false;
					for( int j = 0;j< pkgDescArray.length;j++ ){
						if( ( !currentPackage.get( 6 ).getAsString().toUpperCase().contains("EXTRA")) 
								&& currentPackage.get( 6 ).getAsString().toUpperCase().contains( pkgDescArray[j].toUpperCase()) ){
							descFlag = true;
							break;
						}
					}
					if( descFlag  ){
					JsonElement eExpDate = currentPackage.get( 0 );
					if( eExpDate != null )
						currentPackageExpiryDate = eExpDate.getAsLong();
					}else
						checkKeyArray = true;
				}else
					checkKeyArray = true;
				
				if(checkKeyArray){
					JsonElement eKeyArr = packageObj.get( "keys" );
					JsonArray aKeyArr = new JsonArray();
					if( eKeyArr != null && eKeyArr.isJsonArray() )
						aKeyArr = eKeyArr.getAsJsonArray();
					if(aKeyArr.size() !=0 ){
						for( int i = aKeyArr.size()-1;i >=0;i-- ){
							if( ! packageObj.has( aKeyArr.get( i ).getAsString() ) )
								continue;
							JsonObject thisPkgEntry = packageObj.get(aKeyArr.get( i ).getAsString() ).getAsJsonObject();
							Iterator< Entry< String, JsonElement >> itr = thisPkgEntry.entrySet().iterator();
							while( itr.hasNext() ) {
								Entry< String, JsonElement > entry = itr.next();
								JsonArray aPkgEntry = entry.getValue().getAsJsonArray();
								if( aPkgEntry.size() == 8  
										&& aPkgEntry.get(1).getAsString().toUpperCase().equals( packageCategory.toUpperCase() )
										&& aPkgEntry.get(4).getAsString().toUpperCase().equals( packagefrequency.toUpperCase())) {
									
										boolean descFlag = false;
										for( int j = 0;j< pkgDescArray.length;j++ ){
											if( ( !aPkgEntry.get( 6 ).getAsString().toUpperCase().contains("EXTRA")) 
											&& aPkgEntry.get( 6 ).getAsString().toUpperCase().contains( pkgDescArray[j].toUpperCase()) ){
												descFlag = true;
												break;
											}
										}
										if( descFlag  ){
											JsonElement eExpDate = aPkgEntry.get( 0 );
											if( eExpDate != null )
												currentPackageExpiryDate = eExpDate.getAsLong();
												break;
										}
								}
							
							}
							
						
						}
					}
				}
			}
			if( currentComboPkgEle != null && currentComboPkgEle.isJsonArray() ) {
				currentComboPackage = currentComboPkgEle.getAsJsonArray();
				if( currentComboPackage.size() == 10 && currentComboPackage.get( 1 ).getAsString().contains( packageCategory )
						&& currentComboPackage.get( 4 ).getAsString().toUpperCase().equals( packagefrequency.toUpperCase() ) ) {
					
					boolean descFlag = false;
					for( int j = 0;j< pkgDescArray.length;j++ ){
						if(  currentComboPackage.get( 6 ).getAsString().toUpperCase().contains( pkgDescArray[j].toUpperCase()) ){
							descFlag = true;
							break;
						}
					}
					if( descFlag  ){
						JsonElement eComboExpDate = currentComboPackage.get( 0 );
						if( eComboExpDate != null )
							currentComboPackageExpiryDate = eComboExpDate.getAsLong();
							
					}
					
				}
			}

			if( ( currentPackageExpiryDate == 0l && currentComboPackageExpiryDate == 0l ) )
				return -1;

			if( currentPackageExpiryDate >= currentComboPackageExpiryDate )
				finalExpDate = currentPackageExpiryDate;
			else
				finalExpDate = currentComboPackageExpiryDate;

			if( finalExpDate < currentDay )
				return -1;
			else {
				double numOfDaysToExpire = ( finalExpDate - currentDay ) / msecPerDay;
				return numOfDaysToExpire;
			}
			
	}
	
	public double getUtilizationPercentage( String utilizationType, int start, int duration ) {
		JsonElement ele = customerState.subscriberIndicators.get( "NETWORK_AREA_UTILIZATION_SERIES_LifeTime-L" );
		if( ele == null || ele.isJsonNull() ) 
			return 0;

		int end = duration - 1;
		if ((start < 0) || (end < 0))
			return 0;

		long lStart = getCurrentDay();
		lStart = lStart - start * msecPerDay;
		long lEnd = lStart - end * msecPerDay;

		JsonObject networkObj = ele.getAsJsonObject();
		JsonElement eKeysArr = networkObj.get( "keys" );
		if( eKeysArr == null )
			return 0;
		JsonArray keysArr = eKeysArr.getAsJsonArray();
		
		double lowUtilCount = 0;
		double nonLowUtilCount = 0;
		
		for ( int i = keysArr.size() - 1; i >= 0; i -- ){
			long currentKey = keysArr.get( i ).getAsLong();
			
			if( keysArr.get( i ).getAsLong() > lStart )
				continue;
			
			if( keysArr.get( i ).getAsLong() < lEnd )
				break;

			JsonElement dayEle = networkObj.get( Long.toString( currentKey ) );
			if( dayEle == null || dayEle.isJsonNull() )
				continue;

			JsonObject dayObj = dayEle.getAsJsonObject();
			if( dayObj.has( "LOW" ) )
				lowUtilCount += dayObj.get( "LOW" ).getAsInt();
			if( dayObj.has( "NON_LOW" ) )
				nonLowUtilCount += dayObj.get( "NON_LOW" ).getAsInt();
		}
		double totalUtilCount = lowUtilCount + nonLowUtilCount;
		if( totalUtilCount == 0 )
			return 0;
		
		if( utilizationType.equalsIgnoreCase( "low" ) )
			return ( lowUtilCount / totalUtilCount ) * 100;
		else
			return ( nonLowUtilCount / totalUtilCount ) * 100;
	}
	
	public double maxDailyARPU( JsonObject DAILY_USAGE_SERIES_LifeTime, JsonObject PACKAGE_TIMESERIES_LifeTime, 
																String packageIds,int colunmIndx, int start, int duration) {
		int end = duration - 1;
		if( start < 0 || end < 0 )
			return 0;

		if( DAILY_USAGE_SERIES_LifeTime == null && PACKAGE_TIMESERIES_LifeTime == null )
			return 0;

		long lStart = getCurrentTime() - ( start * msecPerDay );
		lStart = lStart - ( lStart % msecPerDay );
		long lEnd = lStart - ( end * msecPerDay );
		packageIds = "," + packageIds + ",";
		

		double[ ] dRes = new double[ duration ];
		
		int j = 0;
		for( long i = lStart; i >= lEnd; ) {
			String index = new Long( i ).toString();
			if ( DAILY_USAGE_SERIES_LifeTime.has( index )) {
				dRes[j] = DAILY_USAGE_SERIES_LifeTime.get(index).getAsJsonArray().get( colunmIndx ).getAsDouble();
			}
			else
				dRes[j] = 0.0;
			
			if ( PACKAGE_TIMESERIES_LifeTime.has( index )) {
				JsonObject thisDayPkg = PACKAGE_TIMESERIES_LifeTime.get(index).getAsJsonObject();
				Iterator< Entry< String, JsonElement >> itr = thisDayPkg.entrySet().iterator();
				while( itr.hasNext() ) {
					Entry< String, JsonElement > entry = itr.next();
					String pkgId = entry.getKey();
					pkgId = "," + pkgId + ",";
					if ( packageIds.contains( pkgId )) {
						JsonArray aPkgEntry = entry.getValue().getAsJsonArray();
						dRes[j] += ( aPkgEntry.get(2).getAsDouble() * aPkgEntry.get(3).getAsDouble() );
					}
				}
			}
	
			j++;
			i = i - msecPerDay;
		}
		Max max = new Max();
		return max.evaluate( dRes );
	}
	
	public boolean isARPUInDecrOrder( JsonObject indicatorName, int numberOfMonths ) {
		JsonElement keysElement = indicatorName.get( "keys" );
		if( keysElement != null ) {
			JsonArray KeysArray = keysElement.getAsJsonArray();
			int arraySize = KeysArray.size();
			if ( arraySize < numberOfMonths )
				return false;

			for( int i=numberOfMonths; i>1; i-- ) {
				LocalDate currentTime = LocalDate.now();
				currentTime = currentTime.minusMonths( i );
				String currentMonthKeys = String.valueOf( currentTime.getYear() * 100 + currentTime.getMonthOfYear() );
				currentTime = currentTime.minusMonths( -1 );
				String nextMonthKeys = String.valueOf( currentTime.getYear() * 100 + currentTime.getMonthOfYear() );

				JsonElement eARPUValueCurrMonth = indicatorName.get( currentMonthKeys );
				JsonElement eARPUValueNextMonth = indicatorName.get( nextMonthKeys );
				if ( eARPUValueCurrMonth != null  && eARPUValueNextMonth != null) {
					if( eARPUValueCurrMonth.getAsLong() < eARPUValueNextMonth.getAsLong() )
						return false;
				}
				else
					return false;
			}
		}
		else
			return false;
		return true;
	}
	
	
	public long getTimeSinceOfferTriggered( String programID, String offerID, String offerTriggerType ) {
		if( offerTriggerType.equals( "notify" ) || offerTriggerType.equals( "monitor" ) || offerTriggerType.equals( "fulfil" ) ) {

			JsonElement jProgramSet = customerState.subscriberIndicators.get( "PROGRAMS_LifeTime-L" );
			if( jProgramSet != null ) {
				JsonObject jProgramInstance = null;
				JsonArray programHistoryArray = null;
				JsonElement programHistory = ( ( JsonObject ) jProgramSet ).get( programID );
				if( programHistory != null ) {
					programHistoryArray = programHistory.getAsJsonArray();
					if( programHistoryArray.size() != 0 ) {
						for( int programElePos = programHistoryArray.size() - 1; programElePos >= 0; programElePos-- ) {
							jProgramInstance = programHistoryArray.get( programElePos ).getAsJsonObject();
							if( ! offerID.equals( "*" ) ) {
								JsonElement thisOfferHistory = jProgramInstance.get( offerID );
								if( thisOfferHistory != null ) {
									JsonArray thisOfferHistoryArray = thisOfferHistory.getAsJsonArray();
									if( thisOfferHistoryArray.size() > 0 ) {
										JsonObject latestOfferHistory = thisOfferHistoryArray.get( thisOfferHistoryArray.size() - 1 ).getAsJsonObject();
										JsonElement jele = null;
										if( offerTriggerType == "notify" )
											jele = latestOfferHistory.get( "notificationAction" );
										else if( offerTriggerType == "fulfil" )
											jele = latestOfferHistory.get( "fulfillmentAction" );
										else
											jele = latestOfferHistory.get( "monitoringAction" );
										
										long currtime = customerState.tupleMap.get( "RECORD_TIMESTAMP" ).getAsLong();
										if( jele != null ) {
											long lTimeVal = jele.getAsLong();
											return ( currtime - lTimeVal );
										}
										break;
									}
								}
							}else {
								
								long latestOfferTriggerTime = 0;
								Iterator< Entry< String, JsonElement >> itr = jProgramInstance.entrySet().iterator();

								while( itr.hasNext() ) {
									String key = itr.next().getKey();
									JsonArray jOfferIdArr = jProgramInstance.get( key ).getAsJsonArray();
									JsonObject thisOfferIns = jOfferIdArr.get( jOfferIdArr.size() - 1 ).getAsJsonObject();
									JsonElement jele = null;
									if( offerTriggerType == "notify" )
										jele = thisOfferIns.get( "notificationAction" );
									else if( offerTriggerType == "fulfil" )
										jele = thisOfferIns.get( "fulfillmentAction" );
									else
										jele = thisOfferIns.get( "monitoringAction" );
									if ( jele != null ) {
									long contactTime = jele.getAsLong();
									if ( contactTime > latestOfferTriggerTime )
										latestOfferTriggerTime = contactTime;
									}
								}
								
								if( latestOfferTriggerTime == 0 )
									return DAYS_SINCE_DEFAULT;
								return ( customerState.tupleMap.get( "RECORD_TIMESTAMP" ).getAsLong() - latestOfferTriggerTime );
							}
						}
					}
				}
			}

		}
		return DAYS_SINCE_DEFAULT;
	}
	
	
	public double getMostFrequentRechargeValue( int rechargeCount, String returnRechargeType ){
		double defaultVal = -1;
		int maxCount = 0;
		ArrayList <Double >rechargeDenoArray = new ArrayList<Double>();
		ArrayList <Integer >rechargeCountArray = new ArrayList<Integer>();
		ArrayList <Double >maxCountDenoArray = new ArrayList<Double>();



		JsonElement eRechargeBlocks = customerState.subscriberIndicators.get( "RECHARGE_BLOCK_SERIES_LifeTime-L" );
		if( eRechargeBlocks == null )
			return defaultVal;
		JsonObject jRechargeBlocks = ( JsonObject ) eRechargeBlocks;
		JsonElement keys = jRechargeBlocks.get( "keys" );

		if( keys == null  )
			return defaultVal;
		JsonArray keyArray = keys.getAsJsonArray();
		int indRechargeCount = rechargeCount;
		int keyArraySize = keyArray.size();
		if( keyArraySize < indRechargeCount )
			indRechargeCount = keyArraySize;

		HashMap< Double, Integer > finalDataMap = new HashMap<Double,Integer >(); 

		for(int i=0 ;i< indRechargeCount ;i++){
			JsonElement jEntry = keyArray.get( keyArraySize - 1 );
			String sEntry = jEntry.getAsString();

			JsonObject thisBlock = (JsonObject) jRechargeBlocks.get(sEntry);
			if( thisBlock == null || thisBlock.isJsonNull())
				continue;
			JsonElement rechargeBlockData = thisBlock.get( "RECHARGE_DATA");
			if( rechargeBlockData == null )
				continue;
			JsonArray rechargeBlockDataArray = rechargeBlockData.getAsJsonArray(); 
			if(  rechargeBlockDataArray.size() < 3 )
				continue;
			Double dRechargeDeno = rechargeBlockDataArray.get(0).getAsDouble();
			if( !finalDataMap.containsKey( dRechargeDeno ) )
				finalDataMap.put( dRechargeDeno, 1 );
			else
				finalDataMap.put( dRechargeDeno, finalDataMap.get( dRechargeDeno )+1 );

			keyArraySize--;
		}

		Set< Entry< Double, Integer >> es = finalDataMap.entrySet();
		Iterator< Entry< Double,Integer >> itr = es.iterator();



		while( itr.hasNext() ) {
			Entry< Double, Integer > elem = itr.next();
			rechargeDenoArray.add(  elem.getKey());
			rechargeCountArray.add(  elem.getValue());
		}

		maxCount = Collections.max( rechargeCountArray);
		for( int i=0 ;i <rechargeCountArray.size();i++ ) {
			if( rechargeCountArray.get( i ) == maxCount )
				maxCountDenoArray.add( rechargeDenoArray.get( i ) );
		}

		if( returnRechargeType.equalsIgnoreCase("HIGH" ) )
			return Collections.max( maxCountDenoArray );
		else if(  returnRechargeType.equalsIgnoreCase( "LOW" ) )
			return Collections.min( maxCountDenoArray );
		else
			return defaultVal;

	}
	
	public boolean houseKeepTriggerHistory( long numDays ) {
//		EVENT_TRIGGER_LifeTime-L": {
//		"DTAC_REVENUE_STRETCH_TARGET_DETECTED": {
//			DTAC_REVENUE_STRETCH_TARGET_DETECTED : [ [ 1428268802781, "66813044911-DTAC_REVENUE_STRETCH_TARGET_DETECTED-1428268802781" ] ],
//			DTAC_REVENUE_STRETCH_TARGET_DETECTED_SCHEDULED_REMINDER : [ [ 1428268802781, "66813044911-DTAC_REVENUE_STRETCH_TARGET_DETECTED-1428268802781" ] ],
//			DTAC_REVENUE_STRETCH_TARGET_DETECTED_PARTIAL_REMINDER : [ [ 1428268802781, "66813044911-DTAC_REVENUE_STRETCH_TARGET_DETECTED-1428268802781" ] ],
//		}	
		long limit = getCurrentTime() - ( numDays * msecPerDay ) ;

		if( customerState.subscriberIndicators == null )
			return true;
		JsonObject subscriberEvents = ( JsonObject ) customerState.subscriberIndicators.get( "EVENT_TRIGGER_LifeTime-L" );
		Iterator< Entry< String, JsonElement >> itr = subscriberEvents.entrySet().iterator();
		while( itr.hasNext() ) {
			Entry< String, JsonElement > entry = itr.next();
			JsonObject mainEventHistory = entry.getValue().getAsJsonObject();
			Iterator< Entry< String, JsonElement >> itrSub = mainEventHistory.entrySet().iterator();
			while( itrSub.hasNext() ) {
				JsonArray subEventHistory = itrSub.next().getValue().getAsJsonArray();
				Iterator< JsonElement > arrItr = subEventHistory.iterator();
				while( arrItr.hasNext() ) {
					JsonArray eachHistory = arrItr.next().getAsJsonArray();
					if( eachHistory.get( 0 ).getAsLong() < limit )
						arrItr.remove();
				}
				if( subEventHistory.size() == 0 )
					itrSub.remove();
			}
		}
		return true;
	}
	
	public boolean houseKeepMapSeriesByElementCount( JsonObject series, int retentionElements ) {
		int size = series.entrySet().size();
		if( size <= retentionElements )
			return true;
		long[] days = new long[ size ];
		Iterator< Entry< String, JsonElement >> itr = series.entrySet().iterator();
		int i = 0;
		while( itr.hasNext() ) {
			Entry< String, JsonElement > entry = itr.next();
			String key = entry.getKey();
			days[ i++ ] = Long.parseLong( key );
		}
		Arrays.sort( days );
		for( int j=0; j<( size-retentionElements); j++ ) {
			String key = new Long( days[ j ] ).toString();
			series.remove( key );
		}
		return true;
	}
	
	
	/**
	 * Returns daily offer count for the specified program id, offer id.
	 * @param programId - program identification.
	 * @param offerId - offer identification.
	 * @param countType - this cane MonitoringCounts, FulfillmentCounts.
	 * @param recordDayStamp - recordDayStamp.
	 * @return count for the record day stamp if available, 0 otherwise.
	 */
	public long getDailyOfferCount( String programId, String offerId, String countType, long recordDayStamp ) {
		java.lang.StringBuilder sb = new java.lang.StringBuilder( "TELCO_" );
		sb.append( programId );
		sb.append( "--" );
		sb.append( offerId );
		sb.append( "-" );
		sb.append( commonsState.nWorkerId );
		sb.append( "-" );
		sb.append(countType);
		String finalKey= sb.toString();
		

		JsonObject jOfferCount = commonsState.summaryIndicatorsMap.get( finalKey );
		if( jOfferCount == null && Commons.pandaCache != null ) {
			String keyValue = ( String ) Commons.pandaCache.get( finalKey );
			if( keyValue != null ) {
				jOfferCount = ( JsonObject ) new JsonParser().parse( keyValue );
				commonsState.summaryIndicatorsMap.put(finalKey,jOfferCount);
			}
		}
		if( jOfferCount != null && !jOfferCount.isJsonNull() && jOfferCount.isJsonObject() ) {
			JsonElement dailyCountTarget = jOfferCount.get( "DailyCount-target" );
			if( dailyCountTarget != null && !dailyCountTarget.isJsonNull() && dailyCountTarget.isJsonObject() ) {
				JsonElement countElement = dailyCountTarget.getAsJsonObject().get( String.valueOf( recordDayStamp ) );
				if( countElement != null && !countElement.isJsonNull() && countElement.isJsonPrimitive() ) {
					return countElement.getAsLong();
				}
			}
		}
		return 0;
	}
	
	public JsonElement getRawField ( String name ) throws SiftFeedRejectException, SiftMandatoryFieldException{
		JsonElement eVal = tupleJsonData.get( name );
		if ( eVal == null || eVal.isJsonNull() || eVal.toString().trim().length() == 0 )
			return new JsonPrimitive( customerState.currentMapping.get( "defaultValue" ).getAsString());
		else
			return eVal;
	} 
	
	public long getLastFulfilDate( String programID, String offerID ) {
		JsonElement jProgramSet = customerState.subscriberIndicators.get( "PROGRAMS_LifeTime-L" );
		if( jProgramSet == null || jProgramSet.isJsonNull() )
			return DAYS_SINCE_DEFAULT;

		JsonElement programHistory = jProgramSet.getAsJsonObject().get( programID );
		if( programHistory == null ) 
			return DAYS_SINCE_DEFAULT;

		JsonObject jProgramInstance = null;
		JsonArray programHistoryArray = programHistory.getAsJsonArray();

		if( programHistoryArray.size() == 0 )
			return DAYS_SINCE_DEFAULT;

		jProgramInstance = programHistoryArray.get( 0 ).getAsJsonObject();
		JsonElement offerEle = jProgramInstance.get( offerID );
		if( offerEle == null || offerEle.isJsonNull() )
			return DAYS_SINCE_DEFAULT;

		JsonArray jOfferIdArr = offerEle.getAsJsonArray();
		JsonObject thisOfferIns = jOfferIdArr.get( jOfferIdArr.size() - 1 ).getAsJsonObject();
		JsonElement jele = thisOfferIns.get( "fulfillmentAction" );
		if ( jele == null || jele.isJsonNull() ) 
			return DAYS_SINCE_DEFAULT;
		
		if( jele.isJsonArray() ) {
			JsonArray fulfilArray = jele.getAsJsonArray();
			if( fulfilArray.size() == 0 )
				return DAYS_SINCE_DEFAULT;
			
			return fulfilArray.get( fulfilArray.size() - 1 ).getAsLong();
		}
		
		return jele.getAsLong();
	}
	
	public long getNextContactTime( String channel, long contactTime ) {
		Commons.logger.info( "Fetching the time of contact based on time: " + contactTime );
		String[] days = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
		int dayOfWeek = -1;
		int hourOfDay = -1;
		long dayStamp = -1L;
		
		Date date = new Date( contactTime );
		Calendar cal = Calendar.getInstance();
		cal.setTime( date );
		
		cal.setTimeZone(TimeZone.getTimeZone("GMT"));
		
		dayOfWeek = cal.get( Calendar.DAY_OF_WEEK ) - 1;
		hourOfDay = cal.get( Calendar.HOUR_OF_DAY );
		dayStamp = contactTime - ( contactTime % 86400000L );

		if ( ! Commons.siftConfig.has( "contactWindows" ) )
			return -1L;

		JsonObject contactWindows = Commons.siftConfig.get( "contactWindows" ).getAsJsonObject();
		JsonElement thisChannelEle = contactWindows.get( channel );
		if( ( thisChannelEle == null ) || ( thisChannelEle.isJsonNull() ) )
			return -1L;

		JsonObject thisChannelContact = thisChannelEle.getAsJsonObject();
		JsonElement thisDayEle = thisChannelContact.get( days[ dayOfWeek ] );
		if ((thisDayEle == null) || (thisDayEle.isJsonNull()))
			return -1L;

		JsonArray thisDay = thisDayEle.getAsJsonArray();
		int startHour = Integer.parseInt( thisDay.get( 0 ).getAsString().split( ":" )[ 0 ]);
		int endHour = Integer.parseInt( thisDay.get( 1 ).getAsString().split( ":" )[ 0 ] );
		if( ( hourOfDay >= startHour ) && ( hourOfDay < endHour ) )
			return -1;
		
		if ( hourOfDay < startHour ) {
			JsonArray nextContactDay = thisChannelContact.get( days[ dayOfWeek ] ).getAsJsonArray();
			int nextContactHour = Integer.parseInt( nextContactDay.get( 0 ).getAsString().split( ":" )[ 0 ]);
			return dayStamp + ( nextContactHour * 3600000 );
		}
		
		if ( hourOfDay >= endHour ) {
			dayOfWeek ++;
			
			if ( dayOfWeek == 7 )
				dayOfWeek = 0;

			JsonArray nextContactDay = thisChannelContact.get( days[ dayOfWeek ] ).getAsJsonArray();
			int nextContactHour = Integer.parseInt( nextContactDay.get( 0 ).getAsString().split( ":" )[ 0 ] );
			return dayStamp + 86400000L + ( nextContactHour * 3600000 );
		}
		return -1L;
	}
	
	public long getMonitoringEndtime( String programId, String offerId ) {
		JsonElement eEventTag = customerState.subscriberEventTags.get( programId + "_FT_" + offerId );
		if( eEventTag == null || eEventTag.isJsonNull() )
			return -1;
		
		JsonObject thisTag = eEventTag.getAsJsonObject();
		return thisTag.get( "MONITORING_END_TIME" ).getAsLong();
	}
	
	public JsonObject httpPostRequest( JsonObject jReq, String requestURL ) {
		try { 
			if( httpClient == null )
				httpClient = HttpClientBuilder.create().build();

			Commons.logger.info( "Reserve Number input to the web service call: " + jReq.toString() );
			HttpPost request = new HttpPost( requestURL );
			String jReqString = jReq.toString();
			StringEntity params = new StringEntity( jReqString );
			request.addHeader( "content-type", "application/json" );
			request.setEntity( params );
			HttpResponse response = httpClient.execute( request );
			BufferedReader breader = new BufferedReader( new InputStreamReader( response.getEntity().getContent() ) );

			String responseOutput = breader.readLine();
			Commons.logger.info( "Reserve Number output from web service call: " + responseOutput );
			if( responseOutput == null )
				return new JsonObject();

			return ( JsonObject ) Commons.parser.parse( responseOutput );
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}

		return new JsonObject();
	}
	
	public void createMapping( String contractId, String MSISDN ) {
		JsonObject thisMapping = new JsonObject();
		thisMapping.addProperty( "MSISDN", MSISDN );
		thisMapping.addProperty( "docType", "IDMapping" );
		Commons.logger.info( "Adding the contractId and Msisdn mapping: " + thisMapping.toString() );
		Commons.pandaCache.set( contractId + "-IDMapping", 0, thisMapping.toString() );
	}
	
	public void deleteMapping( String contractId ) {
		Commons.logger.info( "Deleting the contract Id mapping for id: " + contractId );
		Commons.pandaCache.delete( contractId + "-IDMapping" );
	}
	
	public String getMappedMsisdn( String contractId ) {
		Object contractIdObject = Commons.pandaCache.get( contractId + "-IDMapping" );
		Commons.logger.info( "Value of contract Id object: " + contractIdObject );
		if( contractIdObject == null ) {
			Commons.logger.info( "Returning -99 as value of contract Id object " );
			return "-99";
		}
		
		JsonObject contractIdJson = ( JsonObject ) Commons.parser.parse( contractIdObject.toString() );
		Commons.logger.info( "Value of contract Id object before returning: " + contractIdJson.toString() );
		return contractIdJson.get( "MSISDN" ).getAsString();
	}
	
	
	
	public static void main( String[ ] arg ) throws ParseException, URISyntaxException, CompileException, NoSuchMethodException, InstantiationException, InvocationTargetException, IllegalAccessException, PExpressionException {
		LinkedList< URI > uris = new LinkedList< URI >();
		URI uri = new URI( "http://127.0.0.1:8091/pools" );
		uris.add( uri );
		
		PandaCache cache = new PandaCache( uris );
		PandaCacheConfig cacheCofig = new PandaCacheConfig( uris );

		JsonParser parser = new JsonParser();
		Commons.parser=parser;
		Commons.pandaCacheConfig = cacheCofig;
		
		CustomerState cstate = new CustomerState();
		SiftMath thisObj = new SiftMath( cstate );
		cstate.customerId = "66123123123";
		cstate.siftMath = thisObj;

		String hisIndicators = ( String ) cache.getClient().get( cstate.customerId + "-Indicators" );
		cstate.subscriberIndicators = parser.parse( hisIndicators ).getAsJsonObject();
//		JsonObject DTAC_PKG_HIST_SERIES_LifeTime = cstate.subscriberIndicators.get("DTAC_PKG_HIST_SERIES_LifeTime-L").getAsJsonObject(); 
		
//		double value = thisObj.getValueFromMultiSeries(DTAC_PKG_HIST_SERIES_LifeTime, "COUNT", 5, "3,5", "DATA|COMBO,0", "CONTAINS,>", "String,double", 4, 30);
//		System.out.println( value );
		
//		System.out.println( thisObj.evaluateRScript( "/StreamsData/siftmeta/rscripts/PP_ROCKET_NBO_R.json" ) );
		System.out.println(  SiftMath.getCurrentTime() );
	}
}