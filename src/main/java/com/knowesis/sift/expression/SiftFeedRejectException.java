package com.knowesis.sift.expression;

public class SiftFeedRejectException extends PExpressionException {
	public SiftFeedRejectException( String exp ){
		super( exp );
	}
}
