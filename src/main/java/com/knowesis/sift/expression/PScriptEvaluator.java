package com.knowesis.sift.expression;

import java.util.Arrays;

import org.codehaus.commons.compiler.CompileException;

/**
 * @author Raja SP
 * 
 */
public class PScriptEvaluator extends org.codehaus.janino.ScriptEvaluator implements java.io.Serializable {

	private Class optionalExpressionType;
	private String[ ] parameterNames;
	private Class[ ] parameterTypes;
	private Class[ ] thrownExceptions;
	private String[ ] optionalDefaultImports;
	private String expression;
	private String name;
	private String type; // is this a rule or a computation
	private String serviceType;
	private String serviceSubType;
	private String frequency;

	public String getServiceSubType() {
		return serviceSubType;
	}

	public void setServiceSubType( String serviceSubType ) {
		this.serviceSubType = serviceSubType;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType( String serviceType ) {
		this.serviceType = serviceType;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency( String frequency ) {
		this.frequency = frequency;
	}

	/**
	 * @return the expressionType
	 */
	public String getType() { // computation or detection
		return type;
	}

	/**
	 * @param expressionType
	 *            the expressionType to set
	 */
	public void setType( String type ) {
		this.type = type;
	}

	public PScriptEvaluator(){
	}

	public Class getOptionalExpressionType() {
		return optionalExpressionType;
	}

	public void setOptionalExpressionType( Class optionalExpressionType ) {
		this.optionalExpressionType = optionalExpressionType;
		super.setReturnType( optionalExpressionType );
	}

	public String[ ] getParameterNames() {
		return parameterNames;
	}

	public void setParameterNames( String[ ] parameterNames ) {
		this.parameterNames = parameterNames;
	}

	public Class[ ] getParameterTypes() {
		return parameterTypes;
	}

	public void setParameterTypes( Class[ ] parameterTypes ) {
		this.parameterTypes = parameterTypes;
	}

	public void setParameters( String[ ] parameterNames, Class[ ] parameterTypes ) {
		this.parameterNames = parameterNames;
		this.parameterTypes = parameterTypes;
		super.setParameters( parameterNames, parameterTypes );
	}

	public Class[ ] getThrownExceptions() {
		return thrownExceptions;
	}

	public void setThrownExceptions( Class[ ] thrownExceptions ) {
		this.thrownExceptions = thrownExceptions;
	}

	public String[ ] getOptionalDefaultImports() {
		return optionalDefaultImports;
	}

	public void setOptionalDefaultImports( String[ ] optionalDefaultImports ) {
		this.optionalDefaultImports = optionalDefaultImports;
	}

	public String getExpression() {
		return expression;
	}

	public void setExpression( String expression ) {
		this.expression = expression;
	}

	public String getName() {
		return name;
	}

	public void setName( String name2 ) {
		this.name = name2;
		// TODO Auto-generated method stub

	}

	public void validateExpression( String name, Class optionalExpressionType, String[ ] parameterNames,
			Class[ ] parameterTypes, String type, String serviceType, String frequency, String expression )
			throws PExpressionException {

		PScriptEvaluator anExpEval = new PScriptEvaluator();
		anExpEval.setName( name );
		anExpEval.setReturnType( optionalExpressionType );
		anExpEval.setParameters( parameterNames, parameterTypes );
		anExpEval.setType( type );
		anExpEval.setServiceType( serviceType );
		anExpEval.setFrequency( frequency );
		try {
			anExpEval.cook( expression );
		}catch( CompileException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new PExpressionException( e.getMessage() );
		}
	}

	public void validateExpression() throws PExpressionException {
		try {
			cook( expression );
		}catch( CompileException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new PExpressionException( e.getMessage() );
		}
	}

	@Override
	public String toString() {
		return "PExpressionEvaluator [optionalExpressionType=" + optionalExpressionType + ", parameterNames="
				+ Arrays.toString( parameterNames ) + ", parameterTypes=" + Arrays.toString( parameterTypes )
				+ ", thrownExceptions=" + Arrays.toString( thrownExceptions ) + ", optionalDefaultImports="
				+ Arrays.toString( optionalDefaultImports ) + ", expression=" + expression + ", name=" + name
				+ ", indicatorType=" + type + ", serviceType=" + serviceType + ", frequency=" + frequency + "]";
	}

}