package com.knowesis.sift.expression;


public class SiftDateFormatException extends PExpressionException{
	
	public SiftDateFormatException( String exp ){
		super( exp );
	}
}
